import * as Sentry from "@sentry/nextjs";

const SENTRY_DSN = process.env.SENTRY_DSN || process.env.NEXT_PUBLIC_SENTRY_DSN;

Sentry.init({
  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  // ...
  // Note: if you want to override the automatic release value, do not set a
  // `release` value here - use the environment variable `SENTRY_RELEASE`, so
  // that it will also get attached to your source maps
  dsn:
    process.env.NODE_ENV === "production"
      ? SENTRY_DSN
      : false,
  environment: process.env.NODE_ENV,
  // integrations: [
  //   new BrowserTracing({
  //     routingInstrumentation: Sentry.reactRouterV6Instrumentation(
  //       useLocation,
  //       useNavigationType,
  //       createRoutesFromChildren,
  //       matchRoutes
  //     ),
  //   }),

  //   new Sentry.Replay({
  //     maskAllText: true,
  //     blockAllMedia: true,
  //   }),
  // ],
  replaysSessionSampleRate: 0.05,
  replaysOnErrorSampleRate: 1.0,
  tracesSampleRate: 0.1,
  ignoreErrors: ["Blocked a frame with origin", "Network Error"],
});