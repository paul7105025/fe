declare module '$tool/ToolLNB';
declare module '$tool/ToolPage';
declare module '$tool/ChatToolLNB';
declare module '$editor/EditorDocumentPage';
declare module '$editor/EditorDocumentListPage';
declare module '$chat/ChatPage';
declare module '$chat/InappChatPage';
declare module '$chat/LeftChatNavigationContainer'
declare module '*.svg' {
  import React = require('react');
  export const ReactComponent: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  const src: string;
  export default src;
}
