import { getTool, getUser, syncHeader } from "@wrtn/core";
import { access } from "fs";
import {
  GetServerSideProps,
  GetServerSidePropsContext,
  GetServerSidePropsResult,
} from "next";
import { paths } from "src/constants";
import { readAccessKey } from "src/utils";
export type AuthRedirectUrl = {
  success?: string,
  failure?: string
}

export type AuthCheckOptions<T = {}> = {
  returnParam?: GetServerSidePropsResult<T>,
  redirect?: AuthRedirectUrl,
  updateUser?: boolean,
  updateTool?: boolean,
}
export const withAuthCheck = async<T = any>(
  ctx: GetServerSidePropsContext,
  options?: AuthCheckOptions<T>
): Promise<GetServerSidePropsResult<any>> => {
  const updateUser = (async () => {
    syncHeader({ key: 'Authorization', value: `Bearer ${accessKey}`});
    const res = await getUser();
    if (res.status === 200) {
      return res.data.data;
    }
    return null;
  });
  const updateTool = (async () => {
    const res = await getTool();
    if (res.status === 200) {
      return res.data.data;
    }
    return null;
  })
  const accessKey = await readAccessKey(ctx);

  if (accessKey) {
    const user = options?.updateUser ? await updateUser() : null;
    const tool = options?.updateTool ? await updateTool() : null;
    if (options?.redirect?.success) {
      return ({
        redirect: { permanent: false, destination: options.redirect.success }
      });
    }
    return (
      options?.returnParam || { props: { isAuth: true, accessKey: accessKey, user, tool } }
    );
  }

  if (options?.redirect?.failure) {
    return ({
      redirect: { permanent: false, destination: options.redirect.failure }
    });
  }
  return (
    options?.returnParam || { props: { isAuth: false } }
  );
};

// 



// withAuthCheck => 로그인 검증 후 로그인 여부를 returnParmas.props.isAuth에 추가해줌.
// 만약, redirect가 존재한다면, serverside에서 리다이렉트.
// 성공 시, failure 시 둘 다 할 수 있음.