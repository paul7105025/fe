import { ModalPortal, ModalPositionPortal } from "@wrtn/ui/components/ModalPortal"

export const ChannelLayout = ({ children }: React.PropsWithChildren) => {

  return (
    <>
      {/* <ModalPositionPortal
        position={{
          bottom: 40,
          right: 40
        }}
      >
        <div id="channel-custom-btn">채널톡</div>
      </ModalPositionPortal> */}
      {children}
    </>
  )
}
export default ChannelLayout;