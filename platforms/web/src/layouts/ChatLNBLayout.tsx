import dynamic, { Loader, LoaderComponent } from "next/dynamic";
import styled, { css } from "styled-components";
import React, { PropsWithChildren } from "react";

import {
  colors,
  FlexButton,
  FlexWrapper,
  IconArrowDown,
  IconChevron,
  newColors,
  typo,
} from "@wrtn/ui";
import { MFELoadingPage, MFEWrapper } from "@wrtn/ui/containers/MFE";
import { currentToolChatIdState, ToolType } from "@wrtn/core";
import { useRecoilState } from "recoil";
import useLNBMenu from "src/hooks/layout/useLNBMenu";
import { GuideButtonLNB } from "src/containers/Button";

const ChatLNB = dynamic<{
  onClose?: () => void;
}>(() => import("$chat/LeftChatNavigationContainer"), {
  ssr: false,
  loading: () => <MFELoadingPage />,
});

const ToolLNB = dynamic<{
  onClose?: () => void;
}>(() => import("$tool/ToolLNB"), {
  ssr: false,
  loading: () => <MFELoadingPage />,
});

export const ChatMobileLNB = ({ onClose }) => {
  const { handleChangeLNB, isChat } = useLNBMenu();

  return (
    <MobileLNBWrapper isOpen={true}>
      <BackButton onClick={onClose}>
        <IconChevron />
      </BackButton>
      <Visible visible={isChat}>
        <ChatLNB onClose={onClose} />
      </Visible>
    </MobileLNBWrapper>
  );
};

export const ToolMobileLNB = ({ onClose }) => {
  const { handleChangeLNB, isChat } = useLNBMenu();

  return (
    <MobileLNBWrapper isOpen={true}>
      <BackButton onClick={onClose}>
        <IconChevron />
      </BackButton>
      <Visible visible={true}>
        {
          <ToolLNB onClose={onClose} />
        }
      </Visible>
    </MobileLNBWrapper>
  );
};

export const ChatLNBLayout = ({ children }: PropsWithChildren) => {
  const { handleChangeLNB, isChat, isOpen } = useLNBMenu();

  return (
    <Wrapper>
      <LNBWrapper isOpen={isOpen}>
        <Visible visible={isChat}>
          <ChatLNB />
          <Divider />
          <GuideButtonLNB />
        </Visible>
      </LNBWrapper>
      <ContentWrapper isOpen={isOpen}>{children}</ContentWrapper>
    </Wrapper>
  );
};

const LNBWrapper = styled.div<{ isOpen: boolean }>`
  max-width: 235px;
  width: 235px;
  left: ${({ isOpen }) => (isOpen ? "0px" : "-235px")};
  position: absolute;
  height: 100%;
  transition: left 0.5s;
  background: ${colors.BACKGROUND};
  border-right: 1px solid ${newColors.GRAY_100};
`;

const BackButton = styled.button`
  width: 26px;
  height: 26px;
  background: none;
  border: none;
  margin: 20px;
  svg {
    width: 100%;
    height: 100%;
    transform: rotate(270deg);
    path {
      fill: #222;
    }
  }
`;

const MobileLNBWrapper = styled(LNBWrapper)`
  width: 100%;
  max-width: 100vw;
  background: white;
`;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
`;

const ContentWrapper = styled.div<{ isOpen: boolean }>`
  width: 100%;
  height: 100%;
  padding-left: ${({ isOpen }) => (isOpen ? "235px" : "0px")};
  transition: padding-left 0.5s;
`;

const Visible = styled.div<{ visible: boolean }>`
  width: 100%;
  height: calc(100% - 69px);
  display: ${(props) => (props.visible ? "inherit" : "none")};
`;

const Menu = styled(FlexWrapper)<{ isSelected: boolean }>`
  ${typo({
    weight: 500,
    size: "14px",
    height: "100%",
    color: colors.GRAY_60,
  })};
  cursor: pointer;
  width: 100%;
  flex: 1;

  padding-bottom: 15px;
  border-bottom: 1px solid ${colors.WHITE_BOLDER_LINE};

  ${({ isSelected }) =>
    isSelected &&
    css`
      ${typo({
        weight: 700,
        size: "14px",
        height: "100%",
        color: colors.GRAY_90,
      })};

      padding-bottom: 14px;
      border-bottom: 2px solid ${colors.POINT_PURPLE};
    `};
`;

const MenuWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 69px;
`;



const Divider = styled.div`
  width: 100%;
  height: 1px;
  background: ${newColors.GRAY_200};
  margin-top: 12px;
`;