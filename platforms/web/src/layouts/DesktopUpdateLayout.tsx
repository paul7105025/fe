import { FlexWrapper } from "@wrtn/ui";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import React, { PropsWithChildren } from "react";
import DesktopUpdateEventDialog from "src/components/Events/DesktopUpdateEventDialog";
import styled from "styled-components";

const LATEST_DESKTOP_VERSION = "1.0.8";

const needUpdating = (version: string) => {
  const [major, minor, patch] = version?.split(".");
  const [latestMajor, latestMinor, latestPatch] = LATEST_DESKTOP_VERSION?.split(".")  
  if (Number(latestMajor) > Number(major)) return true;
  if (Number(latestMajor) < Number(major)) return false;
  if (Number(latestMinor) > Number(minor)) return true;
  if (Number(latestMinor) < Number(minor)) return false;
  if (Number(latestPatch) > Number(patch)) return true;
  if (Number(latestPatch) < Number(patch)) return false;
  return false;
}
export const DesktopUpdateLayout = ({ children }: PropsWithChildren) => {
  const [isOldDesktop, setIsOldDesktop] = React.useState(false)

  React.useEffect(() => {
    const ua = window.navigator.userAgent;
    if (ua.includes("wrtn-desktop/")) {
      // const versionIdx = ua.search("wrtn-desktop/");
      const version = ua.split("wrtn-desktop/")[1].split(' ')[0];
      if (needUpdating(version)) {
        setIsOldDesktop(true)
      }
    }
  }, [])
  return (
    <>
      {children}
      {
        isOldDesktop && 
        <ModalPortal zIndex={99}>
          <DialogWrapper align="center" justify="center">
            <SingleDialogWrapper>
              <DesktopUpdateEventDialog
                onClose={() => setIsOldDesktop(false)}
                onCloseDay={() => setIsOldDesktop(false)}
              />
            </SingleDialogWrapper>
          </DialogWrapper>
        </ModalPortal>
      }
    </>
  )
}

export default DesktopUpdateLayout;

const SingleDialogWrapper = styled(FlexWrapper)`
  position: absolute;
`

const DialogWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  position: relative;
`;