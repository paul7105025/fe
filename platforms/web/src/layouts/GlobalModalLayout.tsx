import { useModalOpen } from "@wrtn/core";
import { PropsWithChildren } from "react";
import {
  FavoriteToolModal,
  TemplateEditModal,
  TemplateListModal,
  TemplateDeleteCheckModal,
} from "../components/Modal";
import TemplateAddModal from "../components/Modal/TemplateAddModal";
import BlockChatGPT4LimitModal from "src/components/Modal/BlockChatGPT4LimitModal";
import { ErrorModalLayout } from "./ErrorModalLayout";
import { BanAbuseModal, ChatDeleteCheckModal } from "../components/Modal";

export const GlobalModalLayout = ({ children }: PropsWithChildren) => {
  const favoriteToolModal = useModalOpen({ modalId: "favoriteModal" });
  const templateEditModal = useModalOpen({ modalId: "templateEditModal" });
  const templateListModal = useModalOpen({ modalId: "templateListModal" });
  const templateAddModal = useModalOpen({ modalId: "templateAddModal" });
  const templateDeleteCheckModal = useModalOpen({
    modalId: "templateDeleteCheckModal",
  });
  const blockChatGPT4LimitModal = useModalOpen({
    modalId: "blockChatGPT4Limit",
  });
  const banAbuseDialogModal = useModalOpen({ modalId: "banAbuseModal" });
  const chatDeleteCheckModal = useModalOpen({
    modalId: "chatDeleteCheckModal",
  });

  return (
    <ErrorModalLayout>
      {children}
      {favoriteToolModal.isOpen && <FavoriteToolModal />}
      {templateEditModal.isOpen && <TemplateEditModal />}
      {templateListModal.isOpen && <TemplateListModal />}
      {templateAddModal.isOpen && <TemplateAddModal />}
      {templateDeleteCheckModal.isOpen && <TemplateDeleteCheckModal />}
      {blockChatGPT4LimitModal.isOpen && <BlockChatGPT4LimitModal />}
      {banAbuseDialogModal.isOpen && <BanAbuseModal />}
      {chatDeleteCheckModal.isOpen && <ChatDeleteCheckModal />}
    </ErrorModalLayout>
  );
};
