import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useRecoilState, useRecoilValue } from "recoil";
import styled, { css } from "styled-components";
import { newTypo, newColors } from "@wrtn/ui";
import { useAlert } from "react-alert";

import { useGoToTool } from "src/hooks/useGoToTool";

import {
  chatInputTypeState,
  chatToggleState,
  showMessenger,
  TOGGLE_BARD,
  useChannelBoot,
  useEvent,
  useHoverPopup,
  useLoginDialog,
  WRTN_COMMUNITY_HREF,
  WRTN_KAKAO_CHAT_HREF,
  WRTN_OPEN_KAKAO_HREF,
} from "@wrtn/core";

import { paths } from "src/constants";
import { useUser } from "src/hooks/useUser";
import useHoverMenu from "src/hooks/useHoverMenu";

import { Avatar } from "@wrtn/ui/components/Avatar";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";

import { FlexWrapper, colors, typo, FlexButton } from "@wrtn/ui/styles";
import {
  Icon,
  IconChatCircle,
  IconChevron,
  IconMenu,
  LogoWrtnPurple,
  WrtnLogoGray,
  WrtnLogoPurple,
} from "@wrtn/ui";
import { ChatLNBLayout, ChatMobileLNB, ToolMobileLNB } from "./ChatLNBLayout";
import useLNBMenu from "src/hooks/layout/useLNBMenu";

const aboutNavList = [
  {
    key: "뤼튼 가이드",
    href: "/about",
  },
];

const storeNavList = [
  {
    key: "전체",
    href: "/store",
  },
  {
    key: "툴",
    href: "/store?tab=tool",
  },
  {
    key: "챗봇",
    href: "/store?tab=chatbot",
  },
];

const guideNavList = [
  {
    key: "뤼튼 소개",
    href: "/about",
  },
  {
    key: "공식 커뮤니티",
    href: WRTN_COMMUNITY_HREF,
  },
  {
    key: "사용자 카톡방",
    href: WRTN_OPEN_KAKAO_HREF,
  },
  {
    key: "고객 문의 / 상담",
    href: "/guide",
  },
];

const downloadNavList = [
  {
    key: "Android & iOS",
    href: "/mobile",
  },
  {
    key: "Window & Mac",
    href: "/desktop",
  },
  {
    key: "카카오톡",
    href: WRTN_KAKAO_CHAT_HREF,
  },
];

const settingNavList = [
  {
    key: "계정 정보",
    href: paths.setting.account(),
  },
  {
    key: "요금제",
    href: paths.setting.plan(),
  },
  {
    key: "결제정보",
    href: paths.setting.invoice(),
  },
  {
    key: "쿠폰",
    href: paths.setting.coupon(),
  },
  {
    key: "툴 히스토리",
    href: paths.setting.history(),
  },
  {
    key: "로그아웃",
    href: paths.logout(),
  },
];

const CHANNEL_ID_PLUGIN_KEY =
  process.env.NEXT_PUBLIC_CHANNEL_ID_PLUGIN_KEY || "";

export const NavBar = ({ darkmode = false }) => {
  const { handleOpen, open, close } = useLNBMenu();

  const chatInputType = useRecoilValue(chatInputTypeState);

  // const [chatToggle, setChatToggle] = useRecoilState(chatToggleState);

  const [openMobileLNB, setOpenMobileLNB] = React.useState(false);
  const [openMobileChatLNB, setOpenMobileChatLNB] = React.useState(false);
  const [openMobileToolLNB, setOpenMobileToolLNB] = React.useState(false);

  const { collectEvent } = useEvent();
  const router = useRouter();
  const { user, name } = useUser();
  const loginDialog = useLoginDialog();
  useChannelBoot(CHANNEL_ID_PLUGIN_KEY);

  const guideHover = useHoverMenu({
    position: {
      top: 30,
      left: 0,
    },
  });

  const downloadHover = useHoverMenu({
    position: {
      top: 30,
      left: 0,
    },
  });

  const userHover = useHoverMenu({
    position: {
      top: 30,
      left: -20,
    },
  });

  const goToTool = useGoToTool();

  const goToChat = () => {
    if (router.pathname === paths.chat()) router.reload();
    else router.push(paths.chat());
  };

  const TOGGLE_SPEED = 0;
  const TOGGLE_PERFORMANCE = 1;

  const [chatToggle, setChatToggle] = useRecoilState(chatToggleState);
  const [openMobileUser, setOpenMobileUser] = React.useState(false);
  const [openMobileUserGuide, setOpenMobileUserGuide] = React.useState(false);
  const [openMobileDownLoadGuide, setOpenMobileDownLoadGuide] =
    React.useState(false);
  const [openMobileStore, setOpenMobileStore] = React.useState(false);
  const [openMobileChatToggle, setOpenMobileChatToggle] = React.useState(false);

  const hoverPopupGPT3 = useHoverPopup();
  const hoverPopupGPT4 = useHoverPopup();
  const alert = useAlert();

  React.useEffect(() => {
    if (typeof window !== "undefined") {
      if (window.innerWidth < 1279) {
        close();
      }
      window.addEventListener("resize", () => {
        if (window.innerWidth < 1279) {
          close();
        } else {
          open();
        }
      });
    }
  }, []);

  React.useEffect(() => {
    if (downloadHover.hover) {
      collectEvent("click_download_menu");
    }
  }, [downloadHover.hover]);

  const handleCopyLink = async () => {
    await navigator.clipboard.writeText(window.location.href);
    alert.show("링크를 복사했어요.");
  };

  return (
    <Switch isSticky={router.pathname === "/share"}>
      <Wrapper row isDark={darkmode}>
        <MainNavWrapper row>
          <MenuButton
            onClick={handleOpen}
            disabled={
              router.pathname !== "/tool/[[...toolId]]" &&
              router.pathname !== "/" &&
              router.pathname !== "/store"
            }
          >
            <Icon icon="bars" size={24} color={colors.GRAY_80} />
          </MenuButton>
          <LogoWrapper isDark={darkmode} onClick={goToChat}>
            <WrtnLogoPurple />
          </LogoWrapper>
          <NavWrapper row>
            <NavButton
              isDark={darkmode}
              isActive={router.pathname === "/" ? true : undefined}
              onClick={goToChat}
            >
              채팅
            </NavButton>
            <NavButton
              isDark={darkmode}
              isActive={
                router.pathname === "/tool/[[...toolId]]" ? true : undefined
              }
              onClick={goToTool}
            >
              툴
            </NavButton>
            <NavLink
              isDark={darkmode}
              isActive={router.pathname === "/editor" ? true : undefined}
              href="/editor"
            >
              에디터
            </NavLink>
            <NavLink
              isDark={darkmode}
              isActive={router.pathname === "/plugins" ? true : undefined}
              href="/plugins"
            >
              플러그인
            </NavLink>
          </NavWrapper>
        </MainNavWrapper>
        <GuideNavWrapper row>
          <NavWrapper row style={{ gap: "28px" }}>
            <NavLink
              isDark={darkmode}
              isActive={router.pathname.startsWith("/store") ? true : undefined}
              href="/store"
            >
              AI 스토어
            </NavLink>
            <NavLink
              isDark={darkmode}
              isActive={router.pathname === "/share" ? true : undefined}
              href="/share"
            >
              공유 트렌드
            </NavLink>
            <NavButton
              isDark={darkmode}
              ref={downloadHover.parentRef}
              onClick={() => {
                downloadHover.handleClick();
              }}
            >
              다운로드
              <ArrowButton isActive={downloadHover.hover}>
                <Icon icon="angle-down" color={colors.GRAY_80} size={20} />
              </ArrowButton>
            </NavButton>
          </NavWrapper>
          {downloadHover.open(({ position, childRef }) => {
            return (
              <ModalPositionPortal position={position}>
                <HoverWrapper column ref={childRef}>
                  <GuideOuterItemWrapper>
                    <GuideOuterItem
                      target="_self"
                      rel="noopener noreferrer"
                      href={downloadNavList[0].href} // 어플
                      key={downloadNavList[0].key}
                      onClick={() => {
                        collectEvent('view_download_mobile');
                      }}
                    >
                      {downloadNavList[0].key}
                    </GuideOuterItem>
                  </GuideOuterItemWrapper>
                  <GuideOuterItemWrapper>
                    <GuideOuterItem
                      target="_self"
                      rel="noopener noreferrer"
                      href={downloadNavList[1].href} // 데스크탑
                      key={downloadNavList[1].key}
                      onClick={() => {
                        collectEvent('view_download_desktop')
                      }}
                    >
                      {downloadNavList[1].key}
                    </GuideOuterItem>
                  </GuideOuterItemWrapper>
                  <GuideOuterItemWrapper
                    onClick={() => {
                      collectEvent("click_kakao_talk");
                    }}
                  >
                    <GuideOuterItem
                      target="_blank"
                      rel="noopener noreferrer"
                      href={downloadNavList[2].href} // 카카오톡
                      key={downloadNavList[2].key}
                    >
                      {downloadNavList[2].key}
                    </GuideOuterItem>
                  </GuideOuterItemWrapper>
                </HoverWrapper>
              </ModalPositionPortal>
            );
          })}
          <LoginWrapper>
            {user && (
              <>
                <Avatar width={"26px"} size={"16px"}>
                  {name && name.length > 0 ? name[0] : "W"}
                </Avatar>
                <NavButton
                  isActive={
                    router.pathname.includes("/setting") ? true : undefined
                  }
                  ref={userHover.parentRef}
                  onClick={userHover.handleClick}
                >
                  <p>{name}</p>
                  <ArrowButton isActive={userHover.hover}>
                    <Icon icon="angle-down" color={colors.GRAY_80} size={20} />
                  </ArrowButton>
                </NavButton>
                {userHover.open(({ position, childRef }) => {
                  return (
                    <ModalPositionPortal position={position}>
                      <HoverWrapper column ref={childRef}>
                        {settingNavList.map((v) => (
                          <>
                            {v.key === "로그아웃" && <Divider />}
                            <GuideItem href={v.href} key={v.key}>
                              {v.key}
                            </GuideItem>
                          </>
                        ))}
                      </HoverWrapper>
                    </ModalPositionPortal>
                  );
                })}
              </>
            )}
            {!user && (
              <NavButton
                isDark={darkmode}
                onClick={() => loginDialog.handleOpen()}
              >
                로그인
              </NavButton>
            )}
          </LoginWrapper>
        </GuideNavWrapper>
      </Wrapper>
      <MobileWrapper isDark={darkmode}>
        <MainNavWrapper>
          {router.pathname.includes("/share/[chatId]") || (
            <MenuButton onClick={() => setOpenMobileLNB(true)}>
              <IconMenu />
            </MenuButton>
          )}
          {router.pathname.includes("/share/[chatId]") && (
            <Link href="/share">
              <Icon icon="arrow-left" size={26} color={newColors.GRAY_700} />
            </Link>
          )}
          <LogoMobileWrapper isDark={darkmode} onClick={goToChat}>
            <WrtnLogoPurple />
          </LogoMobileWrapper>
        </MainNavWrapper>
        {router.pathname.includes("/share") && (
          <>
            <LogoArea>
              <ShareLogo>공유</ShareLogo>
              <Link href="/share">
                <TrandLogo>#트렌드</TrandLogo>
              </Link>
            </LogoArea>
            {router.pathname.includes("/share/[chatId]") || (
              <div style={{ width: "32px" }} />
            )}
          </>
        )}
        {router.pathname.includes("/share/[chatId]") && (
          <MenuButton onClick={handleCopyLink}>
            <Icon icon="upload" size={20} color={newColors.GRAY_500} />
          </MenuButton>
        )}
        {router.pathname === "/" && chatInputType === "default" && (
          <>
            <MobileChatToggle
              onClick={() => {
                setOpenMobileChatToggle((c) => !c);
              }}
              open={openMobileChatToggle}
            >
              {chatToggle === TOGGLE_SPEED ? (
                "GPT-3.5"
              ) : chatToggle === TOGGLE_PERFORMANCE ? (
                "GPT-4"
              ) : chatToggle === TOGGLE_BARD ? (
                <>
                  Bard <BETA selected={chatToggle === TOGGLE_BARD}>BETA</BETA>
                </>
              ) : (
                <></>
              )}
              <IconChevron />
              {openMobileChatToggle && (
                <ChatToggle>
                  <MobileToggle
                    onClick={() => {
                      setChatToggle(TOGGLE_SPEED);
                    }}
                    ref={hoverPopupGPT3.hoverRef}
                    onMouseEnter={hoverPopupGPT3.onMouseEnter}
                    onMouseLeave={hoverPopupGPT3.onMouseLeave}
                    selected={chatToggle === TOGGLE_SPEED}
                  >
                    GPT-3.5
                  </MobileToggle>
                  <MobileToggle
                    onClick={() => {
                      setChatToggle(TOGGLE_PERFORMANCE);
                    }}
                    ref={hoverPopupGPT4.hoverRef}
                    onMouseEnter={hoverPopupGPT4.onMouseEnter}
                    onMouseLeave={hoverPopupGPT4.onMouseLeave}
                    selected={chatToggle === TOGGLE_PERFORMANCE}
                  >
                    GPT-4
                  </MobileToggle>
                  {/* <Toggle 
                    onClick={() => { setChatToggle(TOGGLE_BARD) }}
                    ref={hoverPopupBard.hoverRef} 
                    onMouseEnter={hoverPopupBard.onMouseEnter} 
                    onMouseLeave={hoverPopupBard.onMouseLeave} 
                    selected={chatToggle === TOGGLE_BARD}>Bard <BETA selected={chatToggle === TOGGLE_BARD}>BETA</BETA></Toggle> */}
                </ChatToggle>
              )}
            </MobileChatToggle>
            {/* <ChatToggle
            // onClick={() =>
            //   setChatToggle(
            //     chatToggle === TOGGLE_SPEED ? TOGGLE_PERFORMANCE : TOGGLE_SPEED
            //   )
            // }
          >
            <Toggle 
              onClick={() => { setChatToggle(TOGGLE_SPEED) }}
              ref={hoverPopupGPT3.hoverRef} 
              onMouseEnter={hoverPopupGPT3.onMouseEnter} 
              onMouseLeave={hoverPopupGPT3.onMouseLeave} 
              selected={chatToggle === TOGGLE_SPEED}>빠르게</Toggle>
            <Toggle 
              onClick={() => { setChatToggle(TOGGLE_PERFORMANCE) }}
              ref={hoverPopupGPT4.hoverRef} 
              onMouseEnter={hoverPopupGPT4.onMouseEnter} 
              onMouseLeave={hoverPopupGPT4.onMouseLeave} 
              selected={chatToggle === TOGGLE_PERFORMANCE}>똑똑하게</Toggle>
            <Toggle 
              onClick={() => { setChatToggle(TOGGLE_BARD) }}
              ref={hoverPopupBard.hoverRef} 
              onMouseEnter={hoverPopupBard.onMouseEnter} 
              onMouseLeave={hoverPopupBard.onMouseLeave} 
              selected={chatToggle === TOGGLE_BARD}>Bard <BETA selected={chatToggle === TOGGLE_BARD}>BETA</BETA></Toggle>
          </ChatToggle> */}
          </>
        )}
        {router.pathname === "/" && (
          <MenuButton
            onClick={() => setOpenMobileChatLNB(true)}
            style={{ width: "auto" }}
          >
            <ChatIcon />
            <ChatIconText>채팅방 목록</ChatIconText>
          </MenuButton>
        )}
        {router.pathname.includes("/tool") && (
          <MenuButton onClick={() => setOpenMobileToolLNB(true)}>
            <Icon icon="ellipsis-h" size={22} />
          </MenuButton>
        )}
      </MobileWrapper>
      {openMobileLNB && (
        <ModalPositionPortal position={{ top: 0, left: 0 }}>
          <MobileLNBWrapper>
            <MobileLNB>
              <MobileLogoWrapper>
                <LogoWrtnPurple />
              </MobileLogoWrapper>
              <Link href="/">
                <MobileItem>홈</MobileItem>
              </Link>
              <MobileItem onClick={goToTool}>툴</MobileItem>
              <Link href="/editor">
                <MobileItem>에디터</MobileItem>
              </Link>
              <Link href="/plugins">
                <MobileItem>플러그인</MobileItem>
              </Link>
              <MobileItemWithIcon
                onClick={() => {
                  // setOpenMobileUserGuide(!openMobileUserGuide);
                  setOpenMobileStore((c) => !c);
                }}
                open={openMobileStore}
              >
                스토어 <IconChevron />
              </MobileItemWithIcon>
              {openMobileStore && (
                <>
                  {storeNavList.map((item, idx) => {
                    return (
                      <Link key={idx} href={item.href}>
                        <MobileInnerItem>{item.key}</MobileInnerItem>
                      </Link>
                    );
                  })}
                </>
              )}
              <Link href="/share">
                <MobileItem>공유 트렌드</MobileItem>
              </Link>
              <MobileDivider />
              <MobileItemWithIcon
                onClick={() => {
                  collectEvent("click_guide");
                  setOpenMobileUserGuide(!openMobileUserGuide);
                }}
                open={openMobileUserGuide}
              >
                뤼튼 가이드 <IconChevron />
              </MobileItemWithIcon>
              {openMobileUserGuide && (
                <>
                  {guideNavList.map((item, idx) => {
                    if (item.key === "고객 문의 / 상담") {
                      if (typeof window !== 'undefined' && navigator.language.includes("zh")) return <></>;

                      return (
                        <MobileInnerItem
                          key={idx}
                          onClick={() => {
                            collectEvent("click_channel_talk");
                            showMessenger();
                          }}
                        >
                          {item.key}
                        </MobileInnerItem>
                      );
                    }
                    return (
                      <Link
                        key={idx}
                        href={item.href}
                        onClick={() => {
                          if (item.key === "공식 커뮤니티")
                            collectEvent("click_community");
                          if (item.key === "사용자 카톡방")
                            collectEvent("click_open_kakao");
                        }}
                      >
                        <MobileInnerItem>{item.key}</MobileInnerItem>
                      </Link>
                    );
                  })}
                </>
              )}
              <MobileItemWithIcon
                onClick={() => {
                  setOpenMobileDownLoadGuide(!openMobileDownLoadGuide);
                }}
                open={openMobileDownLoadGuide}
              >
                다운로드 <IconChevron />
              </MobileItemWithIcon>
              {openMobileDownLoadGuide && (
                <>
                  {downloadNavList.map((item, idx) => {
                    return (
                      <Link key={idx} href={item.href}>
                        <MobileInnerItem>{item.key}</MobileInnerItem>
                      </Link>
                    );
                  })}
                </>
              )}
              {user ? (
                <>
                  <MobileDivider />
                  <MobileItemWithProfile
                    onClick={() => setOpenMobileUser(!openMobileUser)}
                    open={openMobileUser}
                  >
                    <Avatar width={"24px"} size={"14px"}>
                      {name && name.length > 0 ? name[0] : "W"}
                    </Avatar>
                    {name} <IconChevron />
                  </MobileItemWithProfile>
                  {openMobileUser && (
                    <>
                      {settingNavList.map((item, idx) => {
                        return (
                          <Link key={idx} href={item.href}>
                            <MobileInnerItem>{item.key}</MobileInnerItem>
                          </Link>
                        );
                      })}
                    </>
                  )}
                </>
              ) : (
                <Link href="/login">
                  <MobileItem>로그인</MobileItem>
                </Link>
              )}
            </MobileLNB>
            <MobileSpace onClick={() => setOpenMobileLNB(false)} />
          </MobileLNBWrapper>
        </ModalPositionPortal>
      )}
      {openMobileChatLNB && (
        <ModalPositionPortal position={{ top: 0, right: 0 }}>
          <ChatLNBWrapper>
            <ChatMobileLNB onClose={() => setOpenMobileChatLNB(false)} />
            <MobileSpace onClick={() => setOpenMobileChatLNB(false)} />
          </ChatLNBWrapper>
        </ModalPositionPortal>
      )}
      {openMobileToolLNB && (
        <ModalPositionPortal position={{ top: 0, right: 0 }}>
          <ChatLNBWrapper>
            <ToolMobileLNB onClose={() => setOpenMobileToolLNB(false)} />
            <MobileSpace onClick={() => setOpenMobileToolLNB(false)} />
          </ChatLNBWrapper>
        </ModalPositionPortal>
      )}
    </Switch>
  );
};

export default NavBar;

const Switch = styled.div<{ isSticky: boolean }>`
  ${({ isSticky }) =>
    isSticky
      ? "position: sticky; top: 0; background-color: white; z-index: 10;"
      : ""}
`;

const MobileLogoWrapper = styled(FlexWrapper)`
  height: 103px;
  width: 100%;
  justify-content: center;
  svg {
    width: 80px;
    height: 76px;
  }
`;

const Wrapper = styled(FlexWrapper)<{ isDark?: boolean }>`
  width: 100%;
  height: 74px;
  background: ${({ isDark }) => (isDark ? "none" : colors.WHITE)};
  justify-content: space-between;
  padding: 0px 30px 0px 30px;
  margin: 0px auto;
  border-bottom: 1px solid
    ${({ isDark }) => (isDark ? "none" : colors.WHITE_BOLDER_LINE)};
  display: flex;
  @media (max-width: 1279px) {
    display: none;
  }
`;

const MobileSpace = styled.div`
  width: 100%;
  height: 100%;
`;

const MobileDivider = styled.div`
  width: 100%;
  height: 1px;
  background: ${colors.BLUE_GRAY_LINE};
  margin: 14px 0px;
`;

const MobileItem = styled.div`
  padding: 10px 26px;
  ${typo({
    weight: 700,
    size: "16px",
    color: colors.GRAY_80,
    height: "100%",
  })}
  cursor: pointer;
  &:hover {
    background: ${colors.LIGHT_BLUE};
    color: ${colors.GRAY_90};
  }
`;

const MobileInnerItem = styled(MobileItem)`
  padding: 16px 26px 16px 56px;
`;

const MobileItemWithIcon = styled(MobileItem)<{ open: boolean }>`
  padding: 6.5px 26px 5.5px;
  display: flex;
  gap: 5px;
  align-items: center;
  svg {
    transform: ${({ open }) => (open ? `rotate(180deg)` : `rotate(90deg)`)};
    width: 24px;
    height: 24px;
  }
`;

const MobileItemWithProfile = styled(MobileItemWithIcon)`
  padding: 6px 26px;
  display: flex;
  gap: 10px;
  align-items: center;
  svg {
    transform: rotate(90deg);
    width: 24px;
    height: 24px;
  }
`;

const MobileLNBWrapper = styled.div`
  background: rgba(59, 63, 78, 0.3);
  backdrop-filter: blur(1px);
  display: flex;
  width: 100vw;
  height: 100vh;
  padding-bottom: 40px;
`;

const MobileLNB = styled.div`
  width: 232px;
  height: 100vh;
  flex: 0 0 232px;
  background: white;
  overflow: scroll;
  padding-bottom: 80px;
`;

const ChatLNBWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background: rgba(59, 63, 78, 0.3);
  backdrop-filter: blur(1px);
  display: flex;
  z-index: 99999;
  background: white;
`;

const MobileWrapper = styled(Wrapper)<{ isDark?: boolean }>`
  height: 56px;
  display: none;
  padding: 12px 13px;
  justify-content: space-between;
  @media (max-width: 1279px) {
    display: flex;
  }
`;

const ChatToggle = styled(FlexWrapper)`
  position: absolute;
  // height: 28px;
  top: 40px;
  left: 0px;
  width: 100%;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 5px;
  cursor: pointer;
  flex-direction: column;
  z-index: 10;
  background-color: ${newColors.WHITE};
  padding: 8px 0px;
  border-radius: 8px;
  border: 1px solid ${newColors.GRAY_200};
  overflow: hidden;
`;

const BETA = styled.span<{ selected: boolean }>`
  ${(props) =>
    typo({
      weight: 700,
      size: "8px",
      height: "160%",
      color: props.selected ? colors.ACTION_BLUE : colors.GRAY_58,
    })}
`;

const Toggle = styled.div<{ selected: boolean }>`
  display: inline-block;
  text-align: center;
  width: 100%;

  height: 100%;
  border-radius: 5px;
  ${typo({
    weight: 600,
    size: "14px",
    height: "28px",
    color: colors.GRAY_58,
  })};

  ${({ selected }) =>
    selected &&
    css`
      outline: 1px solid ${colors.ACTION_BLUE};
      color: ${colors.ACTION_BLUE};
      background: ${colors.LIGHT_BLUE};
    `}
`;

const MenuButton = styled(FlexButton)`
  width: 34px;
  height: 32px;
  border-radius: 5px;

  opacity: ${({ disabled }) => (disabled ? 0 : 1)};

  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
  &:hover {
    background: ${({ disabled }) =>
      disabled ? "transparent" : colors.GRAY_20};
  }
`;

const GuideNavWrapper = styled(FlexWrapper)`
  gap: 40px;
  @media (max-width: 1023px) {
    gap: 20px;
  }
`;

const NavWrapper = styled(FlexWrapper)`
  gap: 40px;
  @media (max-width: 1023px) {
    gap: 20px;
  }
`;

const MainNavWrapper = styled(FlexWrapper)`
  gap: 36px;
  @media (max-width: 1023px) {
    gap: 24px;
  }
`;

const NavItemStyle = (active?: boolean, isDark?: boolean) => css`
  gap: 8px;

  display: flex;
  align-items: center;

  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: active ? colors.POINT_PURPLE : colors.gray_80,
  })};

  cursor: pointer;
  padding: 6px 8px;
  &:hover {
    background: ${colors.gray_20};
    border-radius: 5px;
  }

  ${isDark &&
  css`
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: colors.WHITE,
    })};

    &:hover {
      background: none;
      border-radius: 5px;
    }
  `}
`;

const NavLink = styled(Link)<{ isActive?: boolean; isDark?: boolean }>`
  ${({ isActive, isDark }) => NavItemStyle(isActive, isDark)}
`;

const NavButton = styled.div<{ isActive?: boolean; isDark?: boolean }>`
  ${({ isActive, isDark }) => NavItemStyle(isActive, isDark)}
`;

const NavOuterLink = styled.a<{ isActive?: boolean }>`
  ${({ isActive }) => NavItemStyle(isActive)}
`;

const LoginWrapper = styled(FlexWrapper)`
  gap: 2px;
`;

const LogoWrapper = styled.div<{ isDark?: boolean }>`
  cursor: pointer;
  margin-top: 4px;
  ${({ isDark }) =>
    isDark &&
    css`
      svg {
        path {
          fill: ${colors.WHITE};
        }
      }
    `}
`;

const LogoMobileWrapper = styled.div<{ isDark?: boolean }>`
  cursor: pointer;
  margin-top: 4px;
  ${({ isDark }) =>
    isDark &&
    css`
      svg {
        path {
          fill: ${colors.WHITE};
        }
      }
    `}
  @media (max-width: 767px) {
    display: none;
  }
`;

const HoverWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  justify-content: flex-start;
  background-color: ${colors.WHITE};
  padding: 8px 0px;
  border-radius: 8px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  overflow: hidden;
`;

const GuideStyle = css`
  text-align: left;
  width: 100%;
  min-width: 105px;
  padding: 12px 16px;
  border-radius: 0px;
  &:hover {
    border-radius: 0px;
  }
`;

const GuideItem = styled(NavLink)`
  ${GuideStyle}
`;

const GuideButton = styled(NavButton)`
  ${GuideStyle}
`;

const GuideOuterItem = styled(NavOuterLink)`
  ${GuideStyle}
`;

const WrtnLogo = styled(WrtnLogoGray)`
  & {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const Divider = styled.div`
  width: 100%;
  height: 1px;
  margin: 8px 0px;
  background-color: ${colors.BLUE_GRAY_LINE};
`;

const ArrowButton = styled.div<{ isActive: boolean }>`
  transform: ${({ isActive }) =>
    isActive ? "rotate(180deg)" : "rotate(0deg)"};
  transition: transform 0.2s ease-in-out;
`;

const GuideOuterItemWrapper = styled.div`
  width: 100%;
`;

const NewLabel = styled.div`
  background: #ffcf24;
  padding: 4px 10px;
  border-radius: 5px;
  color: ${newColors.WHITE};
  ${newTypo("content-14-semi")};
`;

const Popup = styled.div`
  background-color: ${colors.gray_80};
  padding: 4px 9px;
  ${typo({
    weight: 600,
    size: "12px",
    color: colors.white,
  })};
  /* transform: translateX(-50%); */
  border-radius: 5px;
`;

const TrandLogo = styled.span`
  background-color: ${newColors.PURPLE_500_PRIMARY};
  padding: 4px 12px 4px 12px;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  border-bottom-right-radius: 20px;

  ${typo({
    weight: 800,
    size: "17px",
    color: colors.WHITE,
  })}
`;

const LogoArea = styled.div``;

const ShareLogo = styled.span`
  margin-right: 5px;
  ${typo({
    weight: 800,
    size: "17px",
    color: colors.GRAY_80,
  })}
`;

const MobileChatToggle = styled(MobileItemWithIcon)`
  position: relative;
  justify-content: space-between;
  min-width: 103px;
  height: 34px;
  padding: 10px 10px 8px 12px;
  border: 1px solid ${newColors.BLUE_500_NOTICE};
  background: ${newColors.BLUE_400_BG};
  border-radius: 8px;

  color: ${newColors.BLUE_500_NOTICE};
  &:hover {
    color: ${newColors.BLUE_500_NOTICE};
  }
  svg {
    width: 16px;
    height: 16px;
    path {
      fill: ${newColors.BLUE_500_NOTICE};
    }
  }
  display: none;
  @media (max-width: 767px) {
    display: flex;
  }
`;

const MobileToggle = styled.div<{ selected: boolean }>`
  display: inline-block;
  text-align: center;
  width: 100%;

  height: 100%;
  ${typo({
    weight: 600,
    size: "14px",
    height: "28px",
    color: colors.GRAY_58,
  })};

  ${({ selected }) =>
    selected &&
    css`
      color: ${colors.ACTION_BLUE};
      background: ${colors.LIGHT_BLUE};
    `}
`;

const ChatIcon = styled(IconChatCircle)`
  display: none;
`;

const ChatIconText = styled.div`
  ${newTypo("content-14-semi")};
  width: 80px;
  display: initial;
`;
