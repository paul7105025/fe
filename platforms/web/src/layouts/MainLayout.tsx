import { PropsWithChildren } from "react";

import NavBar from "./NavBar";

import { useIsMobile } from "@wrtn/core";
import styled from "styled-components";

export const MainLayout = ({ children }: PropsWithChildren) => {
  const isMobile = useIsMobile();

  return (
    <>
      <NavBar />
      <main
        style={{
          height: isMobile ? "calc(100% - 56px)" : "calc(100% - 76px)",
          overflow: isMobile ? "scroll" : "default",
        }}
      >
        {children}
      </main>
    </>
  );
};

export const MainDarkLayout = ({ children }: PropsWithChildren) => {
  const isMobile = useIsMobile();

  return (
    <DarkBackground>
      <Wrapper>
        <NavBar darkmode={true} />
      </Wrapper>
      <DarkMain
        style={{ height: isMobile ? "calc(100% - 56px)" : "calc(100% - 76px)" }}
      >
        {children}
      </DarkMain>
    </DarkBackground>
  );
};

export default MainLayout;

const DarkBackground = styled.div`
  background: radial-gradient(
      113.8% 113.8% at 50% 50%,
      rgba(0, 0, 0, 0) 0%,
      rgba(128, 0, 255, 0.2) 100%
    ),
    #070314;
`;

const Wrapper = styled.div`
  background: #0d0623;
  position: sticky;
  top: 0;
  z-index: 9;
`;

const DarkMain = styled.main`
  background: radial-gradient(
      113.8% 113.8% at 50% 50%,
      rgba(0, 0, 0, 0) 0%,
      rgba(128, 0, 255, 0.2) 100%
    ),
    #070314;
`;
