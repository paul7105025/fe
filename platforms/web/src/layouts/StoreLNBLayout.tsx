import { colors, FlexWrapper, newColors } from "@wrtn/ui";
import { PropsWithChildren } from "react";
import { GuideButtonLNB } from "src/containers/Button";
import { StoreLNB } from "src/containers/Store";
import useLNBMenu from "src/hooks/layout/useLNBMenu";
import styled from "styled-components";

export const StoreLNBLayout = ({ children }: PropsWithChildren) => {
  const { isOpen, isStore } = useLNBMenu();
  return (
    <Wrapper>
      <LNBWrapper isOpen={isOpen}>
        <Visible visible={isStore}>
          <StoreLNB />
        </Visible>
      </LNBWrapper>
      <ContentWrapper isOpen={isOpen}>{children}</ContentWrapper>
    </Wrapper>
  )
}

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
`;

const LNBWrapper = styled.div<{ isOpen: boolean }>`
  max-width: 235px;
  width: 235px;
  left: ${({ isOpen }) => (isOpen ? "0px" : "-235px")};
  position: absolute;
  height: 100%;
  transition: left 0.5s;
  border-right: 1px solid ${colors.BLUE_GRAY_LINE};
  background: ${colors.BACKGROUND};
`;

const Visible = styled.div<{ visible: boolean }>`
  width: 100%;
  height: calc(100% - 34px);
  display: ${(props) => (props.visible ? "inherit" : "none")};
`;


const ContentWrapper = styled.div<{ isOpen: boolean }>`
  width: 100%;
  height: 100%;
  padding-left: ${({ isOpen }) => (isOpen ? "235px" : "0px")};
  transition: padding-left 0.5s;
`;

const Divider = styled.div`
  width: 100%;
  height: 1px;
  background: ${newColors.GRAY_200};
  margin-top: 12px;
`;