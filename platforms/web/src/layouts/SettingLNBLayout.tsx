import { colors, FlexWrapper } from "@wrtn/ui";
import { MFEWrapper, MFELoadingPage } from "@wrtn/ui/containers/MFE";
import dynamic from "next/dynamic";
import { PropsWithChildren } from "react";
import useLNBMenu from "src/hooks/layout/useLNBMenu";
import styled from "styled-components";
import SettingLNB from "./SettingLNB";

export const SettingLNBLayout = ({ children }: PropsWithChildren) => {
  const { isOpen } = useLNBMenu()

  return (
    <Wrapper>
      <LNBWrapper isOpen={isOpen}>
        <MFEWrapper>
          <SettingLNB />
        </MFEWrapper>
      </LNBWrapper>
      <ContentWrapper justify="center" align="flex-start" isOpen={isOpen}>
        {children}
      </ContentWrapper>
    </Wrapper>
  );
};

const LNBWrapper = styled.div<{ isOpen: boolean }>`
  max-width: 235px;
  width: 235px;
  left: 0px;
  position: absolute;
  height: 100%;
  border-right: 1.5px solid ${colors.WHITE_BOLDER_LINE};
  @media (max-width: 767px){
    display: none;
  }
`;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
`;

const ContentWrapper = styled(FlexWrapper)<{ isOpen: boolean }>`
  width: 100%;
  height: 100%;
  padding-left: 235px;

  position: initial;
  @media (max-width: 767px){
    padding-left: 0px;
  }
`;
