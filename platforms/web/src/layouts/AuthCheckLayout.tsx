import {
  isBlockIPState,
  isBlockWithdrawState,
  isDuplicateLoginState,
  useFetchTemplate,
  useLoginDialog,
  userState,
} from "@wrtn/core";
import { removeCookies } from "cookies-next";

import React, { PropsWithChildren } from "react";
import { useRecoilState, useRecoilValue } from "recoil";

import { cookieKey, paths } from "src/constants";
import { LoginDialog } from "src/containers";

export const AuthCheckLayout = ({ children }: PropsWithChildren) => {
  const { openDialog, handleClose } = useLoginDialog();
  const user = useRecoilValue(userState);
  const { fetchTemplate } = useFetchTemplate();

  const [isBlockWithdraw, setIsBlockWithdraw] =
    useRecoilState(isBlockWithdrawState);
  const [isAlreadyExist, setIsDuplicateLogin] = useRecoilState(
    isDuplicateLoginState
  );
  const [isBlockIP, setIsBlockIP] = useRecoilState(isBlockIPState);

  React.useEffect(() => {
    if (user) {
      fetchTemplate();
    }
  }, [user]);

  React.useEffect(() => {
    const ua = window.navigator.userAgent;

    // localhost일때 스킵
    if (window.location.hostname.includes("localhost")) return ;
    // desktop 앱일 때 스킵
    if (ua.includes("wrtn-desktop/")) return ;
    removeCookies("refresh_token", { });
  }, [])

  return (
    <>
      {children}
      <>
        <LoginDialog.Login onClose={handleClose} isOpen={openDialog} />
        {isBlockWithdraw && (
          <LoginDialog.BlockWithdraw
            email={isBlockWithdraw?.email}
            provider={isBlockWithdraw?.provider}
            onClose={() => setIsBlockWithdraw(null)}
          />
        )}
        {isAlreadyExist && (
          <LoginDialog.BlockDuplicate
            onClose={() => setIsDuplicateLogin(null)}
            email={isAlreadyExist?.email}
            provider={isAlreadyExist?.provider}
            from={isAlreadyExist?.from || "local"}
          />
        )}
        {isBlockIP && (
          <LoginDialog.BlockIP
            onClose={() => setIsBlockIP(null)}
            provider={isBlockIP.provider}
          />
        )}
        {/* { isBan && (
          <LoginDialog.Ban
            onClose={() => setIsBan(false)}
          />
        )} */}
      </>
    </>
  );
};
