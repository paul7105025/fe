import React from "react";
import { useIs480, useModalOpen } from "@wrtn/core";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import { ErrorModal } from "@wrtn/ui/components/ErrorModal";
import { useRouter } from "next/router";

const SERVER_ERROR = false;
//기억안날 때 전체 찾기용 단어들 : error, 에러, 서버, 클로바, 좆, 시발, tlqkf, 아, 이거, 뭐더라, 그, 버그, 멈춰, 에러모달, 하이퍼클로바, 네이버, 혐이버, 에휴, 망했네, bug, server, 생성, 안됨, 안, 됌, 됨, 안됌, 망,

export const ErrorModalLayout = ({ children }: React.PropsWithChildren) => {
  const serverErrorModal = useModalOpen({ modalId: "serverErrorModal" });
  const isMobile = useIs480();
  const router = useRouter();

  React.useEffect(() => {
    if (SERVER_ERROR)
      serverErrorModal.open();
  }, [router.pathname]);

  return (
    <>
      {children}
      {serverErrorModal.isOpen && (
        <ModalPositionPortal
          position={{
            top: 100,
            left: isMobile ? "5vw" : "auto",
            right: isMobile ? "5vw" : 40,
          }}
        >
          <ErrorModal
            onClose={() => serverErrorModal.close()}
            title="현재 생성 실패 오류가 발생하고 있습니다."
            // title="현재 이미지 생성 실패 오류가 발생하고 있습니다."
            // title="현재 파일과 검색 모드는 사용 불가능합니다."
            subTitle={[
              "문제 파악 후 조치를 취하는 중입니다. 빠르게 해결될 수 있도록 최선을 다하겠습니다.",
              // "네이버 클라우드 플랫폼의 서버 점검 영향으로",
              // "5/25 (목) 18시 ~ 24시 사이에 생성이 원활하지 않을 수 있습니다.",
            ]}
          />
        </ModalPositionPortal>
      )}
    </>
  );
};
