import dayjs from "dayjs";
import styled from "styled-components";
import { useRecoilValue, useRecoilState } from "recoil";
import React, { PropsWithChildren } from "react";

import { useDialog } from "src/hooks/layout/useDialog";

import { userState, isDialogOpenState } from "@wrtn/core";

import { outerDialogData } from "src/constants/eventDialog";
import NewWrtnUpdateSorryEventDialog from "src/components/Events/NewWrtnUpdateSorryEventDialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { colors, FlexWrapper } from "@wrtn/ui";
import SatisficationSurveyEventDialog from "src/components/Events/SatisficationSurveyEventDialog";
import KakaotalkGPT4UnlimitedEventDialog from "src/components/Events/KakaotalkGPT4UnlimitedEventDialog";
import KakaotalkPluginAnnouncementEventDialog from "src/components/Events/KakaotalkPluginAnnouncementEventDialog";
import ImageChatEventDialog from "src/components/Events/ImageChatEventDialog";
import KakaoStyleCouponEventDialog from "src/components/Events/KakaoStyleCouponEventDialog";
import ToolRemoveAnnouncementDialog from "src/components/Events/ToolRemoveAnnouncementDialog";
import ToolStoreOpenEventDialog from "src/components/Events/ToolStoreOpenDialog";
import ShareTrendOpenEventDialog from "src/components/Events/ShareTrendOpenDialog";
import Prompthon1stChampionshipEventDialog from "src/components/Events/Prompthon1stChampionshipEventDialog";
import StudioOpenEventDialog from "src/components/Events/StudioOpenDialog";

export const EventLayout = ({ children }: PropsWithChildren) => {
  const { openDialog, closeDialogTemp, closeDialog } =
    useDialog(outerDialogData);

  const user = useRecoilValue(userState);

  const [openErrorBanner, setOpenErrorBanner] = React.useState(false);

  const isOpen = React.useMemo(() => {
    let isOpen = false;
    openDialog.forEach((dialog) => {
      if (dialog.isOpen) isOpen = true;
    });
    return isOpen;
  }, [openDialog]);

  const [isDialogOpen, setIsDialogOpen] = useRecoilState(isDialogOpenState);

  React.useEffect(() => {
    setIsDialogOpen(isOpen);
  }, [isOpen])

  React.useEffect(() => {
    if (user) {
      if (user?.dueDate && dayjs(Date.now()).isAfter(user?.dueDate)) {
        setOpenErrorBanner(true);
      }
    }
  }, [user]);

  const dialog = openDialog.find((dialog) => dialog.isOpen);

  return (
    <>
      {children}
      {isOpen && (
        <ModalPortal zIndex={100}>
          <DialogWrapper justify="center">
            { dialog &&
              (dialog.type === "studio_open" && dialog.isOpen)
                && (
                  <SingleDialogWrapper key={dialog.name}>
                    <StudioOpenEventDialog
                      onClose={() => closeDialogTemp(dialog)}
                      onCloseDay={() => closeDialog(dialog)}
                    />
                  </SingleDialogWrapper>
                )
            }
            { dialog &&
              (dialog.type === "kakao-style-coupon" && dialog.isOpen)
                && (
                  <SingleDialogWrapper key={dialog.name}>
                    <KakaoStyleCouponEventDialog
                      onClose={() => closeDialogTemp(dialog)}
                      onCloseDay={() => closeDialog(dialog)}
                    />
                  </SingleDialogWrapper>
                )
            }
            {
              dialog &&
              ( dialog.type === "share_trend_open_announcement" && dialog.isOpen )                
              && <SingleDialogWrapper key={dialog.name}>
                <ShareTrendOpenEventDialog
                  onClose={() => closeDialogTemp(dialog)}
                  onCloseDay={() => closeDialog(dialog)}
                  dialog={dialog}
                />
              </SingleDialogWrapper>
            }
              { dialog && 
                (dialog.type === "prompthon_1st_championship" && dialog.isOpen)
                && (
                  <SingleDialogWrapper key={dialog.name}>
                    <Prompthon1stChampionshipEventDialog
                      onClose={() => closeDialogTemp(dialog)}
                      onCloseDay={() => closeDialog(dialog)}
                      dialog={dialog}
                    />
                  </SingleDialogWrapper>
                )
              }
          </DialogWrapper>
        </ModalPortal>
      )}
    </>
  );
};

const DialogWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  z-index: 100;

  position: relative;
`;

const SingleDialogWrapper = styled(FlexWrapper)`
  position: absolute;
`
