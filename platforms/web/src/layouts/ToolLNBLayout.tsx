import dynamic from "next/dynamic";
import styled from "styled-components";
import React, { PropsWithChildren } from "react";

import { colors, FlexWrapper, newColors } from "@wrtn/ui";
import { MFELoadingPage, MFEWrapper } from "@wrtn/ui/containers/MFE";
import { useRouter } from "next/router";
import useLNBMenu from "src/hooks/layout/useLNBMenu";
import { GuideButtonLNB } from "src/containers/Button";

interface LeftToolNavigationContainerProps {
  isMobile?: boolean;
  // isSideNavigationOpened: boolean;
  toggleWrapper?: () => void;
  toggleSideNavigation?: () => void;
  currentToolId?: string | null;
  navigate?: (id: string) => void;
}

const ToolLNB = dynamic<LeftToolNavigationContainerProps>(() => import("$tool/ToolLNB"), {
  ssr: false,
  loading: () => <MFELoadingPage />,
});

export const ToolLNBLayout = ({ children }: PropsWithChildren) => {
  const { isOpen } = useLNBMenu();
  const router = useRouter();
  const { toolId } = router.query;
  const _toolId = React.useMemo(() => {
    if (!toolId) return null;
    return typeof toolId === "string" ? toolId : toolId[0];
  }, [toolId]);

  return (
    <Wrapper>
      <LNBWrapper isOpen={isOpen}>
        <Visible>
          <ToolLNB 
            currentToolId={_toolId}
            navigate={(url: string) => router.push(url)}
          />
          <Divider />
          <GuideButtonLNB />
        </Visible>
      </LNBWrapper>
      <ContentWrapper isOpen={isOpen}>{children}</ContentWrapper>
    </Wrapper>
  );
};

const LNBWrapper = styled.div<{ isOpen: boolean }>`
  max-width: 235px;
  width: 235px;
  left: ${({ isOpen }) => (isOpen ? "0px" : "-235px")};
  position: absolute;
  height: 100%;
  transition: left 0.5s;
  background: ${colors.BACKGROUND};
  border-right: 1px solid ${newColors.GRAY_100};
`;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
`;

const ContentWrapper = styled.div<{ isOpen: boolean }>`
  width: 100%;
  height: 100%;
  padding-left: ${({ isOpen }) => (isOpen ? "235px" : "0px")};
  transition: padding-left 0.5s;
`;


const Divider = styled.div`
  width: 100%;
  height: 1px;
  background: ${newColors.GRAY_200};
  margin-top: 12px;
`;

const Visible = styled.div`
  width: 100%;
  height: calc(100% - 69px);
  display: inherit;
`;