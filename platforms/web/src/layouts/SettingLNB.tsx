import { colors, FlexWrapper, typo } from "@wrtn/ui";
import Link from "next/link";
import { useRouter } from "next/router";
import { paths } from "src/constants";
import styled from 'styled-components'

export const SettingLNB = () => {

  const router = useRouter();

  return (
    <Wrapper column align="flex-start">
      <Title>설정</Title>
      <SettingLink isActive={router.pathname === paths.setting.account()} href={paths.setting.account()}>계정 정보</SettingLink>
      <SettingLink isActive={router.pathname === paths.setting.plan()} href={paths.setting.plan()}>요금제</SettingLink>
      <SettingLink isActive={router.pathname === paths.setting.invoice()} href={paths.setting.invoice()}>결제 정보</SettingLink>
      <SettingLink isActive={router.pathname === paths.setting.coupon()} href={paths.setting.coupon()}>쿠폰</SettingLink>
      <SettingLink isActive={router.pathname === paths.setting.history()} href={paths.setting.history()}>툴 히스토리</SettingLink>
    </Wrapper>
  )
}

export default SettingLNB;

const Title = styled.div`
  ${typo({
    weight: 600,
    size: '24px',
    height: '100%',
    color: colors.gray_90
  })};
  padding-bottom: 20px;
`;

const SettingLink = styled(Link)<{ isActive: boolean }>`
  ${props => !props.isActive ? `
    ${typo({
      weight: 500,
      size: '18px',
      height: '100%',
      color: colors.gray_70
    })}
  ` : `
    ${typo({
      weight: 700,
      size: '18px',
      height: '100%',
      color: colors.POINT_PURPLE
    })}
  
  `}
`;

const Wrapper = styled(FlexWrapper)`
    padding-left: 89px;
    padding-top: 76px;
    gap: 28px;
`