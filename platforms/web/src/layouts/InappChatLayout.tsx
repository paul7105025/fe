import { boxShadow, colors, FlexWrapper, typo } from "@wrtn/ui";
import { MFELoadingPage, MFEWrapper } from "@wrtn/ui/containers/MFE";
import dynamic from "next/dynamic";
import React, { PropsWithChildren } from "react";
import styled from "styled-components";
import { IconCarouselDown, IconRoundWrtn } from "@wrtn/ui/assets";
import {
  chatListState,
  hoverChatGuideState,
  inappChatDefaultValueState,
  inappChatOpenState,
  useClickOutside,
  useEvent,
  userState,
} from "@wrtn/core";
import { useRecoilState, useRecoilValue } from "recoil";

export declare type ChatControllerProps = {
  toggleSideNavigation: () => void;
  defaultValue?: null | string;
};

interface ChatNavbarProps {
  isMobile?: boolean;
}

const InappChat = dynamic<ChatControllerProps>(
  () => import("$chat/InappChatPage").catch((err) => err),
  {
    ssr: false,
    loading: () => <MFELoadingPage />,
  }
);

const LeftChatNavigationContainer = dynamic<ChatNavbarProps>(
  () => import("$chat/LeftChatNavigationContainer").catch((err) => err),
  {
    ssr: false,
    loading: () => <MFELoadingPage />,
  }
);

export const InappChatLayout = ({ children }: PropsWithChildren) => {
  const { collectEvent } = useEvent();

  const [isOpenChat, setIsOpenChat] = useRecoilState(inappChatOpenState);
  const [chatDefaultValue, setChatDefaultValue] = useRecoilState(
    inappChatDefaultValueState
  );
  const chatList = useRecoilValue(chatListState);
  const user = useRecoilValue(userState);

  const [openNav, setOpenNav] = React.useState(false);
  const [chatId, setChatId] = React.useState<null | string>(null);
  const isHoverGuide = useRecoilValue(hoverChatGuideState);
  const chatRef = React.useRef<HTMLDivElement | null>(null);
  const butRef = React.useRef<HTMLDivElement | null>(null);

  const handleOpenChat = () => {
    collectEvent("view_chat_page", {
      position: "tool_mini",
    });
    setIsOpenChat(true);
  };

  const handleCloseChat = () => {
    setIsOpenChat(false);
    setChatId(null);
    setChatDefaultValue(null);
    setOpenNav(false);
  };
  useClickOutside([chatRef, butRef], "mousedown", () => {
    if (butRef.current && chatRef.current && isOpenChat && !isHoverGuide) {
      handleCloseChat();
    }
  });

  const recentChat = chatList?.length > 0 ? chatList[0]._id : null;

  if (!user) return <>{children}</>;

  return (
    <>
      {children}
      {/* <ChatWrtnWrapper isOpen={isOpenChat} ref={chatRef} row>
        <MobileRowWrapper row isOpen={openNav}>
          <LeftChatNavigationContainer isMobile={true} />
        </MobileRowWrapper>
        <InappChat
          // chatId={chatId || recentChat}
          // handleChangeChatId={(id: string) => {
          //   setChatId(id)
          //   setChatDefaultValue(null);
          // }}
          toggleSideNavigation={() => setOpenNav((c) => !c)}
          defaultValue={chatDefaultValue}
        />

        <Background
          isOpen={openNav}
          isVisible={false}
          onClick={() => {
            setOpenNav(false);
          }}
        />
      </ChatWrtnWrapper>
      <MobileKneifWrapper
        onClick={() => {
          handleOpenChat();
        }}
      >
        <IconRoundWrtn />
      </MobileKneifWrapper>
      <KneifWrapper>
        <ChatWrtnButton
          ref={butRef}
          onClick={() => {
            handleOpenChat();
          }}
        >
          챗 뤼튼 <CarouselUp />
        </ChatWrtnButton>
      </KneifWrapper> */}
    </>
  );
};

export default InappChatLayout;

const MobileRowWrapper = styled(FlexWrapper)<{ isOpen: boolean }>`
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 92;

  transform: ${({ isOpen }) =>
    isOpen ? "translateX(0)" : "translateX(-100%)"};
  transition: transform 0.29s ease-in-out;
  overflow: hidden;
  ${"" /* align-self: stretch; */}
`;

const IFrame = styled.iframe`
  width: 100%;
  height: 100%;
`;

const LNBWrapper = styled(FlexWrapper)`
  position: absolute;
  width: 100%;
  height: 100%;
  justify-content: flex-start;
  z-index: 92;
`;

const Background = styled.div<{ isOpen: boolean; isVisible: boolean }>`
  position: absolute;

  width: 100%;
  height: 100%;
  background: ${({ isOpen }) =>
    isOpen ? "rgba(59, 63, 78, 0.3)" : "transparent"};

  backdrop-filter: ${({ isOpen }) => (isOpen ? "blur(4px)" : "blur(0px)")};

  z-index: ${({ isVisible, isOpen }) => (isVisible ? 90 : isOpen ? 90 : -2)};

  transition: backdrop-filter 0.3s ease-in-out, background 0.16s ease-in-out;
`;

const KneifWrapper = styled(FlexWrapper)`
  position: absolute;
  justify-content: flex-end;
  bottom: 0px;
  height: 40px;
  width: 100%;
  padding: 0px 26px;
  @media (max-width: 610px) {
    visibility: hidden;
  }
`;

const MobileKneifWrapper = styled(FlexWrapper)`
  position: absolute;
  justify-content: flex-end;
  bottom: 0px;
  width: 100%;
  visibility: hidden;
  cursor: pointer;
  @media (max-width: 610px) {
    visibility: visible;
  }
`;

const ChatWrtnButton = styled(FlexWrapper)`
  position: relative;
  height: 100%;
  width: 131px;
  z-index: 3;
  padding-right: 5px;
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.white,
    height: "100%",
  })};
  background: ${colors.POINT_PURPLE};
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 5px 5px 0px 0px;
  cursor: pointer;
`;

const ChatWrtnWrapper = styled(FlexWrapper)<{ isOpen: boolean }>`
  visibility: ${(props) => (props.isOpen ? "visible" : "hidden")};
  position: absolute;
  overflow: hidden;
  bottom: 0px;
  right: 26px;
  z-index: 91;
  border-radius: 10px 10px 0px 0px;
  height: 699px;
  width: 579px;
  max-height: 95vh;
  background: ${colors.white};
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  ${boxShadow.dialog_shadow};

  @media (max-width: 610px) {
    width: 100vw;
    right: 0px;
    bottom: 0px;
  }
`;

const CarouselUp = styled(IconCarouselDown)`
  transform: rotate(180deg);
  position: absolute;
  right: 10px;
  & path {
    fill: ${colors.white};
  }
`;
