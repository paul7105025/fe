import { pluginWaitlisterState } from "@wrtn/core";
import { useRecoilState } from "recoil";

export const usePluginWaitlister = () => {
  const [pluginWaitlister, setPluginWaitlister] = useRecoilState(
    pluginWaitlisterState
  );

  return {
    pluginWaitlister,
    _id: pluginWaitlister?._id,
    rank: pluginWaitlister?.rank,
    waitlisterId: pluginWaitlister?.waitlisterId,
    inviterId: pluginWaitlister?.inviterId,
  };
};
