import React from "react";

import { useCategoryList, useFetchToolList, useTagManager } from "@wrtn/core";

export const useCategoryFilter = ({ isImageAccess }) => {
  const tagManager = useTagManager();

  const { allToolList } = useFetchToolList();
  const { categoryList: toolCategoryList } = useCategoryList(allToolList);

  const tabList = React.useMemo(
    () => (isImageAccess ? ["all", "liked", "image"] : ["all", "liked"]),
    [isImageAccess]
  );
  const [currentTab, setCurrentTab] = React.useState(tabList[0] || null);

  const categoryList = React.useMemo(
    () =>
      currentTab === "image"
        ? ["all", "normal", "pop-art", "water", "oil", "animation"]
        : ["all", ...toolCategoryList.map((item) => item.category)],

    [currentTab, toolCategoryList]
  );
  const [currentCategory, setCurrentCategory] = React.useState(
    categoryList[0] || null
  );

  React.useEffect(() => {
    if (currentTab === "image") {
      if (
        currentCategory !== "normal" &&
        currentCategory !== "pop-art" &&
        currentCategory !== "water" &&
        currentCategory !== "oil" &&
        currentCategory !== "animation"
      ) {
        setCurrentCategory("all");
      }
    } else if (currentTab === "liked" || currentTab === "all") {
      if (
        currentCategory === "pop-art" ||
        currentCategory === "water" ||
        currentCategory === "oil" ||
        currentCategory === "animation"
      ) {
        setCurrentCategory("all");
      }
    }
  }, [currentTab, currentCategory]);

  const _setCurrentTab = React.useCallback(
    (value) => {
      setCurrentTab(value);
      tagManager({
        event: "click_tab",
        data: {
          current_category: currentCategory,
          current_tab: value,
        },
      });
    },
    [currentCategory, tagManager]
  );

  const _setCurrentCategory = React.useCallback(
    (value) => {
      setCurrentCategory(value);
      tagManager({
        event: "click_category_tab",
        data: {
          category: value,
        },
      });
    },
    [tagManager]
  );

  return React.useMemo(() => {
    return {
      currentTab,
      currentCategory,
      setCurrentTab: _setCurrentTab,
      setCurrentCategory: _setCurrentCategory,
      categoryList,
      tabList,
    };
  }, [
    tabList,
    categoryList,
    currentTab,
    currentCategory,
    _setCurrentCategory,
    _setCurrentTab,
  ]);
};
