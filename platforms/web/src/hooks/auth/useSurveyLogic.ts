import React from "react";
import { useRecoilState } from "recoil";

//@ts-ignore
import { userState } from "@wrtn/core/stores/login";

import {
  userSurveyType,
  SurveyStep,
  STEP_NAME_RECORD,
  useEvent,
} from "@wrtn/core";
import {
  useTagManager,
  useTimeCounter,
  useDelayUpdate,
} from "@wrtn/core/hooks";
import {
  putUser,
  postUserRecommend,
  putUserAgreement,
} from "@wrtn/core/services";
import { useRouter } from "next/router";
import { paths } from "src/constants";

const goNextStep = (step: SurveyStep) => {
  switch (step) {
    case "STEP_TERMS":
      return "STEP_JOB_SURVEY";
    case "STEP_JOB_SURVEY":
      return "STEP_JOB_SURVEY";
    default:
      return "STEP_TERMS";
    // return "STEP_INFLOW_SURVEY";
    // case "STEP_INFLOW_SURVEY":
    //   return "STEP_PURPOSE_SURVEY";
    // case "STEP_PURPOSE_SURVEY":
    //   return "STEP_CONSENT";
    // case "STEP_CONSENT":
    //   return "STEP_CONSENT";
  }
};

export const useSurveyLogic = () => {
  const [user, setUser] = useRecoilState(userState);

  const [surveyData, setSurveyData] = React.useState<userSurveyType>({
    job: [],
    company: "",
    inflow: "",
    recommendCode: "",
    purpose: [],
    termMarketing: false,
  });

  const [surveyStep, setSurveyStep] = React.useState<SurveyStep>("STEP_TERMS");

  const [userText, setUserText] = React.useState("");
  const [isEnableUserText, setIsEnableUserText] = React.useState(false);

  const [isRecommendError, setIsRecommendError] = React.useState(false);
  const errorRecommend = React.useRef("ERROR");

  const tagManager = useTagManager();
  const timeCounter = useTimeCounter("survey");

  const router = useRouter();

  const { collectEvent } = useEvent();


  React.useEffect(() => {
    if (user) {
      const recommend = router.query["recommend"];

      setSurveyData({
        job: user.job || [],
        company: user.company || "",
        inflow: user.inflow || "",
        recommendCode: typeof recommend === "string" ? recommend : (recommend  ? recommend[0] : ""),
        //   recommend: user.recommend,
        purpose: user.purpose || [],
        termMarketing: false,
      });

      let step: SurveyStep = "STEP_TERMS";

      if ((user.privacyTerm && user.serviceTerm) || user.provider === "kakao" || user.provider === "naver") {
        if (user.job.length > 0 && user.company.length > 0) {
          router.push(paths.chat())
        } else step = "STEP_JOB_SURVEY";
      } else {
        step = "STEP_TERMS";
      }

      setSurveyStep(step);
    }
  }, []);

  React.useEffect(() => {
    if (surveyData.recommendCode === errorRecommend.current) {
      setIsRecommendError(true);
    } else {
      setIsRecommendError(false);
    }
  }, [surveyData.recommendCode]);

  const isEnableNext = React.useMemo(() => {
    if (surveyStep === "STEP_JOB_SURVEY") {
      return surveyData.job.length > 0;
    }
    return false;
  }, [surveyStep, surveyData, isRecommendError]);

  const onChangeSurvey = (
    key: keyof userSurveyType,
    value: string | boolean,
    b?: boolean
  ) => {
    if (key === "job" && typeof value === "string") {
      if (!b) {
        if (surveyData.job.includes(value)) {
          setSurveyData((prev) => ({
            ...prev,
            job: prev.job.filter((item) => item !== value),
          }));
        } else {
          if (surveyData.job.length >= 3) return;
          setSurveyData((prev) => ({
            ...prev,
            job: [...prev.job, value],
          }));
        }
      }
    }
    else setSurveyData((prev) => ({ ...prev, [key]: value }));
  };

  const update = useDelayUpdate(
    (e: React.ChangeEvent<HTMLInputElement>) => setUserText(e.target.value),
    (
      e: React.ChangeEvent<HTMLInputElement>,
      key: keyof userSurveyType,
      b?: boolean
    ) => onChangeSurvey(key, e.target.value, b),
    500
  );

  const onChangeText = (
    e: React.ChangeEvent<HTMLInputElement>,
    k: string,
    b?: boolean
  ) => update(e, k, b);

  const handleNext = async () => {
    if (user) {
      timeCounter.reset();

      const goNext = () => {
        setSurveyStep(goNextStep(surveyStep));
        setUserText("");
        setIsEnableUserText(false);
      };

      if (surveyStep === "STEP_TERMS") {
        const res = await putUser({
          data: {
            serviceTerm: true,
            privacyTerm: true,
            marketingTerm: surveyData["termMarketing"],
          },
        });
        if (res?.data) {
          const { data } = res.data;
          setUser(data);

          if (data?.job?.length > 0 && data?.company?.length > 0) {
            router.push(paths.chat())
          } else {
            // collectEvent("signup_done");
            goNext();
          }
        }
      }
      if (surveyStep === "STEP_JOB_SURVEY") {
        const jobs = surveyData.job.map((v) => (v === "기타" ? userText : v));

        const res = await putUser({
          data: {
            job: jobs,
            company: surveyData.company,
          },
        });
        if (res?.status === 200) {
          const { data } = res.data;
          setUser(data);
          tagManager({
            event: "signup_done"
          });
          collectEvent("signup_done");
          router.push(paths.chat())
        }
      }
    }
  };

  const handleClickSurvey = (v: string) => {
    if (surveyStep === "STEP_TERMS") {
      if (v === "true") onChangeSurvey("termMarketing", true);
      else onChangeSurvey("termMarketing", false);
    }
    if (surveyStep === "STEP_JOB_SURVEY") {
      if (v === "기타") {
        onChangeSurvey("job", v);
        setIsEnableUserText((prev) => !prev);
      } else {
        onChangeSurvey("job", v);
      }
    }
  };

  return {
    surveyStep,
    surveyData,
    isEnableUserText,
    userText,
    isEnableNext,
    isRecommendError,
    handleClickSurvey,
    onChangeSurvey,
    onChangeText,
    handleNext,
  };
};
