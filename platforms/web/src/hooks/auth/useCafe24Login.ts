
import { useSearchParams } from "next/navigation";
import React from 'react';

export const useCafe24Login = () => {

  const searchParams = useSearchParams();

  const mallId = searchParams.get("mall_id");
  const userId = searchParams.get('user_id');

  const isCafe24 = React.useMemo(() => {
    return mallId && userId
  }, [mallId, userId])

  return {
    // handleCafe24Login: handleLogin,
    isCafe24SearchParams: isCafe24
  }
}

export default useCafe24Login;