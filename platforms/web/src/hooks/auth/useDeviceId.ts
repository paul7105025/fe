import { nanoid } from "nanoid";
import { getCookie, setCookie } from 'cookies-next';
import { cookieKey } from "src/constants";

export const useDeviceId = () => {
  // const [cookies, setCookies] = useCookies([
  //   '_ga',
  //   '_deviceId',
  // ]);

  const getDeviceId = () => {
    const deviceId = getCookie(cookieKey.deviceId);
    const ga = getCookie(cookieKey.ga) as string;

    if (typeof deviceId === "string" && deviceId.length > 0) return deviceId;
  
    const expire = new Date();
    const newId = (!ga || (typeof ga === "string" && ga.length === 0)) ? nanoid() : ga;
    expire.setFullYear(new Date().getFullYear() + 2)
    
    setCookie('_deviceId', newId, 
    // {
    //   expires: expire,
    //   path: '/'
    // }
    );
    return newId
  }
  return getDeviceId;
}

export default useDeviceId;