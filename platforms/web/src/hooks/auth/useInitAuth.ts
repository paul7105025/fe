import React from "react";
import { useRouter } from "next/router";
import { useRecoilState } from "recoil";
import { useAlert } from 'react-alert';

import {
  accessTokenState,
  addInterceptors,
  getUser,
  syncHeader,
  toolListState,
  useLoginDialog,
  useModalOpen,
  useRefresh,
  userState,
} from "@wrtn/core";

import { paths } from "src/constants";

type renderType = "client" | "server" | "unique";
type useInitLoginOptions = {
  updateUser?: renderType;
  updateTool?: renderType;
};

type useInitLoginProps = {
  readonly accessKey?: string | null;
  readonly props?: any;
  readonly options?: useInitLoginOptions;
};

const useInitAuth = ({ accessKey, props, options }: useInitLoginProps) => {
  const { fetchTool, fetchUser } = useRefresh();
  const banAbuseModal = useModalOpen({ modalId: "banAbuseModal" });

  const [user, setUser] = useRecoilState(userState);
  const [toolList, setToolList] = useRecoilState(toolListState);
  const [accessToken, setAccessToken] = useRecoilState(accessTokenState);
  const router = useRouter();
  const alert = useAlert()
  const { handleOpen } = useLoginDialog();

  const checkSurvey = () => {
    if (accessToken) {
      if (!user) return ;
      if ((user.provider === "kakao" || user.provider === "naver") && (user?.job?.length > 0)) return ;
      if (user?.privacyTerm && user?.serviceTerm && user?.job?.length > 0) return ;
      if (router.pathname === paths.survey()) return ;
      router.push(paths.survey());
    }
  }

  React.useEffect(() => {
    console.log(accessKey)
    checkSurvey();
    syncHeader({ key: "Authorization", value: `Bearer ${accessKey}` });
    setAccessToken(accessKey || "");
    addInterceptors(setAccessToken, handleOpen, banAbuseModal.open, () => {
      alert.removeAll();
      alert.show("새벽 시간에는 서버 정비 작업으로 인해 간헐적으로 생성이 원활하지 않을 수 있습니다. 이용에 불편함을 드려 죄송합니다.")
    }, (response) => {
      alert.removeAll();
      if(response.data.message.includes("과도한")) {
        alert.show(response.data.message);
      }
    });

    const updateUser = async () => {
      const res = await getUser();
      if (res?.status === 200) {
        const { data } = res.data;
        setUser(data);
        if (!data) return;

        if ((data.provider === "kakao" || data.provider === "naver") && (data?.job?.length > 0)) return ;
        if (data?.privacyTerm && data?.serviceTerm && data?.job?.length > 0) return ;
        if (router.pathname === paths.survey()) return ;
        router.push(paths.survey());
      }
    };

    const updateTool = async () => {
      await fetchTool();
    };

    if (options?.updateUser) {
      options?.updateUser === "client"
        ? updateUser()
        : options?.updateUser === "server"
        ? setUser(props.user)
        : options?.updateUser === "unique"
        ? !user && updateUser()
        : null;
    }
    if (options?.updateTool) {
      options?.updateTool === "client"
        ? updateTool()
        : options?.updateTool === "server"
        ? setToolList(props.tool)
        : options?.updateTool === "unique"
        ? toolList?.length === 0 && updateTool()
        : null;
    }
  }, []);

  return null;
};

export default useInitAuth;
