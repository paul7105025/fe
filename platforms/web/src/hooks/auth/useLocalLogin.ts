import {
  isDuplicateLoginState,
  isBlockIPState,
  isBlockWithdrawState,
} from "@wrtn/core/stores/login";
import {
  IsDuplicateLoginState,
  LocalLoginState,
  LoginProvider,
} from "@wrtn/core/types/login";
import React from "react";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
  localLoginEmailState,
  localLoginPasswordState,
  localLoginProviderState,
  localLoginState,
} from "../../stores";
import { emailRegExp, passwordRegExp } from "@wrtn/core/utils";
import { useEvent } from "@wrtn/core/hooks";
import useDeviceId from "./useDeviceId";
import { postAuthLocalLogin, postAuthLocalRegister, getFunnel, getPlatform } from "@wrtn/core";
import useLogin from "./useLogin";
import useCafe24Login from "./useCafe24Login";
import { useRouter } from "next/router";
import { paths } from "src/constants";
import { useSearchParams } from "next/navigation";

export const useLocalLogin = () => {
  const router = useRouter();
  const [email, setEmail] = useRecoilState<string>(localLoginEmailState);
  const [password, setPassword] = useRecoilState<string>(
    localLoginPasswordState
  );
  const [provider, setProvider] = useRecoilState<LoginProvider>(
    localLoginProviderState
  );
  const [localLogin] = useRecoilState<LocalLoginState>(localLoginState);
  const setIsDuplicateLogin = useSetRecoilState<IsDuplicateLoginState>(
    isDuplicateLoginState
  );
  const { collectEvent } = useEvent();

  const setIsBlockIP = useSetRecoilState(isBlockIPState);
  const setIsBlockWithdraw = useSetRecoilState(isBlockWithdrawState);

  const getDeviceId = useDeviceId();

  const login = useLogin();

  const handleFindEmail = () => {
    handleInit();
    // TODO: search
    router.push(paths.login.find.email());
  };

  const handleLoginEmail = () => {
    setIsDuplicateLogin(null);
    collectEvent("click_start_platform_btn", { platform: "wrtn" });
    router.push(paths.login.local.index());
  };

  const handleFindPassword = () => {
    handleInit();
    router.push(paths.login.find.password());
  };

  const handleFindResetPassword = () => {
    router.push(paths.login.find.reset());
  };

  const handleLoginLogin = () => {
    router.push(paths.login.local.signin());
  };

  const handleLoginCert = () => {
    router.push(paths.login.local.cert());
  };

  const handleFindEmailResult = () => {
    // navigate('wrtn/')
  };

  const handleLoginPassword = () => {
    router.push(paths.login.local.signup());
  };

  const handleClose = () => {
    router.push(paths.chat());
    handleInit();
  };

  const handleInit = () => {
    setEmail("");
    setPassword("");
  };

  const handleChangeEmail = (val: string) => {
    setEmail(val);
  };

  const handleChangePassword = (val: string) => {
    setPassword(val);
  };

  const handleChangeProvider = (val: LoginProvider) => {
    setProvider(val);
  };

  const isValidEmailFormat = React.useMemo<boolean>(() => {
    return emailRegExp.test(email);
  }, [email]);

  const isValidPasswordFormat = React.useMemo<boolean>(() => {
    return passwordRegExp.test(password);
  }, [password]);

  const isValidLoginFormat = React.useMemo<boolean>(() => {
    return isValidEmailFormat && isValidPasswordFormat;
  }, [isValidEmailFormat, isValidPasswordFormat]);

  const handleRegister = async () => {
    const deviceId = getDeviceId();
    const funnel = getFunnel();
    const platform = getPlatform();
    const res = await postAuthLocalRegister(
      {
        email: localLogin.email,
        password: localLogin.password,
      },
      deviceId,
      funnel,
      platform,
    );

    if (res.status === 201) {
      if (typeof res.data.data === "object" && res?.data.data?.provider) {
        if (res?.data.data?.type === "expired") {
          setIsBlockWithdraw({
            email: res.data.data.email,
            provider: res.data.data.provider,
          });
          return;
        }
      }

      const loginRes = await postAuthLocalLogin({
        email: localLogin.email,
        password: localLogin.password,
      });

      login({
        response: loginRes,
        provider: "local",
        callback: () => {},
      });
    } else if (res.status === 409) {
      setIsBlockIP({
        provider: "local",
      });
    }
  };

  return {
    handleRegister,
    handleFindEmail,
    handleLoginEmail,
    handleFindPassword,
    handleFindResetPassword,
    handleLoginLogin,
    handleLoginCert,
    handleFindEmailResult,
    handleLoginPassword,
    handleClose,
    handleInit,
    handleChangeEmail,
    handleChangePassword,
    handleChangeProvider,
    open: localLogin.open,
    section: localLogin.section,
    email: email,
    password: password,
    provider: provider,
    isValidLoginFormat,
    isValidEmailFormat,
    isValidPasswordFormat,
  };
};
