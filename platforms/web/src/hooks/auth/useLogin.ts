import {
  getUser,
  isBlockIPState,
  isBlockWithdrawState,
  isDuplicateLoginState,
  syncHeader,
  useEvent,
  useLoginDialog,
  useTagManager,
  useTimeCounter,
} from "@wrtn/core";
import {
  IsBlockIPLoginState,
  IsBlockWithDrawLoginState,
  IsDuplicateLoginState,
  Login,
} from "@wrtn/core/types/login";
import { setCookie } from "cookies-next";
import dayjs from "dayjs";

import { useRouter } from "next/router";

import { useSetRecoilState, useResetRecoilState } from "recoil";

import { cookieKey, paths } from "src/constants";
import { localLoginEmailState, localLoginPasswordState } from "src/stores";

export type useLoginProps = {
  loginSuccessPath?: string | undefined;
};

export const useLogin = () => {
  const router = useRouter();
  const loginDialog = useLoginDialog();
  const setIsDuplicateLogin = useSetRecoilState<IsDuplicateLoginState>(
    isDuplicateLoginState
  );
  const resetEmail = useResetRecoilState(localLoginEmailState);
  const resetPassword = useResetRecoilState(localLoginPasswordState);
  const setIsBlockIP = useSetRecoilState<IsBlockIPLoginState>(isBlockIPState);
  const setIsBlockWithdraw =
    useSetRecoilState<IsBlockWithDrawLoginState>(isBlockWithdrawState);

  // const alert = useAlert();
  const tagManager = useTagManager();
  const { collectEvent } = useEvent();

  const login = async ({ response, provider, token, callback }: Login) => {
    if (response?.status === 201) {
      const { data } = response.data;
      const { accessToken, refreshToken } = data;

      if (typeof data === "object" && data?.provider) {
        if (data.type === "expired") {
          setIsBlockWithdraw({
            provider: data?.provider,
            email: data?.email,
          });
        } else {
          setIsDuplicateLogin({
            provider: data?.provider,
            email: data?.email,
            from: provider,
          });
        }

        if (callback) {
          callback(response.status);
        }
        return;
      }

      // setCookies(window.location.hostname + ".refresh_token", refreshToken, {
      //   path: "/",
      //   domain: window.location.hostname,
      //   sameSite: "lax",
      //   expires: dayjs().add(14, "day").toDate(),
      // });

      setCookie(cookieKey.refreshToken, refreshToken, {
        sameSite: "lax",
        domain: window.location.hostname.includes("wrtn.ai")
          ? "wrtn.ai"
          : "localhost",
        expires: dayjs().add(14, "day").toDate(),
      });

      setCookie(cookieKey.accessToken, accessToken, {
        sameSite: "lax",
        domain: window.location.hostname.includes("wrtn.ai")
          ? "wrtn.ai"
          : "localhost",
        expires: dayjs().add(14, "day").toDate(),
      });

      syncHeader({
        key: "Authorization",
        value: `Bearer ${accessToken}`,
      });

      syncHeader({
        key: "Refresh",
        value: `${refreshToken}`,
      });

      const resUser = await getUser();

      if (resUser?.status === 200) {
        const { data } = resUser.data;

        tagManager({
          event: "newbie_check",
          data: {
            user_newbie: data?.isNewbie,
          },
        });

        collectEvent("login_done");

        resetEmail();
        resetPassword();
        const desktopLogin = sessionStorage.getItem("login_app");
        const redirect_url = sessionStorage.getItem("redirect_url");

        if (redirect_url) {
          // setCookie(cookieKey.refreshToken, refreshToken, {
          //   sameSite: "lax",
          //   domain: "wrtn.ai",
          //   expires: dayjs().add(14, "day").toDate(),
          // });

          // setCookie(cookieKey.accessToken, accessToken, {
          //   sameSite: "lax",
          //   domain: "wrtn.ai",
          //   expires: dayjs().add(14, "day").toDate(),
          // });
          window.sessionStorage.removeItem("redirect_url");
          window.location.assign(redirect_url);
          return;
        }

        if (desktopLogin === "dt") {
          // from desktop application open external.
          router.push(paths.login.desktop.success());
          window.sessionStorage.removeItem("login_app");
          return;
        }
        if (
          router.pathname === paths.chat() ||
          router.pathname === paths.plugins()
        ) {
          router.reload();
        }
        const plugin = sessionStorage.getItem("pluginPath");
        if (plugin === "/plugins") {
          router.push(paths.plugins());
          window.sessionStorage.removeItem("pluginPath");
        } else {
          router.push(paths.chat());
        }
        loginDialog.handleClose();
        // window.location.assign(`${loginSuccessPath}` + window.location.search)
      }
    } else if (response.status === 409) {
      setIsBlockIP({ provider: provider });
    } else {
      // 400, 5xx
      // alert.removeAll();
      // alert.show("다시 시도해주세요.");
    }
    if (callback) callback(response.status);
  };

  return login;
};

export default useLogin;
