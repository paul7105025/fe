import React from "react";

export const useBlockInAppBrowser = () => {
  const [blockInAppBrowser, setBlockInAppBrowser] = React.useState(false);

  React.useEffect(() => {
    // @ts-ignore
    const ua = window.navigator.userAgent || window.navigator.vendor || window.opera;

    if (ua.indexOf("FBAN") > -1 || ua.indexOf("FBAV") > -1) {
      if (!window.location.href.match("redirect_fb")) {
        setBlockInAppBrowser(true);
      }
    }
  }, []);

  return blockInAppBrowser;
};

export default useBlockInAppBrowser;