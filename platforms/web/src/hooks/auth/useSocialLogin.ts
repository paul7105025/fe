import { CredentialResponse } from "@react-oauth/google";
import { getPlatform, useEvent, useLoginDialog } from "@wrtn/core";
import {
  KakaoLoginSuccessProps,
  NaverLoginSuccessProps,
} from "@wrtn/core/types/login";
import { setCookie } from "cookies-next";
import { useRouter } from "next/router";
import React from "react";
import { cookieKey, paths } from "src/constants";
import { postAuthGoogle, postAuthKakao, postAuthNaver, postAuthApple } from "@wrtn/core/services";
import useLogin from "./useLogin";
import { getFunnel, getUserAgent } from "@wrtn/core";

export const useSocialLogin = (position = "modal") => {
  const { collectEvent } = useEvent();
  const { handleClose } = useLoginDialog();

  const login = useLogin();

  const handleGoogleLoginSuccess = React.useCallback(
    async (credential: CredentialResponse) => {
      collectEvent("click_start_platform_btn", {
        platform: "google",
        position: position,
      });
      handleClose();
      const funnel = getFunnel();
      const platform = getPlatform();
      await login({
        response: await postAuthGoogle({
          auth: {
            token: credential?.credential || "",
          },
          funnel: funnel,
          platform: platform,
        }),
        provider: "google",
        token: credential.credential,
      });
    },
    []
  );

  const handleNaverLoginSuccess = React.useCallback(
    async ({ loginStatus }: NaverLoginSuccessProps) => {
      collectEvent("click_start_platform_btn", {
        platform: "naver",
        position: position,
      });
      handleClose();
      const funnel = getFunnel();
      const platform = getPlatform();

      await login({
        response: await postAuthNaver({
          auth: {
            token: loginStatus.accessToken.accessToken,
          },
          funnel: funnel,
          platform: platform
        }),
        provider: "naver",
      });
    },
    []
  );

  const handleKakaoLoginSuccess = React.useCallback(
    async (data: KakaoLoginSuccessProps) => {
      collectEvent("click_start_platform_btn", {
        platform: "kakao",
        position: position,
      });
      handleClose();
      const funnel = getFunnel();
      const platform = getPlatform();
      await login({
        response: await postAuthKakao({
          auth: {
            token: data.response.access_token,
          },
          funnel: funnel,
          platform: platform
        }),
        provider: "kakao",
      });
    },
    []
  );

  const handleAppleLoginSuccess = React.useCallback(
    async (data) => {
      collectEvent("click_start_platform_btn", {
        platform: "apple",
        position: position,
      });
      handleClose();
      // console.log(data);

      const funnel = getFunnel();
      const platform = getPlatform();
      const response = await postAuthApple({
        auth: {
          token: data.authorization.id_token,
        },
        funnel: funnel,
        platform: platform
      });
      // console.log(response);

      await login({
        response,
        provider: "apple",
      });
    },
    [],
  )

  const handleGoogleLoginFailure = () => {
    collectEvent("click_start_platform_btn", {
      platform: "google",
      position: position,
    });
  };

  const handleNaverLoginFailure = (state) => {
    collectEvent("click_start_platform_btn", {
      platform: "naver",
      position: position,
    });
  };

  const handleKakaoLoginFailure = () => {
    collectEvent("click_start_platform_btn", {
      platform: "kakao",
      position: position,
    });
  };
  
  const handleAppleLoginFailure = () => {
    collectEvent("click_start_platform_btn", {
      platform: "apple",
      position: position,
    });
  }

  return {
    handleGoogleLoginSuccess,
    handleNaverLoginSuccess,
    handleKakaoLoginSuccess,
    handleAppleLoginSuccess,
    handleGoogleLoginFailure,
    handleNaverLoginFailure,
    handleKakaoLoginFailure,
    handleAppleLoginFailure,
  };
};

export default useSocialLogin;
