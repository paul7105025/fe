import { useCallback } from "react";
import { useRouter } from "next/router";
import { useSearchParams, usePathname } from "next/navigation";

const useQueryString = () => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  const createQueryString = useCallback((key: string, value: string) => {
    const params = new URLSearchParams(searchParams);
    params.delete("page");
    params.set(key, value);

    return params.toString();
  }, [searchParams]);

  const onChangeQuery = (key: string, value: string) => {
    router.push(pathname + "?" + createQueryString(key, value));
  }

  return {
    onChangeQuery,
  }
}

export default useQueryString;
