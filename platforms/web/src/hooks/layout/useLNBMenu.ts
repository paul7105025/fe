import { currentLNBMenuState, currentLNBOpenState } from "@wrtn/core";
import { useRecoilState } from "recoil";
import React from "react";
import { useRouter } from "next/router";

type LNBMenu = "chat" | "store" | "tool";

export const useLNBMenu = () => {
  const router = useRouter();
  const [currentLNBMenu, setCurrentLNBMenu] = useRecoilState(currentLNBMenuState);
  const [currentLNBOpen, setCurrentLNBOpen] = useRecoilState(currentLNBOpenState);

  const handleChange = (lnb: LNBMenu) => {
    setCurrentLNBMenu(lnb);
  };

  const open = () => {
    setCurrentLNBOpen(true)
  }

  const close = () => {
    setCurrentLNBOpen(false);    
  }
  
  const handleOpen = () => {
    setCurrentLNBOpen(c => !c);
  }

  React.useEffect(() => {
    const pn = router.pathname;

    if (pn === "/") handleChange("chat")
    if (pn === "/tool/[[...toolId]]") handleChange("tool")
    if (pn === "/store") handleChange("store")
  }, [router.pathname])

  const isChat = currentLNBMenu === "chat";
  const isStore = currentLNBMenu === "store";
  const isTool = currentLNBMenu === "tool";

  return {
    isTool,
    isChat,
    isStore,
    handleChangeLNB: handleChange,
    handleOpen,
    open,
    close,
    isOpen: currentLNBOpen
  };
};

export default useLNBMenu;