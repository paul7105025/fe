import { useRecoilState } from "recoil";

import { LNBControllerState } from "src/stores";

export const useToolLNB = () => {
  const [toolController, setToolController] =
    useRecoilState(LNBControllerState);

  const open = () => {
    setToolController((c) => ({
      ...c,
      isOpen: true,
    }));
  };

  const close = () => {
    setToolController((c) => ({
      ...c,
      isOpen: false,
    }));
  };

  const handleOpen = () => {
    setToolController((c) => ({
      ...c,
      isOpen: !c.isOpen,
    }));
  };

  return {
    open,
    close,
    handleOpen,
    isOpen: toolController.isOpen,
  };
};

export default useToolLNB;
