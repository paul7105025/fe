import React from "react";

import { useRecoilValue } from 'recoil';
import { getLocal, setLocal, userState } from "@wrtn/core";
import { dialogDataType, dialogLocalDataType } from "@wrtn/core";
import { useRouter } from "next/router";

type dialogDataArrayType = Array<dialogDataType>;
type dialogLocalDataArrayType = Array<dialogLocalDataType>;

export const useDialog = (dialogData: dialogDataArrayType) => {
  const router = useRouter();
  
  const user = useRecoilValue(userState);

  const [openDialog, setOpenDialog] =
    React.useState<dialogDataArrayType>(dialogData);

  const toggleDialog = (type: string, value: boolean) => {
    setOpenDialog((res) =>
      res.map((dialog) => {
        if (dialog.name === type) return { ...dialog, isOpen: value };
        else return dialog;
      })
    );
  };

  const closeDialogTemp = (dialog: dialogDataType) => {
    const { name } = dialog;
    if (dialog?.onlyOnce) return closeDialog(dialog);
    toggleDialog(name, false);
  };

  const closeDialog = (dialog: dialogDataType) => {
    const { name } = dialog;

    toggleDialog(name, false);
    const localData = getLocal<dialogLocalDataArrayType>(`wrtn-image-event`);

    const newData = {
      name: name,
      dueDate: dialog?.onlyOnce && dialog.isOpen ? new Date("2100-12-31T00:00:00") : new Date(),
    };

    if (localData) {
      const targetLocalData = localData.find((data) => data.name === name);
      if (targetLocalData) {
        setLocal(`wrtn-image-event`, [
          ...localData.filter((data) => data.name !== name),
          newData,
        ]);
      } else {
        setLocal(`wrtn-image-event`, [...localData, newData]);
      }
    } else {
      setLocal(`wrtn-image-event`, [newData]);
    }
  };

  const fetchDataFromLocal = (
    targetLocalData: dialogLocalDataType | undefined,
    dialog: dialogDataType
  ) => {
    const currentTime = new Date();

    const currentPath = router.pathname;

    if (dialog?.associatedFunnelCheck 
      && dialog?.associatedFunnelCheck !== user?.associatedFunnel
      ) return toggleDialog(dialog.name, false);
    
    if (dialog?.skipActiveFirstday && user?.isNewbie) {
      return closeDialog(dialog);
    }
    
    if (dialog?.skipNewbie && user?.isNewbie) return toggleDialog(dialog.name, false);


    if (dialog?.onlyLogin && !user) return toggleDialog(dialog.name, false);
    if (currentPath.match(dialog.path)) {
      if (targetLocalData) {
        const diffTime = currentTime.getTime() - new Date(targetLocalData.dueDate).getTime()
        const closeDay = dialog.closeDay ?? 1;
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        if (diffDays > closeDay) {
          toggleDialog(dialog.name, true);
        } else {
          toggleDialog(dialog.name, false);
        }
      } else {
        toggleDialog(dialog.name, true);
      }
    }
  };

  React.useEffect(() => {
    const localData = getLocal<dialogLocalDataArrayType>(`wrtn-image-event`);

    openDialog.forEach((dialog) => {
      const targetLocalData = localData
        ? localData.find((data) => data.name === dialog.name) || undefined
        : undefined;

      if (dialog?.onlyLogin && !user) return ;
      if (dialog?.associatedFunnelCheck && dialog?.associatedFunnelCheck !== user?.associatedFunnel) return ;
      fetchDataFromLocal(targetLocalData, dialog);
    });
  }, [router.pathname, user]);

  return {
    openDialog,
    closeDialogTemp,
    closeDialog,
  };
};
