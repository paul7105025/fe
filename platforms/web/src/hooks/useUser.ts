import { userState } from "@wrtn/core";
import { useRecoilState } from "recoil";

export const useUser = () => {
  const [user, setUser] = useRecoilState(userState);

  return {
    user,
    name: user?.name,
  };
};
