import React from "react";
import { useRouter } from "next/router";
import { useFetchToolList, useFavoriteToolListSearch } from "@wrtn/core";

export const useGoToTool = () => {
  const router = useRouter();

  const { allToolList } = useFetchToolList();
  const favoriteToolList = useFavoriteToolListSearch("", allToolList);
  const newToolList = allToolList.filter((tool) => tool.label === "new");

  const goToTool = React.useCallback(() => {
    if (allToolList.length > 0) {
      if (favoriteToolList.length > 0) {
        router.push(`/tool/${favoriteToolList[0]._id}`);
      } else if (newToolList.length > 0) {
        router.push(`/tool/${newToolList[0]._id}`);
      } else {
        router.push(`/tool/${allToolList[0]._id}`);
      }
    }
  }, [allToolList, favoriteToolList]);

  return goToTool;
};
