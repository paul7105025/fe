import { useClickOutside } from "@wrtn/core";
import React from "react";

type Position = { 
  top: number;
  left: number;
}

const useHoverMenu = ({
  position,
}) => {
  const [hover, setHover] = React.useState(false);
  const parentRef = React.useRef<any>(null);
  const childRef = React.useRef<HTMLElement>(null);

  useClickOutside([childRef, parentRef], "mousedown", () => {
    setHover(false);
  })

  const handleMouseOver = () => {
    setHover(true);
  }

  const handleMouseOut = () => {
    setHover(false);
  }

  const handleClick = () => {
    setHover(c => !c);
  }

  const childPosition = {
    top: parentRef.current?.getBoundingClientRect().top + position.top,
    left: parentRef.current?.getBoundingClientRect().left + position.left,
  }

  const open = (renderFunc) => {
    if (!hover) return null;

    return (renderFunc({ position: childPosition, childRef }));
  }

  return {
    parentRef,
    childRef,
    handleMouseOut,
    handleMouseOver,
    handleClick,
    // position: childPosition,
    hover,
    open,
  }
}

export default useHoverMenu;