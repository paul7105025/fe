import { useRouter } from 'next/router';
import useSWR from 'swr';
import { useStoreFetcher } from ".."
import { StoreChatBotType } from '@wrtn/core';

export const useAuthorChatbotList = ({
  authorId,
}) => {

  const router = useRouter();
  const { b2bId } = router.query;
  const { defaultFetcher } = useStoreFetcher();
  const query = `?${b2bId ? 'b2bId=' + b2bId + '&' : ''}`;
  const { data, isLoading, error } = useSWR(router.isReady ? `/store/chat-bot/author/${authorId}` + query : null, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: [] as StoreChatBotType[]
    },
    errorRetryCount: 0,
  })

  if (!!error) {
    return ({
      authorChatbotList: [] as StoreChatBotType[],
      error,
    })
  }
  return ({
    authorChatbotList: data.data as StoreChatBotType[],
    error,
    isLoading,
  })
}