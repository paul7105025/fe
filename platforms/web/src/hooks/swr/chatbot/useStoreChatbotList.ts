import useSWR from 'swr';
import { useStoreFetcher } from '..';

interface useStoreChatbotListProps {
  title?: string
  category?: string
  page: number
  b2bId?: string | null
  limit: number
  excludeB2b: boolean
  doNotLoad?: boolean
}

export const useStoreChatbotList = ({
  title,
  category,
  page,
  b2bId,
  limit,
  excludeB2b, // 전체 or 일반
  doNotLoad
}: useStoreChatbotListProps) => {
  const { defaultFetcher } = useStoreFetcher();
  const _category = category === "전체" ? null : category;
  const query = `?${page ? 'page=' + page + '&' : ''}${limit ? 'limit=' + limit + '&' : ''}${title ? 'title=' + title + '&' : ''}${_category ? 'category=' + _category + '&' : ''}${b2bId ? 'b2bId=' + b2bId + '&' : ''}${excludeB2b ? `excludeB2b=true&` : ''}`;
  const { data, error, isLoading } = useSWR(!doNotLoad && 
    _category === "즐겨찾기"
    ? `/store/favorite/only` + query + "type=chatbot"
    : `/store/chat-bot` + query,
    defaultFetcher,
    {
      keepPreviousData: true,
      fallbackData: {
        data: {
          chatBotList: [],
          maxPage: 0,
        }
      },
      errorRetryInterval: 1000,
      errorRetryCount: 3,
    },
  );
  if (error || doNotLoad) 
    return {
    storeChatbotList: [],
    storeChatbotListMaxCount: 0,
    isError: error,
    isLoading,
  }

  return {
    storeChatbotList: data?.data.chatBotList,
    storeChatbotListMaxCount: data?.data.maxPage,
    isError: error,
    isLoading,
  }
}