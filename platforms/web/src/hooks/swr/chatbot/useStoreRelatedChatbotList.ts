import { useRouter } from 'next/router';
import useSWR from 'swr';
import { useStoreFetcher } from ".."
import { StoreChatBotType } from '@wrtn/core';

export const useStoreRelatedChatbotList = ({ chatbotId }: { chatbotId: string }) => {
  const router = useRouter();
  const { b2bId } = router.query;
  const { defaultFetcher } = useStoreFetcher();
  const query = `?${b2bId ? 'b2bId=' + b2bId + '&' : ''}`;
  const { data } = useSWR(router.isReady ? `/store/chat-bot/${chatbotId}/related` + query : null, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: []
    },
    errorRetryCount: 0,
  });

  return ({
    storeRelatedChatbotList: data.data as StoreChatBotType[]
  })
}