import useSWR from 'swr';
import { useStoreFetcher } from '..';
import { StoreChatBotType } from '@wrtn/core';

interface useStoreTrendChatbotListProps {
  title?: string;
  page?: number;
  limit?: number;
  doNotLoad?: boolean
}
export const useStoreTrendChatbotList = ({
  title,
  page,
  limit,
  doNotLoad,
}: useStoreTrendChatbotListProps) => {
  const { defaultFetcher } = useStoreFetcher();
  const query = `?${page ? 'page=' + page + '&' : ''}${limit ? 'limit=' + limit + '&' : ''}${title ? 'title=' + title + '&' : ''}`;
  const { data, error, isLoading } = useSWR(!doNotLoad && `/store/chat-bot/trend` + query, defaultFetcher, {
    fallbackData: {
      data: [] as StoreChatBotType[]
    },
    errorRetryCount: 0,
  });
  if (doNotLoad) return {
    storeTrendChatbotList: [],
    isError: error,
    isLoading,
  }

  return {
    storeTrendChatbotList: data.data as StoreChatBotType[],
    isError: error,
    isLoading,
  }
}