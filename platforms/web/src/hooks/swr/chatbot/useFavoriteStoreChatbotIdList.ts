
import { userState } from '@wrtn/core';
import useSWR from 'swr';
import { useStoreFetcher } from '..';
import { useRecoilValue } from 'recoil';

// TODO: 바궈
export const useFavoriteStoreChatbotIdList = () => {
  const { defaultFetcher } = useStoreFetcher();
  const user = useRecoilValue(userState);
  const { data, error, isLoading, mutate } = useSWR(user ? `/tool/favorite` : null, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: [
        {
          chatbotIdList: []
        }
      ]
    },
    errorRetryCount: 0,
  });

  if (error) 
    return (
      {
        favoriteChatbotIdList: [],
        append: () => {},
        remove: () => {}
      }
    )
  return {
    favoriteChatbotIdList: data?.data?.at(0)?.chatbotIdList || [],
    append: (id: string) => mutate({ 
      ...data, 
      data: [
        {
          chatbotIdList: data?.data[0].chatbotIdList.concat([id]) ?? []
        }
    ]}),
    remove: (id: string) => mutate({ 
      ...data, 
      data: [
        {
          chatbotIdList: data?.data[0].chatbotIdList.filter(v => v !== id) ?? []
        }
    ]})
  }
}