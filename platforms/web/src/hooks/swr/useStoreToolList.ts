import useSWR from 'swr';
import { useStoreFetcher } from '.';

interface useStoreToolListProps {
  title?: string
  category?: string
  page: number
  b2bId?: string | null
  limit: number
  excludeB2b: boolean
  doNotLoad?: boolean
}

export const useStoreToolList = ({
  title,
  category,
  page,
  b2bId,
  limit,
  excludeB2b,
  doNotLoad
}: useStoreToolListProps) => {
  const { defaultFetcher } = useStoreFetcher();
  const _category = category === "전체" ? null : category;
  const query = `?${page ? 'page=' + page + '&' : ''}${limit ? 'limit=' + limit + '&' : ''}${title ? 'title=' + title + '&' : ''}${_category ? 'category=' + _category + '&' : ''}${b2bId ? 'b2bId=' + b2bId + '&' : ''}${excludeB2b ? `excludeB2b=true&` : ''}`;
  const { data, error, isLoading } = useSWR(!doNotLoad &&
    _category === "즐겨찾기"
    ? `/tool/favorite/only` + query
    : `/tool` + query,
    defaultFetcher,
    {
      keepPreviousData: true,
      fallbackData: {
        data: {
          toolList: [],
          maxPage: 1,
        }
      },
      errorRetryInterval: 1000,
      errorRetryCount: 3,
    },
    // {
    //   errorRetryInterval: 1000,
    //   errorRetryCount: 3,
    // }
  );
  if (error || doNotLoad) 
    return {
    storeToolList: [],
    storeToolListMaxCount: 1,
    isError: error,
    isLoading,
  }

  return {
    storeToolList: data?.data.toolList,
    storeToolListMaxCount: data?.data.maxPage,
    isError: error,
    isLoading,
  }
}