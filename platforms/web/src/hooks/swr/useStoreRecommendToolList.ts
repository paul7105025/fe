import useSWR from 'swr';
import { useStoreFetcher } from "."

export const useStoreRecommendToolList = () => {
  const { defaultFetcher } = useStoreFetcher();
  const { data } = useSWR(`/tool/recommend`, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: []
    },
    errorRetryCount: 0,
  })

  return ({
    storeRecommendToolList: data.data
  })
}