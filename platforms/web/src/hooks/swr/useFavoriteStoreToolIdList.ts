
import { userState } from '@wrtn/core';
import useSWR from 'swr';
import { useStoreFetcher } from '.';
import { useRecoilValue } from 'recoil';

export const useFavoriteStoreToolIdList = () => {
  const { defaultFetcher } = useStoreFetcher();
  const user = useRecoilValue(userState);
  const { data, error, isLoading, mutate } = useSWR(user ? `/tool/favorite` : null, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: [
        {
          toolIdList: []
        }
      ]
    },
    errorRetryCount: 0,
  });

  if (error) 
    return (
      {
        favoriteToolIdList: [],
        // mutateFavoriteToolIdList: mutate,
        append: () => {},
        remove: () => {}
      }
    )
  return {
    favoriteToolIdList: data?.data?.at(0)?.toolIdList || [],
    append: (id: string) => mutate({ 
      ...data, 
      data: [
        {
          toolIdList: data?.data[0].toolIdList.concat([id]) ?? []
        }
    ]}),
    remove: (id: string) => mutate({ 
      ...data, 
      data: [
        {
          toolIdList: data?.data[0].toolIdList.filter(v => v !== id) ?? []
        }
    ]})
  }
}