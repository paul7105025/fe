import { useRouter } from 'next/router';
import useSWR from 'swr';
import { useStoreFetcher } from "."

export const useStoreRelatedToolList = ({ toolId }) => {
  const router = useRouter();
  const { b2bId } = router.query;
  const { defaultFetcher } = useStoreFetcher();
  const query = `?${b2bId ? 'b2bId=' + b2bId + '&' : ''}`;
  const { data } = useSWR(router.isReady ? `/tool/related/${toolId}` + query : null, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: []
    },
    errorRetryCount: 0,
  });

  return ({
    storeRelatedToolList: data.data
  })
}