import { storeInstance } from "@wrtn/core";

export const useStoreFetcher = () => {
  const defaultFetcher = (url: string) => storeInstance.get(url).then(res => res.data);
  
  return {
    defaultFetcher
  }
}