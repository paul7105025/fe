import { useStoreFetcher } from "."
import useSWR from 'swr';
import { B2bMember } from "@wrtn/core";

export const useStoreB2bMemberList = () => {
  const { defaultFetcher } = useStoreFetcher(); 
  const { data, error, isLoading } = useSWR(`/b2bmember`, defaultFetcher, {
    fallbackData: {
      data: []
    }
  });

  if (error) {
    return {
      storeB2bMemberList: [] as B2bMember[]
    }
  }
  return {
    storeB2bMemberList: data?.data as B2bMember[],
    isError: error,
    isLoading,
  }
}