import { useRouter } from 'next/router';
import useSWR from 'swr';
import { useStoreFetcher } from "."

export const useAuthorToolList = ({
  authorId,
}) => {

  const router = useRouter();
  const { b2bId } = router.query;
  const { defaultFetcher } = useStoreFetcher();
  const query = `?${b2bId ? 'b2bId=' + b2bId + '&' : ''}`;
  const { data, isLoading, error } = useSWR(router.isReady ? `/tool/author/${authorId}` + query : null, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: []
    },
    errorRetryCount: 0,
  })

  if (!!error) {
    return ({
      authorToolList: [],
      error,
    })
  }
  return ({
    authorToolList: data.data,
    error,
    isLoading,
  })
}