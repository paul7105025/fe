import useSWR from 'swr';
import { useStoreFetcher } from '.';

interface useStoreTrendToolListProps { 
  title?: string;
  page?: number;
  limit?: number;
  doNotLoad?: boolean;
}
export const useStoreTrendToolList = ({
  title,
  page,
  limit,
  doNotLoad,
}: useStoreTrendToolListProps) => {
  const { defaultFetcher } = useStoreFetcher();
  const query = `?${page ? 'page=' + page + '&' : ''}${limit ? 'limit=' + limit + '&' : ''}${title ? 'title=' + title + '&' : ''}`;
  const { data, error, isLoading } = useSWR(!doNotLoad && `/tool/trend` + query, defaultFetcher, {
    fallbackData: {
      data: []
    }
  });

  // TODO: 
  const { data: tempData, error: tempError } = useSWR(`/tool/6471be3d3163fcd56f190fd3`, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: null
    },
    errorRetryCount: 0
  });
  const { data: temp2Data, error: temp2Error } = useSWR(`/tool/647198913163fcd56f18ee70`, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: null
    },
    errorRetryCount: 0
  });
  
  if (error || tempError || temp2Error || doNotLoad) {
    return {
      storeTrendToolList: [],
      isError: error,
      isLoading,
    }
  }
  return {
    storeTrendToolList: tempData?.data && temp2Data?.data ? [tempData?.data, temp2Data.data].concat(data?.data.filter(v => v._id !== tempData.data._id && v._id !== temp2Data.data._id)) : data?.data,
    isError: error || tempError,
    isLoading,
  }
}