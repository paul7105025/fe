import { useEffect, useState } from "react";
import { useRecoilState, useRecoilValue } from "recoil";

import { deleteLike, getMyLikedChatIds, likedChatIdsState, postLike, useEvent, userState } from "@wrtn/core";

export const useLike = ({ chatId, likes: _likes }) => {
  const [isLiked, setIsLiked] = useState(false);
  const [likes, setLikes] = useState(_likes);
  const [likedChatIds, setLikedChatIds] = useRecoilState(likedChatIdsState);
  const user = useRecoilValue(userState);

  const { collectEvent } = useEvent();

  useEffect(() => {
    if (user && !likedChatIds) {
      fetchMyLikedChatList();
    }
  }, [user, likedChatIds])

  useEffect(() => {
    if (likedChatIds?.includes(chatId)) {
      setIsLiked(true);
    } else {
      setIsLiked(false);
    }
  }, [likedChatIds]);

  const fetchMyLikedChatList = async () => {
    try {
      const res = await getMyLikedChatIds();
      if (res.data.data && Array.isArray(res.data.data.shareChatIdList)) {
        setLikedChatIds(res.data.data.shareChatIdList);
      }
    } catch(err) {
      // console.log(err);
    }
  }

  const onClickLike = async () => {
    if (!user) return;

    try {
      if (isLiked) {
        collectEvent("click_share_trend_like_btn", {
          share_trend_post_id: chatId,
          onoff: 'off',
        });
        await deleteLike(chatId);
        setLikes(likes - 1);
        setLikedChatIds(likedChatIds?.filter(id => id !== chatId) ?? []);
      } else {
        collectEvent("click_share_trend_like_btn", {
          share_trend_post_id: chatId,
          onoff: 'on',
        });
        await postLike(chatId);
        setLikes(likes + 1);
        setLikedChatIds([...(likedChatIds ?? []), chatId]);
      }
    } catch(err) {
      // console.log(err);
    }
  }

  return {
    likes,
    isLiked,
    onClickLike,
  }
}
