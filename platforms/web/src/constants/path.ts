export const paths = {
  login: {
    index: () => '/login',
    desktop: {
      index: () => "/login/desktop",
      success: () => "/login/desktop/success"
    },
    social: {
      google: () => "/login",
      kakao: () => "/login",
      naver: () => "/login",
    },
    local: {
      index: () => "/login/wrtn",
      cert: () => "/login/wrtn/cert",
      signin: () => "/login/wrtn/signIn",
      signup: () => "/login/wrtn/signUp",
    },
    find: {
      password: () => "/login/find/password",
      email: () => "/login/find/email",
      reset: () => "/login/find/reset",
    },
  },
  setting: {
    account: () => "/setting/account",
    coupon: () => "/setting/coupon",
    history: () => "/setting/history",
    invoice: () => "/setting/invoice",
    plan: () => "/setting/plan",
    billing: {
      index: () => "/setting/billing",
      success: () => "/setting/billing/success",
    },
  },
  about: () => "/about",
  guide: () => "/guide",
  chat: () => "/",
  editor: () => "/editor",
  tool: () => "/tool",
  logout: () => "/logout",
  survey: () => "/survey",
  plugins: () => "/plugins",
};
