
export const cookieKey = {
  accessToken: "access_token",
  refreshToken: "refresh_token",
  deviceId: "_deviceId",
  ga: "_ga",
} as const;