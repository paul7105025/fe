export const isUpperPlan = (current: string, target: string): boolean => {
  const planList = ["FREE", "PLUS"];
  const currentIdx = planList.findIndex((e) => e === current);
  const targetIdx = planList.findIndex((e) => e === target);
  return currentIdx < targetIdx;
};
