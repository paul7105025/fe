import { LoginProvider } from "@wrtn/core/types/login";
import { CERT_GOOGLE_DUPLICATE_MESSAGE, CERT_KAKAO_DUPLICATE_MESSAGE, CERT_MAIL_DUPLICATE_MESSAGE, CERT_NAVER_DUPLICATE_MESSAGE } from "../constants";

export const providerAuthHref = (provider: LoginProvider) => {
  if (provider === "kakao") return "https://accounts.kakao.com";
  if (provider === "google") return "https://www.google.com/";
  if (provider === "naver") return "https://www.naver.com/";
}

export const providerKor = (provider: LoginProvider) => {
  if (provider === "google") return '구글'
  if (provider === "kakao") return '카카오'
  if (provider === "naver") return '네이버'
  if (provider === "local") return "뤼튼"
  return ""
}

export const providerDuplicateMessage = (provider: LoginProvider) => {
  if (provider === "local") return CERT_MAIL_DUPLICATE_MESSAGE;
  if (provider === "kakao") return CERT_KAKAO_DUPLICATE_MESSAGE;
  if (provider === "naver") return CERT_NAVER_DUPLICATE_MESSAGE;
  if (provider === "google") return CERT_GOOGLE_DUPLICATE_MESSAGE;
  return null
}