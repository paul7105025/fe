import { ParsedUrlQuery } from "querystring";

export const parseQuery = (query: ParsedUrlQuery, key: string) => {
  const value = query[key];
  return (
    !value ? null
    : typeof value === "string"
    ? value
    : value[0]
  );
}