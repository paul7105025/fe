import { cookieKey } from "src/constants/cookieKey";
import { getCookie } from 'cookies-next';
import { GetServerSidePropsContext } from "next";
import { postAuthRefresh } from "@wrtn/core";

export const readAccessKey = async (ctx?: GetServerSidePropsContext): Promise<string | null> => {
  const refreshTokenKey = cookieKey.refreshToken;
  const accessTokenKey = cookieKey.accessToken;
  let refreshToken: string | null | undefined = null;
  let accessToken: string | null | undefined = null;
  if (ctx) {
    refreshToken = getCookie(refreshTokenKey, {
      req: ctx.req,
      res: ctx.res,
    })?.toString();
    accessToken = getCookie(accessTokenKey, {
      req: ctx.req,
      res: ctx.res,
    })?.toString();
  } else {
    refreshToken = getCookie(refreshTokenKey, {
    })?.toString().trim();
    accessToken = getCookie(accessTokenKey, {
    })?.toString().trim();
  }

  if (!refreshToken) return null;
  const res = await postAuthRefresh({ refreshToken });
  if (res?.status === 201) {
    return res.data.data.accessToken;
  }
  // if (accessToken) return accessToken


  return null;
}

// export const isAuth = async (ctx: GetServerSidePropsContext): boolean => {
//   const accessKey = await readAccessKey(ctx);


//   return !!readAccessKey(ctx);
// }