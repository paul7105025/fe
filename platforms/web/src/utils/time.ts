export const secondFormatter = (time: number) => {
  return `${Math.floor(time / 60)}:${
    (time % 60).toString().length < 2
      ? `0${(time % 60).toString()}`
      : (time % 60).toString()
  }`;
};