import styled from "styled-components";
import dayjs from "dayjs";

import { couponApplyText, couponType, COUPON_CONDITION_RECORD, userCouponStatusRecord } from "@wrtn/core";

import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";

import { Close } from "@wrtn/ui/assets";

interface CouponDialogProps {
  coupon: couponType;
  onClose: () => void;
  onAccept: () => void;
}

export const CouponDialog = ({
  coupon,
  onClose,
  onAccept,
}: CouponDialogProps) => {

  return (
    <ModalPortal>
      <CouponDialogWrapper column>
        <CloseButton onClick={onClose}>
          <Close />
        </CloseButton>
        <Title>쿠폰 코드 등록</Title>
        <SubTitle>쿠폰 이름</SubTitle>
        <Context>{coupon.name || "-"}</Context>
        <SubTitle>혜택 내용</SubTitle>
        <Context>{coupon.description || "-"}</Context>
        <SubTitle>혜택 조건</SubTitle>
        <Context>
          {coupon?.condition ? COUPON_CONDITION_RECORD[coupon.condition] : "-"}
        </Context>
        <SubTitle>혜택 적용 기간</SubTitle>
        <Context>
          { couponApplyText(coupon) }
        </Context>
        <SubTitle>쿠폰 만료일</SubTitle>
        <Context>
          {dayjs(coupon.dueDate).format("YYYY년 MM월 DD일 HH:mm")}
        </Context>
        <AlertText>
          <b>주의사항</b>
          <div style={{ height: "8px" }} />
          {
            coupon?.type === "discount" &&
            <ul>
              <li style={{"listStylePosition": "outside"}}>결제를 하지 않은 사용자는 결제 시 쿠폰을 적용할 수 있습니다.</li>
              <li style={{"listStylePosition": "outside"}}>
                이미 구독 중인 사용자는 <b>{"결제 정보 > 결제 정보 수정 > 쿠폰 등록"}</b> 에서 쿠폰을 적용할 수 있습니다. <br/>이 경우, 다음 달 결제부터 쿠폰 혜택이 적용됩니다.
              </li>
              <li style={{"listStylePosition": "outside"}}>
                구독 취소하신 사용자는 <b>{"요금제 > 구독 재개"}</b> 이후 위 방법으로 쿠폰을 적용할 수 있습니다.
              </li>
              <li style={{"listStylePosition": "outside"}}>추후 쿠폰을 적용한 결제 건을 환불하거나 구독을 취소하더라도 이미 사용된 쿠폰은 다시 제공되지 않습니다.</li>
            </ul>
          }
          { coupon?.type === "experience" &&
            <ul>
              <li style={{"listStylePosition": "outside"}}>등록 시 즉시 적용됩니다.</li>
            </ul>
          }
        </AlertText>
        <AcceptButton onClick={onAccept}>등록하기</AcceptButton>
      </CouponDialogWrapper>
    </ModalPortal>
  );
};

const CouponDialogWrapper = styled(FlexWrapper)`
  width: 640px;
  max-width: 95vw;
  padding: 32px 40px 40px;

  position: relative;

  background-color: ${colors.WHITE};
  border-radius: 12px;

  justify-content: flex-start;
  align-items: flex-start;
`;

const CloseButton = styled(FlexButton)`
  position: absolute;
  top: 36px;
  right: 40px;

  width: 20px;
  height: 20px;

  cursor: pointer;
`;

const Title = styled.p`
  ${typo({
    weight: 700,
    size: "24px",
    color: colors.GRAY_90,
    height: "29px",
    isStatic: true,
  })}
  margin-bottom: 36px;
`;

const SubTitle = styled.p`
  ${typo({
    weight: 600,
    size: "14px",
    color: colors.GRAY_60,
    isStatic: true,
  })}
  margin-bottom: 16px;
`;

const Context = styled.p`
  ${typo({
    weight: 600,
    size: "18px",
    color: colors.GRAY_80,
    isStatic: true,
  })}
  margin-bottom: 22px;
`;

const AlertText = styled.div`
  ${typo({
    weight: 500,
    size: "14px",
    height: "130%",
    color: colors.GRAY_60,
    isStatic: true,
  })}
  margin-top: 34px;
  margin-bottom: 20px;

  list-style-position: inside;

  li {
    margin-bottom: 9px;
  }
`;

const AcceptButton = styled(FlexButton)`
  width: 100%;
  padding: 20px;

  background-color: ${colors.POINT_PURPLE};
  border-radius: 5px;

  ${typo({
    weight: 700,
    size: "20px",
    color: colors.WHITE,
    isStatic: true,
  })}

  cursor: pointer;
  &:hover {
    background-color: ${colors.POINT_PURPLE_HOVER};
  }
`;
