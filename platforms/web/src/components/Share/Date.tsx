import { parseISO, format } from "date-fns"

export default function Date({ dateString }) {
  const date = parseISO(dateString);
  return <time dateTime={dateString}>{format(date, "yyyy년 MM월 dd일 HH:mm")}</time>
}
