"use client"
import React from 'react'
import { useAlert } from "react-alert";
import styled from 'styled-components';
import { Icon, newColors } from '@wrtn/ui'

type Props = {
  content: string;
}

export default function CopyMessageButton({ content }: Props) {
  const alert = useAlert();

  const onClickCopyMessage = async () => {
    await navigator.clipboard.writeText(content);
    alert.show("메시지를 복사했어요.");
  }
  
  return (
    <IconArea onClick={onClickCopyMessage}>
      <Icon icon="copy-alt" size={22} color={newColors.GRAY_300}/>
    </IconArea>
  )
}

export const IconArea = styled.div`
  align-self: flex-end;
  cursor: pointer;
`
