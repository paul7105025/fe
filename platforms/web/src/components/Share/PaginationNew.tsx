import { FlexWrapper, newColors, typo } from "@wrtn/ui";
import styled from "styled-components";
import { RoundCarouselLeft, RoundCarouselRight, RoundCarouselRightDouble } from './RoundCarousel';
import { useMemo } from "react";

type Props = {
  currentIndex: number;
  maxIndex: number;
  handlePage(newIndex: number): void;
}

export default function PaginationNew({ currentIndex, maxIndex, handlePage }: Props) {
  /*
  * page는 1, 2, 3, ... - 유저에게 보여지는 페이지
  * index는 0, 1, 2, ... - 백엔드에게 보내지는 인덱스
  * page = index+1 인 관계를 늘 성립함
  */
  const pageArray = useMemo(() => {
    const startIndex = Math.floor(currentIndex / 10);
    const lastIndex = startIndex + 9 < maxIndex ? startIndex + 9 : maxIndex;

    const array: number[] = [];
    for (let i = startIndex; i <= lastIndex; i++) {
      const page = i + 1;
      array.push(page);
    }
    return array;
  }, [currentIndex, maxIndex]);

  const showLastPageMoveButton = useMemo(() => {
    const lastIndex = pageArray[pageArray.length - 1] - 1;

    return lastIndex < maxIndex;
  }, [maxIndex]);

  const onClickPage = (nextIndex: number) => {
    handlePage(nextIndex);
  }

  return (
    <PaginationContainer>
      <RoundCarouselLeft 
        disabled={currentIndex === 0}
        onClick={() => onClickPage(currentIndex - 1)}
      />
      <div>
        {pageArray.map((page) => {
          return (
            <PageWrapper
              isSelected={page === currentIndex+1}
              onClick={() => onClickPage(page - 1)}
            >{page}</PageWrapper>
          )
        })}
      </div>
      <CarouselRightContainer>
        <RoundCarouselRight
          disabled={currentIndex === maxIndex}
          onClick={() => onClickPage(currentIndex + 1)}
        />
        {showLastPageMoveButton && (
          <RoundCarouselRightDouble disabled={true} onClick={() => onClickPage(pageArray[pageArray.length - 1] - 1)}/>
        )}
      </CarouselRightContainer>
    </PaginationContainer>
  )
}

const PaginationContainer = styled(FlexWrapper)`
  flex-direction: row;
  gap: 17px;
  align-items: center;
`

const PageWrapper = styled.span<{ isSelected: boolean }>`
  padding: 5px 10px;
  cursor: pointer;

  ${typo({
    weight: 500,
    size: "18px",
  })}
  color: ${({ isSelected }) => isSelected ? newColors.PURPLE_500_PRIMARY : newColors.GRAY_400};
`

const CarouselRightContainer = styled(FlexWrapper)`
  flex-direction: row;
  gap: 10px;
`