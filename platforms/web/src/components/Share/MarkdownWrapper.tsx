"use client"
import { useAlert } from "react-alert";
import React from "react";
import ReactMarkdown from "react-markdown";
import remarkGfm from "remark-gfm";
import { PrismLight as SyntaxHighlighter } from 'react-syntax-highlighter';
import { materialDark } from "react-syntax-highlighter/dist/cjs/styles/prism";

import { newColors, Copy, newTypo } from "@wrtn/ui";
import styled from "styled-components";

const Code = ({ node, inline, className, ...props }) => {
  const alert = useAlert();
  const handleCopy = (e: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
    const text = e.currentTarget.parentElement?.nextElementSibling?.textContent;
    window.navigator.clipboard.writeText(text ?? "");
    alert.removeAll();
    alert.show("복사되었습니다.")
  }

  const language = /language-(\w+)/.exec(className || '');
  if (inline) {
    return <code style={{ whiteSpace: "break-spaces" }} {...props} />
  }
  return (
    <>
      <ChatPreHeader>
        <div style={{ color: "white" }}>{language?.at(1) || ""}</div>
        <CodeCopy
          onClick={handleCopy}/>
      </ChatPreHeader>
      <div style={{width: '100%', paddingTop: '1px', backgroundColor: "#2f2f2f"}}>
        <SyntaxHighlighter style={materialDark}
          language={language?.at(1) || 'javascript'}
        >{ props.children }
        </SyntaxHighlighter>
      </div>
    </>
  )
}

export default function MarkdonwWrapper({ text }: { text: string }) {
  return (
    <ReactMarkdown
      remarkPlugins={[remarkGfm]}
      components={{
        // @ts-ignore
        code: Code,
        ul: ({ ...props}) => (
          <ul style={{ listStylePosition: "inside", paddingLeft: "10px", marginBottom: "10px" }} {...props} />),
        ol: ({ ordered, ...props }) => (
          <ol style={{ listStylePosition: "inside", marginBottom: "10px" }} {...props} />),
        a: ({ ...props }) => (
          <a {...props} target="_blank" onClick={() => {
            console.log("i'm cool")
          }} />
        )
      }}
    >
      {text}
    </ReactMarkdown>
  )
}

export const ChatPreHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background: ${newColors.GRAY_800};
  padding: 12px 20px;
  border-radius: 8px 8px 0px 0px;
`;

export const CodeCopy = styled(Copy)`
  cursor: pointer;
  path {
    fill: ${newColors.WHITE};
  }
`;

export const ChatCode = styled.code`
  width: 100%;
  ${newTypo("body_compact-16-semi")}
`