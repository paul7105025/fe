import React from 'react'
import styled from 'styled-components'
import { FlexWrapper, newColors, typo } from '@wrtn/ui'

type Props = {
  label: string;
  isSelected: boolean;
  onClick(): void;
}

export default function CategoryChip({ label, isSelected, onClick }: Props) {
  return (
    <LabelWrapper isSelected={isSelected} onClick={onClick}>{label}</LabelWrapper>
  )
}

const LabelWrapper = styled(FlexWrapper)<{ isSelected: boolean }>`
  border: 1px solid ${({isSelected}) => isSelected ? newColors.PURPLE_500_PRIMARY : newColors.WHITE};
  width: 115px;
  height: 36px;
  justify-content: center;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  border-bottom-right-radius: 20px;
  ${({ isSelected }) => isSelected ? "" : "border-bottom-left-radius: 20px;"};
  background-color: white;
  cursor: pointer;

  ${({ isSelected }) => isSelected ? (
    typo({
      color: newColors.PURPLE_500_PRIMARY,
      weight: 700,
      size: "16px",
    })) : (
      typo({
        color: newColors.GRAY_500,
        weight: 500,
        size: "16px",
      })
    )};
`