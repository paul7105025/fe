import Link from 'next/link'
import { useRef, useState } from 'react'
import styled from 'styled-components'

import { ShareChatType, useClickOutside, useEvent } from '@wrtn/core';
import { FilledHeart, FlexWrapper, Icon, newColors, typo } from '@wrtn/ui'

import Date from "src/components/Share/Date";
import { useLike } from 'src/hooks';

type Props = {
  chat: ShareChatType;
  editable?: boolean;
  removeChat?(chatId: string): Promise<void>;
  editChat?(chatId: string, newTopic: string): Promise<void>;
}

export default function ChatCardItem({ chat: { _id, topic, userName, shareId, views, likes: _likes, createdAt }, editable, removeChat, editChat }: Props) {
  const [showMenu, setShowMenu] = useState(false);
  const [isEditable, setIsEditable] = useState(false);
  const [newTopic, setNewTopic] = useState(topic)
  const ref = useRef<HTMLDivElement>(null);

  const { likes, isLiked } = useLike({ chatId: _id, likes: _likes });

  const { collectEvent } = useEvent();
  
  useClickOutside(ref, "mousedown", () => {
    setShowMenu(false);
  });

  const editChatTopic = async (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    setShowMenu(false);
    setIsEditable(true);
  }

  const deleteChat = async (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    setShowMenu(false);
    if (removeChat) {
      await removeChat(_id);
      collectEvent("delete_share_trend_post", {
        share_trend_post_id: shareId,
      })
    }
  }

  const cancelEditChatTopic = async () => {
    setIsEditable(false);
    setNewTopic(topic);
  }

  const submitEditChatTopic = async () => {
    setIsEditable(false);
    if (editChat) await editChat(_id, newTopic);
  }

  const handleKeyDown = async (e) => {
    if (e.key === "Enter") {
      setIsEditable(false);
      if (editChat) await editChat(_id, newTopic);
    }
  }

  return (
    <CardWrapper>
        <ChatInfoLayout>
          {isEditable ? (
          <InfoArea>
            <UserInfoArea>
              <NickName>{(userName ?? "").slice(0, 1)}</NickName>
              <Name>{userName}</Name>
              <div>·</div>
              <div>
                <Date dateString={createdAt ?? ""} />
              </div>
            </UserInfoArea>
            <EditArea>
              <InputWrapper>
                <input
                  value={newTopic}
                  onChange={(e) => setNewTopic(e.target.value)} 
                  onKeyDown={handleKeyDown}
                  style={{ backgroundColor: "inherit", width: "100%", fontSize: "inherit" }}
                />
              </InputWrapper>
              <span onClick={submitEditChatTopic}><Icon icon="check" size={22} color={newColors.GRAY_600} /></span>
              <span onClick={cancelEditChatTopic}><Icon icon="times" size={22} color={newColors.GRAY_400} /></span>
            </EditArea>
          </InfoArea>
          ) : (
            <Link href={`/share/${shareId}`} style={{ width: "100%" }}>
              <InfoArea>
                <UserInfoArea>
                  <NickName>{(userName ?? "").slice(0, 1)}</NickName>
                  <Name>{userName}</Name>
                  <div>·</div>
                  <div>
                    <Date dateString={createdAt ?? ""} />
                  </div>
                </UserInfoArea>
                  <TitleArea>{topic?.length > 100 ? `${topic.slice(0, 100)}...`: `${topic ?? "제목없음"}`}</TitleArea>
              </InfoArea>
            </Link>
          )}
          <IconWrapper>
            {editable ? (
              <>
                <span onClick={() => setShowMenu(!showMenu)}>
                  <Icon icon="ellipsis-h" size={20} color={newColors.GRAY_500} />
                </span>
                <MenuWrapper showMenu={showMenu} ref={ref}>
                  <div onClick={editChatTopic}>
                    <Icon icon="edit-alt" size={20} color={newColors.GRAY_400}/>
                    게시물 제목 수정
                  </div>
                  <div onClick={deleteChat}>
                    <Icon icon="trash-alt" size={20} color={newColors.GRAY_400} />
                    게시물 삭제
                  </div>
                </MenuWrapper>
              </>
            ) : (
              <Link href={`/share/${shareId}`}>
                <Icon icon="angle-right" size={24} color={newColors.GRAY_500} />
              </Link>
            )}
          </IconWrapper>
        </ChatInfoLayout>
      <BottomArea>
        <BottomIconWrapper>
          <Icon icon="eye" size={20} color={newColors.GRAY_600} />
          <div style={{ marginRight: "10px" }}>{views}</div>
        </BottomIconWrapper>
        <BottomIconWrapper>
          {isLiked ? (
            <FilledHeart />
          ) : (
            <Icon icon="heart" size={20} color={newColors.GRAY_600} />
          )}
          <div>{likes ?? 0}</div>
        </BottomIconWrapper>
      </BottomArea>
    </CardWrapper>
  )
}

const CardWrapper = styled(FlexWrapper)`
  flex-direction: column;
  border: 1px solid ${newColors.GRAY_100};
  border-radius: 10px;
  self-align: center;
  width: 100%;
  padding: 20px;

  box-shadow: 0px 2px 10px rgba(79, 68, 195, 0.1);
`

const ChatInfoLayout = styled(FlexWrapper)`
  flex-direction: row;
  width: 100%;
  padding-bottom: 20px;
  border-bottom: 1px solid ${newColors.GRAY_100}
`

const InfoArea = styled(FlexWrapper)`
  flex-direction: column;
  align-items: baseline;
  gap: 20px;
  flex: 1;
`

const UserInfoArea = styled(FlexWrapper)`
  flex-direction: row;
  gap: 10px;

  ${typo({
  weight: 500,
  size: "16px",
  color: newColors.GRAY_500,
})}
`

const BottomArea = styled(FlexWrapper)`
  flex-direction: row;
  align-self: baseline;
  padding-top: 10px;
  gap: 20px;
  align-items: center;

  ${typo({
  weight: 500,
  size: "14px",
  color: newColors.GRAY_600,
})}
`

const BottomIconWrapper = styled(FlexWrapper)`
  flex-direction: row;
  gap: 10px;
`

const TitleArea = styled.div`
  ${typo({
    weight: 600,
    size: "18px",
    color: newColors.GRAY_800,
    height: "28.8px",
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 600,
      size: "16px",
      color: newColors.GRAY_800,
      height: "25.6px",
    })}
  }
`
const NickName = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${newColors.WHITE};
  background-color: #C8CAFF;
  border-radius: 20px;
  width: 26px;
  height: 26px;
`

const Name = styled.span`
  color: ${newColors.GRAY_700};
`

const IconWrapper = styled.div`
  padding: 4px;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: ${newColors.GRAY_000}
  }
`

const MenuWrapper = styled(FlexWrapper)<{ showMenu: boolean }>`
  position: absolute;
  right: -175px;
  top: 20px;
  width: 171px;

  @media (max-width: 767px) {
    right: 0px;
    top: 50px;
  }

  background-color: white;
  border: 1px solid ${newColors.GRAY_200};
  border-radius: 4px;
  flex-direction: column;
  align-items: flex-start;
  padding: 8px 0;
  display: ${({ showMenu }) => showMenu ? "show" : "none"};

  ${typo({
    weight: 600,
    size: "16px",
    color: newColors.GRAY_700,
  })}

  > div {
    display: flex;
    padding: 10px;
    gap: 11px;
    width: 100%;
    align-items: center;
    cursor: pointer;

    &:hover {
      background-color: ${newColors.GRAY_000};
    }
  }
`

const EditArea = styled(FlexWrapper)`
  gap: 10px;
  width: 90%;
`

const InputWrapper = styled(FlexWrapper)`
  flex: 1;
  height: 26px;
  border-bottom: 1px solid ${newColors.PURPLE_500_PRIMARY};
  background-color: ${newColors.BLUE_400_BG};
  padding: 5px;

  > span {
    cursor: pointer;
  }
`
