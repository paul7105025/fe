import Image from "next/image";
import { OutputMessageType } from "@wrtn/core";
import { Icon, newColors } from "@wrtn/ui";
import { useState } from "react";
import { ArrowArea, ChatLogo, ContentArea, IconArea, LogoArea, MessageWrapper, Outline, PageWrapper } from "./OutputMessage";
import styled from "styled-components";
import { RoundCarouselLeft, RoundCarouselRight } from "../RoundCarousel";

type Props = {
  messages: OutputMessageType;
}

export function ImageOutputMessage({ messages }: Props) {
  const [currentIndex, setCurrentPage] = useState(0);
  if (messages.length < 1) return null;

  const isLastPage = (index: number) => index === messages.length - 1;
  const isFirstPage = (index: number) => index === 0;

  const onClickPage = (isNext: boolean) => {
    if (isNext) {
      if (isLastPage(currentIndex)) return;
      setCurrentPage((page) => page+1);
    } else {
      if (isFirstPage(currentIndex)) return;
      setCurrentPage((page) => page-1);
    }
  }

  return (
    <div style={{ width: "100%" }}>
      <Outline>
        <MessageWrapper>
          <LogoArea>
            <ChatLogo/>
          </LogoArea>
          <ContentArea>
            <div dangerouslySetInnerHTML={{ __html: messages[currentIndex].content }}/>
            <ImagesArea>
              {messages[currentIndex].image?.imageUrls.map((img) => {
                return (
                  <ImageWrapper>
                    <Image src={img.url} alt="image" fill={true} style={{ objectFit: "cover" }} />
                  </ImageWrapper>
                )
              })}
            </ImagesArea>
          </ContentArea>
        </MessageWrapper>
        <IconArea><Icon icon="copy-alt" size={22} color={newColors.GRAY_300}/></IconArea>
      </Outline>
      {messages.length > 1 && (
        <ArrowArea>
          <RoundCarouselLeft disabled={isFirstPage(currentIndex)} onClick={() => onClickPage(false)}/>
          <PageWrapper>{currentIndex+1} / {messages.length}</PageWrapper>
          <RoundCarouselRight disabled={isLastPage(currentIndex)} onClick={() => onClickPage(true)}/>
        </ArrowArea>
      )}
    </div>
  )
}

const ImagesArea = styled.div`
  width: 600px;
  display: grid;
  grid-template-columns: 49% 49%;
  gap: 2%;
  width: 100%;

  @media (max-width: 767px) {
    width: 100%;
    grid-template-columns: 100%;
    height: 600px;
  }
`

const ImageWrapper = styled.div`
  position: relative;
  overflow: hidden;
  height: 200px;

  @media (max-width: 767px) {
    height: 100%;
  }
`