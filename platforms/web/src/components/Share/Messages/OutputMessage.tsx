import React, { useCallback, useState } from 'react'
import styled from 'styled-components'
import { FlexWrapper, colors, newColors, typo } from '@wrtn/ui'
import { ReactComponent as ChatWrtnLogo } from "@wrtn/ui/assets/wrtn/logo_chat_wrtn.svg";
import { OutputMessageType } from '@wrtn/core';
import CopyMessageButton from '../CopyMessageButton';
import MarkdonwWrapper from "../MarkdownWrapper";
import { RoundCarouselLeft, RoundCarouselRight } from '../RoundCarousel';

type Props = {
  messages: OutputMessageType;
}

export function OutputMessage({ messages }: Props) {
  const [currentIndex, setCurrentPage] = useState(0);
  if (messages.length < 1) return null;

  const isLastPage = useCallback((index: number) => index === messages.length - 1, [messages]);
  const isFirstPage = useCallback((index: number) => index === 0, []);

  const onClickPage = (isNext: boolean) => {
    if (isNext) {
      if (isLastPage(currentIndex)) return;
      setCurrentPage((page) => page+1);
    } else {
      if (isFirstPage(currentIndex)) return;
      setCurrentPage((page) => page-1);
    }
  }

  return (
    <Outline>
      <MessageWrapper>
        <LogoArea>
          <ChatLogo/>
        </LogoArea>
        <ContentArea>
          <MarkdonwWrapper text={messages[currentIndex].content}/>
        </ContentArea>
      </MessageWrapper>
      {messages.length > 1 && (
        <ArrowArea>
          <RoundCarouselLeft disabled={isFirstPage(currentIndex)} onClick={() => onClickPage(false)}/>
          <PageWrapper>{currentIndex+1} / {messages.length}</PageWrapper>
          <RoundCarouselRight disabled={isLastPage(currentIndex)} onClick={() => onClickPage(true)}/>
        </ArrowArea>
      )}
      <CopyMessageButton content={messages[currentIndex].content} />
    </Outline>
  )
}

export const Outline = styled(FlexWrapper)`
  border: 1px solid ${newColors.GRAY_200};
  background: ${newColors.BLUE_400_BG};
  border-radius: 8px;
  width: 760px;
  padding: 30px 40px 25px;
  flex-direction: column;
  gap: 20px;

  @media (max-width: 767px) {
    width: 100%;
  }
`

export const MessageWrapper = styled(FlexWrapper)`
  align-self: baseline;
  gap: 20px;
  width: 100%;
`

export const ContentArea = styled.div`
  overflow: hidden;
  width: inherit;

  ${typo({
    color: newColors.GRAY_700,
    size: "16px",
    weight: 600,
  })};

  line-height: 180%;
`

export const LogoArea = styled.div`
  width: 27px;
  height: 27px;
  svg {
    width: 100%;
    height: 100%;
  }
  align-self: flex-start;
`

export const IconArea = styled.div`
  align-self: flex-end;
`

export const ChatLogo = styled(ChatWrtnLogo)`
  width: 46px;
  height: 46px;
`;

export const ArrowArea = styled(FlexWrapper)`
  position: absolute;
  left: 10px;
  bottom: 10px;
  gap: 10px;
  align-items: center;

  ${typo({
    weight: 600,
    size: "16px",
    height: "160%",
    color: colors.GRAY_80,
  })};
`;

export const PageWrapper = styled.div`
  // margin-bottom: 7px;
`
