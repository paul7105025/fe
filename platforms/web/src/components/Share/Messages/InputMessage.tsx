import { InputMessageType } from '@wrtn/core'
import { FlexWrapper, Icon, newColors, typo } from '@wrtn/ui'
import React from 'react'
import styled from 'styled-components'
import CopyMessageButton from '../CopyMessageButton'
import MarkdonwWrapper from '../MarkdownWrapper'

type Props = {
  message: InputMessageType;
  userName: string;
}

export function InputMessage({ message, userName }: Props) {
  const isFileInput = message.type === "FileInput";
  const isSearchPlugin = message.type === "PluginInput" && message.pluginName === "search";

  return (
    <Outline>
      <MessageWrapper>
        <NiknameArea>{userName.slice(0, 1)}</NiknameArea>
        <ContentArea>
          {isFileInput && <div style={{ display: "flex", gap: "10px" }}>
            <Icon icon="paperclip" size={20}/>
            {message.content}
          </div>}
          {isFileInput || <MarkdonwWrapper text={`${isSearchPlugin ? "! " : ""}${message.content}`}/>}
        </ContentArea>
      </MessageWrapper>
      <CopyMessageButton content={message.content} />
    </Outline>
  )
}

const Outline = styled(FlexWrapper)`
  border: 1px solid ${newColors.GRAY_200};
  border-radius: 8px;
  width: 760px;
  padding: 30px 40px 25px;
  flex-direction: column;

  @media (max-width: 767px) {
    width: 100%;
  }
`

const MessageWrapper = styled(FlexWrapper)`
  width: 100%;
  align-self: baseline;
  gap: 20px;
  align-items: center;
`

const NiknameArea = styled.span`
  background-color: #C8CAFF;
  height: 28px;
  width: 28px;
  border-radius: 28px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  align-self: flex-start;
  
  ${typo({
    color: newColors.WHITE,
    size: "14px",
    weight: 700,
  })}
`

const ContentArea = styled.div`
  flex: 1;
  overflow: hidden;

  ${typo({
    color: newColors.GRAY_700,
    size: "16px",
    weight: 600,
  })}

  line-height: 180%;
`

const IconArea = styled.div`
  align-self: flex-end;
`