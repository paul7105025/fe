"use client"
import React, { useEffect, useState } from 'react'
import { useRecoilValue } from "recoil";

import { Icon } from '@wrtn/ui'
import { CircleButton } from './CircleButton'
import useInitAuth from 'src/hooks/auth/useInitAuth';
import { putShareChat, userState } from '@wrtn/core';

type Props = {
  chatId: string;
  userId: string;
  originalId: string;
}

export function TrashCircle({ chatId, originalId, userId }: Props) {
  useInitAuth({
    options: {
      updateUser: "unique"
    }
  });
  const user = useRecoilValue(userState);

  const onClickTrashButton = async () => {
    try {
      const res = await putShareChat(chatId, { isDeleted: true, originalId });
      if (res.data.data) {
        window.location.href = "/share";
      } else {
        throw new Error("cannot delete")
      }
    } catch(err) {
      console.log(err)
    }
  }

  if (!user || user._id !== userId) {
    return <></>
  }

  return (
    <div onClick={onClickTrashButton}>
      <CircleButton isClickable={true}><Icon icon="trash-alt" size={20} /></CircleButton>
    </div>
  )
}

