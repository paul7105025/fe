"use client"
import { useEffect, useState } from 'react'

import { putIncreaseView, useEvent } from '@wrtn/core';
import { Icon } from '@wrtn/ui'
import { CircleButton } from './CircleButton'
import { ModalBodyArea } from './ShareCircle';
import BasicModal from '../BasicModal';

type Props = {
  chatId: string;
  shareId: string;
  userId: string;
}

export function ViewCircle({ chatId, shareId, userId }: Props) {
  const [showModal, setShowModal] = useState(false);
  const [view, setView] = useState(0);
  const [toggle, setToggle] = useState(true);
  const { collectEvent } = useEvent();

  useEffect(() => {
    collectEvent("view_share_trend_post", {
      share_trend_post_id: shareId,
      share_trend_post_uploader: userId,
    });
  }, [])

  useEffect(() => {
    if (toggle) {
      increaseViewCount();
      setToggle(false);
    }
  }, [toggle])

  const increaseViewCount = async () => {
    try {
      const res = await putIncreaseView(chatId);
      setView(res.data.data);

      if (res.data.data === 1) {
        setShowModal(true);
      }
    } catch(err) {
      console.log(err)
    }
  }

  useEffect(() => {
    if (showModal) {
      setTimeout(() => {
        setShowModal(false);
      }, 1000)
    }
  }, [showModal])

  return (
    <>
      <CircleButton label={`${view}`}>
        <Icon icon="eye" size={20} />
      </CircleButton>
      <BasicModal isOpen={showModal} onCloseModal={() => setShowModal(false)} showCloseIcon={false}>
        <ModalBodyArea>
          <Icon icon="link" size={20}/>
          <span>링크가 복사되었습니다.</span>
        </ModalBodyArea>
      </BasicModal>
    </>
  )
}
