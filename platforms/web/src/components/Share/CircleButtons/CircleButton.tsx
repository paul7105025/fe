import { FlexWrapper, newColors, typo } from '@wrtn/ui';
import React from 'react'
import styled from 'styled-components'

type Props = {
  children: React.ReactNode;
  isClickable?: boolean;
  label?: string;
}

export function CircleButton({ children, isClickable, label }: Props) {
  return (
    <Outline>
      <CircleWrapper isClickable={!!isClickable}>
        {children}
      </CircleWrapper>
      {label}
    </Outline>
  )
}

const Outline = styled(FlexWrapper)`
  flex-direction: column;
  gap: 5px;

  ${typo({
    weight: 600,
    size: "14px",
    color: newColors.GRAY_700,
  })}
`

const CircleWrapper = styled.span<{ isClickable: boolean }>`
  background-color: white;
  border-radius: 24px;
  border: 1px solid ${newColors.GRAY_200};
  width: 48px;
  height: 48px;
  cursor: ${({ isClickable }) => isClickable ? "pointer" : "inherit"};

  display: flex;
  justify-content: center;
  align-items: center;
`