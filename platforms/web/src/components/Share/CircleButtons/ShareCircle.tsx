"use client"
import React, { useEffect } from 'react'

import { FlexWrapper, Icon, newColors, typo } from '@wrtn/ui'
import { CircleButton } from './CircleButton'
import BasicModal from '../BasicModal';
import styled from 'styled-components';
import { useEvent } from '@wrtn/core';

export function ShareCircle() {
  const [showModal, setShowModal] = React.useState(false);
  const { collectEvent } = useEvent();

  const onClickCopyLink = async () => {
    const originalUrl = window.location.href;
    await navigator.clipboard.writeText(originalUrl);
    setShowModal(true);

    const urls = originalUrl.split("/")
    collectEvent("click_share_trend_post_link_copy", {
      share_trend_post_id: urls.pop(),
    });
  }

  useEffect(() => {
    if (showModal) {
      setTimeout(() => {
        setShowModal(false);
      }, 1000)
    }
  }, [showModal])

  return (
    <>
      <div onClick={onClickCopyLink}>
        <CircleButton isClickable={true}><Icon icon="link" size={20}/></CircleButton>
      </div>
      <BasicModal isOpen={showModal} onCloseModal={() => setShowModal(false)} showCloseIcon={false}>
        <ModalBodyArea>
          <Icon icon="link" size={20}/>
          <span>링크가 복사되었습니다.</span>
        </ModalBodyArea>
      </BasicModal>
    </>
  )
}

export const ModalBodyArea = styled(FlexWrapper)`
  flex-direction: column;
  gap: 10px;

  > span {
    ${typo({
      weight: 700,
      size: "16px",
      color: newColors.GRAY_800,
    })}
  }
`
