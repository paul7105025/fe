"use client"
import { FilledHeart, Icon } from '@wrtn/ui'
import { CircleButton } from './CircleButton'
import { useLike } from 'src/hooks'

type Props = {
  chatId: string;
  likes: number;
}

export function HeartCircle({ chatId, likes: _likes }: Props) {

  const { likes, isLiked, onClickLike } = useLike({ chatId, likes: _likes });

  return (
    <div onClick={onClickLike}>
      <CircleButton label={`${likes}`} isClickable={true}>
        {isLiked ? (<FilledHeart />) : (<Icon icon="heart" size={20} />)}
      </CircleButton>
    </div>
  )
}
