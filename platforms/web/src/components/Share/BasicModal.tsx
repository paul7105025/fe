import styled from "styled-components";
import { colors, FlexWrapper, newColors } from "@wrtn/ui";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { CloseIcon } from "../Modal";

type Props = {
  children: React.ReactNode;
  isOpen: boolean;
  showCloseIcon?: boolean;
  onCloseModal(): void;
}

export default function BasicModal({ isOpen, children, showCloseIcon, onCloseModal }: Props) {
  return (
    <>
      {isOpen && (
        <ModalPortal onClose={onCloseModal}>
          <Wrapper justify="center" column>
            {showCloseIcon && <CloseIcon onClick={onCloseModal} />}
            <BodyArea>
              {children}
            </BodyArea>
          </Wrapper>
        </ModalPortal>
      )}
    </>
  )
}

const Wrapper = styled(FlexWrapper)`
  background-color: ${colors.WHITE};
  border-radius: 12px;
  padding: 32px 24px 24px;
  min-width: 320px;
  height: 118px;
  border: 1px solid ${newColors.GRAY_100}
`

const BodyArea = styled(FlexWrapper)`
  flex-direction: column;
`