"use client"
import { useEvent } from '@wrtn/core';
import { FlexWrapper, Icon, newColors } from '@wrtn/ui'
import { useSearchParams } from 'next/navigation';
import React, { useEffect, useState } from 'react'
import useQueryString from 'src/hooks/useQueryString';
import styled from 'styled-components'

export default function SearchInput() {
  const [searchKeyword, setSearchKeyword] = useState<string>("")
  const [keyword, setKeyword] = useState<string>("");
  const [isFocus, setIsFocus] = useState(false);

  const { collectEvent } = useEvent();

  const { onChangeQuery } = useQueryString();
  const searchParams = useSearchParams();


  useEffect(() => {
    const _keyword = searchParams.get("keyword");

    setKeyword(_keyword ?? "");
    setSearchKeyword(_keyword ?? "");
  }, [searchParams])

  const handleSearch = () => {
    onChangeQuery("keyword", keyword);
    setSearchKeyword(keyword);
  }

  const handleEmptySearch = () => {
    if (searchKeyword === "") {
      setKeyword("")
      return
    };
    onChangeQuery("keyword", "");
    setSearchKeyword("");

    collectEvent("search_share_trend", {
      share_keyword: keyword,
    })
  }

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      onChangeQuery("keyword", keyword);
      setSearchKeyword(keyword);

      collectEvent("search_share_trend", {
        share_keyword: keyword,
      })
    }

    if(e.key === "Backspace" && keyword === "") {
      onChangeQuery("keyword", "");
      setSearchKeyword("");
    }
  }

  return (
    <SearchInputWrapper isFocus={isFocus}>
      <input 
        style={{ width: "100%", height: "100%" }}
        placeholder="게시물 제목, 공유자 검색"
        value={keyword}
        onKeyDown={handleKeyDown}
        onChange={(e) => setKeyword(e.target.value)}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
      />
      {searchKeyword === "" && !isFocus && (
        <IconWrapper onClick={handleSearch}>
          <Icon icon="search" size={24} color={newColors.GRAY_300}/>
        </IconWrapper>
      )}
      {searchKeyword === "" && isFocus && (
        <IconWrapper onClick={handleSearch}>
          <Icon icon="search" size={24} color={newColors.PURPLE_500_PRIMARY}/>
        </IconWrapper>
      )}
      {searchKeyword !== "" && (
        <IconWrapper onClick={handleEmptySearch}>
          <Icon icon="multiply" size={20} color={newColors.GRAY_300}/>
        </IconWrapper>
      )}
    </SearchInputWrapper>
  )
}

const SearchInputWrapper = styled(FlexWrapper)<{ isFocus: boolean }>`
  border: ${({ isFocus }) => isFocus ? `1px solid ${newColors.PURPLE_500_PRIMARY}` : `1px solid ${newColors.GRAY_200}`};
  height: 48px;
  flex: 1;
  border-radius: 5px;
  padding: 10px;

  @media (min-width: 767px) {
    min-width: 500px;
  }
`

const IconWrapper = styled.div`
  cursor: pointer;
`