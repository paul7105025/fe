import styled from "styled-components";
import { IconRoundCarouselRightDouble, colors } from "@wrtn/ui";

type Props = {
  disabled: boolean;
  onClick(): void;
}

export function RoundCarouselRightDouble({ disabled, onClick }: Props) {
  return (
    <CarouselWrapper onClick={disabled ? undefined : onClick}>
      <IconRoundCarousel disabled={disabled}></IconRoundCarousel>
    </CarouselWrapper>
  )
}

const CarouselWrapper = styled.div`
  height: 24px;
`

const IconRoundCarousel = styled(IconRoundCarouselRightDouble)<{ disabled: boolean }>`
  cursor: ${({ disabled }) => disabled ? "default" : "pointer"};
  circle {
    stroke: ${({ disabled }) =>
      disabled ? colors.BLUE_GRAY_LINE : colors.gray_60};
  }
  path {
    fill: ${({ disabled }) =>
      disabled ? colors.BLUE_GRAY_LINE : colors.gray_60};
  }
`;