import styled from "styled-components";
import { IconRoundCarouselRight, colors } from "@wrtn/ui";

type Props = {
  disabled: boolean;
  onClick(): void;
}

export function RoundCarouselLeft({ disabled, onClick }: Props) {
  return (
    <CarouselWrapper onClick={disabled ? undefined : onClick}>
      <IconRoundCarouselLeft disabled={disabled}></IconRoundCarouselLeft>
    </CarouselWrapper>
  )
}

const CarouselWrapper = styled.div`
  height: 22px;
`

const IconRoundCarouselLeft = styled(IconRoundCarouselRight)<{ disabled: boolean }>`
  cursor: ${({ disabled }) => disabled ? "default" : "pointer"};
  transform: rotate(180deg);
  circle {
    stroke: ${({ disabled }) =>
      disabled ? colors.BLUE_GRAY_LINE : colors.gray_60};
  }
  path {
    fill: ${({ disabled }) =>
      disabled ? colors.BLUE_GRAY_LINE : colors.gray_60};
  }
`;
