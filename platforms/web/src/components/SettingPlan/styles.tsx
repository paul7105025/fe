import styled from "styled-components";

import { PlanType } from "@wrtn/core";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

// import { ReactComponent as IconCheck } from "../../assets/icons/icon_check_green.svg";
import { IconCheckGreen } from "@wrtn/ui/assets";

export const BoldText = styled.p`
  ${typo({ weight: 700, size: "16px", height: "160%", color: "#414141" })};
  /* margin-bottom: 7px; */
`;

export const InfoChecker = (info: boolean) => {
  if (info) return <IconCheckGreen />;
  else return <BoldText>-</BoldText>;
};

export interface PlanElementProps {
  currentPlan: PlanType;
}
