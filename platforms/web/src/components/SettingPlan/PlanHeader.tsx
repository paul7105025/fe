import styled from "styled-components";

import { PlanType, PLAN_INFO, discount, comma } from "@wrtn/core";

import { PlanPriceLabel } from "@wrtn/ui/components/PlanLabel";

import { FlexWrapper, typo, colors, FlexButton } from "@wrtn/ui/styles";

import { ArrowShort } from "@wrtn/ui/assets/SVGComponent/Icon";
import { useRouter } from "next/navigation";

interface PlanHeaderProps {
  currentPlan: PlanType;
  plan: PlanType;
}

const buttonLabel = (currentPlan: PlanType, plan: PlanType) => {
  if (currentPlan === plan) {
    return "이용 중";
  }
  if (plan === "PLUS") {
    return "이 요금제로 업그레이드";
  }
  return "-";
};

export const PlanHeader = ({ currentPlan, plan }: PlanHeaderProps) => {
  const router = useRouter();

  const navigateToUpgrade = () => {
    router.push(PLAN_INFO[plan].href);
  };

  return (
    <HeaderWrapper column>
      <HeaderColorHead color={PLAN_INFO[plan].color} />
      <InnerWrapper column>
        <PlanPriceLabel type={plan} />
        <HeaderName>{PLAN_INFO[plan].name} 요금제</HeaderName>
        <HeaderDesc>{PLAN_INFO[plan].desc}</HeaderDesc>
        <HeaderOriginalPriceWrapper isHidden={plan === "FREE"}>
          <HeaderOriginalPrice>
            {comma(PLAN_INFO[plan].originalPrice)}원
          </HeaderOriginalPrice>
          <PlanDiscount>
            {discount(PLAN_INFO[plan].price, PLAN_INFO[plan].originalPrice)}%
            <ArrowShort />
          </PlanDiscount>
        </HeaderOriginalPriceWrapper>
        <HeaderPriceWrapper>
          <HeaderPrice>{comma(PLAN_INFO[plan].price)}원</HeaderPrice>
          <HeaderPriceUnit>{PLAN_INFO[plan].priceUnit}</HeaderPriceUnit>
        </HeaderPriceWrapper>
        <HeaderLinkButton
          onClick={navigateToUpgrade}
          disabled={plan === currentPlan || currentPlan === "PLUS"}
          isHidden={currentPlan === "PLUS" && plan === "FREE"}
          color={PLAN_INFO[plan].color}
        >
          {buttonLabel(currentPlan, plan)}
        </HeaderLinkButton>
      </InnerWrapper>
    </HeaderWrapper>
  );
};

const HeaderWrapper = styled(FlexWrapper)`
  width: 100%;
  border-bottom: 1px solid ${colors.BLUE_GRAY_LINE};
`;

const InnerWrapper = styled(FlexWrapper)`
  padding: 0px 18px 16px;
  width: 100%;
`;

const HeaderColorHead = styled.div<{ color: string }>`
  width: 100%;
  height: 16px;

  background-color: ${({ color }) => color};
  margin-bottom: 24px;
`;

const HeaderName = styled.div`
  ${typo({ weight: 700, size: "20px", color: colors.GRAY_90 })};
  margin-top: 18px;
  margin-bottom: 13px;
`;

const HeaderDesc = styled.p`
  ${typo({ weight: 600, size: "16px", height: "160%", color: colors.GRAY_70 })};
  margin-bottom: 19px;

  text-align: center;
  white-space: pre-line;
`;

const HeaderOriginalPriceWrapper = styled(FlexWrapper)<{ isHidden: boolean }>`
  gap: 8px;
  opacity: ${({ isHidden }) => (isHidden ? 0 : 1)};
  visibility: ${({ isHidden }) => (isHidden ? "hidden" : "visible")};
  margin-bottom: 14px;
`;

const HeaderOriginalPrice = styled.span`
  ${typo({ weight: 600, size: "16px", color: colors.GRAY_60 })};
  text-decoration: line-through;
`;

const PlanDiscount = styled.span`
  ${typo({ weight: 700, size: "14px", color: colors.WHITE })};
  background-color: #fb533d;
  border-radius: 10px;
  padding: 3px 20px 3px 7px;
  position: relative;
  svg {
    position: absolute;
    top: 0;
    right: 3px;
  }
`;

const HeaderPriceWrapper = styled(FlexWrapper)`
  align-items: flex-end;
  gap: 12px;
  margin-bottom: 27px;
`;

const HeaderPrice = styled.span`
  ${typo({ weight: 700, size: "28px", color: colors.GRAY_90 })};
`;

const HeaderPriceUnit = styled.span`
  ${typo({ weight: 600, size: "12px", color: colors.GRAY_58 })};
`;

const HeaderLinkButton = styled(FlexButton)<{
  color: string;
  isHidden: boolean;
}>`
  ${typo({ weight: 700, size: "14px", color: colors.WHITE })};
  padding: 13px 0px;
  width: 100%;

  background-color: ${({ color }) => color};
  border-radius: 8px;

  opacity: ${({ isHidden }) => (isHidden ? 0 : 1)};
  /* visibility: ${({ isHidden }) => (isHidden ? "hidden" : "visible")}; */

  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
`;
