import React from "react";
import styled from "styled-components";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

import { IconChevron } from '@wrtn/ui/assets'

const questionList = [
  {
    q: "요금제가 어떻게 구성되어 있나요?",
    a: (
      <>
        뤼튼의 요금제는 무료, 플러스 두 가지의 <b>월 구독제</b>로
        구성되어있습니다. 구독을 유지할 경우 다음 달에 자동으로 갱신됩니다.
      </>
    ),
  },
  {
    q: "무료와 유료 무제한 요금제의 차이가 궁금해요. ",
    a: (
      <>
        뤼튼은 더 많은 사용자님께 좋은 생성 경험을 제공하기 위해, 무료와 유료
        요금제 모두 생성 횟수에 제한을 두고 있지 않습니다. 그러나 무료 요금제의
        경우 비용문제로 부득이하게 속도에 제한이 있을 수 있습니다. 보다 쾌적한
        생성 경험과 부가 기능을 원하신다면 유료 요금제를 선택해주세요. 더 자세한
        차이는 상단 표를 참조해주세요.
      </>
    ),
  },
  {
    q: "뤼튼으로 만든 결과물의 저작권은 누구의 것인가요?",
    a: `결과물의 사용 권한은 사용자에게 귀속되며, 개인적 용도 및 상업적 용도로 활용할 수 있습니다. 따라서 생성된 결과물을 활용하며 발생하는 문제의 책임 또한 사용자에게 있습니다. 다만 현재 AI를 통해 생성된 창작물의 저작권에 대해서는 정확한 법률적 제도가 마련되어있지 않은 점 참고 부탁드립니다.`,
  },
  {
    q: "뤼튼으로 만든 결과물은 어떤 데이터로 이루어져 있나요?",
    a: `뤼튼은 초거대 생성 AI(Generative AI)를 기반으로 만들어졌으며, 사용자가 입력한 내용을 토대로 매번 새로운 문장을 생성해 냅니다. 모델의 크기가 매우 크기에 학습된 데이터를 그대로 뱉을 확률이 거의 없습니다. 문장을 생성하는 과정에서 경우에 따라 사실과 다른 내용이 나올 수 있습니다.`,
  },
  {
    q: "뤼튼으로 만든 결과물의 내용을 신뢰할 수 있나요?",
    a: `결과물은 사용자가 입력한 내용을 최우선으로 반영하며, 경우에 따라 문장을 생성하는 과정에서 사실과 다른 내용이 나올 수 있습니다. 생성된 글 이용 시 사실 관계 여부를 확인하시는 것을 권장합니다.`,
  },
];

const FQAQuestionItem = ({
  q = "",
  a,
}: {
  q: string;
  a: React.ReactNode | string;
}) => {
  const [isCollapsed, setIsCollapsed] = React.useState(true);
  return (
    <FAQListItem>
      <FAQHeader onClick={() => setIsCollapsed((c) => !c)}>
        <FAQQuestion>{q}</FAQQuestion>
        <IconArrow isCollapsed={isCollapsed} />
      </FAQHeader>
      {!isCollapsed && <FAQAnswer>{a}</FAQAnswer>}
    </FAQListItem>
  );
};

export const PlanFAQ = () => {
  return (
    <Wrapper column>
      {questionList.map((item, idx) => {
        return <FQAQuestionItem key={idx} q={item.q} a={item.a} />;
      })}
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  width: 100%;
`;

const FAQListItem = styled.div`
  margin-top: 24px;
  padding-bottom: 20px;

  width: 100%;

  border-bottom: 1px solid ${colors.GRAY_90};
`;

const FAQHeader = styled.div`
  height: 28px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  cursor: pointer;
  @media (max-width: 767px) {
    align-items: flex-start;
  }
`;

const FAQQuestion = styled.h6`
  flex: 1;

  ${typo({
    size: "20px",
    weight: 700,
    color: colors.GRAY_90,
  })}

  @media (max-width: 767px) {
    font-size: 16px;
  }
  user-select: none;
`;

const FAQAnswer = styled.p`
  ${typo({
    size: "16px",
    weight: 400,
    height: "160%",
    color: colors.GRAY_90,
  })}

  padding-top: 16px;
`;

const IconArrow = styled(IconChevron)<{ isCollapsed: boolean }>`
  cursor: pointer;
  transform: ${({ isCollapsed }) =>
    isCollapsed ? "rotate(180deg)" : "rotate(0deg)"};
  transition: transform 0.3s ease-in-out;

  path {
    fill: ${colors.GRAY_90};
  }
`;
