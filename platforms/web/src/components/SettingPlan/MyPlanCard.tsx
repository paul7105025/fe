import React from "react";
import dayjs from "dayjs";
import styled from "styled-components";

import { colors, typo, FlexWrapper } from "@wrtn/ui";

import { PLAN_NAME, postUserSubscribe, userType } from "@wrtn/core";

import { useRefresh, useEvent } from "@wrtn/core/hooks";

import { Dialog } from "@wrtn/ui/components/Dialog";
import { PlanLabel } from "@wrtn/ui/components/PlanLabel";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import { CancelSubscribeDialogContainer } from "../../containers/DialogContainer";

interface MyPlanCardProps {
  user: userType;
}

export const MyPlanCard = ({ user }: MyPlanCardProps) => {
  const { plan, nextPlan, paymentDate, dueDate } = user;

  const [openStopSub, setOpenStopSub] = React.useState(false);
  const [openStartSub, setOpenStartSub] = React.useState(false);

  const { fetchUser, fetchMetric } = useRefresh();
  const { collectEvent } = useEvent();

  const closeStopSub = () => {
    setOpenStopSub(false);
    fetchUser();
  };
  const closeStartSub = () => setOpenStartSub(false);

  const getNextPlanLabel = () => {
    if (nextPlan === plan) {
      return "";
    } else if (nextPlan === "FREE") {
      return "구독 취소 됨";
    } else {
      if (nextPlan === "PLUS") {
        return "요금제 변경 예정";
      }
    }
  };

  const handleResume = async () => {
    const res = await postUserSubscribe();
    if (res?.status === 201) {
      fetchMetric();
      fetchUser();
      setOpenStartSub(false);
      collectEvent("click_change_plan_btn", {
        current_plan: plan,
        change_plan: nextPlan,
        is_upgrade: false,
      });
    }
  };

  return (
    <>
      <MyPlanHeader>
        <LabelWrapper>
          <PlanLabel type={plan} />
          <PlanLabelText>{PLAN_NAME[plan]}</PlanLabelText>
          <PlanStateLabel>{getNextPlanLabel()}</PlanStateLabel>
        </LabelWrapper>
        {plan !== "FREE" && (
          <FlexWrapper style={{ gap: "19px" }}>
            <Anchor
              onClick={() => {
                if (plan === nextPlan) {
                  setOpenStopSub(true);
                  collectEvent("click_cancel_purchase");
                } else {
                  setOpenStartSub(true);
                }
              }}
            >
              {plan === nextPlan ? "구독 취소" : "구독 재개"}
            </Anchor>
          </FlexWrapper>
        )}
      </MyPlanHeader>
      <PlanSectionBoxDivider />
      <MyPlanContent>
        <MyPlanTextWrapper>
          <MyPlanTitle>구독 시작일</MyPlanTitle>
          <MyPlanText>
            {dayjs(paymentDate).format("YYYY년 MM월 DD일")}
          </MyPlanText>
        </MyPlanTextWrapper>
        <MyPlanTextWrapper>
          <MyPlanTitle>구독 종료일</MyPlanTitle>
          <MyPlanText>
            {plan !== nextPlan && dueDate
              ? dayjs(dueDate).format("YYYY년 MM월 DD일")
              : "-"}
          </MyPlanText>
        </MyPlanTextWrapper>
        <MyPlanTextWrapper>
          <MyPlanTitle>요금제</MyPlanTitle>
          <MyPlanText>뤼튼 {PLAN_NAME[plan]} 요금제</MyPlanText>
        </MyPlanTextWrapper>
        {plan !== nextPlan && (
          <MyPlanInnerContent>
            <MyPlanTextWrapper>
              <MyPlanTitle>요금제 변경</MyPlanTitle>
              <MyPlanText>뤼튼 {PLAN_NAME[nextPlan]} 요금제</MyPlanText>
            </MyPlanTextWrapper>
            <MyPlanTextWrapper>
              <MyPlanTitle>변경 예정일</MyPlanTitle>
              <MyPlanText>
                {dueDate ? dayjs(dueDate).format("YYYY년 MM월 DD일") : "-"}
              </MyPlanText>
            </MyPlanTextWrapper>
          </MyPlanInnerContent>
        )}
      </MyPlanContent>
      {openStopSub && <CancelSubscribeDialogContainer onClose={closeStopSub} />}
      {openStartSub && (
        <ModalPortal>
          <Dialog
            handleClose={closeStartSub}
            title="구독을 재개하시겠어요?"
            description={[
              "구독 취소 혹은 변경 예정된 요금제를 취소하고",
              "기존 요금제로 구독을 재개합니다.",
            ]}
            rightButtonLabel="재개하기"
            iconType="laugh"
            handleRightButton={handleResume}
          />
        </ModalPortal>
      )}
    </>
  );
};

const PlanStateLabel = styled.div`
  ${typo({ weight: 600, size: "14px" })};

  color: ${colors.gray_60};
  margin-left: 3px;
`;

export const PlanLabelText = styled.p`
  ${typo({ weight: 600, size: "16px", height: "100%", color: colors.gray_80 })};
`;

export const PlanSectionBoxDivider = styled.div`
  position: absolute;
  width: 100%;
  height: 1px;
  background-color: ${colors.BLUE_GRAY_LINE};
  margin-left: -22px;
`;

export const MyPlanHeader = styled(FlexWrapper)`
  padding-bottom: 22px;
  justify-content: space-between;
`;

export const MyPlanContent = styled(FlexWrapper)`
  padding-top: 20px;
  flex-direction: column;
  align-items: flex-start;
  gap: 10px;
`;

const MyPlanInnerContent = styled(MyPlanContent)`
  border-top: 1px solid ${colors.BLUE_GRAY_LINE};
  width: 100%;
`;

export const MyPlanTextWrapper = styled(FlexWrapper)``;

export const MyPlanText = styled.p`
  ${typo({ weight: 500, size: "12px", height: "100%", color: colors.gray_80 })};
`;

export const MyPlanTitle = styled.p`
  ${typo({ weight: 500, size: "12px", height: "100%", color: colors.gray_60 })};
  width: 75px;
`;

export const LabelWrapper = styled(FlexWrapper)`
  justify-content: flex-start;
  gap: 6px;
`;

export const Anchor = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.gray_60,
  })};
  text-decoration-line: underline;
  cursor: pointer;
`;
