import styled from "styled-components";

import { PlanType, PLAN_INFO } from "@wrtn/core";

import { FlexWrapper, colors, typo } from "@wrtn/ui/styles";
import {
  BoldText,
  InfoChecker,
  InfoGuide,
  PlanElementProps,
  PlanHeader,
} from ".";
import { IconCheckGreen } from "@wrtn/ui";

const data = {
  FREE: [
    "무제한 채팅 가능",
    "60개 이상의 툴(마케팅, 쇼핑몰, 학생, 업무용, 컨텐츠 등)\n무제한 생성 가능",
    "긴 문서 작업에 특화된 에디터 무제한 사용 가능",
    "채팅에서 무제한 템플릿 등록 및 사용 가능",
    "다양한 생성AI 모델 제공 (계속 추가 예정)",
    ""
  ],
  PLUS: [
    "무료 요금제에서 제공되는 모든 기능",
    "더 빠른 생성 속도",
    "한 번에 3개까지 더 많은 생성 결과물을 받아볼 수 있음",
    "생성 이력 관리 기한 7일 →90일로 연장",
    "생성 이력 다운로드 가능",
    "CS 우선권",
  ],
};

const SingleTable = ({
  plan,
  currentPlan,
}: {
  plan: PlanType;
  currentPlan: PlanType;
}) => {
  return (
    <SingleTableWrapper column>
      <PlanHeader currentPlan={currentPlan} plan={plan} />

      {data[plan].map((v, i) => (
        <Row isLast={i === data[plan].length - 1}>
          <div style={{width: '38px'}}>
          { v.length > 0  && <IconCheckGreen width={38}/> }
          </div>
          <InfoData>{v}</InfoData>
        </Row>
      ))}

      {/* <Row
        style={{
          alignItems: "flex-start",
          borderTop: `1px solid ${colors.BLUE_GRAY_LINE}`,
        }}
      >
        <LabelData>월 제공량</LabelData>
        <InfoData>
          <BoldText>무제한</BoldText>
          {PLAN_INFO[plan].amountCondition}
        </InfoData>
      </Row>
      <Row>
        <LabelData>
          1회 생성 결과 수
          <InfoGuide
            info={`자동 생성 버튼을 1회 누를 때 한 번에\n생성 가능한 결과물의 최대 개수`}
          />
        </LabelData>
        <InfoData>
          <BoldText>최대 {PLAN_INFO[plan].maxGenerate}개</BoldText>
        </InfoData>
      </Row>
      <Row>
        <LabelData>
          생성 이력 관리 기한
          <InfoGuide
            info={`히스토리 메뉴에서 생성한 결과물을\n보관/열람할 수 있는 기한`}
          />
        </LabelData>
        <InfoData>
          <BoldText>{PLAN_INFO[plan].historyDeadline}일</BoldText>
        </InfoData>
      </Row>
      <Row>
        <LabelData>CS 우선권</LabelData>
        <InfoData>{InfoChecker(PLAN_INFO[plan].csPreference)}</InfoData>
      </Row>
      <Row>
        <LabelData>새 기능 우선 접근권</LabelData>
        <InfoData>{InfoChecker(PLAN_INFO[plan].newFeaturePreference)}</InfoData>
      </Row>
      <Row>
        <LabelData>
          생성 이력 다운로드
          <InfoGuide
            info={`히스토리 메뉴에서 생성한 결과물을\n엑셀 파일로 다운받는 기능`}
          />
        </LabelData>
        <InfoData>{InfoChecker(PLAN_INFO[plan].downloadAvailable)}</InfoData>
      </Row>
      <Row>
        <LabelData>
          결과물 수정 기능
          <InfoGuide
            info={`생성한 결과물을 그 자리에서\n바로 수정할 수 있는 기능`}
          />
        </LabelData>
        <InfoData>{InfoChecker(PLAN_INFO[plan].modifyAvailable)}</InfoData>
      </Row> */}
    </SingleTableWrapper>
  );
};

export const PlanMobile = ({ currentPlan = "FREE" }: PlanElementProps) => {
  return (
    <MobileWrapper>
      <SingleTable plan="FREE" currentPlan={currentPlan} />
      <SingleTable plan="PLUS" currentPlan={currentPlan} />
    </MobileWrapper>
  );
};

const MobileWrapper = styled(FlexWrapper)`
  width: 100%;
  height: calc(100% - 34px);

  align-items: flex-start;
  gap: 20px;

  @media (max-width: 767px) {
    max-width: 95vw;
    flex-direction: column;

    gap: 30px;

    height: 100%;
  }
`;

const SingleTableWrapper = styled(FlexWrapper)`
  width: 100%;
  max-width: 90vw;
  height: 100%;

  overflow: hidden;

  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 12px;
`;

const Row = styled(FlexWrapper)<{ isLast: boolean }>`
  width: 100%;
  padding: 12px 24px;

  gap: 16px;

  justify-content: flex-start;
  align-items: center;

  border-bottom: ${({ isLast }) =>
    isLast ? "none" : `1px dashed ${colors.BLUE_GRAY_LINE}`};
`;

const InfoData = styled.p`
  ${typo({ weight: 600, size: "16px", height: "120%", color: colors.GRAY_90 })};

  text-align: left;
  white-space: pre-line;
`;
