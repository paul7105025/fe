import React from "react";
import styled from "styled-components";

import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import Image from "next/image";

export const PlanBanner = () => {
  return (
    <Wrapper column>
      <ImageWrapper>
        <Image
          src="/plan_banner.png"
          alt="plan_banner"
          fill
          style={{ objectFit: "cover" }}
        />
      </ImageWrapper>
      <SubTitle>뤼튼 컨설팅 프로그램</SubTitle>
      <Title>
        뤼튼의 <span>AI기술</span>을 도입하고 싶다면?
      </Title>
      <Description>출시 예정일</Description>
      <DateText>2023년 4월</DateText>
      <InvitationButton>준비 중</InvitationButton>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  padding: 32px 43px;
  border-radius: 12px;
  background-size: cover;

  width: 100%;

  justify-content: flex-start;
  align-items: flex-start;

  overflow: hidden;

  @media (max-width: 767px) {
    margin-top: 45px;
  }
`;

const ImageWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  z-index: -1;
`;

const SubTitle = styled.p`
  ${typo({ size: "16px", weight: 600, color: colors.WHITE })}
`;

const Title = styled.p`
  margin-top: 1px;
  ${typo({ size: "30px", weight: 900, height: "140%", color: colors.WHITE })}

  span {
    color: #c0a3ff;
  }
`;

const Description = styled.p`
  margin-top: 26px;

  ${typo({ size: "16px", weight: 700, color: "#c0a3ff" })}
`;

const DateText = styled.p`
  margin-top: 10px;
  ${typo({ size: "16px", weight: 600, color: colors.WHITE })}
`;

const InvitationButton = styled(FlexButton)`
  position: absolute;
  right: 28px;
  bottom: 22px;

  ${typo({ size: "18px", weight: 600, color: colors.POINT_PURPLE })}

  border-radius: 12px;
  padding: 13px 28px;

  background-color: ${colors.WHITE};

  cursor: pointer;
`;
