import React from "react";
import styled from "styled-components";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { Help } from "@wrtn/ui/assets/SVGComponent/Icon";

import { FlexWrapper, colors, typo } from "@wrtn/ui/styles";

export const InfoGuide = ({ info }: { info: string }) => {
  const [isHover, setIsHover] = React.useState(false);

  const ref = React.useRef<HTMLDivElement>(null);

  const handleMouseEnter = () => {
    setIsHover(true);
  };

  const handleMouseOut = () => {
    setIsHover(false);
  };

  return (
    <GuideWrapper
      ref={ref}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseOut}
    >
      <HelpIcon />
      {isHover && (
        <ModalPositionPortal
          position={{
            left: ref.current && ref.current.getBoundingClientRect().left + 25,
            top: ref.current && ref.current.getBoundingClientRect().top + 10,
          }}
        >
          <InfoText>{info}</InfoText>
        </ModalPositionPortal>
      )}
    </GuideWrapper>
  );
};

const GuideWrapper = styled(FlexWrapper)`
  position: relative;
`;

const InfoText = styled.div`
  width: fit-content;
  transform: translateY(-50%);
  white-space: nowrap;

  background: ${colors.GRAY_80};

  box-shadow: 0px 0px 5px rgba(56, 71, 130, 0.25);
  border-radius: 5px;

  ${typo({ weight: 600, size: "12px", height: "130%", color: colors.WHITE })};

  text-align: center;

  padding: 4px 9px;
  z-index: 100;

  white-space: pre-line;
`;

const HelpIcon = styled(Help)`
  width: 20px;
  height: 20px;

  path {
    fill: ${colors.GRAY_58};
  }
`;
