import React from "react";
import styled from "styled-components";

import { PLAN_INFO } from "@wrtn/core";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

import {
  InfoChecker,
  InfoGuide,
  PlanHeader,
  PlanElementProps,
  BoldText,
} from ".";

export const PlanTable = ({ currentPlan = "FREE" }: PlanElementProps) => {
  return (
    <Wrapper>
      <TableWrapper>
        <thead>
          <tr>
            <EmptyHeader>
              <div>
                
              </div>
            </EmptyHeader>
            <Header>
              <PlanHeader currentPlan={currentPlan} plan="FREE" />
            </Header>
            <Header>
              <PlanHeader currentPlan={currentPlan} plan="PLUS" />
            </Header>
          </tr>
        </thead>
        <tbody>
          <tr>
            <LabelRow>월 제공량</LabelRow>
            <DataRow
              style={{
                padding: "15px 32px",
              }}
            >
              <BoldText
                style={{
                  marginBottom: "7px",
                }}
              >
                무제한
              </BoldText>
              {PLAN_INFO.FREE.amountCondition}
            </DataRow>
            <DataRow
              style={{
                padding: "15px 32px",
              }}
            >
              <BoldText
                style={{
                  marginBottom: "7px",
                }}
              >
                무제한
              </BoldText>
              {PLAN_INFO.PLUS.amountCondition}
            </DataRow>
          </tr>
          <tr>
            <LabelRow>
              1회 생성 결과 수
              <InfoGuide
                info={`자동 생성 버튼을 1회 누를 때 한 번에\n생성 가능한 결과물의 최대 개수`}
              />
            </LabelRow>
            <DataRow>
              <BoldText>최대 {PLAN_INFO.FREE.maxGenerate}개</BoldText>
            </DataRow>
            <DataRow>
              <BoldText>최대 {PLAN_INFO.PLUS.maxGenerate}개</BoldText>
            </DataRow>
          </tr>
          <tr>
            <LabelRow>
              생성 이력 관리 기한
              <InfoGuide
                info={`히스토리 메뉴에서 생성한 결과물을\n보관/열람할 수 있는 기한`}
              />
            </LabelRow>
            <DataRow>
              <BoldText>{PLAN_INFO.FREE.historyDeadline}일</BoldText>
            </DataRow>
            <DataRow>
              <BoldText>{PLAN_INFO.PLUS.historyDeadline}일</BoldText>
            </DataRow>
          </tr>
          <tr>
            <LabelRow>CS 우선권</LabelRow>
            <DataRow>{InfoChecker(PLAN_INFO.FREE.csPreference)}</DataRow>
            <DataRow>{InfoChecker(PLAN_INFO.PLUS.csPreference)}</DataRow>
          </tr>
          <tr>
            <LabelRow>새 기능 우선 접근권</LabelRow>
            <DataRow>
              {InfoChecker(PLAN_INFO.FREE.newFeaturePreference)}
            </DataRow>
            <DataRow>
              {InfoChecker(PLAN_INFO.PLUS.newFeaturePreference)}
            </DataRow>
          </tr>
          <tr>
            <LabelRow>
              생성 이력 다운로드
              <InfoGuide
                info={`히스토리 메뉴에서 생성한 결과물을\n엑셀 파일로 다운받는 기능`}
              />
            </LabelRow>
            <DataRow>{InfoChecker(PLAN_INFO.FREE.downloadAvailable)}</DataRow>
            <DataRow>{InfoChecker(PLAN_INFO.PLUS.downloadAvailable)}</DataRow>
          </tr>
          <tr>
            <LabelRow>
              결과물 수정 기능
              <InfoGuide
                info={`생성한 결과물을 그 자리에서\n바로 수정할 수 있는 기능`}
              />
            </LabelRow>
            <DataRow>{InfoChecker(PLAN_INFO.FREE.modifyAvailable)}</DataRow>
            <DataRow>{InfoChecker(PLAN_INFO.PLUS.modifyAvailable)}</DataRow>
          </tr>
        </tbody>
      </TableWrapper>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  overflow: hidden;

  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 12px;
`;

const TableWrapper = styled.table`
  border-collapse: collapse;

  width: 100%;

  table-layout: fixed;

  thead {
    min-height: 266px;
    border-bottom: 1px solid ${colors.BLUE_GRAY_LINE};

    tr {
      width: 100%;
    }
  }
  tbody {
    tr {
      min-height: 56px;

      border-bottom: 1px dashed ${colors.BLUE_GRAY_LINE};
    }
  }
`;

const EmptyHeader = styled.th`
  width: 200px;
`;

const Header = styled.th`
  border-left: 1px solid ${colors.BLUE_GRAY_LINE};
`;

const LabelRow = styled.td`
  padding: 20px 17px 20px 32px;

  display: flex;
  align-items: center;
  gap: 6px;
  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: colors.GRAY_80,
  })};
`;

const DataRow = styled.td`
  text-align: center;
  gap: 7px;

  padding: 9px 32px;

  border-left: 1px solid ${colors.BLUE_GRAY_LINE};

  ${typo({ weight: 600, size: "14px", height: "160%", color: colors.GRAY_70 })};

  white-space: pre-line;
`;
