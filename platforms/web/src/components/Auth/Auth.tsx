import { useEvent, useLoginDialog } from "@wrtn/core";
import { colors, FlexWrapper, LogoEmailRound, newColors, typo } from "@wrtn/ui";
import Link from "next/link";
import { useRouter } from "next/router";
import { paths } from "src/constants";
import { useSocialLogin } from "src/hooks/auth";
import styled from "styled-components";
import LoginProviderButton from "../Button";

export const Auth = ({ position = "modal" }) => {
  const { handleClose } = useLoginDialog();

  const {
    handleGoogleLoginSuccess,
    handleKakaoLoginSuccess,
    handleNaverLoginSuccess,
    handleAppleLoginSuccess,
    handleGoogleLoginFailure,
    handleNaverLoginFailure,
    handleKakaoLoginFailure,
    handleAppleLoginFailure,
  } = useSocialLogin(position);

  const { collectEvent } = useEvent();

  const router = useRouter();

  return (
    <>
      <Text>SNS 또는 이메일로 로그인/회원가입</Text>
      <Gap />
      <AuthLoginButtons>
        <RoundButton style={{ transform: "scale(1.375) translate(6px, 7px)" }}>
          <LoginProviderButton.Google
            size={"large"}
            type="icon"
            shape="circle"
            onSuccess={handleGoogleLoginSuccess}
            onError={handleGoogleLoginFailure}
            width={"300px"}
          />
        </RoundButton>
        <RoundButton>
          <LoginProviderButton.NaverRound
            onSuccess={handleNaverLoginSuccess}
            onFailure={handleNaverLoginFailure}
            callbackUrl={
              process.env.NEXT_PUBLIC_NAVER_REDIRECT_URL || window.location.href
            }
          />
        </RoundButton>
        <RoundButton>
          <LoginProviderButton.KakaoRound
            onSuccess={handleKakaoLoginSuccess}
            onFail={handleKakaoLoginFailure}
          />
        </RoundButton>
        <RoundButton>
          <LoginProviderButton.Apple
            isDefaultStyle={false}
            onSuccess={handleAppleLoginSuccess}
            onFail={handleAppleLoginFailure}
          />
        </RoundButton>
        <RoundButton
          onClick={() => {
            collectEvent("click_start_platform_btn", {
              platform: "wrtn",
              position: position,
            });
            handleClose();
            router.push({
              pathname: paths.login.local.index(),
              query: router.query
            });
          }}
        >
          <LogoEmailRound />
        </RoundButton>
      </AuthLoginButtons>
      <Text2Wrapper row>
          <Text2 href="https://wrtn.ai/terms">이용약관</Text2>
          <Divider  />
          <Text2 href="https://wrtn.ai/privacy">개인정보처리방침</Text2>
      </Text2Wrapper>
    </>
  );
};

export default Auth;

const Gap = styled.div`
  min-height: 16px;
`;

const AuthLoginButtons = styled(FlexWrapper)`
  gap: 13px;

  @media (max-width: 767px) {
    flex-wrap: wrap;
    justify-content: center;
  }
`;

const RoundButton = styled.div`
  width: 55px;
  height: 55px;
  border-radius: 100%;
  cursor: pointer;
`;

const Text = styled.div`
  ${typo({
    weight: 500,
    size: "12px",
    height: "100%",
    color: newColors.GRAY_500,
  })};
`;

const Text2Wrapper = styled(FlexWrapper)`
  padding-top: 40px;
  gap: 10.5px;
  align-items: center;
`;

const Text2 = styled.a`
  ${typo({
    weight: 500,
    size: "14px",
    height: "100%",
    color: newColors.GRAY_500,
  })};
  
  &:hover {
    text-decoration: underline;
  }
`;

const Divider = styled.div`
  display: flex;
  width: 1px;
  height: 14px;
  background-color: ${newColors.GRAY_100};
`;
