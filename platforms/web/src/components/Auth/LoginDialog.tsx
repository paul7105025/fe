import React from "react";
import { useRouter } from "next/router";
import { useGoogleLogin } from "@react-oauth/google";

import {
  colors,
  FlexWrapper,
  LogoEmailRound,
  LogoGoogleRound,
  LogoKakaoRound,
  LogoNaverRound,
  typo,
} from "@wrtn/ui";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";

import Auth from "./Auth";

import { paths } from "src/constants";
import { CloseIcon } from "src/containers/LoginDialog/styles";
import { useSocialLogin } from "src/hooks/auth";

import styled from "styled-components";
import LoginProviderButton from "../Button";
import { LogoWrtnPurple } from "@wrtn/ui/assets";

export const LoginMainDialog = ({ onClose, isOpen }) => {
  const [init, setInit] = React.useState(false);

  React.useEffect(() => {
    setInit(true);
  }, []);

  const {
    handleGoogleLoginSuccess,
    handleKakaoLoginSuccess,
    handleNaverLoginSuccess,
    handleGoogleLoginFailure,
    handleNaverLoginFailure,
    handleKakaoLoginFailure,
  } = useSocialLogin();

  const clickGoogleLogin = useGoogleLogin({
    // @ts-ignore
    onSuccess: (tokenResponse) => handleGoogleLoginSuccess(tokenResponse),
    onError: handleGoogleLoginFailure,
  });

  const router = useRouter();

  return (
    <>
      {init && (
        <HiddenNaver>
          {/* INFO: 네이버 스크립트 초기화 용도입니다. 지우면 안됩니다. */}
          <LoginProviderButton.NaverRound
            onSuccess={handleNaverLoginSuccess}
            onFailure={handleNaverLoginFailure}
            callbackUrl={
              process.env.NEXT_PUBLIC_NAVER_REDIRECT_URL || window.location.href
            }
          />
        </HiddenNaver>
      )}
      {isOpen && (
        <ModalPortal onClose={onClose}>
          <Wrapper justify="center" column>
            <CloseIcon onClick={onClose} />
            {/* <WrtnLogo /> */}
            <Gap />
            <LogoWrtnPurple />
            <Gap />
            <Desc>
              로그인 후{" "}
              <span style={{ color: colors.POINT_PURPLE }}>무료로</span> 뤼튼의
              <br />
              놀라운 창작 실력을 경험해보세요.
            </Desc>
            <Gap />
            <Divider />
            <Gap />
            <Auth />
          </Wrapper>
        </ModalPortal>
      )}
    </>
  );
};

export default LoginMainDialog;

const Wrapper = styled(FlexWrapper)`
  background-color: ${colors.WHITE};
  border-radius: 20px;
  padding: 40px 23px;
  min-width: 395px;

  @media (max-width: 480px) {
    min-width: 0;
    max-width: calc(100vw - 40px);
  }
`;

const RoundButton = styled.div`
  width: 55px;
  height: 55px;
  border-radius: 100%;
  cursor: pointer;
`;

const Divider = styled.div`
  width: 100%;
  border: 0.5px solid ${colors.BLUE_GRAY_LINE};
`;

const Desc = styled.div`
  ${typo({
    weight: 500,
    size: "16px",
    height: "25.6px",
    color: colors.gray_80,
  })};
  text-align: center;
`;

const AuthLoginButtons = styled(FlexWrapper)`
  gap: 13px;
`;

const Gap = styled.div`
  height: 27px;
`;

const HiddenNaver = styled.div`
  visibility: hidden;
  position: absolute;
  top: 0px;
  left: 0px;
`;
