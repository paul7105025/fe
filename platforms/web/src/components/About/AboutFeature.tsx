import styled from "styled-components";

import { AboutChip, DynamicText, MainDesc } from "./styles";
import { colors, FlexWrapper, typo } from "@wrtn/ui";
import { useIsMobile } from "@wrtn/core";

const chipArray = [
  "부동산 매물 소개",
  "마케팅 문구",
  "문서 작업",
  "자기소개서",
  "블로그 포스팅",
  "쇼츠 대본",
];

export const AboutFeature = () => {
  const isMobile = useIsMobile();

  return (
    <FeatureWrapper column>
      <MaxWrapper column>
        <SingleFeature
          align="flex-start"
          style={{
            minHeight: isMobile ? 450 : 650,
          }}
        >
          <FeatureDescWrapper>
            <MainDesc boldColor={colors.HOVER_PURPLE}>
              <b>한국에 최적화된</b>
              <br />
              생성 AI 서비스
            </MainDesc>
          </FeatureDescWrapper>
          <FeatureImageWrapper
            style={{
              top: 214,
              right: 0,
            }}
          >
            <Image
              src="/about/feat_2billion.png"
              alt="feat_2billion"
              style={{
                width: "100%",
                height: "100%",
                maxWidth: 836,
                maxHeight: 410,
              }}
            />
          </FeatureImageWrapper>
          <div style={{ flex: 1 }} />
          <FeatureDescWrapper>
            <BigText
              style={{
                color: colors.HOVER_PURPLE,
              }}
            >
              22억 단어
              <SmallSpan>2023년 3월 기준</SmallSpan>
            </BigText>
            <SubDesc color={colors.GRAY_90}>
              뤼튼이 세상에 나와 지금까지 생성한 한국어 단어입니다.
              <br />
              <b>압도적인 양</b>의 생성 데이터와 <b>사용자 니즈</b>를 분석해,
              <br /> 한국 문화에 더 최적화된 결과물을 만들어냅니다.
            </SubDesc>
          </FeatureDescWrapper>
        </SingleFeature>
        <SingleFeature
          align="flex-end"
          style={{
            minHeight: isMobile ? 450 : 550,
          }}
        >
          <FeatureDescWrapper>
            <MainDesc boldColor="#7DAF8D">
              <b>다양한</b> 생성 AI
              <br />
              모델을 <b>가장 쉽게</b>
            </MainDesc>
            <SubWrapper column align="flex-start">
              <SubDesc color="#7DAF8D">
                OpenAI <b>ChatGPT | GPT3.5 | GPT4</b>
                <br />
                네이버 <b>하이퍼클로바</b>
                <br />
                Stability AI <b>Stable Diffusion</b> + 지속적인 모델 추가
              </SubDesc>
            </SubWrapper>
          </FeatureDescWrapper>
          <FeatureImageWrapper
            style={{
              top: 120,
              left: 0,
            }}
          >
            <Image
              src="/about/feat_models.png"
              alt="feat_models"
              style={{
                width: "100%",
                height: "100%",
                maxWidth: 576,
                maxHeight: 342,
              }}
            />
          </FeatureImageWrapper>
          <div style={{ flex: 1 }} />
          <FeatureDescWrapper>
            <SubDesc color={colors.GRAY_90}>
              전세계 모든 생성 AI를 뤼튼에서 무료로{" "}
              <BigText
                style={{
                  color: "#7DAF8D",
                  lineHeight: "33px",
                }}
              >
                한 번에.
              </BigText>
              <br />
              복잡한 모델의 원리를 공부 할 필요 없어요.
              <br /> <b>가장 쉽고 편한 사용자 경험</b>을 제공합니다.
            </SubDesc>
          </FeatureDescWrapper>
        </SingleFeature>
        <SingleFeature
          align="flex-start"
          style={{
            minHeight: isMobile ? 400 : 500,
          }}
        >
          <FeatureDescWrapper>
            <MainDesc boldColor={colors.POINT_PURPLE}>
              나를{" "}
              <b>
                가장
                <br />잘 이해하는 AI
              </b>
            </MainDesc>
          </FeatureDescWrapper>
          <FeatureImageWrapper
            style={{
              top: 0,
              right: 0,
            }}
          >
            <Image
              src="/about/feat_circle_wrtn.png"
              alt="feat_circle_wrtn"
              style={{
                width: "100%",
                height: "100%",
                maxWidth: 513,
                maxHeight: 539,
              }}
            />
          </FeatureImageWrapper>
          <div style={{ flex: 1 }} />
          <FeatureDescWrapper>
            <ChipWrapper>
              {chipArray.map((chip, index) => (
                <AboutChip type="md" key={index}>
                  {chip}
                </AboutChip>
              ))}
            </ChipWrapper>
            <SubDesc color={colors.GRAY_90}>
              뤼튼은 나에게 가장 적절한 도구를 <b>똑똑하게 추천</b>해 줍니다.
            </SubDesc>
          </FeatureDescWrapper>
        </SingleFeature>
      </MaxWrapper>
    </FeatureWrapper>
  );
};

const Image = styled.img`
  display: block;
`;

const FeatureWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 240px 20px;

  @media (max-width: 767px) {
    padding: 10px 20px 100px;
  }
`;

const MaxWrapper = styled(FlexWrapper)`
  max-width: 1512px;
  width: 100%;
  gap: 200px;

  @media (max-width: 767px) {
    gap: 100px;
  }
`;

const SingleFeature = styled(FlexWrapper)`
  width: 100%;

  flex-direction: column;
`;

const FeatureDescWrapper = styled(FlexWrapper)`
  width: 40%;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  @media (max-width: 767px) {
    width: 100%;
  }
`;

const FeatureImageWrapper = styled(FlexWrapper)`
  position: absolute;
  @media (max-width: 767px) {
    position: relative;
    top: 0 !important;
    left: 0 !important;
  }
`;

const SubDesc = styled.p<{ color: string }>`
  ${({ color }) =>
    DynamicText({
      size: "20px",
      weight: 400,
      height: "165%",
      color: colors.GRAY_90,
      boldColor: color,
    })}
  b {
    font-weight: 600;
  }

  @media (max-width: 767px) {
    font-size: 14px;
  }
`;

const BigText = styled.span`
  ${typo({
    size: "40px",
    weight: 900,
    height: "165%",
    color: colors.GRAY_90,
  })}

  @media (max-width: 767px) {
    font-size: 24px;
  }
`;

const SmallSpan = styled.span`
  ${typo({
    size: "9px",
    weight: 500,
    height: "165%",
    color: colors.HOVER_PURPLE,
  })}
`;

const SubWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 21px 0px;

  border-top: 1px solid #7daf8d;
  border-bottom: 1px solid #7daf8d;
`;

const ChipWrapper = styled(FlexWrapper)`
  width: 100%;
  margin-bottom: 20px;
  gap: 10px;

  flex-wrap: wrap;
`;
