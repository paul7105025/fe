import styled from "styled-components";
import Image from "next/image";

import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui";
import { SubDesc, MainDesc } from "./styles";
import { AboutFAQ } from "./AboutFAQ";
import Link from "next/link";
import {
  WRTN_NAVER_BLOG_HREF,
  WRTN_COURIER_HREF,
  WRTN_PRIVACY_POLICY_HREF,
  WRTN_USER_GUIDE_HREF,
} from "@wrtn/core";

export const AboutFooter = () => {
  return (
    <FooterWrapper>
      <MaxWrapper>
        <Sub>뤼튼과 함께 하는 AI Life</Sub>
        <Main>시작할 준비 되셨나요?</Main>
        <FAQWrapper column align="flex-start">
          <FAQInnerWrapper>
            <FAQHeader>자주 묻는 질문</FAQHeader>
            <AboutFAQ />
          </FAQInnerWrapper>
        </FAQWrapper>
        <BannerWrapper column align="flex-start">
          <LogoText>Team Wrtn</LogoText>
          <LogoDesc>
            Generative AI를 선도하는
            <br />
            뤼튼 팀을 소개합니다.
          </LogoDesc>
          <div style={{ flex: 1 }} />
          <ButtonWrapper>
            <a href={WRTN_NAVER_BLOG_HREF} target="_blank" rel="noopener noreferrer">
              <FooterButton
                color={colors.POINT_PURPLE}
                bgColor={colors.HOVER_PURPLE}
              >
                팀블로그
              </FooterButton>
            </a>
            <a
              href={WRTN_COURIER_HREF}
              target="_blank"
              rel="noopener noreferrer"
            >
              <FooterButton color={colors.WHITE} bgColor={colors.BLACK}>
                인재 채용
              </FooterButton>
            </a>
          </ButtonWrapper>
          <BannerImageWrapper>
            <Image
              src="/about/feat_circle_wrtn.png"
              alt="feat_circle_wrtn"
              fill
            />
          </BannerImageWrapper>
        </BannerWrapper>
        <LogoWrapper>
          <Image
            src="/logo_wrtn_technologies.svg"
            alt="logo_wrtn_technologies"
            width={168}
            height={16}
          />
        </LogoWrapper>
        <Footer>
          <FooterText>
            주식회사 뤼튼테크놀로지스 I 대표 이세영 | 홍보 및 대외협력
            pr@wrtn.io
            <br />
            사업자 등록번호 : 202-81-67042 I 통신판매업 신고번호 :
            2021-서울마포-3504
          </FooterText>
          <FooterTextRight>
            <FooterLink
              href={WRTN_USER_GUIDE_HREF}
              target="_blank"
              rel="noopener noreferrer"
            >
              사용자가이드
            </FooterLink>
            <FooterLink
              href={WRTN_PRIVACY_POLICY_HREF}
              target="_blank"
              rel="noopener noreferrer"
            >
              개인정보처리방침
            </FooterLink>

            <br />
            <b>ⓒ 2023. Wrtn Technologies, Inc.</b>
          </FooterTextRight>
        </Footer>
      </MaxWrapper>
    </FooterWrapper>
  );
};

const FooterWrapper = styled.div`
  width: 100%;
  background: linear-gradient(0deg, #9e96ff 10.49%, #8b78ff 100%);
`;

const MaxWrapper = styled.div`
  width: 100%;
  max-width: 1256px;
  margin: 0px auto;
  padding: 130px 20px;
  @media (max-width: 767px) {
    padding: 106px 20px;
  }
`;

const Sub = styled(SubDesc)`
  margin-bottom: 25px;
  text-align: center;
  color: ${colors.WHITE};
  @media (max-width: 767px) {
    margin-bottom: 10px;
    ${typo({
      size: "16px",
      weight: 500,
      height: "100%",
      color: colors.white,
    })}
  }
`;

const Main = styled(MainDesc)`
  margin-bottom: 100px;
  text-align: center;
  color: ${colors.WHITE};
  @media (max-width: 767px) {
    ${typo({
      size: "24px",
      weight: 700,
      height: "100%",
      color: colors.white,
    })}
    margin-bottom: 25px;
  }
`;

const FAQInnerWrapper = styled.div`
  width: 100%;
  max-width: 1070px;
  padding: 63px 20px;
  margin: 0px auto;

  @media (max-width: 767px) {
    padding: 41px 20px;
  }
`;

const FAQWrapper = styled(FlexWrapper)`
  width: 100%;
  background-color: ${colors.POINT_PURPLE};
  border-radius: 8px;

  margin-bottom: 80px;

  margin: 0px auto 80px;
`;

const FAQHeader = styled.p`
  ${typo({
    size: "36px",
    weight: 600,
    color: colors.WHITE,
  })}

  margin-bottom: 64px;
  @media (max-width: 767px) {
    font-size: 16px;
    text-align: center;
  }
`;

const BannerWrapper = styled(FlexWrapper)`
  width: 100%;
  max-height: 438px;
  background-color: ${colors.POINT_PURPLE};
  border-radius: 8px;
  padding: 70px 74px;

  margin-bottom: 70px;

  overflow: hidden;
  @media (max-width: 1024px) {
    display: block;
    padding: 37px 20px;
    max-height: 100%;
    overflow: hidden;
  }
`;

const LogoText = styled.p`
  ${typo({
    size: "20px",
    weight: 700,
    color: colors.HOVER_PURPLE,
  })}
  margin-left: 18px;
  margin-bottom: 7px;
  @media (max-width: 1024px) {
    text-align: center;
  }
`;

const LogoDesc = styled.p`
  ${typo({
    size: "40px",
    weight: 900,
    color: colors.WHITE,
    height: "145%",
  })}
  margin-bottom: 74px;

  @media (max-width: 1024px) {
    text-align: center;
    ${typo({
      size: "20px",
      weight: 700,
      color: colors.WHITE,
      height: "150%",
    })}
    margin-bottom: 30px;
  }
`;

const ButtonWrapper = styled(FlexWrapper)`
  gap: 20px;
  @media (max-width: 1024px) {
    display: block;
  }
`;

const FooterButton = styled(FlexButton)<{
  bgColor: string;
  color: string;
}>`
  padding: 30px 0px;
  width: 170px;

  border-radius: 5px;

  ${typo({
    size: "20px",
    weight: 700,
    color: colors.BLACK,
  })}

  background-color: ${colors.WHITE};

  &:hover {
    color: ${({ color }) => color};

    background-color: ${({ bgColor }) => bgColor};
    cursor: pointer;
  }
  @media (max-width: 1024px) {
    width: 100%;
    margin-bottom: 20px;
    padding: 20px 0px;
  }
`;

const BannerImageWrapper = styled(FlexWrapper)`
  width: 505px;
  height: 530px;

  position: absolute;
  right: -30px;
  top: 0;

  @media (max-width: 1024px) {
    position: relative;
    transform: scale(1.5) translate(-20%, 25%);
    height: 200px;
    width: 200px;
  }
`;

const Footer = styled(FlexWrapper)`
  width: 100%;
  margin-top: 18px;
  padding-top: 10px;

  border-top: 1px solid ${colors.WHITE};
  justify-content: space-between;
  @media (max-width: 767px) {
    display: block;
  }
`;

const FooterText = styled.p`
  ${typo({
    size: "10px",
    weight: 400,
    height: "160%",
    color: colors.WHITE,
  })}
`;

const FooterTextRight = styled(FooterText)`
  text-align: right;
  @media (max-width: 767px) {
    margin-top: 40px;
    text-align: left;
  }
`;

const FooterLink = styled(Link)`
  color: ${colors.WHITE};
  :visited {
    color: ${colors.WHITE};
  }
  margin-left: 20px;
  @media (max-width: 767px) {
    margin-left: 0px;
    margin-right: 20px;
  }
`;

const LogoWrapper = styled(FlexWrapper)`
  width: 100%;
`;
