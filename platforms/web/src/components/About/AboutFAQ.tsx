import React from "react";
import styled from "styled-components";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import { Icon } from "@wrtn/ui";

const questionList = [
  {
    q: "뤼튼은 어떤 서비스인가요?",
    a: "뤼튼은 초거대 생성 AI(Generative AI) 기반의 콘텐츠 생성 플랫폼입니다. 키워드를 입력하여 원하는 상황에 대한 문장을 생성할 수 있습니다.",
  },
  {
    q: "뤼튼은 누가 사용하면 좋나요?",
    a: "뤼튼은 마케터, 이커머스 사장님, 콘텐츠 크리에이터, 스타트업 종사자 등 글쓰기 창작 영역에서 어려움을 겪고 있는 누구에게나 도움을 줄 수 있습니다.",
  },
  {
    q: "뤼튼은 어떤 장점이 있고 언제 도움을 받을 수 있나요?",
    a: "뤼튼은 사용자의 시간을 아껴드리기 위해 빠르게 문장을 생성합니다. 광고 문구, 메일 작성, 문의 응대, 블로그 글과 같은 다양한 글쓰기 순간에 활용하실 수 있습니다.",
  },
  {
    q: "뤼튼으로 만든 결과물의 저작권은 누구의 것인가요?",
    a: "결과물의 저작권 및 사용 권한은 사용자에게 귀속되며, 개인적 용도 및 상업적 용도로 활용할 수 있습니다.",
  },
  {
    q: "뤼튼으로 만든 결과물은 어떤 데이터로 이루어져 있나요?",
    a: "뤼튼은 초거대 생성 AI(Generative AI)를 기반으로 만들어졌으며, 사용자가 입력한 내용을 토대로 매번 새로운 문장을 생성해 냅니다. \n모델의 크기가 매우 크기에 학습된 데이터를 그대로 뱉을 확률이 거의 없습니다. 문장을 생성하는 과정에서 경우에 따라 사실과 다른 내용이 나올 수 있습니다.",
  },
];

const FQAQuestionItem = ({
  q = "",
  a,
}: {
  q: string;
  a: React.ReactNode | string;
}) => {
  const [isCollapsed, setIsCollapsed] = React.useState(true);
  return (
    <FAQListItem>
      <FAQHeader onClick={() => setIsCollapsed((c) => !c)}>
        <FAQQuestion>{q}</FAQQuestion>
        <IconArrow
          icon="angle-down"
          color={colors.WHITE}
          size={20}
          isCollapsed={isCollapsed}
        />
      </FAQHeader>
      {!isCollapsed && <FAQAnswer>{a}</FAQAnswer>}
    </FAQListItem>
  );
};

export const AboutFAQ = () => {
  return (
    <Wrapper column>
      {questionList.map((item, idx) => {
        return <FQAQuestionItem key={idx} q={item.q} a={item.a} />;
      })}
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  width: 100%;
`;

const FAQListItem = styled.div`
  margin-top: 24px;
  padding-bottom: 20px;

  width: 100%;

  border-bottom: 1px solid ${colors.WHITE};
`;

const FAQHeader = styled.div`
  height: 28px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  cursor: pointer;
  @media (max-width: 767px) {
    align-items: flex-start;
  }
`;

const FAQQuestion = styled.h6`
  flex: 1;

  ${typo({
    size: "20px",
    weight: 700,
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    font-size: 14px;
  }
  user-select: none;
`;

const FAQAnswer = styled.p`
  ${typo({
    size: "16px",
    weight: 400,
    height: "160%",
    color: colors.WHITE,
  })}

  padding-top: 16px;
  @media (max-width: 767px) {
    font-size: 12px;
  }
`;

const IconArrow = styled(Icon)<{ isCollapsed: boolean }>`
  cursor: pointer;
  transform: ${({ isCollapsed }) =>
    isCollapsed ? "rotate(180deg)" : "rotate(0deg)"};
  transition: transform 0.3s ease-in-out;
`;
