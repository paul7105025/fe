import React from "react";
import styled, { keyframes } from "styled-components";

import { colors, FlexWrapper, Icon, typo } from "@wrtn/ui";
import { AboutChip } from "./styles";

const descList = [
  <p>
    <b>한국에 최적화</b>된 결과물을 받아보세요
  </p>,
  <p>
    다양한 <b>생성 AI 모델</b>을 가장 쉽게 경험하세요.
  </p>,
  <p>
    내 상황에 맞는 <b>툴을 추천</b> 받아보세요.
  </p>,
];

const chipList = [
  "블로그 글 써줘",
  "보라 공룡이 햄버거 먹는 그림 그려줘",
  "존댓말로 바꿔줘",
  "pdf 파일 요약해줘",
];

export const AboutIntro = () => {
  return (
    <IntroWrapper column justify="flex-end">
      <ZoomWrapper column>
        <ZoomTitle>모두를 위한 AI 포털</ZoomTitle>
        <Image
          src="/logo_wrtn_purple.svg"
          alt="wrtn_logo"
          style={{
            width: "100%",
            height: "100%",
            maxWidth: 560,
            maxHeight: 156,
          }}
        />
      </ZoomWrapper>
      <FadeWrapper column>
        <FadeDescWrapper>
          <Image
            src="/about/about_desc_bg.png"
            alt="wrtn_logo"
            style={{
              width: "100%",
              height: "100%",
              maxWidth: 450,
              maxHeight: 175,
            }}
          />
          <FadeDesc column justify="center" align="center">
            {descList.map((d, i) => (
              <SingleDesc key={i.toString()}>
                <span> {i + 1}</span>
                {d}
              </SingleDesc>
            ))}
          </FadeDesc>
        </FadeDescWrapper>
        <FadeChipDescWrapper>
          <FadeChipDesc>AI 질문 제안</FadeChipDesc>
          <Icon icon="angle-down" size={14} color={colors.POINT_PURPLE} />
        </FadeChipDescWrapper>
        <FadeChipWrapper>
          {chipList.map((c, i) => (
            <AboutChip key={i.toString()} type="sm">
              {c}
            </AboutChip>
          ))}
        </FadeChipWrapper>
        <FadeInputWrapper justify="center">
          <ImageDesktop
            src="/about/chat_input.png"
            alt="wrtn_logo"
            style={{
              width: "100%",
              height: "100%",
              maxWidth: 1256,
              maxHeight: 232,
            }}
          />
          <ImageMobile src="/about/mobile_chat_input.svg" />
        </FadeInputWrapper>
      </FadeWrapper>
    </IntroWrapper>
  );
};

const Image = styled.img``;

const zoomIn = keyframes`
    0% {
        transform:  translate(-50%, -50%) scale(1.0);
        top: 50%;
        @media (max-width: 767px) {
            transform:  translate(-50%, -50%) scale(0.6);
        }
    }
    100% {
        transform: translate(-50%, 0%) scale(0.4);
        top : 147px;
    }
`;

const FadeIn = keyframes`
    0% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
`;

const IntroWrapper = styled(FlexWrapper)`
  width: 100%;
  min-height: 920px;
  padding: 147px 128px 65px;
  max-width: 1256px;
  margin: 0px auto;
  padding: 0px 20px;
`;

const ImageDesktop = styled.img`
  @media (max-width: 767px) {
    display: none;
  }
`;

const ImageMobile = styled.img`
  display: none;
  width: 100%;
  @media (max-width: 767px) {
    display: block;
  }
`;

const ZoomWrapper = styled(FlexWrapper)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  gap: 60px;

  @media (max-width: 767px) {
    transform: translate(-50%, -50%) scale(0.6);
  }

  animation: ${zoomIn} 0.6s ease-in-out;
  animation-delay: 0.6s;
  animation-fill-mode: forwards;
  transform-origin: center top;
`;

const ZoomTitle = styled.h1`
  ${typo({
    size: "45px",
    weight: 600,
    color: colors.GRAY_90,
  })}
  white-space: nowrap;
`;

const FadeWrapper = styled(FlexWrapper)`
  width: 100%;
  opacity: 0;

  animation: ${FadeIn} 0.6s ease-in-out;
  animation-delay: 1.2s;
  animation-fill-mode: forwards;
  transform-origin: center top;
`;

const FadeDescWrapper = styled(FlexWrapper)`
  margin-bottom: 41px;
`;

const FadeDesc = styled(FlexWrapper)`
  position: absolute;

  top: 0;
  left: 0;

  width: 100%;
  height: 100%;

  gap: 26px;
  padding-left: 56px;

  @media (max-width: 767px) {
    gap: 18px;
    padding-left: 28px;
  }
`;

const SingleDesc = styled(FlexWrapper)`
  ${typo({
    size: "17px",
    weight: 600,
    color: colors.GRAY_80,
  })}

  width: 100%;
  gap: 32px;

  span {
    color: ${colors.POINT_PURPLE};
    font-weight: 900;
  }
  p {
    b {
      color: ${colors.POINT_PURPLE};
    }
  }

  @media (max-width: 767px) {
    gap: 16px;
  }
`;

const FadeChipDescWrapper = styled(FlexWrapper)`
  gap: 4px;

  margin-bottom: 24px;
`;

const FadeChipDesc = styled.p`
  ${typo({
    size: "14px",
    weight: 600,
    height: "186%",
    color: colors.POINT_PURPLE,
  })}
`;

const FadeChipWrapper = styled(FlexWrapper)`
  gap: 12px;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
`;

const FadeInputWrapper = styled(FlexWrapper)`
  width: 100%;
  min-height: 232px;
`;
