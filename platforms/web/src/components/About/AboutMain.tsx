import React from "react";
import Image from "next/image";
import styled from "styled-components";

import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui";
import { AboutChip, DynamicText, MainDesc, SubDesc } from "./styles";
import { useIsMobile } from "@wrtn/core";

type featureType = "chat" | "editor";

const CheckImage = ({ blue }: { blue?: boolean }) => {
  const isMobile = useIsMobile();

  return (
    <Image
      src={`/about/check_circle_${blue ? "blue" : "purple"}.svg`}
      alt={`check_circle_${blue ? "blue" : "purple"}`}
      width={isMobile ? 18 : 30}
      height={isMobile ? 16 : 28}
    />
  );
};

export const AboutMain = () => {
  const [feature, setFeature] = React.useState<featureType>("chat");

  const isMobile = useIsMobile();

  return (
    <NewWrapper column>
      <SelectWrapper justify="center">
        <SelectButton
          selected={feature === "chat"}
          type="chat"
          onClick={() => setFeature("chat")}
        >
          챗&툴
        </SelectButton>
        <SelectButton
          selected={feature === "editor"}
          type="editor"
          onClick={() => setFeature("editor")}
        >
          에디터
        </SelectButton>
      </SelectWrapper>
      <BodyWrapper column justify="flex-end">
        {feature === "chat" && (
          <>
            <Sub>나를 가장 잘 이해하는 AI</Sub>
            <Main boldColor={colors.POINT_PURPLE}>
              <b>뤼튼</b>과 이야기 해보세요.
            </Main>
            <FeatureWrapper align="flex-start">
              <ImageWrapper>
                <Image
                  src="/about/chat_preview.png"
                  alt="chat_preview"
                  width={829}
                  height={496}
                />
              </ImageWrapper>
              <MobileImageWrapper>
                <Image
                  src="/about/mobile_chat_preview.png"
                  alt="mobile_chat_preview"
                  width={215}
                  height={285}
                />
              </MobileImageWrapper>
              <DescWrapper column>
                <DescBox column>
                  <DescHeader>
                    <CheckImage />
                    <DescText type="chat">무엇이든 질문하세요!</DescText>
                  </DescHeader>
                  <SubDescText type="chat">
                    뤼튼은 오픈 AI 의 ChatGPT, 네이버의 클로바 등{" "}
                    <b>다양한 모델을 결합</b>한 채팅 경험을 무료로 제공해요.
                  </SubDescText>
                  <ChipWrapper>
                    <AboutChip type="sm">엑셀 수식을 알려줘!</AboutChip>
                    <AboutChip type="sm">블로그 글을 써줘!</AboutChip>
                  </ChipWrapper>
                  <SubDescText type="chat">
                    각 상황에 최적화된 <b>모델을 호출</b>하는 툴이 있어요.
                  </SubDescText>
                </DescBox>
                <DescBox column>
                  <DescHeader>
                    <CheckImage />

                    <DescText type="chat">
                      어떻게 질문할지 고민하지 마세요!
                    </DescText>
                  </DescHeader>
                  <SubDescText type="chat">
                    엑셀 공부하듯 ChatGPT를 사용법을 공부하고 계셨다면{" "}
                    <b>이제 그만!</b>
                    <br /> 뤼튼에선 더 이상 고민하지 마세요! 똑똑하게{" "}
                    <b>질문을 제안</b> 해드립니다.
                  </SubDescText>
                </DescBox>
                <DescBox column>
                  <DescHeader>
                    <CheckImage />
                    <DescText type="chat">
                      자주 쓰는 툴과 템플릿을 저장해보세요!
                    </DescText>
                  </DescHeader>
                  <SubDescText type="chat">
                    자주 입력하는 내용을 저장하면 <b>"/" 를 입력</b>해 쉽게
                    불러올 수 있어요!
                    <br />
                    뤼튼은 앞으로도 <b>작업 시간을 단축</b> 해주는 여러 편의
                    기능들을 추가할 예정이에요.
                  </SubDescText>
                </DescBox>
              </DescWrapper>
            </FeatureWrapper>
          </>
        )}
        {feature === "editor" && (
          <>
            <Sub style={{ opacity: 0 }}>e</Sub>
            <Main boldColor={colors.ACTION_BLUE}>
              긴 글도 막힘없이 <b>빠르게</b> 완성!
            </Main>
            <FeatureWrapper align="flex-start">
              <ImageWrapper>
                <Image
                  src="/about/editor_preview.png"
                  alt="editor_preview"
                  fill
                />
              </ImageWrapper>
              <MobileImageWrapper>
                <Image
                  src="/about/editor_preview.png"
                  alt="editor_preview"
                  width={317}
                  height={214}
                />
              </MobileImageWrapper>
              <DescWrapper column>
                <DescBox column>
                  <DescHeader>
                    <CheckImage blue />
                    <DescText type="editor">백지 공포증은 이제 그만!</DescText>
                  </DescHeader>
                  <SubDescText type="editor">
                    뤼튼이 긴 글 초안을 생성해드려요.
                  </SubDescText>
                </DescBox>
                <DescBox column>
                  <DescHeader>
                    <CheckImage blue />
                    <DescText type="editor">
                      글을 쓰다 막혀도 걱정마세요!
                    </DescText>
                  </DescHeader>
                  <SubDescText type="editor">
                    <b>Ctrl + Enter</b>만 누르면 뤼튼이 자동으로 이어서
                    써드려요.
                  </SubDescText>
                </DescBox>
                <DescBox column>
                  <DescHeader>
                    <CheckImage blue />
                    <DescText type="editor">
                      에디터는 더욱 똑똑해질 예정이에요!
                    </DescText>
                  </DescHeader>
                  <SubDescText type="editor">
                    <b>문서, 이미지, 비디오</b>까지!
                    <br /> 뤼튼 에디터로 창의력을 마음껏 확장해보세요.
                  </SubDescText>
                </DescBox>
                {isMobile && (
                  <EditorMobileText>
                    에디터 기능은 현재
                    <br />
                    <b>PC 환경</b>에서만 지원하고있어요.
                  </EditorMobileText>
                )}
              </DescWrapper>
            </FeatureWrapper>
          </>
        )}
      </BodyWrapper>
    </NewWrapper>
  );
};

const NewWrapper = styled(FlexWrapper)`
  width: 100%;
  background-color: ${colors.SKY_BLUE};
  padding: 118px 20px 164px;

  @media (max-width: 767px) {
    background-color: transparent;
    background: linear-gradient(
      360deg,
      #eaf2ff 85.71%,
      rgba(234, 242, 255, 0) 100%
    );
    padding: 87px 20px 84px;
  }
`;

const SelectWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 34px;
`;

const SelectButton = styled(FlexWrapper)<{
  selected: boolean;
  type: featureType;
}>`
  padding: 10px 20px;

  ${({ selected }) =>
    typo({
      size: "20px",
      weight: 600,
      color: selected ? colors.GRAY_90 : colors.GRAY_55,
    })}

  &:hover {
    cursor: pointer;
    color: ${({ type }) =>
      type === "chat" ? colors.POINT_PURPLE : colors.ACTION_BLUE};
  }
`;

const BodyWrapper = styled(FlexWrapper)`
  width: 100%;
  max-width: 1512px;

  margin-top: 92px;

  @media (max-width: 767px) {
    margin-top: 76px;
  }
`;

const Sub = styled(SubDesc)`
  margin-bottom: 25px;
  ${typo({
    size: "16px",
    weight: 700,
    height: "160%",
  })}

  @media (max-width: 767px) {
    margin-bottom: 10px;
  }
`;

const Main = styled(MainDesc)`
  margin-bottom: 100px;
  ${typo({
    size: "64px",
    weight: 700,
    height: "150%",
  })};
  @media (max-width: 767px) {
    margin-bottom: 26px;
    ${typo({
      size: "24px",
      weight: 700,
      height: "150%",
    })};
  }
`;

const FeatureWrapper = styled(FlexWrapper)`
  width: 100%;
  @media (max-width: 767px) {
    display: block;
  }
`;

const ImageWrapper = styled(FlexWrapper)`
  flex: 0.66;
  min-height: calc((100vw - 256px) * 0.44);
  @media (max-width: 767px) {
    display: none;
  }
`;

const MobileImageWrapper = styled(FlexWrapper)`
  align-items: center;
  justify-content: center;
  width: 100%;
  display: none;
  @media (max-width: 767px) {
    display: flex;
    margin-bottom: 20px;
  }
`;

const DescWrapper = styled(FlexWrapper)`
  padding-left: 23px;
  flex: 0.34;

  gap: 60px;
  padding-top: 10px;

  @media (max-width: 767px) {
    padding: 0;
    gap: 40px;
  }
`;

const DescBox = styled(FlexWrapper)`
  width: 100%;
  align-items: flex-start;
  @media (max-width: 767px) {
    align-items: center;
  }
`;

const DescHeader = styled(FlexWrapper)`
  gap: 14px;
  @media (max-width: 767px) {
    gap: 6px;
  }
`;

const DescText = styled.p<{ type: featureType }>`
  ${({ type }) =>
    typo({
      size: "23px",
      weight: 700,
      height: "180%",
      color: type === "chat" ? colors.POINT_PURPLE : colors.ACTION_BLUE,
    })}

  @media (max-width: 767px) {
    font-size: 16px;
    text-align: center;
  }
`;

const SubDescText = styled.p<{ type: featureType }>`
  ${({ type }) =>
    DynamicText({
      size: "15px",
      weight: 500,
      height: "160%",
      color: colors.GRAY_90,
      boldColor: type === "chat" ? colors.POINT_PURPLE : colors.ACTION_BLUE,
    })}

  b {
    font-weight: 700;
  }

  @media (max-width: 767px) {
    font-size: 12px;
    text-align: center;
  }
`;

const ChipWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 10px;

  margin: 17px 0px 5px;

  @media (max-width: 767px) {
    justify-content: center;
  }
`;

const EditorMobileText = styled.p`
  ${DynamicText({
    size: "14px",
    weight: 600,
    height: "140%",
    color: colors.GRAY_90,
    boldColor: colors.ACTION_BLUE,
  })}
  text-align: center;
  margin-top: 26px;
`;
