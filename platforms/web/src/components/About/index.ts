export * from "./AboutIntro";
export * from "./AboutFeature";
export * from "./AboutMain";
export * from "./AboutNew";
export * from "./AboutPower";
export * from "./AboutFooter";

export * from "./AboutFAQ";
export * from "./AboutRegisterModal";
