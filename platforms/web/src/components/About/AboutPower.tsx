import Image from "next/image";
import styled, { keyframes } from "styled-components";

import { MainDesc, SubDesc } from "./styles";

import { colors, FlexWrapper, typo } from "@wrtn/ui";
import { useIsMobile } from "@wrtn/core";

const recommendList: Array<{
  text: string;
  logo: string | null;
  name: string;
  width: number;
  height: number;
}> = [
  {
    text: "너무 유용하고, 진짜 중요한 것들에 집중할 수 있도록 도와주고 있어요! 빠르게 성과를 내게끔 시간을 줄여줍니다.",
    logo: "/about/logo/logo_lotte.png",
    name: "마케터 이OO님",
    width: 51,
    height: 23,
  },
  {
    text: "생각보다 퀄리티가 좋아서 놀랐습니다. 어떤 알고리즘으로 문장을 완성 시키는지 궁금해졌어요. 저도 테크업계에 있지만, 최근 본 제품중 가장 완성도가 높은 AI 관련 제품이네요.",
    logo: "/about/logo/logo_tossbank.png",
    name: "디자이너 소OO님",
    width: 72,
    height: 25,
  },
  {
    text: "마케터로서 정말 흥미로운 서비스입니다. 원하는 키워드와 주제를 넣으면 자동으로 완성도있는 문장이 나와서, 전달력있고 매끄럽게 글을 다듬는데에 큰 도움을 받고있습니다.",
    logo: "/about/logo/logo_yanolja_cloud.png",
    name: "마케터 윤OO님",
    width: 69,
    height: 23,
  },
  {
    text: "아무것도 없는 백지에서 기획서를 작성할 때마다 막막했는데, 뤼튼으로 초안을 만든 후 글을 쓰기 시작하니 업무 효율이 굉장히 올라갔습니다. 타 부서와 협업 할 때 요긴하게 활용하고 있습니다.",
    logo: "/about/logo/logo_wanted.png",
    name: "디자이너 김OO님",
    width: 55,
    height: 21,
  },
  {
    text: "소구점 하나를 입력하면 AI가 타깃의 니즈를 정확히 파악해서 문장을 만들어주는 점이 소름돋고 좋았어요!",
    logo: "/about/logo/logo_wadiz.png",
    name: "마케터 류OO님",
    width: 40,
    height: 17,
  },
  {
    text: "아주 신박한 ai 툴의 혜성같은 등장으로 여러 업무의 효율성이 증대되어 좋습니다. 몸이 열개라도 모자른 스타트업의 마케터로서 정말 편하고 감사히 잘 사용하고 있습니다.",
    logo: "/about/logo/logo_tripbtoz.png",
    name: "마케터 고OO님",
    width: 48,
    height: 21,
  },
  {
    text: "키워드만 입력하면 자동으로 다양한 카피가 쏟아지다니! 이제 야근 없는 삶이 가능할 것 같아요.",
    logo: "/about/logo/logo_leadgen.png",
    name: "마케터 박OO님",
    width: 32,
    height: 17,
  },
  {
    text: "바쁜 일정에 시달리다 보면 번뜩이는 단어가 생각나지 않아 답답할 때가 많았는데, 뤼튼으로 빠르게 다양한 표현을 받아보면서 굉장히 많은 도움을 받았습니다. 뤼튼은 단비이자 오아시스같은 곳입니다. ",
    logo: null,
    name: "13년차 작가ㆍ크리에이터 김OO님",
    width: 0,
    height: 0,
  },
];

export const AboutPower = () => {
  const isMobile = useIsMobile();

  return (
    <PowerWrapper>
      <PowerHeader column>
        <Sub>AI의 힘, 아직도 모르고 계셨나요? </Sub>
        <Main>
          수많은 사용자들이 증명하고 있는{isMobile && <br />} 뤼튼의 힘
        </Main>
        <UserImageWrapper
          style={{
            bottom: isMobile ? "94px" : "169px",
            left: isMobile ? "4px" : "261px",
          }}
        >
          <Image
            src="/about/profile_blue.png"
            alt="profile_blue"
            width={isMobile ? 25 : 69}
            height={isMobile ? 25 : 69}
          />
        </UserImageWrapper>
        <UserImageWrapper
          style={{
            bottom: isMobile ? "20px" : "10px",
            right: isMobile ? "32px" : "243px",
          }}
        >
          <Image
            src="/about/profile_yellow.png"
            alt="profile_yellow"
            width={isMobile ? 25 : 69}
            height={isMobile ? 25 : 69}
          />
        </UserImageWrapper>
      </PowerHeader>
      <RecommendWrapper>
        <RecommendDesktop>
          <RecommendContentWrapper>
            {[...recommendList, ...recommendList].map((v, index) => (
              <RecommendItemWrapper key={index}>
                <RecommendItemContent>
                  <RecommendItemText>{v.text}</RecommendItemText>
                  <RecommendBottom>
                    <RecommendItemIcon>
                      {v.logo && (
                        <RecommendLogo height={v.height} src={v.logo} />
                      )}
                    </RecommendItemIcon>
                    <RecommendItemName>{v.name}</RecommendItemName>
                  </RecommendBottom>
                </RecommendItemContent>
              </RecommendItemWrapper>
            ))}
          </RecommendContentWrapper>
        </RecommendDesktop>
        <RecommendMobile>
          <RecommendContentWrapper>
            {[...recommendList].map((v, index) => (
              <RecommendItemWrapper key={index}>
                <RecommendItemContent>
                  <RecommendItemText>{v.text}</RecommendItemText>
                  <RecommendBottom>
                    <RecommendItemIcon>
                      {v.logo && (
                        <RecommendLogo height={v.height} src={v.logo} />
                      )}
                    </RecommendItemIcon>
                    <RecommendItemName>{v.name}</RecommendItemName>
                  </RecommendBottom>
                </RecommendItemContent>
              </RecommendItemWrapper>
            ))}
          </RecommendContentWrapper>
        </RecommendMobile>
        <ShadowWrapper
          style={{
            left: 0,
          }}
        />
        <ShadowWrapper
          style={{
            right: 0,
            transform: "rotate(180deg)",
          }}
        />
      </RecommendWrapper>
    </PowerWrapper>
  );
};

const PowerWrapper = styled(FlexWrapper)`
  flex-direction: column;

  padding: 136px 20px 155px;
  background-color: ${colors.WHITE};

  @media (max-width: 767px) {
    padding: 40px 20px 80px;
  }
`;

const PowerHeader = styled(FlexWrapper)`
  width: 100%;
  position: relative;

  @media (max-width: 767px) {
    width: auto;
  }
`;

const UserImageWrapper = styled.div`
  position: absolute;
`;

const Main = styled(MainDesc)`
  margin-bottom: 70px;
  @media (max-width: 767px) {
    ${typo({
      size: "24px",
      weight: 700,
      height: "160%",
    })}
    text-align: center;
    margin-bottom: 25px;
  }
`;

const Sub = styled(SubDesc)`
  margin-bottom: 25px;
  @media (max-width: 767px) {
    ${typo({
      size: "16px",
      weight: 500,
      height: "100%",
    })}
    text-align: center;
    margin-bottom: 10px;
  }
`;

const rightMoveRecommend = keyframes`
  from {
    transform: translateX(15%);
  }
  to {
    transform: translateX(-130%);
  }
`;

const RecommendWrapper = styled.div`
  width: 100%;
  max-width: 100vw;
  overflow: hidden;
`;

const ShadowWrapper = styled.div`
  position: absolute;
  top: 0;
  width: 400px;
  height: 100%;

  background: linear-gradient(90deg, #ffffff 0%, rgba(255, 255, 255, 0) 100%);
  z-index: 3;
  @media (max-width: 960px) {
    display: none;
  }
`;

const RecommendContentWrapper = styled(FlexWrapper)`
  padding: 20px 0px;

  animation: ${rightMoveRecommend} 90000ms infinite linear;
  @media (max-width: 767px) {
    animation: none;
    display: block;
    width: 100%;
  }
`;

const RecommendItemWrapper = styled.div`
  background: #ffffff;
  border: 1px solid ${colors.HOVER_PURPLE};
  border-radius: 8px;
  min-width: 448px;
  padding: 37px 51px;
  margin-right: 30px;
  @media (max-width: 767px) {
    margin-right: 0px;
    margin-bottom: 20px;
  }
  @media (max-width: 480px) {
    padding: 20px 26px;
    border-radius: 8px;
    width: 100%;
    min-width: 0;
  }
`;

const RecommendItemIcon = styled.div`
  display: flex;
  align-items: center;
`;

const RecommendItemText = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 160%;
  color: #3b3f4e;
  @media (max-width: 480px) {
    font-size: 12px;
    line-height: 160%;
    margin-bottom: 20px;
  }
`;

const RecommendItemName = styled.p`
  font-weight: 500;
  font-size: 16px;
  /* line-height: 160%; */
  color: #717488;
  @media (max-width: 480px) {
    font-size: 12px;
  }
`;

const RecommendDesktop = styled.div`
  display: block;
  @media (max-width: 767px) {
    display: none;
  }
`;

const RecommendMobile = styled.div`
  display: none;
  @media (max-width: 767px) {
    display: block;
  }
`;

const RecommendItemContent = styled.div`
  position: relative;
  width: 100%;
  white-space: normal;
  height: 180px;
  @media (max-width: 480px) {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
`;

const RecommendBottom = styled(FlexWrapper)`
  position: absolute;
  gap: 15px;
  bottom: 0px;

  width: 100%;

  padding-top: 15px;
  border-top: 1px solid ${colors.HOVER_PURPLE};

  /* justify-content: center; */
  align-items: center;
  @media (max-width: 480px) {
    position: static;
    margin-right: auto;
  }
`;

const RecommendLogo = styled.img`
  height: ${(props) => (props.height ? props.height : "17px")};
  padding-right: 10px;
`;
