import {
  ChipStyleSelector,
  colors,
  FlexButton,
  FlexWrapper,
  typo,
} from "@wrtn/ui";
import styled, { css } from "styled-components";

export const CommonWrapper = styled(FlexWrapper)`
  flex-direction: column;
  max-width: 1512px;
  padding: 136px 128px 155px;
  background-color: ${colors.WHITE};
`;

export const DynamicText = ({
  size,
  weight,
  height,
  color,
  boldColor,
}: {
  size?: string;
  weight?: number;
  height?: string;
  color?: string;
  boldColor?: string;
}) => css`
  ${typo({
    size: size || "20px",
    weight: weight || 500,
    height: height || "100%",
    color: color || colors.GRAY_90,
  })}

  b {
    color: ${boldColor || colors.POINT_PURPLE};
  }
`;

export const AboutChip = styled(FlexWrapper)<{
  type: "sm" | "md" | "lg";
}>`
  ${({ type }) => ChipStyleSelector[type]}
  white-space: nowrap;
`;

export const SubDesc = styled.p`
  ${typo({
    size: "20px",
    weight: 500,
    color: colors.GRAY_90,
  })}

  @media (max-width: 767px) {
    font-size: 14px;
  }
`;

export const MainDesc = styled.p<{
  boldColor?: string;
}>`
  ${({ boldColor }) =>
    DynamicText({
      size: "64px",
      weight: 700,
      height: "150%",
      color: colors.BLACK,
      boldColor: boldColor,
    })}

  white-space: nowrap;
  b {
    font-weight: 700;
  }

  @media (max-width: 767px) {
    font-size: 36px;
  }
`;
