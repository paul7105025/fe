import React from "react";
import styled from "styled-components";

import {
  AutoSizeTextArea,
  colors,
  FlexButton,
  FlexWrapper,
  InputStyle,
  typo,
} from "@wrtn/ui";
import {
  getJosa,
  userType,
  WRTN_MARKETING_POLICY_HREF,
  WRTN_PRIVACY_POLICY_HREF,
} from "@wrtn/core";

interface onSubmitProps {
  email: string;
  name: string;
  marketingTerm: boolean;
  opinion: string;
  kind: string;
}

interface AboutRegisterModalProps {
  user: userType | null;
  currentFeature: string;
  onClose: () => void;
  onSubmit: ({
    email,
    name,
    opinion,
    kind,
    marketingTerm,
  }: onSubmitProps) => Promise<void>;
}

const colorSelector = {
  "co-op": colors.ABOUT_YELLOW,
  image: colors.POINT_GREEN,
  ocr: colors.ABOUT_SKY_BLUE,
};

const HeaderTitleSelector = {
  "co-op": "뤼튼 단체 채팅",
  image: "이미지 생성",
  ocr: "문서 인식",
};

export const AboutRegisterModal = ({
  user,
  currentFeature,
  onClose,
  onSubmit,
}: AboutRegisterModalProps) => {
  const [name, setName] = React.useState(user?.name || "");
  const [email, setEmail] = React.useState(user?.email || "");
  const [opinion, setOpinion] = React.useState("");

  const [isPrivacy, setIsPrivacy] = React.useState(false);
  const [isMarketing, setIsMarketing] = React.useState(false);

  const submit = async () => {
    await onSubmit({
      email,
      name,
      opinion,
      kind: currentFeature,
      marketingTerm: isMarketing,
    });
  };

  return (
    <ModalWrapper column>
      <ModalHeader column>
        <ModalTitle boldColor={colorSelector[currentFeature]}>
          <b>{HeaderTitleSelector[currentFeature]}</b> 대기자 명단 등록
        </ModalTitle>
        <ModalDesc>회원가입시 대기자 선정 우선순위에 등록됩니다!</ModalDesc>
      </ModalHeader>
      <ModalBody column>
        <Label>
          <LabelTitle>이름</LabelTitle>
          <Input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="홍길동"
          />
        </Label>
        <Label>
          <LabelTitle>이메일</LabelTitle>
          <Input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="wrtn@wrtn.com"
          />
        </Label>
        <Label>
          <LabelTitle>
            {getJosa(HeaderTitleSelector[currentFeature], "을/를")} 어떻게
            활용하고 싶으신가요?
          </LabelTitle>
          <TextArea
            value={opinion}
            onChange={(e) => setOpinion(e.target.value)}
            placeholder="팀원들과 회의 중 브레인스토밍에 사용하고 싶다."
          />
        </Label>
        <TermWrapper column>
          <TermDesc>
            <b>
              개인정보 활용 동의 <span>(필수)</span>
            </b>
            <br />
            귀하께서는 위 개인정보 · 수집이용에 대한 동의를 거부하실 권리가
            있습니다.
            <br />
            다만, 동의를 거부할 경우 대기자 선발 과정에 있어 제한이 발생할 수
            있음을 알려드립니다.{" "}
            <TermLink
              href={WRTN_PRIVACY_POLICY_HREF}
              target="_blank"
              rel="noopener noreferrer"
            >
              자세히 보기
            </TermLink>
          </TermDesc>
          <TermSelectWrapper>
            <SelectLabel>
              <input
                type="radio"
                name="privacy"
                checked={isPrivacy}
                onChange={() => setIsPrivacy(true)}
              />
              동의합니다.
            </SelectLabel>
            <SelectLabel>
              <input
                type="radio"
                name="privacy"
                checked={!isPrivacy}
                onChange={() => setIsPrivacy(false)}
              />
              동의하지 않습니다.
            </SelectLabel>
          </TermSelectWrapper>
        </TermWrapper>
        <TermWrapper column>
          <TermDesc>
            <b>
              마케팅 활용 동의 <span>(선택)</span>
            </b>
            <br />
            뤼튼의 제품 관련 정보 수신 동의 절차입니다.
            <br />
            귀하께서는 위 마케팅을 위한 개인정보 수집 · 이용 및 광고성 정보
            수신에 대한 동의를 거부하실 권리가 있습니다.{" "}
            <TermLink
              href={WRTN_MARKETING_POLICY_HREF}
              target="_blank"
              rel="noopener noreferrer"
            >
              자세히 보기
            </TermLink>
          </TermDesc>
          <TermSelectWrapper>
            <SelectLabel>
              <input
                type="radio"
                name="marketing"
                checked={isMarketing}
                onChange={() => setIsMarketing(true)}
              />
              동의합니다.
            </SelectLabel>
            <SelectLabel>
              <input
                type="radio"
                name="marketing"
                checked={!isMarketing}
                onChange={() => setIsMarketing(false)}
              />
              동의하지 않습니다.
            </SelectLabel>
          </TermSelectWrapper>
        </TermWrapper>
        <ButtonWrapper>
          <Button onClick={onClose}>취소하기</Button>
          <Button onClick={submit} disabled={!name || !email || !isPrivacy}>
            확인
          </Button>
        </ButtonWrapper>
      </ModalBody>
    </ModalWrapper>
  );
};

const ModalWrapper = styled(FlexWrapper)`
  width: 620px;
  border-radius: 20px;
  background-color: ${colors.WHITE};
  overflow: hidden;
  border: 1px solid ${colors.POINT_PURPLE};

  @media (max-width: 620px) {
    width: calc(100vw - 40px);
    max-height: 95vh;
  }
`;

const ModalHeader = styled(FlexWrapper)`
  width: 100%;
  padding: 33px 0px 24px;

  background-color: ${colors.POINT_PURPLE};

  gap: 15px;
`;

const ModalTitle = styled.p<{
  boldColor: string;
}>`
  ${typo({
    size: "18px",
    weight: 700,
    color: colors.WHITE,
  })}
  b {
    color: ${({ boldColor }) => boldColor};
    font-size: 18px;
  }
`;

const ModalDesc = styled.p`
  ${typo({
    size: "16px",
    weight: 400,
    color: colors.WHITE,
  })}
`;

const ModalBody = styled(FlexWrapper)`
  width: 100%;
  padding: 26px 32px 35px;
  @media (max-width: 767px) {
    overflow: scroll;
  }
`;

const Label = styled.label`
  width: 100%;

  display: flex;
  flex-direction: column;
  gap: 13px;

  margin-bottom: 28px;
`;

const LabelTitle = styled.p`
  ${typo({
    size: "16px",
    weight: 600,
    color: colors.GRAY_90,
  })}
`;

const Input = styled.input`
  ${InputStyle}
`;

const TextArea = styled(AutoSizeTextArea)`
  ${InputStyle}
`;

const TermWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 10px;
  margin-bottom: 20px;
`;

const TermDesc = styled.p`
  width: 100%;
  ${typo({
    size: "12px",
    weight: 500,
    height: "150%",
    color: colors.GRAY_90,
  })}

  span {
    color: ${colors.POINT_PURPLE};
  }
`;

const TermSelectWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 30px;
`;

const TermLink = styled.a`
  text-decoration: underline;
  &:visited {
    color: ${colors.GRAY_90};
  }
`;

const SelectLabel = styled.label`
  display: flex;
  align-items: center;
  gap: 6px;
  ${typo({
    size: "10px",
    weight: 600,
    height: "150%",
    color: colors.GRAY_90,
  })}
`;

const ButtonWrapper = styled(FlexWrapper)`
  margin-top: 54px;

  width: 100%;
`;

const Button = styled(FlexButton)`
  flex: 1;
  padding: 12px 0px;

  ${typo({
    size: "16px",
    weight: 500,
    color: colors.GRAY_80,
  })}

  &:hover {
    background-color: ${colors.GRAY_55};
    cursor: pointer;
  }

  &:disabled {
    color: ${colors.GRAY_55};
    &:hover {
      background-color: ${colors.WHITE};
      cursor: default;
    }
  }
`;
