import React from "react";
import Image from "next/image";
import styled from "styled-components";
import { useAlert } from "react-alert";
import { useRecoilValue } from "recoil";

import { DynamicText, MainDesc, SubDesc } from "./styles";
import { FlexWrapper, colors, FlexButton, typo } from "@wrtn/ui";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { AboutRegisterModal } from "./AboutRegisterModal";
import { postWaitList, userState, useEvent } from "@wrtn/core";

interface onSubmitProps {
  email: string;
  name: string;
  marketingTerm: boolean;
  opinion: string;
  kind: string;
}

export const AboutNew = () => {
  const alert = useAlert();

  const user = useRecoilValue(userState);

  const { collectEvent } = useEvent();

  const [currentFeature, setCurrentFeature] = React.useState("none");

  const onSubmit = async ({
    email,
    name,
    opinion,
    kind,
    marketingTerm,
  }: onSubmitProps) => {
    const res = await postWaitList({
      email,
      name,
      opinion,
      kind,
      marketingTerm,
    });

    if (res.status === 200 || res.status === 201) {
      collectEvent("waitlist_done", {
        waitlist_feature: kind,
      });
      alert.show("대기자 명단에 등록되었습니다!");
      setCurrentFeature("none");
    } else {
      alert.show("대기자 명단 등록에 실패했습니다. 다시 시도해주세요.");
    }
  };

  return (
    <>
      <NewWrapper column>
        <Main>공개 예정 신기능</Main>
        <Sub>
          뤼튼에서 공개 예정인 신기능, 대기자 명단에 등록하고 더 빠르게 만나
          보세요!
        </Sub>
        <FeatureWrapper>
          <FeatureBox column align="flex-start">
            <FeatureText boldColor={colors.ABOUT_YELLOW}>
              친구와 <b>동시에 쓰는</b>
              <br /> ChatGPT
            </FeatureText>
            <div style={{ flex: 1 }} />
            <FeatureDesc boldColor={colors.ABOUT_YELLOW}>
              채팅방에서 <b>친구들과 대화</b>하며 ChatGPT를 사용할 수 있어요.
              마피아게임 사회자도 ChatGPT 에게 시켜보세요!
            </FeatureDesc>
            <FeatureImage
              style={{
                top: 30,
                right: -76,
              }}
            >
              <Image
                src="/about/feat_collaboration.png"
                alt="feat_collaboration"
                width={235}
                height={315}
              />
            </FeatureImage>
            <RegisterButton onClick={() => setCurrentFeature("co-op")}>
              대기자 명단 등록
            </RegisterButton>
          </FeatureBox>
          <FeatureBox column align="flex-start">
            <FeatureText boldColor={colors.POINT_GREEN}>
              <b>이미지</b>도 만들어주는
              <br /> 편리한 채팅
            </FeatureText>
            <div style={{ flex: 1 }} />
            <FeatureDesc boldColor={colors.POINT_GREEN}>
              채팅방에서 <b>이미지도 바로 생성</b>할 수 있어요. 복잡한 사용법
              배울 필요 없이 채팅방에 대화하듯 요청하면 바로 이미지를
              만들어줘요!
            </FeatureDesc>
            <FeatureImage
              style={{
                top: 144,
                left: 54,
              }}
            >
              <Image
                src="/about/feat_image.png"
                alt="feat_image"
                width={291}
                height={193}
              />
            </FeatureImage>
            <RegisterButton onClick={() => setCurrentFeature("none")}>
              출시 완료
            </RegisterButton>
          </FeatureBox>
          <FeatureBox column align="flex-start">
            <FeatureText boldColor={colors.ABOUT_SKY_BLUE}>
              <b>문서파일</b>도 인식하는
              <br /> 똑똑한 채팅
            </FeatureText>
            <div style={{ flex: 1 }} />
            <FeatureDesc boldColor={colors.ABOUT_SKY_BLUE}>
              pdf, docx, hwp 등 문서 파일 작업도 요청해보세요!{" "}
              <b>요약, 번역, 문서 내 검색</b> 등 다양한 기능을 지원할
              예정이에요.
            </FeatureDesc>
            <FeatureImage
              style={{
                top: 134,
                left: 68,
              }}
            >
              <Image
                src="/about/feat_doc.png"
                alt="feat_doc"
                width={263}
                height={205}
              />
            </FeatureImage>
            <RegisterButton onClick={() => setCurrentFeature("ocr")}>
              대기자 명단 등록
            </RegisterButton>
          </FeatureBox>
        </FeatureWrapper>
      </NewWrapper>
      {currentFeature !== "none" && (
        <ModalPortal onClose={() => setCurrentFeature("none")}>
          <AboutRegisterModal
            user={user}
            currentFeature={currentFeature}
            onClose={() => setCurrentFeature("none")}
            onSubmit={onSubmit}
          />
        </ModalPortal>
      )}
    </>
  );
};

const NewWrapper = styled(FlexWrapper)`
  width: 100%;
  max-width: 1256px;
  margin: 0px auto;
  padding: 155px 20px;

  @media (max-width: 767px) {
    padding: 97px 20px;
  }
  /* background-color: ${colors.SKY_BLUE}; */
`;

const Main = styled(MainDesc)`
  margin-bottom: 25px;
  @media (max-width: 1024px) {
    text-align: center;
    ${typo({
      size: "24px",
      weight: 700,
      height: "150%",
    })}
  }
  @media (max-width: 767px) {
    margin-bottom: 10px;
  }
`;

const Sub = styled(SubDesc)`
  margin-bottom: 100px;
  @media (max-width: 1024px) {
    text-align: center;
    ${typo({
      size: "16px",
      weight: 500,
      height: "150%",
    })}
  }
  @media (max-width: 767px) {
    margin-bottom: 26px;
  }
`;

const FeatureWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 30px;
  @media (max-width: 767px) {
    display: block;
  }
`;

const FeatureBox = styled(FlexWrapper)`
  flex: 1;
  background-color: ${colors.POINT_PURPLE};
  border-radius: 8px;
  padding: 36px 28px;

  overflow: hidden;
  min-height: 567px;

  @media (max-width: 767px) {
    height: 189px;
    margin-bottom: 20px;
  }
`;

const FeatureText = styled.p<{
  boldColor: string;
}>`
  ${({ boldColor }) =>
    DynamicText({
      size: "30px",
      weight: 700,
      height: "150%",
      color: colors.WHITE,
      boldColor: boldColor,
    })}
`;

const FeatureDesc = styled.p<{
  boldColor: string;
}>`
  ${({ boldColor }) =>
    DynamicText({
      size: "17px",
      weight: 500,
      height: "150%",
      color: colors.WHITE,
      boldColor: boldColor,
    })}
`;

const FeatureImage = styled(FlexWrapper)`
  position: absolute;
`;

const RegisterButton = styled(FlexButton)`
  margin-top: 30px;
  min-width: 271px;

  padding: 10px 70px;
  background-color: ${colors.WHITE};
  border-radius: 70px;

  justify-self: flex-end;
  align-self: center;

  ${typo({
    size: "20px",
    weight: 700,
    height: "160%",
    color: colors.POINT_PURPLE,
  })}

  cursor: pointer;

  &:hover {
    background-color: ${colors.HOVER_PURPLE};
    color: ${colors.WHITE};
  }

  @media (max-width: 767px) {
    width: 100%;
    min-width: 0px;
  }
`;
