import { colors, FlexWrapper } from "@wrtn/ui/styles";
import polyfill from "@wrtn/ui/styles/polyfill";
import styled from "styled-components";

export const LocalPageWrapper = styled(FlexWrapper)`
  width: 100vw;
  ${polyfill.dvh('min-height', 100)};
  background: linear-gradient(180deg, #f3f4ff -24.67%, #ffffff 103.17%);
  z-index: 2;

  flex-direction: column;

  padding: 10px;
  /* justify-content: flex-start; */
`;

export const LocalPageBox = styled(FlexWrapper)`
  position: relative;
  padding: 40px 35px;
  background: #ffffff;
  box-shadow: 5px 5px 20px rgba(0, 0, 0, 0.05);
  border-radius: 12px;

  overflow-y: scroll;

  flex-direction: column;
  justify-content: flex-start;
  height: 580px;
  width: 100%;
  max-width: 784px;
  margin: 0px 20px;

  @media (max-width: 767px) {
    max-width: 95vw;
  }
`;

export const LocalPageInnerWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;

  width: 100%;
  max-width: 460px;
  height: 100%;
`;

export const LocalPageBottomLogo = styled.div`
  position: absolute;
  top: calc(100% - 60px);
  svg {
    height: 28px;
    width: 255px;
    path {
      fill: ${colors.gray_30};
    }
  }
  @media (max-height: 720px) {
    display: none;
  }
`;
