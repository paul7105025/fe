import { useModalOpen } from "@wrtn/core";
import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

export const ChatDeleteCheckModal = () => {
  const chatDeleteCheckModal = useModalOpen({
    modalId: "chatDeleteCheckModal",
  });

  return (
    <ModalPortal>
      <Dialog
        iconType="waste"
        title="채팅방을 삭제하시겠어요?"
        description={["삭제된 채팅방은 다시 복구될 수 없어요!"]}
        rightButtonLabel="삭제하기"
        handleRightButton={() => chatDeleteCheckModal.resolve()}
        handleClose={() => chatDeleteCheckModal.close()}
      />
    </ModalPortal>
  );
};

