import { modalOpenState, useModalOpen } from "@wrtn/core";
import { LoginProvider } from "@wrtn/core/types/login";
import { Icon } from "@wrtn/ui";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

export type BanAbuseModalProps = {
  onClose: () => void;
};

export const BanAbuseModal = () => {
  const banAbuseModal = useModalOpen({ modalId: "banAbuseModal" });
  return (
    <ModalPortal onClose={banAbuseModal.close}>
      <LoginDialogWrapper width={395}>
          <CloseIcon onClick={banAbuseModal.close}/>
        <TopWrapper></TopWrapper>
        <BottomWrapper>
          <Title>악성 사용 감지에 의한 계정 차단</Title>
          <Description>
            차단된 계정은 '고객 문의/상담'을 통해 문의 주세요.
          </Description>
          <LoginWrapper>
            <Button onClick={banAbuseModal.close}>확인</Button>
          </LoginWrapper>
        </BottomWrapper>
      </LoginDialogWrapper>
    </ModalPortal>
  );
};

export default BanAbuseModal;

const TopWrapper = styled.div`
  width: 100%;
  height: 57px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 20px 20px 0px 0px;
`;

const BottomWrapper = styled.div`
  padding: 32px 22px 18px;
`;

const Title = styled.p`
  ${typo({
    weight: "700",
    size: "18px",
    height: "100%",
    color: colors.gray_90,
  })}

  text-align: center;
  margin-bottom: 22px;
`;

const Description = styled.p`
  ${typo({
    weight: "500",
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};

  text-align: center;
`;

const Button = styled(FlexButton)`
  ${typo({
    weight: 600,
    size: "16px",
    height: "16px",
    color: colors.ACTION_BLUE,
  })};
  background-color: #f2f7ff;
  width: 100%;
  padding: 12px;
  cursor: pointer;
`;

const CloseButton = styled(FlexButton)`
  position: absolute;
  top: 18px;
  right: 18px;
`;

const LoginWrapper = styled(FlexWrapper)`
  padding-top: 26px;
  padding-bottom: 20px;
  gap: 41px;
  flex-direction: column;
`;


interface ICloseIcon {
  onClick: () => void;
}

export const CloseIcon = ({ onClick }: ICloseIcon) => {
  return (
    <CloseButton onClick={onClick}>
      <Icon icon="times" size={20} color={colors.GRAY_80} />
    </CloseButton>
  );
};

export const LoginDialogWrapper = styled.div<{ width: number }>`
  position: relative;
  width: ${({ width }) => width}px;
  border-radius: 20px;

  background: ${colors.WHITE};
`;