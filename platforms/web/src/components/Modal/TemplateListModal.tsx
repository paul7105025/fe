import styled from "styled-components";
import {
  AutoSizeTextArea,
  colors,
  FlexButton,
  FlexWrapper,
  Icon,
  typo,
} from "@wrtn/ui";
import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";
import { CloseIcon } from "src/containers/LoginDialog/styles";
import { templateToolListState, useModalOpen } from "@wrtn/core";
import { useRecoilState, useRecoilValue } from "recoil";
import { selectedTemplateState } from "@wrtn/core";

export const TemplateListModal = () => {
  const modalOpen = useModalOpen({ modalId: "templateListModal" });
  const editModalOpen = useModalOpen({ modalId: "templateEditModal" });
  const addModalOpen = useModalOpen({ modalId: "templateAddModal" });
  const templateToolList = useRecoilValue(templateToolListState);
  const [template, setTemplate] = useRecoilState(selectedTemplateState);

  const openEdit = (v) => {
    setTemplate({
      id: v._id,
      name: v.name,
      content: v.content,
    });
    editModalOpen.open();
  };
  return (
    <>
      <ModalPortal onClose={modalOpen.close}>
        <Wrapper justify="center" column>
          <Header justify="flex-end">
            <AddTemplate onClick={() => addModalOpen.open()}>
              자주 쓰는 문구 추가
            </AddTemplate>
            <CloseIcon onClick={modalOpen.close} />
          </Header>
          <Content column align="flex-start">
            <Overflow column align="flex-start">
              {templateToolList.map((v) => (
                <TemplateMenu key={v.name} align="flex-start">
                  <IconWrapper onClick={() => openEdit(v)}>
                    <Icon icon="edit-alt" size={20} />
                  </IconWrapper>
                  <TemplateTitle>{v.name}</TemplateTitle>

                  <TemplateDesc maxRows={6} value={v.content} />
                </TemplateMenu>
              ))}
            </Overflow>
          </Content>
          <Bottom justify="center">
            <CancelButton onClick={() => modalOpen.close()}>
              취소하기
            </CancelButton>
            <Button onClick={() => modalOpen.close()}>확인</Button>
          </Bottom>
        </Wrapper>
      </ModalPortal>
    </>
  );
};

export default TemplateListModal;

const Wrapper = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;
  background-color: ${colors.WHITE};

  border-radius: 19px;
  border: 1px solid #d1d7ed;

  overflow: hidden;
  padding-bottom: 30px;

  @media (max-width: 767px) {
    max-width: 95vw;
  }
`;

const Header = styled(FlexWrapper)`
  background: ${colors.BACKGROUND};
  width: 100%;
  height: 47px;
`;

const AddTemplate = styled.div`
  position: absolute;
  left: 31px;
  ${typo({
    weight: 600,
    size: "12px",
    color: colors.ACTION_BLUE,
  })};
  cursor: pointer;
`;

const Content = styled(FlexWrapper)`
  width: 100%;
  max-height: 80vh;
  padding: 24px 32px;
  gap: 26px;

  overflow: scroll;
`;

const Overflow = styled(FlexWrapper)`
  width: 100%;
  gap: 6px;
  min-height: 200px;
`;

const Bottom = styled(FlexWrapper)`
  width: 100%;
  gap: 20px;
  padding: 0px 24px;
`;

const Button = styled(FlexButton)`
  background: #f2f7ff;
  border-radius: 5px;
  padding: 12px 60px;
  gap: 10px;
  width: 100%;
  cursor: pointer;
  white-space: nowrap;
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.ACTION_BLUE,
  })}
`;

const TemplateMenu = styled(FlexWrapper)`
  width: 100%;
  min-height: 46px;
  gap: 10px;
`;

const TemplateTitle = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.gray_90,
  })};
  padding-top: 5px;
  width: 145px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const TemplateDesc = styled(AutoSizeTextArea)`
  ${typo({
    weight: 600,
    size: "16px",
    height: "120%",
    color: colors.gray_60,
  })};
  width: 100%;
  padding: 5px 0px;
  flex: 1;
  resize: none;
  overflow-wrap: anywhere;
`;

const CancelButton = styled(FlexButton)`
  background: ${colors.WHITE};
  border-radius: 5px;
  padding: 12px 60px;
  gap: 10px;
  width: 100%;
  cursor: pointer;
  ${typo({
    weight: 500,
    size: "16px",
    color: colors.gray_80,
  })}
  white-space: nowrap;
`;

const IconWrapper = styled.div`
  cursor: pointer;
`;
