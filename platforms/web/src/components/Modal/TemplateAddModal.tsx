import styled from 'styled-components'
import { colors, FlexButton, FlexWrapper, Input, Textarea, typo } from "@wrtn/ui";
import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";
import { CloseIcon } from "src/containers/LoginDialog/styles";
import { deleteTemplate_Id, getTemplate, postTemplate, templateToolListState, useEvent, useFetchTemplate, useModalOpen } from '@wrtn/core';
import React from 'react';
import { useRecoilState, useSetRecoilState } from 'recoil';
import { selectedTemplateState } from '@wrtn/core';

export const TemplateAddModal = () => {
  const modalOpen = useModalOpen({ modalId: 'templateAddModal' });
  const setTemplateToolList = useSetRecoilState(templateToolListState);
  const [template, setTemplate] = useRecoilState(selectedTemplateState);
  const { fetchTemplate } = useFetchTemplate();
  const [name, setName] = React.useState(template.name || "")
  const [desc, setDesc] = React.useState(template.content || "")
  const { collectEvent } = useEvent();

  const handleAdd = async () => {
    const res = await postTemplate({
      name: name,
      content: desc
    });
    if (res?.status === 201) {
      collectEvent("add_template");
      fetchTemplate(() => {
        modalOpen.close();
      })
    }
  }

  return (
    <>
      <ModalPortal onClose={modalOpen.close}>
        <Wrapper justify="center" column>
          <Header justify="flex-start" column>
            <Title>
              자주 쓰는 문구 등록
            </Title>
            <SubTitle>
              자주 사용하는 입력을 단축어로 등록해보세요.
            </SubTitle>
            <CloseIcon onClick={modalOpen.close} />
          </Header>
          <Content column align="flex-start">
            <InputLabel>이름</InputLabel>
            <TextAreaInput rows={1} placeholder='명령어' value={name} onChange={(e) => setName(e.target.value)}/>
            <InputLabel>내용</InputLabel>
            <TextAreaInput rows={6} placeholder='실행할 명령을 입력해주세요' value={desc} onChange={(e) => setDesc(e.target.value)}/>
            <Bottom justify="center">
              <CancelButton onClick={() => modalOpen.close()}>취소하기</CancelButton>
              <Button disabled={name.length === 0 || desc.length === 0} onClick={() => handleAdd()}>등록하기</Button>
            </Bottom>
          </Content>
        </Wrapper>
      </ModalPortal>
    </>
  ) 
}

export default TemplateAddModal;

const Header = styled(FlexWrapper)`
  background: ${colors.BACKGROUND};
  width: 100%;
  height: 106px;
  border-radius: 19px 19px 0px 0px;
`

const Wrapper = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;
  border-radius: 19px;
`

const Title = styled.div`
  padding: 24px 0px 15px 0px;
  ${typo({
    weight: 700,
    size: '18px',
    color: colors.gray_90
  })};
`;

const SubTitle = styled.div`
  padding-bottom: 25px;
  ${typo({
    weight: 500,
    size: '16px',
    color: colors.gray_70
  })};
`;

const Content = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;
  padding: 24px 32px;
  gap: 26px;
  background: ${colors.WHITE};
  border-radius: 0px 0px 19px 19px;
`;

const TextAreaInput = styled(Textarea)`
  background: #F2F7FF;
  border-radius: 8px;
  ${typo({
    weight: 500,
    size: "16px",
    color: colors.gray_100,
    height: '26px'
  })}
  padding: 8px 16px;
  resize: none;
`;

const InputLabel = styled.div`
${typo({
  weight: 600,
  size: "16px",
  color: colors.gray_100
})};
`;

const Bottom = styled(FlexWrapper)`
  width: 100%;
  gap: 20px;
`

const Button = styled(FlexButton)`
  background: #F2F7FF;
  border-radius: 5px;
  padding: 12px 60px;
  gap: 10px;
  width: 100%;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: '16px',
    color: colors.ACTION_BLUE,
  })}

  &:disabled {
    cursor: default;
    background: ${colors.WHITE};
    ${typo({
      weight: 500,
      size: '16px',
      color: colors.gray_80,
    })}
  }
`;

const CancelButton = styled(FlexButton)`
  background: ${colors.WHITE};
  border-radius: 5px;
  padding: 12px 60px;
  gap: 10px;
  width: 100%;
  cursor: pointer;
  ${typo({
    weight: 500,
    size: '16px',
    color: colors.gray_80,
  })}
`;

