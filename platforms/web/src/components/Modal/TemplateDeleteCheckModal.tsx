import { deleteTemplate_Id, getTemplate, templateToolListState, useModalOpen } from "@wrtn/core";
import { Dialog } from "@wrtn/ui/components/Dialog";
import { selectedTemplateState } from "@wrtn/core";
import { useRecoilState, useSetRecoilState } from "recoil";
import { useFetchTemplate } from "@wrtn/core/hooks"
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

export const TemplateDeleteCheckModal = () => {
  const templateDeleteCheckModal = useModalOpen({ modalId: "templateDeleteCheckModal" });
  const templateEditModal = useModalOpen({ modalId: "templateEditModal" });
  const setTemplateToolList = useSetRecoilState(templateToolListState);
  const [template, setTemplate] = useRecoilState(selectedTemplateState);

  const { fetchTemplate }  = useFetchTemplate();
  const handleDelete = async () => {
    if (!template.id) return ;
    // 정말로 삭제하시겠습니까?
    const res = await deleteTemplate_Id({ id: template.id })
    if (res?.status === 200) {
      fetchTemplate(() => {
        setTemplate({
          id: "",
          name: "",
          content: "",
        });
        templateDeleteCheckModal.close();
        templateEditModal.close()
      });
    }
  }

  return (
    <ModalPortal>
      <Dialog 
        iconType="trash"
        title="자주 쓰는 문구를 삭제하시겠어요?"
        description={[
          "등록한 문구를 삭제합니다", 
          "채팅창에서 다시 등록할 수 있어요!"
        ]}
        rightButtonLabel="삭제하기"
        handleRightButton={handleDelete}
        handleClose={() => {
          templateDeleteCheckModal.close();
        }}
      />
    </ModalPortal>
  )
}

export default TemplateDeleteCheckModal;