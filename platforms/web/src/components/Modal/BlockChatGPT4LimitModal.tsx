import styled from "styled-components";
import { boxShadow, colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui";
import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";
import { CloseIcon } from "src/containers/LoginDialog/styles";
import {
  ToolType,
  TOOL_CATEGORY_RECORD,
  useCategoryList,
  useCategorySearch,
  useFavoriteIdList,
  useFetchToolList,
  useModalOpen,
} from "@wrtn/core";
import { SearchInput } from "@wrtn/ui/components/SearchInput";
import React from "react";
import { FavoriteCategoryToggleWithProvider } from "@wrtn/ui/containers/CategoryToggle/FavoriteCategoryToggle";
import { useRouter } from "next/router";

export const BlockChatGPT4LimitModal = () => {
  const modalOpen = useModalOpen({ modalId: "blockChatGPT4Limit" });
  const router = useRouter();

  return (
    <>
      <ModalPortal onClose={modalOpen.close}>
        <Dialog
          width={420}
          iconType="surprise"
          title={"플러스 요금제에서만 사용할 수 있는 기능이에요!"}
          description={[
            "무료 요금제는 GPT-4모델을 하루 10회만 사용할 수 있어요.",
            "요금제를 업그레이드하고 제한 없이 사용해보세요!",
          ]}
          handleClose={modalOpen.close}
          rightButtonLabel="요금제 업그레이드"
          handleRightButton={() => {
            // collectEvent("view_plan_page", {
            //   position: "download",
            // });
            modalOpen.close();
            router.push("/setting/plan");
          }}
          disableCancel={true}
        />
      </ModalPortal>
    </>
  );
};

export default BlockChatGPT4LimitModal;

const Wrapper = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;

  background-color: ${colors.WHITE};

  border-radius: 19px;
  border: 1px solid #d1d7ed;

  ${boxShadow.tooltip_message_shadow};
  overflow: hidden;
`;

const ContentWrapper = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;
  padding: 24px 14px;
  gap: 26px;
  background: ${colors.WHITE};
`;

const Header = styled(FlexWrapper)`
  background: ${colors.BACKGROUND};
  width: 100%;
  height: 47px;
`;

const Overflow = styled(FlexWrapper)`
  height: 500px;
  width: 100%;
  overflow: scroll;
`;
