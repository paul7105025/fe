import styled from "styled-components";
import { boxShadow, colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui";
import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";
import { CloseIcon } from "src/containers/LoginDialog/styles";
import {
  ToolType,
  TOOL_CATEGORY_RECORD,
  useCategoryList,
  useCategorySearch,
  useFavoriteIdList,
  useFetchToolList,
  useModalOpen,
} from "@wrtn/core";
import { SearchInput } from "@wrtn/ui/components/SearchInput";
import React from "react";
import { FavoriteCategoryToggleWithProvider } from "@wrtn/ui/containers/CategoryToggle/FavoriteCategoryToggle";

export const FavoriteToolModal = () => {
  const modalOpen = useModalOpen({ modalId: "favoriteModal" });

  const [searchInput, setSearchInput] = React.useState("");

  const { handleClickFavorite } = useFavoriteIdList();
  const { chatToolList } = useFetchToolList();
  const { categoryList } = useCategoryList(chatToolList);
  const searchedToolList = useCategorySearch(
    searchInput,
    chatToolList,
    categoryList
  );

  const [searchCollapsedList, setSearchCollapsedList] = React.useState([]);

  const isSearchMode = React.useMemo(
    () => searchInput.length > 0,
    [searchInput]
  );

  const currentCollapsedList = React.useMemo(
    () => (isSearchMode ? searchCollapsedList : []),
    [isSearchMode, searchCollapsedList]
  );

  React.useEffect(() => {
    setSearchCollapsedList([]);
  }, [searchInput.length]);

  return (
    <>
      <ModalPortal onClose={modalOpen.close}>
        <Wrapper justify="center" column>
          <Header justify="flex-end">
            <CloseIcon onClick={modalOpen.close} />
          </Header>
          <ContentWrapper column>
            <SearchInput
              long={true}
              value={searchInput}
              onChange={(e) => setSearchInput(e.target.value)}
              placeholder="툴 검색"
              onKeyDown={undefined}
              style={{
                width: "100%",
              }}
            />
            <Overflow column>
              {searchedToolList.map((item) => {
                return (
                  <FavoriteCategoryToggleWithProvider
                    key={item.category}
                    category={item.category}
                    searchInput={searchInput}
                    title={TOOL_CATEGORY_RECORD[item.category]}
                    initialCollapsed={true}
                    saveCollapse={() => {}}
                    data={item.data}
                    outputLoading={false}
                    selectedToolId={""}
                    onClick={(tool: ToolType) => {
                      handleClickFavorite(tool._id);
                    }}
                  />
                );
              })}
            </Overflow>
          </ContentWrapper>
        </Wrapper>
      </ModalPortal>
    </>
  );
};

export default FavoriteToolModal;

const Wrapper = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;

  background-color: ${colors.WHITE};

  border-radius: 19px;
  border: 1px solid #d1d7ed;

  ${boxShadow.tooltip_message_shadow};
  overflow: hidden;

  @media (max-width: 767px) {
    max-width: 95vw;
  }
`;

const ContentWrapper = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;
  padding: 24px 14px;
  gap: 26px;
  background: ${colors.WHITE};
`;

const Header = styled(FlexWrapper)`
  background: ${colors.BACKGROUND};
  width: 100%;
  height: 47px;
`;

const Overflow = styled(FlexWrapper)`
  height: 500px;
  width: 100%;
  overflow: scroll;
`;
