import styled from "styled-components";
import {
  boxShadow,
  colors,
  FlexButton,
  FlexWrapper,
  Input,
  Textarea,
  typo,
} from "@wrtn/ui";
import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";
import { CloseIcon } from "src/containers/LoginDialog/styles";
import {
  deleteTemplate_Id,
  getTemplate,
  putTemplate_Id,
  templateToolListState,
  useModalOpen,
} from "@wrtn/core";
import { useRecoilValue, useRecoilState, useSetRecoilState } from "recoil";
import { selectedTemplateState } from "@wrtn/core";
import React from "react";

export const TemplateEditModal = () => {
  const modalOpen = useModalOpen({ modalId: "templateEditModal" });
  const templateDeleteCheckModal = useModalOpen({
    modalId: "templateDeleteCheckModal",
  });
  const template = useRecoilValue(selectedTemplateState);

  const setTemplateToolList = useSetRecoilState(templateToolListState);

  const [tempName, setTempName] = React.useState(template.name);
  const [tempContent, setTempContent] = React.useState(template.content);

  const handleDelete = async () => {
    if (!template.id) return;
    templateDeleteCheckModal.open();
    // // 정말로 삭제하시겠습니까?
    // const res = await deleteTemplate_Id({ id: template.id })
    // if (res?.status === 200) {
    //   modalOpen.close();
    // }
  };

  const handleEdit = async () => {
    const res = await putTemplate_Id({
      id: template.id,
      name: tempName,
      content: tempContent,
    });
    if (res?.status === 200) {
      const res = await getTemplate();
      if (res?.status == 200) {
        setTemplateToolList(res?.data?.data || []);
        modalOpen.close();
      }
    }
  };

  return (
    <>
      <ModalPortal onClose={modalOpen.close}>
        <Wrapper justify="center" column>
          <Header justify="flex-start" column>
            <Title>자주 쓰는 문구 수정</Title>
            <SubTitle>자주 사용하는 입력을 단축어로 등록해보세요.</SubTitle>
            <CloseIcon onClick={modalOpen.close} />
          </Header>
          <Content column align="flex-start">
            <InputLabel>이름</InputLabel>
            <TextAreaInput
              rows={1}
              placeholder="분량증가"
              value={tempName}
              onChange={(e) => setTempName(e.target.value)}
            />
            <InputLabel>내용</InputLabel>
            <TextAreaInput
              rows={6}
              placeholder="분량을 좀 더 늘려줘. 그리고 이모지도 좀 붙여줘라"
              value={tempContent}
              onChange={(e) => setTempContent(e.target.value)}
            />
            <Bottom justify="center">
              <CancelButton onClick={() => handleDelete()}>
                삭제하기
              </CancelButton>
              <Button onClick={() => handleEdit()}>수정하기</Button>
            </Bottom>
          </Content>
        </Wrapper>
      </ModalPortal>
    </>
  );
};

export default TemplateEditModal;

const Wrapper = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;

  border-radius: 19px;

  ${boxShadow.tooltip_message_shadow};
  overflow: hidden;

  @media (max-width: 767px) {
    max-width: 95vw;
  }
`;

const Header = styled(FlexWrapper)`
  background: ${colors.BACKGROUND};
  width: 100%;
  height: 106px;
`;

const Title = styled.div`
  padding: 24px 0px 15px 0px;
  ${typo({
    weight: 700,
    size: "18px",
    color: colors.gray_90,
  })};
`;

const SubTitle = styled.div`
  padding-bottom: 25px;
  ${typo({
    weight: 500,
    size: "16px",
    color: colors.gray_70,
  })};
`;

const Content = styled(FlexWrapper)`
  max-width: 653px;
  width: 100%;
  padding: 24px 14px;
  gap: 26px;
  background: ${colors.WHITE};
`;

const TextAreaInput = styled(Textarea)`
  background: #f2f7ff;
  border-radius: 8px;
  ${typo({
    weight: 500,
    size: "16px",
    color: colors.gray_100,
    height: "26px",
  })}
  padding: 8px 16px;
  resize: none;
`;

const InputLabel = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.gray_100,
  })};
`;

const Bottom = styled(FlexWrapper)`
  width: 100%;
  gap: 20px;
`;

const Button = styled(FlexButton)`
  background: #f2f7ff;
  border-radius: 5px;
  padding: 12px 60px;
  gap: 10px;
  width: 100%;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.ACTION_BLUE,
  })}
  white-space: nowrap;
`;

const CancelButton = styled(FlexButton)`
  background: ${colors.WHITE};
  border-radius: 5px;
  padding: 12px 60px;
  gap: 10px;
  width: 100%;
  cursor: pointer;
  ${typo({
    weight: 500,
    size: "16px",
    color: colors.gray_80,
  })}
  white-space: nowrap;
`;
