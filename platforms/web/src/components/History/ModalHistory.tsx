import React from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import { useRecoilState } from "recoil";

import { CopyToClipboard } from "react-copy-to-clipboard";

import { Dialog } from "@wrtn/ui/components/Dialog";
import Loading from "@wrtn/ui/components/Loading";
import { ReportModal } from "@wrtn/ui/components/ReportModal";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import {
  HistoryDetailType,
  PlanType,
  useClickOutside,
  useEvent,
  useNonInitialEffect,
} from "@wrtn/core";
import { useHistoryAPI, dateResultFormat } from "@wrtn/core";
import { getHistory_Id } from "@wrtn/core/services";

import { typo, colors, boxShadow, FlexWrapper } from "@wrtn/ui/styles";

import {
  Close,
  Like,
  FilledLike,
  Copy,
  TrashCan,
  Bell,
  Refresh,
  Dislike,
  FilledDislike,
  Carousel,
} from "@wrtn/ui/assets/SVGComponent/Icon";
import { currentHistoryState } from "src/stores/history";
import { useQuery } from "react-query";

const BLOCK_UNPAID = (plan: PlanType) => {
  return plan === "FREE";
};

/**
 * @param {object} props.history
 * @param {function()} props.handlePrev
 * @param {function()} props.handleNext
 * @param {function()} props.onClose
 * @returns
 */

interface Props {
  id: string;
  handleNext: () => void;
  handlePrev: () => void;
  onClose: () => void;
  nextDisabled: boolean;
  prevDisabled: boolean;
  updateBackground: () => void;
  plan: PlanType;
}

interface InnerProps extends Props {
  data: HistoryDetailType;
}

export const InnerModal = ({
  handleNext,
  handlePrev,
  onClose,
  nextDisabled,
  prevDisabled,
  updateBackground,
  data,
  plan,
}: InnerProps) => {
  const router = useRouter();

  const [history, setHistory] = React.useState(data);

  const inputRef = React.useRef<HTMLTextAreaElement>(null);

  const historyAPI = useHistoryAPI({ history, setHistory });

  const [currentHistory, setCurrentHistory] =
    useRecoilState(currentHistoryState);
  const { collectEvent } = useEvent();

  const [openReport, setOpenReport] = React.useState(false);
  const [value, setValue] = React.useState(history?.output);
  const [isBlockModalOpened, setIsBlockModalOpened] = React.useState(false);

  const wrapperRef = React.useRef(null);
  const modalRef = React.useRef(null);
  const timer = React.useRef<any>(null);

  const integratedInputs = React.useMemo(() => {
    let result: Array<string> = [];

    let inputIndex = 0;
    let optionIndex = 0;

    if (history.tool.forms) {
      history.tool.forms.forEach((form) => {
        if (form.type === "option_chips") {
          result.push(history.options[optionIndex]);
          optionIndex++;
        } else {
          result.push(history.inputs?.[inputIndex]);
          inputIndex++;
        }
      });
    }

    return result;
  }, [currentHistory, history.inputs, history.options]);

  useClickOutside([wrapperRef, modalRef], "mousedown", () => {
    if (!openReport) onClose();
  });

  useNonInitialEffect(() => {
    setHistory(data);
    setValue(data.output);
    if (timer.current) clearTimeout(timer.current);
  }, [data]);

  useNonInitialEffect(() => {
    if (BLOCK_UNPAID(plan) && value !== history?.originOutput) {
      setIsBlockModalOpened(true);
      setValue(history?.originOutput);
    } else {
      if (timer.current) clearTimeout(timer.current);
      timer.current = setTimeout(async () => {
        historyAPI.Write(value, () => {
          setCurrentHistory({
            ...history,
            output: value,
          });
        });
      }, 500);
    }
  }, [value]);

  React.useEffect(() => {
    setCurrentHistory({
      ...history,
      output: value,
    });
  }, [history]);

  const handleAfterDelete = () => {
    updateBackground();
    if (prevDisabled) {
      setCurrentHistory(null);
    } else {
      handlePrev();
    }
  };

  if (history.tool === null || history.tool === undefined) return null;

  return (
    <Wrapper ref={wrapperRef}>
      <CloseIcon onClick={onClose} />
      <Header />
      <Body>
        <BodyTop>
          <ToolName>{history.tool.name}</ToolName>
          <IconWrapper>
            {history.originOutput !== value && (
              <RefreshIcon
                onClick={() =>
                  historyAPI.Reset(() => {
                    setValue(history.originOutput);
                  })
                }
              />
            )}
            {history.liked ? (
              <FilledLikeIcon
                onClick={() => {
                  collectEvent("click_like_btn", {
                    onoff: false,
                    position: "history",
                    feature_menu: "tool",
                    feature_category: history.tool.category,
                    feature_name: history.tool.name,
                    feature_tag: history.tool.tag,
                    feature_form: history.tool.kind,
                  });
                  historyAPI.Like();
                }}
              />
            ) : (
              <LikeIcon
                onClick={() => {
                  collectEvent("click_like_btn", {
                    onoff: true,
                    position: "history",
                    feature_menu: "tool",
                    feature_category: history.tool.category,
                    feature_name: history.tool.name,
                    feature_tag: history.tool.tag,
                    feature_form: history.tool.kind,
                  });
                  historyAPI.Like();
                }}
              />
            )}
            <CopyToClipboard text={history.output}>
              <CopyIcon
                onClick={() => {
                  collectEvent("click_copy_btn", {
                    position: "history",
                    feature_menu: "tool",
                    feature_category: history.tool.category,
                    feature_name: history.tool.name,
                    feature_tag: history.tool.tag,
                    feature_form: history.tool.kind,
                  });
                  historyAPI.Copy();
                }}
              />
            </CopyToClipboard>
            <DeleteIcon
              onClick={() => historyAPI.Remove(() => handleAfterDelete())}
            />
            <ReportIcon
              onClick={() => {
                collectEvent("click_report_btn", {
                  onoff: true,
                  position: "history",
                  feature_menu: "tool",
                  feature_category: history.tool.category,
                  feature_name: history.tool.name,
                  feature_tag: history.tool.tag,
                  feature_form: history.tool.kind,
                });
                historyAPI.Report(() => setOpenReport(true));
              }}
            />
            {history.disliked ? (
              <FilledDislikeIcon
                onClick={() => {
                  collectEvent("click_unlike_btn", {
                    onoff: false,
                    position: "history",
                    feature_menu: "tool",
                    feature_category: history.tool.category,
                    feature_name: history.tool.name,
                    feature_tag: history.tool.tag,
                    feature_form: history.tool.kind,
                  });
                  historyAPI.Dislike();
                }}
              />
            ) : (
              <DislikeIcon
                onClick={() => {
                  collectEvent("click_unlike_btn", {
                    onoff: true,
                    position: "history",
                    feature_menu: "tool",
                    feature_category: history.tool.category,
                    feature_name: history.tool.name,
                    feature_tag: history.tool.tag,
                    feature_form: history.tool.kind,
                  });
                  historyAPI.Dislike();
                }}
              />
            )}
          </IconWrapper>
        </BodyTop>
        <BodyMid>
          {history.tool.forms?.map((form, index) => {
            return (
              <InputWrapper key={form.id}>
                <InputLabel>{form.label}</InputLabel>
                <InputValue>
                  {integratedInputs[index].replace(/'/g, "")}
                </InputValue>
              </InputWrapper>
            );
          })}
        </BodyMid>
        <BodyBottom>
          <Textarea
            ref={inputRef}
            spellCheck={false}
            value={value}
            onChange={(e) => {
              collectEvent("click_update_output", {
                position: "history",
                feature_menu: "tool",
                feature_category: history.tool.category,
                feature_name: history.tool.name,
                feature_tag: history.tool.tag,
                feature_form: history.tool.kind,
                current_plan: plan,
              });
              setValue(e.target.value);
            }}
          />
        </BodyBottom>
        <LastEditedDate>{dateResultFormat(history.updatedAt)}</LastEditedDate>
        <IconGroup>
          <IconButton onClick={handlePrev} disabled={prevDisabled}>
            <IconLeft />
          </IconButton>
          <IconButton onClick={handleNext} disabled={nextDisabled}>
            <IconRight />
          </IconButton>
        </IconGroup>
      </Body>
      {openReport && (
        <ModalPortal>
          <ReportModal
            onClose={() => setOpenReport(false)}
            currentHistory={currentHistory}
            onSuccess={() => handleAfterDelete()}
            type="history"
            kind={history.tool.kind}
          />
        </ModalPortal>
      )}
      {isBlockModalOpened && (
        <ModalPortal>
          <div ref={modalRef}>
            <Dialog
              width={395}
              iconType="surprise"
              title="플러스 요금제에서만 사용할 수 있는 기능이에요!"
              description={[
                "<결과물 수정> 기능은 플러스 요금제에서만",
                "사용할 수 있어요. 요금제를 업그레이드하고",
                "더 많은 기능을 사용해보세요!",
              ]}
              handleClose={() => setIsBlockModalOpened(false)}
              rightButtonLabel="요금제 업그레이드"
              handleRightButton={() => {
                collectEvent("view_plan_page", {
                  position: "edit_result",
                });
                setIsBlockModalOpened(false)
                router.push("/setting/plan");
              }}
              disableCancel={true}
            />
          </div>
        </ModalPortal>
      )}
    </Wrapper>
  );
};

export const ModalHistory = (props: Props) => {
  const { id } = props;

  const { data, isLoading } = useQuery({
    queryKey: ["history", id],
    queryFn: () =>
      getHistory_Id({
        historyId: id,
      }).then((res) => res.data.data),
    keepPreviousData: false,
  });

  if (isLoading) {
    return (
      <Wrapper>
        <Header />
        <Loading />
      </Wrapper>
    );
  } else {
    return <InnerModal {...props} data={data} />;
  }
};

const Wrapper = styled.div`
  position: relative;

  width: 1020px;
  max-width: 95vw;
  height: 750px;
  max-height: 95vh;
  ${boxShadow.history_modal_shadow};
  border-radius: 30px;
  background: white;
`;

const CloseIcon = styled(Close)`
  width: 20px;
  height: 20px;

  position: absolute;
  top: 20px;
  right: 29px;

  cursor: pointer;
`;

const Header = styled.div`
  width: 100%;
  height: 63px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 30px 30px 0px 0px;
`;

const Body = styled(FlexWrapper)`
  flex-direction: column;
  width: 100%;
  height: calc(100% - 63px);

  border-radius: 0px 0px 30px 30px;

  background: ${colors.white};

  padding: 38px 92px 55px 92px;

  @media (max-width: 767px) {
    padding: 28px 20px 25px;
  }
`;

const BodyTop = styled(FlexWrapper)`
  width: 100%;

  justify-content: space-between;
  padding-bottom: 23px;
  border-bottom: 1px solid ${colors.gray_50};
`;

const BodyMid = styled(FlexWrapper)`
  padding: 29px 0px 23px 0px;
  width: 100%;

  flex-direction: column;

  gap: 16px;

  border-bottom: 1px solid ${colors.gray_50};
`;

const BodyBottom = styled(FlexWrapper)`
  padding: 20px 0px 50px;
  width: 100%;
  flex: 1;
  overflow: auto;

  ::-webkit-scrollbar {
    display: block !important;
  }
`;

const InputWrapper = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  padding-top: 4px;
  gap: 10px;

  @media (max-width: 767px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

const InputLabel = styled.div`
  min-width: 107px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  ${typo({
    weight: "500",
    size: "14px",
    height: "180%",
    color: colors.gray_70,
  })}
`;

const InputValue = styled.p`
  flex: 1;
  background: ${colors.LIGHT_BLUE};
  border-radius: 5px;

  max-height: 88px;
  overflow: auto;

  ${typo({
    size: "16px",
    weight: "500",
    height: "20px",
    color: colors.gray_70,
  })};

  padding: 6px 8px;
`;

const ToolName = styled.p`
  ${typo({
    size: "20px",
    weight: "600",
    height: "100%",
    color: colors.gray_100,
  })};
`;

const IconWrapper = styled.div`
  display: flex;
  gap: 17px;
`;

const IconGroup = styled.div`
  position: absolute;
  bottom: 45px;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  gap: 49px;
  align-items: center;

  @media (max-width: 767px) {
    bottom: 15px;
  }
`;

const LikeIcon = styled(Like)`
  width: 19px;
  height: 17px;

  > path {
    fill: ${colors.gray_55};
  }

  cursor: pointer;

  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const FilledLikeIcon = styled(FilledLike)`
  width: 19px;
  height: 17px;

  > path {
    fill: ${colors.POINT_PURPLE};
  }

  cursor: pointer;
`;

const CopyIcon = styled(Copy)`
  width: 15px;
  height: 18px;

  > path {
    fill: ${colors.gray_55};
  }

  cursor: pointer;

  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const DeleteIcon = styled(TrashCan)`
  width: 17px;
  height: 19px;

  > path {
    fill: ${colors.gray_55};
  }

  cursor: pointer;

  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const ReportIcon = styled(Bell)`
  width: 20px;
  height: 20px;

  > path {
    fill: ${colors.gray_55};
  }

  cursor: pointer;

  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const RefreshIcon = styled(Refresh)`
  width: 20px;
  height: 20px;

  > path {
    fill: ${colors.gray_55};
  }

  cursor: pointer;

  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const LastEditedDate = styled.p`
  position: absolute;
  left: 92px;
  bottom: 83px;

  ${typo({
    size: "12px",
    weight: "500",
    height: "100%",
    color: colors.gray_60,
  })}

  @media (max-width: 767px) {
    left: 42px;
    bottom: 43px;
  }
`;

const IconLeft = styled(Carousel)`
  transform: rotate(180deg);
`;

const IconButton = styled.button`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  border: none;
  background: none;
  svg {
    width: 100%;
    height: 100%;
    path {
      fill: ${colors.gray_80};
    }
  }

  &:hover {
    cursor: pointer;
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  }

  &:disabled {
    cursor: default;
    svg {
      path {
        fill: ${colors.gray_55};
      }
    }
  }
`;

const IconRight = styled(Carousel)``;

const Textarea = styled.textarea`
  border: none;

  width: 100%;
  height: 100%;

  resize: none;

  flex: 1;
  ${typo({
    size: "18px",
    height: "180%",
    weight: "500",
    color: colors.gray_100,
  })};

  @media (max-width: 767px) {
    font-size: 14px;
  }
`;

const FilledDislikeIcon = styled(FilledDislike)`
  width: 20px;
  height: 20px;
  cursor: pointer;
  path {
    fill: #5a2cda !important;
  }
  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const DislikeIcon = styled(Dislike)`
  width: 20px;
  height: 20px;
  cursor: pointer;

  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;
