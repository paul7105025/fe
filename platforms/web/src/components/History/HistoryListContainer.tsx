import React from "react";
import styled from "styled-components";

import { useEvent, useTagManager } from "@wrtn/core";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";

import { Crazy, Surprise } from "@wrtn/ui/assets";

import { HistoryCard } from "./HistoryCard";
import { ImageHistoryCard } from "./ImageHistoryCard";
import Link from "next/link";

const HistoryListContainer = ({ data, currentCategory, currentTab, open }) => {
  const historyList = data;
  const tagManager = useTagManager();
  const { collectEvent } = useEvent();

  return (
    <ContentWrapper>
      {historyList.length > 0 ? (
        currentTab === "image" ? (
          historyList?.map((item) => {
            return <ImageHistoryCard key={item._id} data={item} />;
          })
        ) : (
          historyList?.map((item) => {
            return (
              <HistoryCard
                key={item._id}
                history={item}
                currentCategory={currentCategory}
                currentTab={currentTab}
                open={open}
              />
            );
          })
        )
      ) : (
        <EmptyWrapper>
          {currentTab === "liked" ? <Crazy /> : <Surprise />}
          <EmptyText>
            {currentTab === "liked"
              ? "아직 마음에 드는 결과물이 없으신가요?"
              : `아직 아무 ${
                  currentTab === "image" ? "이미지" : "글"
                }도 생성하지
            않으셨네요!`}
          </EmptyText>
          <Link
            onClick={() => {
              tagManager({
                event: "click_empty_history",
                data: {
                  current_category: currentCategory,
                  current_tab: currentTab,
                },
              });
              tagManager({
                event: "enter_tool",
                data: {
                  category: currentCategory,
                  enter_type: "empty_history",
                },
              });
              if (currentTab !== "/image") {
                // TODO: Hard cording
                collectEvent("click_tool_card", {
                  position: "history",
                  feature_category: "copywriting",
                  feature_name: "SNS 광고문구",
                  feature_tag: "title",
                  feature_form: "tool",
                  is_recomm_feature: false,
                });
              }
            }}
            href={currentTab === "image" ? "/app/image" : "/app/tool"}
          >
            <EmptyButton>+ 생성하러 가기</EmptyButton>
          </Link>
        </EmptyWrapper>
      )}
    </ContentWrapper>
  );
};

export default HistoryListContainer;

const EmptyWrapper = styled(FlexWrapper)`
  position: absolute;
  flex-direction: column;
  top: 200px;
  width: 100%;
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  gap: 21px;

  @media (max-width: 767px) {
    top: 100px;
  }
`;

const EmptyText = styled.p`
  ${typo({
    size: "18px",
    height: "18px",
    weight: 600,
    color: colors.GRAY_60,
  })}
`;

const EmptyButton = styled(FlexButton)`
  padding: 4px 16px;
  border-radius: 5px;
  background-color: ${colors.POINT_PURPLE};
  cursor: pointer;

  ${typo({
    size: "16px",
    height: "26px",
    weight: 600,
    color: colors.WHITE,
  })}
  text-decoration: none;

  &:hover {
    opacity: 70%;
  }
`;

const ContentWrapper = styled.div`
  width: 100%;
  padding: 0px 40px 23px 40px;
  display: grid;
  gap: 27px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  position: relative;

  @media (max-width: 1439px) {
    grid-template-columns: 1fr 1fr 1fr;
  }
  @media (max-width: 767px) {
    grid-template-columns: 1fr 1fr;
  }
  @media (max-width: 480px) {
    grid-template-columns: 1fr;
    padding: 0px 20px 23px 20px;
  }
`;
