import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import {
  CircleDownArrow,
  colors,
  FlexButton,
  FlexWrapper,
  IMAGE_STYLE_SELECTOR,
  typo,
} from "@wrtn/ui";
import { AddImage, Close, Download, WrtnImageLogo } from "@wrtn/ui/assets";
import { DefaultSpinner } from "@wrtn/ui/components/DefaultSpinner";
import { outputLoadingState } from "@wrtn/core/stores";
import { dateCalculator } from "@wrtn/core";

async function download(url, name = "download", type = "png") {
  const blob = await fetch(url, {
    mode: "no-cors",
  })
    .then((res) => {
      return res.blob();
    })
    .catch((err) => {
      console.error("err : ", err);
      return null;
    });

  if (blob) {
    const newUrl = window.URL.createObjectURL(blob);
    const anchor = document.createElement("a");
    anchor.href = newUrl;

    anchor.download = `${name}.${type}`;
    anchor.click();
    window.URL.revokeObjectURL(newUrl);
  }
}

interface ImageResultContainerProps {
  type: "generate" | "history";
  output: any;
  onClose: () => void;

  handleImageIndex?: (type: "prev" | "next") => void;
  isEnable?: (type: "prev" | "next") => boolean;
  ratio?: string;
}

export const ImageResultContainer = ({
  type = "generate",
  output,

  handleImageIndex,
  isEnable,
  ratio,

  onClose,
}: ImageResultContainerProps) => {
  const outputLoading = useRecoilValue(outputLoadingState);

  const image = output?.imageUrl || null;
  const text = output?.text || null;

  const [imageLoading, setImageLoading] = React.useState(false);

  React.useEffect(() => {
    if (image) {
      setImageLoading(true);
    }
  }, [image]);

  return (
    <ArtContainer>
      <ArtResultHeader type={type}>
        <WrtnImageLogo />
        <div style={{ flex: 1 }} />
        {type === "generate" && handleImageIndex && isEnable && (
          <>
            <CircleArrowButton
              onClick={() => handleImageIndex("prev")}
              isEnable={!imageLoading && isEnable("prev")}
            >
              <LeftCircleArrow />
            </CircleArrowButton>
            <CircleArrowButton
              onClick={() => handleImageIndex("next")}
              isEnable={!imageLoading && isEnable("next")}
            >
              <RightCircleArrow />
            </CircleArrowButton>
            <div style={{ minWidth: 19 }} />
          </>
        )}
        {/* {type === "generate" && ( */}
        <SaveButton
          disabled={imageLoading || outputLoading || !image}
          onClick={() => {
            if (image && text) {
              download(image, text, "png");
            }
          }}
        >
          <Download />
          저장하기
        </SaveButton>
        {/* )} */}
        {type === "history" && (
          <CloseButton onClick={onClose}>
            <CloseIcon />
          </CloseButton>
        )}
      </ArtResultHeader>
      <ArtResultBody>
        {image ? (
          <ArtDefaultContent>
            {outputLoading && (
              <ArtInnerShadowContainer>
                <DefaultSpinner width={24} height={24} color={colors.WHITE} />
                <CreateText>AI가 이미지를 생성하는 중이에요!</CreateText>
              </ArtInnerShadowContainer>
            )}
            {imageLoading && (
              <ArtInnerShadowContainer>
                <DefaultSpinner width={24} height={24} color={colors.WHITE} />
              </ArtInnerShadowContainer>
            )}
            <ArtImage
              src={image}
              alt={
                ratio && ratio === "16:9"
                  ? "16:9 생성된 이미지"
                  : "5:5 생성된 이미지"
              }
              onLoad={() => setImageLoading(false)}
            />
          </ArtDefaultContent>
        ) : (
          <ArtDefaultContent>
            <ArtDefaultImage
              style={{ width: "100%" }}
              src={
                ratio && ratio === "16:9"
                  ? require("../../assets/images/img_frame_16_9.png")
                  : require("../../assets/images/img_frame_5_5.png")
              }
              alt={
                ratio && ratio === "16:9"
                  ? "16:9 이미지 프레임"
                  : "5:5 이미지 프레임"
              }
            />
            <ArtInnerContainer>
              {outputLoading ? (
                <>
                  <DefaultSpinner width={24} height={24} color={colors.WHITE} />
                  <CreateText>AI가 이미지를 생성하는 중이에요!</CreateText>
                </>
              ) : (
                <>
                  <AddImage />
                  <CreateText>이미지를 생성해보세요!</CreateText>
                </>
              )}
            </ArtInnerContainer>
          </ArtDefaultContent>
        )}
        {type === "history" && (
          <ArtResultFooter>
            <Time>{dateCalculator(output?.createdAt)}</Time>
            <Text>{output?.text}</Text>
            <Row>
              <Style>{IMAGE_STYLE_SELECTOR[output?.style]}</Style>
              <Style>{output?.size}</Style>
            </Row>
          </ArtResultFooter>
        )}
      </ArtResultBody>
    </ArtContainer>
  );
};

const ArtContainer = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;

  border-radius: 12px;

  flex: 1;

  max-width: 100%;

  min-width: 600px;
  max-width: 40vw;

  @media (max-width: 1600px) {
    min-width: 500px;
  }
  @media (max-width: 1300px) {
    min-width: 400px;
  }
  @media (max-width: 1200px) {
    min-width: 360px;
  }
  @media (max-width: 1023px) {
    min-width: 60vw;
  }
  @media (max-width: 767px) {
    min-width: 85vw;
  }
  @media (max-width: 480px) {
    min-width: 92vw;
  }

  max-height: 90vh;
`;

const ArtResultHeader = styled(FlexWrapper)<{ type: string }>`
  padding: ${({ type }) =>
    type === "generate" ? "16px 40px" : "16px 20px 16px 40px"};
  justify-content: flex-start;

  width: 100%;

  border-radius: 12px 12px 0px 0px;
  background-color: ${colors.POINT_PURPLE_2};

  @media (max-width: 1200px) {
    padding: 16px 20px;
  }
`;

const CircleArrowButton = styled(FlexButton)<{ isEnable: boolean }>`
  opacity: ${({ isEnable }) => (isEnable ? 1 : 0.5)};
  cursor: pointer;

  &:hover {
    opacity: ${({ isEnable }) => (isEnable ? 0.8 : 0.5)};
  }
`;

const LeftCircleArrow = styled(CircleDownArrow)`
  transform: rotate(90deg);
`;

const RightCircleArrow = styled(CircleDownArrow)`
  transform: rotate(-90deg);
`;

const SaveButton = styled(FlexButton)`
  padding: 8px 12px;
  background-color: ${colors.POINT_PURPLE};
  border-radius: 4px;
  gap: 8px;

  cursor: ${({ disabled }) => (!disabled ? "pointer" : "default")};

  ${typo({
    size: "14px",
    color: colors.white,
  })}

  opacity: ${({ disabled }) => (!disabled ? 1 : 0.5)};

  &:hover {
    background-color: ${colors.POINT_PURPLE_HOVER};
  }
`;

const ArtResultBody = styled(FlexWrapper)`
  padding: 44px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  overflow: auto;

  width: 100%;

  border-radius: 0px 0px 12px 12px;
  background-color: ${colors.POINT_PURPLE_3};

  @media (max-width: 1600px) {
    padding: 44px;
  }
  @media (max-width: 1300px) {
    padding: 36px;
  }
  @media (max-width: 1200px) {
    padding: 28px;
  }
  @media (max-width: 1023px) {
    padding: 44px;
  }
  @media (max-width: 767px) {
    padding: 36px;
  }
  @media (max-width: 480px) {
    padding: 22px;
  }
`;

const ArtDefaultContent = styled(FlexWrapper)`
  position: relative;
  width: 100%;
  flex: 1;
  border-radius: 8px;
`;

const ArtDefaultImage = styled.img`
  width: 100%;

  user-select: none;
`;

const ArtInnerContainer = styled(FlexWrapper)`
  flex-direction: column;
  width: 100%;
  height: 100%;

  position: absolute;
  top: 0;
  left: 0;

  gap: 8px;
`;

const ArtInnerShadowContainer = styled(ArtInnerContainer)`
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6));
`;

const ArtImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 8px;
`;

const CreateText = styled.p`
  ${typo({
    size: "16px",
    color: colors.WHITE,
  })}

  user-select: none;
`;

const CloseButton = styled(FlexButton)`
  width: 34px;
  height: 34px;

  margin-left: 10px;

  cursor: pointer;
`;

const CloseIcon = styled(Close)`
  width: 20px;
  height: 20px;
  path {
    fill: ${colors.WHITE};
  }
`;

const ArtResultFooter = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`;

const Time = styled.p`
  margin-top: 16px;

  ${typo({
    size: "14px",
    color: colors.WHITE,
  })}
`;

const Text = styled.p`
  margin-top: 10px;

  ${typo({
    size: "20px",
    color: colors.WHITE,
  })}
`;

const Row = styled(FlexWrapper)`
  gap: 9px;

  justify-content: flex-start;
`;

const Style = styled.div`
  margin-top: 18px;

  ${typo({
    size: "14px",
    color: colors.WHITE,
  })}

  padding : 8px;

  border: 1px solid ${colors.WHITE};
  border-radius: 4px;
`;
