import React from "react";
import { useRecoilState } from "recoil";

import { HistoryMetaData, useHistoryAPI, useTagManager } from "@wrtn/core";
import { currentHistoryState } from "src/stores/history";
import { InnerHistoryCard } from "./InnerHistoryCard";

export const HistoryCard = (props) => {
  const [history, setHistory] = React.useState<HistoryMetaData>(props.history);
  const tagManager = useTagManager();
  const [currentHistory, setCurrentHistory] =
    useRecoilState(currentHistoryState);

  React.useEffect(() => {
    if (currentHistory?._id === history._id) {
      setHistory({
        ...history,
        ...currentHistory,
      });
    }
  }, [currentHistory]);

  const { Like } = useHistoryAPI({ history, setHistory });

  return (
    <InnerHistoryCard
      history={history}
      onClick={() => {
        setCurrentHistory(props.history);
        props.open();
        tagManager({
          event: "click_history_card",
          data: {
            history_id: history._id,
            category: history.tool.category,
            current_category: props.currentCategory,
            current_tab: props.currentTab,
          },
        });
      }}
      handleClickFavorite={(e) => {
        e.stopPropagation();
        tagManager({
          event: history.liked
            ? "click_history_card_delete_like"
            : "click_history_card_add_like",
          data: {
            history_id: history._id,
            category: history.tool.category,
            current_category: props.currentCategory,
            current_tab: props.currentTab,
          },
        });
        Like();
      }}
    />
  );
};
