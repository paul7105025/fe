import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styled, { css } from "styled-components";

import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import { historyToExcel } from "src/utils/historyToExcel";

import { colors, Download, FlexButton, FlexWrapper, typo } from "@wrtn/ui";

import { PlanType, PLAN_INFO, PLAN_NAME, useEvent } from "@wrtn/core";
import { getHistoryExcel } from "@wrtn/core/services";

interface PayWrapperProps {
  plan: PlanType;
  count: number;
}

const HistoryPayWrapper = ({ plan, count }: PayWrapperProps) => {
  const [isBlockModalOpened, setIsBlockModalOpened] = React.useState(false);
  const { collectEvent } = useEvent();

  const router = useRouter();

  const downloadAllHistory = async () => {
    collectEvent("click_download_excel", {
      current_plan: plan,
    });
    if (PLAN_INFO[plan].downloadAvailable) {
      const res = await getHistoryExcel();

      if (res.status === 200) {
        const data = res.data.data;

        historyToExcel(data);
      }
    } else {
      setIsBlockModalOpened(true);
    }
  };

  return (
    <PayWrapper>
      <InnerWrapper>
        <CurrentPay>{PLAN_NAME[plan]} 요금제 사용 중</CurrentPay>
        <Divider />
        <OpenHistoryDate>
          최근 {PLAN_INFO[plan].historyDeadline}일까지 열람 가능
        </OpenHistoryDate>
      </InnerWrapper>
      <InnerWrapper style={{ flex: 1 }}>
        {plan === "FREE" && (
          <UpgradeLink href="/setting/plan">요금제 업그레이드</UpgradeLink>
        )}
        <div style={{ flex: 1 }} />
        <DownloadButton
          disabled={Number.isInteger(count) && count === 0}
          onClick={downloadAllHistory}
        >
          <DownloadIcon />
          <DownloadText>전체 다운로드</DownloadText>
        </DownloadButton>
      </InnerWrapper>
      {isBlockModalOpened && (
        <ModalPortal>
          <Dialog
            width={395}
            iconType="surprise"
            title={"플러스 요금제에서만 사용할 수 있는 기능이에요!"}
            description={[
              "<결과물 다운로드> 기능은 플러스 요금제에서만",
              "사용할 수 있어요. 요금제를 업그레이드하고",
              "더 많은 기능을 사용해보세요!",
            ]}
            handleClose={() => setIsBlockModalOpened(false)}
            rightButtonLabel="요금제 업그레이드"
            handleRightButton={() => {
              collectEvent("view_plan_page", {
                position: "download",
              });
              setIsBlockModalOpened(false)
              router.push("/setting/plan");
            }}
            disableCancel={true}
          />
        </ModalPortal>
      )}
    </PayWrapper>
  );
};

export default HistoryPayWrapper;

const PayWrapper = styled(FlexWrapper)`
  justify-content: flex-start;
  padding: 0px 40px 40px;

  ${typo({
    size: "14px",
    weight: "600",
    color: colors.GRAY_58,
  })}

  @media (max-width: 767px) {
    flex-direction: column;
    padding: 0px 30px 30px;
    gap: 10px;
  }
`;

const InnerWrapper = styled(FlexWrapper)`
  @media (max-width: 767px) {
    width: 100%;
    justify-content: flex-start;
  }
`;

const CurrentPay = styled.span`
  font-weight: 700;
`;

const Divider = styled.div`
  width: 1px;
  height: 12.5px;

  background-color: ${colors.GRAY_58};

  margin: 0px 5px;
`;

const OpenHistoryDate = styled.span`
  margin-right: 12px;
`;

const UpgradeLink = styled(Link)`
  color: ${colors.ACTION_BLUE};
  text-decoration: underline;

  cursor: pointer;
`;

const DownloadButton = styled(FlexButton)`
  padding: 6px;
  gap: 9px;

  cursor: pointer;

  ${typo({
    size: "14px",
    weight: "600",
    color: colors.GRAY_60,
  })}

  ${({ disabled }) =>
    disabled
      ? css`
          cursor: not-allowed;
          color: ${colors.GRAY_55};
          svg {
            path {
              fill: ${colors.GRAY_55};
            }
          }
        `
      : css`
            &:hover {
                color: ${colors.POINT_PURPLE};
                svg {
                    path {
                        fill: ${colors.POINT_PURPLE};
                    }
                }
      
      `}
`;

const DownloadIcon = styled(Download)`
  width: 15px;
  height: 15px;

  path {
    fill: ${colors.GRAY_60};
  }
`;

const DownloadText = styled.span``;
