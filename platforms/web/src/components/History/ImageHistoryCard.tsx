import React from "react";
import styled from "styled-components";
import { useSetRecoilState } from "recoil";

import {
  boxShadow,
  colors,
  FlexButton,
  FlexWrapper,
  typo,
} from "@wrtn/ui/styles";
import { dateCalculator } from "@wrtn/core";

import { IMAGE_STYLE_SELECTOR } from "@wrtn/ui";
import { currentImageState } from "src/stores/history";

export const ImageHistoryCard = ({ data }) => {
  const setCurrentImage = useSetRecoilState(currentImageState);

  return (
    <Wrapper
      onClick={() => {
        setCurrentImage(data);
      }}
    >
      <Image src={data.imageUrl} />
      <Body>
        <DateLabel>{dateCalculator(data.createdAt)}</DateLabel>
        <PromptLabel>{data.text}</PromptLabel>
        <TypeLabel>
          {IMAGE_STYLE_SELECTOR[data.style]}, {data?.size}
        </TypeLabel>
      </Body>
    </Wrapper>
  );
};

const Wrapper = styled(FlexButton)`
  flex-direction: column;

  ${boxShadow.basic_card_shadow}

  overflow: hidden;

  border-radius: 16px;
  border: 1px solid ${colors.WHITE_BOLDER_LINE};

  cursor: pointer;
  &:hover {
    filter: drop-shadow(10px 10px 24px rgba(79, 68, 196, 0.1));
  }
`;

const Image = styled.img`
  object-fit: cover;

  width: 100%;
  height: 184px;
`;

const Body = styled(FlexWrapper)`
  background-color: ${colors.WHITE};
  flex-direction: column;
  padding: 16px 20px;
  gap: 12px;

  width: 100%;

  justify-content: flex-start;
  align-items: flex-start;
`;

const DateLabel = styled.span`
  ${typo({
    size: "12px",
    color: colors.GRAY_60,
  })}
`;

const PromptLabel = styled.p`
  ${typo({
    size: "16px",
    height: "140%",
    color: colors.BLACK,
  })}
  text-align: left;
`;

const TypeLabel = styled.span`
  ${typo({
    size: "14px",
    color: colors.GRAY_70,
  })}
`;
