import React from "react";
import styled from "styled-components";

import { dateCalculator, HistoryMetaData } from "@wrtn/core";

import { TOOL_ICON_SELECTOR, TOOL_TAG_SELECTOR } from "@wrtn/ui/types";

import {
  typo,
  colors,
  SmallTag,
  FlexWrapper,
  boxShadow,
} from "@wrtn/ui/styles";

import { Like, FilledLike } from "@wrtn/ui/assets/SVGComponent/Icon";

// TODO: SVG 별로 크기가 달라서 크기 조정 필요
/**
 * @param {object} props.history
 * @param {function} props.handleClickFavorite
 * @param {function} props.onClick
 */

interface Props {
  history: HistoryMetaData;
  handleClickFavorite: (e: React.MouseEvent<HTMLDivElement>) => void;
  onClick: () => void;
}

export const InnerHistoryCard = ({
  history,
  handleClickFavorite,
  onClick,
}: Props) => {
  const { createdAt, liked, tool, output, originOutput } = history;
  const { icon, tag, name: toolName } = tool;

  const isEdited = output !== originOutput;

  return (
    <Wrapper onClick={onClick}>
      <Header>
        <DateLabel>
          <LabelSpan>{dateCalculator(createdAt)}</LabelSpan>
          {isEdited && <LabelSpan> · </LabelSpan>}
          {isEdited && <LabelSpan>수정됨</LabelSpan>}
        </DateLabel>
        <LikeButton onClick={handleClickFavorite}>
          {liked ? <LogoFilledLike /> : <LogoLike />}
        </LikeButton>
      </Header>
      <Body>
        <ValueLabel>{output}</ValueLabel>
      </Body>
      <Footer>
        <LogoIcon>{TOOL_ICON_SELECTOR[icon]}</LogoIcon>
        {tag && tag !== "none" && (
          <SmallTag color={TOOL_TAG_SELECTOR[tag]?.color}>
            {TOOL_TAG_SELECTOR[tag]?.name}
          </SmallTag>
        )}
        <FooterLabel>{toolName}</FooterLabel>
      </Footer>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  filter: drop-shadow(4px 4px 10px rgba(79, 68, 195, 0.05));

  &:hover {
    ${boxShadow.history_card_hover_shadow};
  }

  position: relative;

  width: 100%;
  min-height: 220px;
  border: 1px solid #ecf0ff;
  border-radius: 20px;
  padding: 26px 27px;
  background: white;
  cursor: pointer;
  &:hover {
    filter: drop-shadow(10px 10px 24px rgba(79, 68, 196, 0.1));
  }
`;

const Header = styled.div``;

const Body = styled.div`
  padding: 14px 0px;
`;

const Footer = styled(FlexWrapper)`
  width: 100%;
  gap: 8px;

  position: absolute;
  justify-content: flex-start;

  left: 0;
  bottom: 0;
  padding: 19px 0px 23px 27px;
  border-top: 1.5px solid #ecf0ff;
  border-radius: 0.5px;
`;

const ValueLabel = styled.p`
  ${typo({
    weight: "500",
    size: "16px",
    height: "145%",
    color: colors.gray_90,
  })};
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  max-width: 100%;
  word-break: break-all;
`;

const DateLabel = styled.p`
  ${typo({
    weight: "500",
    size: "12px",
    height: "145%",
    color: colors.gray_60,
  })};
`;

const LabelSpan = styled.span`
  padding: 0px 2px;
`;

const FooterLabel = styled.p`
  ${typo({
    weight: "600",
    size: "16px",
    height: "100%",
    color: colors.gray_60,
  })}
`;

const LikeButton = styled.div`
  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const LogoLike = styled(Like)`
  position: absolute;
  top: 25px;
  right: 20px;

  cursor: pointer;

  path {
    fill: ${colors.gray_55};
  }
`;

const LogoFilledLike = styled(FilledLike)`
  position: absolute;
  top: 25px;
  right: 20px;

  cursor: pointer;

  path {
    fill: #5a2cda;
  }
`;

const LogoIcon = styled.div`
  svg {
    width: 18px;
    height: 18px;
    display: block;
    margin: 0 auto;
    text-align: center;
    g {
      transform: scale(1);
      transform-origin: center center;
    }
  }
`;
