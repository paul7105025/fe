import { LogoAppleRound } from "@wrtn/ui/assets";
import AppleSignin from "react-apple-signin-auth"

export const AppleLoginButton = ({
  isDefaultStyle,
  onSuccess,
  onFail,
}) => {  
  return (
    <AppleSignin
      authOptions={{
        clientId: process.env.NEXT_PUBLIC_APPLE_CLIENT_ID || process.env.REACT_APP_APPLE_CLIENT_ID,
        scope: 'email name',
        redirectURI: process.env.NEXT_PUBLIC_BASE_URL || process.env.REACT_APP_BASE_URL,
        responseType: "code",
        usePopup: true,
      }}
      onSuccess={onSuccess}
      onError={onFail}
      buttonExtraChildren="Apple 아이디로 시작하기"
      style={{ height: "38px", fontSize: "0.875rem", fontWeight: 600 }}
      render={isDefaultStyle ? undefined : (props) => <LogoAppleRound onClick={() => props.onClick()} />}
    />
  )
}