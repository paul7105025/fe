import { IconLoginNaver } from "@wrtn/ui/assets";
import { colors, typo } from "@wrtn/ui/styles";
import styled from 'styled-components';
import NaverLogin from "@wrtn/core/utils/login/naver";
import { LogoNaverRound } from "@wrtn/ui";

const NAVER_CLIENT_ID = process.env.NEXT_PUBLIC_NAVER_CLIENT_ID;

export type NaverLoginRoundButtonProps = {
  onSuccess: Function,
  onFailure: Function,
  callbackUrl: string
}

export const NaverLoginRoundButton = ({
  onSuccess,
  onFailure,
  callbackUrl,

}: NaverLoginRoundButtonProps) => {
  return (
    // @ts-ignore
    <NaverLogin
      clientId={NAVER_CLIENT_ID}
      callbackUrl={callbackUrl}
      onSuccess={onSuccess}
      onFailure={onFailure}
      isSignedIn={true}
      render={(props) => (
        <LogoNaverRound onClick={() => props.onClick()}/>
      )}
    />
  )
}

const LoginLink = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  background: ${colors.BRAND_NAVER_MAIN};
  height: 55px;
  border-radius: 100%;
  width: 55px;
  cursor: pointer;
  ${typo({ weight: 600, size: "14px", height: "100%" })};
  color: ${ colors.WHITE};

  @media (max-width: 480px) {
    font-size: 14px;
  }
`;;