import { IconMail } from "@wrtn/ui/assets";
import { boxShadow, colors, typo } from "@wrtn/ui/styles";
import { MouseEventHandler } from "react";
import styled from 'styled-components';

type LocalLoginButtonProps = {
  onClick: MouseEventHandler;
  text?: string
}
export const LocalLoginButton = ({
  onClick,
  text = "뤼튼 계정으로 시작하기"
}: LocalLoginButtonProps) => {
  return (
    <EmailLoginLink type="local" onClick={onClick}>
      <IconMail width="17" height="17" />
      {text}
    </EmailLoginLink>
  )
}

export default LocalLoginButton;

const LoginLink = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 48px;
  cursor: pointer;
  background: ${colors.WHITE};
  height: 38px;
  border-radius: 5px;
  width: 300px;
  max-width: 400px;
  min-width: 200px;
  padding: 0px 25px;
  cursor: pointer;
  ${typo({ weight: 600, size: "14px", height: "100%" })};
  color: ${colors.gray_80};
  ${boxShadow.login_box_shadow};

  @media (max-width: 480px) {
    font-size: 14px;
  }
`;

const EmailLoginLink = styled(LoginLink)`
  border: 1px solid #dee4f3;
`;
