import { GoogleLogin } from "@react-oauth/google";
import { Cafe24LoginButton } from "./Cafe24LoginButton";
import KaKaoLogin from "./KaKaoLoginButton";
import KakaoLoginRound from './KaKaoLoginRoundButton';
import { LocalLoginButton } from "./LocalLoginButton";
import { NaverLoginButton } from "./NaverLoginButton";
import { NaverLoginRoundButton } from "./NaverLoginRoundButton";
import { AppleLoginButton } from "./AppleLoginButton";

const LoginProviderButton = () => {
  return (<></>)
}

LoginProviderButton.Kakao = KaKaoLogin;
LoginProviderButton.KakaoRound = KakaoLoginRound;
LoginProviderButton.Naver = NaverLoginButton;
LoginProviderButton.NaverRound = NaverLoginRoundButton;
LoginProviderButton.Google = GoogleLogin;
LoginProviderButton.Local = LocalLoginButton;
LoginProviderButton.Cafe24 = Cafe24LoginButton;
LoginProviderButton.Apple = AppleLoginButton;

// LoginProviderButton.GoogleLoginRound = GoogleLoginRound;

export default LoginProviderButton;