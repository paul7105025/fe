import { IconLoginNaver } from "@wrtn/ui/assets";
import { colors, typo } from "@wrtn/ui/styles";
import styled from 'styled-components';
import NaverLogin from "@wrtn/core/utils/login/naver";

const NAVER_CLIENT_ID = process.env.NEXT_PUBLIC_NAVER_CLIENT_ID;

export type NaverLoginButtonProps = {
  onSuccess: Function,
  onFailure: Function,
  callbackUrl: string
}

export const NaverLoginButton = ({
  onSuccess,
  onFailure,
  callbackUrl,

}: NaverLoginButtonProps) => {
  return (
    // @ts-ignore
    <NaverLogin
      clientId={NAVER_CLIENT_ID}
      callbackUrl={callbackUrl}
      onSuccess={onSuccess}
      onFailure={onFailure}
      isSignedIn={true}
      render={(props) => (
        <LoginLink
          onClick={() => props.onClick()}
          type="naver">
          <IconLoginNaver width="14" height="14" />
          네이버 아이디로 시작하기
        </LoginLink>
      )}
    />
  )
}

const LoginLink = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 48px;
  cursor: pointer;
  background: ${colors.BRAND_NAVER_MAIN};
  height: 38px;
  border-radius: 5px;
  width: 300px;
  max-width: 400px;
  min-width: 200px;
  padding: 0px 25px;
  cursor: pointer;
  ${typo({ weight: 600, size: "14px", height: "100%" })};
  color: ${ colors.WHITE};

  @media (max-width: 480px) {
    font-size: 14px;
  }
`;;