import { IconLoginKakao } from "@wrtn/ui/assets";
import { colors, typo } from "@wrtn/ui/styles";
import React from "react";
import styled from "styled-components";
import { LogoKakaoRound } from "@wrtn/ui";

const loadSdk = () => {
  return new Promise((resolve) => {
    const js = document.createElement("script");

    js.id = "kakao-sdk";
    js.src = "//developers.kakao.com/sdk/js/kakao.min.js";
    js.onload = resolve;

    document.body.append(js);
  });
};

class KaKaoLoginRound extends React.PureComponent {
  state = { isLoggedIn: false };

  async componentDidMount() {
    await loadSdk();
    window.Kakao?.init("1a46f08d916c3f63913205110dc509ab");
  }

  onButtonClick = () => {
    const {
      throughTalk = true,
      persistAccessToken = true,
      needProfile = true,
      useLoginForm = false,
      onSuccess,
      onFail,
    } = this.props;

    const method = useLoginForm ? "loginForm" : "login";

    // eslint-disable-next-line no-unsafe-optional-chaining
    (window.Kakao?.Auth)[method]({
      throughTalk,
      persistAccessToken,
      success: (response) => {
        this.setState({ isLoggedIn: true });

        if (needProfile) {
          window.Kakao?.API.request({
            url: "/v2/user/me",
            success: (profile) => {
              const result = { response, profile };
              onSuccess(result);
            },
            fail: onFail,
          });
        } else {
          onSuccess({ response });
        }
      },
      fail: onFail,
    });
  };

  onLogout = () => {
    window.Kakao?.Auth.logout(() => {
      this.props.onLogout?.(window.Kakao?.Auth.getAccessToken());
      this.setState({ isLoggedIn: false });
    });
  };

  render() {
    const { isLoggedIn } = this.state;
    const onClick = isLoggedIn ? this.onLogout : this.onButtonClick;
    const {
      render,
      className = "",
    } = this.props;

    if (typeof render === "function") {
      return render({ onClick });
    }

    return (
      <LogoKakaoRound onClick={onClick}/>
    );
  }
}

const LoginLink = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  background: ${colors.BRAND_KAKAO_MAIN};
  border-radius: 100%;
  width: 55px;
  height: 55px;
  padding: 0px 25px;
  cursor: pointer;
  ${typo({ weight: 600, size: "14px", height: "100%" })};
  color: ${colors.gray_80};

  @media (max-width: 480px) {
    font-size: 14px;
  }
`;

export default KaKaoLoginRound;
