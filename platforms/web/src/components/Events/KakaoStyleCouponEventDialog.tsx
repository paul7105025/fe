import React from "react";
import { Close } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";
import { useRouter } from "next/router";
import { chatToggleState, useEvent } from "@wrtn/core";
import { IconPaperPlane } from "@wrtn/ui/assets"
import { useSetRecoilState } from "recoil";
import { ReactComponent as Component1 } from "src/icons/1.svg"
import { ReactComponent as Component2 } from "src/icons/2.svg"
import { ReactComponent as Component3 } from "src/icons/3.svg"
import { ReactComponent as Component4 } from "src/icons/4.svg"
import { ReactComponent as ComponentBlackArrow } from "src/icons/black_right_arrow.svg"
import { ReactComponent as ComponentPurpleArrow } from "src/icons/purple_right_arrow.svg"

interface KakaoStyleCouponEventDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
}

const KakaoStyleCouponEventDialog = ({
  onClose,
  onCloseDay,
}: KakaoStyleCouponEventDialogProps) => {
  const router = useRouter();
  const { collectEvent } = useEvent();
  const setChatToggle = useSetRecoilState(chatToggleState);
  const navigateToCoupon = () => { 
    router.push('/setting/coupon')
    collectEvent('click_event_modal_button', {
      modal_name: '카카오 스타일 쿠폰 이벤트'
    })
    onCloseDay();
  };

  return (
    <Wrapper>
      {/* <Image
        src="/event/220425_kakaotalk_gpt4_unlimited_event.png"
        alt="version_2_bg"
        fill
        style={{
          position: "absolute",
          zIndex: -1,
          backgroundColor: colors.WHITE,
          borderRadius: 20,
        }}
      /> */}
      <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton>
      <Title>
        플러스 요금제 50% 할인 이벤트
      </Title>
      <Period>2023.5.15.(월) ~ 2023.6.11.(일)</Period>

      <FlexWrapper row style={{gap: '16px'}}>
        <FlexWrapper column>
          <Chip bgColor="black">일반 고객</Chip>
          <Price style={{"textDecorationLine": "line-through"}}>월 34,900원</Price>
        </FlexWrapper>
        <PurpleArrow/>
        <FlexWrapper column>
          <Chip bgColor="#6446ff">혜택가</Chip>
          <Price>월 17,450원</Price>
        </FlexWrapper>
      </FlexWrapper>

      <ComponentWrapper column>
        <ComponentHeader>이용 방법</ComponentHeader>
        <InnerComponentWrapper row>
          <Component1/>
          <ComponentBlackArrow/>
          <Component2/>
          <ComponentBlackArrow/>
          <Component3/>
          <ComponentBlackArrow/>
          <Component4/>
        </InnerComponentWrapper>
      </ComponentWrapper>

      <LinkButton onClick={navigateToCoupon}>
        <LinkText>쿠폰 확인하러가기</LinkText>
        <div style={{width: '12px'}}/>
        <PaperPlane />
      </LinkButton>
      <CloseDayButton onClick={onCloseDay}>
        ☒ 오늘 하루 보지 않기
      </CloseDayButton>
    </Wrapper>
  );
};

export default KakaoStyleCouponEventDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;
  background: url(/event/220427_kakao_style_coupon.png);
  background-size: cover;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 460px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 360px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const Title = styled.p`
  ${typo({
    weight: 900,
    size: "43px",
    height: "130%",
    color: "#6446FF",
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 900,
      size: "28px",
      height: "130%",
      color: "#6446FF",
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 900,
      size: "26px",
      height: "130%",
      color: "#6446FF",
    })}
  }
`;

const TitleFocus = styled.span`
  ${typo({
    weight: 800,
    size: "50px",
    height: "110%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "40px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "30px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const Description = styled.p`
  ${typo({
    weight: 700,
    size: "10px",
    height: "180%",
    color: colors.BLACK,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "10px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "10px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  background: ${colors.white};
  border-radius: 6px;

  padding: 17px 25px;

  @media (max-width: 767px) {
    padding: 14px 22px;
  }

  @media (max-width: 480px) {
    padding: 10px 18px;

    svg {
      width: 16px;
      height: 16px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width : 767px) {
    ${typo({
      weight: 600,
      size: "20px",
      height: "100%",
      color: colors.POINT_PURPLE,
    })}
  }

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.GRAY_90} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "26px",
    height: "180%",
    color: colors.BLACK,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "22px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "18px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
`;

const Period = styled.div`
  border-radius: 50px;
  margin-bottom: 36px;
  background: ${colors.white};
  text-align: center;
  width: 100%;
  ${typo({
    weight: 700,
    size: "17px",
    height: "180%",
    color: "#6446ff",
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "14px",
      height: "180%",
      color: "#6446ff",
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: "#6446ff",
    })}
  }
`;

const Chip = styled.div<{ bgColor: string }>`
  background: ${props => props.bgColor ?? 'black'};
  border-radius: 50px;
  padding: 13px 26px;

  ${typo({
    weight: 700,
    size: "20px",
    height: "100%",
    color: colors.white
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    padding: 9px 20px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "100%",
      color: colors.white
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    padding: 4px 16px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "100%",
      color: colors.white
    })}
  }
`;

const Price = styled.div`
  color: ${colors.BLACK};
  ${typo({
    weight: 700,
    size: "40px",
    height: "180%",
    color: colors.BLACK
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "26px",
      height: "180%",
      color: colors.BLACK
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "24px",
      height: "180%",
      color: colors.BLACK
    })}
  }
`;

const ComponentWrapper = styled(FlexWrapper)`
  background-color: white;
  width: 100%;

  border-radius: 5px;
  margin-bottom: 39px;
`;

const ComponentHeader = styled.div`
  background-color: #6446ff;
  width: 100%;
  padding: 7px 0px;
  border-radius: 5px 5px 0px 0px;
  text-align: center;
  ${typo({
    weight: 700,
    size: "11px",
    height: "120%",
    color: colors.white
  })}
`;

const InnerComponentWrapper = styled(FlexWrapper)`
  gap: 24px;
  justify-content: center;
  width: 100%;
  padding: 10px;
  @media(max-width: 767px) {
    gap: 10px;
  }
  @media(max-width: 480px) {
    gap: 10px;
  }
`;

const PurpleArrow = styled(ComponentPurpleArrow)`
  @media(max-width: 767px) {
    width: 35px;
  }
  @media(max-width: 480px) {
    width: 20px;
  }
`;

const PaperPlane = styled(IconPaperPlane)`
  path {
    fill: #6446ff
  }
`;