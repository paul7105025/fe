import React from "react";
import { Close } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";
import { useRouter } from "next/router";
import { chatToggleState, dialogDataType } from "@wrtn/core";
import { IconPaperPlane } from "@wrtn/ui/assets"
import { useSetRecoilState } from "recoil";

interface ImageChatEventDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
  dialog?: dialogDataType;
}

const ImageChatEventDialog = ({
  onClose,
  onCloseDay,
  dialog,
}: ImageChatEventDialogProps) => {
  const router = useRouter();
  const navigateToChat = () => { 
    router.push('/');
    onCloseDay();
  };

  return (
    <Wrapper>
      <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton>

      <SubTitle>이미지 생성 기능 출시</SubTitle>
      <Title>
        뤼튼과 함께
        <br/>
        상상을 현실로 그려보세요!
      </Title>
      <Description>'그려줘' 또는 'Draw'라고 입력해보세요.</Description>

      <div style={{ flex: 1 }} />
      <LinkButton onClick={navigateToChat}>
        <LinkText>지금 바로 그려보기</LinkText>
        <div style={{width: '12px'}}/>
        <PaperPlane color={colors.POINT_PURPLE} />
      </LinkButton>
      <CloseDayButton onClick={onCloseDay}>
      {(dialog?.closeDay === undefined || dialog?.closeDay === 1) ? "☒ 오늘 하루 보지 않기" : `☒ ${dialog?.closeDay}일 동안 보지 않기`}
      </CloseDayButton>
    </Wrapper>
  );
};

export default ImageChatEventDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;
  background: url("/event/220427_image_event.png");
  background-size: cover;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 400px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 267px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const Title = styled.p`
  margin-top: 10px;
  ${typo({
    weight: 700,
    size: "50px",
    height: "110%",
    color: colors.white,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 700,
      size: "34px",
      height: "130%",
      color: colors.white,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 700,
      size: "28px",
      height: "130%",
      color: colors.white,
    })}
  }
`;

const TitleFocus = styled.span`
  ${typo({
    weight: 800,
    size: "50px",
    height: "110%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "40px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "30px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const Description = styled.p`
  margin-top: 10px;
  ${typo({
    weight: 700,
    size: "20px",
    height: "180%",
    color: colors.white,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "180%",
      color: colors.white,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: colors.white,
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  background: ${colors.white};
  border-radius: 6px;

  padding: 20px 30px;

  @media (max-width: 767px) {
    padding: 18px 33px;
  }

  @media (max-width: 480px) {
    padding: 12px 22px;

    svg {
      width: 16px;
      height: 16px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width : 767px) {
    ${typo({
      weight: 600,
      size: "20px",
      height: "100%",
      color: colors.POINT_PURPLE,
    })}
  }

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.white} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "26px",
    height: "180%",
    color: "#BF98FF",
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "22px",
      height: "180%",
      color: "#BF98FF",
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "18px",
      height: "180%",
      color: "#BF98FF",
    })};
    svg {
      width: 12px;
      height: 12px;
    }
  }
`;

const PaperPlane = styled(IconPaperPlane)`
  path {
    fill: ${colors.POINT_PURPLE};
  }
`;