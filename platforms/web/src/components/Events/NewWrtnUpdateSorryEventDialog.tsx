import React from "react";
import { Close } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";

interface RefundEventDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
}

const NewWrtnUpdateSorryEventDialog = ({
  onClose,
  onCloseDay,
}: RefundEventDialogProps) => {
  const navigateToCommunity = () => {
    window.open(
      "https://wrtn.imweb.me/notice/?bmode=view&idx=14607347&back_url=&t=board&page=",
      "_blank"
    );
    onClose();
  };

  return (
    <Wrapper>
      <Image
        src="/event/version_2_bg.png"
        alt="version_2_bg"
        fill
        style={{
          position: "absolute",
          zIndex: -1,
          backgroundColor: colors.WHITE,
          borderRadius: 20,
        }}
      />
      <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton>

      <DayWrapper>
        <New>NEW</New>
        <Day>2023. 4. 6.</Day>
      </DayWrapper>

      {/* <SubTitle>무엇이든 물어보세요!</SubTitle> */}
      {/* <ImageWrapper>
        <img style={{width: '100%', height: '100%' }} src={DateImage} />
      </ImageWrapper> */}
      <Title>뤼튼 2.0</Title>
      <Description>놀라운 변화를 소개합니다!</Description>

      <div style={{ flex: 1 }} />
      <LinkButton onClick={navigateToCommunity}>
        <LinkText>커뮤니티 방문하기</LinkText>
      </LinkButton>
      <CloseDayButton onClick={onCloseDay}>
        ☒ 오늘 하루 보지 않기
      </CloseDayButton>
    </Wrapper>
  );
};

export default NewWrtnUpdateSorryEventDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 400px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 267px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const DayWrapper = styled(FlexWrapper)`
  gap: 14px;
  margin-bottom: 33px;

  @media (max-width: 767px) {
    margin-bottom: 20px;
  }
  @media (max-width: 480px) {
    margin-bottom: 10px;
  }
`;

const New = styled.div`
  border-radius: 60px;
  padding: 6px 24px 1px;
  background-color: ${colors.POINT_PURPLE};

  ${typo({
    weight: 900,
    size: "32px",
    height: "130%",
    color: colors.WHITE,
  })};

  @media (max-width: 767px) {
    padding: 6px 18px 1px;
    font-size: 24px;
  }
  @media (max-width: 480px) {
    padding: 6px 12px 1px;
    font-size: 16px;
  }
`;

const Day = styled.div`
  ${typo({
    weight: 700,
    size: "37px",
    height: "130%",
    color: "#5A2CDA",
  })};

  @media (max-width: 767px) {
    font-size: 28px;
  }
  @media (max-width: 480px) {
    font-size: 20px;
  }
`;

const Title = styled.p`
  ${typo({
    weight: 900,
    size: "90px",
    height: "110%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 900,
      size: "60px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 900,
      size: "45px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const Description = styled.p`
  margin-top: 15px;
  ${typo({
    weight: 700,
    size: "32px",
    height: "180%",
    color: colors.gray_90,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "24px",
      height: "180%",
      color: colors.gray_90,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "180%",
      color: colors.gray_90,
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  background-color: ${colors.POINT_PURPLE};
  border-radius: 6px;

  padding: 24px 45px;

  @media (max-width: 767px) {
    padding: 18px 33px;
  }

  @media (max-width: 480px) {
    padding: 12px 22px;

    svg {
      width: 100px;
      height: 27px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: colors.white,
  })}

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: colors.white,
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.GRAY_90} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;
