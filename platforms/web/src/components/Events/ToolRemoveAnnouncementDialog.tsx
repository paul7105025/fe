import React from "react";
import { Close } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";
import { useRouter } from "next/router";
import { chatToggleState, dialogDataType } from "@wrtn/core";
import { SolidCarousel } from "@wrtn/ui/assets"
import { useSetRecoilState } from "recoil";

interface ToolRemoveAnnouncementDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
  dialog?: dialogDataType;
}

const ToolRemoveAnnouncementDialog = ({
  onClose,
  onCloseDay,
  dialog
}: ToolRemoveAnnouncementDialogProps) => {
  const router = useRouter();
  const setChatToggle = useSetRecoilState(chatToggleState);
  const navigateToCommunity = () => { 
    window.open("https://wrtn.imweb.me/notice/?q=YToxOntzOjEyOiJrZXl3b3JkX3R5cGUiO3M6MzoiYWxsIjt9&bmode=view&idx=15064424&t=board");
    setChatToggle(1)
    onCloseDay();
  };

  return (
    <Wrapper>
      <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton>

      <SubTitle>채팅 좌측 '툴 목록' 삭제</SubTitle>
      <Title>
        채팅 화면 좌측
        <TitleFocus> 툴 목록</TitleFocus>
        <br/>
        삭제에 따른 대응 방법
      </Title>
      <Description>채팅에서 툴을 계속 사용하고 싶은 분들은 커뮤니티 안내를 참고해주세요!</Description>

      <div style={{ flex: 1 }} />
      <LinkButton onClick={navigateToCommunity}>
        <LinkText>자세히 알아보기</LinkText>
        <div style={{width: '12px'}}/>
        <IconCarousel />
      </LinkButton>
      <CloseDayButton onClick={onCloseDay}>
      {(dialog?.closeDay === undefined || dialog?.closeDay === 1) ? "☒ 오늘 하루 보지 않기" : `☒ ${dialog?.closeDay}일 동안 보지 않기`}
      </CloseDayButton>
    </Wrapper>
  );
};

export default ToolRemoveAnnouncementDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;
  background: url(/event/230503_remove_tool.png);
  background-size: cover;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 400px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 267px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const Title = styled.p`
  ${typo({
    weight: 800,
    size: "44px",
    height: "110%",
    color: colors.BLACK,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "32px",
      height: "130%",
      color: colors.BLACK,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "32px",
      height: "130%",
      color: colors.BLACK,
    })}
  }
`;

const TitleFocus = styled.span`
  ${typo({
    weight: 800,
    size: "44px",
    height: "110%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "32px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "32px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const Description = styled.p`
  ${typo({
    weight: 700,
    size: "16px",
    height: "180%",
    color: colors.gray_70,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: colors.gray_70,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "10px",
      height: "180%",
      color: colors.gray_70,
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  background: linear-gradient(90.34deg, #C57DFF -21.05%, #BC6BFF -21.03%, #669CFF 81.23%, #99E5F4 169.62%);
  border-radius: 6px;

  padding: 20px 30px;

  @media (max-width: 767px) {
    padding: 18px 33px;
  }

  @media (max-width: 480px) {
    padding: 12px 22px;

    svg {
      width: 16px;
      height: 16px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: colors.white,
  })}

  @media (max-width : 767px) {
    ${typo({
      weight: 600,
      size: "20px",
      height: "100%",
      color: colors.white,
    })}
  }

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: colors.white,
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.GRAY_90} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "20px",
    height: "180%",
    color: colors.BLACK,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
`;

const IconCarousel = styled(SolidCarousel)`
  path {
    fill: ${colors.white};
  }
`;