import React from "react";
import { Close } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";
import { useRouter } from "next/router";
import { chatToggleState } from "@wrtn/core";
import { IconPaperPlane } from "@wrtn/ui/assets"
import { useSetRecoilState } from "recoil";

interface KakaotalkGPT4UnlimitedEventDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
}

const KakaotalkGPT4UnlimitedEventDialog = ({
  onClose,
  onCloseDay,
}: KakaotalkGPT4UnlimitedEventDialogProps) => {
  const router = useRouter();
  const setChatToggle = useSetRecoilState(chatToggleState);
  const navigateToChat = () => { 
    router.push('/')
    setChatToggle(1)
    onCloseDay();
  };

  return (
    <Wrapper>
      {/* <Image
        src="/event/220425_kakaotalk_gpt4_unlimited_event.png"
        alt="version_2_bg"
        fill
        style={{
          position: "absolute",
          zIndex: -1,
          backgroundColor: colors.WHITE,
          borderRadius: 20,
        }}
      /> */}
      <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton>

      <SubTitle>무제한 무료 서비스 시작</SubTitle>
      <Title>
        <TitleFocus>뤼튼</TitleFocus>에서 GPT-4를
        <br/>
        <TitleFocus>무료로 사용</TitleFocus>하세요.
      </Title>
      <Description>채팅방 생성 개수 | GPT-4 생성 횟수 무제한</Description>

      <div style={{ flex: 1 }} />
      <LinkButton onClick={navigateToChat}>
        <LinkText>지금 바로 사용하기</LinkText>
        <div style={{width: '12px'}}/>
        <IconPaperPlane />
      </LinkButton>
      <CloseDayButton onClick={onCloseDay}>
        ☒ 오늘 하루 보지 않기
      </CloseDayButton>
    </Wrapper>
  );
};

export default KakaotalkGPT4UnlimitedEventDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;
  background: url(/event/220425_kakaotalk_gpt4_unlimited_event.png);
  background-size: cover;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 400px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 267px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const Title = styled.p`
  ${typo({
    weight: 800,
    size: "50px",
    height: "110%",
    color: colors.BLACK,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "34px",
      height: "130%",
      color: colors.BLACK,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "28px",
      height: "130%",
      color: colors.BLACK,
    })}
  }
`;

const TitleFocus = styled.span`
  ${typo({
    weight: 800,
    size: "50px",
    height: "110%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "40px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "30px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const Description = styled.p`
  ${typo({
    weight: 700,
    size: "20px",
    height: "180%",
    color: colors.gray_70,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "180%",
      color: colors.gray_70,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: colors.gray_70,
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  background: linear-gradient(90.34deg, #C57DFF -21.05%, #BC6BFF -21.03%, #669CFF 81.23%, #99E5F4 169.62%);
  border-radius: 6px;

  padding: 20px 30px;

  @media (max-width: 767px) {
    padding: 18px 33px;
  }

  @media (max-width: 480px) {
    padding: 12px 22px;

    svg {
      width: 16px;
      height: 16px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: colors.white,
  })}

  @media (max-width : 767px) {
    ${typo({
      weight: 600,
      size: "20px",
      height: "100%",
      color: colors.white,
    })}
  }

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: colors.white,
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.GRAY_90} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "26px",
    height: "180%",
    color: colors.BLACK,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "22px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "18px",
      height: "180%",
      color: colors.BLACK,
    })}
  }
`;