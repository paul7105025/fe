import React from "react";
import { Close } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";
import { useRouter } from "next/router";
import { chatToggleState, dialogDataType } from "@wrtn/core";
import { IconPaperPlane } from "@wrtn/ui/assets"
import { useSetRecoilState } from "recoil";

interface DesktopUpdateEventDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
  dialog?: dialogDataType;
}

const DesktopUpdateEventDialog = ({
  onClose,
  onCloseDay,
  dialog,
}: DesktopUpdateEventDialogProps) => {
  const router = useRouter();
  const navigateToDesktop = () => { 
    router.push('/desktop');
    onCloseDay();
  };

  return (
    <Wrapper>
      {/* <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton> */}

      <Title>
        데스크탑앱 재설치 안내
      </Title>
      <Description>
        데스크탑앱에서 발생중인 로그인, 생성 오류 등 치명적인 버그를<br/>
        개선한 새로운 버전이 공개 되었습니다.<br/>
        새로운 버전을 재설치 후, 사용하시길 권장합니다.<br/>
        <br/>
        이용에 불편함을 드려 죄송합니다.
      </Description>

      <div style={{ flex: 1 }} />
      <LinkButton onClick={navigateToDesktop}>
        <LinkText>새 버전 재설치하기</LinkText>
      </LinkButton>
    </Wrapper>
  );
};

export default DesktopUpdateEventDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;
  background: #0D0623;
  background-size: cover;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 400px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 267px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const Title = styled.p`
  margin-top: 10px;
  ${typo({
    weight: 700,
    size: "50px",
    height: "110%",
    color: colors.white,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 700,
      size: "34px",
      height: "130%",
      color: colors.white,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 700,
      size: "28px",
      height: "130%",
      color: colors.white,
    })}
  }
`;

const TitleFocus = styled.span`
  ${typo({
    weight: 800,
    size: "50px",
    height: "110%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "40px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "30px",
      height: "130%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const Description = styled.p`
  margin-top: 10px;
  ${typo({
    weight: 700,
    size: "20px",
    height: "180%",
    color: colors.white,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "180%",
      color: colors.white,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: colors.white,
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  background: ${colors.white};
  border-radius: 6px;

  padding: 20px 30px;

  @media (max-width: 767px) {
    padding: 18px 33px;
  }

  @media (max-width: 480px) {
    padding: 12px 22px;

    svg {
      width: 16px;
      height: 16px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: colors.POINT_PURPLE,
  })}

  @media (max-width : 767px) {
    ${typo({
      weight: 600,
      size: "20px",
      height: "100%",
      color: colors.POINT_PURPLE,
    })}
  }

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: colors.POINT_PURPLE,
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.white} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "26px",
    height: "180%",
    color: "#BF98FF",
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "22px",
      height: "180%",
      color: "#BF98FF",
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "18px",
      height: "180%",
      color: "#BF98FF",
    })};
    svg {
      width: 12px;
      height: 12px;
    }
  }
`;

const PaperPlane = styled(IconPaperPlane)`
  path {
    fill: ${colors.POINT_PURPLE};
  }
`;