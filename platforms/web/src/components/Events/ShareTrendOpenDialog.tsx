import React from "react";
import { Close, IconStore } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo, newColors } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";
import { useRouter } from "next/router";
import { dialogDataType, useEvent } from "@wrtn/core";

interface ShareTrendOpenEventDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
  dialog?: dialogDataType
}

const ShareTrendOpenEventDialog = ({
  onClose,
  onCloseDay,
  dialog
}: ShareTrendOpenEventDialogProps) => {
  const router = useRouter();
  const { collectEvent } = useEvent();
  const navigateToStore = () => {
    router.push("/share");
    collectEvent('click_event_modal_button', {
      modal_name: '공유 트렌드 이벤트'
    })
    onCloseDay();
  };

  return (
    <Wrapper>
      <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton>

      <SubTitle>공유 트렌드 기능 출시</SubTitle>
      <Title>
        채팅을 친구에게 공유해보세요!
      </Title>
      <Description>채팅방의 '공유' 버튼을 누르면 바로 이용할 수 있어요.</Description>

      <div style={{ flex: 1 }} />
      <LinkButton onClick={navigateToStore}>
        <LinkText>#실시간 인기 둘러보기</LinkText>
      </LinkButton>
      <CloseDayButton onClick={onCloseDay}>
      {(dialog?.closeDay === undefined || dialog?.closeDay === 1) ? "☒ 오늘 하루 보지 않기" : `☒ ${dialog?.closeDay}일 동안 보지 않기`}
      </CloseDayButton>
    </Wrapper>
  );
};

export default ShareTrendOpenEventDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;
  background: url("/event/220425_share_trend.png");
  background-size: cover;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 400px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 267px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const DayWrapper = styled(FlexWrapper)`
  gap: 14px;
  margin-bottom: 33px;

  @media (max-width: 767px) {
    margin-bottom: 20px;
  }
  @media (max-width: 480px) {
    margin-bottom: 10px;
  }
`;

const New = styled.div`
  border-radius: 60px;
  padding: 6px 24px 1px;
  background-color: ${colors.POINT_PURPLE};

  ${typo({
    weight: 900,
    size: "32px",
    height: "130%",
    color: colors.WHITE,
  })};

  @media (max-width: 767px) {
    padding: 6px 18px 1px;
    font-size: 24px;
  }
  @media (max-width: 480px) {
    padding: 6px 12px 1px;
    font-size: 16px;
  }
`;

const Day = styled.div`
  ${typo({
    weight: 700,
    size: "37px",
    height: "130%",
    color: "#5A2CDA",
  })};

  @media (max-width: 767px) {
    font-size: 28px;
  }
  @media (max-width: 480px) {
    font-size: 20px;
  }
`;

const Title = styled.p`
  ${typo({
    weight: 800,
    size: "50px",
    height: "120%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "40px",
      height: "120%",
      color: colors.WHITE,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "30px",
      height: "120%",
      color: "#ffcf55",
    })}
  }
`;

const Description = styled.p`
  margin-top: 15px;
  ${typo({
    weight: 700,
    size: "20px",
    height: "180%",
    color: newColors.WHITE,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "180%",
      color: newColors.WHITE,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: newColors.WHITE,
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  gap: 8px;
  background-color: #6446FF;
  border-radius: 6px;

  padding: 24px 45px;

  @media (max-width: 767px) {
    padding: 18px 33px;
  }

  @media (max-width: 480px) {
    padding: 12px 22px;

    svg {
      width: 100px;
      height: 27px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: newColors.WHITE,
  })}

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: newColors.WHITE,
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.WHITE} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "26px",
    height: "180%",
    color: colors.white,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "22px",
      height: "180%",
      color: colors.white,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "18px",
      height: "180%",
      color: colors.white,
    })}
  }
`;