import React from "react";
import { Close } from "@wrtn/ui/assets";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

import Image from "next/image";
import { useRouter } from "next/router";

interface KakaotalkPluginAnnouncementEventDialogProps {
  onClose: () => void;
  onCloseDay: () => void;
}

const KakaotalkPluginAnnouncementEventDialog = ({
  onClose,
  onCloseDay,
}: KakaotalkPluginAnnouncementEventDialogProps) => {
  const router = useRouter();
  const navigateToChat = () => {
    router.push("/");
    onClose();
  };

  return (
    <Wrapper>
      <CloseButton onClick={onClose}>
        <CloseIcon />
      </CloseButton>

      <SubTitle>실시간 정보반영 AI 뤼튼</SubTitle>
      <Title>
        ?뤼튼에게
        <br/>
        실시간으로 질문하세요!
      </Title>
      <Description>‘?’을 달고 질문시 실시간 검색정보를 보여드려요.</Description>

      <div style={{ flex: 1 }} />
      <LinkButton onClick={navigateToChat}>
        <LinkText>? 지금 바로 질문하기</LinkText>
      </LinkButton>
      <CloseDayButton onClick={onCloseDay}>
        ☒ 오늘 하루 보지 않기
      </CloseDayButton>
    </Wrapper>
  );
};

export default KakaotalkPluginAnnouncementEventDialog;

const Wrapper = styled(FlexWrapper)`
  position: relative;
  background: url("/event/220425_plugin_event.png");
  background-size: cover;

  width: 720px;
  height: 533px;

  max-width: 90vw;
  max-height: 90vh;

  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 43px;
  border-radius: 20px;

  @media (max-width: 767px) {
    width: 540px;
    height: 400px;
  }

  @media (max-width: 480px) {
    width: 360px;
    height: 267px;
    padding: 20px;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  top: 16px;
  right: 16px;

  width: 40px;
  height: 40px;

  @media (max-width: 767px) {
  }
`;

const DayWrapper = styled(FlexWrapper)`
  gap: 14px;
  margin-bottom: 33px;

  @media (max-width: 767px) {
    margin-bottom: 20px;
  }
  @media (max-width: 480px) {
    margin-bottom: 10px;
  }
`;

const New = styled.div`
  border-radius: 60px;
  padding: 6px 24px 1px;
  background-color: ${colors.POINT_PURPLE};

  ${typo({
    weight: 900,
    size: "32px",
    height: "130%",
    color: colors.WHITE,
  })};

  @media (max-width: 767px) {
    padding: 6px 18px 1px;
    font-size: 24px;
  }
  @media (max-width: 480px) {
    padding: 6px 12px 1px;
    font-size: 16px;
  }
`;

const Day = styled.div`
  ${typo({
    weight: 700,
    size: "37px",
    height: "130%",
    color: "#5A2CDA",
  })};

  @media (max-width: 767px) {
    font-size: 28px;
  }
  @media (max-width: 480px) {
    font-size: 20px;
  }
`;

const Title = styled.p`
  ${typo({
    weight: 800,
    size: "50px",
    height: "120%",
    color: colors.white,
  })}

  @media (max-width: 767px) {
    ${typo({
      weight: 800,
      size: "40px",
      height: "120%",
      color: colors.white,
    })}
  }
  @media (max-width: 480px) {
    ${typo({
      weight: 800,
      size: "30px",
      height: "120%",
      color: colors.white,
    })}
  }
`;

const Description = styled.p`
  margin-top: 15px;
  ${typo({
    weight: 700,
    size: "20px",
    height: "180%",
    color: "#ffcf55",
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "16px",
      height: "180%",
      color: "#ffcf55",
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "12px",
      height: "180%",
      color: "#ffcf55",
    })}
  }
`;

const LinkButton = styled(FlexButton)`
  cursor: pointer;

  background-color: ${colors.white};
  border-radius: 6px;

  padding: 24px 45px;

  @media (max-width: 767px) {
    padding: 18px 33px;
  }

  @media (max-width: 480px) {
    padding: 12px 22px;

    svg {
      width: 100px;
      height: 27px;
    }
  }
`;

const LinkText = styled.p`
  ${typo({
    weight: 700,
    size: "23px",
    height: "100%",
    color: "#5a2cda",
  })}

  @media (max-width : 480px) {
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: "#5a2cda",
    })}
  }
`;

const CloseIcon = styled(Close)`
  > path {
    fill: ${colors.GRAY_90} !important;
  }
`;

const CloseDayButton = styled(FlexButton)`
  position: absolute;
  bottom: -42px;
  right: 0px;
  cursor: pointer;
  ${typo({
    weight: 600,
    size: "20px",
    height: "140%",
    color: colors.WHITE,
  })}

  @media (max-width: 767px) {
    bottom: -32px;
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "26px",
    height: "180%",
    color: colors.white,
  })}

  @media (max-width: 767px) {
    margin-top: 10px;
    ${typo({
      weight: 700,
      size: "22px",
      height: "180%",
      color: colors.white,
    })}
  }
  @media (max-width: 480px) {
    margin-top: 5px;
    ${typo({
      weight: 700,
      size: "18px",
      height: "180%",
      color: colors.white,
    })}
  }
`;