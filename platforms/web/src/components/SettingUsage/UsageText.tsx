import React from "react";
import { UsageTextWrapper, UsageTextNumber } from "./styles";

export const UsageText = ({ usage }: { usage: number }) => {
  return (
    <UsageTextWrapper>
      <UsageTextNumber>{usage.toLocaleString()}</UsageTextNumber> 자
    </UsageTextWrapper>
  );
};
