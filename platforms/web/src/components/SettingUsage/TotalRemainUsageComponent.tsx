import React from "react";
import dayjs from "dayjs";
import { useAlert } from "react-alert";

import { useRecoilValue } from "recoil";
import { CopyToClipboard } from "react-copy-to-clipboard";

// @ts-ignore
import { userState } from "@wrtn/core/stores/login";

import { PaymentHistory } from "./PaymentHistory";
import { UsageText } from "./UsageText";

import {
  UsageWrapper,
  UsageDivideSection,
  UsageSection,
  UsageVerticalDivideSection,
  CreditLongInnerContainer,
  RecommendText,
  RecommendCode,
  CopyButton,
  DividerDash,
  GuideButtonText,
  MainText,
  UsageSectionTitle,
  SectionHead,
} from "./styles";
import { SimpleGuideButton } from "@wrtn/ui/components/GuideButton/GuideButton";

import { colors } from "@wrtn/ui";
import { useEvent, useTagManager } from "@wrtn/core";

export const TotalRemainUsageComponent = ({
  wordCount,
  currentPaymentHistory,
  isMobile,
}) => {
  const user = useRecoilValue<any>(userState);

  const { collectEvent } = useEvent();
  const tagManager = useTagManager();
  const alert = useAlert();

  const copyToClipboard = () => {
    tagManager({
      event: "click_recommend_btn",
      data: null,
    });
    alert.removeAll();
    alert.show("추천 링크를 복사했어요.");
  };

  return (
    <UsageWrapper style={{ flex: "1.9" }}>
      <UsageDivideSection>
        <UsageSection>
          <UsageSectionTitle>
            <MainText>남은 총 글자 수</MainText>
            <SimpleGuideButton
              iconColor={colors.GRAY_58}
              guide={{
                isVisible: true,
              }}
            >
              <GuideButtonText>
                <b>유료 글자 수</b>와 <b>보너스 글자 수</b>를 합친{" "}
                <b>총 잔여 글자 수</b> 입니다.
              </GuideButtonText>
            </SimpleGuideButton>
          </UsageSectionTitle>
          <UsageText
            usage={
              (wordCount?.leftFreeWordCount || 0) +
              (wordCount?.leftPaidWordCount || 0)
            }
          />
        </UsageSection>
      </UsageDivideSection>
      {!isMobile ? (
        <UsageDivideSection last={true}>
          <UsageVerticalDivideSection>
            <UsageSection>
              <SectionHead>
                <UsageSectionTitle>
                  <MainText>남은 유료 글자 수</MainText>
                  <SimpleGuideButton
                    iconColor={colors.GRAY_58}
                    guide={{
                      isVisible: true,
                    }}
                  >
                    <GuideButtonText>
                      월 구독 기간에 부여된 유료 글자의 잔여량입니다. 매월 결제
                      시점에 초기화 됩니다. 단, 구독 기간 중에 요금제를 업데이트
                      해 결제일이 변경된 경우 업데이트 전과 후의 이용 기간이
                      겹치는 동안은 합산되어 안내됩니다.
                    </GuideButtonText>
                  </SimpleGuideButton>
                </UsageSectionTitle>
                <UsageText usage={wordCount?.leftPaidWordCount || 0} />
              </SectionHead>
              <DividerDash />
              {Array.isArray(currentPaymentHistory) &&
                currentPaymentHistory
                  ?.filter((e) => e.isPaid)
                  .map((v) => (
                    <PaymentHistory
                      key={v._id}
                      type={v.description}
                      date={`${dayjs(v.paymentDate).format("YYYY.MM.DD")} ~ ${
                        v.dueDate
                          ? dayjs(v.dueDate).format("YYYY.MM.DD") + " PM 7:00"
                          : "-"
                      }`}
                      usage={v.totalWordCount - v.usedWordCount}
                    />
                  ))}
            </UsageSection>
          </UsageVerticalDivideSection>
          <UsageVerticalDivideSection last={true}>
            <UsageSection>
              <SectionHead>
                <UsageSectionTitle>
                  <MainText>남은 보너스 글자 수</MainText>
                  <SimpleGuideButton
                    iconColor={colors.GRAY_58}
                    guide={{
                      isVisible: true,
                    }}
                  >
                    <GuideButtonText>
                      <b>기본 제공 및 친구 추천</b>으로 얻은 보너스 글자의
                      잔여량입니다. <b>추천 링크를 통해 가입</b>하거나, 가입 시{" "}
                      <b>추천인 코드를 입력하면</b> 초대한 사람과 초대받은
                      사람에게 각각 보너스 글자 <b>1000자</b>가 부여됩니다.
                      보너스 글자 수는 유효기간이 없이 사용 가능합니다.
                    </GuideButtonText>
                  </SimpleGuideButton>
                </UsageSectionTitle>
                <UsageText usage={wordCount?.leftFreeWordCount || 0} />
              </SectionHead>
              <DividerDash />
              <CreditLongInnerContainer style={{ gap: 0 }}>
                <UsageSectionTitle style={{ marginBottom: 13 }}>
                  <RecommendText>추천인 코드</RecommendText>
                  <RecommendCode>{user.recommendCode}</RecommendCode>
                </UsageSectionTitle>
                <CopyToClipboard
                  text={`${window.location.origin}/app/login?recommend=${user.recommendCode}`}
                >
                  <CopyButton onClick={copyToClipboard}>
                    추천 링크 복사
                  </CopyButton>
                </CopyToClipboard>
              </CreditLongInnerContainer>
            </UsageSection>
          </UsageVerticalDivideSection>
        </UsageDivideSection>
      ) : (
        <>
          <UsageDivideSection>
            <UsageSection>
              <UsageSectionTitle>
                <MainText>남은 유료 글자 수</MainText>
                <SimpleGuideButton
                  iconColor={colors.GRAY_58}
                  guide={{
                    isVisible: true,
                  }}
                >
                  <GuideButtonText>
                    월 구독 기간에 부여된 유료 글자의 잔여량입니다. 매월 결제
                    시점에 초기화 됩니다. 단, 구독 기간 중에 요금제를 업데이트
                    해 결제일이 변경된 경우 업데이트 전과 후의 이용 기간이
                    겹치는 동안은 합산되어 안내됩니다.
                  </GuideButtonText>
                </SimpleGuideButton>
              </UsageSectionTitle>
              <UsageText usage={wordCount?.leftPaidWordCount || 0} />
              <DividerDash />
              {Array.isArray(currentPaymentHistory) &&
                currentPaymentHistory
                  ?.filter((e) => e.isPaid)
                  .map((v) => (
                    <PaymentHistory
                      key={v._id}
                      type={v.description}
                      date={`${dayjs(v.paymentDate).format("YYYY.MM.DD")} ~ ${
                        v.dueDate
                          ? dayjs(v.dueDate).format("YYYY.MM.DD") + " PM 7:00"
                          : "-"
                      }`}
                      usage={v.totalWordCount - v.usedWordCount}
                    />
                  ))}
            </UsageSection>
          </UsageDivideSection>
          <UsageDivideSection>
            <UsageSection>
              <UsageSectionTitle>
                <MainText>남은 보너스 글자 수</MainText>
                <SimpleGuideButton
                  guide={{
                    isVisible: true,
                  }}
                >
                  <GuideButtonText>
                    <b>기본 제공 및 친구 추천</b>으로 얻은 보너스 글자의
                    잔여량입니다. <b>추천 링크를 통해 가입</b>하거나, 가입 시{" "}
                    <b>추천인 코드를 입력하면</b> 초대한 사람과 초대받은
                    사람에게 각각 보너스 글자 <b>1000자</b>가 부여됩니다. 보너스
                    글자 수는 유효기간이 없이 사용 가능합니다.
                    <ul>
                      <li>
                        추천인 보상은 계정 당 최대 10,000자(10회)까지 획득 할 수
                        있습니다.
                      </li>
                      <li>
                        추천인 입력 보상은 계정 당 최대 1,000자(1회)를 획득 할
                        수 있습니다.{" "}
                      </li>
                    </ul>
                  </GuideButtonText>
                </SimpleGuideButton>
              </UsageSectionTitle>
              <UsageText usage={wordCount?.leftFreeWordCount || 0} />
              <DividerDash />
              <CreditLongInnerContainer style={{ gap: 0 }}>
                <UsageSectionTitle style={{ marginBottom: 13 }}>
                  <RecommendText>추천인 코드</RecommendText>
                  <RecommendCode>{user.recommendCode}</RecommendCode>
                </UsageSectionTitle>
                <CopyToClipboard
                  text={`${window.location.origin}/app/login?recommend=${user.recommendCode}`}
                >
                  <CopyButton
                    onClick={() => {
                      collectEvent("copy_recommend_link", {
                        position: "amount",
                      });
                    }}
                  >
                    추천 링크 복사
                  </CopyButton>
                </CopyToClipboard>
              </CreditLongInnerContainer>
            </UsageSection>
          </UsageDivideSection>
        </>
      )}
    </UsageWrapper>
  );
};
