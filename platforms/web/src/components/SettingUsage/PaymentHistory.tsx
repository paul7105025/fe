import styled from "styled-components";

import { FlexWrapper, typo, colors } from "@wrtn/ui";

interface PaymentHistoryProps {
  type: string;
  date: string;
  usage: number;
}

export const PaymentHistory = ({ type, date, usage }: PaymentHistoryProps) => {
  return (
    <PaymentHistoryWrapper>
      <InnerWrapper style={{ justifyContent: "space-between", width: "100%" }}>
        <HistoryType>{type}</HistoryType>
        <HistoryType>{usage.toLocaleString()} 자</HistoryType>
      </InnerWrapper>
      <HistoryDate>{date}</HistoryDate>
    </PaymentHistoryWrapper>
  );
};

const PaymentHistoryWrapper = styled(FlexWrapper)`
  flex-direction: column;
  gap: 8px;
  width: 100%;
  max-width: 200px;
  padding-bottom: 14px;
  align-items: flex-start;
`;

const InnerWrapper = styled(FlexWrapper)`
  justify-content: space-between;
  width: 100%;
`;

const HistoryType = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.gray_80,
  })};
`;

const HistoryDate = styled.div`
  ${typo({
    weight: 500,
    size: "12px",
    height: "100%",
    color: colors.gray_60,
  })};
`;
