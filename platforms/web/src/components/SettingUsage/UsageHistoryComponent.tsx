import React from "react";
import dayjs from "dayjs";

import { PLAN_NAME } from "@wrtn/core";

import {
  CreditHistoryWrapper,
  CreditHistoryHeader,
  CreditHistoryHeaderItem,
  CreditHistoryBody,
  CreditHistoryBox,
  CreditHistoryBodyItem,
  PaginationWrapper,
} from "./styles";

import { Pagination } from "@wrtn/ui/components/Pagination";

const sampleData = [
  {
    _id: 1,
    charge_credit: 10,
    type: "친구 추천 크레딧",
    charge_date: "2022-11-17",
    valid_date: "2022-12-17",
  },
  {
    _id: 2,
    charge_credit: 20,
    type: "프로 플랜 크레딧",
    charge_date: "2022-11-01",
    valid_date: "2022-11-31",
  },
  {
    _id: 3,
    charge_credit: 10,
    type: "친구 추천 크레딧",
    charge_date: "2022-10-17",
    valid_date: "2022-11-17",
  },
  {
    _id: 4,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
  {
    _id: 5,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
  {
    _id: 6,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
  {
    _id: 7,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
  {
    _id: 8,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
  {
    _id: 9,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
  {
    _id: 10,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
  {
    _id: 11,
    charge_credit: 50,
    type: "웰컴 크레딧",
    charge_date: "2022-10-01",
    valid_date: "2022-10-31",
  },
];

export const UsageHistoryComponent = ({ isMobile, paymentHistory }) => {
  const [pagination, setPagination] = React.useState(0);

  const maxPage = React.useMemo(() => {
    return Math.floor((paymentHistory?.length || 0) / 10);
  }, [paymentHistory]);

  return (
    <CreditHistoryWrapper>
      <CreditHistoryHeader>
        <CreditHistoryHeaderItem>충전 글자 수</CreditHistoryHeaderItem>
        <CreditHistoryHeaderItem>유형</CreditHistoryHeaderItem>
        {!isMobile && <CreditHistoryHeaderItem>지급일</CreditHistoryHeaderItem>}
        <CreditHistoryHeaderItem>유효기간</CreditHistoryHeaderItem>
      </CreditHistoryHeader>
      <CreditHistoryBody>
        {Array.isArray(paymentHistory) &&
          paymentHistory.map((data) => {
            return (
              <CreditHistoryBox key={data._id}>
                <CreditHistoryBodyItem>
                  + {Number(data?.totalWordCount || 0).toLocaleString()}자
                </CreditHistoryBodyItem>
                <CreditHistoryBodyItem>
                  {PLAN_NAME[data?.plan]}
                </CreditHistoryBodyItem>
                {!isMobile && (
                  <CreditHistoryBodyItem>
                    {dayjs(data?.paymentDate).format("YYYY-MM-DD")}
                  </CreditHistoryBodyItem>
                )}
                <CreditHistoryBodyItem>
                  {data?.plan === "FREE"
                    ? "-"
                    : dayjs(data?.dueDate).format("YYYY-MM-DD")}
                </CreditHistoryBodyItem>
              </CreditHistoryBox>
            );
          })}
      </CreditHistoryBody>
      {sampleData.length > 0 && (
        <PaginationWrapper>
          <Pagination
            maxPage={maxPage}
            currentPage={pagination}
            setPage={setPagination}
            // onHandleDecrease={() => {
            //   if (pagination > 0) setPagination(pagination - 1);
            // }}
            // onHandleIncrease={() => {
            //   if (pagination < maxPage - 1) setPagination(pagination + 1);
            // }}
            // setPage={setPagination}
          />
        </PaginationWrapper>
      )}
    </CreditHistoryWrapper>
  );
};
