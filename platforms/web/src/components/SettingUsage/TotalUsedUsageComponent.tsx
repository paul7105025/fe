import React from "react";
import dayjs from "dayjs";

import { PaymentHistory } from "./PaymentHistory";

import {
  UsageWrapper,
  UsageMoreText,
  UsageDivideSection,
  UsageSection,
  DividerDash,
  GuideButtonText,
  MainText,
  UsageSectionTitle,
  UsageVerticalDivideSection,
  SectionHead,
} from "./styles";

import { colors } from "@wrtn/ui";

import { SimpleGuideButton } from "@wrtn/ui/components/GuideButton/GuideButton";
import { UsageText } from "./UsageText";

export const TotalUsedUsageComponent = ({
  wordCount,
  currentPaymentHistory,
}) => {
  return (
    <UsageWrapper style={{ flex: "1" }}>
      <UsageDivideSection>
        <UsageSection>
          <UsageSectionTitle>
            <MainText>생성한 총 글자 수</MainText>
            <SimpleGuideButton
              iconColor={colors.GRAY_58}
              guide={{
                isVisible: true,
              }}
            >
              <GuideButtonText>
                <b>월 구독 기간 중 생성한 글자 수의 총합</b>입니다. 매월 결제
                시점에 초기화 됩니다. 단, 구독 기간 중에 요금제를 업데이트 해
                결제일이 변경된 경우 업데이트 전과 후의 이용 기간이 겹치는
                동안은 합산되어 안내됩니다.
              </GuideButtonText>
            </SimpleGuideButton>
          </UsageSectionTitle>
          <UsageText
            usage={
              wordCount?.usedFreeWordCount + wordCount?.usedPaidWordCount || 0
            }
          />
        </UsageSection>
      </UsageDivideSection>
      <UsageDivideSection last={true} style={{ height: "100%" }}>
        <UsageVerticalDivideSection last={true}>
          <UsageSection>
            <SectionHead>
              <UsageMoreText>
                더 많은 글자가 필요하다면
                <br />
                친구에게 뤼튼을 추천해보세요! 🙂
              </UsageMoreText>
            </SectionHead>
            <DividerDash />
            {Array.isArray(currentPaymentHistory) &&
              currentPaymentHistory &&
              currentPaymentHistory
                .filter((h) => h.isPaid)
                .map((v) => (
                  <PaymentHistory
                    key={v._id}
                    type={v.description}
                    date={`${dayjs(v.paymentDate).format("YYYY.MM.DD")} - ${
                      v.dueDate
                        ? dayjs(v.dueDate).format("YYYY.MM.DD") + " PM 7:00"
                        : ""
                    }`}
                    usage={v.usedWordCount}
                  />
                ))}
          </UsageSection>
        </UsageVerticalDivideSection>
      </UsageDivideSection>
    </UsageWrapper>
  );
};
