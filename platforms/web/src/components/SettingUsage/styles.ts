import styled from "styled-components";

import { colors, typo, FlexWrapper, FlexButton } from "@wrtn/ui";

export const UsageSectionTitle = styled(FlexWrapper)`
  justify-content: flex-start;
  margin-bottom: 17px;
  gap: 6px;
`;

export const UsageMoreText = styled.p`
  ${typo({
    weight: 700,
    size: "18px",
    height: "150%",
    color: colors.gray_80,
  })};

  text-align: center;
`;

export const UsageWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  flex: 1;

  flex-direction: column;
  justify-content: flex-start;

  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 15px;
`;

export const paymentHistories = [
  {
    type: "베이직 요금제",
    used: 28801,
    left: 11199,
    date: "2022.01.18 ~ 2022.02.18 PM 19:00",
  },
  {
    type: "프로 요금제",
    used: 0,
    left: 50000,
    date: "2022.01.22 ~ 2022.02.22 PM 19:00",
  },
];

export const UsageTextWrapper = styled.p`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.gray_80,
  })};
`;

export const UsageTextNumber = styled.span`
  ${typo({
    weight: 600,
    size: "26px",
    height: "100%",
    color: "#F4900C",
  })};
`;

export const UsageDivideSection = styled(FlexWrapper)<{ last?: boolean }>`
  width: 100%;
  border-bottom: ${(props) =>
    props.last ? `0px` : `1px solid ${colors.BLUE_GRAY_LINE};`};
`;

export const UsageSection = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  height: 100%;
  padding: 30px 30px 22px;
`;

export const UsageVerticalDivideSection = styled(FlexWrapper)<{ last?: boolean }>`
  height: 100%;
  width: 100%;
  border-right: ${(props) =>
    props.last ? `0px` : `1px solid ${colors.BLUE_GRAY_LINE};`};
  align-items: stretch;
`;

export const CreditLongCard = styled(FlexWrapper)`
  min-width: 397px;
  min-height: 120px;

  flex: 1.58;
  padding: 13px 42px;

  justify-content: space-between;

  border: 1px solid ${colors.GRAY_30};
  border-radius: 15px;

  gap: 30px;
`;

export const CreditLongInnerContainer = styled(FlexWrapper)`
  flex-direction: column;

  gap: 5px;
`;

export const Divider = styled.div`
  min-width: 1px;
  height: 100%;
  min-height: 94px;
  background-color: ${colors.GRAY_30};
`;

export const RecommendText = styled.span`
  ${typo({
    size: "14px",
    weight: "600",
    color: colors.GRAY_70,
    height: "170%",
  })}

  white-space: nowrap;
  margin-right: 11px;
`;

export const RecommendCode = styled.span`
  ${typo({
    size: "18px",
    weight: "700",
    color: colors.GRAY_80,
    height: "170%",
  })}
`;

export const CopyButton = styled(FlexButton)`
  padding: 6px 29px;

  background-color: ${colors.POINT_PURPLE};
  border-radius: 5px;

  ${typo({
    size: "16px",
    weight: "600",
    color: colors.WHITE,
    height: "170%",
  })}

  white-space: nowrap;
  cursor: pointer;

  &:hover {
    opacity: 0.7;
  }
`;

export const GuideButtonText = styled.p`
  ${typo({
    size: "16px",
    weight: "600",
    color: colors.GRAY_80,
    height: "175%",
  })}
  word-break: keep-all;
  white-space: pre-wrap;
  font-size: 1rem;
`;

export const DividerDash = styled.div`
  width: 100%;
  border: 1px dashed ${colors.BLUE_GRAY_LINE};
  margin: 20px 0px;
`;

export const MainText = styled.span`
  ${typo({
    size: "16px",
    weight: "600",
    color: colors.GRAY_80,
  })}
  white-space: nowrap;
`;

export const CreditHistoryWrapper = styled(FlexWrapper)`
  flex-direction: column;

  border: 1px solid ${colors.GRAY_30};
  border-radius: 12px;

  width: 100%;
  margin-bottom: 80px;
`;

export const CreditHistoryHeader = styled(FlexWrapper)`
  width: 100%;
  padding: 35px 10% 25px;
  border-bottom: 1px solid ${colors.GRAY_30};

  justify-content: space-between;
`;

export const CreditHistoryHeaderItem = styled(FlexWrapper)`
  flex: 1;
  white-space: nowrap;

  ${typo({
    size: "16px",
    weight: "600",
    color: colors.GRAY_80,
  })}
`;

export const CreditHistoryBody = styled(FlexWrapper)`
  width: 100%;
  padding: 35px 10% 25px;
  flex-direction: column;
  gap: 27px;
`;

export const CreditHistoryBox = styled(FlexWrapper)`
  width: 100%;
`;

export const CreditHistoryBodyItem = styled(FlexWrapper)`
  flex: 1;
  justify-content: center;
  white-space: nowrap;

  ${typo({
    size: "14px",
    weight: "600",
    color: colors.GRAY_80,
  })};
`;

export const PaginationWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 20px;
`;

export const SectionHead = styled(FlexWrapper)`
  flex-direction: column;
  height: 64px;
`;
