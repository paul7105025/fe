import { dateCalculator, favoriteStoreToolListState } from '@wrtn/core';
import { boxShadow, colors, FilledStar, FlexWrapper, Icon, newColors, newTypo, Star, STORE_ICON_SELECTOR } from '@wrtn/ui';
import styled from 'styled-components'
import { StoreCardTypeLabel } from './StoreCardTypeLabel';
import React from 'react';
import { useUser } from 'src/hooks/useUser';
import { formatNumber } from 'src/utils';

export const StoreCard = ({ 
  icon,
  _id,
  type,
  title,
  description,
  author,
  isFavorite,
  createdAt,
  viewCount,
  genCount,
  exportCount,
  favoriteCount,
  onClick,
  onClickExport,
  onClickFavorite
}) => {
  // favorite local temp state
  const [fav, setFav] = React.useState(isFavorite);
  const [initialFav, setInitialFav] = React.useState(isFavorite);

  const tempAddFav = initialFav && fav ? 0
    : initialFav && !fav ? -1
    : fav ? 1 : 0
  const { user } = useUser();
  React.useEffect(() => {
    setFav(isFavorite);
  }, [isFavorite])

  const handleClickFavorite = (e) => {
    e.stopPropagation();
    onClickFavorite();
    if(user){
      setFav(c => !c)
    
    }
  }

  return (
    <Wrapper onClick={onClick}>
      {/* header */}
      <Content>
      <FlexWrapper justify={"space-between"} align="center" style={{paddingBottom: '17px'}}>
        <div>
        {STORE_ICON_SELECTOR[icon]}
        </div>
        <FlexWrapper>
          {/* <ExportIcon onClick={onClickExport}>
            <Icon icon="external-link-alt" size={20} color={newColors.GRAY_400}/>
          </ExportIcon> */}
          <FavoriteIcon onClick={handleClickFavorite}>
            { fav
              ? <FilledStar />
              : <Star />
            }
          </FavoriteIcon>
        </FlexWrapper>

      </FlexWrapper>
      {/* title */}
      <FlexWrapper style={{gap: '9px', paddingBottom: '12px'}}>
        <StoreCardTypeLabel type={type}/>
        <Title>{title}</Title>
      </FlexWrapper>
      {/* description */}
      <Desc>
        { description }
      </Desc>
      {/* author */}
      <Author>
        { author }
      </Author>
      </Content>
      {/* divider */}
      <Divider />
      {/* footer */}
      <Footer justify="space-between">
        <FlexWrapper style={{gap: '12px'}}>
          <Count><Icon icon="eye" size={13}/> {formatNumber(viewCount)}</Count>
          <Count><Icon icon="star" size={13}/> {formatNumber(favoriteCount + tempAddFav)}</Count>
          <Count><Icon icon="play" size={13}/> {formatNumber(genCount)}</Count>
          <Count><Icon icon="external-link-alt" size={13}/> {formatNumber(exportCount)}</Count>
        </FlexWrapper>
        <CreateAt>
          { dateCalculator(createdAt.toString()) }
        </CreateAt>
      </Footer>
    </Wrapper>
  )
}

export default StoreCard;

const Wrapper = styled.div`
  max-height: 243px;
  min-width: 342px;
  border-radius: 8px;
  border: 1px solid ${colors.WHITE_BOLDER_LINE};

  cursor: pointer;
  :hover {
    ${boxShadow.basic_card_shadow};
  }
`;

const FavoriteIcon = styled.div`
  max-width: 20px;
  height: 20px;
  svg {
    height: 20px;
    display: block;
    margin: 0 auto;
    text-align: center;
    g {
      transform-origin: center center;
    }
  }
  cursor: pointer;
`;

const ExportIcon = styled.div`
  cursor: pointer;
`;

const Title = styled.div`
  ${newTypo("heading-18-semi")};
  color: ${newColors.GRAY_800};
`;

const Desc = styled.div`
  min-height: 52px;
  ${newTypo("body-16-med")};
  color: ${newColors.GRAY_600};
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

const Author = styled.div`
  min-height: 22px;
  ${newTypo("body-14-med")};
  color: ${newColors.GRAY_500};
`;

const Divider = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${colors.WHITE_BOLDER_LINE};
`;


const Content = styled.div`
  padding: 18px 24px;
`;

const Footer = styled(FlexWrapper)`
  padding: 12px 24px;
`;

const CreateAt = styled.div`
  ${newTypo("body-14-med")};
  color: ${newColors.GRAY_500};
`;

const Count = styled(FlexWrapper)`
  gap: 5px;
  ${newTypo("body-12-med")};
  color: ${newColors.GRAY_600};
`;