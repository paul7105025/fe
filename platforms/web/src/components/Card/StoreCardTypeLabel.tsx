import { newColors, newTypo } from "@wrtn/ui";
import styled from 'styled-components';

export const StoreCardTypeLabel = ({ type }) => {
  
  if (type === "tool") return (
    <Label style={{background: `${newColors.PURPLE_500_PRIMARY}`}}>
      툴
    </Label>
  )
  if (type === "chatbot") return (
    <Label style={{background: `${newColors.BLUE_500_NOTICE}`}}>
      챗봇
    </Label>
  )
  return null;
}

const Label = styled.div`
  padding: 4px 10px;
  border-radius: 5px;
  ${newTypo("content-14-semi")};
  color: ${newColors.WHITE};
`;