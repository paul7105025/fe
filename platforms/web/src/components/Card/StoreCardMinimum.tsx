import { boxShadow, colors, FlexWrapper, newColors, newTypo, STORE_ICON_SELECTOR, TOOL_ICON_SELECTOR } from "@wrtn/ui";
import { StoreCardTypeLabel } from "./StoreCardTypeLabel";
import styled from 'styled-components';


export const StoreCardMinimum = ({
  onClick,
  type,
  icon,
  title
}) => {
  return (
    <Wrapper align="center" onClick={onClick}>
      <IconWrapper>{STORE_ICON_SELECTOR[icon]}</IconWrapper>
      <StoreCardTypeLabel type={type} />
      <Title>{title}</Title>
    </Wrapper>
  )
}

export default StoreCardMinimum;


const Wrapper = styled(FlexWrapper)`
  border-radius: 10px;
  padding: 16px 16px;
  gap: 10px;
  background-color: ${newColors.WHITE};
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  ${boxShadow.basic_card_shadow};
  width: 100%;

  cursor: pointer;
  :hover {
    ${boxShadow.basic_card_hover_shadow};
  }
`;

const Title = styled.div`
  ${newTypo("heading-18-semi")};
  color: ${newColors.GRAY_800};
`;

const IconWrapper = styled(FlexWrapper)`

  svg {
    width: 22px;
    height: 22px;
  }
`;