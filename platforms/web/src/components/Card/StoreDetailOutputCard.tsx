import { AutoSizeTextArea, colors, Copy, newColors, newTypo } from '@wrtn/ui';
import CopyToClipboard from 'react-copy-to-clipboard';
import styled from 'styled-components';

export const StoreDetailOutputCard = ({ text }) => { 
  return (
    <Wrapper>
      <InnerWrapper>
        <TextWrapper>
          <Text value={text} contentEditable={false}/>
        </TextWrapper>
        <BottomMenuWrapper>
          <CountWrapper>
            공백 제외<CountText>{text.replace(/\s/g, "").length}자</CountText>
          </CountWrapper>
          <CountWrapper>
            공백 포함<CountText>{text.length}자</CountText>
          </CountWrapper>
          <Spacer />
          <CopyToClipboard text={text}>
            <IconButton>
              <Copy />
            </IconButton>
          </CopyToClipboard>
        </BottomMenuWrapper>
      </InnerWrapper>
    </Wrapper>
  )
}

export default StoreDetailOutputCard;

const Wrapper = styled.div`
  width: 100%;
  padding: 3px;
  background: white;
  border-radius: 20px;
  margin-bottom: 20px;
  border: 1px solid ${newColors.GRAY_200};

  &:hover {
    filter: drop-shadow(0px 4px 24px rgba(79, 68, 195, 0.1));
    border: 1px solid ${newColors.PURPLE_500_PRIMARY};
  }
`;

const InnerWrapper = styled.div`
  width: 100%;
  padding: 16px 20px;
  background: white;
  border-radius: 17px;
`;

const Text = styled(AutoSizeTextArea)`
  width: 100%;
  resize: none;
  border: none;
  font-weight: 600;
  font-size: 16px;
  line-height: 170%;
  color: ${colors.GRAY_90};
  padding: 7px 13px;
  margin-bottom: 25px;
  border-radius: 5px;
  &:focus {
    outline: none;
  }
`;

const CountWrapper = styled.div`
  display: flex;
  gap: 4px;
  justify-content: flex-end;
  ${newTypo("content-14-semi")};
  color: ${colors.GRAY_58};
`;

const CountText = styled.p`
  color: ${colors.GRAY_60};
`;

const BottomMenuWrapper = styled.div`
  display: flex;
  gap: 14px;
  padding: 0px 27px;
`;

const IconButton = styled.button`
  background: none;
  border: none;
  svg {
    width: 15px;
    height: 15px;
    path {
      fill: ${colors.gray_55};
    }
  }
  &:hover {
    cursor: pointer;
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  }
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const TextWrapper = styled.div`
  padding: 0px 15px;
  width: 100%;
`;
