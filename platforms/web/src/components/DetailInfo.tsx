import { dateCalculator } from "@wrtn/core";
import { colors, FilledStar, FlexWrapper, Icon, newColors, newTypo } from "@wrtn/ui";
import React from "react";
import { StoreCardTypeLabel } from "src/components/Card";
import { formatNumber } from "src/utils";
import styled from "styled-components";

export const DetailInfo = ({
  icon,
  type,
  title,
  description,
  author,
  createdAt,
  viewCount,
  category,
  isFavorite,
  genCount,
  // exportCount,
  favoriteCount,
  onClick,
  onClickExport,
  onClickFavorite,
}) => {

  return (
    <Wrapper justify="space-between" align="flex-start">
      <Etc>
        {/* statitlcayy */}
        <EtcBlock align="center">
          {/* <Count align="center"><Icon icon="share-alt" size={24} />공유</Count> */}
          {/* <VerticalDivider /> */}
          <Count><Icon icon="eye" size={20} />{formatNumber(viewCount)}</Count>
          <Count>
            <IconWrapper onClick={onClickFavorite}>
            { isFavorite
              ? <FilledStar />
              : <Icon icon="star" size={20} />
            }
            </IconWrapper>
            {formatNumber(favoriteCount + (isFavorite ? 1 : 0))}
          </Count>
          <Count><Icon icon="play" size={20} />{formatNumber(genCount)}</Count>
        </EtcBlock>
      </Etc>
      <Info>
        {/* info */}
        <FlexWrapper style={{gap: '12px', paddingBottom: '16px'}}>
          <StoreCardTypeLabel type={type}/>
          <Title>{title}</Title>
        </FlexWrapper>
        <Desc>
          { description }
        </Desc>
        <CategoryWrapper>
          {
            category.map(v => (
              <Category key={v}>
                {v}
              </Category>
            ))
          }
        </CategoryWrapper>
        <MetaData>
          { author } | { dateCalculator(createdAt?.toString()) }
        </MetaData>
      </Info>
    </Wrapper>
  )
}

export default DetailInfo;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  padding-bottom: 32px;
  flex-direction: row-reverse;

  @media(max-width: 1140px) {
    display: initial;
  }
`

const Title = styled.div`
  ${newTypo("heading-18-semi")};
  color: ${newColors.GRAY_800};
`;

const Info = styled.div`
`;

const Etc = styled(FlexWrapper)`
  @media(max-width: 1140px) {
    justify-content: center;
  }
  padding-bottom: 30px;
`;

const Desc = styled.div`
  ${newTypo("content-16-med")};
  color: ${newColors.GRAY_700};
  padding-bottom: 20px;
`;

const MetaData = styled.div`
  ${newTypo("body-16-med")};
  color: ${newColors.GRAY_500};
`;

const EtcBlock = styled(FlexWrapper)`
  background-color: ${newColors.BLUE_200_BG};
  border-radius: 5px;
  border: 1px solid ${newColors.GRAY_100};
  ${newTypo("body-16-med")};
  color: ${colors.gray_60};
  padding: 12px 16px;
  gap: 18px;
`;  

const VerticalDivider = styled.div`
  width: 1.5px;
  height: 20px;
  background-color: ${colors.gray_60};
`;

const Count = styled(FlexWrapper)`
  ${newTypo("body-16-med")};
  gap: 6px;
`;

const CategoryWrapper = styled(FlexWrapper)`
  gap: 8px;
  padding-bottom: 29px;
`;

const Category = styled.div`
  color: ${colors.gray_70};
  ${newTypo("content-14-med")};
  padding: 5px 9px;
  border: 1px solid ${newColors.GRAY_300};
  background: ${newColors.WHITE};
  border-radius: 5px;
`;

const IconWrapper = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  svg {
     width: 20px;
     height: 20px;
  }
`;