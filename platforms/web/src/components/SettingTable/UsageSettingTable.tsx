import React from "react";
import dayjs from "dayjs";

import { PaginationWrapper } from "../SettingUsage";
import { Pagination } from "@wrtn/ui/components/Pagination";

import {
  ContentWrapper,
  TitleWrapper,
  Title,
  Table,
  TableHeadRow,
  TableHead,
  TablePCHead,
  TableBodyRow,
  TableRow,
  TableData,
  TablePCData,
  NoPayment,
} from "./styles";

export const UsageSettingTable = ({ usageHistory }) => {
  const [pagination, setPagination] = React.useState(0);

  const maxPage = React.useMemo(() => {
    return Math.floor((usageHistory?.length || 0) / 10);
  }, [usageHistory]);

  return (
    <ContentWrapper>
      <TitleWrapper>
        <Title>충전 내역</Title>
      </TitleWrapper>
      <Table>
        <TableHeadRow>
          <tr>
            <TableHead>충전량</TableHead>
            <TableHead>내용</TableHead>
            <TableHead>날짜</TableHead>
            <TablePCHead>유효기한</TablePCHead>
          </tr>
        </TableHeadRow>
        <TableBodyRow>
          {Array.isArray(usageHistory) &&
            usageHistory.map((data, idx) => {
              return (
                <TableRow key={idx}>
                  <TableData width="20%">
                    + {Number(data?.totalWordCount || 0).toLocaleString()}자
                  </TableData>
                  <TableData width="40%">{data?.description}</TableData>
                  <TableData width="20%">
                    {dayjs(data?.paymentDate).format("YYYY년 MM월 DD일")}
                  </TableData>
                  <TablePCData width="20%">
                    {data?.dueDate
                      ? dayjs(data?.dueDate).format("YYYY년 MM월 DD일")
                      : "-"}
                  </TablePCData>
                </TableRow>
              );
            })}
        </TableBodyRow>
      </Table>
      {usageHistory?.length === 0 && (
        <NoPayment>충전 내역이 없습니다.</NoPayment>
      )}
      {Array.isArray(usageHistory) && usageHistory.length > 0 && (
        <PaginationWrapper>
          <Pagination
            maxPage={maxPage}
            currentPage={pagination}
            setPage={setPagination}
          />
        </PaginationWrapper>
      )}
    </ContentWrapper>
  );
};
