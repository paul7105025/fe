import React from "react";
import dayjs from "dayjs";

import {
  couponApplyTextShort,
  couponConditionText,
  couponFilterList,
  couponFilterType,
  couponType,
  userCouponStatusRecord,
} from "@wrtn/core";

import { PaginationWrapper } from "../SettingUsage";

import { ListSelect } from "@wrtn/ui/components/Select";
import { Pagination } from "@wrtn/ui/components/Pagination";

import {
  ContentWrapper,
  TitleWrapper,
  SelectWrapper,
  Title,
  Table,
  TableHeadRow,
  TableHead,
  TablePCHead,
  TableBodyRow,
  TableRow,
  TableData,
  TablePCData,
  NoPayment,
  InnerWrapper,
} from "./styles";

type couponStatusType = "available" | "used" | "using" | "expired";

interface CouponSettingTableProps {
  couponHistory: Array<{
    count: number;
    status: couponStatusType;
    userId: string;
    coupon: couponType;
  }>;
  currentFilter: couponFilterType;
  setCurrentFilter: (filter: couponFilterType) => void;
}

export const CouponSettingTable = ({
  couponHistory,
  currentFilter,
  setCurrentFilter,
}: CouponSettingTableProps) => {
  const [pagination, setPagination] = React.useState(0);

  const maxPage = React.useMemo(() => {
    return Math.floor((couponHistory?.length || 0) / 10);
  }, [couponHistory]);

  const filteredCouponHistory = React.useMemo(() => {
    if (currentFilter === "사용 가능 쿠폰") {
      return couponHistory.filter((item) => item.status === "available");
    }
    if (currentFilter === "할인 쿠폰") {
      return couponHistory.filter((item) => item.coupon.type === "discount");
    }
    return couponHistory;
  }, [couponHistory, currentFilter]);

  return (
    <ContentWrapper>
      <TitleWrapper>
        <InnerWrapper>
          <Title>쿠폰 목록</Title>
          {/* <LinkText to="/app/setting/account?recommend=true">
            할인 쿠폰을 발급받고 싶다면 ?
          </LinkText> */}
        </InnerWrapper>
        <div style={{ flex: 1 }} />
        <SelectWrapper>
          <ListSelect
            value={currentFilter}
            setValue={(filter) => setCurrentFilter(filter as couponFilterType)}
            list={couponFilterList}
            small
          />
        </SelectWrapper>
      </TitleWrapper>
      <Table>
        <TableHeadRow>
          <tr>
            <TableHead>이름</TableHead>
            <TableHead>혜택 내용</TableHead>
            <TableHead>혜택 조건</TableHead>
            <TablePCHead>혜택 적용 기간</TablePCHead>
            <TablePCHead>쿠폰 만료일</TablePCHead>
            <TableHead>사용 여부</TableHead>
          </tr>
        </TableHeadRow>
        <TableBodyRow>
          {filteredCouponHistory.map((item, idx) => {
            return (
              <TableRow
                key={item.coupon._id}
                isDisabled={item.status !== "available"}
              >
                <TableData width="20%">{item.coupon.name}</TableData>
                <TableData width="16%">{item.coupon.description}</TableData>
                <TableData width="18%">
                  {/* {item.coupon?.condition ? COUPON_CONDITION_RECORD[item.coupon.condition] : "-"} */}
                  {couponConditionText(item.coupon, true)}
                </TableData>
                <TablePCData width="20%">
                  {/* {`사용일로부터 ${item.coupon.count}개월`} */}
                  {couponApplyTextShort(item.coupon)}
                </TablePCData>
                <TablePCData width="18%">
                  {dayjs(item.coupon.dueDate).format("YYYY년 MM월 DD일")}
                </TablePCData>
                <TableData width="8%">
                  {userCouponStatusRecord[item.status]}
                </TableData>
              </TableRow>
            );
          })}
        </TableBodyRow>
      </Table>
      {!couponHistory || couponHistory.length < 1 ? (
        <NoPayment>쿠폰 목록이 없습니다.</NoPayment>
      ) : (
        filteredCouponHistory.length < 1 && (
          <NoPayment>조건에 맞는 쿠폰 목록이 없습니다.</NoPayment>
        )
      )}
      {couponHistory.length > 0 && (
        <PaginationWrapper>
          <Pagination
            maxPage={maxPage}
            currentPage={pagination}
            setPage={setPagination}
          />
        </PaginationWrapper>
      )}
    </ContentWrapper>
  );
};
