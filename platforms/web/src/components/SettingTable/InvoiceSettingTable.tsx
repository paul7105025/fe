import React from "react";
import dayjs from "dayjs";

import { PaginationWrapper } from "../SettingUsage";
import { ChannelButton } from "@wrtn/ui/components/ChannelButton";
import { Pagination } from "@wrtn/ui/components/Pagination";

import { PaymentHistory, useTagManager, useEvent } from "@wrtn/core";

import {
  ContentWrapper,
  TitleWrapper,
  Title,
  Table,
  TableHeadRow,
  TableHead,
  TablePCHead,
  TableBodyRow,
  TableRow,
  TableData,
  TablePCData,
  A,
  NoPayment,
} from "./styles";

export const InvoiceSettingTable = ({
  paymentHistory,
}: {
  paymentHistory: Array<PaymentHistory>;
}) => {
  const [pagination, setPagination] = React.useState(0);

  const maxPage = React.useMemo(() => {
    return Math.floor((paymentHistory?.length || 0) / 10);
  }, [paymentHistory]);

  const tagManager = useTagManager();
  const { collectEvent } = useEvent();

  return (
    <ContentWrapper>
      <TitleWrapper>
        <Title>결제 내역</Title>
        {paymentHistory && (
          <ChannelButton
            label="환불문의"
            onClick={() => {
              tagManager({ event: "refund_request" });
              collectEvent("click_ask_fund");
              collectEvent("click_channel_talk");
            }}
          />
        )}
      </TitleWrapper>
      <Table>
        <TableHeadRow>
          <tr>
            <TableHead>내용</TableHead>
            <TablePCHead>날짜</TablePCHead>
            <TableHead style={{ textAlign: "right" }}>금액</TableHead>
            <TableHead>&nbsp;</TableHead>
            <TablePCHead>구분</TablePCHead>
            <TableHead>영수증</TableHead>
          </tr>
        </TableHeadRow>
        <TableBodyRow>
          {Array.isArray(paymentHistory) &&
            paymentHistory.map((item, idx) => {
              return (
                <TableRow key={idx}>
                  <TableData>{item.orderName}</TableData>
                  <TablePCData width="20%">
                    {dayjs(item.approvedAt).format("YYYY년 MM월 DD일")}
                  </TablePCData>
                  <TableData
                    width="20%"
                    align="right"
                    style={{ textAlign: "right" }}
                  >
                    {Number(item.totalAmount).toLocaleString()}원
                  </TableData>
                  <TableData width="10%">&nbsp;</TableData>
                  <TablePCData width="15%">{item.kind}</TablePCData>
                  <TableData>
                    <A target="_blank" href={item.receipt?.url}>
                      영수증 확인하기
                    </A>
                  </TableData>
                </TableRow>
              );
            })}
        </TableBodyRow>
      </Table>
      {(!paymentHistory || paymentHistory.length < 1) && (
        <NoPayment>결제 내역이 없습니다.</NoPayment>
      )}
      {Array.isArray(paymentHistory) && paymentHistory.length > 0 && (
        <PaginationWrapper>
          <Pagination
            maxPage={maxPage}
            currentPage={pagination}
            setPage={setPagination}
          />
        </PaginationWrapper>
      )}
    </ContentWrapper>
  );
};
