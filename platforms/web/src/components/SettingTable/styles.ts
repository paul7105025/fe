import styled from "styled-components";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import Link from "next/link";

export const ContentWrapper = styled.div`
  margin-bottom: 34px;
  width: 100%;
`;

export const TitleWrapper = styled(FlexWrapper)`
  margin-bottom: 20px;

  justify-content: space-between;
  align-items: center;

  @media (max-width: 480px) {
    /* flex-direction: column; */
  }
`;

export const InnerWrapper = styled(FlexWrapper)`
  gap: 15px;

  @media (max-width: 480px) {
    margin-bottom: 15px;
  }
`;

export const Title = styled.div`
  ${typo({
    weight: 600,
    color: colors.GRAY_80,
    size: "16px",
  })}

  text-align: left;
`;

export const LinkText = styled(Link)`
  ${typo({
    weight: 500,
    color: colors.ACTION_BLUE,
    size: "14px",
  })}
  text-decoration: underline;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

export const TableRow = styled.tr<{
  isDisabled?: boolean;
}>`
  border-bottom: 1px solid #d8dde9;

  ${({ isDisabled }) =>
    typo({
      weight: 600,
      size: "16px",
      color: isDisabled ? colors.GRAY_58 : colors.GRAY_90,
    })}
`;

export const TableHead = styled.th<{ right?: boolean }>`
  font-weight: 400;
  font-size: 12px;
  line-height: 100%;

  color: #717488;
  padding-bottom: 20px;
  text-align: ${(props) => (props.right ? "right" : "left")};
`;

export const TableHeadRow = styled.thead`
  border-bottom: 1px solid #717488;
`;

export const TableBodyRow = styled.tbody``;

export const TableData = styled.td<{ align?: "left" | "right" }>`
  padding: 20px 0px;

  text-align: ${(props) => (props.align ? props.align : "left")};

  @media (max-width: 630px) {
    font-size: 12px;
  }
`;

export const A = styled.a`
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;

  color: #5a2cda;

  @media (max-width: 630px) {
    font-size: 12px;
  }
`;

export const PaginationWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 20px;
`;

export const NoPayment = styled.div`
  position: absolute;
  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: colors.gray_90,
  })};
  right: 50%;
  transform: translate(50%, 30px);
`;

export const TablePCHead = styled(TableHead)`
  @media (max-width: 630px) {
    display: none;
  }
`;

export const TablePCData = styled(TableData)`
  @media (max-width: 630px) {
    display: none;
  }
`;

export const SelectWrapper = styled(FlexWrapper)`
  width: 200px;
  align-self: flex-end;
`;
