export type InputForm = {
  label: string;
  description?: string;
  placeholder?: string;
  value: string;
  name?: string;
};

export type TextAreaForm = {
  label: string;
  description?: string;
  placeholder?: string;
  value: string;
  name?: string;
};

export type OptionItem = {
  label: string;
  value: string;
};

export type OptionForm = {
  label: string;
  description?: string;
  options: OptionItem[];
  value: string;
  name?: string;
};

export type RangeForm = {
  label: string;
  description?: string;
  placeholder?: string;
  value: number;
  name?: string;
  min: number;
  max: number;
  step: number;
};

export type MultipleOptionForm = {
  label: string;
  description?: string;
  options: OptionItem[];
  value: string[];
  name?: string;
};

export type DropdownForm = {
  label: string;
  description?: string;
  options: OptionItem[];
  value: string;
  name?: string;
};

export type DropdownIconForm = {
  label: string;
  description?: string;
  options: string[];
  value: string;
  name?: string;
};

export type DropdownChipForm = {
  label: string;
  description?: string;
  options: OptionItem[] | string[];
  value: string[];
  name?: string;
};

export type FormType = "input" | "textarea" | "option" | "dropdown";

export type Form = {
  id: string;
  text?: string;
  type: FormType;
  data: InputForm | TextAreaForm | OptionForm | DropdownForm;
};

export type Tool = {
  _id?: string;
  description: string;
  forms: Form[];
  title: string;
  model?: "gpt-3.5-turbo" | "gpt-4" | "text-davinci-003" | "";
  icon: string;
  category: string[];
  isPublished: string;
  isDeleted: boolean;
  isTemporarySave: boolean;
  priceType?: string;
  views: number;
  likes: number;
  updatedAt?: string;
  outputExample: string;
  usedNum: number;
  userId?: string;
  difficulty: string;
  promptForDifficult: string; // 수정 필요
  promptForEasy: string; // 수정필요
  maxTokens: number;
};

export const PlanList = ["FREE", "PLUS"] as const;

export type PlanType = (typeof PlanList)[number];

export const providerList = ["local", "google", "kakao", "naver"] as const;

export type ProviderType = (typeof providerList)[number];
export interface userType {
  name: string;
  email: string;
  password?: string;
  salt?: string;
  number: string;
  provider: ProviderType;
  inflow: string;
  company: string;
  job: Array<string>;
  purpose: Array<string>;
  status: "user" | "admin" | "ban";
  refreshToken: string;
  isNewbie: boolean;
  agreementDate: string | null;
  serviceTerm: string | null;
  privacyTerm: string | null;
  marketingTerm: string | null;
  isActive: boolean;
  isDeleted: boolean;
  deletedAt: string;
  cards: Array<string>;
  usages: Array<any>;

  plan: PlanType;
  nextPlan: PlanType;

  dueDate: string | null;
  paymentDate: null;

  parentUserId: string;
  recommendCode: string;
  childUserIdList: Array<string>;

  createdAt: string;
  updatedAt: string;
  _id: string;

  isRefund?: boolean;
  associatedFunnel: string;
}
