// import { DropdownForm, OptionItem } from "@/types/declare";
import { colors, IconWaterDropList } from "@wrtn/ui";
import React from "react";
import styled from "styled-components";
import { DropdownForm, OptionItem } from "./types";
import { ReactComponent as DropDown } from "./dropdown.svg";

interface DropdownInputType extends DropdownForm {
  onChange: Function;
  required?: boolean;
}

export const DynamicDropdownWithLabel = ({
  label,
  options,
  onChange,
  value,
  description,
  required = false,
}: DropdownInputType) => {
  const [clicked, setClicked] = React.useState(false);

  const handleChangeValue = (item: OptionItem) => {
    let value = (!item.value || item.value === "") ? item.label : item.value;
    onChange(value)
  }

  return (
    <InputWrapper>
      <Label>
        {label} {required && <Star> *</Star>}
      </Label>
      {description ? (
        <Description>{description}</Description>
      ) : (
        <DescriptionNull />
      )}
      <Selector onClick={() => setClicked(!clicked)}>
        {options.find((e) => 
          (e.value === value) || (e.value === "" || !e.value) && e.label === value
        )?.label || "선택해주세요."}

        {/* <DropdownImage src="/dropdown.svg" /> */}
        {/* <DropdownImage src="./dropdown.svg" /> */}
        <DropDownIcon />
      {clicked ? (
        <OptionGroup isActive={clicked}>
          {options?.map((item, idx) => {
            return (
              <Option key={item.label} onClick={() => handleChangeValue(item)}>
                {item.label}
              </Option>
            );
          })}
        </OptionGroup>
      ) : null}
      </Selector>
    </InputWrapper>
  );
};

const InputWrapper = styled.div`
  width: 100%;
  position: relative;
  margin-top: 32px;
`;

const Label = styled.div`
  height: 16px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;
  color: #505467;
`;

const Star = styled.span`
  color: #ff6363;
`;

const Description = styled.div`
  margin: 16px 0px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 160%;
  color: #717688;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const DescriptionNull = styled.div`
  margin-top: 20px;
`;

const Selector = styled.div`
  position: relative;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  justify-content: space-between;
  line-height: 26px;
  width: 100%;
  padding: 8px 16px;
  border: none;
  background: #fff;
  border-radius: 8px;
  border: 1px solid #c3c8d9;
  &::placeholder {
    color: ${colors.GRAY_55};
  }
  &:focus {
    outline: none;
    padding: 7px 15px;
  }
  &:hover {
    cursor: pointer;
    div {
      cursor: pointer;
      display: block;
    }
  }
`;

const OptionGroup = styled.div<{ isActive?: boolean }>`
  display: none;
  position: absolute;
  gap: 10px;
  padding: 8px 16px;
  width: calc(100% + 2px);
  bottom: -1px;
  left: -1px;
  background: white;
  border-radius: 8px;
  z-index: 9;
  border: 1px solid #cccccc;
  max-height: 300px;
  overflow: auto;
`;

const Option = styled.div`
  padding: 10px 20px;
  background: #fff;
  &:hover {
    background: #f9fbfc;
  }
`;

const DropdownImage = styled.img`
  width: 11px;
  height: 7px;
  flex: none;
  position: absolute;
  top: 17.5px;
  right: 17.5px;
  order: 1;
  flex-grow: 0;
`;


const DropDownIcon = styled(DropDown)`
width: 11px;
height: 7px;
flex: none;
position: absolute;
top: 17.5px;
right: 17.5px;
order: 1;
flex-grow: 0;
`;