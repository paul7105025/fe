import { ChangeEventHandler } from "react";
import styled from "styled-components";
import { AutoSizeTextArea, colors } from "@wrtn/ui";
import { TextAreaForm } from "./types";

interface TextAreaInputForm extends TextAreaForm {
  onChange: ChangeEventHandler<HTMLTextAreaElement>;
  defaultValue?: string;
  required?: boolean;
}

export const DynamicTextAreaWithLabel = ({
  label,
  placeholder,
  description,
  onChange,
  value,
  name,
  defaultValue,
  required = false,
}: TextAreaInputForm) => {
  return (
    <InputWrapper>
      <Label>
        {label} {required && <Star> *</Star>}
      </Label>
      {description ? (
        <Description>{description}</Description>
      ) : (
        <DescriptionNull />
      )}

      <Textarea
        name={name}
        value={value}
        onChange={onChange}
        defaultValue={defaultValue}
        placeholder={placeholder}
        spellCheck={false}
        minRows={3}
      />
    </InputWrapper>
  );
};

const InputWrapper = styled.div`
  width: 100%;
  margin-top: 32px;
`;

const Label = styled.div`
  height: 16px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;
  color: #505467;
`;

const Star = styled.span`
  color: #ff6363;
`;

const Description = styled.div`
  margin: 16px 0px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 160%;
  color: #717688;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const DescriptionNull = styled.div`
  margin-top: 20px;
`;

const Textarea = styled(AutoSizeTextArea)`
  width: 100%;
  padding: 8px 16px;
  border: none;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 26px;
  background: #f2f7ff;
  border-radius: 8px;
  resize: none;
  &:focus {
    outline: none;
    border: 1px solid ${colors.ACTION_BLUE};
    padding: 7px 15px;
  }
  &::placeholder {
    color: ${colors.GRAY_55};
  }
`;
