import { newColors, newTypo } from "@wrtn/ui";
import React from "react";
import styled from "styled-components";
import { OptionForm, OptionItem } from "./types";

interface OptionInputForm extends OptionForm {
  onChange: Function;
  required?: boolean;
}

interface TempOption extends OptionItem {
  id: string;
}

export const DynamicOptionWithLabel = ({
  label,
  description,
  onChange,
  value,
  name,
  options,
  required = false,
}: OptionInputForm) => {
  const tempValue: string[] = React.useMemo(() => {
    const arr = value.split(",");
    return arr.filter((e) => e.trim() !== "");
  }, [value]);

  const handleChange = (item: string) => {
    const tempIdx = tempValue.findIndex((e) => e === item);
    if (tempIdx > -1) {
      let t = [...tempValue.slice(0, tempIdx), ...tempValue.slice(tempIdx + 1)];
      onChange(t.join(","));
    } else {
      let t = [...tempValue, item];
      onChange(t.join(","));
    }
  };


  return (
    <InputWrapper>
      <Label>
        {label} {required && <Star> *</Star>}
      </Label>
      {description ? (
        <Description>{description}</Description>
      ) : (
        <DescriptionNull />
      )}
      <MockOptionWrapper>
        {options?.map((item, idx) => {
          const selected = tempValue.findIndex((e) => e === item.label) > -1;
          return (
            <OptionChip
              key={idx}
              selected={selected}
              onClick={() => handleChange(item.label)}
            >
                {String(
                  typeof item === "string" ? item : (item as OptionItem).label
                )}
            </OptionChip>
          );
        })}
      </MockOptionWrapper>
    </InputWrapper>
  );
};

const InputWrapper = styled.div`
  width: 100%;
  margin-top: 32px;
`;

const Label = styled.div`
  height: 16px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;
  color: #505467;
`;

const Star = styled.span`
  color: #ff6363;
`;

const MockOptionWrapper = styled.div`
  display: flex;
  gap: 12px;
  flex-wrap: wrap;
`;

const OptionChip = styled.div<{ selected: boolean }>`
  cursor: pointer;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 8px 16px;
  gap: 10px;
  height: 35px;
  background: ${props => props.selected ? newColors.PURPLE_500_PRIMARY : newColors.WHITE};
  color: ${props => !props.selected ? newColors.PURPLE_500_PRIMARY : newColors.WHITE};
  border: 1px solid #5a2cda;
  border-radius: 8px;
  flex: none;
  order: 0;
  flex-grow: 0;

  ${newTypo("body-16-med")};

  // background: #fff;
  // &:hover {
  //   background: #f9fbfc;
  // }

  
`;

const Description = styled.div`
  margin: 16px 0px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 160%;
  color: #717688;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const DescriptionNull = styled.div`
  margin-top: 20px;
`;

const OptionWrapper = styled.div``;
