import React from "react";
import dayjs from "dayjs";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import {
  getUsageCurrent,
  userState,
  useTagManager,
  useTimeCounter,
} from "@wrtn/core";

import { PLAN_PRICE, PLAN_NAME, PLAN_AMOUNT } from "@wrtn/core/constants";

import { isUpperPlan } from "../../utils/isUpperPlan";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

import { Arrow, Close } from "@wrtn/ui/assets/SVGComponent/Icon";
import { ReactComponent as Check } from "@wrtn/ui/assets/icons/icon_check.svg";
import { getCurrentPrice } from "./PaymentContainer";
import polyfill from "@wrtn/ui/styles/polyfill";

const planDescription = {
  upper: {
    title: "요금제 업그레이드 주의사항",
    contents: [
      "상위 요금제로 변경 시 새로운 요금제의 금액이 즉시 결제됩니다.",
      "결제와 동시에 매월 결제일이 결제일을 기준으로 변경됩니다.",
      "남은 글자 수는 변경 전 요금제의 다음 결제일에 만료됩니다.",
      "변경된 요금제에 대한 환불은 7일 이내, 미사용 시에만 가능합니다.",
    ],
  },
  lower: {
    title: "요금제 변경 주의사항",
    contents: [
      "하위 요금제로 변경 시 다음 결제일 부터 해당 요금제의 금액이 결제됩니다.",
      "다음 결제일까지 변경 전 요금제가 유지됩니다.",
      "남은 글자 수에 대한 환불은 불가능합니다.",
      "남은 글자 수는 다음 결제일에 만료됩니다.",
    ],
  },
};

const getNextPayment = (date) => {
  let dueDate = dayjs(date).add(1, "month");

  if (!dayjs(dueDate).isValid()) {
    dueDate = dayjs().add(1, "month").endOf("month").toDate();
  }

  return dueDate;
};
  

const title = (state) => {
  if (state === "결제") return "요금제를 결제하시겠어요?";
  if (state === "쿠폰적용") return "쿠폰을 적용하시겠어요?"
  return "요금제를 변경하시겠어요?"
}

const paymentDateText = (state) => {
  if (state === "결제") return "이번달 결제 금액";
  return "다음달 결제 금액"
}

const confirmText = (state) => {
  if (state === "결제") return "결제하기";
  return "변경하기"
}

const ConfirmModal = ({ onSubmit, onClose, plan, type, currentCoupon }) => {
  const user = useRecoilValue(userState);
  const [consent, setConsent] = React.useState({
    first: false,
    second: false,
    third: false,
    fourth: false,
    fifth: false,
  });

  const confirmModalState = isUpperPlan(user?.plan, plan) ? "결제"
  : currentCoupon ? "쿠폰적용"
  : "변경" 

  const render = () => {
      return (
        <React.Fragment>
          <ConfirmContentWrapper>
            <ContentPointLabel>선택한 요금제</ContentPointLabel>
            <ConfirmPlanTitleWrapper>
              <ConfirmPlanName>{PLAN_NAME[plan]}</ConfirmPlanName>
              <ConfirmPlanPriceWrapper>
                <ConfirmPlanPrice>
                  {Number(PLAN_PRICE[plan]).toLocaleString()}원
                </ConfirmPlanPrice>
                <ConfirmPlanPriceUnit>/월</ConfirmPlanPriceUnit>
              </ConfirmPlanPriceWrapper>
            </ConfirmPlanTitleWrapper>
            {currentCoupon && (
              <ConfirmPlanCouponWrapper column>
                <ConfirmCouponWrapper>
                  <ConfirmDateLabel>쿠폰 사용</ConfirmDateLabel>
                  <ConfirmPlanPriceWrapper>
                    <ConfirmPlanPrice>
                      -
                      {Number(
                        getCurrentPrice(currentCoupon.coupon.percent)
                          .couponPrice
                      ).toLocaleString()}
                      원
                    </ConfirmPlanPrice>
                  </ConfirmPlanPriceWrapper>
                </ConfirmCouponWrapper>
                <ConfirmCouponWrapper>
                  <ConfirmDateLabel>{paymentDateText(confirmModalState)}</ConfirmDateLabel>
                  <ConfirmPlanPriceWrapper>
                    <ConfirmPlanPrice
                      style={{
                        color: colors.POINT_PURPLE,
                      }}
                    >
                      {Number(
                        getCurrentPrice(currentCoupon.coupon.percent)
                          .currentPrice
                      ).toLocaleString()}
                      원
                    </ConfirmPlanPrice>
                  </ConfirmPlanPriceWrapper>
                </ConfirmCouponWrapper>
              </ConfirmPlanCouponWrapper>
            )}
            { confirmModalState === "결제" &&
              <ConfirmDateWrapper>
                <ConfirmDateLabel>구독 시작일</ConfirmDateLabel>
                <ConfirmDateValue>
                  {dayjs().format("YYYY.MM.DD")}
                </ConfirmDateValue>
              </ConfirmDateWrapper>
            }
            <ConfirmDateWrapper>
              <ConfirmDateLabel>다음 결제일</ConfirmDateLabel>
              <ConfirmDateValue>
                {dayjs(getNextPayment(dayjs(Date.now()))).format("YYYY.MM.DD")}
              </ConfirmDateValue>
            </ConfirmDateWrapper>
          </ConfirmContentWrapper>
          <ConsentWrapper>
            <AgreementWrapper
              onClick={() => setConsent({ ...consent, first: !consent.first })}
            >
              <CheckBox consent={consent.first}>
                <Check />
              </CheckBox>
              <Agreement>전자금융거래 기본약관 동의 (필수)</Agreement>
              <AgreementLink
                target="_blank"
                href={"https://pages.tosspayments.com/terms/user"}
                onClick={(e) => e.stopPropagation()}
              >
                자세히 보기
              </AgreementLink>
            </AgreementWrapper>
            <AgreementWrapper
              onClick={() =>
                setConsent({ ...consent, second: !consent.second })
              }
            >
              <CheckBox consent={consent.second}>
                <Check />
              </CheckBox>
              <Agreement>고유식별정보의 수집 및 이용 동의 (필수)</Agreement>
              <AgreementLink
                target="_blank"
                href={
                  "https://pages.tosspayments.com/terms/unique-identification"
                }
                onClick={(e) => e.stopPropagation()}
              >
                자세히 보기
              </AgreementLink>
            </AgreementWrapper>
            <AgreementWrapper
              onClick={() => setConsent({ ...consent, third: !consent.third })}
            >
              <CheckBox consent={consent.third}>
                <Check />
              </CheckBox>
              <Agreement>자동승인(정기결제) 이용약관 (필수)</Agreement>
              <AgreementLink
                target="_blank"
                href={
                  "https://xpayvvip.uplus.co.kr/xpay/pop/term/infotrans1_10.html"
                }
                onClick={(e) => e.stopPropagation()}
              >
                자세히 보기
              </AgreementLink>
            </AgreementWrapper>
            <AgreementWrapper
              onClick={() =>
                setConsent({ ...consent, fourth: !consent.fourth })
              }
            >
              <CheckBox consent={consent.fourth}>
                <Check />
              </CheckBox>
              <Agreement>개인정보 수집이용 동의 (필수)</Agreement>
              <AgreementLink
                target="_blank"
                href={"https://pages.tosspayments.com/terms/privacy"}
                onClick={(e) => e.stopPropagation()}
              >
                자세히 보기
              </AgreementLink>
            </AgreementWrapper>
            <AgreementWrapper
              onClick={() => setConsent({ ...consent, fifth: !consent.fifth })}
            >
              <CheckBox consent={consent.fifth}>
                <Check />
              </CheckBox>
              <Agreement>개인정보 제3자 제공 동의 (필수)</Agreement>
              <AgreementLink
                target="_blank"
                href={"https://pages.tosspayments.com/terms/privacy2"}
                onClick={(e) => e.stopPropagation()}
              >
                자세히 보기
              </AgreementLink>
            </AgreementWrapper>
          </ConsentWrapper>

          <AgreementWrapper
            onClick={() => {
              if (
                consent.third &&
                consent.second &&
                consent.first &&
                consent.fourth &&
                consent.fifth
              ) {
                setConsent({
                  first: false,
                  second: false,
                  third: false,
                  fourth: false,
                  fifth: false,
                });
              } else {
                setConsent({
                  first: true,
                  second: true,
                  third: true,
                  fourth: true,
                  fifth: true,
                });
              }
            }}
          >
            <CheckBox
              consent={
                consent.third &&
                consent.second &&
                consent.first &&
                consent.fifth &&
                consent.fourth
              }
            >
              <Check />
            </CheckBox>
            <Agreement>전체 이용 약관에 동의합니다.</Agreement>
          </AgreementWrapper>
          <PaymentButton
            disabled={
              !consent.third ||
              !consent.second ||
              !consent.first ||
              !consent.fourth ||
              !consent.fifth
            }
            onClick={() => onSubmit()}
          >
            { confirmText(confirmModalState) }
          </PaymentButton>
        </React.Fragment>
      );
  };

  return (
    <AddCardModalWrapper>
      <PlanTitle>
        { title(confirmModalState) }
      </PlanTitle>
        {render()}
      <CloseButton onClick={() => onClose()}>
        <Close />
      </CloseButton>
    </AddCardModalWrapper>
  );
};

export default ConfirmModal;

const AddCardModalWrapper = styled.div`
  background: white;
  max-width: 640px;
  padding: 32px 35px 40px 40px;
  border-radius: 12px;
  width: 100%;
  position: relative;
  @media (max-width: 630px) {
    ${polyfill.dvh('height', 100)};
    border-radius: 0px;
    padding: 32px 20px 40px;
  }
`;

const ConsentWrapper = styled.div`
  border: 1px solid #eceef7;
  border-radius: 12px;
  padding: 22px 24px 18px 22px;
  margin-bottom: 21px;
`;

const AgreementLink = styled.a`
  color: ${colors.ACTION_BLUE};
  font-weight: 500;
  font-size: 16px;
  line-height: 100%;
  /* identical to box height, or 16px */

  text-decoration-line: underline;
  margin-left: auto;
`;

const ConfirmContentWrapper = styled.div`
  flex: 1;
  max-width: 260px;
  width: 100%;
  background: #f2f7ff;
  border-radius: 12px;
  padding: 23px 34px 18px;
  margin: 0px auto;
  margin-bottom: 40px;
  @media (max-width: 630px) {
    margin-bottom: 12px;
    max-width: 100%;
  }
`;

const ContentPointLabel = styled.p`
  font-weight: 700;
  font-size: 12px;
  line-height: 14px;
  color: ${colors.ACTION_BLUE};
  margin-bottom: 14px;
`;

const ConfirmPlanName = styled.p`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.gray_90};
`;

const ConfirmPlanPrice = styled.p`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  color: ${colors.gray_90};
`;

const ConfirmPlanTitleWrapper = styled(FlexWrapper)`
  justify-content: space-between;
  align-items: flex-start;

  padding-bottom: 13px;
  border-bottom: 1px solid ${colors.GRAY_55};
  margin-bottom: 14px;
`;

const ConfirmPlanCouponWrapper = styled(ConfirmPlanTitleWrapper)`
  padding: 6px 0px 20px;
  gap: 13px;
`;

const ConfirmCouponWrapper = styled(FlexWrapper)`
  justify-content: space-between;
  width: 100%;
`;

const ConfirmDateWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 13px;
`;

const ConfirmDateLabel = styled.p`
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: ${colors.gray_90};
`;

const ConfirmDateValue = styled.div`
  font-weight: 700;
  font-size: 14px;
  line-height: 17px;
  color: ${colors.gray_90};
`;

const ConfirmPlanPriceWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  gap: 3px;
`;

const ConfirmPlanPriceUnit = styled.p`
  font-weight: 700;
  font-size: 12px;
  line-height: 19px;
  color: ${colors.gray_60};
`;

const ConfirmSpacer = styled.div`
  height: 26px;
`;

const ConfirmMultipleWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 8px;
  @media (max-width: 630px) {
    flex-direction: column;
  }
`;

const PlanDescriptionWrapper = styled.div`
  margin-bottom: 25px;
`;

const PlanDescription = styled.li`
  ${typo({ weight: 500, size: "16px", height: "180%", color: colors.GRAY_90 })};
  list-style-position: inside;
  padding-left: 9px;
  @media (max-width: 630px) {
    line-height: 160%;
  }
`;

const PlanDescriptionTitle = styled.div`
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: ${colors.GRAY_70};
  margin-bottom: 12px;
  @media (max-width: 630px) {
    margin-top: 24px;
  }
`;

const ArrowWrapper = styled.div`
  transform: rotate(180deg);
  margin-bottom: 24px;
  svg {
    path {
      fill: ${colors.ACTION_BLUE};
    }
  }
  @media (max-width: 630px) {
    transform: rotate(270deg);
    width: 20px;
    height: 20px;
    margin-bottom: 12px;
    svg {
      width: 100%;
      height: 100%;
    }
  }
`;

const CheckBox = styled.div`
  width: 21px;
  height: 21px;
  cursor: pointer;
  background: ${(props) => (props.consent ? colors.ACTION_BLUE : colors.white)};
  border: 1px solid
    ${(props) => (props.consent ? colors.ACTION_BLUE : colors.BLUE_GRAY_LINE)};
  border-radius: 1px;
  padding: 0px 3.515px;
  svg {
    width: 100%;
    height: 100%;
    path {
      stroke: white !important;
    }
  }
`;

const AgreementWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
  cursor: pointer;
  margin-bottom: 16px;
`;

const Agreement = styled.div`
  font-weight: 500;
  font-size: 16px;
  line-height: 100%;
  /* identical to box height, or 14px */

  /* Gray 7 */

  color: ${colors.GRAY_90};
`;

const AgreementSub = styled(Agreement)`
  font-size: 14px;
  line-height: 100%;
  color: ${colors.GRAY_70};
`;

const CloseButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  position: absolute;
  top: 36px;
  right: 40px;
`;

const PaymentButton = styled.button`
  width: 100%;
  padding: 19px;
  background: ${colors.POINT_PURPLE};
  color: white;
  font-weight: 700;
  font-size: 20px;
  line-height: 100%;
  text-align: center;
  color: ${colors.GRAY_30};
  border-radius: 5px;
  border: none;
  cursor: pointer;

  &:disabled {
    background: ${colors.gray_55};
    color: ${colors.GRAY_30};
    cursor: default;
  }

  @media (max-width: 630px) {
    margin-bottom: 40px;
  }
`;

const PlanTitle = styled.p`
  font-weight: 700;
  font-size: 24px;
  line-height: 29px;
  color: ${colors.GRAY_80};
  margin-bottom: 40px;
`;
