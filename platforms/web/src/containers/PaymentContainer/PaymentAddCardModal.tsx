import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";
import { useSearchParams } from "next/navigation";

import { userState } from "@wrtn/core/stores/login";
import { useTagManager, useTimeCounter } from "@wrtn/core";

import { colors } from "@wrtn/ui/styles";

import { Close } from "@wrtn/ui/assets/SVGComponent/Icon";

const AddCardModal = ({ onSubmit, onClose }) => {
  const user = useRecoilValue(userState);

  const inputsRef = React.useRef<Array<HTMLInputElement | null>>([]);

  const [cardNumber, setCardNumber] = React.useState("");
  const [expiry, setExpiry] = React.useState("");
  const [birth, setBirth] = React.useState("");
  const [password, setPassword] = React.useState("");

  const tagManager = useTagManager();
  const { time, reset } = useTimeCounter("add_card_modal");


  const searchParams = useSearchParams();

  const handleInput = (event, index) => {
    if (event.target.value.length === 4) {
      setCardNumber(
        `${inputsRef.current[0]?.value}-${inputsRef.current[1]?.value}-${inputsRef.current[2]?.value}-${inputsRef.current[3]?.value}`
      );
      if (index < inputsRef.current.length - 1) {
        inputsRef.current[index + 1]?.focus();
      }
    }
  };

  const handleInputLast = (event, index) => {
    if (event.target.value.length > 2) {
      setCardNumber(
        `${inputsRef.current[0]?.value}-${inputsRef.current[1]?.value}-${inputsRef.current[2]?.value}-${inputsRef.current[3]?.value}`
      );
      if (index < inputsRef.current.length - 1) {
        inputsRef.current[index + 1]?.focus();
      }
    }
  };

  const handleExpiryInput = (e) => {
    let expiry = e.target.value;

    if (expiry.length === 2) {
      expiry = expiry + " / ";
    }

    if (expiry.replace(" / ", "").length > 4) {
      e.preventDefault();
    } else {
      setExpiry(expiry);
    }
  };

  return (
    <AddCardModalWrapper>
      <PlanTitle>결제 카드 등록</PlanTitle>
      <PaymentContentWrapper>
        <InputTitle>신용카드</InputTitle>
        <CreditCardInputWrapper>
          <CreditCardInput
            tabIndex={1}
            ref={(el) => (inputsRef.current[0] = el)}
            placeholder="1234"
            maxLength={4}
            type="tel"
            onChange={(e) => handleInput(e, 0)}
          />
          <CreditCardInput
            tabIndex={2}
            ref={(el) => (inputsRef.current[1] = el)}
            placeholder="1234"
            maxLength={4}
            type="tel"
            onChange={(e) => handleInput(e, 1)}
          />
          <CreditCardInput
            tabIndex={3}
            ref={(el) => (inputsRef.current[2] = el)}
            placeholder="1234"
            maxLength={4}
            type="tel"
            onChange={(e) => handleInput(e, 2)}
          />
          <CreditCardInput
            tabIndex={4}
            ref={(el) => (inputsRef.current[3] = el)}
            placeholder="1234"
            maxLength={4}
            type="tel"
            onChange={(e) => handleInputLast(e, 3)}
          />
        </CreditCardInputWrapper>
        <Input
          tabIndex={-1}
          type="email"
          style={{ position: "absolute", top: -9999999, left: -9999999 }}
        />
        <Input
          tabIndex={-1}
          type="password"
          style={{ position: "absolute", top: -9999999, left: -9999999 }}
        />
        <MobileGrid>
          <InputWrapper>
            <InputTitle>유효 기간</InputTitle>
            <Input
              tabIndex={5}
              placeholder="MM / YY"
              value={expiry}
              onChange={handleExpiryInput}
            />
          </InputWrapper>
          <Grid>
            <InputWrapper>
              <InputTitle>비밀번호 앞 2자리</InputTitle>
              <Input
                tabIndex={6}
                placeholder="12"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type="password"
              />
            </InputWrapper>
            <InputWrapper>
              <InputTitle>&nbsp;</InputTitle>
              <Password>● ●</Password>
            </InputWrapper>
          </Grid>
        </MobileGrid>
        <InputWrapper>
          <InputTitle>생년월일 또는 사업자등록번호</InputTitle>
          <Input
            tabIndex={7}
            placeholder="생년월일 (6자리) / 사업자등록번호"
            value={birth}
            onChange={(e) => setBirth(e.target.value)}
          />
          <HintCaptionWrapper>
            <HintCaption>
              법인카드의 경우 사업자등록번호 10자리를 입력해주세요.
            </HintCaption>
          </HintCaptionWrapper>
        </InputWrapper>
        <InputWrapper>
          <InputTitle>이메일 주소</InputTitle>
          <Input defaultValue={user?.email} readOnly />
          <HintCaptionWrapper>
            <HintCaption>
              가입된 계정의 이메일로 청구서를 보내드립니다.
            </HintCaption>
          </HintCaptionWrapper>
        </InputWrapper>
      </PaymentContentWrapper>
      <PaymentButton
        tabIndex={4}
        disabled={!cardNumber || !expiry || !birth || !password}
        onClick={async () => {
          let is_first_payment = false;

          tagManager({
            event: "register_card",
            data: {
              action_msec: time(),
              is_upgrade: searchParams.get("type") === "changePlan" ? false : true,
              is_first_payment: is_first_payment,
              plan_category: searchParams.get("plan"),
            },
          });
          onSubmit({
            cardNumber,
            expiry,
            birth,
            password,
          });
        }}
      >
        등록하기
      </PaymentButton>
      <CloseButton onClick={() => onClose()}>
        <Close />
      </CloseButton>
    </AddCardModalWrapper>
  );
};

export default AddCardModal;

const AddCardModalWrapper = styled.div`
  background: white;
  max-width: 640px;
  padding: 32px 35px 40px 40px;
  border-radius: 12px;
  width: 100%;
  position: relative;
  @media (max-width: 630px) {
    padding: 32px 16px 40px;
  }
`;

const PlanTitle = styled.p`
  font-weight: 700;
  font-size: 24px;
  line-height: 29px;
  color: ${colors.GRAY_80};
  margin-bottom: 40px;
`;

const Input = styled.input`
  border: 1px solid ${colors.GRAY_55};
  border-radius: 5px;
  width: 100%;
  padding: 8px 16px;
  font-weight: 500;
  font-size: 16px;
  line-height: 26px;

  color: ${colors.GRAY_90};
  &::placeholder {
    color: ${colors.GRAY_55};
  }

  &:read-only {
    background: ${colors.GRAY_10};
    border: 1px solid ${colors.GRAY_30};
    color: ${colors.GRAY_60};
  }
`;

const InputWrapper = styled.div`
  margin-bottom: 34px;
  @media (max-width: 720px) {
    margin-bottom: 15px;
  }
`;

const InputTitle = styled.div`
  font-weight: 600;
  font-size: 18px;
  line-height: 100%;
  color: ${colors.GRAY_80};
  margin-bottom: 14px;
`;

const HintCaption = styled.p`
  font-weight: 500;
  font-size: 14px;
  line-height: 100%;
  /* identical to box height, or 14px */

  color: ${colors.GRAY_60};
`;

const HintCaptionWrapper = styled.div`
  margin: 10px 0px;
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 8px;
`;

const PaymentContentWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const PaymentButton = styled.button`
  width: 100%;
  padding: 19px;
  background: ${colors.POINT_PURPLE};
  color: white;
  font-weight: 700;
  font-size: 20px;
  line-height: 100%;
  text-align: center;
  color: ${colors.GRAY_30};
  border-radius: 5px;
  border: none;
  cursor: pointer;

  &:disabled {
    background: ${colors.gray_55};
    color: ${colors.GRAY_30};
    cursor: default;
  }
`;

const CreditCardInputWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  gap: 8px;
  margin-bottom: 34px;
  @media (max-width: 630px) {
    margin-bottom: 15px;
  }
`;

const CreditCardInput = styled.input`
  border: 1px solid ${colors.GRAY_55};
  border-radius: 5px;
  width: 100%;
  padding: 8px 16px;
  font-weight: 500;
  font-size: 16px;
  line-height: 26px;

  color: ${colors.GRAY_90};
  &::placeholder {
    color: ${colors.GRAY_55};
  }
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }
`;

const CloseButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  position: absolute;
  top: 36px;
  right: 40px;
  @media (max-width: 630px) {
    right: 20px;
  }
`;

const Password = styled.div`
  height: 42px;
  align-self: center;
  color: #83879d;
  font-size: 12px;
  padding: 9px 2px;
  line-height: 26px;
`;

const MobileGrid = styled(Grid)`
  @media (max-width: 630px) {
    grid-template-columns: 1fr;
  }
`;
