import styled from "styled-components";

import { colors } from "@wrtn/ui/styles";

import { IconCheckCircle } from "@wrtn/ui/assets/SVGComponent/Icon";

const PlanItem = ({ children }) => {
  return (
    <PlanItemWrapper>
      <PlanItemIcon>
        <IconCheckCircle />
      </PlanItemIcon>
      <PlanItemText>{children}</PlanItemText>
    </PlanItemWrapper>
  );
};

export default PlanItem;

const PlanItemIcon = styled.div`
  background: ${colors.POINT_PURPLE};
  border-radius: 50%;
  width: 14px;
  height: 14px;
  svg {
    width: 100%;
    height: 100%;
    path {
      stroke: #fff;
    }
    circle {
      stroke-width: 0;
    }
  }
`;

const PlanItemWrapper = styled.div`
  margin-bottom: 19px;
  display: flex;
  gap: 7px;
  align-items: center;
`;

const PlanItemText = styled.p`
  font-weight: 700;
  font-size: 14px;
  line-height: 17px;
  /* identical to box height */

  /* Gray 8 */

  color: ${colors.GRAY_80};
`;
