import React from "react";
import { useAlert } from "react-alert";
import { useRecoilValue } from "recoil";
import creditCardType from "credit-card-type";
import styled, { css } from "styled-components";

import AddCardModal from "./PaymentAddCardModal";
import ConfirmModal from "./PaymentConfirmModal";
import PlanItem from "./PaymentPlanItem";

import { Dialog } from "@wrtn/ui/components/Dialog";
import { CouponDropdown } from "@wrtn/ui/components/Select";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { DefaultSpinner } from "@wrtn/ui/components/DefaultSpinner";
import dayjs from "dayjs";

import {
  couponListState,
  PLAN_COLOR,
  PLAN_NAME,
  PLAN_PRICE,
  useEvent,
  useRefresh,
  useTagManager,
  useTimeCounter,
} from "@wrtn/core";
import {
  deleteBillingCard_CardId,
  postBillingCard,
  postBillingPayment,
  putBillingCard_CardId,
  putCouponCode,
} from "@wrtn/core/services/";
import { userState } from "@wrtn/core/stores/login";
import { getUserCard } from "@wrtn/core/services";

import { isUpperPlan } from "../../utils/isUpperPlan";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

import { ReactComponent as Check } from "@wrtn/ui/assets/icons/icon_check.svg";
import { Arrow, ArrowShort, TrashCan } from "@wrtn/ui/assets/SVGComponent/Icon";
import polyfill from "@wrtn/ui/styles/polyfill";
import { useRouter } from "next/router";

const PLAN_INFO = {
  PLUS: {
    price: {
      original: 44900,
      discount: 20,
      final: 34900,
    },
    amount: "inf",
    benefit: [
      "무제한 생성",
      "더 빠른 속도로 생성 가능",
      "트래픽 과중 시 우선 접근 보장",
      "1회 생성 결과 수 최대 3개",
      "생성 이력 관리 기한 최대 30일",
      "온라인 CS 우선권",
      "생성 이력 다운로드 기능",
      "결과물 수정 기능",
    ],
  },
  FREE: {
    price: {
      final: 0,
    },
    amount: "inf",
    benefit: ["무제한 생성"],
  },
};

export const getCurrentPrice = (percent) => {
  const currentPrice = PLAN_PRICE[`PLUS_DISCOUNT_${percent}_PERCENT`];
  const couponPrice = PLAN_PRICE.PLUS - currentPrice;

  return {
    currentPrice,
    couponPrice,
  };
};

const getLogo = (type) => {
  if (type === "visa") {
    return require("@wrtn/ui/assets/logo/logo_credit_visa.svg");
  } else if (type === "unionpay") {
    return require("@wrtn/ui/assets/logo/logo_credit_unionpay.svg");
  } else if (type === "mastercard") {
    return require("@wrtn/ui/assets/logo/logo_credit_master.svg");
  } else if (type === "jcb") {
    return require("@wrtn/ui/assets/logo/logo_credit_jcb.svg");
  } else if (type === "american-express") {
    return require("@wrtn/ui/assets/logo/logo_credit_amex.svg");
  } else {
    return null;
  }
};

const getTitle = (type, currentPlan, plan) => {
  if (type === "changeCard") {
    return "결제 카드 변경하기";
  } else {
    if (isUpperPlan(currentPlan, plan)) {
      return "결제하기";
    } else {
      return "요금제 변경하기";
    }
  }
};

const PaymentContainer = ({ plan, type }) => {
  const [openError, setOpenError] = React.useState(false);
  const [isAddCard, setIsAddCard] = React.useState(false);
  const [isConfirm, setIsConfirm] = React.useState(false);
  const [cardList, setCardList] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [currentDeleteCard, setCurrentDeleteCard] = React.useState(null);
  const [currentCoupon, setCurrentCoupon] = React.useState(null);
  const [usingCoupon, setUsingCoupon] = React.useState(null);
  const [isUsingCoupon, setIsUsingCoupon] = React.useState(false);
  const [isAlreadyApplyCoupon, setIsAlreadyApplyCoupon] = React.useState(false);

  const user = useRecoilValue(userState);
  const couponList = useRecoilValue(couponListState);

  const router = useRouter();
  const tagManager = useTagManager();
  const timeCounter = useTimeCounter();

  const {
    collectUserProperties_increment,
    collectUserProperties_once,
    collectUserProperties,
    collectEvent,
  } = useEvent();

  const confirmModalState = isUpperPlan(user?.plan, plan)
    ? "결제"
    : currentCoupon
    ? "쿠폰적용"
    : "변경";

  const alert = useAlert();
  const { fetchCoupon } = useRefresh();

  const filteredCouponList = React.useMemo(() => {
    if (couponList)
      return couponList.filter(
        (e) =>
          (e.status === "available" || e.status === "using") &&
          e.coupon.type === "discount"
      );
    else return [];
  }, [couponList]);

  React.useEffect(() => {
    const usingCoupon = filteredCouponList.find((v) => v.status === "using");
    setUsingCoupon(usingCoupon);
    if (!currentCoupon) setCurrentCoupon(usingCoupon);
  }, [filteredCouponList]);

  const fetchUserCard = async () => {
    const res = await getUserCard();
    if (res?.status === 200) {
      const { data } = res.data;
      if (Array.isArray) {
        setCardList(data);
      }
      if (Array.isArray(data) && data.length === 0) {
        setIsAddCard(true);
      }
    }
  };

  const handleSubmitPayment = async () => {
    try {
      if (!loading) {
        setLoading(true);

        const currentCard = cardList?.find((e) => e.isSelected === true);
        if (isUsingCoupon && currentCard && confirmModalState === "쿠폰적용") {
          const res = await putCouponCode({
            couponId: currentCoupon.coupon._id,
            force: false,
          });

          if (res.status === 200) {
            router.push(
              `/setting/billing/success?plan=${plan}&type=${"coupon"}`
            );
          } else if (res.status === 409) {
            setIsAlreadyApplyCoupon(true);
            setIsConfirm(false);
            return;
          } else {
            alert.removeAll();
            alert.show(res?.data?.message || res?.message);
            setIsConfirm(false);
          }
        }
        if (currentCard) {
          const res = await postBillingPayment(
            isUsingCoupon
              ? {
                  cardId: currentCard._id,
                  plan: plan,
                  couponId: currentCoupon.coupon._id,
                }
              : {
                  cardId: currentCard._id,
                  plan: plan,
                }
          );
          if (res?.status === 201) {
            collectUserProperties_increment("purchase_count_accum", 1);
            collectUserProperties_increment(
              "purchase_amount_accum",
              PLAN_INFO[plan]?.price?.final
            );
            collectUserProperties_once(
              "first_purchase_period",
              parseInt(
                new Date().getTime() - new Date(user?.agreementDate).getTime()
              ) /
                (1000 * 3600 * 24)
            );
            collectUserProperties_once("first_purchase_date", new Date());
            collectUserProperties("last_purchase_date", new Date());
            collectEvent("purchase_done", {
              current_plan: user.plan,
              change_plan: plan,
              is_auto: false,
              payment_amount: PLAN_INFO[plan].price.final,
              left_amount_paid: user.leftPaidWordCount,
              left_amount_bonus: user.leftFreeWordCount,
              is_coupon: isUsingCoupon,
              coupon_name: currentCoupon?.coupon.name,
              coupon_discount_amount: isUsingCoupon
                ? currentCoupon.coupon.discount
                : 0,
            });
            if (user.nextPlan === "FREE" && user.plan !== "FREE") {
              collectEvent("repurchase_done", {
                current_plan: user.plan,
                change_plan: plan,
              });
            }

            setTimeout(() => {
              router.push(`/setting/billing/success?plan=${plan}&type=${type}`);
            }, 1000);
          } else {
            alert.removeAll();
            alert.show(res?.data?.message || res?.message);
          }
        }
      }
      setLoading(false);
    } catch (e) {
    } finally {
      setLoading(false);
    }
  };

  const handleSubmitCard = async ({ cardNumber, expiry, birth, password }) => {
    if (!loading) {
      setLoading(true);

      const res = await postBillingCard({
        name: user?.email,
        cardNumber: cardNumber.replaceAll("-", ""),
        cardExpirationYear: expiry.split(" / ")[1],
        cardExpirationMonth: expiry.split(" / ")[0],
        customerIdentityNumber: birth,
        cardPassword: password,
      });
      if (res?.status === 201) {
        collectEvent("submit_register_card");
        setIsAddCard(false);
        fetchUserCard();
      } else {
        setOpenError(res?.data?.message || res?.message);
      }
      setLoading(false);
    }
  };

  const selectCard = async (cardId) => {
    if (!loading) {
      const res = await putBillingCard_CardId({
        cardId: cardId,
      });
      if (res?.status === 200) {
        fetchUserCard();
      }
    }
    setLoading(false);
  };

  const deleteCard = async () => {
    if (!loading) {
      const res = await deleteBillingCard_CardId({
        cardId: currentDeleteCard._id,
      });
      if (res?.status === 200) {
        fetchUserCard();
        setCurrentDeleteCard(null);
      }
    }
    setLoading(false);
  };

  const handleApplyCouponForce = async () => {
    if (!loading) {
      const res = await putCouponCode({
        couponId: currentCoupon.coupon._id,
        force: true,
      });

      if (res?.status === 200) {
        router.push(`/setting/billing/success?plan=${plan}&type=${"coupon"}`);
      } else {
        alert.removeAll();
        alert.show("결제에 실패했습니다.");
      }
    }
  };

  const handleSelectCoupon = (coupon) => {
    if (coupon === "none") {
      setIsUsingCoupon(false);
      setCurrentCoupon(null);
    } else {
      setIsUsingCoupon(true);
      setCurrentCoupon(coupon);
    }
  };

  React.useEffect(() => {
    fetchCoupon();
    fetchUserCard();
  }, []);

  const canApplyCoupon = !(type === "changeCard" && user?.nextPlan === "FREE");

  if (!user) return <></>;

  return (
    <OuterWrapper>
      <Wrapper>
        <LeftWrapper>
          <SafeWrapper style={{ paddingBottom: 50 }}>
            <PlanTitle>선택한 요금제</PlanTitle>
            <PlanCard>
              <PriceCardLabel color={PLAN_COLOR[plan]} />
              <PlanInnerWrapper>
                <PlanTitleWrapper>
                  <PlanTitle>{PLAN_NAME[plan]}</PlanTitle>
                </PlanTitleWrapper>
                <PlanPriceWrapper>
                  {PLAN_INFO[plan]?.price?.original && (
                    <PlanOriginalPriceWrapper>
                      <PlanOriginalPrice>
                        {Number(
                          PLAN_INFO[plan]?.price?.original
                        ).toLocaleString()}
                        원
                      </PlanOriginalPrice>
                      <PlanDiscount>
                        {PLAN_INFO[plan]?.price?.discount}% <DiscountArrow />
                      </PlanDiscount>
                    </PlanOriginalPriceWrapper>
                  )}
                  <PlanFinalPriceWrapper>
                    <PlanPrice type={plan}>
                      {PLAN_INFO[plan]?.price?.final?.toLocaleString()}원
                    </PlanPrice>
                    <PricePer>/월</PricePer>
                  </PlanFinalPriceWrapper>
                </PlanPriceWrapper>
                <PlanItemContentWrapper>
                  {PLAN_INFO[plan]?.benefit?.map((item, idx) => {
                    return <PlanItem key={idx}>{item}</PlanItem>;
                  })}
                </PlanItemContentWrapper>
              </PlanInnerWrapper>
            </PlanCard>
            <RefundTitle>뤼튼 환불 규정</RefundTitle>
            <RefundTextWrapper>
              <RefundText>
                요금제 결제 후 7일 이내 사용 이력이 없을 시 환불 가능합니다.
              </RefundText>
              <RefundText>
                환불 문의는 서비스 내 설정 → 결제 정보 → 환불 문의를 통해 접수할
                수 있습니다.
              </RefundText>
            </RefundTextWrapper>
          </SafeWrapper>
        </LeftWrapper>
        <RightWrapper>
          <SafeWrapper style={{ paddingTop: 50 }}>
            <PaymentTitle>{getTitle(type, user.plan, plan)}</PaymentTitle>
            <PaymentContentWrapper>
              {canApplyCoupon && (
                <>
                  <PaymentResultTitle>
                    <InputTitle>쿠폰 등록</InputTitle>
                  </PaymentResultTitle>
                  <PaymentCardsListWrapper>
                    <CouponDropdown
                      list={filteredCouponList}
                      value={currentCoupon}
                      setValue={handleSelectCoupon}
                    />
                  </PaymentCardsListWrapper>
                </>
              )}
              <PaymentResultTitle>
                <InputTitle>결제 카드</InputTitle>
              </PaymentResultTitle>
              <PaymentCardsListWrapper>
                {cardList.map((card) => {
                  const type = creditCardType(
                    card?.info?.number?.slice(0, 4)
                  )[0]?.type;
                  return (
                    <PaymentCardOuterWrapper>
                      <PaymentResultCard
                        selected={card.isSelected}
                        onClick={() => selectCard(card._id)}
                      >
                        <PaymentResultNumber>
                          {card.info.number
                            .split("")
                            .map((e, idx) => (idx % 4 === 3 ? e + " " : e))}
                          <CreditCardWrapper>
                            <CreditLogo src={getLogo(type)?.default} />
                          </CreditCardWrapper>
                        </PaymentResultNumber>
                        {card.isSelected && (
                          <PaymentSelectText>
                            <Check /> 선택됨
                          </PaymentSelectText>
                        )}
                      </PaymentResultCard>
                      {cardList.length >= 1 && user.nextPlan !== "PLUS" ? (
                        <DeleteButton
                          onClick={() => setCurrentDeleteCard(card)}
                        >
                          <TrashCan />
                        </DeleteButton>
                      ) : (
                        <DeleteButton />
                      )}
                    </PaymentCardOuterWrapper>
                  );
                })}
                <PaymentCardOuterWrapper>
                  <PaymentResultCard onClick={() => setIsAddCard(true)}>
                    + 새로운 결제 카드 추가
                  </PaymentResultCard>
                  <DeleteButton />
                </PaymentCardOuterWrapper>
              </PaymentCardsListWrapper>
              <PaymentResultTitle>
                <InputTitle>결제 내용</InputTitle>
              </PaymentResultTitle>
              <PaymentCardsListWrapper>
                <PayAmountWrapper>
                  <AmountTitle>총 금액</AmountTitle>
                  <Amount>
                    {Number(PLAN_INFO[plan]?.price?.final).toLocaleString()}원
                  </Amount>
                </PayAmountWrapper>
                <Divider />
                {isUsingCoupon && (
                  <>
                    <PayAmountWrapper>
                      <AmountTitle>쿠폰 사용</AmountTitle>
                      <Amount>
                        -
                        {Number(
                          getCurrentPrice(currentCoupon.coupon.percent)
                            .couponPrice
                        ).toLocaleString()}
                        원
                      </Amount>
                    </PayAmountWrapper>
                    <Divider />
                  </>
                )}
                {plan !== "FREE" && (
                  <PayAmountWrapper>
                    <AmountTitle>결제 예정 금액</AmountTitle>
                    <Amount style={{ color: colors.POINT_PURPLE }}>
                      {currentCoupon
                        ? Number(
                            getCurrentPrice(currentCoupon.coupon.percent)
                              .currentPrice
                          ).toLocaleString()
                        : Number(PLAN_INFO[plan].price.final).toLocaleString()}
                      원
                    </Amount>
                  </PayAmountWrapper>
                )}
                <Divider />
                {currentCoupon && plan !== "FREE" && (
                  <CouponResultWrapper>
                    <AmountTitle>할인 적용 기간 및 결제 예정 금액</AmountTitle>
                    <CouponResultDataWrapper>
                      {/* { currentCoupon } */}
                      <CouponResultData>
                        {user?.dueDate
                          ? dayjs(user.dueDate).format("YYYY년 MM월 DD일") +
                            " ~ " +
                            dayjs(user.dueDate)
                              .add(currentCoupon.coupon.count, "month")
                              .subtract(1, "day")
                              .format("YYYY년 MM월 DD일")
                          : dayjs().format("YYYY년 MM월 DD일") +
                            " ~ " +
                            dayjs()
                              .add(currentCoupon.coupon.count, "month")
                              .subtract(1, "day")
                              .format("YYYY년 MM월 DD일")}
                      </CouponResultData>
                      <CouponResultData>
                        할인가 적용{" "}
                        <CouponPrice>
                          {Number(
                            getCurrentPrice(currentCoupon.coupon.percent)
                              .currentPrice
                          ).toLocaleString()}
                          원
                        </CouponPrice>
                      </CouponResultData>
                    </CouponResultDataWrapper>
                    <CouponResultDataWrapper>
                      {/* { currentCoupon } */}
                      <CouponResultData>
                        {user?.dueDate
                          ? dayjs(user.dueDate)
                              .add(currentCoupon.coupon.count, "month")
                              .format("YYYY년 MM월 DD일")
                          : dayjs()
                              .add(currentCoupon.coupon.count, "month")
                              .format("YYYY년 MM월 DD일")}
                        부터
                      </CouponResultData>
                      <CouponResultData>
                        정상가 적용{" "}
                        <CouponPrice>
                          {Number(PLAN_INFO[plan].price.final).toLocaleString()}
                          원
                        </CouponPrice>
                      </CouponResultData>
                    </CouponResultDataWrapper>
                  </CouponResultWrapper>
                )}
              </PaymentCardsListWrapper>
              <BottomWrapper>
                <PaymentButtonWrapper>
                  <PaymentButton
                    disabled={
                      cardList.findIndex((e) => e.isSelected === true) === -1
                    }
                    onClick={async () => {
                      if (plan === "FREE") {
                        return router.push(
                          `/setting/billing/success?plan=${plan}&type=${type}`
                        );
                      }
                      if (type === "changeCard") {
                        if (!currentCoupon)
                          return router.push(
                            `/setting/billing/success?plan=${plan}&type=${type}`
                          );
                        setIsConfirm(true);
                      } else {
                        // const res = await getUsageCurrent();
                        let is_first_payment = false;
                        // if (res?.status === 200) {
                        //   const { data } = res.data;
                        //   if (
                        //     Array.isArray(data) &&
                        //     data.filter((e) => e.isPaid).length === 0
                        //   ) {
                        //     is_first_payment = true;
                        //   }
                        // }
                        tagManager({
                          event: "payment_initial",
                          data: {
                            action_msec: timeCounter.time(),
                            is_upgrade: type === "changePlan" ? false : true,
                            is_first_payment: is_first_payment,
                            plan_category: plan,
                          },
                        });
                        collectEvent("click_purchase_btn", {
                          current_plan: user?.plan,
                          change_plan: plan,
                          is_upgrade: type === "changePlan" ? false : true,
                        });
                        setIsConfirm(true);
                      }
                    }}
                  >
                    {!isUpperPlan(user?.plan, plan) ? "변경하기" : "결제하기"}
                  </PaymentButton>
                  <DeleteButton />
                </PaymentButtonWrapper>
              </BottomWrapper>
            </PaymentContentWrapper>
          </SafeWrapper>
        </RightWrapper>
        <BackButtonWrapper onClick={() => window.history.back()}>
          <Arrow />
          <BackLabel>뤼튼</BackLabel>
        </BackButtonWrapper>
        {isAddCard && (
          <ModalPortal onClose={() => setIsAddCard(false)}>
            <AddCardModal
              onSubmit={handleSubmitCard}
              onClose={() => setIsAddCard(false)}
            />
          </ModalPortal>
        )}
        {isConfirm && (
          <ModalPortal onClose={() => setIsConfirm(false)}>
            <ConfirmModal
              onSubmit={handleSubmitPayment}
              onClose={() => setIsConfirm(false)}
              plan={plan}
              type={type}
              currentCoupon={currentCoupon}
            />
          </ModalPortal>
        )}
        {openError && (
          <ModalPortal>
            <Dialog
              title="카드 정보가 일치하지 않아요"
              description={[openError]}
              handleClose={() => setOpenError(false)}
              handleRightButton={() => setOpenError(false)}
              disableCancel
              rightButtonLabel="확인"
            />
          </ModalPortal>
        )}
        {loading && (
          <ModalPortal>
            <FlexWrapper1>
              <DefaultSpinner />
              <Text>잠시만 기다려주세요.</Text>
            </FlexWrapper1>
          </ModalPortal>
        )}
        {currentDeleteCard && (
          <ModalPortal>
            <Dialog
              iconType="creditCard"
              title="이 결제 카드를 삭제하시겠어요?"
              description={["카드가 삭제되면 복구할 수 없습니다."]}
              handleClose={() => setCurrentDeleteCard(null)}
              handleRightButton={() => {
                deleteCard();
              }}
              rightButtonLabel="확인"
            />
          </ModalPortal>
        )}
        {isAlreadyApplyCoupon && (
          <ModalPortal>
            <Dialog
              title="이미 다음 달에 적용될 쿠폰이 등록되어있어요."
              handleClose={() => setIsAlreadyApplyCoupon(false)}
              handleRightButton={() => {
                handleApplyCouponForce();
              }}
              rightButtonLabel="새 쿠폰 등록"
              children={
                <FlexWrapper column style={{ gap: "22px" }}>
                  <BoldTypo>{"<" + usingCoupon.coupon.name + ">"}</BoldTypo>
                  <FlexWrapper column>
                    <BoldTypo>적용 기간</BoldTypo>
                    <Typo>
                      {dayjs(user.dueDate)
                        .add(1, "day")
                        .format("YYYY년 MM월 DD일") +
                        " ~ " +
                        dayjs(user.dueDate)
                          .add(1, "day")
                          .add(currentCoupon.coupon.count, "month")
                          .format("YYYY년 MM월 DD일")}
                    </Typo>
                  </FlexWrapper>
                  <Typo style={{ textAlign: "center" }}>
                    새 쿠폰을 적용하면 다음 달 결제일부터 기존 쿠폰 혜택이
                    종료됩니다.
                  </Typo>
                </FlexWrapper>
              }
            />
          </ModalPortal>
        )}
      </Wrapper>
    </OuterWrapper>
  );
};

export default PaymentContainer;

const OuterWrapper = styled.div``;

const Wrapper = styled.div`
  width: 100vw;
  ${polyfill.dvh("height", 100)};
  background: white;
`;

const LeftWrapper = styled.div`
  flex: 1 0 720px;
  flex-wrap: nowrap;
  background: ${colors.BACKGROUND};
  justify-content: center;
`;

const RightWrapper = styled.div`
  flex: 1 0 720px;
`;

const SafeWrapper = styled.div`
  margin: 0px auto;
  padding: 148px 20px 100px;
  display: flex;
  flex-direction: column;
  max-width: 630px;
  width: 100%;
  height: 100%;
`;

const PlanTitle = styled.p`
  font-weight: 700;
  font-size: 24px;
  line-height: 29px;
  color: ${colors.GRAY_80};
  margin-bottom: 40px;
`;

const PlanPriceWrapper = styled.div`
  border-bottom: 1px solid ${colors.GRAY_60};
  width: 100%;
`;

const PlanItemContentWrapper = styled.div`
  padding-bottom: 34px;
  padding-top: 28px;
`;

const PaymentTitle = styled.p`
  font-weight: 700;
  font-size: 24px;
  line-height: 29px;

  color: ${colors.GRAY_90};
  margin-bottom: 40px;
`;

const InputTitle = styled.div`
  font-weight: 600;
  font-size: 18px;
  line-height: 100%;
  color: ${colors.GRAY_80};
  margin-bottom: 14px;
`;

const PaymentContentWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const PaymentButton = styled.button`
  width: 100%;
  padding: 19px;
  background: ${colors.POINT_PURPLE};
  color: white;
  font-weight: 700;
  font-size: 20px;
  line-height: 100%;
  text-align: center;
  color: ${colors.GRAY_30};
  border-radius: 5px;
  border: none;
  cursor: pointer;

  &:disabled {
    background: ${colors.gray_55};
    color: ${colors.GRAY_30};
    cursor: default;
  }
`;

const BottomWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: flex-start;
  gap: 20px;
  width: 100%;
`;

const PaymentResultTitle = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  margin-bottom: 20px;
  width: 100%;
  > div {
    margin-bottom: 0px;
  }
`;

const PaymentResultCard = styled.div`
  border-radius: 5px;
  width: 100%;
  padding: 12px 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: ${colors.GRAY_60};
  border: 1px solid ${colors.GRAY_55};
  cursor: pointer;
  img {
    filter: grayscale(1);
  }
  ${({ selected }) =>
    selected &&
    css`
      border-bottom: 1px solid #c4c9d7;
      color: #3f3f3f;
      img {
        filter: grayscale(0);
      }
    `};
`;

const PaymentResultNumber = styled.p`
  font-weight: 700;
  font-size: 16px;
  line-height: 100%;
`;

const PaymentSelectText = styled.p`
  font-weight: 500;
  font-size: 14px;
  line-height: 100%;
  color: #08b539;
  svg {
    height: 9px;
  }
`;

const FlexWrapper1 = styled(FlexWrapper)`
  flex-direction: column;
  gap: 10px;
`;

const Text = styled.div`
  color: white;
  font-size: 14px;
  font-weight: 700;
`;

const CreditLogo = styled.img`
  height: 12px;
`;

const CreditCardWrapper = styled.div`
  display: inline-block;
  margin-left: 13px;
`;

const BackButtonWrapper = styled.div`
  position: absolute;
  top: 55px;
  left: 50px;
  display: flex;
  align-items: center;
  gap: 10px;
  color: ${colors.GRAY_90};
  cursor: pointer;
  svg {
    width: 30px;
    height: 30px;
    fill {
      color: ${colors.GRAY_90};
    }
  }
  @media (max-width: 720px) {
    left: 30px;
  }
`;

const BackLabel = styled.p`
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
`;

const PlanOriginalPriceWrapper = styled.div`
  display: flex;
  gap: 5px;
  align-items: center;
  margin-bottom: 6px;
`;

const PlanOriginalPrice = styled.div`
  ${typo({
    weight: 700,
    size: "16px",
    height: "100%",
    color: colors.gray_60,
  })};
  text-decoration: line-through;
`;

const PlanDiscount = styled.div`
  ${typo({
    weight: 700,
    size: "14px",
    height: "100%",
    color: colors.white,
  })};
  background-color: #fb533d;
  border-radius: 10px;
  padding: 3px 20px 3px 7px;
  position: relative;
  svg {
    position: absolute;
    top: 0;
    right: 3px;
  }
`;

const PlanPrice = styled.div`
  ${typo({
    weight: 700,
    size: "48px",
    height: "100%",
    color: colors.gray_80,
  })}
  color: ${(props) =>
    props.type === "Pro" || props.type === "Business"
      ? "#5A2CDA"
      : colors.gray_90};
`;

const PlanCard = styled.div`
  background: white;
  border-radius: 12px;
  overflow: hidden;
  width: 100%;
  margin-bottom: 30px;
`;

const PlanFinalPriceWrapper = styled.div`
  display: flex;
  gap: 7px;
  align-items: flex-end;
  margin-bottom: 23px;
`;

const PricePer = styled.span`
  ${typo({
    weight: 600,
    size: "20px",
    height: "152.52%",
    color: colors.gray_55,
  })}
`;

const PriceCardLabel = styled.div`
  height: 12px;
  width: 100%;
  background-color: ${({ color }) => color};
`;

const PlanInnerWrapper = styled.div`
  max-width: 462px;
  margin: 0px auto;
  padding: 20px 30px;
`;

const PlanTitleWrapper = styled.div`
  margin-bottom: 27px;
  margin-top: 60px;
  @media (max-width: 540px) {
    margin-top: 30px;
    margin-bottom: 0px;
  }
`;

const PaymentCardsListWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  margin-bottom: 48px;
`;

const PaymentCardOuterWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 16px;
  margin-bottom: 14px;
  width: 100%;
  position: relative;
`;

const DeleteButton = styled.button`
  position: absolute;

  top: 11px;
  right: -40px;

  width: 24px;
  height: 24px;
  padding: 0;
  margin: 0;
  background: none;
  border: none;
  cursor: pointer;
  flex: 0 0 24px;
  svg {
    width: 80%;
    height: 80%;
    path {
      fill: ${colors.GRAY_50};
    }
  }
`;

const DiscountArrow = styled(ArrowShort)``;

const RefundTitle = styled.div`
  ${typo({ weight: 600, color: colors.GRAY_60 })};
  margin-bottom: 13px;
`;

const RefundTextWrapper = styled.ul`
  ${typo({ weight: 600, color: colors.GRAY_80, height: "160%" })};
  margin-bottom: 15px;
`;

const RefundText = styled.li`
  list-style-position: inside;
`;

const PaymentButtonWrapper = styled(FlexWrapper)`
  width: 100%;
`;

const PayAmountWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: space-between;
  padding: 18px 0px;
`;

const AmountTitle = styled.p`
  ${typo({ weight: 600, size: "16px", color: "#3F3F3F" })};
`;

const Amount = styled.p`
  ${typo({ weight: 700, size: "18px", color: "#3F3F3F" })};
`;

const Divider = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${colors.GRAY_55};
`;

const CouponResultWrapper = styled(FlexWrapper)`
  flex-direction: column;
  width: 100%;
  align-items: flex-start;
  gap: 20px;
  margin-top: 20px;
`;

const CouponResultData = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    color: colors.gray_60,
  })};
`;

const CouponResultDataWrapper = styled(FlexWrapper)`
  ${"" /* flex-direction: column; */}
  justify-content: space-between;
  width: 100%;
`;

const CouponPrice = styled.a`
  color: ${colors.gray_80};
`;

const BoldTypo = styled.div`
  ${typo({
    weight: 700,
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })}
`;

const Typo = styled.div`
  ${typo({
    weight: 500,
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })}
`;
