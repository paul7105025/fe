import { StoreChatBotType, StoreToolType } from "@wrtn/core";
import { colors, FlexWrapper, newColors, newTypo, STORE_ICON_SELECTOR, TOOL_ICON_SELECTOR } from "@wrtn/ui";
import styled from 'styled-components';

interface ChatbotDetailFormInfoProps {
  chatbot?: StoreChatBotType;
}

export const ChatbotDetailFormInfo = ({ chatbot }: ChatbotDetailFormInfoProps) => {
  return (
    <Wrapper column align="flex-start">
      <FlexWrapper style={{gap: '13px', marginBottom: '19px'}} align="center">
        { chatbot && STORE_ICON_SELECTOR[chatbot.icon]}
        <Title>{chatbot?.title}</Title>
      </FlexWrapper>
      <Desc>
        {chatbot?.description}
      </Desc>
      <CategoryWrapper>
        {
          chatbot?.category.map(v => (
            <Category key={v}>
              {v}
            </Category>
          ))
        }
      </CategoryWrapper>
    </Wrapper>
  )
}

export default ChatbotDetailFormInfo;

const Desc = styled.div`
  ${newTypo("body-16-med")};
  color: ${newColors.GRAY_700};
  padding-bottom: 20px;
`;

const CategoryWrapper = styled(FlexWrapper)`
  gap: 8px;
  padding-bottom: 29px;
`;

const Category = styled.div`
  color: ${colors.gray_70};
  ${newTypo("content-14-med")};
  padding: 5px 9px;
  border: 1px solid ${newColors.GRAY_300};
  background: ${newColors.WHITE};
  border-radius: 5px;
`;

const Title = styled.div`
  ${newTypo("heading-22-semi")};
  color: ${newColors.GRAY_800};
`;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
`;