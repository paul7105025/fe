import { StoreChatBotType } from "@wrtn/core";
import { FlexWrapper } from "@wrtn/ui";
import styled from 'styled-components';
import { ChatbotDetailContent } from "./ChatbotDetailContent";
import { ChatbotDetailMenu } from "./ChatbotDetailMenu";

export const ChatbotDetailMainContainer = ({ chatbot }: { chatbot?: StoreChatBotType}) => {
  return (
    <>
      {/* info banner */}
      <Content align="flex-start">
        {/* content. SEO */}
        <ChatbotDetailContent chatbot={chatbot}/>
        {/* store related/recommend. 추후 개인화. SSR */}
        <ChatbotDetailMenu  chatbot={chatbot}/>
      </Content>
    </>
  )
}

export default ChatbotDetailMainContainer;

const Content = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  @media (max-width: 1023px) {
    flex-direction: column;
    height: fit-content;
  }
`;