import { currentStoreTabIdState, storeAPI, StoreChatBotType, useEvent } from "@wrtn/core";
import { colors, FlexWrapper, newColors, newTypo } from "@wrtn/ui";
import { useRouter } from "next/router";
import React from "react";
import { StoreCardMinimum } from "src/components/Card";
import { Tool } from "src/components/Dynamic/types";
import { useStoreRecommendToolList } from "src/hooks/swr";
import { useStoreRelatedChatbotList } from "src/hooks/swr/chatbot"
import styled from "styled-components";
import { useRecoilValue } from 'recoil';

export const ChatbotDetailMenu = ({ chatbot }: { chatbot?: StoreChatBotType }) => {
  const router = useRouter();
  const { collectEvent } = useEvent();
  const currentStoreTabId = useRecoilValue(currentStoreTabIdState);

  // TODO: useStoreREcommendChatbotList();
  const {
    storeRecommendToolList
  } = useStoreRecommendToolList();


  const {
    storeRelatedChatbotList
  } = useStoreRelatedChatbotList({ chatbotId: chatbot?._id || "" });

  const handleClickRelatedCard = (chatbot: StoreChatBotType) => {
    collectEvent("click_store_card", {
      click_position: "related",
      feature_menu: "store_chatbot", 
      feature_category: chatbot.category,
      feature_name: chatbot.title,
      feature_form: "chatbot",
      prompt_id: chatbot._id,
      views: chatbot.viewNum,
      favorites: chatbot.likeNum,
      runs: chatbot.usedNum,
      downloads: 0, // TODO
      is_updated: false, // TODO
      maker_id: chatbot.userId
    })
      //TODO: 임시 처리
      window.location.href=`/store/details/chatbot/${chatbot._id}`
    router.replace(`/store/details/chatbot/${chatbot._id}`)
  }

  const handleClickRecommendCard = (chatbot: StoreChatBotType) => {
    collectEvent("click_store_card", {
      click_position: "recommended",
      feature_menu: "store_chatbot", 
      feature_category: chatbot.category,
      feature_name: chatbot.title,
      feature_form: "chatbot",
      prompt_id: chatbot._id,
      views: chatbot.viewNum,
      favorites: chatbot.likeNum,
      runs: chatbot.usedNum,
      downloads: 0, // TODO
      is_updated: false, // TODO
      maker_id: chatbot.userId
    })
    //TODO: 임시 처리
    window.location.href=`/store/details/chatbot/${chatbot._id}`
    //router.push(`/store/details/chatbot/${chatbot._id}`)
  }

  return (
    <Wrapper column align="flex-start">
      <Title>👉 연관 있는 툴/챗봇</Title>
      <CardList column>
        {
          storeRelatedChatbotList.map((v: StoreChatBotType) => (
            <StoreCardMinimum 
              key={v._id}
              onClick={() => { handleClickRelatedCard(v) }}
              type={"chatbot"}
              icon={v.icon}
              title={v.title}
            />
          ))
        }
      </CardList>
      {/* { !currentStoreTabId &&
        <>
          <Title>💜 추천 툴/챗봇</Title>
          <CardList column>
            {
              storeRecommendToolList.map((v: StoreChatBotType) => (
                <StoreCardMinimum 
                  key={v._id}
                  onClick={() => { handleClickRecommendCard(v) }}
                  type={"chatbot"}
                  icon={v.icon}
                  title={v.title}
                />
              ))
            }
          </CardList>
        </>
        } */}
    </Wrapper>
  )
}

export default ChatbotDetailMenu;

const Wrapper = styled(FlexWrapper)`
  background-color: ${newColors.BLUE_200_BG};
  width: 100%;
  padding: 47px 34px 0px 31px;
  max-width: 362px;
  height: 100%;
  overflow: scroll;
  border-left: 1px solid ${newColors.GRAY_100};

  @media (max-width: 1024px) {
    max-width: none;
    overflow: initial;
  }
`;

const CardList = styled(FlexWrapper)`
  gap: 16px;
  width: 100%;
  padding-bottom: 60px;
`;

const Title = styled.div`
  ${newTypo("content-20-semi")};
  color: ${newColors.GRAY_700};
  padding-bottom: 32px;
`;