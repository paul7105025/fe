import { StoreChatBotType, StoreToolType, apiInstance, getPlatform, handleStream, useCustomFetch, useEvent } from "@wrtn/core";
import { FlexWrapper, IconPaperPlane, LogoWrtnDefaultProfile, STORE_ICON_SELECTOR, TOOL_ICON_SELECTOR, colors, newColors, newTypo } from "@wrtn/ui";
import styled from "styled-components";
import ChatbotDetailFormInfo from "./ChatbotDetailFormInfo";
import { SpeechBallon } from "@wrtn/ui/assets/SVGComponent/StoreIcon";
import React from "react";
import {
  useTransition,
  animated,
  useChain,
  useSpringRef,
} from "@react-spring/web";
import { nanoid } from "nanoid";
import { Spinner } from "@wrtn/ui/components/Spinner";
// import { ChatbotDetailToolFormInfo } from "./ChatbotDetailFormInfo";

interface ChatbotDetailPreview {
  chatbot: StoreChatBotType
}

const UserChat = ({
  content
}) => {
  return (
    <MockCardWrapper>
      <DynamicChatWrapper>
        <UserChatWrapper>
          <UserChatContent>
            {content}
          </UserChatContent>
        </UserChatWrapper>
      </DynamicChatWrapper>
    </MockCardWrapper>
  )
}

const AIChat = ({
  icon,
  title,
  content
}) => {
  return (
    <MockCardWrapper>
      <DynamicChatWrapper>
        <DefaultChatbotWrapper>
          <DefaultChatbotNameWrapper>
            {icon ? STORE_ICON_SELECTOR[icon] : <LogoWrtnDefaultProfile />}
            <DefaultChatbotName>
              {title}
            </DefaultChatbotName>
          </DefaultChatbotNameWrapper>
          <DefaultChatContent>
            {content}
          </DefaultChatContent>
        </DefaultChatbotWrapper>
      </DynamicChatWrapper>
    </MockCardWrapper>
  )
}

const InitialMessage = ({ chatbot, messages, onGenerate }: { chatbot?: StoreChatBotType, messages: messageType[], onGenerate: (val: string, isChip?: boolean) => void }) => {
  return (
    <MockCardWrapper>
      <DynamicChatWrapper>
        <DefaultChatbotWrapper>
          <DefaultChatbotNameWrapper>
            {chatbot?.icon ? STORE_ICON_SELECTOR[chatbot?.icon] :  <LogoWrtnDefaultProfile />}
            <DefaultChatbotName>
              {chatbot?.title || "챗봇 이름"}
            </DefaultChatbotName>
          </DefaultChatbotNameWrapper>
          <DefaultChatContent>
            {chatbot?.firstMessage ||
              "안녕하세요! 무엇을 요청하고 싶으신가요?"}
          </DefaultChatContent>
        </DefaultChatbotWrapper>
        {messages.length === 0 && chatbot?.selectTypeForExampleQuestion === "예시 질문 사용" && (
          <ExampleQuestionWrapper>
            <ExampleQuestionDescriptionWrapper>
              <SpeechBallon />
              <ExampleQuestionDescription>
                이런 걸 물어볼 수 있어요
              </ExampleQuestionDescription>
            </ExampleQuestionDescriptionWrapper>
            {chatbot?.exampleQuestion?.map((item, index) => (
              <ExampleQuestionButton key={index} onClick={() => onGenerate(item, true)}>
                <ExampleQuestion>{item}</ExampleQuestion>
              </ExampleQuestionButton>
            ))}
          </ExampleQuestionWrapper>
        )}
      </DynamicChatWrapper>
    </MockCardWrapper>
  )
}

type messageType = {
  role: 'user' | 'assistant',
  content: string,
  uuid: string
};

export const ChatbotDetailPreview = ({ chatbot }: ChatbotDetailPreview) => {

  const [messages, setMessages] = React.useState<messageType[]>([])

  const [streamingMessage, setStreamingMessage] = React.useState<messageType | null>(null);
  const [input, setInput] = React.useState<string>("")
  const [modelToggle, setModelToggle] = React.useState<"GPT3.5" | "GPT4">("GPT3.5")
  const [isDisabled, setIsDisabled] = React.useState(true);

  const scrollRef = React.useRef<HTMLDivElement>(null);
  const bottomRef = React.useRef(null);
  const textareaRef = React.useRef(null);

  const chatTransRef = useSpringRef();

  const { customStudioFetch } = useCustomFetch();
  const { collectEvent } = useEvent();


  const handleChange = (value: string) => {
    setInput(value)
    if (textareaRef.current) {
      const textarea = textareaRef.current as HTMLTextAreaElement;
      textarea.style.height = 'auto';
      textarea.style.height = `${textarea.scrollHeight}px`;
    }
  }

  const changeStream = (val: string) => {
    setStreamingMessage(c => {
      if (c) return ({
        ...c,
        content: val,
      })
      return c
    });
  }

  const doneStream = (msg: any, err: any, uuid: string, isChip?: boolean) => {
    if (err) {
      setMessages([]);
      return setStreamingMessage(null);
    }
    setMessages(c => [...c, {
      content: msg?.content,
      uuid: uuid,
      role: 'assistant',
    }]);
    handleScrollToBottom();
    setStreamingMessage(null);
    collectEvent("generate_done", {
      feature_menu: "store_chatbot",
      generate_position: "store",
      is_command_chip: isChip === true ? true : false,
      feature_form: "chatbot",
      re_generate: false,
      generate_character: msg.content?.replace(/ /g, "")?.length,
    
    })
  }

  const handleGenerate = () => {
    generate(input);
  }

  const handleScrollToBottom = () => {
    if (scrollRef && scrollRef.current) {
      scrollRef.current.scrollTo({
        top: scrollRef.current.scrollHeight,
        behavior: 'smooth'
      });
    }
  }

  const generate = async (value: string, isChip?: boolean) => {
    if (streamingMessage) return ;
    const uuid = nanoid();
    setMessages(c => [...c, {
      role: 'user',
      uuid: nanoid(),
      content: value
    }]);
    setInput("");
    setStreamingMessage({
      role: 'assistant',
      content: '',
      uuid: uuid,
    });
    const platform = getPlatform();
    customStudioFetch(
      `/store/chat-bot/${chatbot._id}/generate?model=${modelToggle}&platform=${platform ?? "web"}`,
      {
        method: 'POST',
        body: JSON.stringify({
          content: value,
          model: modelToggle === 'GPT3.5' ? "gpt-3.5-turbo" : 'gpt-4',
          oldMessages: messages.map(v => ({
            role: v.role,
            content: v.content
          }))
        })
      }
    ).then(async (res) => {
      const stream = res.body?.getReader();
      if (!stream) return ;
      return await handleStream(
        stream,
        changeStream,
        (msg, err) => doneStream(msg, err, uuid, isChip)
      )
    }).catch((err) => {
      doneStream(null, err, uuid);
    })
  }

  React.useEffect(() => {
    // bottomRef.current?.scrollIntoView({
    //   behavior: "smooth"
    // })
  }, [messages])

  React.useEffect(() => {
    handleScrollToBottom();
  }, [streamingMessage])

  React.useEffect(() => {
    if (input !== "") {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [input]);

  const chatTransition = useTransition(messages, {
    keys: (item) => (item.uuid),
    ref: chatTransRef,
    trail: 800,
    from: { opacity: 0, transform: "translate3d(0, 20px, 0)" },
    enter: { opacity: 1, transform: "translate3d(0, 0px, 0)" },
    config: { duration: 400 },
  });


  useChain([chatTransRef], [0, 0.1]);

  return (
    <Wrapper column>
      <OverflowWrapper column ref={scrollRef}>
        <InitialMessage chatbot={chatbot} messages={messages} onGenerate={generate}/>
        { chatTransition((style, item, t, idx) => {
          let component: React.ReactNode | null = null;
          if (item.role === "user") {
            component = <UserChat
              key={item.uuid}
              content={item.content}
            />;
          }
          
          if (item.role === "assistant") {
            component = <AIChat
            icon={chatbot.icon}
            key={item.uuid}
            title={chatbot.title}
            content={item.content}
          />;
          }

          return (
            <>
              {component}
            </>
          )
        })
        }
        { streamingMessage && 
          <AIChat
            icon={chatbot.icon}
            key={streamingMessage.uuid}
            title={chatbot.title}
            content={streamingMessage.content}
          />
        }
        <div ref={bottomRef}/>
      </OverflowWrapper>
      <MessageWrapper>
        <MessageInputWrapper>
          <MessageInput
            ref={textareaRef}
            value={input}
            rows={1}
            onChange={(e) => handleChange(e.target.value)}
            placeholder="메시지를 입력해주세요."
            onKeyUp={(e: any) => {
              if (streamingMessage) return;
              if (e.key === "Enter" && !e.shiftKey) {
                e.preventDefault();
              }

              if (
                !e.metaKey &&
                !e.shiftKey &&
                e.key === "Enter" &&
                input.trim().length > 0
              ) {
                e.preventDefault();
                e.stopPropagation()
                handleGenerate();
              }
            }}
          />
          <SendWrapper>
            <ModelSelectButtonWrapper>
              <ModelButton
                onClick={() => {
                  setModelToggle("GPT3.5");
                }}
                selected={modelToggle === "GPT3.5"}
              >
                <ModelText selected={modelToggle === "GPT3.5"}>
                  GPT-3.5
                </ModelText>
              </ModelButton>
              <ModelButton
                onClick={() => {
                  setModelToggle("GPT4");
                }}
                selected={modelToggle === "GPT4"}
              >
                <ModelText selected={modelToggle === "GPT4"}>
                  GPT-4
                </ModelText>
              </ModelButton>
            </ModelSelectButtonWrapper>
            <MessageSendButton disabled={!!streamingMessage} onClick={handleGenerate}>
              {/* <Send src="./send.svg" /> */}
              { streamingMessage ?
                <Spinner width={16} height={16} color={newColors.WHITE} /> :
                <IconPaperPlane />
              }
            </MessageSendButton>
          </SendWrapper>
        </MessageInputWrapper>
      </MessageWrapper>
    </Wrapper>
  )
}

export default ChatbotDetailPreview;

const Wrapper = styled(FlexWrapper)`
  width: 100%;

`;

const OverflowWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 450px;
  overflow: scroll;
  margin-bottom: 20px;
`;

const MockCardWrapper = styled.div`
  width: 100%;
  max-width: 1024px;
  background: white;
  border-radius: 20px;
`;

const MockFirstContentWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 13px;
  margin-bottom: 16px;
  > svg {
    height: 26px;
  }
`;

const HeaderTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 100%;
  color: ${colors.GRAY_80};
  margin-top: 5px;
`;

const HeaderDescription = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px;
  width: 100%;
  flex: none;
  order: 1;
  flex-grow: 0;
  font-weight: 500;
  font-size: 16px;
  line-height: 100%;
  color: #717488;
`;


const UserChatWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  gap: 16px;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const DefaultChatbotWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 16px;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const DefaultChatbotNameWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 8px;
  height: 24px;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const DefaultChatbotProfile = styled.img`
  width: 24px;
  height: 24px;
  border-radius: 60px;
`;

const DefaultChatbotName = styled.div`
  height: 14px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 100%;
  color: ${newColors.GRAY_800};
`;

const UserChatContent = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px 16px;
  max-width: 335px;
  ${newTypo("body-15-med")};
  color: ${newColors.WHITE};
  background: ${newColors.PURPLE_500_PRIMARY};
  border-radius: 10px 10px 0px 10px;
`;

const DefaultChatContent = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px 16px;
  max-width: 335px;
  background: #f2f7ff;
  border-radius: 10px;
  ${newTypo("body-15-med")};
`;

const ExampleQuestionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
  padding: 0px;
  gap: 16px;
  margin-top: 55px;
`;

const ExampleQuestionDescriptionWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 0px;
  gap: 9px;
  height: 16px;
`;

const QuestionIcon = styled.img`
  width: 16px;
  height: 16px;
`;

const ExampleQuestionDescription = styled.div`
  height: 14px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 100%;
  color: ${newColors.PURPLE_500_PRIMARY};
`;

const ExampleQuestionButton = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 10px 16px;
  gap: 4px;
  height: 40px;
  background: #ffffff;
  border: 1px solid ${newColors.GRAY_100};
  box-shadow: 0px 2px 10px rgba(79, 68, 195, 0.1);
  border-radius: 20px;
  cursor: pointer;
`;

const ExampleQuestion = styled.div`
  height: 16px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 100%;
  color: ${newColors.PURPLE_500_PRIMARY};
`;

const CategoryMockWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  margin-top: 20px;
  gap: 18px;
  flex: none;
  order: 0;
  flex-grow: 0;
`;


const CategoryMock = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  padding: 5px 9px;
  gap: 10px;
  height: 24px;
  border: 1px solid #c3c8d9;
  border-radius: 5px;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const CategoryMockText = styled.div`
  height: 14px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 100%;
  color: #717488;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const MockDivideLine = styled.hr`
  height: 1px;
  margin-top: 20px;
  background: #e1e7f5;
  width: 100%;
  border-radius: 0.5px;
  flex: none;
  order: 1;
  flex-grow: 0;
`;


const DynamicChatWrapper = styled.div`
  margin-top: 24px;
`;


const MessageWrapper = styled.div`
  position: sticky;
  bottom: 0px;
  display: flex;
  align-items: center;
  width: 100%;
  max-width: 1024px;
  margin-bottom: 20px;
  flex-direction: column;
  flex: 1 1 0%;
  height: 100%;
`;

const MessageInputWrapper = styled.div`
  display: flex;
  width: 100%;
  border-radius: 8px;
  border: 1px solid #d1d7ed;
  background: ${newColors.WHITE};
  padding: 8px 11px 16px 11px;
  flex-direction: column;
  min-height: 103px;
  max-height: 194px;
`;

const MessageInput = styled.textarea`
  line-height: 180%;
  width: 100%;
  border: none;
  resize: none;
  max-height: 116px;
  padding: 12px 12px 0px;
  word-break: break-all;
  transition: height 0.4s ease-in-out 0s;

  ${newTypo("body-16-med")};
  &::-webkit-scrollbar {
    width: 4px;
  }

  &::-webkit-scrollbar-track {
    background-color: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #c3c8d9;
    border-radius: 20px;
  }
`;

const SendWrapper = styled.div`
  display: flex;
  gap: 14px;
  padding-top: 6px;
  justify-content: end;
`;

const MessageSendButton = styled.button<{ disabled: boolean }>`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 8px 16px;
  width: 70px;
  height: 34px;
  gap: 10px;
  color: ${newColors.WHITE};
  border-radius: 5px;
  background: linear-gradient(
    90.34deg,
    #c57dff -21.05%,
    #669cff 70.31%,
    #99e5f4 169.62%
  );
  cursor: pointer;

  ${({ disabled }) =>
    disabled &&
    `
    background: ${newColors.GRAY_300};
    cursor: not-allowed;
    `};
`;



const ModelText = styled.p<{ selected: boolean }>`
  text-align: center;
  ${newTypo("content-14-semi")};
  color: ${newColors.GRAY_400};

  ${({ selected }) =>
    selected &&
    `
      color: ${colors.ACTION_BLUE};
    `}
`;

const ModelSelectButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  width: 160px;
  height: 34px;
  border: 1px solid ${newColors.GRAY_200};
  border-radius: 5px;
  cursor: pointer;
`;

const ModelButton = styled.div<{ selected: boolean }>`
  display: inline-block;
  width: 80px;
  height: 100%;
  border-radius: 5px;
  padding: 10px 12px;
  cursor: pointer;

  ${({ selected }) =>
    selected &&
    `
      outline: 1px solid ${colors.ACTION_BLUE};
      color: ${colors.ACTION_BLUE};
      background: ${colors.LIGHT_BLUE};
    `}
`;