import { favoriteStoreToolListState, storeAPI, StoreChatBotType, StoreToolType } from "@wrtn/core";
import { FlexWrapper, newColors } from "@wrtn/ui";
import styled from "styled-components";
import { useRecoilState } from 'recoil';
import { useFavoriteStoreToolIdList, useAuthorToolList } from "src/hooks/swr";
import { useRouter } from "next/router";
import ChatbotDetailCardList from "./ChatbotDetailCardList";
import { useAuthorChatbotList } from "src/hooks/swr/chatbot";
import { useFavoriteStoreChatbotIdList } from "src/hooks/swr/chatbot/useFavoriteStoreChatbotIdList";
import { DetailInfo } from "src/components";
import { ChatbotDetailPreview } from "./ChatbotDetailPreview";
import { ChatbotDetailFormInfo } from "./ChatbotDetailFormInfo";

interface ChatbotDetailContent {
  chatbot?: StoreChatBotType;
  // authorToolList: StoreToolType[];
}
export const ChatbotDetailContent = ({ chatbot }: ChatbotDetailContent) => {

  // TODO: useFavoriteStoreChatbotIdList()
  const {
    favoriteChatbotIdList,
    append,
    remove
  } = useFavoriteStoreChatbotIdList();

  // TODO: useAuthorChatbotList()
  const {
    authorChatbotList,
  } = useAuthorChatbotList({ authorId: chatbot?.userId });

  const handleClickFavorite = async (chatbotId: string) => {
    const favIndex = favoriteChatbotIdList.findIndex(w => w === chatbotId);

    if (favIndex >= 0) {
      const res = await storeAPI.deleteStoreFavorite_Id({
        chatbotId
      });
      if (res.status === 200) {
        append(chatbotId);
      }
    } else {
      const res = await storeAPI.deleteStoreFavorite_Id({
        chatbotId
      });
      if (res.status === 201) {
        remove(chatbotId);
      }
    }
  }

  return (
    <Wrapper column align="center">
      {/* store detail preview */}
      {/* <StoreDetailPreview tool={tool}/> */}
      <ChatbotDetailFormInfo chatbot={chatbot}/>
      <Divider />
      { chatbot && 
        <ChatbotDetailPreview chatbot={chatbot} />
      }
      {/* divider */}
      <Divider />
      {/* store detail info */}
      { chatbot &&
        <DetailInfo
          key={chatbot._id}
          icon={chatbot.icon}
          title={chatbot.title}
          type={"chatbot"}
          description={chatbot.description}
          author={chatbot?.userName || ""}
          createdAt={chatbot.createdAt}
          viewCount={chatbot.viewNum}
          isFavorite={favoriteChatbotIdList.includes(chatbot._id)}
          genCount={chatbot.usedNum}
          // exportCount={0}
          favoriteCount={chatbot.likeNum}
          category={chatbot?.category || []}
          onClick={() => {}}
          onClickExport={() => {}}
          onClickFavorite={() => { handleClickFavorite(chatbot._id) }}
        />
      } 
      {/* store detail card list */}
      <ChatbotDetailCardList
        title={`${chatbot?.userName || ""}의 툴/봇`}
        chatbotList={authorChatbotList}
      />
    </Wrapper>
  )
}

export default ChatbotDetailContent;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  padding: 52px 40px;
  overflow: scroll;

  @media (max-width: 1023px) {
    overflow: initial;
  }
`;

const Divider = styled.div`
  width: 100%;
  border: 0.5px solid ${newColors.GRAY_200};
  border-radius: 0.5px;
  margin-bottom: 44px;
`;