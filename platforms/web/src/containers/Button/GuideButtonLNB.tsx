import { showMessenger, useEvent, WRTN_COMMUNITY_HREF, WRTN_OPEN_KAKAO_HREF } from "@wrtn/core";
import { FlexWrapper, Icon, IconGuide, newColors, newTypo, typo } from "@wrtn/ui";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import React from "react";
import useHoverMenu from "src/hooks/useHoverMenu";
import styled, { css } from "styled-components";

const guideNavList = [
  {
    key: "뤼튼 소개",
    href: "/about",
  },
  {
    key: "공식 커뮤니티",
    href: WRTN_COMMUNITY_HREF,
  },
  {
    key: "사용자 카톡방",
    href: WRTN_OPEN_KAKAO_HREF,
  },
  {
    key: "고객 문의 / 상담",
    href: "/guide",
  },
];

export const GuideButtonLNB = () => {
  const { collectEvent } = useEvent();
  const guideHover = useHoverMenu({
    position: {
      top: (typeof window !== 'undefined' && window.navigator.language.includes("zh")) ? -100 : -138,
      left: 234,
    }
  });
  return (
    <>
      <Wrapper
        justify="space-between"
        align="center"
        ref={guideHover.parentRef}
        onClick={() => {
          guideHover.handleClick();
          collectEvent("click_guide")
        }}
      >
        <Text>
          <IconGuide />
          뤼튼 가이드
        </Text>
        <Icon icon="angle-right" color={newColors.GRAY_500 } size={20}/>
      </Wrapper>
      { guideHover.open(({ position, childRef }) => {
        return (
          <>
            <ModalPositionPortal position={position}>
              <HoverWrapper column ref={childRef}>
                <GuideOuterItemWrapper>
                  <GuideOuterItem
                    target="_self"
                    rel="noopener noreferrer"
                    href={guideNavList[0].href} // 소개about
                    key={guideNavList[0].key}
                  >
                    {guideNavList[0].key}
                  </GuideOuterItem>
                </GuideOuterItemWrapper>
                <Divider />
                <GuideOuterItemWrapper
                  onClick={() => {
                    collectEvent("click_community");
                  }}
                >
                  <GuideOuterItem
                    target="_blank"
                    rel="noopener noreferrer"
                    href={guideNavList[1].href} // 공식 커뮤니티
                    key={guideNavList[1].key}
                  >
                    {guideNavList[1].key}
                  </GuideOuterItem>
                </GuideOuterItemWrapper>
                <GuideOuterItemWrapper
                  onClick={() => {
                    collectEvent("click_open_kakao");
                  }}
                >
                  <GuideOuterItem
                    target="_blank"
                    rel="noopener noreferrer"
                    href={guideNavList[2].href} // 사용자 카톡방
                    key={guideNavList[2].key}
                  >
                    {guideNavList[2].key}
                  </GuideOuterItem>
                </GuideOuterItemWrapper>
                { typeof window !== 'undefined' && !navigator.language.includes("zh") &&
                  <GuideButton
                    onClick={() => {
                      collectEvent("click_channel_talk");
                      showMessenger();
                    }}
                  >
                    {guideNavList[3].key}
                  </GuideButton>
                }
              </HoverWrapper>
            </ModalPositionPortal>
          </>
        );
      })
      }
    </>
  )
}

export default GuideButtonLNB;

const Wrapper = styled(FlexWrapper)`
  cursor: pointer;
  padding: 16px;
  &:hover {
    background: ${newColors.GRAY_100};
  }
`;

const Text = styled(FlexWrapper)`
  gap: 8px;
  ${newTypo("content-14-semi")};
  color: ${newColors.GRAY_600};
`;

const HoverWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  justify-content: flex-start;
  background-color: ${newColors.WHITE};
  padding: 8px 0px;
  border-radius: 8px;
  border: 1px solid ${newColors.GRAY_200};
  overflow: hidden;
`;
const NavOuterLink = styled.a<{ isActive?: boolean }>`
  ${({ isActive }) => NavItemStyle(isActive)}
`;

const GuideStyle = css`
  text-align: left;
  width: 100%;
  min-width: 105px;
  padding: 12px 16px;
  border-radius: 0px;
  &:hover {
    border-radius: 0px;
  }
`;

const NavItemStyle = (active?: boolean, isDark?: boolean) => css`
  gap: 8px;

  display: flex;
  align-items: center;

  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: newColors.GRAY_700,
  })};

  cursor: pointer;
  padding: 6px 8px;
  &:hover {
    background: ${newColors.GRAY_200};
    border-radius: 5px;
  }

  ${isDark &&
  css`
    ${typo({
      weight: 600,
      size: "16px",
      height: "100%",
      color: newColors.WHITE,
    })};

    &:hover {
      background: none;
      border-radius: 5px;
    }
  `}
`;

const GuideOuterItem = styled(NavOuterLink)`
  ${GuideStyle}
`;


const GuideOuterItemWrapper = styled.div`
  width: 100%;
`;

const Divider = styled.div`
  width: 100%;
  height: 1px;
  margin: 8px 0px;
  background-color: ${newColors.GRAY_200};
`;

const NavButton = styled.div<{ isActive?: boolean; isDark?: boolean }>`
  ${({ isActive, isDark }) => NavItemStyle(isActive, isDark)}
`;

const GuideButton = styled(NavButton)`
  ${GuideStyle}
`;
