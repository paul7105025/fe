import React from "react";

import {
  survey,
  userSurveyType,
  SURVEY_TEXT_BY_STEP,
} from "@wrtn/core";
import { useEvent, useReCaptcha } from "@wrtn/core/hooks";
import {
  SurveyWrapper,
  SurveyInnerWrapper,
  Icon,
  Title,
  SurveyTitle,
  SurveySubTitle,
  SurveyGridContainer,
  StyledButton1,
  SurveyTextInput,
  NextButton,
  SurveyInputLabel,
  ErrorMessage,
  SurveyInputWrapper,
  SurveyMoreText,
} from "./styles";

// import { ReactComponent as Next } from "../../assets/icons/icon_next.svg";

import { IconRoundCarouselRight } from "@wrtn/ui";

interface SurveyContainerProps {
  // surveyStep: SurveyStep;
  surveyData: userSurveyType;
  isEnableUserText: boolean;
  userText: string;
  isEnableNext: boolean;
  isRecommendError: boolean;
  handleClickSurvey: (v: string) => void;
  onChangeSurvey: (key: keyof userSurveyType, value: string) => void;
  onChangeText: (
    e: React.ChangeEvent<HTMLInputElement>,
    k: string,
    b?: boolean
  ) => void;
  handleNext: () => Promise<void>;
}

const surveyStep = "STEP_JOB_SURVEY";

const SurveyContainer = ({
  // surveyStep,
  surveyData,
  isEnableUserText,
  userText,
  isEnableNext,
  isRecommendError,
  handleClickSurvey,
  onChangeSurvey,
  onChangeText,
  handleNext,
}: SurveyContainerProps) => {
  const { collectEvent } = useEvent();
  const [inputError, setInputError] = React.useState(false);

  const { isVerified, registerByRecaptcha } =
    useReCaptcha()

  React.useEffect(() => {
    collectEvent("view_onboarding_page", {
      step: "직업",
    });
  }, [surveyStep]);

  React.useEffect(() => {
    if (isEnableUserText) {
      if (userText.trim().length < 1) {
        setInputError(true);
      } else {
        setInputError(false);
      }
    } else {
      setInputError(false);
    }
  }, [isEnableUserText, userText]);

  React.useEffect(() => {
    if (isVerified) {
      handleNext();
    }
  }, [isVerified]);

  //

  return (
    <SurveyWrapper>
      <SurveyInnerWrapper>
        <Icon>🎉</Icon>
        <Title>뤼튼에 오신 것을 환영합니다!</Title>
        <>
          <SurveyTitle isSubTitle={survey[surveyStep].subTitle.length > 0}>
            {survey[surveyStep].title}
          </SurveyTitle>
          {survey[surveyStep].subTitle && (
            <SurveySubTitle>{survey[surveyStep].subTitle}</SurveySubTitle>
          )}
          <SurveyGridContainer>
            {survey[surveyStep].items.map((v: string) => {
              return (
                <StyledButton1
                  key={v}
                  onClick={() => handleClickSurvey(v)}
                  selected={
                    (surveyStep !== "STEP_JOB_SURVEY" &&
                      surveyStep !== "STEP_PURPOSE_SURVEY" &&
                      v === "기타" &&
                      isEnableUserText) ||
                    // (surveyStep === "STEP_INFLOW_SURVEY" &&
                    //   surveyData.inflow === v) ||
                    (surveyStep === "STEP_JOB_SURVEY" &&
                      surveyData.job.find((x) => x === v) !== undefined)
                    //  ||
                    // (surveyStep === "STEP_PURPOSE_SURVEY" &&
                    //   surveyData.purpose.find((x) => x === v) !== undefined)
                  }
                  // disabled={
                  //   surveyStep === "STEP_PURPOSE_SURVEY" &&
                  //   surveyData.purpose.find((x) => x === v) === undefined &&
                  //   surveyData.purpose.length >= 3
                  // }
                  // isRadio={surveyStep !== "STEP_PURPOSE_SURVEY"}
                  isRadio
                >
                  {v}
                </StyledButton1>
              );
            })}
          </SurveyGridContainer>
          {isEnableUserText && (
            <>
              <SurveyTextInput
                placeholder={survey[surveyStep].etc}
                value={userText}
                onChange={(e) =>
                  onChangeText(
                    e,
                    SURVEY_TEXT_BY_STEP[surveyStep],
                    // surveyStep === "STEP_PURPOSE_SURVEY" ||
                    surveyStep === "STEP_JOB_SURVEY"
                  )
                }
              />
              {inputError && (
                <ErrorMessage>빈 칸 없이 입력해주세요.</ErrorMessage>
              )}
            </>
          )}
          {surveyStep === "STEP_JOB_SURVEY" && (
            <>
              <SurveyInputWrapper>
                <SurveyInputLabel>소속</SurveyInputLabel>
              </SurveyInputWrapper>
              <SurveyTextInput
                placeholder="현재 소속을 알려주세요!"
                value={surveyData.company}
                onChange={(e) => onChangeSurvey("company", e.target.value)}
              />
            </>
          )}
          {/* {surveyStep === "STEP_INFLOW_SURVEY" && (
            <>
              <SurveyInputWrapper>
                <SurveyInputLabel>추천인 코드(선택)</SurveyInputLabel>
                <SurveyMoreText>
                  추천인 코드 입력 시 5% 할인 쿠폰을 드려요!
                </SurveyMoreText>
              </SurveyInputWrapper>
              <SurveyTextInput
                placeholder="추천인 코드를 입력해주세요."
                value={surveyData.recommendCode}
                onChange={(e) =>
                  onChangeSurvey("recommendCode", e.target.value.toUpperCase())
                }
                isError={isRecommendError}
                maxLength={6}
              />
              {isRecommendError && (
                <ErrorMessage>존재하지 않는 추천인 코드입니다.</ErrorMessage>
              )}
            </>
          )} */}
        </>

        <NextButton
          disabled={!isEnableNext || inputError}
          onClick={async () => {
            // await handleReCaptchaVerify("register");

            registerByRecaptcha();

            // handleNext();
          }}
        >
          뤼튼 시작하기
        </NextButton>
      </SurveyInnerWrapper>
    </SurveyWrapper>
  );
};

export default SurveyContainer;
