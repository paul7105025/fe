import styled from "styled-components";

// import { StyledButton } from "@wrtn/ui/components";
import {
  boxShadow,
  colors,
  typo,
  FlexWrapper,
  FlexButton,
} from "@wrtn/ui/styles";

export const SurveyWrapper = styled(FlexWrapper)`
  padding: 62px 10px 38px;

  margin: 0px auto;

  width: 100%;
  max-width: 784px;

  /* min-height: 646px; */
  /* max-height: 95vh; */

  background: ${colors.WHITE};
  ${boxShadow.guideline_shadow};

  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  border-radius: 20px;

  flex-direction: column;
  justify-content: flex-start;

  box-shadow: 0px 0px 20px rgba(56, 71, 130, 0.05);

  @media (max-width: 767px) {
    padding: 42px 10px 28px;
  } ;
`;

export const ConsentWrapper = styled.div`
  margin: 0px auto;
  /* width: 100%; */
  max-width: 804px;

  background: ${colors.WHITE};
  ${boxShadow.guideline_shadow};
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  border-radius: 20px;
  padding: 45px 84px 88px;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 767px) {
    padding: 45px 30px 40px;
    max-width: 95vw;
  }
`;

export const Icon = styled.div`
  ${typo({ weight: 700, size: "30px", height: "100%", color: colors.gray_90 })};
  margin-bottom: 13px;
`;

export const Title = styled.p`
  ${typo({ weight: 600, size: "24px", height: "160%", color: colors.gray_90 })};

  margin-bottom: 12px;

  @media (max-width: 767px) {
    ${typo({
      weight: 600,
      size: "18px",
      height: "160%",
      color: colors.gray_80,
    })};
  }
`;

export const ConsentTitle = styled.p`
  ${typo({ weight: 600, size: "24px", height: "160%", color: colors.gray_90 })};
  margin-bottom: 24px;
  max-width: 100%;
  text-align: center;
  @media (max-width: 480px) {
    ${typo({
      weight: 600,
      size: "18px",
      height: "160%",
      color: colors.gray_80,
    })};
    margin-bottom: 16px;
  }
`;

export const SurveyTitle = styled.p<{ isSubTitle?: boolean }>`
  ${typo({ weight: 600, size: "18px", height: "160%", color: colors.gray_80 })};
  margin-bottom: ${(props) => (props.isSubTitle ? "4px" : "42px")};

  @media (max-width: 480px) {
    ${typo({
      weight: 600,
      size: "14px",
      height: "160%",
      color: colors.gray_80,
    })};
    margin-bottom: ${(props) => (props.isSubTitle ? "4px" : "16px")};
  }
`;

export const SurveyInputWrapper = styled(FlexWrapper)`
  width: 100%;
  margin-top: 12px;
  justify-content: space-between;
`;

export const SurveyInputLabel = styled.p`
  ${typo({ weight: 600, size: "14px", height: "160%", color: colors.gray_80 })};
`;

export const SurveyMoreText = styled.p`
  ${typo({ weight: 500, size: "12px", height: "160%", color: colors.gray_70 })};
`;

export const SurveySubTitle = styled.p`
  ${typo({ weight: 500, size: "14px", height: "160%", color: colors.gray_60 })};
  margin-bottom: 16px;
`;

export const ConsentSubTitle = styled.p`
  ${typo({
    weight: 700,
    size: "16px",
    height: "19px",
    color: colors.ACTION_BLUE,
  })};
  margin-bottom: 20px;
  @media (max-width: 480px) {
    ${typo({
      weight: 600,
      size: "14px",
      height: "160%",
      color: colors.ACTION_BLUE,
    })};
    margin-bottom: 12px;
  }
`;

export const SurveyGridContainer = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 20px 22px;

  margin-bottom: 27px;

  @media (max-width: 767px) {
    display: flex;
    flex-direction: column;
    grid-gap: 10px 16px;
  }
`;

export const SurveyInnerWrapper = styled(FlexWrapper)`
  width: 100%;
  max-width: 485px;

  flex-direction: column;

  @media (max-width: 767px) {
    padding: 20px;
  }
`;

export const NextButton = styled.button`
  margin-top: 18px;
  margin-left: auto;
  width: 100%;
  background: ${colors.POINT_PURPLE};
  ${typo({ weight: 600, size: "16px", height: "23px", color: colors.WHITE })};
  border-radius: 5px;
  padding: 10px 30px 8px;
  cursor: pointer;

  &:hover {
    background: ${colors.POINT_PURPLE_HOVER};
  }

  &:disabled {
    background: ${colors.BLUE_GRAY_LINE};
    cursor: auto;
  }
`;

export const SurveyTextInput = styled.input<{ isError?: boolean }>`
  width: 100%;
  ${typo({ weight: 500, size: "16px", height: "26px", color: colors.gray_90 })};
  margin-top: 7px;
  background: ${colors.WHITE};
  border: 1px solid
    ${({ isError }) => (isError ? colors.HOT_RED : colors.BLUE_GRAY_LINE)};
  border-radius: 5px;
  padding: 8px 16px;

  &::placeholder {
    ${typo({
      weight: 500,
      size: "16px",
      height: "26px",
      color: colors.gray_55,
    })};
  }
`;

export const ConsentUnorderedList = styled.ul`
  margin-bottom: 58px;

  @media (max-width: 480px) {
    margin-bottom: 38px;
  }
`;

export const ConsentListItem = styled.li`
  ${typo({
    weight: 500,
    size: "14px",
    height: "200%",
    color: colors.gray_100,
  })};
  @media (max-width: 480px) {
    ${typo({
      weight: 600,
      size: "11px",
      height: "160%",
      color: colors.gray_80,
    })};
  }
`;

export const ConsentText = styled.p`
  ${typo({
    weight: 500,
    size: "14px",
    height: "200%",
    color: colors.gray_100,
  })};
  margin-bottom: 45px;
  @media (max-width: 480px) {
    ${typo({
      weight: 600,
      size: "12px",
      height: "160%",
      color: colors.gray_80,
    })};
  }
`;

export const AgreementContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 8px;
  margin-bottom: 30px;
  cursor: pointer;
`;

export const CheckBox = styled(FlexButton)<{ checked: boolean }>`
  width: 16px;
  height: 16px;

  cursor: pointer;

  background: ${(props) => (props.checked ? colors.ACTION_BLUE : colors.WHITE)};
  border: 2px solid
    ${(props) => (props.checked ? colors.ACTION_BLUE : colors.BLUE_GRAY_LINE)};
  border-radius: 2px;

  svg {
    width: 10px;
    height: 12px;
  }
`;

export const AgreementText = styled.p`
  ${typo({
    weight: 500,
    size: "16px",
    height: "160%",
    color: colors.gray_100,
  })};
  @media (max-width: 767px) {
    ${typo({
      weight: 600,
      size: "12px",
      height: "160%",
      color: colors.gray_100,
    })};
  }
  user-select: none;
  white-space: nowrap;
`;

export const StyledButton1 = styled(FlexButton)<{
  selected: boolean;
  isRadio: boolean;
}>`
  /* height: 42px; */
  box-shadow: none;

  padding: 12px 0px;
  border-radius: 5px;

  background-color: ${({ selected }) =>
    selected ? colors.ACTION_BLUE : colors.BACKGROUND};

  cursor: ${(props) => (props.disabled ? "default" : "pointer")};
  ${typo({ weight: 500, size: "16px", height: "100%" })};
  color: ${(props) => (props.selected ? colors.WHITE : colors.ACTION_BLUE)};

  &:hover {
    background-color: ${(props) =>
      props.selected
        ? props.isRadio
          ? colors.ACTION_BLUE
          : colors.TAG_BLUE
        : colors.BLUE_GRAY_LINE};
  }

  @media (max-width: 480px) {
    padding: 10px 0px;

    ${typo({ weight: 500, size: "12px", height: "100%" })};
    color: ${(props) => (props.selected ? colors.WHITE : colors.ACTION_BLUE)};
  }
`;

// export const StyledButton1 = styled(StyledButton)`
//   height: 42px;
//   box-shadow: none;
//   padding: 12px 0px;
// `;

export const StyledButton2 = styled(FlexButton)<{ selected: boolean }>`
  justify-content: space-evenly;
  width: 100%;
  max-width: 360px;

  padding: 9px 0px;
  gap: 44px;
  border-radius: 5px;

  cursor: pointer;

  color: white;
  background-color: ${({ selected }) =>
    selected ? colors.POINT_PURPLE : colors.BLUE_GRAY_LINE};

  ${typo({ weight: 600, size: "18px", height: "129%", color: colors.WHITE })};

  &:hover {
    background-color: ${({ selected }) =>
      selected ? colors.POINT_PURPLE_HOVER : colors.BLUE_GRAY_LINE};
  }
  white-space: nowrap;
`;

export const ErrorMessage = styled.p`
  width: 100%;
  margin-top: 7px;
  ${typo({
    weight: 400,
    size: "12px",
    color: colors.HOT_RED,
  })};
`;
