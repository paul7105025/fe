import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import { useLogout, useTagManager, userState, useEvent } from "@wrtn/core";

import { colors, typo, FlexButton, FlexWrapper } from "@wrtn/ui/styles";

// import { ReactComponent as GreenCheck } from "../assets/icons/check.svg";
// import { ReactComponent as LogoWrtn } from "../assets/logos/logo_wrtn.svg";

import { ReactComponent as Check } from "@wrtn/ui/assets/icons/icon_check_white.svg";
import polyfill from "@wrtn/ui/styles/polyfill";
import { useRouter } from "next/router";
import { paths } from "src/constants";
import { IconCheckGreen, WrtnLogoGray } from "@wrtn/ui";

const TermContainer = ({
  email,
  handleContinue,
  surveyData,
  onChangeSurvey,
}) => {
  const user = useRecoilValue(userState);
  const [terms, setTerms] = React.useState(false);
  const [privacy, setPrivacy] = React.useState(false);

  const tagManager = useTagManager();
  const logout = useLogout();
  const { collectEvent } = useEvent();

  const router = useRouter();
  return (
    <>
      <TitleIcon>
        <IconCheckGreen />
      </TitleIcon>
      <EmailLoginTitle>서비스 이용 약관을 확인해주세요.</EmailLoginTitle>
      <div style={{ height: "40px" }} />
      <InputWrapper>
        <InputLabel>이메일 주소</InputLabel>
        <InputInnerWrapper>
          <InputBox>
            <Input disabled={true} value={email} />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <CheckLabel>
          <CheckBox
            id="all"
            // @ts-ignore
            type="checkbox"
            checked={terms && privacy && surveyData.termMarketing}
            onClick={() => {
              setTerms(!terms || !privacy || !surveyData.termMarketing);
              setPrivacy(!terms || !privacy || !surveyData.termMarketing);
              onChangeSurvey(
                "termMarketing",
                !terms || !privacy || !surveyData.termMarketing
              );
            }}
          >
            {terms && privacy && surveyData.termMarketing && <Check />}
          </CheckBox>
          <Label htmlFor="all">모든 항목에 동의합니다.</Label>
        </CheckLabel>
        <Divider />
        <CheckLabel>
          <CheckBox
            id="terms"
            checked={terms}
            onClick={() => {
              setTerms((c) => !c);
            }}
          >
            {terms && <Check />}
          </CheckBox>
          <Label htmlFor="terms">
            <Anchor
              href="https://wrtn.ai/terms"
              target="_blank"
              rel="noopener noreferrer"
            >
              서비스 이용약관
            </Anchor>
            에 동의합니다. (필수)
          </Label>
        </CheckLabel>
        <div style={{ height: "11px" }} />
        <CheckLabel>
          <CheckBox
            id="privacy"
            checked={privacy}
            onClick={() => setPrivacy((c) => !c)}
          >
            {privacy && <Check />}
          </CheckBox>
          <Label htmlFor="privacy">
            <Anchor
              href="https://wrtn.ai/privacy"
              target="_blank"
              rel="noopener noreferrer"
            >
              개인정보처리방침
            </Anchor>
            에 동의합니다. (필수)
          </Label>
        </CheckLabel>
        <div style={{ height: "11px" }} />
        <CheckLabel>
          <CheckBox
            id="marketing"
            checked={surveyData.termMarketing}
            onClick={() =>
              onChangeSurvey("termMarketing", !surveyData.termMarketing)
            }
          >
            {surveyData.termMarketing && <Check />}
          </CheckBox>
          <Label htmlFor="marketing">
            <Anchor
              href="https://wrtn.ai/terms-marketing"
              target="_blank"
              rel="noopener noreferrer"
            >
              마케팅 정보 활용
            </Anchor>
            에 동의합니다. (선택)
          </Label>
        </CheckLabel>
      </InputWrapper>
      <InputWrapper>
        <InputButton
          disabled={!(terms && privacy)}
          onClick={async () => {
            tagManager({
              event: "google_agreement",
              data: { is_agreed_marketing: surveyData.termMarketing },
            });
            handleContinue(terms && privacy);
          }}
        >
          계속하기
        </InputButton>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <BottomLabel
          onClick={() => {
            collectEvent("logout");
            logout(user).then(() => router.push(paths.login.index()));
          }}
        >
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export const TermSocialPage = ({ handleNext, surveyData, onChangeSurvey }) => {
  const user = useRecoilValue(userState);

  const { collectEvent } = useEvent();

  React.useEffect(() => {
    collectEvent("view_agree_terms_page");
  }, []);

  const handleContinue = (valid) => {
    if (valid) handleNext();
  };

  return (
    <Wrapper justify="centers">
      <div style={{ flex: 1 }} />
      <BoxWrapper justify="center">
        <CertInnerWrapper>
          <div style={{ flex: 1 }} />
          <TermContainer
            email={user?.email || "email@wrtn.io"}
            handleContinue={handleContinue}
            surveyData={surveyData}
            onChangeSurvey={onChangeSurvey}
          />
        </CertInnerWrapper>
      </BoxWrapper>
      <div style={{ flex: 1 }} />
      <BottomLogo>
        <WrtnLogoGray />
      </BottomLogo>
    </Wrapper>
  );
};
export default TermSocialPage;

const Wrapper = styled(FlexWrapper)`
  width: 100vw;
  ${polyfill.dvh("height", 100)};
  background: linear-gradient(180deg, #f3f4ff -24.67%, #ffffff 103.17%);
  z-index: 2;

  flex-direction: column;
  justify-content: flex-start;
  overflow: scroll;

  gap: 20px;
`;

const BoxWrapper = styled(FlexWrapper)`
  position: relative;
  padding: 40px 35px;
  background: #ffffff;
  box-shadow: 5px 5px 20px rgba(0, 0, 0, 0.05);
  border-radius: 12px;
  height: 702px;
  width: 100%;
  max-width: 784px;
  margin: 0px 20px;

  @media (max-width: 480px) {
    max-width: 95vw;
  }
`;

const CertInnerWrapper = styled(FlexWrapper)`
  width: 100%;
  max-width: 460px;
  height: 100%;

  flex-direction: column;
  justify-content: flex-start;
`;

const BottomLogo = styled.div`
  svg {
    height: 28px;
    width: 255px;
    path {
      fill: ${colors.gray_30};
    }
  }
`;

const Anchor = styled.a`
  text-decoration-line: underline;
  cursor: pointer;
`;

const CheckLabel = styled(FlexWrapper)`
  ${typo({
    weight: 500,
    size: "13px",
    height: "13px",
    color: colors.gray_70,
  })};
  justify-content: flex-start;
  gap: 7px;
  user-select: none;
`;

const Label = styled.label``;

const Divider = styled.div`
  margin: 10px 0px;
  width: 100%;
  border: 0.5px solid ${colors.BLUE_GRAY_LINE};
`;

const InputButton = styled.button`
  width: 100%;
  border: none;
  border-radius: 8px;
  padding: 12px 0px;

  ${typo({
    size: "16px",
    weight: 500,
  })}

  text-align: center;
  background: ${colors.POINT_PURPLE};
  color: white;
  flex-grow: 1;
  cursor: pointer;
  &:disabled {
    background: ${colors.BLUE_GRAY_LINE};
    cursor: default;
  }
`;

const CheckBox = styled(FlexButton)<{ checked: boolean }>`
  width: 16px;
  height: 16px;

  cursor: pointer;

  background: ${(props) => (props.checked ? colors.ACTION_BLUE : colors.WHITE)};
  border: 1px solid
    ${(props) => (props.checked ? colors.ACTION_BLUE : colors.BLUE_GRAY_LINE)};
  border-radius: 2px;

  svg {
    width: 10px;
    height: 12px;
  }
`;

const EmailLoginTitle = styled.h4`
  ${typo({
    size: "24px",
    weight: 600,
    color: colors.GRAY_90,
  })}

  text-align: center;
  margin-bottom: 19px;
`;

const InputWrapper = styled.div`
  width: 100%;
  ${"" /* max-width: 380px; */}
  margin: 0px auto;
  margin-bottom: 20px;
`;

const InputLabel = styled.p`
  ${typo({
    size: "12px",
    weight: 400,
    color: "#555555",
  })}

  margin-bottom: 8px;
  white-space: nowrap;
`;

const InputInnerWrapper = styled.div`
  display: flex;
  gap: 8px;
  width: 100%;

  @media (max-width: 767px) {
    flex-direction: column;
    align-items: flex-end;
  }
`;

const Input = styled.input`
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 5px;
  background: white;
  padding: 12px 16px;

  ${typo({
    size: "16px",
    weight: 500,
    color: colors.GRAY_100,
  })}

  flex-grow: 1;
  width: 100%;
  &::placeholder {
    color: ${colors.BLUE_GRAY_LINE};
  }

  &:focus {
    border: 1px solid ${colors.POINT_PURPLE};
  }

  &:disabled {
    color: ${colors.BLUE_GRAY_LINE};
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

const InputBox = styled.div`
  position: relative;
  flex-grow: 1;
  width: 100%;
`;

const TitleIcon = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 18px;
`;

const BottomLabel = styled.div`
  text-align: center;
  text-decoration-line: underline;
  cursor: pointer;
  ${typo({ weight: 500, size: "14px", height: "100%", color: colors.gray_70 })};
  white-space: nowrap;
`;

const BottomWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 18px;
`;
