import { LoginProvider } from "@wrtn/core/types/login";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";
import { providerKor } from "../../utils/provider";
import { LoginDialogSocialWarning } from "./LoginDialogSocialWarning";
import { CloseIcon, LoginDialogWrapper } from "./styles";

export type BlockWithdrawDialogProps = {
  onClose: () => void;
  email: String;
  provider: LoginProvider;
};

export const BlockWithdrawDialog = ({
  onClose,
  email,
  provider,
}: BlockWithdrawDialogProps) => {
  return (
    <ModalPortal onClose={onClose}>
      <LoginDialogWrapper width={395}>
        <CloseIcon onClick={onClose} />
        <TopWrapper></TopWrapper>
        <BottomWrapper>
          <Title>탈퇴한 지 30일 이후부터 재가입 가능해요.</Title>
          <Description>
            탈퇴일로부터 30일 이전에는 <b>같은 계정</b>
          </Description>
          <Description>(동일 이메일 주소 및 동일 로그인 경로)으로</Description>
          <Description>재가입 할 수 없습니다.</Description>
          <div style={{ height: "22px" }} />
          <Description>
            가입 주소: <b>{email}</b>
          </Description>
          <Description>
            가입 경로: <b>{providerKor(provider)}</b>
          </Description>
          <LoginWrapper>
            <Button justify="center" onClick={onClose}>확인</Button>
          </LoginWrapper>
        </BottomWrapper>
        {provider !== "local" && (
          <LoginDialogSocialWarning provider={provider} />
        )}
      </LoginDialogWrapper>
    </ModalPortal>
  );
};

export default BlockWithdrawDialog;

const TopWrapper = styled.div`
  width: 100%;
  height: 57px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 20px 20px 0px 0px;
`;

const BottomWrapper = styled.div`
  padding: 32px 22px 18px;
`;

const Title = styled.p`
  ${typo({
    weight: "700",
    size: "18px",
    height: "100%",
    color: colors.gray_90,
  })}

  text-align: center;
  margin-bottom: 22px;
`;

const Description = styled.p`
  ${typo({
    weight: "500",
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};

  text-align: center;
`;

const LoginWrapper = styled(FlexWrapper)`
  padding-top: 26px;
  padding-bottom: 20px;
  gap: 41px;
  flex-direction: column;
`;

const Button = styled(FlexWrapper)`
  ${typo({
    weight: 600,
    size: "16px",
    height: "16px",
    color: colors.ACTION_BLUE,
  })};
  background-color: #f2f7ff;
  width: 100%;
  padding: 12px;
  cursor: pointer;
`;
