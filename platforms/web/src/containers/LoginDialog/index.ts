import { LoginMainDialog } from 'src/components/Auth';
import BlockIPDialog from './BlockIpDialog';
import BlockWithdrawDialog from './BlockWithdrawDialog';
import DuplicateRegisterDialog from './DuplicateRegisterDialog';

export const LoginDialog = () => {};

LoginDialog.Login = LoginMainDialog
LoginDialog.BlockIP = BlockIPDialog; 
LoginDialog.BlockWithdraw = BlockWithdrawDialog;
LoginDialog.BlockDuplicate = DuplicateRegisterDialog;


export default LoginDialog;