import { LoginProvider } from "@wrtn/core/types/login";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";
import { LoginDialogSocialWarning } from "./LoginDialogSocialWarning";
import { CloseIcon, LoginDialogWrapper } from "./styles";

export type BlockIPDialogProps = {
  onClose: () => void;
  provider: LoginProvider;
};

export const BlockIPDialog = ({ onClose, provider }: BlockIPDialogProps) => {
  return (
    <ModalPortal onClose={onClose}>
      <LoginDialogWrapper width={395}>
        <CloseButton onClick={onClose}>
          <CloseIcon onClick={onClose}/>
        </CloseButton>
        <TopWrapper></TopWrapper>
        <BottomWrapper>
          <Title>이미 계정을 보유하고 계신가요?</Title>
          <Description>
            이미 계정을 여러 개 보유하고 계신 것 같아요.
          </Description>
          <Description>
            이전에 가입했던 아이디와 경로로 로그인해주세요.
          </Description>
          <Description>가입 이력이 없을 경우, 페이지 우측 하단</Description>
          <Description>고객센터를 통해 문의해주세요.</Description>
          <LoginWrapper>
            <Button onClick={onClose}>확인</Button>
          </LoginWrapper>
        </BottomWrapper>
        {provider !== "local" && (
          <LoginDialogSocialWarning provider={provider} />
        )}
      </LoginDialogWrapper>
    </ModalPortal>
  );
};

export default BlockIPDialog;

const TopWrapper = styled.div`
  width: 100%;
  height: 57px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 20px 20px 0px 0px;
`;

const BottomWrapper = styled.div`
  padding: 32px 22px 18px;
`;

const Title = styled.p`
  ${typo({
    weight: "700",
    size: "18px",
    height: "100%",
    color: colors.gray_90,
  })}

  text-align: center;
  margin-bottom: 22px;
`;

const Description = styled.p`
  ${typo({
    weight: "500",
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};

  text-align: center;
`;

const Button = styled(FlexButton)`
  ${typo({
    weight: 600,
    size: "16px",
    height: "16px",
    color: colors.ACTION_BLUE,
  })};
  background-color: #f2f7ff;
  width: 100%;
  padding: 12px;
  cursor: pointer;
`;

const CloseButton = styled(FlexButton)``;

const LoginWrapper = styled(FlexWrapper)`
  padding-top: 26px;
  padding-bottom: 20px;
  gap: 41px;
  flex-direction: column;
`;
