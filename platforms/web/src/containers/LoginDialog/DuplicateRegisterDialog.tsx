import { GoogleOAuthProvider } from "@react-oauth/google";
import { LoginProvider } from "@wrtn/core/types/login";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { colors, FlexWrapper, typo } from '@wrtn/ui/styles';
import { MouseEventHandler } from "react";
import { useSearchParams } from "next/navigation";
import styled from 'styled-components';
import LoginProviderButton from "../../components/Button";
import { useSocialLogin } from "../../hooks/auth";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import { providerKor } from "../../utils/provider";
import { LoginDialogSocialWarning } from "./LoginDialogSocialWarning";
import { CloseIcon, LoginDialogWrapper } from "./styles";

const GOOGLE_CLIENT_ID = process.env.NEXT_PUBLIC_URL_GOOGLE_CLIENT_ID;

export type DuplicateRegisterDialogProps = {
  onClose: () => void;
  email: string;
  provider: LoginProvider;
  from: LoginProvider;
}

export const DuplicateRegisterDialog = ({
  onClose,
  email,
  provider,
  from,
}: DuplicateRegisterDialogProps) => {
  const searchParams = useSearchParams();
  const recommend = searchParams.get("recommend");
  const localLogin = useLocalLogin();

  const {
    handleGoogleLoginSuccess,
    handleKakaoLoginSuccess,
    handleNaverLoginSuccess,
    handleGoogleLoginFailure,
    handleNaverLoginFailure,
    handleKakaoLoginFailure,
  } = useSocialLogin();

  return (
    <GoogleOAuthProvider clientId={GOOGLE_CLIENT_ID || ""}>
      <ModalPortal onClose={onClose}>
        <LoginDialogWrapper width={395}>
          <CloseIcon onClick={onClose} />
          <TopWrapper></TopWrapper>
          <BottomWrapper>
            <Title>같은 주소로 가입한 계정이 있어요</Title>
            <Description>이 메일 주소 ({email})는</Description>
            <Description><b>{providerKor(provider)} 계정으로</b> 가입되어있어요!</Description>
            <Description>해당 계정으로 로그인 해주세요.</Description>
            <LoginWrapper>
            { provider === 'google' &&
              <LoginProviderButton.Google
                useOneTap
                onSuccess={handleGoogleLoginSuccess}
                onError={handleGoogleLoginFailure}
                width={"324px"}
              />
            }
            { provider === 'naver' &&
              <LoginProviderButton.Naver
                onSuccess={handleNaverLoginSuccess}
                onFailure={handleNaverLoginFailure}
                callbackUrl={`${window.location.origin}/app/login/${recommend ? `?recommend=${recommend}` : ""}`}
              />
            }
            { provider === 'kakao' &&
              <LoginProviderButton.Kakao 
                onSuccess={handleKakaoLoginSuccess}
                onFail={handleKakaoLoginFailure}
              />
            }
            { provider === 'local' &&
              <LoginProviderButton.Local
                onClick={() => {
                  localLogin.handleChangeEmail(email);
                  localLogin.handleLoginEmail();
                }}
              />
            }
            <Other onClick={onClose}>다른 주소로 회원가입</Other>
            </LoginWrapper>
          </BottomWrapper>
          { from !== "local" &&
            <LoginDialogSocialWarning provider={from}/>
          }
        </LoginDialogWrapper>
      </ModalPortal>
    </GoogleOAuthProvider>
  )
};

export default DuplicateRegisterDialog

const TopWrapper = styled.div`
  width: 100%;
  height: 57px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 20px 20px 0px 0px;
`;

const BottomWrapper = styled.div`
  padding: 32px 22px 18px;
`;

const Title = styled.p`
  ${typo({
    weight: "700",
    size: "18px",
    height: "100%",
    color: colors.gray_90,
  })}

  text-align: center;
  margin-bottom: 22px;
`;

const Description = styled.p`
  ${typo({
    weight: "500",
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};

  text-align: center;
`;

const Other = styled.div`
  ${typo({
    weight: 500,
    size: '16px',
    height: '16px',
    color: colors.gray_80,
  })}
  text-decoration-line: underline;
  cursor:pointer;
`;

const LoginWrapper = styled(FlexWrapper)`
  padding-top: 26px;
  padding-bottom: 20px;
  gap: 41px;
  flex-direction: column;
`;