import { LoginProvider } from "@wrtn/core/types/login";
import { IconWarning } from "@wrtn/ui/assets";
import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";
import { providerAuthHref } from "../../utils/provider";

type LoginDialogSocialWarningProps = {
  provider: LoginProvider
}
export const LoginDialogSocialWarning = ({
  provider
}: LoginDialogSocialWarningProps) => {
  return (
    <WarningWrapper>
      <Divider />
      <WarningTitle><IconWarning /> <div>잠깐 확인해주세요!</div></WarningTitle>
      <WarningDesc>
        <b>소셜 로그인</b>을 통해 위 메일 주소와 <b>다른 주소</b>로 로그인을
        <br/>시도하시는 경우, <WarningLink target="_blank" href={providerAuthHref(provider)}>해당 웹사이트</WarningLink>에서 로그아웃 한 후
        <br/>뤼튼에서 로그인을 시도해주세요.
      </WarningDesc>
    </WarningWrapper>
  )
};

const WarningWrapper = styled(FlexWrapper)`
  gap: 12px;
  flex-direction: column;
  padding: 0px 36px 21px 36px;
`

const WarningTitle = styled(FlexWrapper)`
  ${typo({
    weight: '700',
    size: '14px',
    height: '21px',
    color: colors.gray_80
  })};
  gap: 9px;
`;

const WarningDesc = styled.p`
  ${typo({
    weight: 500,
    size: '14px',
    height: '21px',
    color: colors.gray_80
  })};
  text-align: center;
`

const WarningLink = styled.a`
  text-decoration: underline;
`

const Divider = styled.div`
  height: 1px;
  width: 100%;
  background: ${colors.gray_30};
`;
