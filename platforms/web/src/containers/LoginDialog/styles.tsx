import styled from "styled-components";

import { Icon } from "@wrtn/ui";
import { colors, FlexButton } from "@wrtn/ui/styles";

export const LoginDialogWrapper = styled.div<{ width: number }>`
  position: relative;
  width: ${({ width }) => width}px;
  border-radius: 20px;

  background: ${colors.WHITE};
`;

interface ICloseIcon {
  onClick: () => void;
}

export const CloseIcon = ({ onClick }: ICloseIcon) => {
  return (
    <CloseButton onClick={onClick}>
      <Icon icon="times" size={20} color={colors.GRAY_80} />
    </CloseButton>
  );
};

const CloseButton = styled(FlexButton)`
  position: absolute;
  top: 18px;
  right: 24px;
  width: 20px;
  height: 20px;
  cursor: pointer;
`;
