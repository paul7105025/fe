import {
  CERT_MAIL_INFO_DEFAULT,
  CERT_MAIL_INFO_INVALID,
  CERT_MAIL_INFO_UNKNOWN,
  CERT_PW_INFO_DEFAULT,
  CERT_PW_INFO_INVALID,
} from "@wrtn/core/constants/login";
import { getAuthCheck, postAuthLocalLogin } from "@wrtn/core/services";
import { isDuplicateLoginState } from "@wrtn/core/stores/login";
import { IconMail } from "@wrtn/ui/assets";
import { useRouter } from "next/router";
import React from "react";
import { useSetRecoilState } from "recoil";
import { useLocalLogin, } from "../../hooks/auth/useLocalLogin";
import useLogin from "../../hooks/auth/useLogin";
import {
  BottomLabel,
  BottomWrapper,
  EmailLoginSubtitle,
  EmailLoginTitle,
  FindButton,
  FindWrapper,
  InfoLabel,
  Input,
  InputBox,
  InputButton,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  LabelWrapper,
  TitleIcon,
} from "./style";

const EmailLoginSection = () => {
  const localLogin = useLocalLogin();
  const router = useRouter();
  const setIsDuplicateLogin = useSetRecoilState(isDuplicateLoginState);

  const [valid, setValid] = React.useState(CERT_MAIL_INFO_DEFAULT);
  const [pwValid, setPwValid] = React.useState(CERT_PW_INFO_DEFAULT);

  const login = useLogin();

  const handleLogin = async () => {
    setValid(CERT_MAIL_INFO_DEFAULT);
    setPwValid(CERT_PW_INFO_DEFAULT);
    if (!localLogin.isValidEmailFormat) {
      setValid(CERT_MAIL_INFO_INVALID);
      return;
    }

    const check = await getAuthCheck({
      email: localLogin.email,
    });

    if (!check) {
      setValid(CERT_MAIL_INFO_INVALID);
      return;
    }

    const provider = check?.data?.data?.provider;

    if (provider && provider !== "local") {
      setIsDuplicateLogin({
        provider: provider,
        email: localLogin.email, // TODO: API 수정
        from: "local",
      });
      return;
    }

    const res = await postAuthLocalLogin({
      email: localLogin.email,
      password: localLogin.password,
    });
    if (res?.status === 404) return setValid(CERT_MAIL_INFO_UNKNOWN);
    if (res?.status === 400) return setValid(CERT_MAIL_INFO_UNKNOWN);
    await login({
      response: res,
      provider: "local",
      callback: (status) => {
        if (status === 403) {
          setPwValid(CERT_PW_INFO_INVALID);
        }
      },
    });
  };

  return (
    <>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconMail width="28" height="28" />
      </TitleIcon>
      <EmailLoginTitle>이메일로 로그인하기</EmailLoginTitle>
      <EmailLoginSubtitle>
        가입하신 이메일과 비밀번호를 입력해주세요.
      </EmailLoginSubtitle>
      <InputWrapper>
        <LabelWrapper>
          <InputLabel>메일 주소</InputLabel>
          <InfoLabel>{valid}</InfoLabel>
        </LabelWrapper>
        <InputInnerWrapper>
          <InputBox>
            <Input
              placeholder="example@wrtn.com"
              value={localLogin.email}
              onChange={(e) => localLogin.handleChangeEmail(e.target.value)}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <LabelWrapper>
          <InputLabel>비밀번호</InputLabel>
          <InfoLabel>{pwValid}</InfoLabel>
        </LabelWrapper>
        <InputInnerWrapper>
          <InputBox>
            <Input
              placeholder="비밀번호를 입력해주세요."
              type="password"
              value={localLogin.password}
              onChange={(e) => localLogin.handleChangePassword(e.target.value)}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputButton
          onClick={handleLogin}
          disabled={localLogin.password.length === 0}
        >
          계속하기
        </InputButton>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <FindWrapper>
          {/* <FindButton onClick={localLogin.handleFindEmail}>
            이메일 찾기
          </FindButton>
          <VerticalLine /> */}
          <FindButton onClick={localLogin.handleFindPassword}>
            비밀번호 찾기
          </FindButton>
        </FindWrapper>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export default EmailLoginSection;
