import {
  CERT_CODE_INFO_DEFAULT,
  CERT_CODE_INFO_INVALID,
  CERT_MAIL_INFO_DEFAULT,
  CERT_MAIL_INFO_EXPIRED,
  CERT_MAIL_INFO_GOOGLE,
  CERT_MAIL_INFO_INVALID,
  CERT_MAIL_INFO_KAKAO,
  CERT_MAIL_INFO_NAVER,
  CERT_MAIL_INFO_SEND,
  CERT_MAIL_INFO_UNKNOWN,
} from "@wrtn/core/constants/login";
import {
  getAuthCheck,
  getAuthCodePassword,
  postAuthCodePassword,
} from "@wrtn/core/services";
import { IconLock } from "@wrtn/ui/assets";
import React from "react";
import { AUTH_MAX_TIME } from "../../constants";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import { secondFormatter } from "../../utils/time";
import {
  BottomLabel,
  BottomWrapper,
  CertInputButton,
  CountText,
  EmailLoginSubtitle,
  EmailLoginTitle,
  ErrorText,
  Input,
  InputBox,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  TitleIcon,
} from "./style";

const PasswordFindSection = () => {
  const localLogin = useLocalLogin();

  const [cert, setCert] = React.useState("");
  const [time, setTime] = React.useState(-1);
  const [loading, setLoading] = React.useState(false);
  const [isFirst, setIsFirst] = React.useState(true);
  const [mailInfo, setMailInfo] = React.useState(CERT_MAIL_INFO_DEFAULT);
  const [codeInfo, setCodeInfo] = React.useState(CERT_CODE_INFO_DEFAULT);
  const sec = React.useRef(0);

  const handleClickEmail = async () => {
    setMailInfo(CERT_MAIL_INFO_DEFAULT);
    setCodeInfo(CERT_CODE_INFO_DEFAULT);
    if (!localLogin.isValidEmailFormat) {
      setMailInfo(CERT_MAIL_INFO_INVALID);
      return;
    }
    if (loading) return;
    setLoading(true);
    const check = await getAuthCheck({
      email: localLogin.email,
    });

    if (check.status === 200 && check.data.data) {
      const { provider } = check.data.data;
      if (provider !== "local") {
        if (provider === "kakao") setMailInfo(CERT_MAIL_INFO_KAKAO);
        if (provider === "google") setMailInfo(CERT_MAIL_INFO_GOOGLE);
        if (provider === "naver") setMailInfo(CERT_MAIL_INFO_NAVER);
        setLoading(false);
        return;
      }
      const res = await postAuthCodePassword({
        email: localLogin.email,
      });
      if (res?.status === 201) {
        const { data } = res.data;
        if (data === true) {
          setIsFirst(false);
          setTime(AUTH_MAX_TIME);
          sec.current = AUTH_MAX_TIME;
          setMailInfo(CERT_MAIL_INFO_SEND);
        } else {
          setMailInfo(CERT_MAIL_INFO_UNKNOWN);
        }
      } else if (res.status === 400) setMailInfo(CERT_MAIL_INFO_UNKNOWN);
    } else if (check.status === 200) {
      setMailInfo(CERT_MAIL_INFO_UNKNOWN);
    }
    setLoading(false);
  };

  const handleClickCode = async () => {
    setMailInfo(CERT_MAIL_INFO_DEFAULT);
    setCodeInfo(CERT_CODE_INFO_DEFAULT);
    if (!loading) {
      setLoading(true);
      const res = await getAuthCodePassword({
        email: localLogin.email,
        code: cert,
      });
      if (res?.status === 200) {
        const { data } = res.data;
        if (data === true) {
          setCodeInfo(CERT_CODE_INFO_DEFAULT);
          localLogin.handleFindResetPassword();
        } else {
          setCodeInfo(CERT_CODE_INFO_INVALID);
        }
      }
      setLoading(false);
    }
  };

  React.useEffect(() => {
    if (time >= 0) {
      const intervalTimer = setInterval(() => {
        if (sec.current > 0) {
          sec.current--;
          setTime(sec.current);
        } else {
          setMailInfo(CERT_MAIL_INFO_EXPIRED);
          clearInterval(intervalTimer);
        }
      }, 1000);

      return () => {
        clearInterval(intervalTimer);
      };
    }
  }, [time]);

  React.useEffect(() => {
    if (cert.length === 0) setCodeInfo(CERT_CODE_INFO_DEFAULT);
  }, [cert]);

  return (
    <>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconLock width="28" height="28" />
      </TitleIcon>
      <EmailLoginTitle>비밀번호 찾기</EmailLoginTitle>
      <EmailLoginSubtitle>
        이메일 주소로 인증 코드를 받고 비밀번호를 재설정 해주세요.
      </EmailLoginSubtitle>
      <InputWrapper>
        <InputLabel>메일 주소</InputLabel>
        <InputInnerWrapper>
          <InputBox>
            <Input
              disabled={time > 0 || loading}
              placeholder="example@wrtn.com"
              value={localLogin.email}
              onChange={(e) => localLogin.handleChangeEmail(e.target.value)}
            />
            {time > 0 && <CountText>{secondFormatter(time)}</CountText>}
            <ErrorText>{mailInfo}</ErrorText>
          </InputBox>
          <CertInputButton
            disabled={!localLogin.isValidEmailFormat || time > 0 || loading}
            onClick={handleClickEmail}
          >
            {isFirst ? "인증요청" : "재요청"}
          </CertInputButton>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputLabel>인증 코드</InputLabel>
        <InputInnerWrapper>
          <InputBox>
            <Input
              value={cert}
              onChange={(e) => setCert(e.target.value)}
              placeholder="인증코드를 입력해주세요."
              maxLength={6}
            />
            <ErrorText>{codeInfo}</ErrorText>
          </InputBox>
          <CertInputButton
            disabled={cert.length === 0 || time === 0}
            onClick={handleClickCode}
          >
            확인
          </CertInputButton>
        </InputInnerWrapper>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export default PasswordFindSection;
