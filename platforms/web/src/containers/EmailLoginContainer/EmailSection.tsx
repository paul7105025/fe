import { getAuthCheck } from "@wrtn/core/services";
import { isDuplicateLoginState } from "@wrtn/core/stores";
import { IconMail } from "@wrtn/ui/assets";
import React from "react";
import { useSetRecoilState } from "recoil";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import {
  BottomLabel,
  BottomWrapper,
  EmailLoginSubtitle,
  EmailLoginTitle,
  FindButton,
  FindWrapper,
  Input,
  InputBox,
  InputButton,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  LabelWrapper,
  TitleIcon,
} from "./style";

const EmailSection = () => {
  const localLogin = useLocalLogin();
  const setIsDuplicateLogin = useSetRecoilState(isDuplicateLoginState);

  const handleEmail = React.useCallback(async () => {
    const res = await getAuthCheck({
      email: localLogin.email,
    });

    if (res.data.data) {
      const { provider } = res.data.data;
      if (provider === "local") return localLogin.handleLoginLogin();
      if (provider !== "local")
        setIsDuplicateLogin({
          provider: provider,
          email: localLogin.email,
          from: "local",
        });

      return localLogin.handleInit();
    } else {
      localLogin.handleLoginCert();
    }
  }, [localLogin]);

  return (
    <>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconMail width="28" height="28" />
      </TitleIcon>
      <EmailLoginTitle>이메일로 시작하기</EmailLoginTitle>
      <EmailLoginSubtitle>뤼튼에 오신 것을 환영해요!</EmailLoginSubtitle>
      <InputWrapper>
        <LabelWrapper>
          <InputLabel>이메일 주소</InputLabel>
        </LabelWrapper>
        <InputInnerWrapper>
          <InputBox>
            <Input
              placeholder="example@wrtn.com"
              name="email"
              autoComplete="email"
              value={localLogin.email}
              onChange={(e) => {
                localLogin.handleChangeEmail(e.target.value);
              }}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputButton
          disabled={!localLogin.isValidEmailFormat}
          onClick={handleEmail}
        >
          계속하기
        </InputButton>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <FindWrapper>
          {/* <FindButton onClick={localLogin.handleFindEmail}>이메일 찾기</FindButton>
          <VerticalLine /> */}
          <FindButton onClick={localLogin.handleFindPassword}>
            비밀번호 찾기
          </FindButton>
        </FindWrapper>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export default EmailSection;
