import {
  CERT_CODE_INFO_DEFAULT,
  CERT_CODE_INFO_INVALID,
  CERT_MAIL_INFO_DEFAULT,
  CERT_MAIL_INFO_GOOGLE,
  CERT_MAIL_INFO_INVALID,
  CERT_MAIL_INFO_KAKAO,
  CERT_MAIL_INFO_NAVER,
  CERT_MAIL_INFO_SEND,
  CERT_MAIL_INFO_UNKNOWN,
} from "@wrtn/core/constants/login";
import {
  getAuthCheck,
  getAuthCodeEmail,
  postAuthCodePassword,
} from "@wrtn/core/services";
import { IconMail } from "@wrtn/ui/assets";
import React from "react";
import {
  AUTH_MAX_TIME,
  FIND_PHONE_INFO_DEFAULT,
  FIND_PHONE_INFO_EXPIRED,
} from "../../constants";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import { phoneRegExp } from "@wrtn/core/utils";
import { secondFormatter } from "../../utils/time";
import {
  BottomLabel,
  BottomWrapper,
  CertInputButton,
  CountText,
  EmailLoginSubtitle,
  EmailLoginTitle,
  ErrorText,
  Input,
  InputBox,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  TitleIcon,
} from "./style";

// TODO: 이메일 찾기 로직 추가?
const EmailFindSection = () => {
  const localLogin = useLocalLogin();

  const [phone, setPhone] = React.useState("");
  const [cert, setCert] = React.useState("");
  const [time, setTime] = React.useState(-1);
  const [loading, setLoading] = React.useState(false);
  const [isFirst, setIsFirst] = React.useState(true);
  const [phoneInfo, setPhoneInfo] = React.useState(FIND_PHONE_INFO_DEFAULT);
  const [mailInfo, setMailInfo] = React.useState(CERT_MAIL_INFO_DEFAULT);
  const [codeInfo, setCodeInfo] = React.useState(CERT_CODE_INFO_DEFAULT);
  const sec = React.useRef(0);

  const isValidFormat = React.useMemo(() => {
    return phoneRegExp.test(phone.toString());
  }, [phone]);

  const handleClickEmail = async () => {
    setMailInfo(CERT_MAIL_INFO_DEFAULT);
    setCodeInfo(CERT_CODE_INFO_DEFAULT);
    if (!localLogin.isValidEmailFormat) {
      setMailInfo(CERT_MAIL_INFO_INVALID);
      return;
    }
    if (!loading) {
      setLoading(true);
      // const res = await getCode({ number: number });

      // TODO: 비번 수정 인증이 아님.
      const check = await getAuthCheck({
        email: localLogin.email,
      });

      if (check.status === 200 && check.data.data) {
        const { provider } = check.data.data;
        if (provider !== "local") {
          if (provider === "kakao") setMailInfo(CERT_MAIL_INFO_KAKAO);
          if (provider === "google") setMailInfo(CERT_MAIL_INFO_GOOGLE);
          if (provider === "naver") setMailInfo(CERT_MAIL_INFO_NAVER);
          setLoading(false);
          return;
        }
        // TODO: 비번 수정 인증이 아님
        const res = await postAuthCodePassword({
          email: localLogin.email,
        });
        if (res?.status === 201) {
          const { data } = res.data;
          if (data === true) {
            setIsFirst(false);
            setTime(AUTH_MAX_TIME);
            sec.current = AUTH_MAX_TIME;
            setMailInfo(CERT_MAIL_INFO_SEND);
          } else {
            setMailInfo(CERT_MAIL_INFO_UNKNOWN);
          }
        } else if (res.status === 400) setMailInfo(CERT_MAIL_INFO_UNKNOWN);
      } else if (check.status === 200) {
        setMailInfo(CERT_MAIL_INFO_UNKNOWN);
      }
      setLoading(false);
    }
  };

  const handleClickCode = async () => {
    setPhoneInfo(FIND_PHONE_INFO_DEFAULT);
    setCodeInfo(CERT_CODE_INFO_DEFAULT);
    if (!loading) {
      setLoading(true);
      const res = await getAuthCodeEmail({
        phone: phone,
        code: cert,
      });
      if (res?.status === 200) {
        const { email, provider } = res.data.data;
        setCodeInfo(CERT_CODE_INFO_DEFAULT);
        localLogin.handleFindEmailResult();
        localLogin.handleChangeProvider(provider);
        localLogin.handleChangeEmail(email);
      } else if (res?.status === 400) {
        setCodeInfo(CERT_CODE_INFO_INVALID);
      }
      setLoading(false);
    }
  };

  React.useEffect(() => {
    if (time >= 0) {
      const intervalTimer = setInterval(() => {
        if (sec.current > 0) {
          sec.current--;
          setTime(sec.current);
        } else {
          setPhoneInfo(FIND_PHONE_INFO_EXPIRED);
          clearInterval(intervalTimer);
        }
      }, 1000);

      return () => {
        clearInterval(intervalTimer);
      };
    }
  }, [time]);

  React.useEffect(() => {
    if (cert.length === 0) setCodeInfo(CERT_CODE_INFO_DEFAULT);
  }, [cert]);

  return (
    <>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconMail />
      </TitleIcon>
      <EmailLoginTitle>이메일 주소 확인하기</EmailLoginTitle>
      <EmailLoginSubtitle>
        가입 여부 및 경로를 파악하기 위해 이메일 인증을 진행해주세요.
      </EmailLoginSubtitle>
      <InputWrapper>
        <InputLabel>메일 주소</InputLabel>
        <InputInnerWrapper>
          <InputBox>
            <Input
              disabled={time > 0 || loading}
              placeholder="example@wrtn.com"
              value={localLogin.email}
              onChange={(e) => localLogin.handleChangeEmail(e.target.value)}
            />
            {time > 0 && <CountText>{secondFormatter(time)}</CountText>}
            <ErrorText>{mailInfo}</ErrorText>
          </InputBox>
          <CertInputButton
            disabled={!localLogin.isValidEmailFormat || time > 0 || loading}
            onClick={handleClickEmail}
          >
            {isFirst ? "인증요청" : "재요청"}
          </CertInputButton>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputLabel>인증 코드</InputLabel>
        <InputInnerWrapper>
          <InputBox>
            <Input
              value={cert}
              onChange={(e) => setCert(e.target.value)}
              placeholder="인증코드를 입력해주세요."
              maxLength={6}
            />
            <ErrorText>{codeInfo}</ErrorText>
          </InputBox>
          <CertInputButton
            disabled={cert.length === 0 || time === 0}
            onClick={handleClickCode}
          >
            확인
          </CertInputButton>
        </InputInnerWrapper>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export default EmailFindSection;
