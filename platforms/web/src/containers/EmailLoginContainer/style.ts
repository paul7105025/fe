import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import styled from "styled-components";

export const InputButton = styled.button`
  width: 100%;
  border: none;
  border-radius: 8px;
  padding: 12px 0px;

  ${typo({
    size: "16px",
    weight: 500,
  })}

  text-align: center;
  background: ${colors.POINT_PURPLE};
  color: white;
  flex-grow: 1;
  cursor: pointer;
  &:disabled {
    background: ${colors.BLUE_GRAY_LINE};
    cursor: default;
  }
`;

export const CheckBox = styled(FlexButton)<{
  checked: boolean;
}>`
  width: 16px;
  height: 16px;

  cursor: pointer;

  background: ${({ checked }) => (checked ? colors.ACTION_BLUE : colors.WHITE)};
  border: 1px solid
    ${({ checked }) => (checked ? colors.ACTION_BLUE : colors.BLUE_GRAY_LINE)};
  border-radius: 2px;

  svg {
    width: 10px;
    height: 12px;
  }
`;

export const InsideWrapper = styled.div``;

export const EmailLoginTitle = styled.h4`
  ${typo({
    size: "24px",
    weight: 600,
    color: colors.GRAY_90,
  })}

  text-align: center;
  margin-bottom: 19px;
`;

export const EmailLoginSubtitle = styled.p`
  ${typo({
    size: "16px",
    weight: 500,
    color: colors.GRAY_90,
  })}

  text-align: center;
  margin-bottom: 40px;
`;

export const InputWrapper = styled.div`
  width: 100%;
  ${"" /* max-width: 380px; */}
  margin: 0px auto;
  margin-bottom: 20px;
`;

export const InputLabel = styled.p`
  ${typo({
    size: "12px",
    weight: 400,
    color: "#555555",
  })}

  margin-bottom: 8px;
  white-space: nowrap;
`;

export const InputInnerWrapper = styled.div`
  display: flex;
  gap: 8px;
  width: 100%;

  @media (max-width: 767px) {
    flex-direction: column;
    align-items: flex-end;
  }
`;

export const Input = styled.input`
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 5px;
  background: white;
  padding: 12px 16px;

  ${typo({
    size: "16px",
    weight: 500,
    color: colors.GRAY_100,
  })}

  flex-grow: 1;
  width: 100%;
  &::placeholder {
    color: ${colors.BLUE_GRAY_LINE};
  }

  &:focus {
    border: 1px solid ${colors.POINT_PURPLE};
  }

  &:disabled {
    color: ${colors.BLUE_GRAY_LINE};
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

export const ReadOnlyInput = styled(Input)`
  &:disabled {
    color: ${colors.gray_90};
  }
`;

export const CertInputButton = styled(InputButton)`
  width: 102px;
  flex-grow: 0;

  @media (max-width: 767px) {
    width: 100%;
  }
`;

export const InputBox = styled.div`
  position: relative;
  flex-grow: 1;
  width: 100%;
`;

export const ErrorText = styled.p`
  right: 0;
  bottom: 100%;
  position: absolute;
  margin-bottom: 8px;
  font-weight: 400;
  font-size: 12px;
  line-height: 100%;
  color: ${colors.POINT_PURPLE};
`;

export const CountText = styled.p`
  right: 16px;
  top: 50%;
  position: absolute;
  transform: translateY(-50%);

  ${typo({
    size: "16px",
    weight: 500,
    color: colors.POINT_PURPLE,
  })}
`;

export const TitleIcon = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 18px;
`;

export const LabelWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 5px;
`;

export const InfoLabel = styled.div`
  ${typo({
    size: "12px",
    weight: 400,
    color: colors.POINT_PURPLE,
  })}

  margin-bottom: 8px;
`;

export const BottomLabel = styled.div`
  text-align: center;
  text-decoration-line: underline;
  cursor: pointer;
  ${typo({ weight: 500, size: "14px", height: "100%", color: colors.gray_70 })};
  white-space: nowrap;
`;

export const BottomWrapper = styled.div`
  /* position: absolute; */
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 18px;
  /* bottom: 50px; */
  /* left: 50%; */
  /* transform: translateX(-50%); */
`;

export const ProviderIconWrapper = styled.div`
  position: absolute;
  top: 14px;
  left: 22px;
`;

export const VerticalLine = styled.div`
  width: 1px;
  height: 12.5px;
  background-color: ${colors.gray_70};
`;

export const FindWrapper = styled(FlexWrapper)`
  display: flex;
  flex-direction: row;
  gap: 10px;
  align-items: center;
  justify-content: center;
`;

export const FindButton = styled.div`
  ${typo({ weight: 500, size: "14px", height: "100%", color: colors.gray_70 })}
  cursor: pointer;
  white-space: nowrap;
  text-decoration-line: underline;
  text-align: center;
`;
