import {
  GoogleTool,
  IconLoginKakao,
  IconLoginNaverGreen,
  IconMail,
} from "@wrtn/ui/assets";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import { providerDuplicateMessage } from "../../utils/provider";
import {
  BottomLabel,
  BottomWrapper,
  EmailLoginSubtitle,
  EmailLoginTitle,
  InputBox,
  InputButton,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  LabelWrapper,
  ProviderIconWrapper,
  ReadOnlyInput,
  TitleIcon,
} from "./style";

const EmailResultSection = () => {
  const localLogin = useLocalLogin();

  return (
    <>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconMail width="28" height="28" />
      </TitleIcon>
      <EmailLoginTitle>이메일 안내</EmailLoginTitle>
      <EmailLoginSubtitle>
        {providerDuplicateMessage(localLogin.provider)}
      </EmailLoginSubtitle>
      <InputWrapper>
        <LabelWrapper>
          <InputLabel>메일 주소</InputLabel>
        </LabelWrapper>
        <InputInnerWrapper>
          <InputBox>
            <ProviderIconWrapper>
              {localLogin.provider === "kakao" && (
                <IconLoginKakao width="18" height="18" />
              )}
              {localLogin.provider === "naver" && (
                <IconLoginNaverGreen width="18" height="18" />
              )}
              {localLogin.provider === "google" && (
                <GoogleTool width="18" height="18" />
              )}
            </ProviderIconWrapper>
            <ReadOnlyInput
              style={{ textAlign: "center" }}
              disabled={true}
              value={localLogin.email}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputButton
          onClick={
            localLogin.provider === "local"
              ? localLogin.handleLoginEmail
              : localLogin.handleClose
          }
        >
          로그인 하러가기
        </InputButton>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <BottomLabel onClick={localLogin.handleFindPassword}>
          비밀번호 찾기
        </BottomLabel>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export default EmailResultSection;
