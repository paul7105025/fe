import { IconLock, PasswordClose, PasswordOpen } from "@wrtn/ui/assets";
import { FlexWrapper } from "@wrtn/ui/styles";
import { useRouter } from "next/router";
import React from "react";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import {
  BottomLabel,
  BottomWrapper,
  EmailLoginSubtitle,
  EmailLoginTitle,
  Input,
  InputBox,
  InputButton,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  LabelWrapper,
  TitleIcon,
} from "./style";

const PasswordSection = () => {
  const localLogin = useLocalLogin();

  const router = useRouter();

  const [visiblePW, setVisiblePW] = React.useState(true);


  React.useEffect(() => {
    if (localLogin.email.length === 0) router.push("/login/wrtn")
  }, [localLogin.email]);

  return (
    <>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconLock width="28" height="28" />
      </TitleIcon>
      <EmailLoginTitle>비밀번호 만들기</EmailLoginTitle>
      <EmailLoginSubtitle>
        비밀번호는 한 번만 입력하니 주의해서 입력해 주세요.
      </EmailLoginSubtitle>
      <InputWrapper>
        <LabelWrapper>
          <InputLabel>메일 주소</InputLabel>
        </LabelWrapper>
        <InputInnerWrapper>
          <InputBox>
            <Input
              placeholder="example@wrtn.com"
              value={localLogin.email}
              disabled={true}
              onChange={(e) => localLogin.handleChangeEmail(e.target.value)}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <LabelWrapper>
          <FlexWrapper style={{ gap: "5px" }}>
            <InputLabel>비밀번호</InputLabel>
            {visiblePW && (
              <PasswordOpen
                onClick={() => setVisiblePW((c) => !c)}
                style={{ marginBottom: "8px" }}
              />
            )}
            {!visiblePW && (
              <PasswordClose
                onClick={() => setVisiblePW((c) => !c)}
                style={{ marginBottom: "8px" }}
              />
            )}
          </FlexWrapper>
          <InputLabel>
            대소문자, 숫자, 특수문자(@$!%*#?&) 포함 15자 이내{" "}
          </InputLabel>
        </LabelWrapper>
        <InputInnerWrapper>
          <InputBox>
            <Input
              placeholder="비밀번호를 입력해주세요."
              type={visiblePW ? "password" : "text"}
              value={localLogin.password}
              onChange={(e) => localLogin.handleChangePassword(e.target.value)}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputButton
          disabled={!localLogin.isValidLoginFormat}
          onClick={localLogin.handleRegister}
        >
          계속하기
        </InputButton>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export default PasswordSection;
