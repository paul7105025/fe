import { putAuthLocalPassword } from "@wrtn/core/services";
import { IconLock, PasswordClose, PasswordOpen } from "@wrtn/ui/assets";
import { FlexWrapper } from "@wrtn/ui/styles";
import React from "react";
import { useAlert } from "react-alert";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import {
  BottomLabel,
  BottomWrapper,
  EmailLoginSubtitle,
  EmailLoginTitle,
  Input,
  InputBox,
  InputButton,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  InsideWrapper,
  LabelWrapper,
  TitleIcon,
} from "./style";

const PasswordResetSection = () => {
  const localLogin = useLocalLogin();

  const alert = useAlert();

  const [visiblePW, setVisiblePW] = React.useState(true);

  const handleResetPassword = async () => {
    const res = await putAuthLocalPassword({
      email: localLogin.email,
      password: localLogin.password,
    });
    if (res.status === 200) {
      alert.removeAll();
      alert.show("비밀번호가 성공적으로 재설정되었어요!");
      localLogin.handleChangePassword("");

      localLogin.handleLoginEmail();
    }
  };

  return (
    <InsideWrapper>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconLock width="28" height="28" />
      </TitleIcon>
      <EmailLoginTitle>비밀번호 재설정하기</EmailLoginTitle>
      <EmailLoginSubtitle>
        비밀번호는 한 번만 입력하니 주의해서 입력해 주세요.
      </EmailLoginSubtitle>
      <InputWrapper>
        <LabelWrapper>
          <InputLabel>메일 주소</InputLabel>
        </LabelWrapper>
        <InputInnerWrapper>
          <InputBox>
            <Input
              placeholder="example@wrtn.com"
              value={localLogin.email}
              disabled={true}
              onChange={(e) => localLogin.handleChangeEmail(e.target.value)}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <LabelWrapper>
          <FlexWrapper style={{ gap: "5px" }}>
            <InputLabel>새 비밀번호</InputLabel>
            {visiblePW && (
              <PasswordOpen
                onClick={() => setVisiblePW((c) => !c)}
                style={{ marginBottom: "8px" }}
              />
            )}
            {!visiblePW && (
              <PasswordClose
                onClick={() => setVisiblePW((c) => !c)}
                style={{ marginBottom: "8px" }}
              />
            )}
          </FlexWrapper>
          <InputLabel>
            대소문자, 숫자, 특수문자(@$!%*#?&) 포함 15자 이내{" "}
          </InputLabel>
        </LabelWrapper>

        <InputInnerWrapper>
          <InputBox>
            <Input
              placeholder="대소문자, 숫자, 특수문자 포함 15자 이내"
              type={visiblePW ? "password" : "text"}
              value={localLogin.password}
              onChange={(e) => localLogin.handleChangePassword(e.target.value)}
            />
          </InputBox>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputButton
          disabled={!localLogin.isValidLoginFormat}
          onClick={handleResetPassword}
        >
          비밀번호 재설정
        </InputButton>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </InsideWrapper>
  );
};

export default PasswordResetSection;
