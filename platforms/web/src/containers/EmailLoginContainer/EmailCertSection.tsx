import {
  CERT_CODE_INFO_DEFAULT,
  CERT_CODE_INFO_INVALID,
  CERT_MAIL_INFO_DEFAULT,
  CERT_MAIL_INFO_EXISTED,
  CERT_MAIL_INFO_EXPIRED,
  CERT_MAIL_INFO_INVALID,
  CERT_MAIL_INFO_SEND,
} from "@wrtn/core/constants";
import { getAuthCheck, getAuthCode, postAuthCode } from "@wrtn/core/services";
import { isDuplicateLoginState } from "@wrtn/core/stores";
import { IconMail } from "@wrtn/ui/assets";
import { useRouter } from "next/router";
import React from "react";
import { useSetRecoilState } from "recoil";
import { AUTH_MAX_TIME } from "../../constants";
import { useLocalLogin } from "../../hooks/auth/useLocalLogin";
import { secondFormatter } from "../../utils/time";
import {
  BottomLabel,
  BottomWrapper,
  CertInputButton,
  CountText,
  EmailLoginSubtitle,
  EmailLoginTitle,
  ErrorText,
  Input,
  InputBox,
  InputButton,
  InputInnerWrapper,
  InputLabel,
  InputWrapper,
  TitleIcon,
} from "./style";
import { useAlert } from "react-alert";

const EmailCertSection = () => {
  const localLogin = useLocalLogin();
  const setIsDuplicateLogin = useSetRecoilState(isDuplicateLoginState);
  const router = useRouter();
  const alert = useAlert()

  const [valid, setValid] = React.useState<boolean>(false);
  const [cert, setCert] = React.useState<string>("");
  const [time, setTime] = React.useState<number>(-1);
  const [loading, setLoading] = React.useState<boolean>(false);
  const [isFirst, setIsFirst] = React.useState<boolean>(true);
  const [mailInfo, setMailInfo] = React.useState<string>(
    CERT_MAIL_INFO_DEFAULT
  );
  const [codeInfo, setCodeInfo] = React.useState<string>(
    CERT_CODE_INFO_DEFAULT
  );
  const sec = React.useRef<number>(0);

  const handleClickEmail = async () => {
    setMailInfo(CERT_MAIL_INFO_DEFAULT);
    setCodeInfo(CERT_CODE_INFO_DEFAULT);
    setIsFirst(false);
    if (!localLogin.isValidEmailFormat) {
      setMailInfo(CERT_MAIL_INFO_INVALID);
      return;
    }
    if (loading) return;

    setLoading(true);
    const check = await getAuthCheck({
      email: localLogin.email,
    });
    if (!check || check.status !== 200) {
      setLoading(false);
      return;
    }
    const { provider } = check.data?.data || "";
    if (provider && provider !== "local") {
      setIsDuplicateLogin({
        provider: provider,
        email: localLogin.email,
        from: "local",
      });
      setLoading(false);
      return;
    }

    const res = await postAuthCode({
      email: localLogin.email,
    });
    if (res?.status === 201) {
      const { data } = res.data;
      if (data === true) {
        setIsFirst(false);
        setTime(AUTH_MAX_TIME);
        sec.current = AUTH_MAX_TIME;
        setMailInfo(CERT_MAIL_INFO_SEND);
      } else {
        setMailInfo(CERT_MAIL_INFO_EXISTED);
      }
    } else if (res.status === 400) {
      setMailInfo(CERT_MAIL_INFO_EXISTED);
    } else {
      alert.removeAll();
      alert.show("인증을 다시 시도해주세요.")
    }
    setLoading(false);
  };

  const handleClickCode = async () => {
    setCodeInfo(CERT_CODE_INFO_DEFAULT);
    if (loading) return;

    setLoading(true);
    const res = await getAuthCode({
      email: localLogin.email,
      code: cert,
    });
    if (res?.status === 200) {
      const { data } = res.data;
      if (data === true) {
        setCodeInfo(CERT_CODE_INFO_DEFAULT);
        setValid(true);
      } else {
        setCodeInfo(CERT_CODE_INFO_INVALID);
      }
    }
    setLoading(false);
  };

  const handleContinue = () => {
    if (valid) {
      localLogin.handleLoginPassword();
    }
  };

  React.useEffect(() => {
    if (time >= 0) {
      const intervalTimer = setInterval(() => {
        if (sec.current > 0) {
          sec.current--;
          setTime(sec.current);
        } else {
          setMailInfo(CERT_MAIL_INFO_EXPIRED);
          clearInterval(intervalTimer);
        }
      }, 1000);

      return () => {
        clearInterval(intervalTimer);
      };
    }
  }, [time]);

  React.useEffect(() => {
    if (cert.length === 0) setCodeInfo(CERT_CODE_INFO_DEFAULT);
  }, [cert]);

  React.useEffect(() => {
    if (localLogin.email.length === 0) router.push("/login/wrtn")
  }, []);

  return (
    <>
      <div style={{ flex: 1 }} />
      <TitleIcon>
        <IconMail />
      </TitleIcon>
      <EmailLoginTitle>이메일 인증하기</EmailLoginTitle>
      <EmailLoginSubtitle>
        가입을 위해 이메일 인증을 진행해주세요.
      </EmailLoginSubtitle>
      <InputWrapper>
        <InputLabel>이메일</InputLabel>
        <InputInnerWrapper>
          <InputBox>
            <Input
              disabled={time > 0 || loading}
              placeholder="example@wrtn.com"
              value={localLogin.email}
              onChange={(e) => localLogin.handleChangeEmail(e.target.value)}
            />
            {time > 0 && <CountText>{secondFormatter(time)}</CountText>}
            {!isFirst && <ErrorText>{mailInfo}</ErrorText>}
          </InputBox>
          <CertInputButton
            disabled={!localLogin.isValidEmailFormat || time > 0 || loading}
            onClick={handleClickEmail}
          >
            {isFirst ? "인증요청" : "재요청"}
          </CertInputButton>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputLabel>인증 코드</InputLabel>
        <InputInnerWrapper>
          <InputBox>
            <Input
              disabled={valid}
              value={cert}
              onChange={(e) => setCert(e.target.value)}
              placeholder="123456"
              maxLength={6}
            />
            <ErrorText>{codeInfo}</ErrorText>
          </InputBox>
          <CertInputButton
            disabled={cert.length === 0 || time === 0 || valid}
            onClick={handleClickCode}
          >
            확인
          </CertInputButton>
        </InputInnerWrapper>
      </InputWrapper>
      <InputWrapper>
        <InputButton disabled={!valid} onClick={handleContinue}>
          계속하기
        </InputButton>
      </InputWrapper>
      <div style={{ flex: 1 }} />
      <BottomWrapper>
        <BottomLabel onClick={localLogin.handleClose}>
          다른 계정으로 로그인
        </BottomLabel>
      </BottomWrapper>
    </>
  );
};

export default EmailCertSection;
