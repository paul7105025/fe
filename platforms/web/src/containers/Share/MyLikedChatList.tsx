import { useEffect, useState } from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import { ShareChatType, getMyLikeChats, userState } from "@wrtn/core";
import { FlexWrapper, newColors, typo } from "@wrtn/ui";
import { useSearchParams } from "next/navigation";
import Pagination from "src/components/Share/PaginationNew";
import { Spinner } from "@wrtn/ui/components/Spinner";
import ChatCardItem from "src/components/Share/ChatCardItem";

export default function MyLikedChatList() {
  const user = useRecoilValue(userState);
  const [chats, setChats] = useState<ShareChatType[] | null>(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [showNeedLogin, setShowNeedLogin] = useState(false)
  const searchParams = useSearchParams();

  useEffect(() => {
    fetchMyLikeChats();
  })

  const fetchMyLikeChats = async () => {
    try {
      const page = searchParams.get("page") ?? undefined;
      const res = await getMyLikeChats(page, "");

      if (res?.data?.data) {
        setChats(res.data.data.chats);
        setTotalPage(res.data.data.totalCount);
      }
    } catch(err: any) {
      if (err?.response?.status === 401) {
        setShowNeedLogin(true)
      }
    }
  }

  const onChangePage = async (page: number) => {
    try {
      const res = await getMyLikeChats(`${page}`);

      if (res?.data?.data) {
        setChats(res.data.data.chats);
        setTotalPage(res.data.data.totalCount);
        setCurrentPage(page);
      }
    } catch(err) {
      // console.log(err);
    }
  }

  if (!user || showNeedLogin) {
    return (
      <NoChatWrapper>로그인이 필요한 서비스입니다.</NoChatWrapper>
    )
  }

  return (
    <>
      {chats ? (
        <>
        {chats.length === 0 && <NoChatWrapper>공유된 채팅이 없습니다.</NoChatWrapper>}
        {chats.map((chat) => {
          return <ChatCardItem key={chat._id} chat={chat} />
        })}
        </>
      ) : (
        <Spinner width={22} height={22} color={newColors.PURPLE_500_PRIMARY} />
      )}
      <PaginationArea>
        {chats && <Pagination currentIndex={Number(currentPage)} handlePage={onChangePage} maxIndex={Math.floor((totalPage-1)/20)}/>}
      </PaginationArea>
    </>
  )
}

const NoChatWrapper = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    height: "180%",
    color: "#A1A5B9",
  })}
`

const PaginationArea = styled(FlexWrapper)`
  margin-bottom: 30px;
`
