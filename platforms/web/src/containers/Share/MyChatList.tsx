"use client"
import { useEffect, useState } from 'react'
import styled from 'styled-components';
import { useSearchParams } from 'next/navigation';
import { ShareChatType, getMyChats, putShareChat } from '@wrtn/core'
import { FlexWrapper, newColors, typo } from '@wrtn/ui';
import { Spinner } from '@wrtn/ui/components/Spinner';
import ChatCardItem from '../../components/Share/ChatCardItem';
import Pagination from "src/components/Share/PaginationNew";

export default function MyChatList() {
  const [chats, setChats] = useState<ShareChatType[] | null>(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [showNeedLogin, setShowNeedLogin] = useState(false)
  const searchParams = useSearchParams();

  useEffect(() => {
    fetchMyChats();
  }, [])

  const fetchMyChats = async () => {
    try {
      const keyword = searchParams.get("keyword") ?? undefined;
      const page = searchParams.get("page") ?? undefined;
      const res = await getMyChats(page, keyword);

      setChats(res.data.data.chats);
      setTotalPage(res.data.data.totalCount);
    } catch(err: any) {
      if (err?.response?.status === 401) {
        setShowNeedLogin(true)
      }
    }
  }

  const onChangePage = async (page: number) => {
    try {
      const res = await getMyChats(`${page}`);
      setChats(res.data.data.chats);
      setTotalPage(res.data.data.totalCount);
      setCurrentPage(page);
    } catch(err) {
      // console.log(err);
    }
  }

  const removeChat = async (chatId: string) => {
    if (!chats) return

    try {
      const chat = chats.find(chat => chat._id === chatId);
      if (!chat) return

      await putShareChat(chatId, { isDeleted: true, originalId: chat.originalId });
      const newChats = chats.filter(chat => chat._id !== chatId);
      setChats(newChats);
    } catch(err) {
      // 에러발생...
    }
  }

  const editChat = async (chatId: string, newTopic: string) => {
    if (!chats) return

    try {
      await putShareChat(chatId, { topic: newTopic });
      const newChats = chats.map(chat => chat._id === chatId ? { ...chat, topic: newTopic } : chat);
      setChats(newChats);
    } catch(err) {
      // 에러발생..
    }
  }

  if (showNeedLogin) {
    return (
      <NoChatWrapper>로그인이 필요한 서비스입니다.</NoChatWrapper>
    )
  }

  return (
    <>
      {chats ? (
        <>
        {chats.length === 0 && <NoChatWrapper>공유된 채팅이 없습니다.</NoChatWrapper>}
        {chats.map((chat) => {
          return <ChatCardItem key={chat._id} chat={chat} editable={true} removeChat={removeChat} editChat={editChat}/>
        })}
        </>
      ) : (
        <Spinner width={22} height={22} color={newColors.PURPLE_500_PRIMARY} />
      )}
      <PaginationArea>
        {chats && <Pagination currentIndex={Number(currentPage)} handlePage={onChangePage} maxIndex={Math.floor((totalPage-1)/20)}/>}
      </PaginationArea>
    </>
  )
}

const NoChatWrapper = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    height: "180%",
    color: "#A1A5B9",
  })}
`

const PaginationArea = styled(FlexWrapper)`
  margin-bottom: 30px;
`