import React from 'react'
import styled from 'styled-components'
import { FlexWrapper, typo, colors, newColors } from '@wrtn/ui'
import SearchInput from '../../components/Share/SearchInput'

export default function SearchArea() {
  return (
    <ShareSearch>
      <LogoArea>
        <ShareLogo>공유</ShareLogo>
        <TrandLogo>#트렌드</TrandLogo>
      </LogoArea>
      <SearchInput />
    </ShareSearch>
  )
}

const ShareSearch = styled(FlexWrapper)`
  flex-direction: row;
  gap: 13px;

  @media (max-width: 767px) {
    width: 90%;
  }
`

const LogoArea = styled.div`
  @media (max-width: 767px) {
    display: none;
  }
`

const ShareLogo = styled.span`
  margin-right: 5px;
  ${typo({
    weight: 800,
    size: "17px",
    color: colors.GRAY_80,
  })}
`

const TrandLogo = styled.span`
  background-color: ${newColors.PURPLE_500_PRIMARY};
  padding: 7.5px 12px 7.5px 10px;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  border-bottom-right-radius: 20px;

  ${typo({
    weight: 800,
    size: "17px",
    color: colors.WHITE,
  })}
`
