"use client"
import styled from 'styled-components'
import React, { useEffect, useState } from 'react'

import { FlexWrapper } from '@wrtn/ui'
import CategoryChip from '../../components/Share/CategoryChip'
import useQueryString from 'src/hooks/useQueryString';
import { useEvent } from '@wrtn/core'
import { useSearchParams } from 'next/navigation'

export default function CategoryList() {
  const [category, setCategory] = useState<string | null>(null);
  const [keyword, setKeyword] = useState<string | null>(null);
  const { onChangeQuery } = useQueryString();
  const searchParams = useSearchParams();
  const { collectEvent } = useEvent();

  useEffect(() => {
    const _category = searchParams.get("category");
    const _keyword = searchParams.get("keyword");

    setCategory(_category && _category !== "" ? _category : null);
    setKeyword(_keyword && _keyword !== "" ? _keyword : null);
  }, [searchParams])

  useEffect(() => {
    if (category) {
      let kor = ""
      switch (category) {
        case "my": kor = "내 게시물"
          break;
        case "latest": kor = "최신게시물"
          break;
        case "popular": kor = "실시간 인기"
          break;
        case "mylikes": kor = "좋아요"
          break;
        default: kor = "";
      }
      if (kor !== "") {
        collectEvent("click_share_trend_menu", {
          share_trend_menu: kor,
        })
      }
    }
  }, [category]);

  const isDefaultForNoKeyword = !category && !keyword;
  const isDefaultForKeyword = !category && !!keyword;

  return (
    <CategoryWrapper>
      <CategoryChip
        label="#실시간 인기"
        isSelected={category === "popular" || isDefaultForNoKeyword}
        onClick={() => onChangeQuery("category", "popular")}
      />
      <CategoryChip 
        label="최신 게시물"
        isSelected={category === "latest" || isDefaultForKeyword}
        onClick={() => onChangeQuery("category", "latest")}
      />
      <CategoryChip
        label="내 게시물"
        isSelected={category === "my"}
        onClick={() => onChangeQuery("category", "my")}
      />
      <CategoryChip
        label="좋아요"
        isSelected={category === "mylikes"}
        onClick={() => onChangeQuery("category", "mylikes")}
      />
    </CategoryWrapper>
  )
}

const CategoryWrapper = styled(FlexWrapper)`
  margin-top: 50px;
  margin-bottom: 24px;
  gap: 12px;

  @media (max-width: 767px) {
    margin-top: 40px;
    z-index: 9;
    position: sticky;
    top: 25px;
    width: 100%;
  }
`
