import React from "react";
import dayjs from "dayjs";
import { useAlert } from "react-alert";
import styled from "styled-components";
import { useRecoilValue } from "recoil";
import NaverLogin from "@wrtn/core/utils/login/naver";

import {
  withdrawSurvey,
  withdrawDetailSurvey,
  useTagManager,
  deleteUser,
  cancelSubscribeSurvey,
  deleteUserSubscribe,
  postWithdrawUser,
  postWithdrawSubscription,
  deleteUserNaver,
  userState,
} from "@wrtn/core";
import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";

import useRevoke from "@wrtn/core/hooks/login/useRevoke";

import { Dialog, DialogWithSelect } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import { boxShadow, colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import { IconLoginNaver } from "@wrtn/ui/assets";
import { useRefresh, useEvent } from "@wrtn/core/hooks";
import { useRouter } from "next/navigation";

type withdrawDialogStepType =
  | "WITHDRAW_START"
  | "WITHDRAW_REASON"
  | "WITHDRAW_DETAIL_REASON"
  | "WITHDRAW_CONFIRM"
  | "WITHDRAW_END";

const NAVER_CLIENT_ID = process.env.NEXT_PUBLIC_NAVER_CLIENT_ID;

const parseJwt = (token) => {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  var jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
};

export const WithdrawDialogContainer = ({ user, onClose }) => {
  const tagManager = useTagManager();
  const router = useRouter();
  const revoke = useRevoke();
  const alert = useAlert();
  const { collectEvent } = useEvent();

  const [dialogStep, setDialogStep] =
    React.useState<withdrawDialogStepType>("WITHDRAW_START");

  const [dialogReasonData, setDialogReasonData] = React.useState({
    main: "",
    sub: "",
  });
  const [dialogDetailReasonData, setDialogDetailReasonData] = React.useState({
    main: "",
    sub: "",
  });

  const GOOGLE_CLIENT_ID = process.env.NEXT_PUBLIC_URL_GOOGLE_CLIENT_ID;

  const goNextStep = (step: withdrawDialogStepType) => {
    setDialogStep(step);
  };

  const handleGoogleLoginFailure = () => {
    alert.show("구글 인증에 실패했어요. 다시 시도해주세요.");
    onClose();
  };

  const handleGoogleLoginSuccess = async (val: any) => {
    // 계정 동일한지 확인
    const payload = parseJwt(val.credential);

    if (payload.email === user.email) {
      // revoke(user).then(() => navigate("/app/login"));
      await handleWithdrawal(payload.sub);
      goNextStep("WITHDRAW_END");
      collectEvent("withdraw_done", {
        withdraw_reason: dialogReasonData.main,
        withdraw_reason_detail_1: dialogDetailReasonData.main,
        withdraw_reason_detail_2: dialogDetailReasonData.sub,
      });
    } else {
      alert.show("구글 인증에 실패했어요. 다시 시도해주세요.");
      onClose();
    }
  };

  const handleNaverLoginFailure = () => {
    alert.show("네이버 인증에 실패했어요. 다시 시도해주세요.");
    onClose();
  };

  const handleNaverLoginSuccess = async ({ accessToken }) => {
    const surveyRes = await postWithdrawUser({
      mainReason: dialogReasonData.main,
      subReason: dialogDetailReasonData.main,
      subReasonDetail: dialogDetailReasonData.sub,
    });

    if (!surveyRes.data.data) {
      throw new Error("탈퇴 실패");
    }

    const token = accessToken.toString();
    const tokenWithoutBearer = token.substring(token.indexOf(".") + 1);
    const res = await deleteUserNaver({ token: tokenWithoutBearer });
    if (res?.data.data) {
      tagManager({
        event: "click_onboarding_next_btn",
        data: {
          withdraw_date: new Date(),
        },
      });
      collectEvent("withdraw_done", {
        withdraw_reason: dialogReasonData.main,
        withdraw_reason_detail_1: dialogDetailReasonData.main,
        withdraw_reason_detail_2: dialogDetailReasonData.sub,
      });
      window.localStorage.clear();
      revoke(user).then(async () => {
        goNextStep("WITHDRAW_END");
        router.push("/app/revoke?provider=naver");
      });
    }
  };

  const handleWithdrawal = async (sub: string) => {
    const surveyRes = await postWithdrawUser({
      mainReason: dialogReasonData.main,
      subReason: dialogDetailReasonData.main,
      subReasonDetail: dialogDetailReasonData.sub,
    });

    if (!surveyRes.data.data) {
      throw new Error("탈퇴 실패");
    }

    const res = await deleteUser();
    if (res.data.data) {
      tagManager({
        event: "click_onboarding_next_btn",
        data: {
          withdraw_date: new Date(),
        },
      });
      window.localStorage.clear();
      router.push("/app/revoke");
      revoke(user, sub).then(async () => {});
    }
  };

  return (
    <ModalPortal>
      {dialogStep === "WITHDRAW_START" && (
        <Dialog
          iconType="crying"
          rightButtonLabel="탈퇴하기"
          title="회원 탈퇴를 진행하시겠어요?"
          description={[
            "탈퇴하시면 모든 활동 기록이 삭제되며,",
            "삭제된 기록은 복구할 수 없습니다.",
          ]}
          handleRightButton={() => {
            tagManager({ event: "click_withdraw_btn" });
            collectEvent("click_withdraw_btn");
            goNextStep("WITHDRAW_REASON");
          }}
          handleClose={onClose}
          isSimpleButton
        />
      )}
      {dialogStep === "WITHDRAW_REASON" && (
        <DialogWithSelect
          data={withdrawSurvey}
          selected={dialogReasonData}
          setSelected={setDialogReasonData}
          width={442}
          rightButtonLabel="제출하기"
          handleRightButton={() => {
            tagManager({
              event: "click_withdraw_survey",
              data: {
                withdraw_type_first: dialogDetailReasonData.main,
              },
            });

            collectEvent("click_withdraw_reason", {
              withdraw_reason: dialogReasonData.main,
            });
            if (dialogReasonData.main === "계정이 여러개에요") {
              goNextStep("WITHDRAW_CONFIRM");
            } else {
              goNextStep("WITHDRAW_DETAIL_REASON");
            }
          }}
          handleClose={onClose}
        />
      )}
      {dialogStep === "WITHDRAW_DETAIL_REASON" && (
        <DialogWithSelect
          data={withdrawDetailSurvey}
          selected={dialogDetailReasonData}
          setSelected={setDialogDetailReasonData}
          width={560}
          rightButtonLabel="제출하기"
          handleRightButton={() => {
            tagManager({
              event: "click_withdraw_form",
              data: {
                withdraw_type_first: dialogDetailReasonData.main,
                withdraw_type_second: dialogDetailReasonData.sub,
              },
            });
            goNextStep("WITHDRAW_CONFIRM");
          }}
          handleClose={onClose}
        />
      )}
      {dialogStep === "WITHDRAW_CONFIRM" && (
        <Dialog
          width={395}
          rightButtonLabel="탈퇴하기"
          title="탈퇴 전에 한 번 더 확인해주세요!"
          description={[
            "탈퇴 시 생성한 결과물 히스토리를 포함한\n모든 활동 기록이 삭제되며,\n 삭제된 기록은 복구할 수 없습니다.",

            "탈퇴 시 구독하고 있는 요금제에 대한\n 환불은 불가능합니다.",

            "탈퇴일로부터 30일 이전에는 같은 계정\n(동일 이메일 주소 및 로그인 경로)으로\n재가입 할 수 없습니다.",
          ]}
          handleRightButton={() => {
            tagManager({
              event: "click_withdraw_notice",
              data: {
                withdraw_date: dayjs().format("YYYY-MM-DD"),
                withdraw_type_first: dialogDetailReasonData.main,
                withdraw_type_second: dialogDetailReasonData.sub,
              },
            });
            if (user?.provider !== "google" || user?.provider !== "naver") {
              goNextStep("WITHDRAW_END");
              collectEvent("withdraw_done", {
                withdraw_reason: dialogReasonData.main,
                withdraw_reason_detail_1: dialogDetailReasonData.main,
                withdraw_reason_detail_2: dialogDetailReasonData.sub,
              });
              handleWithdrawal("");
            }
          }}
          handleClose={onClose}
          isList
          isSimpleButton
          isDisabled={user?.provider === "google" || user?.provider === "naver"}
        >
          {/* 구글 회원탈퇴 컴포넌트 추가 */}
          {user?.provider === "google" && (
            <GoogleOAuthProvider clientId={String(GOOGLE_CLIENT_ID)}>
              <GoogleWrapper>
                <Divider />
                <GoogleText>구글 계정으로 회원가입하신 경우,</GoogleText>
                <GoogleText>연동 정보를 초기화하기 위해 계정 인증을</GoogleText>
                <GoogleText style={{ paddingBottom: "25px" }}>
                  해야합니다. 아래 버튼을 눌러 인증해주세요.
                </GoogleText>

                <GoogleLogin
                  onSuccess={handleGoogleLoginSuccess}
                  onError={handleGoogleLoginFailure}
                  width={"300"}
                />
              </GoogleWrapper>
            </GoogleOAuthProvider>
          )}
          {user?.provider === "naver" && (
            <GoogleWrapper>
              <Divider />
              <GoogleText>네이버 계정으로 회원가입하신 경우,</GoogleText>
              <GoogleText>연동 정보를 초기화하기 위해 계정 인증을</GoogleText>
              <GoogleText style={{ paddingBottom: "25px" }}>
                해야합니다. 아래 버튼을 눌러 인증해주세요.
              </GoogleText>

              <NaverLogin
                clientId={NAVER_CLIENT_ID}
                callbackUrl={`${window.location.origin}/app/login/`}
                onSuccess={handleNaverLoginSuccess}
                onFailure={handleNaverLoginFailure}
                isSignedIn={true}
                render={(props) => (
                  <LoginLink onClick={() => props.onClick()} type="naver">
                    <IconLoginNaver width="14" height="14" />
                    네이버 계정 인증
                  </LoginLink>
                )}
              />
            </GoogleWrapper>
          )}
        </Dialog>
      )}
    </ModalPortal>
  );
};

type cancelDialogStepType = "CANCEL_START" | "CANCEL_REASON" | "CANCEL_CONFIRM";

export const CancelSubscribeDialogContainer = ({ onClose }) => {
  const alert = useAlert();
  const tagManager = useTagManager();

  const user = useRecoilValue(userState);

  const { collectEvent } = useEvent();

  const [dialogStep, setDialogStep] =
    React.useState<cancelDialogStepType>("CANCEL_START");

  const [dialogReasonData, setDialogReasonData] = React.useState({
    main: "",
    sub: "",
  });

  const goNextStep = (step: cancelDialogStepType) => {
    setDialogStep(step);
  };

  const cancelPlan = async () => {
    const surveyRes = await postWithdrawSubscription({
      mainReason: dialogReasonData.main,
      mainReasonDetail: dialogReasonData.sub,
    });

    if (!surveyRes.data.data) {
      throw new Error("탈퇴 실패");
    }

    let is_first_payment = false;

    tagManager({
      event: "subscription_cancel_form",
      data: {
        plan_category: user?.plan,
        is_first_payment: is_first_payment,
        subscription_cancel_type: dialogReasonData.main,
        subscription_cancel_date: dayjs().format("YYYY-MM-DD"),
      },
    });

    const res2 = await deleteUserSubscribe();
    if (res2?.status === 200) {
      collectEvent("cancel_purchase_done", {
        current_plan: user?.plan,
        cancel_purchase_reason: dialogReasonData.main,
      });
      onClose();
      alert.show("구독이 취소되었습니다.");
      // fetchMetric();
    }
  };

  return (
    <ModalPortal>
      {dialogStep === "CANCEL_START" && (
        <Dialog
          iconType="crying"
          rightButtonLabel="구독 취소하기"
          title="구독을 취소하시겠어요?"
          description={[
            "구독을 취소하면 현재 사용하고 계시는 기능 및",
            "속도에 제한이 생겨요! 그래도 취소하시겠어요?",
          ]}
          handleRightButton={() => goNextStep("CANCEL_REASON")}
          handleClose={onClose}
          isSimpleButton
        />
      )}
      {dialogStep === "CANCEL_REASON" && (
        <DialogWithSelect
          data={cancelSubscribeSurvey}
          selected={dialogReasonData}
          setSelected={setDialogReasonData}
          width={560}
          rightButtonLabel="제출하기"
          handleRightButton={() => goNextStep("CANCEL_CONFIRM")}
          handleClose={onClose}
        />
      )}
      {dialogStep === "CANCEL_CONFIRM" && (
        <Dialog
          rightButtonLabel="구독 취소하기"
          title="구독 취소 전에 한 번 더 확인해주세요!"
          description={[
            "구독을 취소하셔도 자동 환불은 진행되지 않고,\n표기된 구독 종료일까지 요금제가 유지됩니다.\n환불을 원하실 경우, 사용자가이드 → 고객 문의/상담을 통해 신청해주세요.",
            "구독 취소를 하더라도 구독 종료일 전까지 언제든지\n구독을 재개할 수 있습니다.",
          ]}
          handleRightButton={cancelPlan}
          handleClose={onClose}
          isList
          isSimpleButton
        />
      )}
    </ModalPortal>
  );
};

const GoogleWrapper = styled(FlexWrapper)`
  flex-direction: column;
`;

const GoogleText = styled.div`
  ${typo({
    weight: 500,
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};
  text-align: center;
`;

const Divider = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${colors.gray_30};
  margin-bottom: 25px;
`;

const LoginLink = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 48px;
  cursor: pointer;
  background: ${({ type }) => {
    if (type === "naver") return colors.BRAND_NAVER_MAIN;
    else if (type === "local") return colors.WHITE;
    return "";
  }};
  height: 38px;
  border-radius: 5px;
  width: 300px;
  max-width: 400px;
  min-width: 200px;
  padding: 0px 25px;
  cursor: pointer;
  ${typo({ weight: 600, size: "14px", height: "100%" })};
  color: ${({ type }) => {
    if (type === "naver") return colors.WHITE;
    else if (type === "local") return colors.gray_80;
    return "";
  }};
  ${({ type }) => type === "local" && boxShadow.login_box_shadow};

  @media (max-width: 480px) {
    font-size: 14px;
  }
`;
