import { currentStoreDetailToolFormsState, StoreToolFormInputType, StoreToolType } from "@wrtn/core";
import React from "react";
import { DynamicDropdownWithLabel, DynamicInputWithLabel, DynamicOptionWithLabel, DynamicTextAreaWithLabel } from "src/components";
import { useRecoilState } from 'recoil';

interface StoreDetailToolInputsProps {
  tool: StoreToolType
}

export const StoreDetailToolInputs = ({ tool }: StoreDetailToolInputsProps) => {
  const [currentStoreDetailToolForms, setCurrentStoreDetailToolForms] = useRecoilState(currentStoreDetailToolFormsState);

  const handleChangeInput = (idx: number, text: string) => {
    setCurrentStoreDetailToolForms((prev: StoreToolFormInputType[]) => [
      ...prev.slice(0, idx),
      {
        ...prev[idx],
        data: {
          ...prev[idx].data,
          value: text,
        },
      },
      ...prev.slice(idx + 1),
    ]);
  };


  return (
    <div>
      {
        currentStoreDetailToolForms?.map((item, idx) => {
          if (item.type === "input") {
            const { placeholder, description, label, value } =
              item.data ;
            return (
              <DynamicInputWithLabel
                key={item.id}
                label={label}
                description={description || ""}
                placeholder={placeholder || ""}
                value={value}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  handleChangeInput(idx, e.target.value)
                }}
              />
            );
          } else if (item.type === "textarea") {
            const { placeholder, description, label, value } =
              item.data 
              // as TextAreaForm;
            return (
              <DynamicTextAreaWithLabel
                key={item.id}
                label={label}
                description={description || ""}
                placeholder={placeholder || ""}
                value={value}
                onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                  // handleChange(idx, e.target.value)
                  handleChangeInput(idx, e.target.value)
                }}
              />
            );
          } else if (item.type === "option") {
            // @ts-ignore
            const { options, description, label, value } =
              item.data 
              // as OptionForm;
            return (
              <DynamicOptionWithLabel
                key={item.id}
                label={label}
                description={description || ""}
                options={options || []} // 옵션으로 바뀌게
                value={value}
                onChange={(value: string) => {
                  handleChangeInput(idx, value)
                }}
              />
            );
          } else if (item.type === "dropdown") {
            // @ts-ignore
            const { options, description, label, value } =
              item.data 
              // as DropdownForm;
            return (
              <DynamicDropdownWithLabel
                key={item.id}
                label={label}
                value={value}
                description={description || ""}
                options={options || []} // 옵션으로 바뀌게
                onChange={(value: string) => {
                  handleChangeInput(idx, value)
                }}
              />
            );
          } else {
            return <React.Fragment key={idx} />;
          }
        }
      )
    }
    </div>
  )
}

export default StoreDetailToolInputs;