import { StoreToolType } from "@wrtn/core";
import { colors, FlexWrapper, newColors, newTypo, STORE_ICON_SELECTOR, TOOL_ICON_SELECTOR } from "@wrtn/ui";
import styled from 'styled-components';

interface StoreDetailToolFormInfoProps {
  tool: StoreToolType;
}

export const StoreDetailToolFormInfo = ({ tool }: StoreDetailToolFormInfoProps) => {
  return (
    <>
      <FlexWrapper style={{gap: '13px', marginBottom: '19px'}} align="center">
        {STORE_ICON_SELECTOR[tool.icon]}
        <Title>{tool.title}</Title>
      </FlexWrapper>
      <Desc>
        {tool.description}
      </Desc>
      <CategoryWrapper>
        {
          tool.category.map(v => (
            <Category key={v}>
              {v}
            </Category>
          ))
        }
      </CategoryWrapper>
    </>
  )
}

export default StoreDetailToolFormInfo;

const Desc = styled.div`
  ${newTypo("body-16-med")};
  color: ${newColors.GRAY_700};
  padding-bottom: 20px;
`;

const CategoryWrapper = styled(FlexWrapper)`
  gap: 8px;
  padding-bottom: 29px;
`;

const Category = styled.div`
  color: ${colors.gray_70};
  ${newTypo("content-14-med")};
  padding: 5px 9px;
  border: 1px solid ${newColors.GRAY_300};
  background: ${newColors.WHITE};
  border-radius: 5px;
`;

const Title = styled.div`
  ${newTypo("heading-22-semi")};
  color: ${newColors.GRAY_800};
`;