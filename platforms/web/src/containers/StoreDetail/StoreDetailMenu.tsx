import { currentStoreTabIdState, storeAPI, useEvent } from "@wrtn/core";
import { colors, FlexWrapper, newColors, newTypo } from "@wrtn/ui";
import { useRouter } from "next/router";
import React from "react";
import { StoreCardMinimum } from "src/components/Card";
import { Tool } from "src/components/Dynamic/types";
import { useStoreRecommendToolList, useStoreRelatedToolList } from "src/hooks/swr";
import styled from "styled-components";
import { useRecoilValue } from 'recoil';

export const StoreDetailMenu = ({ tool }) => {
  const router = useRouter();
  const { collectEvent } = useEvent();
  const currentStoreTabId = useRecoilValue(currentStoreTabIdState);

  const {
    storeRecommendToolList
  } = useStoreRecommendToolList();

  const {
    storeRelatedToolList
  } = useStoreRelatedToolList({ toolId: tool._id });

  const handleClickRelatedCard = (card) => {
    collectEvent("click_store_card", {
      click_position: "related",
      feature_menu: "store_tool",
      feature_category: card.category,
      feature_name: card.title,
      feature_form: "tool",
      prompt_id: card._id,
      views: card.views,
      favorites: card.likes,
      runs: card.usedNum,
      downloads: 0, // TODO
      is_updated: false, // TODO
      maker_id: card.userId
    })
    router.push(`/store/details/${card._id}`)
  }

  const handleClickRecommendCard = (card) => {
    collectEvent("click_store_card", {
      click_position: "recommended",
      feature_menu: "store_tool",
      feature_category: card.category,
      feature_name: card.title,
      feature_form: "tool",
      prompt_id: card._id,
      views: card.views,
      favorites: card.likes,
      runs: card.usedNum,
      downloads: 0, // TODO
      is_updated: false, // TODO
      maker_id: card.userId
    })
    router.push(`/store/details/${card._id}`)
  }

  return (
    <Wrapper column align="flex-start">
      <Title>👉 연관 있는 툴/챗봇</Title>
      <CardList column>
        {
          storeRelatedToolList.map((v) => (
            <StoreCardMinimum 
              key={v._id}
              onClick={() => { handleClickRelatedCard(v) }}
              type={"tool"}
              icon={v.icon}
              title={v.title}
            />
          ))
        }
      </CardList>
      { !currentStoreTabId &&
        <>
          <Title>💜 추천 툴/챗봇</Title>
          <CardList column>
            {
              storeRecommendToolList.map(v => (
                <StoreCardMinimum 
                  key={v._id}
                  onClick={() => { handleClickRecommendCard(v) }}
                  type={"tool"}
                  icon={v.icon}
                  title={v.title}
                />
              ))
            }
          </CardList>
        </>
        }
    </Wrapper>
  )
}

export default StoreDetailMenu;

const Wrapper = styled(FlexWrapper)`
  background-color: ${newColors.BLUE_200_BG};
  width: 100%;
  padding: 47px 34px 0px 31px;
  max-width: 362px;
  height: 100%;
  overflow: scroll;
  border-left: 1px solid ${newColors.GRAY_100};

  @media (max-width: 1024px) {
    max-width: none;
    overflow: initial;
  }
`;

const CardList = styled(FlexWrapper)`
  gap: 16px;
  width: 100%;
  padding-bottom: 60px;
`;

const Title = styled.div`
  ${newTypo("content-20-semi")};
  color: ${newColors.GRAY_700};
  padding-bottom: 32px;
`;