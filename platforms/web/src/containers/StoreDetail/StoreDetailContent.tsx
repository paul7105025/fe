import { favoriteStoreToolListState, storeAPI, StoreToolType } from "@wrtn/core";
import { FlexWrapper, newColors } from "@wrtn/ui";
import styled from "styled-components";
import { StoreDetailCardList } from "./StoreDetailCardList";
import { StoreDetailInfo } from "./StoreDetailInfo";
import { StoreDetailPreview } from "./StoreDetailPreview";
import { useRecoilState } from 'recoil';
import { useFavoriteStoreToolIdList, useAuthorToolList } from "src/hooks/swr";
import { useRouter } from "next/router";
import { DetailInfo } from "src/components";

interface StoreDetailContent {
  tool: StoreToolType;
  // authorToolList: StoreToolType[];
}
export const StoreDetailContent = ({ tool }: StoreDetailContent) => {
  const {
    favoriteToolIdList,
    append,
    remove
  } = useFavoriteStoreToolIdList();

  const {
    authorToolList,
  } = useAuthorToolList({ authorId: tool.userId });

  const handleClickFavorite = async (toolId: string) => {
    const favIndex = favoriteToolIdList.findIndex(w => w === toolId);

    if (favIndex >= 0) {
      const res = await storeAPI.deleteStoreFavorite_Id({
        toolId
      });
      if (res.status === 200) {
        append(toolId);
      }
    } else {
      const res = await storeAPI.postStoreFavorite_Id({
        toolId
      });
      if (res.status === 201) {
        remove(toolId);
      }
    }
  }

  return (
    <Wrapper column align="center">
      {/* store detail preview */}
      <StoreDetailPreview tool={tool}/>
      {/* divider */}
      <Divider />
      {/* store detail info */}
      <DetailInfo
        key={tool._id}
        icon={tool.icon}
        title={tool.title}
        type={"tool"}
        description={tool.description}
        author={tool?.userName || ""}
        createdAt={tool.createdAt}
        viewCount={tool.views}
        isFavorite={favoriteToolIdList.includes(tool._id)}
        genCount={tool.usedNum}
        // exportCount={tool.}
        favoriteCount={tool.likes}
        category={tool?.category || []}
        onClick={() => {}}
        onClickExport={() => {}}
        onClickFavorite={() => { handleClickFavorite(tool._id) }}
      />
      {/* store detail card list */}
      <StoreDetailCardList 
        title={`${tool?.userName || ""}의 툴/봇`}
        toolList={authorToolList}
      />
    </Wrapper>
  )
}

export default StoreDetailContent;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  padding: 52px 40px;
  overflow: scroll;

  @media (max-width: 1023px) {
    overflow: initial;
  }
`;

const Divider = styled.div`
  width: 100%;
  border: 0.5px solid ${newColors.GRAY_200};
  border-radius: 0.5px;
  margin-bottom: 44px;
`;