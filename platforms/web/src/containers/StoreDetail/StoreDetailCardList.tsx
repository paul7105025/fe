import { currentStoreTabIdState, favoriteStoreToolListState, storeAPI, StoreToolType, useEvent } from "@wrtn/core";
import { FlexWrapper, newColors, newTypo } from "@wrtn/ui";
import { useRouter } from "next/router";
import { StoreCard } from "src/components/Card";
import styled from "styled-components";
import { useRecoilState, useRecoilValue } from 'recoil';


export const StoreDetailCardList = ({ title, toolList }) => {
  const [favoriteStoreToolList, setFavoriteStoreToolList] = useRecoilState(favoriteStoreToolListState);
  const currentStoreTabId = useRecoilValue(currentStoreTabIdState);
  const router = useRouter();
  const  {collectEvent} = useEvent();

  const handleClickFavorite = async (v: StoreToolType) => {
    const favIndex = favoriteStoreToolList.findIndex(w => w === v._id);

    if (favIndex >= 0) {
      const res = await storeAPI.deleteStoreFavorite_Id({
        toolId: v._id
      });
      if (res.status === 200) {
        setFavoriteStoreToolList(c => c.slice(0, favIndex).concat(c.slice(favIndex + 1)))
      }
    } else {
      const res = await storeAPI.postStoreFavorite_Id({
        toolId: v._id
      });
      if (res.status === 201) {
        setFavoriteStoreToolList(c => c.concat([v._id]))
      }
    }
  }

  const handleClickMakerCard = (card) => {
    collectEvent("click_store_card", {
      click_position: "maker",
      feature_menu: "store_tool",
      feature_category: card.category,
      feature_name: card.title,
      feature_form: "tool",
      prompt_id: card._id,
      views: card.views,
      favorites: card.likes,
      runs: card.usedNum,
      downloads: 0, // TODO
      is_updated: false, // TODO
      maker_id: card.userId
    })
    if (currentStoreTabId) {
      router.push({
        pathname: `/store/details/${card._id}`,
        query: { b2bId: currentStoreTabId }
      })
    } else {
      router.push(`/store/details/${card._id}`)
    }
  }

  return (
    <Wrapper>
      <Title>{title}</Title>
      <ContentWrapper>
        {
          toolList.map(v => (
            <StoreCard
            key={v._id}
            _id={v._id}
            icon={v.icon}
            title={v.title}
            type={"tool"}
            isFavorite={favoriteStoreToolList.find(w => w === v._id)}
            description={v.description}
            author={v?.userName || ""}
            createdAt={v.createdAt}
            viewCount={v.views}
            genCount={v.usedNum}
            exportCount={0}
            favoriteCount={v.likes}
            onClick={() => { handleClickMakerCard(v) }}
            onClickExport={() => {}}
            onClickFavorite={() => handleClickFavorite(v)}
    
            />
          ))
        }
      </ContentWrapper>
    </Wrapper>
  )
}


const ContentWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(342px, 1fr));
  gap: 24px 22px;

  @media (max-width: 1023px) {
    grid-auto-flow: column;
    grid-template-rows: initial;
    overflow: scroll;
  }

`

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;  
const Title = styled.div`
  ${newTypo('content-18-semi')};
  color: ${newColors.GRAY_700};
  padding-bottom: 24px;
`;

export default StoreDetailCardList;
