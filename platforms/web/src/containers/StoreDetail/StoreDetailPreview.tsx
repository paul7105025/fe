import { StoreToolType } from "@wrtn/core";
import { FlexWrapper, newColors } from "@wrtn/ui";
import styled from "styled-components";
import { StoreDetailToolForm } from "./StoreDetailToolForm";
import StoreDetailToolGenerateBtn from "./StoreDetailToolGenerateBtn";
import StoreDetailToolOutput from "./StoreDetailToolOutput";

interface StoreDetailPreview {
  tool: StoreToolType
}

export const StoreDetailPreview = ({ tool }) => {
  return (
    <Wrapper>
      <InfoWrapper>
        {/* tool form */}
        <StoreDetailToolForm tool={tool}/>
        {/* tool generate btn */}
        <Btn>
          <StoreDetailToolGenerateBtn tool={tool} />
        </Btn>
      </InfoWrapper>
      <OutputWrapper>
        {/* tool output */}
        <StoreDetailToolOutput />
      </OutputWrapper>
    </Wrapper>
  )
}

export default StoreDetailPreview;

const Wrapper = styled(FlexWrapper)`
  width: 100%;  
  max-height: 600px;
  gap: 30px;
  margin-bottom: 100px;

  @media(max-width: 1023px) {
    flex-direction: column;
    max-height: initial;
    width: 100%;
  }
`;

const InfoWrapper = styled.div`
  flex: 1;
  position: relative;
  height: 100%;
  width: 100%;
  @media(max-width: 1023px) {
    overflow: initial;
    flex: initial;
  }
`;

const Btn = styled.div`
  position: absolute;
  bottom: -60px;
  width: 100%;
  @media(max-width: 1023px) {
    position: initial;
    margin-top: 24px;
  }
`

const OutputWrapper = styled.div`
  flex: 1;
  height: 100%;
  overflow-y: auto;
  width: 100%;
  padding: 0px 19px 0px 0px;
  &::-webkit-scrollbar {
    display: initial;
    width: 4px;
    // background: transparent;
  }
  &::-webkit-scrollbar-thumb {
    background: ${newColors.GRAY_300};
    border-radius: 2px;
    width: 4px;
  }
  &::-webkit-scrollbar-track {
    background-color: transparent;
  }
  @media(max-width: 1023px) {
    overflow: hidden;
    display: grid;
    gap: 20px;
  }
`;