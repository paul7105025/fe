import { currentStoreDetailToolFormsState, currentStoreDetailToolOutputsState, postToxicity, storeAPI, useEvent, useLoginDialog } from '@wrtn/core';
import { colors, DefaultSpinner, MagicWand, newColors } from '@wrtn/ui';
import styled from 'styled-components';
import { useRecoilState } from "recoil";
import { useAlert } from "react-alert";
import React from 'react';
import { useUser } from 'src/hooks/useUser';
import { filterAPI } from "@wrtn/core";

export const StoreDetailToolGenerateBtn = ({ tool }) => {
  const [currentStoreDetailToolOutputs, setCurrentStoreDetailToolOutputs] = useRecoilState(currentStoreDetailToolOutputsState)
  const [currentStoreDetailToolForms, setCurrentStoreDetailToolForms] = useRecoilState(currentStoreDetailToolFormsState);

  const [loading, setLoading] = React.useState(false);

  const { user } = useUser();
  const { handleOpen } = useLoginDialog();

  const { collectEvent } = useEvent();
  const alert = useAlert();

  const validate = React.useMemo(() => {
    let isValid = true;
    currentStoreDetailToolForms.map(v => {
      if (v.data.value.length === 0) isValid = false
    })
    return isValid;
  }, [currentStoreDetailToolForms])

  const handleGenerate = async () => {
    if (loading) return ;
    if (!validate) return ;
    if (!user) {
      handleOpen();
      return;
    }
    
    collectEvent("click_generate_btn", {
      feature_menu: "store_tool",
      generate_position: "store",
      feature_form: "tool",
      feature_category: tool.category,
      feature_name: tool.title,
      feature_id: tool._id,
    })
    const inputs = currentStoreDetailToolForms.map(v => {
        return ({
          name: v?.data?.label || "",
          value: v?.data?.value || ""
        })
      })
  
    setLoading(true);
    // const filterRes = await filterAPI.postToxicity(inputs.map(v => v.value));
    const filterRes = await storeAPI.postValidatorImportant(inputs.map(v => v.value));

    if (filterRes?.status !== 201) {
      alert.removeAll()
      alert.show("생성에 실패했습니다. 다시 시도해주세요.")
      setLoading(false)
      return ;
    }

    if (filterRes?.data?.is_abuse) {
      alert.removeAll()
      alert.show("생성에 실패했습니다. 다시 시도해주세요.")
      setLoading(false)
      return ;
    }

    const res = await storeAPI.postGenerateTool_Id(tool._id, {
      inputs: inputs
    })

    if (res.status !== 201) {
      setLoading(false);
      alert.removeAll()
      alert.show("생성에 실패했습니다. 다시 시도해주세요.")
      setLoading(false)
      return ;
    }
    setCurrentStoreDetailToolOutputs(c => [
      ...c,
      res?.data?.data || ""
    ])
    collectEvent("generate_done", {
      feature_menu: "store_tool",
      generate_position: "store",
      feature_form: "tool",
      feature_category: tool.category,
      feature_name: tool.title,
      feature_id: tool._id,
    })
    setLoading(false)
  }

  return (
    <AutoCreateButton onClick={(e) => { e.preventDefault(); e.stopPropagation(); handleGenerate() } } disabled={loading || !validate}>
      <AutoCreateButtonContentWrapper>
        { loading
          ? <DefaultSpinner width={18} height={18} color={colors.WHITE} />
          : <MagicWand />
        }
        <AutoCreateText>자동 생성</AutoCreateText>
      </AutoCreateButtonContentWrapper>
    </AutoCreateButton>
  )
}

export default StoreDetailToolGenerateBtn;

const AutoCreateButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 42px;
  border: none;
  border-radius: 5px;
  background: ${newColors.GRAY_200};
  &:enabled {
    box-shadow: 4px 4px 15px rgba(79, 68, 195, 0.15);
    background: ${newColors.PURPLE_500_PRIMARY};
    &:hover { 
      cursor: pointer;
      background: linear-gradient(
          0deg,
          rgba(255, 255, 255, 0.2),
          rgba(255, 255, 255, 0.2)
      ),
      ${newColors.PURPLE_500_PRIMARY};
      box-shadow: 4px 4px 15px rgba(79, 68, 195, 0.15);
    }
  }

`;

const AutoCreateButtonContentWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 7px;
  height: 24px;
`;

const AutoCreateText = styled.div`
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;
  flex: none;
`;