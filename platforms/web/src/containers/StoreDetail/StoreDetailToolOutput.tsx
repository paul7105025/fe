
import { currentStoreDetailToolOutputsState } from "@wrtn/core";
import { FlexWrapper, newColors, newTypo, WandGradient } from "@wrtn/ui";
import { useRecoilState } from 'recoil';
import { StoreDetailOutputCard } from "src/components/Card";
import styled from 'styled-components';


export const StoreDetailToolOutput = () => {
  const [currentStoreDetailToolOutputs] = useRecoilState(currentStoreDetailToolOutputsState);

  return (
    <Wrapper>
      {
        currentStoreDetailToolOutputs.length === 0 && 
        <EmptyWrapper column justify="center">
          <EmptyIcon />
          <EmptyText>자동 생성을 눌러보세요!</EmptyText>
        </EmptyWrapper>
      }
      {
        Array.from(currentStoreDetailToolOutputs).reverse().map((v, i) => (
          <CardWrapper idx={i} lastIdx={currentStoreDetailToolOutputs.length - 1} key={v}> 
            <StoreDetailOutputCard 
              text={v}
            />
          </CardWrapper>
        ))
      }
    </Wrapper>
  )
}

export default StoreDetailToolOutput;

const CardWrapper = styled.div<{ idx: number, lastIdx: number }>`
  @media (max-width: 1023px) {
    height: 100%;
    visibility: ${props => props.idx !== 0 ? "hidden" : "initial"};
    position: ${props => props.idx !== 0 ? "absolute" : "initial"};
  }
`;

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const EmptyIcon = styled(WandGradient)`
  width: 48px;
  height: 48px;
  g {
    path {
      fill: ${newColors.GRAY_500};
    }
  }
`;

const EmptyText = styled.p`
  ${newTypo("body-14-med")};
  color: ${newColors.GRAY_600};
`

const EmptyWrapper = styled(FlexWrapper)`
  gap: 14px;
  height: 100%;
`;