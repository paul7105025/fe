import { FlexWrapper } from "@wrtn/ui";
import styled from 'styled-components';
import { StoreDetailContent } from "./StoreDetailContent";
import { StoreDetailMenu } from "./StoreDetailMenu";

export const StoreDetailMainContainer = ({ tool }) => {
  return (
    <>
      {/* info banner */}
      <Content align="flex-start">
        {/* content. SEO */}
        <StoreDetailContent tool={tool}/>
        {/* store related/recommend. 추후 개인화. SSR */}
        <StoreDetailMenu  tool={tool}/>
      </Content>
    </>
  )
}

export default StoreDetailMainContainer;

const Content = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  @media (max-width: 1023px) {
    flex-direction: column;
    height: fit-content;
  }
`;