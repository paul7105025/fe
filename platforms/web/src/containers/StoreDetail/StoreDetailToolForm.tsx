import { StoreToolType } from "@wrtn/core";
import { StoreDetailToolFormInfo } from "./StoreDetailToolFormInfo";
import { StoreDetailToolInputs } from "./StoreDetailToolInputs";
import styled from 'styled-components';
import { newColors } from "@wrtn/ui";

interface StoreDetailToolForm {
  tool: StoreToolType;
}

export const StoreDetailToolForm = ({ tool }: StoreDetailToolForm) => {
  return (
    <Wrapper>
      <StoreDetailToolFormInfo tool={tool}/>
      <Divider />
      <StoreDetailToolInputs tool={tool}/>
    </Wrapper>
  )
}

export default StoreDetailToolForm;

const Wrapper = styled.div`
  overflow: scroll;
  max-height: 600px;
  height: 100%;
  padding: 0px 19px 0px 0px;
  &::-webkit-scrollbar {
    display: initial;
    width: 4px;
    // background: transparent;
  }
  &::-webkit-scrollbar-thumb {
    background: ${newColors.GRAY_300};
    border-radius: 2px;
    width: 4px;
  }
  &::-webkit-scrollbar-track {
    background-color: transparent;
  }
`;

const Divider = styled.div`
  width: 100%;
  border: 0.5px solid ${newColors.GRAY_100};
`;
