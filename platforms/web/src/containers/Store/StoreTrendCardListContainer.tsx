import { apiInstance, currentStorePageState, currentStoreSearchState, currentStoreTabIdState, currentStoreTabState, currentStoreTypeState, favoriteStoreToolListState, storeAPI, StoreChatBotType, storeInstance, StoreToolType, useEvent, useLoginDialog } from "@wrtn/core";
import { colors, newTypo } from "@wrtn/ui";
import { useRouter } from "next/router";
import React from "react";
import { StoreCard } from "src/components/Card";
import styled from "styled-components";
import { useRecoilState, useRecoilValue } from 'recoil'; 
import { Pagination } from "@wrtn/ui/components/Pagination";
import { useUser } from "src/hooks/useUser";
import useSWR from 'swr';
// import { ParsedUrlQuery } from "querystring";
import { parseQuery } from 'src/utils/swr'; 
import { useFavoriteStoreChatbotIdList, useFavoriteStoreToolIdList, useStoreTrendChatbotList, useStoreTrendToolList } from "src/hooks/swr";

type StoreTrendCardListContainerProps = {
  title: string;
}

const TOOL_TREND_LIMIT_COUNT_PER_PAGE = 6;
const CHATBOT_TREND_LIMIT_COUNT_PER_PAGE = 6;

export const StoreTrendCardListContainer = ({ title }: StoreTrendCardListContainerProps) => {
  const router = useRouter();
  const { user } = useUser();
  const { handleOpen } = useLoginDialog();
  const { collectEvent } = useEvent();
  const currentStoreTabId = useRecoilValue(currentStoreTabIdState);
  const currentStoreType = useRecoilValue(currentStoreTypeState);
  const currentStoreSearch = useRecoilValue(currentStoreSearchState)

  const { 
    storeTrendToolList,
    isLoading: isLoadingTool
  } = useStoreTrendToolList({
    title: parseQuery(router.query, "title") || "",
    page: 1,
    limit: TOOL_TREND_LIMIT_COUNT_PER_PAGE,
    doNotLoad: currentStoreType !== 'tool'
  });

  const { 
    storeTrendChatbotList,
    isLoading: isLoadingChatbot
  } = useStoreTrendChatbotList({
    title: parseQuery(router.query, "title") || "",
    page: 1,
    limit: CHATBOT_TREND_LIMIT_COUNT_PER_PAGE,
    doNotLoad: currentStoreType !== 'chatbot'
  })

  const isLoading = isLoadingTool || isLoadingChatbot;

  const {
    favoriteToolIdList,
    append: appendFavTool,
    remove: removeFavTool
  } = useFavoriteStoreToolIdList();

  const { 
    favoriteChatbotIdList,
    append: appendFavChatbot,
    remove: removeFavChatbot,
  } = useFavoriteStoreChatbotIdList();


  const handleClickFavorite = async (v: StoreToolType) => {
    if(!user){
      handleOpen()
    } else {
      const currentFavIdList: any[] = currentStoreType === 'tool' ? favoriteToolIdList : favoriteChatbotIdList;
      const append = currentStoreType === 'tool' ? appendFavTool : appendFavChatbot;
      const remove = currentStoreType === 'tool' ? removeFavTool : removeFavChatbot;
      const favIndex = currentFavIdList.findIndex((w: string) => w === v._id)

      if (favIndex >= 0) {
        const res = await storeAPI.deleteStoreFavorite_Id({
          ...( currentStoreType === 'tool' ? { toolId: v._id } : { chatbotId: v._id })
        });
        if (res.status === 200) {
          append(v._id)
        }
      } else {
        const res = await storeAPI.postStoreFavorite_Id({
          ...( currentStoreType === 'tool' ? { toolId: v._id } : { chatbotId: v._id })
        });
        if (res.status === 201) {
          remove(v._id)
        }
      } 
    }
  }

  const handleClickCard = (v: StoreChatBotType | StoreToolType) => {
    collectEvent("click_store_card", {
      click_position: title === "실시간 트렌드" ? "trend" : "new",
      feature_menu: "store_" + currentStoreType,
      feature_category: v.category,
      feature_name: v.title,
      feature_form: currentStoreType,
      prompt_id: v._id,
      // @ts-ignore
      views: v?.views || v?.viewNum,
      // @ts-ignore
      favorites: v?.likes || v?.likeNum,
      runs: v.usedNum,
      downloads: 0, // TODO
      is_updated: false, // TODO
      maker_id: v.userId
    });
    if (currentStoreType === 'tool') router.push(`/store/details/${v._id}`)
    if (currentStoreType === 'chatbot') router.push(`/store/details/chatbot/${v._id}`)
  }

  if (isLoading) return <></>
  if (currentStoreTabId) return <></>
  if (currentStoreSearch.length !== 0) return <></>

  return (
    <Wrapper>
      <Title>{title}</Title>
      <ContentWrapper >
      {(storeTrendToolList.concat(storeTrendChatbotList)).map((v) => {
          return (
            (
              <StoreCard
                key={v._id}
                _id={v._id}
                icon={v.icon}
                title={v.title}
                type={currentStoreType}
                isFavorite={(favoriteToolIdList.concat(favoriteChatbotIdList)).findIndex(w => w === v._id.toString()) > -1 ? true : false}
                description={v.description}
                author={v?.userName || ""}
                createdAt={v.createdAt}
                viewCount={v?.views || v?.viewNum}
                genCount={v.usedNum}
                exportCount={0}
                favoriteCount={v?.likes || v?.likeNum}
                onClick={() => handleClickCard(v)}
                onClickExport={() => {}}
                onClickFavorite={() => handleClickFavorite(v)}
              />
            )
          )
        })
      }
      </ContentWrapper>
    </Wrapper>
  )
}

export default StoreTrendCardListContainer;

const ContentWrapper = styled.div`
display: grid;
grid-template-columns: repeat(auto-fill, minmax(342px, 1fr));
gap: 24px 22px;
`

const Wrapper = styled.div`
  padding-bottom: 57px;
`;  

const Title = styled.div`
  ${newTypo("heading-20-semi")};
  color: ${colors.gray_80};
  padding-bottom: 32px;
`;

export const PaginationWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 20px;
`;

const EmptyWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 500px;
`

const EmptyInnerWrapper = styled.div`
  
`

const EmptyText = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 180%;
  /* or 29px */

  text-align: center;

  /* Gray 6 */

  color: #83879D;
`

const EmptyIcon = styled.img`
  display: block;
  width: 36px;
  height: 36px;
  margin: 0px auto 16px;
`