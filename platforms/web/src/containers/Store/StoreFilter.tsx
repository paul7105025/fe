import { currentStoreCategoryFilterState } from "@wrtn/core";
import { colors, FlexWrapper, newColors } from "@wrtn/ui";
import FavoriteCategoryToggle from "@wrtn/ui/containers/CategoryToggle/FavoriteCategoryToggle";
import styled from "styled-components";
import { useRecoilState } from "recoil"
import { useRouter } from "next/router";
import { useUser } from "src/hooks/useUser";
import { parseQuery } from "src/utils/swr";
import React from "react";

const categoryList = ["전체", "즐겨찾기", "범용/일반", "블로그", "마케팅", "쇼핑몰", "학생", "업무용", "유튜브", "글짓기", "교육", "엔터테인먼트", "기타"]
const noLoginCategoryList = ["전체", "범용/일반", "블로그", "마케팅", "쇼핑몰", "학생", "업무용", "유튜브", "글짓기", "교육", "엔터테인먼트", "기타"]
export const StoreFilter = () => {
  const user = useUser();
  const currentCategory = user.user ? categoryList : noLoginCategoryList;
  const [currentStoreCategoryFilter, setCurrentStoreCategoryFilter] = useRecoilState(currentStoreCategoryFilterState);

  return (
    <Wrapper align="center">
      {
        currentCategory.map(v => (
          <Category 
            key={v}
            disabled={v === currentStoreCategoryFilter}
            onClick={() => { 
              setCurrentStoreCategoryFilter(v)
            }}
          >{v}</Category>
        ))
      }
    </Wrapper>
  )
}

export default StoreFilter;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 12px;
  flex-wrap: wrap;
  padding: 0px 34px 40px;
  white-space: nowrap;

  @media (max-width: 767px) {
    flex-wrap: nowrap;
    overflow: scroll;
  }
`;

const Category = styled.button`
  padding: 6px 18px;
  background: none;
  border: none;
  color: ${newColors.GRAY_600};
  font-weight: 500;
  font-size: 14px;
  &:hover {
    cursor: pointer;
  }
  &:disabled {
    border: 1px solid ${newColors.PURPLE_500_PRIMARY};
    border-radius: 18px;
    font-weight: 600;
    color: ${newColors.PURPLE_500_PRIMARY};
  }
`;