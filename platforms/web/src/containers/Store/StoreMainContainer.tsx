import { StoreToolType, currentStoreCategoryFilterState, currentStoreTabState, currentStoreTypeState } from '@wrtn/core';
import styled from 'styled-components';
import { StoreHeader, StoreFilter, StoreCardListContainer } from '.';
import { useRecoilValue } from "recoil"
import StoreTrendCardListContainer from './StoreTrendCardListContainer';
import StoreTypeFilter from './StoreTypeFilter';

export const StoreMainContainer = () => {
  const categoryFilter = useRecoilValue(currentStoreCategoryFilterState);
  const currentStoreTab = useRecoilValue(currentStoreTabState);
  const currentStoreType = useRecoilValue(currentStoreTypeState);

  return (
    <Wrapper>
      {/* 헤더 */}
      <StoreHeader />
      <StoreTypeFilter />
      {/* 필터 */}
      <StoreFilter />
      <ContentWrapper>
        {/* 실시간 트렌드 */}
        {categoryFilter === "전체" && currentStoreTab === "전체" && (
          <StoreTrendCardListContainer
            title="실시간 트렌드"
          />
        )}
        
        {/* 신규 툴/봇 */}
        <StoreCardListContainer 
          title={currentStoreType === 'tool' ? "신규 툴" : '신규 봇'}
        />
      </ContentWrapper>
    </Wrapper>
  )
}
export default StoreMainContainer;

const Wrapper = styled.div`
  padding: 40px 0px 0px;
  height: 100%;
`;

const ContentWrapper = styled.div`
// TODO: magic!
  padding: 0px 34px;
  height: calc(100% - 76px - 112px - 64px);
  overflow: scroll;
`;