import { currentStoreTypeState } from '@wrtn/core';
import { newColors, FlexWrapper, newTypo } from '@wrtn/ui';
import styled from 'styled-components';
import { useRecoilState } from 'recoil';
import { useRouter } from 'next/router';
import React from 'react';

export const StoreTypeFilter = () => {
  const router = useRouter();
  const { tab } = router.query;
  const [currentStoreType, setCurrentStoreType] = useRecoilState(currentStoreTypeState);

  React.useEffect(() => {
    if(tab){
      setCurrentStoreType(tab as "chatbot" | "tool");
    }
  }, [tab])

  return (
    <Wrapper>
      <Type selected={currentStoreType === "tool"} onClick={() => { router.replace({ pathname: '/store', query: { tab: 'tool'}}) }}>
        툴
      </Type>
      <Type selected={currentStoreType === "chatbot"} onClick={() => { router.replace({ pathname: '/store', query: { tab: 'chatbot'}}) }}>
        챗봇
      </Type>

    </Wrapper>
  )
}

export default StoreTypeFilter;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 0px 34px;
  border-bottom: 1px solid ${newColors.GRAY_200};
  margin-bottom: 24px;
  gap: 2px;
`;  

const Type = styled.div<{ selected: boolean }>`
  padding: 11px 13px;
  cursor: pointer;
  border-bottom: ${props => props.selected ? `2px solid ${newColors.PURPLE_500_PRIMARY}` : 'initial'};
  ${props => props.selected ? newTypo("content-18-semi") : newTypo("content-18-med")};
  color: ${props => props.selected ? newColors.GRAY_800 : newColors.GRAY_600};
`;