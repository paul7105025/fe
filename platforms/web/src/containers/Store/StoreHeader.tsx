import { colors, FlexWrapper, newTypo } from "@wrtn/ui";
import { SearchInput } from "@wrtn/ui/components/SearchInput";
import styled from "styled-components";
import { useSetRecoilState } from 'recoil'
import { currentStoreSearchState, useNonInitialEffect, currentStorePageState } from "@wrtn/core";
import React from "react";
import _ from "lodash"
import { useRouter } from "next/router";
import { parseQuery } from "src/utils/swr";


export const StoreHeader = () => {
  const router = useRouter();
  // const setCurrentStoreSearch = useSetRecoilState(currentStoreSearchState);
  const [input, setInput] = React.useState("");
  const setCurrentStorePage = useSetRecoilState(currentStorePageState);
  const setCurrentStoreSearch = useSetRecoilState(currentStoreSearchState)
  const inputSync = React.useMemo(() => _.debounce((input) => {
    setCurrentStorePage(1);
    setCurrentStoreSearch(input);
  }, 1000), []);

  useNonInitialEffect(() => {
    inputSync(input);
  }, [input]);

  return (
    <Wrapper>
      <Title>스토어</Title>
      <SearchInput 
        placeholder="툴/봇 이름 또는 카테고리 검색"
        value={input || ""}
        onChange={(e) => { setInput(e.target.value) }}
        onKeyDown={() => {}}
        long={false}
      />
    </Wrapper>
  )
}

export default StoreHeader;

const Wrapper = styled(FlexWrapper)`

  width: 100%;
  justify-content: space-between;
  padding: 0px 34px 29px;
`;  

const Title = styled.div`
  ${newTypo('heading-22-semi')};
  color: ${colors.GRAY_80};
`;