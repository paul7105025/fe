import { apiInstance, currentStorePageState, favoriteStoreToolListState, currentStoreSearchState, currentStoreCategoryFilterState, storeAPI, storeInstance, StoreToolType, useEvent, useLoginDialog, currentStoreTabState, currentStoreTabIdState, currentStoreTypeState, StoreChatBotType } from "@wrtn/core";
import { colors, newTypo } from "@wrtn/ui";
import { useRouter } from "next/router";
import React from "react";
import { StoreCard } from "src/components/Card";
import styled from "styled-components";
import { useRecoilState, useRecoilValue } from 'recoil'; 
import { Pagination } from "@wrtn/ui/components/Pagination";
import { useUser } from "src/hooks/useUser";
import { useFavoriteStoreToolIdList, useStoreChatbotList, useStoreToolList } from "src/hooks/swr";
import { useFavoriteStoreChatbotIdList } from "src/hooks/swr/chatbot";

type StoreCardListContainerProps = {
  title: string;
}

const TOOL_LIMIT_COUNT_PER_PAGE = 24;
const CHATBOT_LIMIT_COUNT_PER_PAGE = 24;

export const StoreCardListContainer = ({ title }: StoreCardListContainerProps) => {
  const router = useRouter();
  const { user } = useUser();
  const { handleOpen } = useLoginDialog();
  const { collectEvent } = useEvent();

  const [currentStorePage, setCurrentStorePage] = useRecoilState(currentStorePageState);
  const currentStoreSearch = useRecoilValue(currentStoreSearchState);
  const currentStoreCategoryFilter = useRecoilValue(currentStoreCategoryFilterState)
  const currentStoreTabId = useRecoilValue(currentStoreTabIdState);
  const currentStoreTab = useRecoilValue(currentStoreTabState);
  const currentStoreType = useRecoilValue(currentStoreTypeState);

  const { 
    storeToolList,
    storeToolListMaxCount,
    isLoading: isLoadingTool
  } = useStoreToolList({
    title: currentStoreSearch,
    page: currentStorePage,
    limit: TOOL_LIMIT_COUNT_PER_PAGE,
    category: currentStoreCategoryFilter,
    b2bId: currentStoreTabId,
    excludeB2b: currentStoreTab === '일반',
    doNotLoad: currentStoreType !== 'tool'
  });

  const {
    storeChatbotList,
    storeChatbotListMaxCount,
    isLoading: isLoadingChatbot
  } = useStoreChatbotList({
    title: currentStoreSearch,
    page: currentStorePage,
    limit: CHATBOT_LIMIT_COUNT_PER_PAGE,
    category: currentStoreCategoryFilter,
    b2bId: currentStoreTabId,
    excludeB2b: currentStoreTab === '일반',
    doNotLoad: currentStoreType !== 'chatbot'
  })

  const isLoading = isLoadingChatbot || isLoadingTool


  const {
    favoriteToolIdList,
    append: appendFavTool,
    remove: removeFavTool
  } = useFavoriteStoreToolIdList();

  const { 
    favoriteChatbotIdList,
    append: appendFavChatbot,
    remove: removeFavChatbot,
  } = useFavoriteStoreChatbotIdList();

  const maxPage = React.useMemo(() => {
    if (currentStoreType === 'tool') return storeToolListMaxCount ? Math.floor(storeToolListMaxCount / TOOL_LIMIT_COUNT_PER_PAGE) + (currentStorePage || 1) : 1
    if (currentStoreType === "chatbot") return storeChatbotListMaxCount ? Math.floor(storeChatbotListMaxCount / CHATBOT_LIMIT_COUNT_PER_PAGE) + (currentStorePage || 1) : 1
    return 1;
  }, [storeToolListMaxCount, storeChatbotListMaxCount, currentStoreType]);

  const handleClickFavorite = async (v: StoreToolType) => {
    // TODO: 분기!
  
    if(!user){
      handleOpen()
    } else {
      const currentFavIdList: any[] = currentStoreType === 'tool' ? favoriteToolIdList : favoriteChatbotIdList;
      const append = currentStoreType === 'tool' ? appendFavTool : appendFavChatbot;
      const remove = currentStoreType === 'tool' ? removeFavTool : removeFavChatbot;
      const favIndex = currentFavIdList.findIndex((w: string) => w === v._id)

      if (favIndex >= 0) {
        const res = await storeAPI.deleteStoreFavorite_Id({
          ...( currentStoreType === 'tool' ? { toolId: v._id } : { chatbotId: v._id })
        });
        if (res.status === 200) {
          append(v._id)
        }
      } else {
        const res = await storeAPI.postStoreFavorite_Id({
          ...( currentStoreType === 'tool' ? { toolId: v._id } : { chatbotId: v._id })
        });
        if (res.status === 201) {
          remove(v._id)
        }
      } 
    }
  }

  const handleClickCard = (v: StoreChatBotType | StoreToolType) => {
    // TODO: 분기
    collectEvent("click_store_card", {
      click_position: title === "실시간 트렌드" ? "trend" : "new",
      feature_menu: currentStoreType === 'tool' ? "store_tool" : 'store_chatbot',
      feature_category: v.category,
      feature_name: v.title,
      feature_form: currentStoreType,
      prompt_id: v._id,
      // @ts-ignore
      views: v?.views || v?.viewNum,
      // @ts-ignore
      favorites: v?.likes || v?.likeNum,
      runs: v.usedNum,
      downloads: 0, // TODO
      is_updated: false, // TODO
      maker_id: v.userId
    })
    const baseURL = currentStoreType === 'tool' ? '/store/details/' : '/store/details/chatbot/'
    if (currentStoreTabId) {
      router.push({
        pathname: `${baseURL}${v._id}`,
        query: { b2bId: currentStoreTabId }
      })
    } else {
      router.push(`${baseURL}${v._id}`)
    }
  }

  return (
    <Wrapper>
      <Title>{title}</Title>
      {(storeToolList.concat(storeChatbotList)).length === 0 ? (
        <EmptyWrapper isLoading={isLoading}>
          <EmptyInnerWrapper>
            <EmptyIcon src="/store/empty.svg" />
            <EmptyText>
              해당되는 툴/챗봇이 없어요
            </EmptyText>
          </EmptyInnerWrapper>
        </EmptyWrapper>
      ) : (
        <ContentWrapper isLoading={isLoading}>
          {
            (storeToolList.concat(storeChatbotList)).map((v) => {
              return (
                (
                  <StoreCard
                    key={v._id}
                    _id={v._id}
                    icon={v.icon}
                    title={v.title}
                    type={currentStoreType}
                    isFavorite={(favoriteToolIdList.concat(favoriteChatbotIdList)).findIndex((w: string) => w === v._id.toString()) > -1 ? true : false}
                    description={v.description}
                    author={v?.userName || ""}
                    createdAt={v.createdAt}
                    viewCount={v?.views || v?.viewNum}
                    genCount={v.usedNum}
                    exportCount={0}
                    favoriteCount={v?.likes || v?.likeNum}
                    onClick={() => handleClickCard(v)}
                    onClickExport={() => {}}
                    onClickFavorite={() => handleClickFavorite(v)}
                  />
                )
              )
            })
          }
        </ContentWrapper>
      )}
      {storeToolListMaxCount && currentStorePage &&
        <PaginationWrapper>
          <Pagination
            maxPage={maxPage}
            currentPage={currentStorePage ? currentStorePage - 1 : 1}
            setPage={(v) => {
              setCurrentStorePage(v + 1)
            }}
          />
        </PaginationWrapper>
      }
    </Wrapper>
  )
}

export default StoreCardListContainer;

const ContentWrapper = styled.div<{ isLoading: boolean}>`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(342px, 1fr));
  gap: 24px 22px;
  opacity: ${props => props.isLoading ? "0.4" : "1"};
`

const Wrapper = styled.div`
  padding-bottom: 57px;
`;  

const Title = styled.div`
  ${newTypo("heading-20-semi")};
  color: ${colors.gray_80};
  padding-bottom: 32px;
`;

export const PaginationWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 20px;
`;

const EmptyWrapper = styled.div<{ isLoading: boolean}>`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 500px;
  opacity: ${props => props.isLoading ? "0.4" : "1"};
`

const EmptyInnerWrapper = styled.div`
  
`

const EmptyText = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 180%;
  /* or 29px */

  text-align: center;

  /* Gray 6 */

  color: #83879D;
`

const EmptyIcon = styled.img`
  display: block;
  width: 36px;
  height: 36px;
  margin: 0px auto 16px;
`