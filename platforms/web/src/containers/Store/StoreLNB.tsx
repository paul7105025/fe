import { currentStoreTabIdState, currentStorePageState, currentStoreTabState, useEvent, userState } from "@wrtn/core";
import { colors, FlexWrapper, Icon, IconBlock, LogoPrompthon, newColors, newTypo } from "@wrtn/ui";
import { useRouter } from "next/router";
import React from "react";
import styled from "styled-components";
import { GuideButtonLNB } from "../Button";
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';
import { useStoreB2bMemberList } from "src/hooks/swr";
import { useUser } from "src/hooks/useUser";
import { GuideButtonStoreLNB } from "../Button";

const studio_url = process.env.NEXT_PUBLIC_STUDIO_URL || process.env.REACT_APP_STUDIO_URL || "";


const NavContent = ({
  onClickTab
}) => {
  const [currentStoreTab, setCurrentStoreTab] = useRecoilState(currentStoreTabState);
  const [currentStoreTabId, setCurrentStoreTabId] = useRecoilState(currentStoreTabIdState);
  const { storeB2bMemberList } = useStoreB2bMemberList();

  return (
    <>
      { storeB2bMemberList.length > 0 &&
        <NavItem onClick={() => { onClickTab("일반") }} selected={currentStoreTab === "일반"}>
          <IconBlock />일반
        </NavItem>
      }
      { storeB2bMemberList.map(v => {
          return (
            <NavItem key={v._id} onClick={() => { onClickTab(v.b2bId.organizationName, v.b2bId._id) }} selected={currentStoreTab === v.b2bId.organizationName}>
              <Icon icon="signal-alt" size={18} color={currentStoreTab === v.b2bId.organizationName ? newColors.GRAY_700 : newColors.GRAY_600}/> {v.b2bId.organizationName}
            </NavItem>
          )
        })
      }
    </>
  )
}

export const StoreLNB = () => {
  const [currentStoreTab, setCurrentStoreTab] = useRecoilState(currentStoreTabState);
  const [currentStoreTabId, setCurrentStoreTabId] = useRecoilState(currentStoreTabIdState);
  const setCurrentStorePage = useSetRecoilState(currentStorePageState);
  const user = useRecoilValue(userState);
  const router = useRouter();
  const { collectEvent } = useEvent();

  const goToStudio = () => {
    collectEvent("click_studio_btn");
    router.push(studio_url)
  }

  const handleClickTab = (tab: string, id?: string) => {
    if (currentStoreTab !== tab) {
      setCurrentStorePage(1);
    }

    setCurrentStoreTab(tab);
    if (!id) setCurrentStoreTabId(null)
    else setCurrentStoreTabId(id)
  }

  return (
    <NavWrapper column>
      <NavItem onClick={() => { handleClickTab("전체") }} selected={currentStoreTab === "전체"}>
        <Icon icon="store" size={18} color={currentStoreTab === "전체" ? newColors.GRAY_700 : newColors.GRAY_600}/>전체
      </NavItem>
      { user &&
        <NavContent 
          onClickTab={handleClickTab}
        />
      }
      <Empty />
      <Divider />
      {/* <NavItem 
        onClick={() => { window.open("https://www.onoffmix.com/event/276106", "_blank")}}
        style={{'padding': '13px 16px'}}
      >
        <Icon icon="lightbulb-alt" size={18}/> <LogoPrompthon />
      </NavItem> */}
      <NavItem 
        onClick={() => { window.open("https://wrtn.imweb.me/tutorial/?bmode=view&idx=15191324&back_url=&t=board&page=1")}}
        style={{'padding': '13px 16px'}}
      >
        <Icon icon="question-circle" size={18}/>스토어가 무엇인가요?
      </NavItem>
      <GuideButtonStoreLNB />
      <StudioBtn onClick={goToStudio}>
        <Icon icon="plus" size={20} color={newColors.WHITE}/> AI 제작 스튜디오
      </StudioBtn>
    </NavWrapper>
  )
}

export default StoreLNB;

const NavWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  overflow: hidden;

  justify-content: flex-start;
  align-items: flex-start;
  align-self: stretch;
  padding-top: 29px;
  @media (max-width: 767px) {
    background: white;
    width: 100%;
  }
`;

const NavItem = styled.button<{ selected?: boolean }>`
  background: transparent;
  display: flex;
  gap: 10px;
  align-items: center;
  width: 100%;
  padding: 10px 25px;
  ${newTypo('content-14-semi')};
  color: ${props => props.selected ? newColors.GRAY_800 : newColors.GRAY_600};
  background: ${props => props.selected ? newColors.WHITE : "transparent"};
  &:enabled {
    &:hover {
      color: ${newColors.GRAY_800};
      i {
        color: ${newColors.GRAY_700};
      }
      path {
        fill: ${newColors.GRAY_700};
      }
      cursor: pointer;
      background: ${newColors.WHITE};
    }
  }

  &:disabled {
    color: ${newColors.GRAY_400};
    &:hover {
      cursor: default;
      background: transparent;
      path {
        fill: ${newColors.GRAY_400};
      }
    }
  }
`;

const Divider = styled.div`
  width: 100%;
  border 0.5px solid ${newColors.GRAY_200};
  margin: 18px 0px;

`;

const Empty = styled.div`
  flex: 1;
`;  

const StudioBtn = styled.button`
  background: ${newColors.PURPLE_500_PRIMARY};
  border 1px solid ${newColors.GRAY_200};
  border-radius: 5px;
  cursor: pointer;
  ${newTypo("content-16-semi")};
  color: ${newColors.WHITE};
  padding: 12px 38px;
  margin: 20px auto 0px;
  &:disabled {
    background: ${newColors.GRAY_300};
    cursor: default;
  }
`;


const Block = styled(IconBlock)<{selected?:boolean}>`
  path {
    fill: ${props => props.selected ? newColors.GRAY_700 :newColors.GRAY_600};
  }

  &:hover {
    path {
      fill: ${newColors.GRAY_700};
    }
  }
`;

