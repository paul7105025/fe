import { atom, RecoilState } from "recoil";
import { HistoryMetaData } from "@wrtn/core";

export const historyState = atom({
  key: "historyState",
  default: [],
});

export const likeListState = atom({
  key: "likeListState",
  default: [],
});

export const currentHistoryState: RecoilState<HistoryMetaData | null> =
  atom<HistoryMetaData | null>({
    key: "currentHistoryState",
    default: null,
  });

export const currentImageState = atom({
  key: "currentImageState",
  default: null,
});

export const historyList = atom({
  key: "historyListState",
  default: null,
});
