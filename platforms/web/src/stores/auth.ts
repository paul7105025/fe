import { LOGIN_EMAIL } from "@wrtn/core/constants";
import { LocalLoginState, LoginProvider } from "@wrtn/core/types/login";
import { atom, DefaultValue, RecoilState, selector } from "recoil";

export const localLoginState: RecoilState<LocalLoginState> =
  atom<LocalLoginState>({
    key: "localLoginState",
    default: {
      open: false,
      section: LOGIN_EMAIL,
      email: "",
      password: "",
      provider: "local",
    },
  });

export const localLoginEmailState: RecoilState<string> = selector<string>({
  key: "localLoginEmailState",
  get: ({ get }) => {
    const localLogin = get(localLoginState);
    return localLogin.email;
  },
  set: ({ set }, newValue) => {
    set(localLoginState, (currVal) =>
      newValue instanceof DefaultValue
        ? newValue
        : {
            ...currVal,
            email: newValue,
          }
    );
  },
});

export const localLoginPasswordState: RecoilState<string> = selector<string>({
  key: "localLoginPasswordState",
  get: ({ get }) => {
    const localLogin = get(localLoginState);
    return localLogin.password;
  },
  set: ({ set }, newValue) => {
    set(localLoginState, (currVal) =>
      newValue instanceof DefaultValue
        ? newValue
        : {
            ...currVal,
            password: newValue,
          }
    );
  },
});

export const localLoginProviderState: RecoilState<LoginProvider> =
  selector<LoginProvider>({
    key: "localLoginProviderState",
    get: ({ get }) => {
      const localLogin = get(localLoginState);
      return localLogin.provider;
    },
    set: ({ set }, newValue) => {
      set(localLoginState, (currVal: LocalLoginState) =>
        newValue instanceof DefaultValue
          ? newValue
          : {
              ...currVal,
              provider: newValue,
            }
      );
    },
  });

export const loginSuccessPathState = atom<string | null>({
  key: "loginSuccessPathState",
  default: null,
});
