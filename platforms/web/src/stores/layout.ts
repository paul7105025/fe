import { atom, RecoilState } from "recoil";

export type LNBControllerStateType = {
  isOpen: boolean;
};
export const LNBControllerState = atom<LNBControllerStateType>({
  key: "@app/layout/LNBControllerState",
  default: {
    isOpen: true,
  },
});
