import { atom } from "recoil";

export const selectedTemplateState = atom<{ id: string, name: string, content: string}>({
  key: '@wrtn/web/selectedTemplateState',
  default: {
    id: "",
    name: "",
    content: ""
  },
})