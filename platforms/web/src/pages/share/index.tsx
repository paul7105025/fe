import { GetServerSideProps, GetServerSidePropsContext, PreviewData } from "next";
import Head from "next/head";
import styled from "styled-components";
import { ParsedUrlQuery } from "querystring";
import { ReactElement, useEffect, useMemo } from "react";
import { useSearchParams } from "next/navigation";

import { FlexWrapper, newColors, typo } from "@wrtn/ui";
import { Spinner } from "@wrtn/ui/components/Spinner";
import { getShareChats, hideChannelButton, showChannelButton, syncHeader, useEvent, useIsMobile } from "@wrtn/core";

import { AuthCheckLayout, ChannelLayout, GlobalModalLayout, MainLayout } from "src/layouts";
import { EventLayout } from "src/layouts/EventLayout";
import ChatCardItem from "src/components/Share/ChatCardItem";
import Pagination from "src/components/Share/PaginationNew";

import CategoryList from "src/containers/Share/CategoryList";
import MyChatList from "src/containers/Share/MyChatList";
import MyLikedChatList from "src/containers/Share/MyLikedChatList";
import SearchArea from "src/containers/Share/SearchArea";

import useInitAuth from "src/hooks/auth/useInitAuth";
import useQueryString from "src/hooks/useQueryString";
import useLoading from "src/hooks/useLoading";

type Props = {
  chats: any[];
  accessKey: string | null;
  totalCount: number;
  page: number;
}

export default function SharePage({ chats, accessKey, totalCount, page }: Props) {
  const { onChangeQuery } = useQueryString();
  const searchParams = useSearchParams();

  const { collectEvent } = useEvent();
  const isMobile = useIsMobile();
  const { loading } = useLoading();

  useInitAuth({ 
    accessKey,
    options: {
      updateUser: "unique"
    }
  });

  useEffect(() => {
    if (!isMobile) {
      showChannelButton();
    }
    collectEvent("view_share_trend_main");

    return () => {
      hideChannelButton();
    }
  }, [])

  const handlePage = (page: number) => {
    onChangeQuery("page", `${page}`);
  }

  const maxPage = useMemo(() => Math.floor((totalCount-1)/20), [totalCount]);
  const category = useMemo(() => searchParams.get("category") ?? "", [searchParams]);

  return (
    <ShareLayout>
      <SearchArea />
      <CategoryList />
      <ChatCardWrapper>
        {loading ? (
          <Spinner width={22} height={22} color={newColors.PURPLE_500_PRIMARY} />
        ) : (
          <>
            {category === "my" && <MyChatList />}
            {category === "mylikes" && <MyLikedChatList />}
            {!["my", "mylikes"].includes(category) && 
            <>
              {
                chats.map((chat) => {
                  return <ChatCardItem chat={chat} />
                })
              }
            </>
            }
            {!["my", "mylikes"].includes(category) && chats.length === 0 && (
              <NoChatWrapper>검색된 채팅이 없습니다.</NoChatWrapper>
            )}
          </>
        )}
      </ChatCardWrapper>
      <PaginationArea>
        {!loading && !["my", "mylikes"].includes(category) && (
          <Pagination currentIndex={Number(page)} maxIndex={Number(maxPage)} handlePage={handlePage}/>
        )}
      </PaginationArea>
    </ShareLayout>
  )
}

SharePage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <ChannelLayout>
              <MainLayout>
                {page}
              </MainLayout>
            </ChannelLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  )
}

export const getServerSideProps: GetServerSideProps<{ chats: any[] }> = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  const { category, keyword, page } = ctx.query;
  const { access_token } = ctx.req.cookies;

  if (Array.isArray(category) || Array.isArray(keyword) || Array.isArray(page)) return { props: { chats: [] }};

  syncHeader({
    key: "Authorization",
    value: `Bearer ${access_token}`
  })
  const accessKey = access_token || null

  try {
    if (category === "my") {
      return { props: { chats: [], accessKey, page: page ?? 0 }};
    } else if (category === "mylikes") {
      return { props: { chats: [], accessKey, page: page ?? 0 }};
    }

    const res = await getShareChats(category, keyword, page ?? "0");
    const { chats, totalCount } = res.data.data;

    return { props: { chats, accessKey, totalCount, page: page ?? 0 }};
  } catch(err) {
    return { props: { chats: [], accessKey, totalCount: 0, page: 0 }};
  }
}

const ShareLayout = styled(FlexWrapper)`
  flex-direction: column;
  margin-top: 24px;

  @media (max-width: 767px) {
    margin-bottom: 40px;
  }
`

const ChatCardWrapper = styled(FlexWrapper)`
  flex-direction: column;
  gap: 20px;
  padding: 10px 0;
  width: 980px;
  margin-bottom: 50px;

  @media (max-width: 767px) {
    width: 100%;
    padding: 10px;
  }
`

const PaginationArea = styled(FlexWrapper)`
  margin-bottom: 30px;
`

const NoChatWrapper = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    height: "180%",
    color: "#A1A5B9",
  })}
`
