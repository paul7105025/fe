import { GetStaticPaths, GetStaticProps } from "next";
import { ReactElement, useEffect, useState } from "react";
import styled from "styled-components";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";

import { InputMessageType, OutputMessageType, ShareChatType, ShareMessageType, getShareChat, getShareChats } from "@wrtn/core";
import { FlexWrapper, Icon, newColors, typo } from "@wrtn/ui";
import { Spinner } from "@wrtn/ui/components/Spinner";

import { CircleButton, ShareCircle, TrashCircle, ViewCircle, HeartCircle } from "src/components/Share/CircleButtons";
import { InputMessage, OutputMessage, ImageOutputMessage } from "src/components/Share/Messages";
import Date from "src/components/Share/Date";

import { AuthCheckLayout, ChannelLayout, GlobalModalLayout, MainLayout } from "src/layouts";
import { EventLayout } from "src/layouts/EventLayout";

type DetailChatType = ShareChatType & { messages: ShareMessageType[] }

type Props = {
  chat: DetailChatType | null;
  ogImage: string;
  fallback: string;
}

const baseURL = process.env.NODE_ENV === "production" ? "https://wrtn.ai" : "https://test-copy.wrtn.ai"

export default function ShareChatPage({ chat, ogImage }: Props) {
  const [showTitleCard, setShowTitleCard] = useState(true);
  const router = useRouter();

  useEffect(() => {
    let timer: NodeJS.Timeout | null = null;

    const handleScroll = () => {
      setShowTitleCard(false);

      if (timer) clearTimeout(timer);

      timer = setTimeout(() => {
        setShowTitleCard(true);
      }, 1000);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
      if (timer) clearTimeout(timer);
    }
  }, [])

  if (!chat || !chat.messages) return (
    <ShareChatLayout>
      <LeftArea>
        <Link href="/share">
          <CircleButton label="목록 보기">
            <Icon icon="arrow-left" size={20}/>
          </CircleButton>
        </Link>
      </LeftArea>
      <MessagesArea>
        {router.isFallback ? (
          <EmptyMessage>
            <Spinner width={22} height={22} color={newColors.PURPLE_500_PRIMARY} />
          </EmptyMessage>
        ) : (
          <EmptyMessage>채팅을 찾을 수 없습니다.</EmptyMessage>
        )}
      </MessagesArea>
      <RightArea>
        <CircleButton><Icon icon="link" size={20}/></CircleButton>
      </RightArea>
    </ShareChatLayout>
  )

  const messages: (InputMessageType | OutputMessageType)[] = [];
  let outputs: OutputMessageType = [];

  chat.messages.forEach((msg, index) => {
    if (msg.role === "user") {
      if (outputs.length !== 0) {
        messages.push(outputs);
        outputs = [];
      }
      messages.push(msg);
    } else {
      outputs.push(msg);
    }

    if (index === chat.messages.length - 1 && outputs.length > 0) {
      messages.push(outputs);
    }
  })

  const cutTitleWhenTooLong = (text: string, maxNum: number) => {
    if (!text || text === "") return "Untitled";
    if (text.length > maxNum) return `${text.slice(0, maxNum)}...`;

    return text;
  }

  return (
    <>
      <Head>
        <title>{`${cutTitleWhenTooLong(chat.topic, 50)} - 뤼튼`}</title>
        <meta charSet="utf-8" />
        <meta name="description" content={chat.messages.map(message => message.content).join(", ")} />
        <meta property="og:title" content={`${cutTitleWhenTooLong(chat.topic, 50)} - 뤼튼`} />
        <meta property="og:description" content={chat.messages[1].content} />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`${baseURL}/share/${chat.shareId}`} />
        <meta property="og:image" content={ogImage} />
        <meta property="og:locale" content="ko_KR" />
      </Head>
      <ShareChatLayout>
        <LeftArea>
          <Link href="/share">
            <CircleButton label="목록 보기">
              <Icon icon="arrow-left" size={20}/>
            </CircleButton>
          </Link>
        </LeftArea>
        <MessagesArea>
          {messages.map((message) => {
            const isInputMessage = !Array.isArray(message) && message.role === "user";
            if (isInputMessage) {
              return <InputMessage message={message} userName={chat.userName ?? ""}/>
            } else {
              const messages = Array.isArray(message) ? message : [message];
              const isImageOutput = messages[0].type === "ImageOutput" && messages[0].image;
              if (isImageOutput) {
                return <ImageOutputMessage messages={messages} />
              }

              return <OutputMessage messages={messages} />
            }
          })}
        </MessagesArea>
        <RightArea>
          <ShareCircle />
          <TrashCircle chatId={chat._id} userId={chat.userId} originalId={chat.originalId}/>
          <ViewCircle chatId={chat._id} userId={chat.userId} shareId={chat.shareId} />
          <HeartCircle chatId={chat._id} likes={chat.likes ?? 0} />
        </RightArea>
        <FixedArea isShow={showTitleCard}>
          {!chat.topic && <TitleArea>(제목없음)</TitleArea>}
          {chat.topic && <TitleArea>{cutTitleWhenTooLong(chat.topic, 40)}</TitleArea>}
          <UserInfoArea>
            <NiknameArea>{(chat.userName ?? "").slice(0, 1)}</NiknameArea>
            <div>{chat.userName}</div>
            <div>·</div>
            <div>
              <Date dateString={chat.createdAt}/>
            </div>
          </UserInfoArea>
        </FixedArea>
      </ShareChatLayout>
    </>
  )
}

ShareChatPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <ChannelLayout>
              <MainLayout>
                {page}
              </MainLayout>
            </ChannelLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  )
}

export const getStaticPaths: GetStaticPaths = async (ctx) => {
  try {
    const res = await getShareChats();
    const { chats, totalCount } = res.data.data;

    return {
      paths: chats.map(chat => ({ params: { chatId: chat.shareId }})),
      fallback: true,
    }
  } catch(err) {
    return {
      paths: [],
      fallback: false,
    }
  }
}

export const getStaticProps: GetStaticProps = async (ctx)  => {
  const chatId = ctx.params?.chatId;

  try {
    if(chatId){
      const res = await getShareChat(String(chatId));
      const chat = res.data.data;

      return { props: { chat, ogImage: encodeURI(`${baseURL}/api/og?thumbnail=${chat.userName[0]}&title=${chat.topic}&description=${chat.messages[1].content}`) } };
    } else{
      return { props: {chat: null, ogImage: '' }};
    }
    
  } catch (err) {
    return { props: { chat: null, ogImage: '' }};
  }
}

const ShareChatLayout = styled(FlexWrapper)`
  flex-direction: row;
  margin-top: 24px;
  margin-bottom: 50px;
  justify-content: center;
  align-items: flex-start;
  padding-right: 10%;
  padding-left: 10%;

  @media (max-width: 767px) {
    padding-right: 20px;
    padding-left: 20px;
  }
`

const MessagesArea = styled(FlexWrapper)`
  flex-direction: column;
  gap: 20px;
  margin: 0 30px 200px 30px;

  @media (max-width: 767px) {
    margin: 0 0 160px 0;
    width: 100%;
  }
`

const LeftArea = styled(FlexWrapper)`
  position: fixed;
  z-index: 10;
  left: calc(50% - 460px);

  @media (max-width: 767px) {
    display: none;
  }
`

const RightArea = styled(FlexWrapper)`
  position: fixed;
  z-index: 10;
  right: calc(50% - 460px);

  flex-direction: column;
  gap: 15px;

  @media (max-width: 767px) {
    display: none;
  }
`

const FixedArea = styled(FlexWrapper)<{ isShow: boolean }>`
  border: 1px solid ${newColors.PURPLE_500_PRIMARY};
  border-radius: 10px;
  width: 480px;
  height: 96px;
  background-color: white;
  flex-direction: column;
  justify-content: center;
  position: fixed;
  bottom: 50px;
  gap: 20px;
  box-shadow: 0px 4px 20px rgba(57, 68, 112, 0.1);
  opacity: ${({ isShow }) => isShow ? "1" : "0"};
  transition: opacity 0.3s ease-in-out;

  @media (max-width: 767px) {
    width: 300px;
    padding: 20px;
    text-align: center;
  }
`

const UserInfoArea = styled(FlexWrapper)`
  flex-direction: row;
  gap: 10px;

  ${typo({
    color: newColors.GRAY_700,
    size: "14px",
    weight: 500,
  })}
`

const TitleArea = styled.div`
  text-align: center;
  padding: 0 10px;
  ${typo({
    color: newColors.PURPLE_500_PRIMARY,
    weight: 700,
    size: "14px"
  })}
`

const NiknameArea = styled.span`
  background-color: #C8CAFF;
  height: 20px;
  width: 20px;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  align-self: flex-start;
  
  ${typo({
    color: newColors.WHITE,
    size: "12px",
    weight: 500,
  })}
`

const EmptyMessage = styled.span`
  width: 760px;
  text-align: center;
  padding-top: 20px;

  @media (max-width: 767px) {
    width: 100%;
  }

  ${typo({
    color: newColors.GRAY_700,
    size: "16px",
    weight: 500,
  })}
`