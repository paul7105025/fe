import React, { ReactElement } from "react";
import {
  AuthCheckLayout,
  ChatLNBLayout,
  GlobalModalLayout,
  MainDarkLayout,
  MainLayout,
} from "../layouts";
import styled from "styled-components";
import { useAlert } from "react-alert";

import Head from "next/head";

import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import { ParsedUrlQuery } from "querystring";
import { withAuthCheck } from "src/hoc";
import { EventLayout } from "src/layouts/EventLayout";
import { colors, FlexWrapper, newColors, newTypo } from "@wrtn/ui";
import { ReactComponent as WindowIcon } from "src/icons/window.svg";
import { ReactComponent as MacIcon } from "src/icons/mac.svg";
import { ReactComponent as DesktopApp } from "src/icons/desktopApp.svg";
import { ReactComponent as DesktopAppMobile1 } from "src/icons/desktopMobile1.svg";
import { ReactComponent as DesktopAppMobile2 } from "src/icons/desktopMobile2.svg";
import { ReactComponent as IssueIcon } from "src/icons/issue.svg";

import {
  WRTN_MAC_HREF,
  WRTN_WIDNOW_HREF,
  showMessenger,
  useIs480,
  useEvent,
} from "@wrtn/core";
import useInitAuth from "src/hooks/auth/useInitAuth";

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px;
  gap: 10px;
  margin: 100px auto 0 auto;
  height: 142px;
`;

const BetaTitle = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 4px 6px;
  gap: 4px;
  width: 47px;
  height: 28px;
  left: 479px;
  top: 7px;
  color: #6446ff;
  background: #f3f0ff;
  border-radius: 5px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 100%;
  margin-left: 8px;
  margin-top: 8px;
`;

const MobileBetaTitle = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 4px 6px;
  gap: 4px;
  width: 47px;
  height: 28px;
  left: 479px;
  top: 7px;
  color: #6446ff;
  background: #f3f0ff;
  border-radius: 5px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 100%;
`;

const TitleFirstWrapper = styled.div`
  display: flex;
`;

const TitleWrapperMobile = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px;
  gap: 10px;
  height: 142px;
  margin: 48px auto 0 auto;
`;

const Title = styled.div`
  display: flex;
  height: 62px;

  /* Content/48-bold */

  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 48px;
  line-height: 130%;
  /* or 62px */

  text-align: center;

  /* Black */
  /* Inside auto layout */

  flex: none;
  order: 0;
  flex-grow: 0;
`;

const TitleMobile = styled.div`
  display: flex;
  height: 45px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 32px;
  line-height: 140%;
  text-align: center;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const Purple = styled.span`
  color: ${colors.POINT_PURPLE};
`;

const DescriptionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px 0px;
  gap: 10px;
  height: 70px;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const Description = styled.div`
  height: 20px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 100%;
  text-align: center;
  color: #717688;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const DescriptionMobile = styled.div`
  width: 320px;
  height: 81px;
  margin: 10px auto 0 auto;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 15px;
  line-height: 180%;
  /* or 27px */

  /* Gray_600 */

  color: #717688;

  /* Inside auto layout */

  flex: none;
  order: 1;
  align-self: stretch;
  flex-grow: 0;
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 16px;
  height: 40px;
  margin: 42px auto 0 auto;
  justify-content: center;
  width: 437px;
`;

const MobileButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 0px;
  gap: 16px;
  margin: 56px auto 0 auto;
  height: 96px;
  width: 320px;
  padding-bottom: 16px;
`;

const Button = styled.button<{selected?:boolean}>`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px;
  gap: 10px;
  cursor: pointer;

  width: 210.5px;
  height: 40px;

  background: ${props => props.selected ? "#F7F6FF" : "#ffffff"};
  /* Purple_500_primary */

  border: 1px solid #6446ff;
  border-radius: 5px;

  /* Inside auto layout */

  flex: none;
  order: 0;
  flex-grow: 1;
`;

const MobileButton = styled.button<{selected?:boolean}>`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px;
  gap: 10px;
  cursor: pointer;
  width: 320px;
  height: 40px;
  background: #ffffff;
  border: 1px solid #6446ff;
  border-radius: 5px;

  /* Inside auto layout */

  flex: none;
  order: 0;
  align-self: stretch;
  flex-grow: 0;
`;

const ButtonText = styled.div`
  height: 16px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;
  color: #6446ff;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const DesktopAppImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 64px auto 0 auto;
`;

const DesktopMobile1Wrapper = styled.div`
  margin-top: 64px;
  display: flex;
  justify-content: flex-end;
`;

const DesktopMobile2Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
`;

const IssueWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 8px;
  width: 244px;
  height: 26px;
  margin: 16px auto 0 auto;
  // padding-bottom: 56px;
`;

const MobileIssueWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 8px;
  width: 244px;
  height: 26px;
  margin: 16px auto 0 auto;
  padding-bottom: 56px;
`;

const IssueText = styled.div`
  width: 216px;
  height: 26px;

  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 26px;
  /* identical to box height, or 186% */

  text-align: right;

  /* Gray_500 */

  color: #8f95ab;

  /* Inside auto layout */

  flex: none;
  order: 1;
  flex-grow: 0;
`;

const TermLink = styled.a`
  text-decoration: underline;
  cursor: pointer;
`;

const DesktopPage = (props: any) => {
  const isMobile = useIs480();
  const alert = useAlert();
  const { collectEvent } = useEvent();

  const [selectedMac, setSelectedMac] = React.useState(false);

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  React.useEffect(() => {
    collectEvent("view_download_desktop");
  }, [])

  const onDownloadWin = () => {
    window.open("https://download.wrtn.ai/wrtn Setup 1.0.8.exe")
  }

  const onDownloadMacIntel = () => {
    window.open("https://download.wrtn.ai/wrtn-1.0.8.pkg")
  }

  const onDownloadMacArm = () => {
    window.open("https://download.wrtn.ai/wrtn-1.0.8-arm64.pkg")
  }

  return (
    <>
      {isMobile ? (
        <>
          <TitleWrapperMobile>
            <MobileBetaTitle>베타</MobileBetaTitle>

            <TitleMobile>
              {" "}
              Window & Mac용 &nbsp;<Purple>뤼튼</Purple>
            </TitleMobile>
            <DescriptionMobile>
              {" "}
              20만 사용자가 선택한 대한민국 AI 플랫폼을 데스크탑앱에서도! AI
              채팅, AI 이미지 생성 기능을 누구나 사용할 수 있어요.
            </DescriptionMobile>
          </TitleWrapperMobile>
          <DesktopMobile1Wrapper>
            <DesktopAppMobile1 />
          </DesktopMobile1Wrapper>
          <DesktopMobile2Wrapper>
            <DesktopAppMobile2 />
          </DesktopMobile2Wrapper>
          <MobileButtonWrapper>
            {/* <a href={WRTN_WIDNOW_HREF} target="_blank"> */}
            <MobileButton onClick={() => onDownloadWin()}>
              <WindowIcon />
              <ButtonText>Window</ButtonText>
            </MobileButton>
            {/* </a> */}
            <MobileButton onClick={() => setSelectedMac(c => !c)} selected={selectedMac}>
              <MacIcon />
              <ButtonText>Mac</ButtonText>
              { selectedMac && 
                <MacButtonWrapper column align="center" justify="center">
                  <MacButton align="center" justify="center" onClick={() => onDownloadMacArm()}>
                    Apple Silicon(M1 / M2)
                  </MacButton>
                  <MacButton align="center" justify="center" onClick={() => onDownloadMacIntel()}>
                    Intel 프로세서
                  </MacButton>
                </MacButtonWrapper>
              }
            </MobileButton>
            {/* </a> */}
          </MobileButtonWrapper>
          <MobileIssueWrapper>
            <IssueIcon />
            <IssueText>
              베타버전의 이슈는{" "}
              <TermLink
                onClick={() => {
                  showMessenger();
                }}
              >
                이곳
              </TermLink>
              에 제보해주세요
            </IssueText>
          </MobileIssueWrapper>
        </>
      ) : (
        <>
          <TitleWrapper>
            <TitleFirstWrapper>
              <Title>
                Window & Mac용 &nbsp;<Purple>뤼튼</Purple>
              </Title>
              <BetaTitle>베타</BetaTitle>
            </TitleFirstWrapper>
            <DescriptionWrapper>
              <Description>
                20만 사용자가 선택한 대한민국 AI 플랫폼을 데스크탑앱에서도!
              </Description>
              <Description>
                AI 채팅, AI 이미지 생성 기능을 누구나 사용할 수 있어요.
              </Description>
            </DescriptionWrapper>
          </TitleWrapper>
          <ButtonWrapper>
            {/* <a href={WRTN_WIDNOW_HREF} target="_blank"> */}
            <Button onClick={() => onDownloadWin()}>
              <WindowIcon />
              <ButtonText>Window</ButtonText>
            </Button>
            {/* </a> */}
            <Button onClick={() => setSelectedMac(c => !c)} selected={selectedMac}>
              <MacIcon />
              <ButtonText>Mac</ButtonText>
              { selectedMac && 
                <MacButtonWrapper column align="center" justify="center">
                  <MacButton align="center" justify="center" onClick={() => onDownloadMacArm()}>
                    Apple Silicon(M1 / M2)
                  </MacButton>
                  <MacButton align="center" justify="center" onClick={() => onDownloadMacIntel()}>
                    Intel 프로세서
                  </MacButton>
                </MacButtonWrapper>
              }
            </Button>
            {/* </a> */}
          </ButtonWrapper>
          <IssueWrapper>
            <IssueIcon />
            <IssueText>
              베타버전의 이슈는{" "}
              <TermLink
                onClick={() => {
                  showMessenger();
                }}
              >
                이곳
              </TermLink>
              에 제보해주세요
            </IssueText>
          </IssueWrapper>
          <DesktopAppImageWrapper>
            <DesktopApp />
          </DesktopAppImageWrapper>
        </>
      )}
    </>
  );
};

export default DesktopPage;

DesktopPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai/desktop" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <MainLayout>{page}</MainLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};


const MacButton = styled(FlexWrapper)`
  height: 40px;
  width: 100%;
  ${newTypo("content-16-semi")};
  color: ${newColors.GRAY_700};
  background-color: ${newColors.WHITE};
  &:hover {
    background-color: #F7F6FF;
  }
`;

const MacButtonWrapper = styled(FlexWrapper)`
  top:40px;
  position: absolute;
  border-radius: 8px;
  border: 1px solid ${newColors.GRAY_200};
  height: 96px;
  width: 100%;
`;