import React, { useState } from "react";
import { AuthCheckLayout, MainDarkLayout } from "../layouts";
import styled, { keyframes } from "styled-components";
import Symbol from "/public/plugin/background1.svg";
import Logo from "/public/plugin/logo.svg";
import LeftIcon from "/public/plugin/left_icon.svg";
import RightIcon from "/public/plugin/right_icon.svg";
import IPhoneIcon from "/public/plugin/iphone.png";
import { ReactComponent as ArrowUpIcon } from "src/icons/arrow_up.svg";

import { FlexWrapper, colors, typo } from "@wrtn/ui";
import Link from "next/link";
import Head from "next/head";
import {
  useLoginDialog,
  postPluginWaitlist,
  pluginWaitlisterState,
  getPluginWaitlist,
  WRTN_MARKETING_POLICY_HREF,
  useEvent,
} from "@wrtn/core";
import { useUser } from "../hooks/useUser";
import { useRouter } from "next/router";
import { usePluginWaitlister } from "src/hooks/usePluginWaitlister";
import { useSetRecoilState } from "recoil";
import { useAlert } from "react-alert";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { Dialog } from "@wrtn/ui/components/Dialog";
import useInitAuth from "src/hooks/auth/useInitAuth";
import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import { ParsedUrlQuery } from "querystring";
import { withAuthCheck } from "src/hoc";

export const WRTN_USER_GUIDE_HREF = "https://wrtn.imweb.me/";
export const WRTN_PRIVACY_POLICY_HREF = "https://wrtn.ai/privacy/";

const section = [
  {
    name: "지식",
    player: [{
      logo: "/plugin/partners/dbpia.png",
      name: "디비피아"
    }],
  },
  {
    name: "부동산",
    player: [{
      logo: "/plugin/partners/zigbang.png",
      name: "직방"
    }],
  },
  {
    name: "의료",
    player: [{
      logo: "/plugin/partners/doctornow.png",
      name: "닥터나우"
    }, {
      logo: "/plugin/partners/gangnamsister.png",
      name: "강남언니"
    }],
  },
  {
    name: "이커머스",
    player: [{
      logo: "/plugin/partners/gmarket.png",
      name: "G마켓"
    }],
  },
  {
    name: "금융",
    player: [{
      logo: "/plugin/partners/hanabank.png",
      name: "하나은행"
    },{
      logo: "/plugin/partners/kb.png",
      name: "KB 금융그룹"
    }],
  },
  {
    name: "홈쇼핑",
    player: [{
      logo: "/plugin/partners/shinsegae.png",
      name: "신세계 라이브쇼핑"
    }],
  },
  {
    name: "구인구직",
    player: [{
      logo: "/plugin/partners/freemoa.png",
      name: "프리모아"
    }, {
      logo: "/plugin/partners/wanted.png",
      name: "원티드"
    }],
  },
  {
    name: "맛집 검색",
    player: [{
      logo: "/plugin/partners/siksin.png",
      name: "식신"
    }],
  },
  {
    name: "뷰티",
    player: [{
      logo: "/plugin/partners/amorepacific.png",
      name: "아모레퍼시픽"
    }],
  },
  {
    name: "패션",
    player: [{
      logo: "/plugin/partners/raportlabs.png",
      name: "라포랩스"
    }],
  },
  {
    name: "숙박",
    player: [{
      logo: "/plugin/partners/allstay.png",
      name: "올스테이"
    }],
  },
  {
    name: "행사 / 이벤트",
    player: [{
      logo: "/plugin/partners/onoffmix.png",
      name: "온오프믹스"
    }],
  },
  {
    name: "법률",
    player: [{
      logo: "/plugin/partners/lawgood.png",
      name: "로앤굿"
    }],
  },
  {
    name: "교육/강의",
    player: [{
      logo: "/plugin/partners/udemy.png",
      name: "유데미",
    },{
      logo: "/plugin/partners/wanted.png",
      name: "원티드"
    }],
  },
  {
    name: "건강",
    player: [{
      logo: "/plugin/partners/planfit.png",
      name: "플랜핏"
    }]
  },
  {
    name: "여행",
    player: [{
      logo: "/plugin/partners/myrealtrip.png",
      name: "마이리얼트립"
    }]
  },
  {
    name: "모빌리티",
    player: [{
      logo: "/plugin/partners/tada.png",
      name: "타다"
    }]
  },
];

const PluginPage = (props: any) => {
  const [images, setImages] = React.useState<HTMLImageElement[]>([]);
  const ref = React.useRef<HTMLDivElement | null>(null);
  const [_height, setHeight] = React.useState(10000);
  const loginDialog = useLoginDialog();
  const { user } = useUser();
  const { pluginWaitlister } = usePluginWaitlister();
  const [waitlister, setWaitlister] = useState(pluginWaitlister);
  const setPluginWaitlisterState = useSetRecoilState(pluginWaitlisterState);
  const router = useRouter();
  const [modalOpen, setModalOpen] = useState(false);
  const { collectEvent } = useEvent();

  function CopyToClipBoard() {
    const userId = user?._id;

    if (userId) {
      const base64EncodedUserId = Buffer.from(userId).toString("base64");
      const link = `${process.env.NEXT_PUBLIC_PLUGIN_URL}/?inviter=${base64EncodedUserId}`;
      navigator.clipboard.writeText(link);
      setModalOpen(true);
    } else {
      return null;
    }
  }

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  function preloadImages() {
    let images = [Symbol, Logo, LeftIcon, RightIcon, IPhoneIcon];
    for (let i = 0; i <= images.length; i++) {
      const img = new Image();
      const imgSrc = images[i];
      //@ts-ignore
      img.src = imgSrc;
      setImages((prevImages) => [...prevImages, img]);
    }
  }

  const RegisterPluginWaitlist = async ({ user }) => {
    const inviter = sessionStorage.getItem("plugin_invite");
    if (inviter) {
      const res = await postPluginWaitlist({
        waitlisterId: user._id,
        inviterId: Buffer.from(inviter, "base64").toString() || undefined,
      });
      if (res?.status === 200 || res?.status === 201) {
        const { data } = res.data;
        setPluginWaitlisterState({
          _id: data._id,
          rank: data.rank,
          waitlisterId: data.waitlisterId,
          inviterId: Buffer.from(inviter, "base64").toString(),
        });
      }
    } else {
      const res = await postPluginWaitlist({
        waitlisterId: user._id,
      });
      if (res?.status === 200 || res?.status === 201) {
        const { data } = res.data;
        setPluginWaitlisterState({
          _id: data._id,
          rank: data.rank,
          waitlisterId: data.waitlisterId,
        });
      }
    }
  };

  React.useEffect(() => {
    collectEvent('view_plugin_page')
  }, [])

  React.useEffect(() => {
    preloadImages();
  }, []);

  React.useEffect(() => {
    async function fetchPluginWaitlist(user) {
      if (user) {
        const waitlistData = await getPluginWaitlist({
          waitlisterId: user._id,
        });
        const { data } = waitlistData.data;
        setWaitlister(data);
      }
    }
    fetchPluginWaitlist(user);
  }, [pluginWaitlister, user]);

  // React.useEffect(() => {
  //   if (images.length > 1) {
  //     const canvas: HTMLCanvasElement | null =
  //       document.querySelector("#canvas");
  //     const ctx = canvas!.getContext("2d");

  //     if (ctx && canvas) {
  //       const render = () => {
  //         const text = "나를 가장 잘 이해하는 AI"; // 적용할 텍스트
  //         let fontSize = window.innerWidth < 1024 ? 50 : 100; // 시작 폰트 크기
  //         let opacity = 1; // 시작 투명도
  //         const dpr = window.devicePixelRatio;

  //         ctx.textAlign = "center";
  //         let width = (canvas.width = window.innerWidth * dpr);
  //         let height = (canvas.height = window.innerHeight * dpr);

  //         ctx.fillStyle = "#000";
  //         ctx.fillRect(0, 0, width, height);
  //         //ctx.drawImage(images[0], 0, 0); // 캔버스에 이미지 그리기
  //         ctx.drawImage(images[0], 0, 0);

  //         ctx.font = "bold " + fontSize + "px Pretendard";
  //         ctx.fillStyle = "white"; // 적용할 폰트 색상

  //         ctx.textAlign = "center";

  //         ctx.fillText(text, width / 2 / dpr, height / 2 / dpr);

  //         window.addEventListener("scroll", () => {
  //           const scrollHeight = document.documentElement.scrollTop;
  //           if (scrollHeight <= 1024) {
  //             //ctx.clearRect(0, 0, width, height);
  //             if (_height !== 10000) {
  //               width = canvas.width = window.outerWidth * dpr;
  //               height = canvas.height = window.outerHeight * dpr;

  //               if (window.innerWidth >= 768) {
  //                 setHeight(1);
  //               }
  //             }

  //             ctx.fillStyle = "#000";
  //             ctx.fillRect(0, 0, width, height);
  //             ctx.drawImage(images[0], 0, 0);

  //             fontSize =
  //               (window.innerWidth < 1024 ? 50 : 100) + scrollHeight / 5;
  //             opacity = 1 - (scrollHeight + 1) / 1000;
  //             ctx.font = "bold " + fontSize + "px Pretendard";
  //             ctx.fillStyle = "rgba(255, 255, 255, " + opacity + ")";
  //             ctx.fillText(text, width / 2 / dpr, height / 2 / dpr);
  //           } else if (scrollHeight > 1024 && scrollHeight <= 1227) {
  //             if (window.innerWidth >= 768) {
  //               setHeight(1);
  //             }
  //             const relativeScrollHeight = scrollHeight - 1024;
  //             ctx.fillStyle = "#000";
  //             ctx.fillRect(0, 0, width, height);
  //             const gradient = ctx.createLinearGradient(
  //               0,
  //               0,
  //               canvas.width,
  //               canvas.height
  //             );

  //             let opacity =
  //               parseFloat((relativeScrollHeight / scrollHeight).toFixed(2)) *
  //               5;
  //             gradient.addColorStop(0, `rgba(19,1,64, ${opacity})`);
  //             gradient.addColorStop(1, `rgba(42,26,169, ${opacity})`);

  //             ctx.fillStyle = gradient;
  //             ctx.fillRect(0, 0, canvas.width, canvas.height);

  //             const logoHeight =
  //               scrollHeight < 1277
  //                 ? height / 2 / dpr / opacity
  //                 : height / 2 / dpr;
  //             const x =
  //               window.innerWidth < 768
  //                 ? (width / dpr - images[1].width / 3) / 2
  //                 : width / dpr / 2 - images[1].width / 2;
  //             const y = logoHeight;
  //             const w = images[1].width;
  //             const h = images[1].height;

  //             ctx.drawImage(
  //               images[1],
  //               x,
  //               y,
  //               window.innerWidth < 768 ? w / 3 : w,
  //               window.innerWidth < 768 ? h / 3 : h
  //             );
  //           } else if (scrollHeight > 1227 && scrollHeight <= 2200) {
  //             const relativeCurrentScrollHeight = scrollHeight - 1227;
  //             3;
  //             ctx.fillStyle = "#000";
  //             ctx.fillRect(0, 0, width, height);
  //             const gradient = ctx.createLinearGradient(
  //               0,
  //               0,
  //               canvas.width,
  //               canvas.height
  //             );

  //             let opacity =
  //               parseFloat(
  //                 (relativeCurrentScrollHeight / scrollHeight).toFixed(2)
  //               ) * 5;
  //             gradient.addColorStop(0, `rgba(19,1,64, ${1 - opacity})`);
  //             gradient.addColorStop(1, `rgba(42,26,169, ${1 - opacity})`);

  //             ctx.fillStyle = gradient;
  //             ctx.fillRect(0, 0, canvas.width, canvas.height);

  //             const logoHeight = height / 3 / dpr / opacity;

  //             const currentPaercentage =
  //               (2200 - relativeCurrentScrollHeight) / 2200;
  //             const x =
  //               window.innerWidth < 768
  //                 ? (width / dpr - images[1].width / 3) / 2
  //                 : width / dpr / 2 -
  //                   (images[1].width / 2) * currentPaercentage;
  //             const y = logoHeight;
  //             const w = images[1].width;
  //             const h = images[1].height;
  //             ctx.drawImage(
  //               images[1],
  //               x,
  //               logoHeight > 790.5 ? 790.5 : y,
  //               window.innerWidth < 768 ? w / 3 : w * currentPaercentage,
  //               window.innerWidth < 768 ? h / 3 : h * currentPaercentage
  //             );
  //           } else if (scrollHeight > 2400 && scrollHeight <= 3280) {
  //             setHeight(0);
  //           }
  //         });
  //       };
  //       requestAnimationFrame(render);
  //     }
  //   } else if (window.innerWidth < 768) {
  //     setHeight(0);
  //   }
  // }, [images]);

  React.useEffect(() => {
    window.sessionStorage.removeItem("login_app");
    const query = new URLSearchParams(location.search);
    const inviter = query.get("inviter");
    window.sessionStorage.setItem("pluginPath", router.pathname);

    if (inviter) {
      window.sessionStorage.setItem("plugin_invite", inviter);
    }
  }, []);

  return (
    <RealWrapper>
      <Content>
        <ContentAbsolute>
          <LogoImage src={Logo} />
          <Subtitle>
            AI 어시스턴트 개인화를 위한 뤼튼의 첫 걸음
          </Subtitle>
        </ContentAbsolute>
        {/* <Mobile src={MobileIcon} /> */}
        <Left src={LeftIcon} />
        <Center src="/plugin/iphone2.png" />
        <Right src={RightIcon} />
        <CaptionButton>2023 상반기 출시 예정</CaptionButton>
      </Content>
      <Section2>
        <SectionText>
          사용자 의도를 파악해 <br />
          최적의 서비스에 연결합니다.
        </SectionText>
        <SectionSubtext>
          대화를 분석해 최적화된 서비스를 제공합니다.
        </SectionSubtext>
        <Section2Content>
          <GroupWrapper>
            <GroupImage src="/plugin/content1.svg" />
              <GroupContent>
                <ChipWrapper>
                  <Chip>
                    <ChipLabel>
                      지메일
                    </ChipLabel>
                    <ChipIcon src="/plugin/logo/gmail.svg" />
                  </Chip>
                  <Chip>
                    <ChipLabel>
                      구글캘린더
                    </ChipLabel>
                    <ChipIcon src="/plugin/logo/calendar.svg" />
                  </Chip>
                </ChipWrapper>
                <CTitle>
                  성공적인 여행을 위한 <br />
                  최고의 플래너
                </CTitle>
                <CSubtitle>
                  숙박부터 액티비티까지, 모든 여정을 함께하세요. <br />
                  뤼튼과 여행의 문을 열고 새로운 경험을 만끽해보세요.
                </CSubtitle>
              </GroupContent>
            </GroupWrapper>
            <GroupOddWrapper>
              <GroupContent>
                <ChipWrapper>
                  <Chip>
                    <ChipLabel>
                      DBPIA
                    </ChipLabel>
                    <ChipIcon src="/plugin/logo/dbpia.svg" />
                  </Chip>
                  <Chip>
                    <ChipLabel>
                      원티드
                    </ChipLabel>
                    <ChipIcon src="/plugin/logo/wanted.svg" />
                  </Chip>
                </ChipWrapper>
                <CTitle>
                  당신의 가치를 높이는<br/>
                  유능한 파트너
                </CTitle>
                <CSubtitle>
                  필요한 논문부터 최신 논문까지 한 눈에 확인하세요.<br/>
                  당신의 성공적인 구인구직 뤼튼과 함께하세요.
                </CSubtitle>
              </GroupContent>
              <GroupImage src="/plugin/content2.svg" />
            </GroupOddWrapper>
            <GroupWrapper>
              <GroupImage src="/plugin/content3.svg" />
                <GroupContent>
                  <ChipWrapper>
                    <Chip>
                      <ChipLabel>
                        올스테이
                      </ChipLabel>
                      <ChipIcon src="/plugin/logo/allstay.svg" />
                    </Chip>
                  </ChipWrapper>
                  <CTitle>
                    언제 어디서나 <br />
                    당신의 생산성을 최대로
                  </CTitle>
                  <CSubtitle>
                    빠르게 메일을 찾고 필요한 내용을 요약하세요. <br />
                    캘린더로 당신의 일정을 체계적으로 관리하세요.
                  </CSubtitle>
                </GroupContent>
              </GroupWrapper>
{/*               
          {window && window?.innerWidth < 768 ? (
            <Slider>
              <ServiceImage src="/plugin/service1.png" />
              <ServiceImage src="/plugin/service3.png" />
              <ServiceImage src="/plugin/service5.png" />
              <ServiceImage src="/plugin/service2.png" />
              <ServiceImage src="/plugin/service4.png" />
              <ServiceImage src="/plugin/service6.png" />
            </Slider>
          ) : (
            <>
              <RowWrapper>
                <ServiceImage src="/plugin/service1.png" />
                <ServiceImage src="/plugin/service3.png" />
                <ServiceImage src="/plugin/service5.png" />
              </RowWrapper>
              <RowWrapper>
                <ServiceImage src="/plugin/service2.png" />
                <ServiceImage src="/plugin/service4.png" />
                <ServiceImage src="/plugin/service6.png" />
              </RowWrapper>
            </>
          )} */}
        </Section2Content>
      </Section2>
      <Section3>
        <SectionText>
          이미 다양한 분야의 서비스들이 <br />
          뤼튼과의 연결을 준비하고 있습니다.
        </SectionText>
        <Section3ContentBox>
          <Logo2 src="/plugin/logo2.svg" />
          {section.map((item, idx) => {
            return (
              <SectionCard>
                <SectionName>{item.name}</SectionName>
                <SectionGroupBox>
                  {item.player.map((p, ip) => {
                    return (
                      <SectionWrapper>
                        <SectionBox src={p.logo} />
                        <SectionName2>{p.name}</SectionName2>
                      </SectionWrapper>
                    );
                  })}
                </SectionGroupBox>
              </SectionCard>
            );
          })}
        </Section3ContentBox>
        <PluginText>뤼튼 플러그인</PluginText>
        <PluginSubText>
          개인화된 AI 어시스턴트를 위한
          <MobileBr /> 뤼튼의 첫 걸음
        </PluginSubText>
        {!user ? (
          <>
            <ApplyButtonWrapper>
              <ApplyButtonForIndividual
                onClick={() => loginDialog.handleOpen()}
              >
                대기자 명단 등록
              </ApplyButtonForIndividual>
            </ApplyButtonWrapper>
            <MarkettingAgreeText>
              대기자 명단에 등록하시면{" "}
              <TermLink
                href={WRTN_MARKETING_POLICY_HREF}
                target="_blank"
                rel="noopener noreferrer"
              >
                마케팅 활용에 동의
              </TermLink>
              한 것으로 간주됩니다.
            </MarkettingAgreeText>
          </>
        ) : user && !pluginWaitlister && !waitlister ? (
          <>
            <ApplyButtonWrapper
              onClick={() => RegisterPluginWaitlist({ user })}
            >
              <ApplyButtonForIndividual>
                대기자 명단 등록
              </ApplyButtonForIndividual>
            </ApplyButtonWrapper>
            <MarkettingAgreeText>
              대기자 명단에 등록하시면{" "}
              <TermLink
                href={WRTN_MARKETING_POLICY_HREF}
                target="_blank"
                rel="noopener noreferrer"
              >
                마케팅 활용에 동의
              </TermLink>
              한 것으로 간주됩니다.
            </MarkettingAgreeText>
          </>
        ) : null}
        {user && waitlister && (
          <>
            <RankWrapper>
              <RankText>내 대기번호</RankText>
              <RankNum>{waitlister.rank}등</RankNum>
            </RankWrapper>
            <PreOpenWrapper>
              <FirstOpen>1차 오픈: 300명</FirstOpen>
              <SecondandThirdOpen>2차 오픈: 미정</SecondandThirdOpen>
              <SecondandThirdOpen>3차 오픈: 미정</SecondandThirdOpen>
            </PreOpenWrapper>
            <RankUpgradeButtonWrapper onClick={CopyToClipBoard}>
              <RankUpgradeButton>100등 올리기</RankUpgradeButton>
              <ArrowUpIcon />
            </RankUpgradeButtonWrapper>
            {modalOpen && (
              <ModalPortal>
                <Dialog
                  width={395}
                  iconType="link"
                  title={"초대 링크가 복사되었습니다."}
                  description={[
                    "초대링크를 받은 친구가 대기자로 등록하면 ",
                    "100등을 올려드려요!",
                  ]}
                  handleClose={() => setModalOpen(false)}
                  rightButtonLabel="확인"
                  handleRightButton={() => {
                    setModalOpen(false);
                    router.push("/plugins");
                  }}
                  disableCancel={true}
                />
              </ModalPortal>
            )}
            <RankUpgradeText>
              등수 올리기 이벤트에 따라 현재 등수가 앞당겨질수도 밀려날수도
              있어요.
            </RankUpgradeText>
          </>
        )}
        <DivideLine />
        <CaptionHeader>기업</CaptionHeader>
        <ApplyButtonCompanyWrapper>
          <ApplyButton
            onClick={() =>
              window?.open("https://wrtn.typeform.com/to/xcJCRb3r")
            }
          >
            플러그인 기업 제휴 신청
          </ApplyButton>
        </ApplyButtonCompanyWrapper>
        <Caption>
          신청 기업 수에 따라 제휴 검토가 지연될 수 있습니다. 순차적으로
          검토 후 <br /> 회신 드리고 있으니 양해 바랍니다.
        </Caption>
      </Section3>
      <FooterWrapper>
        <Footer>
          <FooterText>
            주식회사 뤼튼테크놀로지스 I 대표 이세영 | 홍보 및 대외협력
            pr@wrtn.io
            <br />
            사업자 등록번호 : 202-81-67042 I 통신판매업 신고번호 :
            2021-서울마포-3504
          </FooterText>
          <FooterTextRight>
            <FooterLink
              href={WRTN_USER_GUIDE_HREF}
              target="_blank"
              rel="noopener noreferrer"
            >
              사용자가이드
            </FooterLink>
            <FooterLink
              href={WRTN_PRIVACY_POLICY_HREF}
              target="_blank"
              rel="noopener noreferrer"
            >
              개인정보처리방침
            </FooterLink>

            <br />
            <b>ⓒ 2023. Wrtn Technologies, Inc.</b>
          </FooterTextRight>
        </Footer>
      </FooterWrapper>
    </RealWrapper>
  );
};

export default PluginPage;

PluginPage.getLayout = (page: React.ReactElement) => {
  return (
    <>
      <Head>
        <title>뤼튼 플러그인</title>
        <meta property="og:title" content="귀찮은 일 다 해주는 AI 비서" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai/plugins" />
        <meta
          property="og:description"
          content="대기자로 등록하고 무료로 사용해보세요 "
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/2zY4C4.jpg" />
      </Head>
      <AuthCheckLayout>
        <MainDarkLayout>{page}</MainDarkLayout>
      </AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Canvas = styled.canvas`
  position: sticky;
  top: 0px;
  left: 0px;
`;
const TermLink = styled.a`
  text-decoration: underline;
  color: #adb2c5;
`;

const Wrapper = styled.div`
  height: 10000px;
  @media (max-width: 768px) {
    height: 0px;
    display: none;
  }
`;

const RealWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  z-index: 32;
  background: #070314;
  overflow: auto;
  /* @media(max-width: 767px){
    position: relative;
    height: auto;
    display: block;;
    top: 0;
  } */
  /* @media (max-width: 768px) {
    top: 56px;
  } */
`;

const Content = styled.div`
  min-height: calc(100 - 76px);

  padding: 20px;

  @media (max-width: 768px) {
    display: block;
    min-height: calc(100vh - 36px);
  }
`;

const Section2 = styled.div`
  padding: 20px;
  padding-top: 103px;
  padding-bottom: 103px;
  background: #09031b;
`;

const SectionText = styled.h1`
  font-weight: 700;
  font-size: 48px;
  line-height: 128%;
  /* or 61px */

  text-align: center;

  color: #ffffff;
  margin-bottom: 31px;
  text-align: center;

  @media (max-width: 768px) {
    font-size: 32px;
  }
`;

const SectionSubtext = styled.h2`
  font-weight: 600;
  font-size: 24px;
  line-height: 128%;
  /* identical to box height, or 31px */

  text-align: center;

  color: #ffffff;
  text-align: center;
  margin-bottom: 76px;

  @media (max-width: 768px) {
    font-size: 18px;
  }
`;

const Section2Content = styled.div`
  max-width: 1074px;
  margin: 0px auto;
  overflow: hidden;
`;

const RowWrapper = styled.div`
  & > img {
    margin-bottom: 56px;
  }
`;

const ServiceImage = styled.img`
  width: 100%;
  height: auto;
  display: block;
`;

const Section3 = styled.div`
  padding: 20px;
  padding-top: 122px;
  padding-bottom: 253px;
`;

const Section3ContentBox = styled.div`
  background: #120830;
  position: relative;
  /* point purple */

  border: 1px solid #5a2cda;
  box-shadow: 10px 10px 30px rgba(0, 0, 0, 0.25);
  border-radius: 20px;
  max-width: 1079px;
  margin: 207px auto 61px;
  padding: 50px;
  display: grid;
  gap: 15px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  @media (max-width: 1114px) {
    grid-template-columns: 1fr 1fr 1fr;
  }
  @media (max-width: 890px) {
    grid-template-columns: 1fr 1fr;
  }
  @media (max-width: 600px) {
    padding: 50px 10px;
  }
`;

const SectionCard = styled.div``;

const SectionWrapper = styled.div`
  
`

const SectionBox = styled.img`
  /* point purple 4 */

  background: #0d0623;
  /* point purple 2 */

  border: 0.673626px solid #341a80;
  border-radius: 13.4725px;
  width: 67.36px;
  height: 67.36px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
  font-size: 33.6813px;
  line-height: 100%;
  display: block;
  margin: 0px auto;
  /* identical to box height, or 34px */

  text-align: center;

  /* point purple */

  color: #5a2cda;
  @media (max-width: 600px) {
    width: 48px;
    height: 48px;
    font-size: 20px;
  }
`;

const SectionName2 = styled.p`
  font-weight: 600;
  font-size: 12px;
  line-height: 100%;
  /* identical to box height, or 12px */

  text-align: center;

  color: #FFFFFF;
  margin-top: 12px;
`

const SectionGroupBox = styled.div`
  background: #1f104d;
  border-radius: 13.4725px;
  height: 124px;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 4px;
  min-width: 232px;
  width: 100%;
  @media (max-width: 600px) {
    min-width: 0px;
    height: 80px;
  }
`;

const SectionName = styled.p`
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 20.2088px;
  line-height: 100%;
  /* or 20px */

  text-align: center;

  color: #ffffff;
  margin-bottom: 21px;
  @media (max-width: 600px) {
    font-size: 16px;
  }
`;

const MarkettingAgreeText = styled.div`
  margin: 0 auto;
  text-align: center;
  height: 26px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 160%;
  text-align: center;
  font-feature-settings: "case" on;
  color: #adb2c5;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const DivideLine = styled.div`
  align-items: center;
  width: 640px;
  height: 0px;
  border: 1px solid #3b3f4e;
  margin: 160px auto 0 auto;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

const ApplyButtonWrapper = styled.div`
  display: flex;
  margin-top: 56px;
`;

const ApplyButtonCompanyWrapper = styled.div`
  display: flex;
  margin-top: 16px;
`;

const ApplyButton = styled.button`
  background: #1f104d;
  border: 1px solid #5a2cda;
  border-radius: 12px;
  padding: 20px 40px;
  font-weight: 700;
  font-size: 24px;
  line-height: 100%;
  /* identical to box height, or 24px */

  text-align: center;

  color: #ffffff;
  display: block;
  margin: 16px auto 0px auto;

  width: 400px;

  &:hover {
    cursor: pointer;
  }
`;

const PluginText = styled.div`
  margin: 160px auto 0 auto;
  width: 260px;
  height: 62px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 48px;
  line-height: 130%;
  text-align: right;
  background: linear-gradient(
    90deg,
    #af85ff 0%,
    #7aaefc 116.31%,
    #75b2fc 127.98%
  );
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  background-clip: text;
  text-fill-color: transparent;
`;

const PluginSubText = styled.div`
  margin: 16px auto 0 auto;
  height: 24px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 24px;
  line-height: 100%;
  text-align: center;
  color: #ffffff;
`;

const ApplyButtonForIndividual = styled.button`
  /* point purple */

  background: #5a2cda;
  border-radius: 12px;
  padding: 20px 40px;
  font-weight: 700;
  font-size: 24px;
  line-height: 100%;
  /* identical to box height, or 24px */

  text-align: center;

  color: #ffffff;
  display: block;
  margin: 0px auto 30px;

  width: 400px;

  &:hover {
    cursor: pointer;
  }
`;

const Caption = styled.div`
  font-weight: 500;
  font-size: 16px;
  margin-top: 24px;
  line-height: 140%;
  /* identical to box height, or 16px */

  text-align: center;

  color: #ffffff;
`;

const CaptionHeader = styled.div`
  height: 20px;
  font-weight: 500;
  font-size: 20px;
  line-height: 140%;

  text-align: center;
  margin-top: 150px;
  color: #ffffff;
  opacity: 0.8;
  padding-bottom: 16px;
`;

const RankWrapper = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 32px 64px;
  gap: 10px;

  max-width: 384px;
  height: 158px;
  margin: 64px auto 0 auto;
  /* point purple */

  border: 1px solid #5a2cda;
  border-radius: 10px;
`;

const RankText = styled.div`
  width: 92px;
  height: 20px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 100%;
  color: #c3c8d9;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const RankNum = styled.div`
  width: 256px;
  height: 64px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 64px;
  line-height: 100%;
  text-align: center;
  color: #ffffff;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const RankUpgradeButtonWrapper = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0px 40px;
  gap: 10px;
  width: 244px;
  height: 64px;
  background: #6446ff;
  border-radius: 12px;
  flex: none;
  order: 1;
  flex-grow: 0;
  margin: 64px auto 0 auto;
  cursor: pointer;
`;

const PreOpenWrapper = styled.div`
  display: flex;
  width: 158px;
  flex-direction: column;
  justify-content: center;
  margin: 64px auto 0 auto;
`;

const FirstOpen = styled.div`
  width: 158px;
  height: 38px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 160%;
  text-align: center;
  color: #ffffff;
`;

const SecondandThirdOpen = styled.div`
  height: 38px;
  padding-left: 4px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 160%;
  text-align: center;
  color: #8f95ab;
`;

const RankUpgradeButton = styled.div`
  height: 24px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 24px;
  line-height: 100%;
  text-align: center;
  color: #ffffff;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const RankUpgradeText = styled.div`
  max-width: 525px;
  height: 52px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 160%;
  text-align: center;
  font-feature-settings: "case" on;
  color: #adb2c5;
  flex: none;
  order: 1;
  flex-grow: 0;
  margin: 24px auto 0 auto;
`;

const Footer = styled(FlexWrapper)`
  width: 100%;
  margin-top: 18px;
  padding-top: 10px;
  opacity: 0.75;

  border-top: 1px solid ${colors.WHITE};
  justify-content: space-between;
  max-width: 1079px;
  margin: 0px auto;
  padding-bottom: 270px;
  @media (max-width: 767px) {
    display: block;
  }
`;

const FooterText = styled.p`
  ${typo({
    size: "10px",
    weight: 400,
    height: "160%",
    color: colors.WHITE,
  })}
`;

const FooterTextRight = styled(FooterText)`
  text-align: right;
  @media (max-width: 767px) {
    margin-top: 40px;
    text-align: left;
  }
`;

const FooterLink = styled(Link)`
  color: ${colors.WHITE};
  :visited {
    color: ${colors.WHITE};
  }
  margin-left: 20px;
  @media (max-width: 767px) {
    margin-left: 0px;
    margin-right: 20px;
  }
`;

const LogoWrapper = styled(FlexWrapper)`
  width: 100%;
`;

const MobileBr = styled.br`
  display: none;
  @media (max-width: 768px) {
    display: block;
  }
`;

const Logo2 = styled.img`
  display: block;
  position: absolute;
  bottom: 100%;
  left: 50%;
  transform: translate(-50%, 30%);
  @media (max-width: 768px) {
    width: 120px;
    height: 120px;
  }
`;

const ContentAbsolute = styled.div`
  padding-top: 76px;
  @media (max-width: 768px) {
    position: relative;
  }
`;

const LogoImage = styled.img`
  display: block;
  margin: 0px auto;
  height: 64px;
  margin-bottom: 24px;
  height: 53px;
  @media (max-width: 768px) {
    height: 35px;
  }
`;

const Left = styled.img`
  display: block;
  position: absolute;
  right: calc(50% + 200px);
  top: 50%;
  transform: translateY(-50%);
  @media (max-width: 1080px) {
    max-width: 400px;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

const Right = styled.img`
  display: block;
  position: absolute;
  left: calc(50% + 200px);
  top: 50%;
  z-index: 8;
  transform: translateY(-50%);
  @media (max-width: 1080px) {
    max-width: 400px;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

const Center = styled.img`
  max-width: 370px;
  margin: 0px auto 100px;
  display: block;
  z-index: 10;
  @media (max-width: 768px) {
  }
`;

const FooterWrapper = styled.div`
  padding: 0px 20px;
`;

const slide = keyframes`
  0%{
    transform: translateX(0);
  }
  12%{
    transform: translateX(0);
  }
  16%{
    transform: translateX(-100%);
  }
  28%{
    transform: translateX(-100%);
  }
  32%{
    transform: translateX(-200%);
  }
  44%{
    transform: translateX(-200%);
  }
  48%{
    transform: translateX(-300%);
  }
  60%{
    transform: translateX(-300%);
  }
  64%{
    transform: translateX(-400%);
  }
  76%{
    transform: translateX(-400%);
  }
  80%{
    transform: translateX(-500%);
  }
  92%{
    transform: translateX(-500%);
  }
  96%{
    transform: translateX(-600%);
  }

  /* 100%{
    transform: translateX(-500%);
  } */
`;

const Slider = styled.div`
  width: 100%;
  display: flex;
  animation: ${slide} 28s infinite;
  margin-left: 0px;
  img {
    width: 100%;
    height: 100%;
    display: block;
  }
`;

const Mobile = styled.img`
  margin: 0px auto;
  display: none;
  @media (max-width: 768px) {
    max-width: 327px;
    width: 100%;
    display: block;
  }
`;

const CaptionButton = styled.div`
  border: 1px solid rgba(255, 255, 255, 0.8);
  border-radius: 26px;
  padding: 16px 24px;
  font-weight: 600;
  font-size: 20px;
  line-height: 100%;

  text-align: center;

  color: rgba(255, 255, 255, 0.9);
  margin: 0px auto 46px;
  max-width: 253px;
`;


const Subtitle = styled.h1`
  font-size: 24px;
  line-height: 100%;
  margin-bottom: 24px;
  /* identical to box height, or 24px */

  text-align: center;

  /* White */

  color: #FFFFFF;
`

const CTitle = styled.h3`
  font-style: normal;
  font-weight: 700;
  font-size: 48px;
  line-height: 130%;
  /* or 62px */


  color: #FFFFFF;
  margin-bottom: 15px;
`

const CSubtitle = styled.p`
  font-weight: 400;
  font-size: 22px;
  line-height: 150%;
  /* or 33px */


  color: #FFFFFF;
  text-align: left;

  opacity: 0.8;
`

const ChipWrapper = styled.div`
  display: flex;
  gap: 15px;
  margin-bottom: 15px;
  width: 100%;
`

const Chip = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px 20px;
  gap: 5px;

  /* Purple_500_primary */

  background: #6446FF;
  border-radius: 23px 23px 0px 23px;
  

`

const ChipIcon = styled.img`
  width: 25px;
  height: 25px;
  display: block;
  border-radius: 50%;
`

const ChipLabel = styled.p`
  font-weight: 600;
  font-size: 18px;
  line-height: 100%;
  /* identical to box height, or 18px */


  /* White */

  color: #FFFFFF;
`

const GroupWrapper= styled.div`
  gap: 70px;
  display: flex;
  width: 100%;
  margin-bottom: 80px;
  @media(max-width: 1024px){
    flex-direction: column;
  }
`

const GroupOddWrapper = styled(GroupWrapper)`
   @media(max-width: 1024px){
    flex-direction: column-reverse;
  }
`

const GroupContent = styled.div`
  flex: 1;
  width: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  @media(max-width: 1024px){
    margin-top: -50px;
  }
`


const GroupImage= styled.img`
  display: block;
  width: 50%;
  flex: 0;
  @media(max-width: 1024px){
    width: 100%;
  }
`