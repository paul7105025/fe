import { GetServerSideProps, GetServerSidePropsContext, PreviewData } from "next";
import Head from "next/head";
import { ParsedUrlQuery } from "querystring";
import { ReactElement } from "react";
import { withAuthCheck } from "src/hoc";
import { AuthCheckLayout, GlobalModalLayout, MainLayout } from "src/layouts";
import { EventLayout } from "src/layouts/EventLayout";
import { StoreLNBLayout } from "src/layouts/StoreLNBLayout";


const StorePage = () => {
  return <>
    <div>
      s
    </div>
  </>
}

export default StorePage;

StorePage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>

      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <MainLayout>
              <StoreLNBLayout>
                {page}
              </StoreLNBLayout>
            </MainLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};