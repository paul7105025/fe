import { useEvent } from "@wrtn/core";
import { GetServerSideProps, GetServerSidePropsContext, PreviewData } from "next";
import Head from "next/head";
import { ParsedUrlQuery } from "querystring";
import React, { ReactElement } from "react";
import { StoreMainContainer } from "src/containers/Store";
import { withAuthCheck } from "src/hoc";
import { AuthCheckLayout, GlobalModalLayout, MainLayout } from "src/layouts";
import { EventLayout } from "src/layouts/EventLayout";
import { StoreLNBLayout } from "src/layouts/StoreLNBLayout";
import useInitAuth from "src/hooks/auth/useInitAuth";

interface StorePageProps {
  accessKey: string;
}

const StorePage = (props: StorePageProps) => {
  const { collectEvent } = useEvent();

  useInitAuth({ 
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "server"
    }
  })

  React.useEffect(() => {
    collectEvent("view_store_page", {
      store_menu: "all",
    })
  }, [])

  return (
    <>
      <StoreMainContainer />
    </>
  )
}

export default StorePage;

StorePage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>

      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <MainLayout>
              <StoreLNBLayout>
                {page}
              </StoreLNBLayout>
            </MainLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    updateUser: true,
  })
};