import { currentStoreDetailToolFormsState, currentStoreDetailToolOutputsState, favoriteStoreToolListState, storeAPI, StoreToolType, syncHeader, useEvent, userState } from "@wrtn/core";
import { GetServerSideProps, GetServerSidePropsContext, GetStaticPaths, GetStaticPathsContext, GetStaticProps, GetStaticPropsContext, PreviewData } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import React, { ReactElement } from "react";
import { StoreDetailMainContainer } from "src/containers/StoreDetail";
import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";
import { AuthCheckLayout, GlobalModalLayout, MainLayout } from "src/layouts";
import { EventLayout } from "src/layouts/EventLayout";
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { readAccessKey } from "src/utils";

// TODO: 1차 개발 시에는 toolId = storeId
// TODO: AI 챗봇 추가되면 store 객체 추가되어야 함.
interface StoreDetailPage {
  tool: StoreToolType;
  authorToolList: StoreToolType[];
  relatedToolList: StoreToolType[];
  accessKey: string;
}
// /store/details/[storeId]

const StoreDetailPage = ({ tool }: StoreDetailPage) => {
  const setCurrentStoreDetailToolForms = useSetRecoilState(currentStoreDetailToolFormsState);
  const setCurrentStoreDetailToolOutputs = useSetRecoilState(currentStoreDetailToolOutputsState);
  const { collectEvent } = useEvent();

  const router = useRouter();
  useInitAuth({
    options: {
      updateUser: "client"
    }
  })

  React.useEffect(() => {
    setCurrentStoreDetailToolForms(tool?.forms.map(v => {
      return ({
        ...v,
        data: {
          ...v.data,
          value: ""
        }
      })
    }) || [])
  }, [tool])

  React.useEffect(() => {
    setCurrentStoreDetailToolOutputs([]);
    collectEvent("view_store_use_page");
  }, [tool?._id])

  if (router.isFallback) {
    return <></>
  }

  return (
    <>
      <StoreDetailMainContainer tool={tool} />
    </>
  )
}

StoreDetailPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <MainLayout>
              {page}
            </MainLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  );
};

export default StoreDetailPage;


export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [],
    fallback: true
  }
}

// /store/details/:storeId/:b2bId
export const getStaticProps: GetStaticProps = async (
  ctx: GetStaticPropsContext<ParsedUrlQuery, PreviewData>
) => {
  const _storeId = ctx.params?.storeId;
  const storeId = !_storeId
    ? null
    : typeof _storeId === "string"
    ? _storeId
    : _storeId[0];
  
  if (!storeId) return ({
    props: {
      tool: null
    },
    revalidate: 600,
  })

  const res = await storeAPI.getTool_Id(storeId);

  if (res.status !== 200) return ({
    props: {
      tool: null
    },
    revalidate: 600,
  });

  return ({
    props: {
      tool: res.data.data
    },
    revalidate: 600, // 10 min
  })
}
