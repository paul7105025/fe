import { currentStoreDetailToolFormsState, currentStoreDetailToolOutputsState, favoriteStoreToolListState, storeAPI, StoreChatBotType, StoreToolType, syncHeader, useEvent, userState } from "@wrtn/core";
import { GetServerSideProps, GetServerSidePropsContext, GetStaticPaths, GetStaticPathsContext, GetStaticProps, GetStaticPropsContext, PreviewData } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import React, { ReactElement } from "react";
import { StoreDetailMainContainer } from "src/containers/StoreDetail";
import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";
import { AuthCheckLayout, GlobalModalLayout, MainLayout } from "src/layouts";
import { EventLayout } from "src/layouts/EventLayout";
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { readAccessKey } from "src/utils";
import ChatbotDetailMainContainer from "src/containers/ChatbotDetail/ChatbotDetailMainContainer";

interface StoreDetailPage {
  chatbot?: StoreChatBotType
}
// /store/details/[storeId]

const StoreDetailChatbotPage = ({ chatbot }: StoreDetailPage) => {
  // const setCurrentStoreDetailToolForms = useSetRecoilState(currentStoreDetailToolFormsState);
  // const setCurrentStoreDetailToolOutputs = useSetRecoilState(currentStoreDetailToolOutputsState);
  const { collectEvent } = useEvent();

  const router = useRouter();
  useInitAuth({
    options: {
      updateUser: "client"
    }
  })

  React.useEffect(() => {
    // setCurrentStoreDetailToolOutputs([]);
    // collectEvent("view_store_use_page");
  }, [chatbot?._id])

  if (router.isFallback) {
    return <></>
  }

  return (
    <>
      <ChatbotDetailMainContainer chatbot={chatbot} />
    </>
  )
}

StoreDetailChatbotPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <MainLayout>
              {page}
            </MainLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  );
};

export default StoreDetailChatbotPage;


export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [],
    fallback: true
  }
}

export const getStaticProps: GetStaticProps = async (
  ctx: GetStaticPropsContext<ParsedUrlQuery, PreviewData>
) => {
  const _chatbotId = ctx.params?.chatbotId;
  const chatbotId = !_chatbotId
    ? null
    : typeof _chatbotId === "string"
    ? _chatbotId
    : _chatbotId[0];
  
  if (!chatbotId) return ({
    props: {
      chatbot: null
    },
    revalidate: 600,
  })

  const res = await storeAPI.getChatbot_Id(chatbotId);

  if (res.status !== 200) return ({
    props: {
      chatbot: null
    },
    revalidate: 600,
  });

  return ({
    props: {
      chatbot: res.data.data
    },
    revalidate: 600, // 10 min
  })
}
