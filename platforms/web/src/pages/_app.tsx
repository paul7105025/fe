import "../styles/globals.css";

import { NextPage } from "next";
import { useRouter } from "next/router";
import type { AppProps } from "next/app";
import React, { ReactElement, ReactNode } from "react";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { RecoilEnv, RecoilRoot, useSetRecoilState } from "recoil";

import { positions, Provider as AlertProvider, transitions } from "react-alert";

import { ToastMessage } from "@wrtn/ui/components/ToastMessage";

import {
  hideChannelButton,
  AirbridgeProvider,
  MixpanelProvider,
  nextRouteState,
  RecaptchaProvider,
  useChannelBoot,
} from "@wrtn/core";
import { QueryClient, QueryClientProvider } from "react-query";
import Script from "next/script";
import Head from "next/head";
import { Analytics } from "@vercel/analytics/react";

const CHANNEL_ID_PLUGIN_KEY = process.env.NEXT_PUBLIC_CHANNEL_ID_PLUGIN_KEY;

type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

const AppWrapper = ({ children }) => {
  const router = useRouter();
  const setRoute = useSetRecoilState(nextRouteState);

  React.useEffect(() => {
    hideChannelButton();
    setRoute(router);
  }, []);

  return <div style={{ position: "absolute", inset: "0px" }}>{children}</div>;
};

/* recoil_ prefix doesn't recognize by next/node */
RecoilEnv.RECOIL_DUPLICATE_ATOM_KEY_CHECKING_ENABLED = false;

/* react-alert */
const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

export default function App({ Component, pageProps }: AppPropsWithLayout) {
  const router = useRouter();
  const getLayout = Component.getLayout ?? ((page) => page);
  useChannelBoot(CHANNEL_ID_PLUGIN_KEY || "");

  const isDevelop =
    typeof window !== "undefined" &&
    (window.location.hostname === "localhost" ||
      window.location.hostname.includes("test-copy"));

  return (
    <>
      <Script src="https://developers.kakao.com/sdk/js/kakao.js" />
      <Script src="//wcs.naver.net/wcslog.js" />
      <Script
        dangerouslySetInnerHTML={{
          __html: `
            if(!wcs_add) var wcs_add = {};
            wcs_add["wa"] = "26210a885be542";
            if(window.wcs) {
              wcs_do();
            }
        `,
        }}
      />
      <Script
        dangerouslySetInnerHTML={{
          __html: `
          (function(c,l,a,r,i,t,y){
            c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
            t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
            y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
        })(window, document, "clarity", "script", "g2mbsh6czj"); 
        `,
        }}
      />
      <Script
        dangerouslySetInnerHTML={{
          __html: `
          var link = document.querySelector('link[rel~="icon"]');
          if (!link) {
            link = document.createElement("link");
            link.rel = "icon";
            document.getElementsByTagName("head")[0].appendChild(link);
          }
          if (window.location.hostname === "wrtn.ai") {
            link.href = "%PUBLIC_URL%/favicon.ico";
          } else {
            link.href = "%PUBLIC_URL%/favicon_test.ico";
          }
          `,
        }}
      />
      <Script
        id="facebook"
        dangerouslySetInnerHTML={{
          __html: `
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '637275767893626');
            fbq('track', 'PageView');
        `,
        }}
      />
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-3GPLRFZZSM" />
      <Script
        id="google-gtag"
        dangerouslySetInnerHTML={{
          __html: `
          window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-3GPLRFZZSM');
          `,
        }}
      />
      <Head>
        <noscript>
          <img
            height="1"
            width="1"
            style={{ display: "none" }}
            src="https://www.facebook.com/tr?id=637275767893626&ev=PageView&noscript=1"
          />
        </noscript>

        {/* <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" /> */}
        <link
          rel="icon"
          href={isDevelop ? "/favicon_test.ico" : "/favicon.ico"}
        />
        <link rel="manifest" href="/manifest.json" />
        <link
          rel="apple-touch-icon"
          href={isDevelop ? "/favicon_test.ico" : "/favicon.ico"}
        />
        <link
          rel="shortcut icon"
          href={isDevelop ? "/favicon_test.ico" : "/favicon.ico"}
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=no"
        />
        <title>뤼튼</title>
      </Head>
      <GoogleOAuthProvider
        clientId={process.env.NEXT_PUBLIC_URL_GOOGLE_CLIENT_ID || ""}
      >
        <QueryClientProvider client={queryClient}>
          <AlertProvider template={ToastMessage} {...alertOptions}>
            <RecoilRoot override={false}>
              <MixpanelProvider>
                <AirbridgeProvider>
                  <RecaptchaProvider>
                    <AppWrapper>
                      {getLayout(<Component {...pageProps} />)}
                      <Analytics />
                    </AppWrapper>
                  </RecaptchaProvider>
                </AirbridgeProvider>
              </MixpanelProvider>
            </RecoilRoot>
          </AlertProvider>
        </QueryClientProvider>
      </GoogleOAuthProvider>
    </>
  );
}
