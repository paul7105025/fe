import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import styled from "styled-components";
import { useRecoilValue } from "recoil";
import { ParsedUrlQuery } from "querystring";
import React, { ReactElement } from "react";

import { MainLayout, SettingLNBLayout } from "src/layouts";

import { CouponDialog } from "src/components/SettingCoupon";
import { CouponSettingTable } from "src/components/SettingTable";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";

import {
  couponFilterType,
  couponListState,
  couponType,
  getCouponCode,
  postCouponCode,
  useEvent,
  useRefresh,
} from "@wrtn/core";

import {
  colors,
  FlexButton,
  FlexWrapper,
  IconInfo,
  InputStyle,
  typo,
} from "@wrtn/ui";
import Head from "next/head";

const SettingCouponPage = (props) => {
  const couponList = useRecoilValue(couponListState);

  const [couponText, setCouponText] = React.useState<string>("");
  const [isError, setIsError] = React.useState<string>("");
  const [isModalOpen, setIsModalOpen] = React.useState<boolean>(false);

  const [currentFilter, setCurrentFilter] =
    React.useState<couponFilterType>("전체");
  const [currentCoupon, setCurrentCoupon] = React.useState<couponType | null>(
    null
  );

  const { fetchCoupon, fetchMetric } = useRefresh();

  const { collectEvent } = useEvent();

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  const onChangeText = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCouponText(e.target.value);
    setIsError("");
  };

  const onClickCouponButton = async () => {
    const couponRes = await getCouponCode({ code: couponText });

    if (couponRes?.status === 200) {
      const { data } = couponRes.data as {
        data: couponType;
      };

      setIsModalOpen(true);
      setCurrentCoupon(data);
    } else if (couponRes?.status === 409) {
      if (couponRes?.data.message === "이미 만료된 쿠폰입니다.")
        setIsError("쿠폰의 유효기한이 만료되었어요.");
      else if (couponRes?.data.message === "쿠폰이 모두 소진되었습니다.")
        setIsError("쿠폰이 모두 소진되었습니다.");
      else if (
        couponRes?.data.message ===
        "적용할 수 없는 쿠폰입니다. (쿠폰 사용 기록 존재)"
      )
        setIsError("최초 1회만 사용 가능한 쿠폰입니다.");
      else if (
        couponRes?.data.message ===
        "적용할 수 없는 쿠폰입니다. (결제 기록 존재)"
      )
        setIsError("최초 결제 이후 등록할 수 없는 쿠폰입니다.");
      else setIsError("이미 등록된 쿠폰입니다.");
    } else if (couponRes?.status === 404) {
      setIsError("쿠폰 코드를 다시 확인해주세요.");
    } else if (couponRes?.status >= 400) {
      setIsError("쿠폰 코드를 다시 확인해주세요.");
    }
  };

  const onClickAcceptButton = async () => {
    try {
      const acceptCouponRes = await postCouponCode({
        id: currentCoupon?._id,
        code: couponText,
      });
      if (acceptCouponRes?.status >= 200 && acceptCouponRes?.status < 300) {
        fetchCoupon();
        fetchMetric();
        collectEvent("add_coupon", {
          coupon_name: currentCoupon?.name,
          coupon_category: "할인금액",
          coupon_type: "결제용 / 첫결제용 / 재결제용/구독재개용",
          expired_date: currentCoupon?.dueDate,
        });
      } else {
        throw new Error(acceptCouponRes.stats);
      }
    } catch (error) {
      if (error === "409") {
        setIsError("등록이 불가능한 쿠폰입니다.");
      }
    } finally {
      setIsModalOpen(false);
    }
  };

  React.useEffect(() => {
    fetchCoupon();
  }, []);

  return (
    <Wrapper column>
      <ContentWrapper column>
        <Title>쿠폰 등록</Title>
        <InputWrapper>
          <CouponInput
            value={couponText}
            onChange={onChangeText}
            placeholder="쿠폰 코드를 입력해주세요."
            isError={isError.length > 0}
          />
          <CouponButton onClick={onClickCouponButton}>쿠폰 등록</CouponButton>
          {isError.length > 0 && (
            <CouponErrorWrapper>
              <ErrorIcon />
              <span>{isError}</span>
            </CouponErrorWrapper>
          )}
        </InputWrapper>
      </ContentWrapper>
      <CouponSettingTable
        currentFilter={currentFilter}
        setCurrentFilter={(filter) => setCurrentFilter(filter)}
        couponHistory={couponList}
      />
      {isModalOpen && currentCoupon && (
        <CouponDialog
          coupon={currentCoupon}
          onClose={() => setIsModalOpen(false)}
          onAccept={onClickAcceptButton}
        />
      )}
    </Wrapper>
  );
};

export default SettingCouponPage;

SettingCouponPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <MainLayout>
        <SettingLNBLayout>{page}</SettingLNBLayout>
      </MainLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  max-width: 1080px;

  margin: 0px auto;
  padding: 56px 40px;

  align-items: flex-start;

  @media (max-width: 767px) {
    padding: 56px 24px;
  }
`;

const ContentWrapper = styled(FlexWrapper)`
  align-items: flex-start;
`;

const Title = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.GRAY_80,
  })}

  text-align: left;
`;

const InputWrapper = styled(FlexWrapper)`
  gap: 18px;
  position: relative;

  margin-top: 24px;
  margin-bottom: 64px;
`;

const CouponInput = styled.input<{ isError: boolean }>`
  ${InputStyle}

  min-width: 330px;
  min-height: 42px;

  border: 1px solid
    ${({ isError }) => (isError ? colors.HOT_RED : colors.BACKGROUND)};
  border-radius: 5px;

  &:focus {
    border: 1px solid
      ${({ isError }) => (isError ? colors.HOT_RED : colors.ACTION_BLUE)};
  }

  @media (max-width: 767px) {
    min-width: 30vw;
  }
`;

const CouponErrorWrapper = styled(FlexWrapper)`
  position: absolute;
  bottom: -24px;
  left: 0px;

  gap: 7px;

  ${typo({
    weight: 500,
    size: "14px",
    color: colors.HOT_RED,
  })}
`;

const ErrorIcon = styled(IconInfo)`
  circle {
    stroke: ${colors.HOT_RED};
  }
  path {
    fill: ${colors.HOT_RED};
  }
`;

const CouponButton = styled(FlexButton)`
  padding: 14px 42px;
  border-radius: 5px;

  background-color: ${colors.POINT_PURPLE};

  white-space: nowrap;

  ${typo({
    weight: 700,
    size: "14px",
    color: colors.WHITE,
  })}

  cursor: pointer;
  &:hover {
    background-color: ${colors.POINT_PURPLE_HOVER};
  }
`;
