import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import { useAlert } from "react-alert";
import { useRouter } from "next/router";
import { useRecoilState } from "recoil";
import React, { ReactElement } from "react";
import { ParsedUrlQuery } from "querystring";
import styled, { css, keyframes } from "styled-components";

import { Avatar } from "@wrtn/ui/components/Avatar";
import { CheckboxDropdown } from "@wrtn/ui/components/Select";

import { WithdrawDialogContainer } from "../../containers/DialogContainer";

import { surveyItemList } from "@wrtn/core";
import { userState } from "@wrtn/core/stores";
import { putUser } from "@wrtn/core/services";
import { ProviderType } from "@wrtn/core/types";
import {
  useTempInput,
  useTempArray,
  useTagManager,
  useLogout,
  useEvent,
} from "@wrtn/core/hooks";

import { IconLoginKakao, IconLoginNaver, IconGoogle } from "@wrtn/ui/assets";

import {
  colors,
  FlexButton,
  FlexWrapper,
  InputStyle,
  typo,
} from "@wrtn/ui/styles";

const jobList = [...surveyItemList.STEP_JOB_SURVEY.filter((v) => v !== "기타")];

const jobFormatter = (val: Array<string>) => {
  if (!val) return [];
  return val;
};

const LoginProvider = (provider: ProviderType) => {
  if (provider === "google") return <GoogleIcon />;
  else if (provider === "kakao") return <KaKaoIcon />;
  else if (provider === "naver") return <NaverIcon />;
};

import { MainLayout, SettingLNBLayout } from "src/layouts";

import { withAuthCheck } from "src/hoc";
import { paths } from "src/constants";
import useInitAuth from "src/hooks/auth/useInitAuth";
import Head from "next/head";

const SettingAccountPage = (props) => {
  const router = useRouter();
  const [user, setUser] = useRecoilState(userState);
  const tagManager = useTagManager();
  const alert = useAlert();

  const nameInput = useTempInput(user?.name || "");
  const companyInput = useTempInput(user?.company || "");
  const jobInput = useTempArray(user?.job || null, jobFormatter);
  const emailInput = useTempInput(user?.email || "");

  const logout = useLogout();
  const { collectEvent } = useEvent();

  const [openWithdrawal, setOpenWithdrawal] = React.useState(false);

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  React.useEffect(() => {
    collectEvent("view_account_info_page");
  }, []);

  const handleSubmit = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (
      jobInput.value.length < 0 ||
      jobInput.value.filter((v) => v.length > 1).length < 1
    ) {
      alert.removeAll();
      alert.show("직군을 선택하거나 입력해주세요.");
      return;
    }
    const newUserData = {
      ...(nameInput.value.length > 0 && { name: nameInput.value }),
      ...(companyInput.value.length > 0 && { company: companyInput.value }),
      ...{
        job: jobInput.value,
      },
    };
    const res = await putUser({
      data: newUserData,
    });
    if (res.data.result === "SUCCESS") {
      tagManager({
        event: "click_user_update_btn",
      });
      collectEvent("click_account_info_btn");

      const user = res.data.data;
      setUser(user);
      alert.removeAll();
      alert.show("변경을 완료했어요.");
    }
  };

  return (
    <FlexWrapper>
      <AccountContent column>
        <Avatar width={"78px"} size={"40px"}>
          {user?.name[0]}
        </Avatar>
        <AccountForm>
          <InputLabelContent>
            <InputLabel>이름/닉네임</InputLabel>
            <Input
              onChange={(e) => nameInput.handleChange(e.target.value)}
              value={nameInput.value || ""}
            />
          </InputLabelContent>
          <InputLabelContent>
            <InputLabel>계정</InputLabel>
            <Input disabled value={emailInput.value || ""} />
            {LoginProvider(user ? user.provider : "local")}
          </InputLabelContent>

          <InputLabelContent>
            <InputLabel>소속</InputLabel>
            <Input
              placeholder="현재 소속을 알려주세요!"
              onChange={(e) => companyInput.handleChange(e.target.value)}
              value={companyInput.value || ""}
            />
          </InputLabelContent>

          <InputLabelContent>
            <InputLabel>직무</InputLabel>
            <CheckboxDropdown
              list={jobList}
              value={jobInput.value}
              setValue={jobInput.handleChange}
            />
          </InputLabelContent>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <InputSubmit onClick={handleSubmit}>변경하기</InputSubmit>
          </div>
          <AccountBottom>
            <BottomButton onClick={() => setOpenWithdrawal(true)}>
              회원 탈퇴
            </BottomButton>
            <BottomButtonHighlight
              onClick={() => {
                collectEvent("logout");
                logout(user).then(() => router.push(paths.logout()));
              }}
            >
              로그아웃
            </BottomButtonHighlight>
          </AccountBottom>
        </AccountForm>
      </AccountContent>
      {openWithdrawal && (
        <WithdrawDialogContainer
          user={user}
          onClose={() => setOpenWithdrawal(false)}
        />
      )}
    </FlexWrapper>
  );
};

export default SettingAccountPage;

SettingAccountPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <MainLayout>
        <SettingLNBLayout>{page}</SettingLNBLayout>
      </MainLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Wrapper = styled(FlexWrapper)`
  flex: 1 1 auto;
  width: 100%;
  height: 100%;
`;

const AccountContent = styled(FlexWrapper)`
  padding: 82px 0px;

  max-width: 864px;

  min-width: 484px;

  @media (max-width: 767px) {
    padding: 67px 0px 0px;

    min-width: 360px;
  }
`;

const AccountBottom = styled(FlexWrapper)`
  width: 100%;

  margin-top: 43px;

  justify-content: flex-end;
  gap: 12px;
  margin-bottom: 10%;
`;

const AccountForm = styled(FlexWrapper)`
  width: 100%;

  flex-direction: column;
  justify-content: flex-start;

  padding: 41px 0px 0px;
  gap: 24px;

  @media (max-width: 767px) {
    padding: 41px 20px 0px;
  }
`;

const InputLabelContent = styled(FlexWrapper)`
  width: 100%;
  position: relative;

  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

const InputLabel = styled.label`
  padding-bottom: 14px;

  display: flex;
  align-items: center;

  span {
  }
`;

const EventAnimation = keyframes`
  0% {
    color : ${colors.ACTION_BLUE};
    /* transform: translateX(0px); */
  }
  25% {
    /* transform: translateX(2px); */
  }
  50% {
    color : ${colors.POINT_PURPLE};
    /* transform: translateX(0px); */
  }
  75% {
    /* transform: translateX(-2px); */
  }
  100% {
    color : ${colors.ACTION_BLUE};
    /* transform: translateX(0px); */
  }
`;

const EventSpan = styled.span<{
  recommend: boolean;
}>`
  margin-left: 15px;
  ${typo({
    weight: 700,
    size: "14px",
    color: colors.ACTION_BLUE,
  })};

  ${({ recommend }) =>
    recommend &&
    css`
      animation: ${EventAnimation} 0.6s ease-in-out;
      animation-iteration-count: 2;
    `}
`;

const Input = styled.input`
  ${InputStyle};
  ${({ disabled }) =>
    disabled &&
    css`
      color: ${colors.GRAY_55};
      user-select: none;
    `}
`;

const InputSubmit = styled(FlexButton)`
  margin-top: 3px;
  width: 88px;
  padding: 4px 16px;
  right: 0px;

  background: ${colors.POINT_PURPLE};
  ${typo({ weight: 600, size: "16px", height: "26px", color: colors.WHITE })};
  border-radius: 5px;
  cursor: pointer;
  &:hover {
    opacity: 70%;
  }
`;

const BottomButtonHighlight = styled.button`
  ${typo({ weight: 600, size: "16px", color: colors.gray_80 })};
  padding: 6px 9px;
  background: ${colors.gray_30};
  border-radius: 5px;
  cursor: pointer;

  &:hover {
    background-color: ${colors.gray_40};
  }
`;

const BottomButton = styled.button`
  ${typo({ weight: 600, size: "16px", color: colors.gray_60 })};
  padding: 6px 9px;
  background: ${colors.WHITE};
  border-radius: 5px;
  cursor: pointer;

  &:hover {
    background-color: ${colors.gray_30};
  }
`;

const GoogleIcon = styled(IconGoogle)`
  position: absolute;
  top: 45px;
  right: 19px;
`;

const NaverIcon = styled(IconLoginNaver)`
  position: absolute;
  width: 18px;
  height: 19px;
  top: 45px;
  right: 19px;
  path {
    fill: #2db400;
  }
`;

const KaKaoIcon = styled(IconLoginKakao)`
  position: absolute;
  width: 18px;
  height: 19px;
  top: 45px;
  right: 19px;
`;
