import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import { ReactElement } from "react";
import { ParsedUrlQuery } from "querystring";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import { MainLayout, SettingLNBLayout } from "src/layouts";
import Head from "next/head";
import React from "react";

const SettingPage = () => {
  return <>Setting</>;
};

export default SettingPage;

SettingPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <MainLayout>
        <SettingLNBLayout>{page}</SettingLNBLayout>
      </MainLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};
