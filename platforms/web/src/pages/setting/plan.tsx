import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import React, { ReactElement } from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";
import { ParsedUrlQuery } from "querystring";

import { userState } from "@wrtn/core/stores/login";
import { useIsMobile } from "@wrtn/core";

import {
  MyPlanCard,
  PlanMobile,
  PlanFAQ,
  PlanBanner,
  PlanTable,
} from "src/components/SettingPlan";

import { MainLayout, SettingLNBLayout } from "src/layouts";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import Head from "next/head";

const SettingPlanPage = (props) => {
  const isMobile = useIsMobile();
  const user = useRecoilValue(userState);

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  return (
    <Wrapper column>
      <PaymentInsideContent column>
        <PaymentSection style={{ paddingBottom: "47px" }}>
          <PlanSectionLabel>내 요금제</PlanSectionLabel>
          {user && (
            <PaymentSectionBox>
              <MyPlanCard user={user} />
            </PaymentSectionBox>
          )}
        </PaymentSection>
        <PaymentSection style={{ marginBottom: "45px" }}>
          <PlanSectionLabel>
            <FlexWrapper style={{ justifyContent: "flex-start" }}>
              월 요금제
            </FlexWrapper>
          </PlanSectionLabel>
          {user && <PlanMobile currentPlan={user.plan} />}
        </PaymentSection>
        {/* <PlanBanner /> */}
      </PaymentInsideContent>
      <GuideWrapper column>
        <PlanSectionLabel>자주 묻는 질문</PlanSectionLabel>
        <PlanFAQ />
      </GuideWrapper>
    </Wrapper>
  );
};

export default SettingPlanPage;

SettingPlanPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <MainLayout>
        <SettingLNBLayout>{page}</SettingLNBLayout>
      </MainLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Wrapper = styled(FlexWrapper)`
  align-items: flex-start;
  justify-content: flex-start;

  max-width: 1080px;
  width: 100%;
  height: 100%;
  overflow: scroll;
`;

const PaymentInsideContent = styled(FlexWrapper)`
  width: 100%;
  padding: 56px 40px 80px;
  align-items: flex-start;
  justify-content: flex-start;

  @media (max-width: 767px) {
    padding: 56px 20px 60px;
  }

  @media (max-width: 480px) {
  }
`;

const PaymentSection = styled.div`
  position: relative;
  width: 100%;
`;

const PlanSectionLabel = styled.div`
  ${typo({ weight: 600, size: "16px", height: "100%", color: colors.gray_80 })};
  padding-bottom: 17px;
`;

const PaymentSectionBox = styled.div`
  position: relative;
  border-radius: 12px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  padding: 20px 22px 16px 22px;
`;

const GuideWrapper = styled(PaymentInsideContent)`
  background: linear-gradient(180deg, #f2f7ff -24.67%, #ffffff 103.17%);
`;
