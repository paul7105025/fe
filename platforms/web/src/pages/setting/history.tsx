import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";

import { useQuery } from "react-query";
import React, { ReactElement } from "react";
import { ParsedUrlQuery } from "querystring";
import styled, { css } from "styled-components";
import { useRecoilState, useRecoilValue, useResetRecoilState } from "recoil";

import { MainLayout, SettingLNBLayout } from "src/layouts";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";

import { useCategoryFilter } from "src/hooks";
import useInitAuth from "src/hooks/auth/useInitAuth";

import { currentHistoryState, currentImageState } from "src/stores/history";

import {
  typo,
  colors,
  FlexWrapper,
  FlexButton,
  TOOL_TAB_SELECTOR,
} from "@wrtn/ui";

import {
  userState,
  TOOL_CATEGORY_RECORD,
  useEvent,
  removeLocal,
  showChannelButton,
  getImageHistory,
  getHistory,
  getImageHistory_Count,
  getHistory_Count,
} from "@wrtn/core";

import Loading from "@wrtn/ui/components/Loading";
import { Pagination } from "@wrtn/ui/components/Pagination";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import HistoryPayWrapper from "src/components/History/HistoryPayWrapper";
import HistoryListContainer from "src/components/History/HistoryListContainer";
import { ModalHistory } from "src/components/History/ModalHistory";
import Head from "next/head";
// import { ImageResultContainer } from "src/components/History/ImageResultContainer";

const PAGINATION_LIMIT = 12;

const SettingHistoryPage = (props) => {
  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  const user = useRecoilValue(userState);

  const {
    categoryList,
    tabList,
    currentTab,
    currentCategory,
    setCurrentTab,
    setCurrentCategory,
  } = useCategoryFilter({
    isImageAccess: false,
  });

  const { collectEvent } = useEvent();

  React.useEffect(() => {
    collectEvent("view_history_page");
  }, []);

  React.useEffect(() => {
    removeLocal("wrtn-temp-output");
  }, []);

  React.useEffect(() => {
    showChannelButton();
  }, []);

  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [currentHistory, setCurrentHistory] =
    useRecoilState(currentHistoryState);
  const resetCurrentHistory = useResetRecoilState(currentHistoryState);

  const [currentImage, setCurrentImage] = useRecoilState(currentImageState);
  const resetCurrentImage = useResetRecoilState(currentImageState);

  const [page, setPage] = React.useState(0);
  const [pageMoveDirection, setPageMoveDirection] = React.useState("none");

  const queryHistoryList = useQuery({
    queryKey: ["historyList", currentCategory, currentTab, page],
    queryFn: () =>
      currentTab === "image"
        ? getImageHistory({
            page: page + 1,
            limit: PAGINATION_LIMIT,
            style: currentCategory === "all" ? null : currentCategory,
          }).then((res) => res.data.data)
        : getHistory({
            page: page + 1,
            limit: PAGINATION_LIMIT,
            category:
              currentCategory === "all" ? undefined : currentCategory || "",
            liked: currentTab === "all" ? "n" : "y",
          }).then((res) => res.data.data),
    keepPreviousData: true,
  });

  const queryHistoryCount = useQuery({
    queryKey: ["historyCount", currentCategory, currentTab],
    queryFn: () =>
      currentTab === "image"
        ? getImageHistory_Count({
            page: page + 1,
            limit: PAGINATION_LIMIT,
            style: currentCategory === "all" ? null : currentCategory,
          }).then((res) => res.data.data)
        : getHistory_Count({
            page: page + 1,
            limit: PAGINATION_LIMIT,
            category:
              currentCategory === "all" ? undefined : currentCategory || "",
            liked: currentTab === "all" ? "n" : "y",
          }).then((res) => res.data.data),
  });

  const maxPage = Math.ceil(queryHistoryCount.data / PAGINATION_LIMIT);
  const historyList = queryHistoryList.data;

  React.useEffect(() => {
    setPage(0);
  }, [currentTab, currentCategory]);

  const getCurrentIndex = React.useCallback(
    (currentHistory) => {
      return historyList.findIndex((e) => e._id === currentHistory._id);
    },
    [historyList]
  );

  const handlePrev = async (currentHistory) => {
    const currentIndex = getCurrentIndex(currentHistory);

    if (currentIndex === 0) {
      if (page === 0) {
        return;
      }
      setPage(page - 1);
      setPageMoveDirection("prev");
    } else {
      setCurrentHistory(historyList[currentIndex - 1]);
    }
  };

  const handleNext = async (currentHistory) => {
    const currentIndex = getCurrentIndex(currentHistory);

    if (currentIndex === historyList.length - 1) {
      if (page === maxPage - 1) {
        return;
      }
      setPage(page + 1);
      setPageMoveDirection("next");
    } else {
      setCurrentHistory(historyList[currentIndex + 1]);
    }
  };

  React.useEffect(() => {
    if (historyList && historyList.length > 0) {
      if (pageMoveDirection === "prev") {
        setCurrentHistory(historyList[historyList.length - 1]);
        setPageMoveDirection("none");
      }
      if (pageMoveDirection === "next") {
        setCurrentHistory(historyList[0]);
        setPageMoveDirection("none");
      }
    }
  }, [historyList]);

  if (!user) return <></>;

  return (
    <Wrapper>
      <TitleWrapper>
        <Title>툴 히스토리</Title>
      </TitleWrapper>
      <TabWrapper>
        {tabList.map((tab) => {
          return (
            <TabItem
              key={tab}
              selected={currentTab === tab}
              onClick={() => setCurrentTab(tab)}
            >
              {TOOL_TAB_SELECTOR[tab]}
            </TabItem>
          );
        })}
      </TabWrapper>
      <CategoryWrapper>
        <InnerScrollWrapper>
          {categoryList.map((category, idx) => {
            return (
              <CategoryItem
                key={idx}
                selected={currentCategory === category}
                onClick={() => setCurrentCategory(category)}
              >
                {TOOL_CATEGORY_RECORD[category]}
              </CategoryItem>
            );
          })}
        </InnerScrollWrapper>
      </CategoryWrapper>
      {currentTab !== "image" && (
        <HistoryPayWrapper plan={user.plan} count={queryHistoryCount?.data} />
      )}
      {queryHistoryList.isLoading ? (
        <Loading />
      ) : (
        <HistoryListContainer
          currentCategory={currentCategory}
          currentTab={currentTab}
          data={historyList}
          open={() => setIsModalOpen(true)}
        />
      )}
      <PaginationWrapper>
        {maxPage > 0 && (
          <Pagination maxPage={maxPage} currentPage={page} setPage={setPage} />
        )}
      </PaginationWrapper>
      {isModalOpen && currentHistory && (
        <ModalPortal
          onClose={() => {
            resetCurrentHistory();
            setIsModalOpen(false);
          }}
        >
          <ModalHistory
            id={currentHistory._id}
            plan={user.plan}
            onClose={() => {
              resetCurrentHistory();
              setIsModalOpen(false);
            }}
            handlePrev={() => handlePrev(currentHistory)}
            handleNext={() => handleNext(currentHistory)}
            prevDisabled={
              historyList &&
              page === 0 &&
              historyList[0]._id === currentHistory?._id
            }
            nextDisabled={
              historyList &&
              page === maxPage - 1 &&
              historyList[historyList.length - 1]._id === currentHistory?._id
            }
            updateBackground={() => {
              queryHistoryList.refetch();
              queryHistoryCount.refetch();
            }}
          />
        </ModalPortal>
      )}
      {/* {currentImage && (
        <ModalPortal onClose={resetCurrentImage}>
          <ImageResultContainer
            type="history"
            output={currentImage}
            onClose={resetCurrentImage}
          />
        </ModalPortal>
      )} */}
    </Wrapper>
  );
};

export default SettingHistoryPage;

SettingHistoryPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <MainLayout>
        <SettingLNBLayout>{page}</SettingLNBLayout>
      </MainLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
`;

const TitleWrapper = styled.div`
  padding: 4rem 0px 2.5rem 2.5rem;
`;

const Title = styled.h1`
  ${typo({
    size: "24px",
    weight: "600",
    color: colors.GRAY_90,
  })}
`;

const TabWrapper = styled.div`
  display: flex;
  padding: 0px 0px 0px 40px;
  border-bottom: 1px solid #dee4f3;
  gap: 22px;
`;

const TabItem = styled.button<{ selected: boolean }>`
  border: none;
  background: none;

  ${typo({
    size: "18px",
    weight: "500",
    color: colors.GRAY_60,
  })}

  padding: 0px 4px 12px;
  ${({ selected }) =>
    selected &&
    css`
      color: ${colors.GRAY_90};
      font-weight: 600;
      padding-bottom: 10px;
      border-bottom: 2px solid #5a2cda;
    `}
  &:hover {
    color: ${colors.GRAY_90};
    cursor: pointer;
  }

  @media (max-width: 767px) {
    overflow: auto;
    white-space: nowrap;
    flex-wrap: nowrap;
  }

  user-select: none;
`;

const CategoryWrapper = styled.div`
  position: relative;

  @media (max-width: 767px) {
    &::after {
      position: absolute;
      bottom: 22px;
      right: 0;
      width: 60px;
      height: 30px;
      content: "";

      background: linear-gradient(
        270deg,
        #ffffff 0%,
        rgba(255, 255, 255, 0) 100%
      );
      filter: blur(2px);
    }
  }
`;

const InnerScrollWrapper = styled(FlexWrapper)`
  padding: 1.4rem 2.8rem;

  gap: 12px;
  align-items: center;
  flex-wrap: wrap;
  justify-content: flex-start;

  @media (max-width: 767px) {
    overflow: auto;
    white-space: nowrap;
    flex-wrap: nowrap;
  }
`;

const CategoryItem = styled(FlexButton)<{ selected: boolean }>`
  padding: 8px 16px;

  ${typo({
    size: "16px",
    weight: "500",
    color: colors.GRAY_60,
  })}

  white-space: nowrap;

  line-height: 100%;
  ${({ selected }) =>
    selected &&
    css`
      color: ${colors.POINT_PURPLE};
      padding: 7px 15px;
      border: 1px solid ${colors.POINT_PURPLE};
      border-radius: 40px;
    `}
  &:hover {
    cursor: pointer;
    color: ${colors.POINT_PURPLE};
  }

  @media (max-width: 767px) {
    padding: 8px 12px;
    ${({ selected }) =>
      selected &&
      css`
        padding: 7px 11px;
      `}
  }

  user-select: none;
`;

const PaginationWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 40px;
`;
