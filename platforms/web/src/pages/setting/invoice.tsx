import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import dayjs from "dayjs";
import styled from "styled-components";
import { useRouter } from "next/router";
import { useRecoilValue } from "recoil";
import React, { ReactElement } from "react";
import { ParsedUrlQuery } from "querystring";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";
import { MainLayout, SettingLNBLayout } from "src/layouts";
import { InvoiceSettingTable } from "src/components/SettingTable";

import {
  getBillingPayment,
  getUserCard,
  PaymentHistory,
  PLAN_PRICE,
  useEvent,
  userState,
} from "@wrtn/core";
import { colors, FlexWrapper, IconCreditCard, typo } from "@wrtn/ui";
import Head from "next/head";

const SettingInvoicePage = (props) => {
  const user = useRecoilValue(userState);

  const [paymentHistory, setPaymentHistory] = React.useState<
    Array<PaymentHistory>
  >([]);

  const router = useRouter();
  const { collectEvent } = useEvent();

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  const fetchPaymentHistory = async () => {
    const res = await getBillingPayment();
    if (res?.status === 200 || res?.status === 201) {
      let { data } = res.data;

      if (!data) data = []; // []로 통일
      if (Array.isArray(data) && data.length > 0) {
        setPaymentHistory(data);
      }
    }
  };

  const [cardList, setCardList] = React.useState<any[]>([]);

  const fetchUserCard = async () => {
    const res = await getUserCard();
    if (res?.status === 200) {
      const { data } = res.data;
      if (Array.isArray(data)) {
        setCardList(data);
      }
    }
  };
  const currentPaymentHistory =
    user?.nextPlan && paymentHistory.length > 0 ? paymentHistory[0] : null;

  const currentCard =
    user?.nextPlan && cardList.length > 0 ? cardList[0] : null;

  React.useEffect(() => {
    fetchPaymentHistory();
    collectEvent("view_purchase_info_page");
  }, []);

  React.useEffect(() => {
    fetchUserCard();
  }, []);

  return (
    <Wrapper>
      <ContentWrapper>
        <TitleWrapper>
          <Title>결제 정보</Title>
        </TitleWrapper>
        <CurrentWrapper>
          <CurrentTopWrapper>
            <BetweenWrapper>
              {user?.nextPlan && user?.nextPlan !== "FREE" ? (
                <Price>
                  {Number(PLAN_PRICE[user?.nextPlan]).toLocaleString()}원{" "}
                  <Unit>/월</Unit>
                </Price>
              ) : (
                <Price>결제 중인 요금제가 없습니다.</Price>
              )}
              {currentPaymentHistory && (
                <Anchor
                  onClick={() => {
                    router.push({
                      pathname: paths.setting.billing.index(),
                      query: {
                        type: "chagneCard",
                        plan: user?.plan,
                      },
                    });
                  }}
                >
                  결제 정보 변경
                </Anchor>
              )}
            </BetweenWrapper>
            <CardInfoWrapper>
              <IconCreditCard />
              {currentCard?.card?.company && currentCard?.card?.number
                ? currentCard?.card?.company + " " + currentCard?.card?.number
                : "카드 없음"}
            </CardInfoWrapper>
          </CurrentTopWrapper>
          <CurrentBottomWrapper>
            <BottomContentWrapper>
              <BottomContentLabel>다음 결제일</BottomContentLabel>
              <BottomContentValue>
                {!user?.nextPlan || user?.nextPlan === "FREE"
                  ? "-"
                  : dayjs(user?.dueDate).format("YYYY년 MM월 DD일")}
              </BottomContentValue>
            </BottomContentWrapper>
          </CurrentBottomWrapper>
        </CurrentWrapper>
      </ContentWrapper>
      <InvoiceSettingTable paymentHistory={paymentHistory} />
    </Wrapper>
  );
};

export default SettingInvoicePage;

SettingInvoicePage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <MainLayout>
        <SettingLNBLayout>{page}</SettingLNBLayout>
      </MainLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Wrapper = styled.div`
  display: block;
  width: 100%;
  max-width: 1080px;
  margin: 0px auto;
  padding: 56px 40px;
`;

const Title = styled.div`
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;
  color: ${colors.GRAY_80};
  text-align: left;
`;

const ContentWrapper = styled.div`
  margin-bottom: 34px;
`;

const CurrentWrapper = styled.div`
  border: 1px solid #d1d7ed;
  border-radius: 12px;
`;

const TitleWrapper = styled.div`
  margin-bottom: 20px;
`;

const CurrentTopWrapper = styled.div`
  padding: 21px 30px 16px;
`;

const CurrentBottomWrapper = styled.div`
  border-top: 1px solid #dee4f3;
  padding: 12px 23px;
`;

const BetweenWrapper = styled(FlexWrapper)`
  margin-bottom: 10px;

  justify-content: space-between;
`;

const CardInfoWrapper = styled(FlexWrapper)`
  ${typo({
    weight: 500,
    size: "12px",
    color: colors.GRAY_80,
  })}

  justify-content: flex-start;
  gap: 7px;
`;

const BottomContentWrapper = styled(FlexWrapper)`
  margin-bottom: 10px;
  justify-content: flex-start;
`;

const BottomContentLabel = styled.p`
  ${typo({
    weight: 500,
    size: "12px",
    color: "#83879d",
  })}

  width: 75px;
`;

const BottomContentValue = styled.p`
  ${typo({
    weight: 500,
    size: "12px",
    color: colors.GRAY_80,
  })}
`;

const Price = styled.p`
  ${typo({
    weight: 600,
    size: "20px",
    color: colors.GRAY_80,
  })}
`;

const Unit = styled.span`
  ${typo({
    weight: 500,
    size: "16px",
    color: "#83879d",
  })}
`;

const Anchor = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.gray_60,
  })};

  text-decoration-line: underline;
  cursor: pointer;
`;
