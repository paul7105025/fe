import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import dayjs from "dayjs";
import { ReactElement } from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import { useRecoilValue } from "recoil";
import { ParsedUrlQuery } from "querystring";
import { useSearchParams } from "next/navigation";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";
import { MainLayout, SettingLNBLayout } from "src/layouts";

import { userState, PLAN_NAME } from "@wrtn/core";
import { IconCheckCircle, colors, FlexWrapper, typo } from "@wrtn/ui";
import polyfill from "@wrtn/ui/styles/polyfill";
import Head from "next/head";
import React from "react";

const SettingSuccessPage = (props) => {
  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  const user = useRecoilValue(userState);

  const router = useRouter();

  const searchParams = useSearchParams();

  const plan = searchParams.get("plan");
  const type = searchParams.get("type");

  if (!plan) {
    router.push("/");
  }

  const getTitle = () => {
    if (type === "changeCard") {
      return "결제 카드 변경이 완료되었습니다!";
    } else if (type === "coupon") {
      return "쿠폰 적용이 완료되었습니다!";
    } else if (user?.plan !== user?.nextPlan) {
      return "요금제 변경이 완료되었습니다!";
    } else {
      return "결제가 완료되었습니다!";
    }
  };

  const getSubtitle = () => {
    if (type === "changeCard") {
      return "다음 결제일 부터 변경된 카드로 결제가 진행됩니다.";
    } else if (type === "coupon") {
      return `${dayjs(user?.dueDate).format(
        "YYYY년 MM월 DD일"
      )}부터 쿠폰이 적용된 가격으로 결제됩니다.`;
    } else {
      if (typeof plan === "string")
        if (user?.plan !== user?.nextPlan) {
          return (
            <>
              {dayjs(user?.dueDate).format("YYYY년 MM월 DD일")}부터{" "}
              {PLAN_NAME[plan]} 구독을 시작합니다.
            </>
          );
        } else {
          return (
            <>
              {dayjs().format("YYYY년 MM월 DD일")}부터 {PLAN_NAME[plan]} 구독을
              시작합니다.
            </>
          );
        }
    }
  };

  return (
    <Wrapper>
      <LeftWrapper>
        <FlexWrapper1>
          <SuccessWrapper>
            <SuccessInnerWrapper>
              <SuccessContentWrapper>
                <SuccessIconWrapper>
                  <IconCheckCircle />
                </SuccessIconWrapper>
                <SuccessTitle>{getTitle()}</SuccessTitle>
                <SuccessSubtitle>{getSubtitle()}</SuccessSubtitle>
                <SuccessInfo>
                  <InfoTitle>뤼튼 서비스 이용 주의사항</InfoTitle>
                  <InfoDesc>
                    뤼튼으로 생성된 결과물의 저작권 및 사용 권한은 사용자에게
                    귀속되며, 개인적 용도 및 상업적 용도로 활용할 수 있습니다.
                    생성된 결과물을 사용하여 발생하는 문제의 책임 또한
                    사용자에게 있습니다.
                  </InfoDesc>
                  <InfoDesc>
                    뤼튼으로 생성된 결과물은 사용자가 입력한 내용을 최우선으로
                    반영하며, 경우에 따라 문장을 생성하는 과정에서 사실과 다른
                    내용이 나올 수 있습니다. 따라서 생성된 결과물을 활용 시 사실
                    관계 여부를 확인하시는 것을 권장드립니다.
                  </InfoDesc>
                  <InfoDesc>
                    뤼튼은 AI 윤리 준칙을 준수하여 선정적, 편향적, 기타 논란적인
                    내용을 의도적으로 입력하는 경우 경고 없이 계정이 정지될 수
                    있습니다. 계정 정지 시 별도 문의를 통해 정지를 해제할 수
                    있습니다.
                  </InfoDesc>
                  <InfoDesc>
                    자세한 내용은{" "}
                    <InfoLink
                      href="https://wrtn.ai/terms"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      서비스 이용약관
                    </InfoLink>
                    을 확인해 주세요.
                  </InfoDesc>
                </SuccessInfo>
              </SuccessContentWrapper>
              <PaymentButton onClick={() => (window.location.href = "/")}>
                완료
              </PaymentButton>
            </SuccessInnerWrapper>
          </SuccessWrapper>
        </FlexWrapper1>
      </LeftWrapper>
    </Wrapper>
  );
};

export default SettingSuccessPage;

SettingSuccessPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      {page}
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const LeftWrapper = styled.div`
  flex: 1;
  background: ${colors.BACKGROUND};
`;

const Wrapper = styled.div`
  width: 100vw;
  ${polyfill.dvh("height", 100)};
  background: white;
  display: flex;
`;

const FlexWrapper1 = styled(FlexWrapper)`
  padding: 20px;
  width: 100%;
  height: 100%;
`;

const SuccessWrapper = styled.div`
  margin: 0px auto;
  max-width: 784px;
  background: white;
  box-shadow: 5px 5px 20px rgba(0, 0, 0, 0.05);
  border-radius: 12px;
  width: 100%;
  max-height: 580px;
  height: 100%;
  padding-bottom: 30px;
  @media (max-width: 630px) {
    max-height: 100%;
  }
`;

const SuccessInnerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding: 20px;
  max-width: 692px;
  margin: 0px auto 24px;
  text-align: center;
`;

const SuccessContentWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const SuccessTitle = styled.p`
  font-size: 28px;
  line-height: 100%;
  color: #505467;
  font-weight: 700;
  margin-bottom: 11px;
  @media (max-width: 630px) {
    font-size: 20px;
  }
`;

const SuccessSubtitle = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  color: #505467;
  margin-bottom: 24px;
  @media (max-width: 630px) {
    font-size: 14px;
  }
`;

const SuccessIconWrapper = styled.div`
  width: 60px;
  height: 60px;
  margin-bottom: 24px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    width: 100%;
    height: 100%;
  }

  @media (max-width: 630px) {
    width: 48px;
    height: 48px;
  }
`;

const PaymentButton = styled.button`
  width: 100%;
  padding: 19px;
  background: ${colors.POINT_PURPLE};
  color: white;
  font-weight: 700;
  font-size: 20px;
  line-height: 100%;
  /* identical to box height, or 20px */

  text-align: center;

  /* Gray 3 */

  color: ${colors.GRAY_30};
  border-radius: 5px;
  border: none;
  cursor: pointer;

  @media (max-width: 630px) {
    padding: 10px;
    font-size: 16px;
  }
`;
const SuccessInfo = styled.div`
  padding: 20px 34px;
  border: 1px solid ${colors.gray_30};
  border-radius: 12px;
  display: flex;
  flex-direction: column;
  gap: 17px;
  align-items: flex-start;
  @media (max-width: 630px) {
    padding: 15px 15px;
  }
`;

const InfoTitle = styled.p`
  ${typo({
    weight: 700,
    size: "12px",
    height: "16.8px",
    color: colors.ACTION_BLUE,
  })};
  text-align: left;
`;

const InfoDesc = styled.p`
  ${typo({
    weight: 500,
    size: "12px",
    height: "16.8px",
    color: colors.gray_80,
  })};
  text-align: left;
`;

const InfoLink = styled.a`
  text-decoration: underline;
  color: ${colors.ACTION_BLUE};
  cursor: pointer;
`;
