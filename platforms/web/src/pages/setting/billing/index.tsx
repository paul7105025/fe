import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import { ReactElement } from "react";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import { useSearchParams } from "next/navigation";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";
import { MainLayout, SettingLNBLayout } from "src/layouts";

//@ts-ignore
import PaymentContainer from "src/containers/PaymentContainer/PaymentContainer";
import styled from "styled-components";
import Head from "next/head";
import React from "react";

const SettingBillingPage = (props) => {
  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  const router = useRouter();

  const searchParams = useSearchParams();

  const plan = searchParams.get("plan");
  const type = searchParams.get("type");

  if (!plan) {
    router.push("/");
  }

  return (
    <Wrapper>
      <ScrollWrapper>
        <PaymentContainer plan={plan} type={type} />
      </ScrollWrapper>
    </Wrapper>
  );
};

export default SettingBillingPage;

SettingBillingPage.getLayout = (page: ReactElement) => {
  return (
    <>
      {" "}
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      {page}
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      failure: paths.chat(),
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  overflow: scroll;
`;

const ScrollWrapper = styled.div`
  width: 100%;
`;
