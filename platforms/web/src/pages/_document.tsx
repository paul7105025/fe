import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from "next/document";
import { ServerStyleSheet } from "styled-components";

export default function DocumentPage() {
  return (
    <Html lang="ko">
      <Head>
        <link
          rel="stylesheet"
          href="https://unicons.iconscout.com/release/v4.0.0/css/line.css"
        />

        <meta
          name="description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta
          name="naver-site-verification"
          content="ad6cc073db55ea7d33320a302967959752d49412"
        />
      </Head>

      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}

DocumentPage.getInitialProps = async (ctx: DocumentContext) => {
  const sheet = new ServerStyleSheet(); // 서버사이드 렌더링 할 수 있게함.
  const originRender = ctx.renderPage;
  try {
    ctx.renderPage = () =>
      originRender({
        enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
      });

    if (!Document)
      return {
        styles: [<>{sheet.getStyleElement()}</>],
      };

    const initialProps = await Document?.getInitialProps(ctx);
    return {
      ...initialProps,
      styles: [
        <>
          {initialProps.styles}
          {sheet.getStyleElement()}
        </>,
      ],
    };
  } finally {
    sheet.seal();
  }
};
