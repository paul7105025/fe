import React, { ReactElement } from "react";
import {
  AuthCheckLayout,
  ChatLNBLayout,
  GlobalModalLayout,
  MainDarkLayout,
  MainLayout,
} from "../layouts";
import styled from "styled-components";

import Head from "next/head";

import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import { ParsedUrlQuery } from "querystring";
import { withAuthCheck } from "src/hoc";
import { EventLayout } from "src/layouts/EventLayout";
import { colors } from "@wrtn/ui";
import { ReactComponent as GooglePlayIcon } from "src/icons/googlePlay.svg";
import { ReactComponent as MacIcon } from "src/icons/mac.svg";
import { ReactComponent as AppIcon } from "src/icons/app.svg";
import { ReactComponent as MobileAppIcon } from "src/icons/mobileApp.svg";
import { ReactComponent as IssueIcon } from "src/icons/issue.svg";
import {
  WRTN_ANDROID_HREF,
  WRTN_IOS_HREF,
  showMessenger,
  useIs1024,
  useEvent,
} from "@wrtn/core";
import useInitAuth from "src/hooks/auth/useInitAuth";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 140px 180px 140px 268px;
`;

const MobileTextWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 48px auto 0 auto;
  width: 320px;
`;

const MobileBetaTitle = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  padding: 4px 6px;
  gap: 4px;
  width: 47px;
  height: 28px;
  left: 479px;
  top: 7px;
  color: #6446ff;
  background: #f3f0ff;
  border-radius: 5px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 100%;
`;

const TextAndButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 24px;
`;

const TitleWrapper = styled.div`
  display: flex;
  margin-top: 46px;
`;

const Title = styled.div`
  width: 354px;
  height: 124px;

  /* Content/48-bold */

  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 48px;
  line-height: 130%;
  /* or 62px */

  /* Black */

  color: #000000;

  /* Inside auto layout */

  flex: none;
  order: 0;
  flex-grow: 0;
`;

const MobileTitle = styled.div`
  height: 45px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 32px;
  line-height: 140%;
  color: #000000;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const Purple = styled.span`
  color: ${colors.POINT_PURPLE};
`;

const DescriptionWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  gap: 10px;
  margin-top: 20px;
  width: 476px;
  height: 70px;
  flex: none;
  order: 1;
  flex-grow: 0;
`;

const MobileDescription = styled.div`
  width: 320px;
  height: 81px;
  margin: 10px auto 0 auto;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 15px;
  line-height: 180%;
  color: #717688;
  flex: none;
  order: 1;
  align-self: stretch;
  flex-grow: 0;
`;

const Description = styled.div`
  height: 20px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 100%;
  /* identical to box height, or 20px */

  /* Gray_600 */

  color: #717688;

  /* Inside auto layout */

  flex: none;
  order: 0;
  flex-grow: 0;
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 16px;
  height: 40px;
  margin-left: 24px;
  justify-content: center;
  width: 437px;
`;

const MobileButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px;
  gap: 16px;
  margin: 56px auto 0 auto;
  width: 320px;
  height: 96px;
`;

const Button = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px;
  gap: 10px;
  cursor: pointer;

  width: 210.5px;
  height: 40px;

  background: #ffffff;
  /* Purple_500_primary */

  border: 1px solid #6446ff;
  border-radius: 5px;

  /* Inside auto layout */

  flex: none;
  order: 0;
  flex-grow: 1;
`;

const MobileButton = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px;
  gap: 10px;
  cursor: pointer;

  width: 320px;
  height: 48px;

  background: #ffffff;
  /* Purple_500_primary */

  border: 1px solid #6446ff;
  border-radius: 5px;

  /* Inside auto layout */

  flex: none;
  order: 0;
  flex-grow: 1;
`;

const ButtonText = styled.div`
  height: 16px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;
  color: #6446ff;
  flex: none;
  order: 0;
  flex-grow: 0;
`;

const AppImageWrapper = styled.div`
  width: 533px;
  height: 628px;
  margin-left: 90px;
`;

const MobileImageWrapper = styled.div`
  width: 384px;
  height: 452px;
  margin: 64px auto 0 auto;
`;

const IssueWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 8px;
  width: 196px;
  height: 26px;
  margin: 16px auto 0 auto;
`;

const MobileIssueWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 8px;
  width: 196px;
  height: 26px;
  margin: 32px auto 0 auto;
`;

const IssueText = styled.div`
  width: 168px;
  height: 26px;

  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 26px;
  /* identical to box height, or 186% */

  text-align: right;

  /* Gray_500 */

  color: #8f95ab;

  /* Inside auto layout */

  flex: none;
  order: 1;
  flex-grow: 0;
`;

const TermLink = styled.a`
  text-decoration: underline;
  cursor: pointer;
`;

const Beta = styled.span`
  position: relative;
  justify-content: center;
  bottom: 16px;
  left: 8px;
  align-items: center;
  padding: 4px 6px;
  background: #f3f0ff;
  border-radius: 5px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 100%;
  color: #6446ff;
  flex: none;
`;

const MobilePage = (props: any) => {
  const isMobile = useIs1024();
  const { collectEvent } = useEvent();

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  React.useEffect(() => {
    collectEvent("view_download_mobile");
  }, [])

  return (
    <>
      {isMobile ? (
        <>
          <MobileTextWrapper>
            <MobileBetaTitle>베타</MobileBetaTitle>
            <MobileTitle>
              {" "}
              Android & iOS용 &nbsp;
              <Purple>뤼튼</Purple>
            </MobileTitle>
            <MobileDescription>
              20만 사용자가 선택한 대한민국 AI 플랫폼을 모바일에서도 만나보세요!
              AI 채팅, AI 이미지 생성 기능을 누구나 사용할 수 있어요.
            </MobileDescription>
          </MobileTextWrapper>
          <MobileImageWrapper>
            <MobileAppIcon />
          </MobileImageWrapper>
          <MobileButtonWrapper>
            <a href={WRTN_ANDROID_HREF} target="_blank">
              <MobileButton>
                <GooglePlayIcon />
                <ButtonText>구글플레이</ButtonText>
              </MobileButton>
            </a>
            <a href={WRTN_IOS_HREF} target="_blank">
              <MobileButton>
                <MacIcon />
                <ButtonText>앱스토어</ButtonText>
              </MobileButton>
            </a>
          </MobileButtonWrapper>
          <MobileIssueWrapper>
            <IssueIcon />
            <IssueText>
              앱 이슈는{" "}
              <TermLink
                onClick={() => {
                  showMessenger();
                }}
              >
                이곳
              </TermLink>
              에 제보해주세요
            </IssueText>
          </MobileIssueWrapper>
        </>
      ) : (
        <>
          <Wrapper>
            <TextAndButtonWrapper>
              <TextWrapper>
                <TitleWrapper>
                  <Title>
                    Android & iOS용 <br />
                    <Purple>뤼튼</Purple>
                    <Beta>베타</Beta>
                  </Title>
                </TitleWrapper>
                <DescriptionWrapper>
                  <Description>
                    20만 사용자가 선택한 대한민국 AI 플랫폼을 모바일에서도!{" "}
                  </Description>
                  <Description>
                    AI 채팅, AI 이미지 생성 기능을 누구나 사용할 수 있어요.
                  </Description>
                </DescriptionWrapper>
              </TextWrapper>
              <ButtonWrapper>
                <a href={WRTN_ANDROID_HREF} target="_blank">
                  <Button>
                    <GooglePlayIcon />
                    <ButtonText>구글플레이</ButtonText>
                  </Button>
                </a>
                <a href={WRTN_IOS_HREF} target="_blank">
                  <Button>
                    <MacIcon />
                    <ButtonText>앱스토어</ButtonText>
                  </Button>
                </a>
              </ButtonWrapper>
              <IssueWrapper>
                <IssueIcon />
                <IssueText>
                  앱 이슈는{" "}
                  <TermLink
                    onClick={() => {
                      showMessenger();
                    }}
                  >
                    이곳
                  </TermLink>
                  에 제보해주세요
                </IssueText>
              </IssueWrapper>
            </TextAndButtonWrapper>
            <AppImageWrapper>
              <AppIcon />
            </AppImageWrapper>
          </Wrapper>
        </>
      )}
    </>
  );
};

export default MobilePage;

MobilePage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai/mobile" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>
        <GlobalModalLayout>
          <EventLayout>
            <MainLayout>{page}</MainLayout>
          </EventLayout>
        </GlobalModalLayout>
      </AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};
