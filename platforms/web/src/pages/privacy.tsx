import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import React from "react";
import { ParsedUrlQuery } from "querystring";
import { withAuthCheck } from "src/hoc";
import { privacy } from "../templates/privacy";

const PrivacyPage = () => {
  return (
    <div>
      <div dangerouslySetInnerHTML={{ __html: privacy }}></div>
    </div>
  );
};

export default PrivacyPage;

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};
