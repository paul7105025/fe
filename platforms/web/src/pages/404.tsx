import React from 'react';
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";

export const ErrorPage = () => {
  const router = useRouter();
  React.useEffect(() => {
    router.push("/")
  }, [])
  return (
    <>
    </>
  )
}

export default ErrorPage;