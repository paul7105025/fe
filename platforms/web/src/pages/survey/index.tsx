import React from "react";
import Head from "next/head";
import styled from "styled-components";

import { useSurveyLogic } from "src/hooks/auth";
import TermSocialPage from "src/containers/survey/TermSocialPage";
import SurveyContainer from "src/containers/survey/SurveyContainer";

import polyfill from "@wrtn/ui/styles/polyfill";
import { FlexWrapper, WrtnLogoGray } from "@wrtn/ui";

// import { ReactComponent as Logo } from "../assets/logos/logo_wrtn.svg";

const ConsentPage = () => {
  const {
    surveyStep,
    surveyData,
    isEnableUserText,
    userText,
    isEnableNext,
    isRecommendError,
    handleClickSurvey,
    onChangeSurvey,
    onChangeText,
    handleNext,
  } = useSurveyLogic();

  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>

      <Wrapper justify="center">
        <ContentWrapper justify={"center"}>
          {surveyStep === "STEP_TERMS" ? (
            <TermSocialPage
              handleNext={handleNext}
              surveyData={surveyData}
              onChangeSurvey={onChangeSurvey}
            />
          ) : (
            <SurveyContainer
              // surveyStep='STEP_JOB_SURVEY'
              surveyData={surveyData}
              isEnableUserText={isEnableUserText}
              userText={userText}
              isEnableNext={isEnableNext}
              isRecommendError={isRecommendError}
              handleClickSurvey={handleClickSurvey}
              onChangeSurvey={onChangeSurvey}
              onChangeText={onChangeText}
              handleNext={handleNext}
            />
          )}
        </ContentWrapper>
        <BottomLogo>
          <WrtnLogoGray />
        </BottomLogo>
      </Wrapper>
    </>
  );
};

export default ConsentPage;

const Wrapper = styled(FlexWrapper)`
  flex-direction: column;
  width: 100%;
  ${polyfill.dvh("height", 100)};

  position: relative;
  background: linear-gradient(180deg, #f3f4ff -24.67%, #ffffff 103.17%);
`;

const ContentWrapper = styled(FlexWrapper)`
  flex-direction: column;

  width: 100%;
  height: 100%;
  overflow: auto;

  padding: 20px;
  @media (max-width: 480px) {
    justify-content: flex-start;
  }

  z-index: 2;
`;

const BottomLogo = styled.div`
  position: absolute;

  margin: 0px auto;
  bottom: 36px;

  > svg {
    height: 28px;
    width: 255px;
    path {
      fill: #eceef7;
    }
  }

  @media (max-width: 1023px) {
    margin: 60px 0px;
  }
  @media (max-width: 480px) {
    display: flex;
    justify-content: center;
    > svg {
      width: 180px;
    }
  }
`;
