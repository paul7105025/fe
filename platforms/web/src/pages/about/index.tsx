import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import React, { ReactElement } from "react";
import styled from "styled-components";
import { ParsedUrlQuery } from "querystring";

import { withAuthCheck } from "src/hoc";

import { AuthCheckLayout, MainLayout } from "src/layouts";

import {
  AboutIntro,
  AboutFeature,
  AboutMain,
  AboutNew,
  AboutPower,
  AboutFooter,
} from "src/components/About";

import { FlexWrapper } from "@wrtn/ui";
import { useEvent } from "@wrtn/core";
import Head from "next/head";
import useInitAuth from "src/hooks/auth/useInitAuth";

const AboutPage = (props: any) => {
  const { collectEvent } = useEvent();

  React.useEffect(() => {
    collectEvent("view_about_page");
  }, []);

  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  return (
    <AboutWrapper column>
      <ScrollWrapper column>
        <AboutIntro />
        <AboutFeature />
        <AboutMain />
        <AboutNew />
        <AboutPower />
        <AboutFooter />
      </ScrollWrapper>
    </AboutWrapper>
  );
};

export default AboutPage;

const AboutWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  overflow: auto;
`;

const ScrollWrapper = styled(FlexWrapper)`
  width: 100%;
`;

AboutPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>
        <MainLayout>{page}</MainLayout>
      </AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};
