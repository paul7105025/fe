import { useRouter } from "next/router";
import React from "react";

const NHNPage = () => {
  const router = useRouter();
  const { code } = router.query;

  React.useEffect(() => {
    if(code){
      router.replace({
        pathname: "/",
        query: {
          code: code,
          funnel: "nhn_commerce"
        }
      })
    }
  }, [code]);

  return (
    <>
      redirect...
    </>
  )
}

export default NHNPage;
