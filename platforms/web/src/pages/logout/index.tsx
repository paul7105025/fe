import { useLogout } from "@wrtn/core";
import { useRouter } from "next/router";
import React from "react";
import { paths } from "src/constants";
import { useUser } from "src/hooks/useUser";

const LogoutPage = () => {
  const { user } = useUser();
  const logout = useLogout();
  const router = useRouter();

  React.useEffect(() => {
    logout(user).then(() => {
      router.push(paths.chat());
    });
  }, []);

  return <></>;
};

export default LogoutPage;
