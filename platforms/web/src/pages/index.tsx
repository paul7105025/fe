import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import dynamic from "next/dynamic";
import styled from "styled-components";
import React, { ReactElement } from "react";
import { ParsedUrlQuery } from "querystring";
import { MFELoadingPage } from "@wrtn/ui/containers/MFE";

import { FlexWrapper } from "@wrtn/ui";
import { withAuthCheck } from "src/hoc";

import useInitAuth from "../hooks/auth/useInitAuth";

import { MainLayout } from "src/layouts";
import { ChatLNBLayout } from "src/layouts/ChatLNBLayout";
import { AuthCheckLayout } from "src/layouts/AuthCheckLayout";
import { GlobalModalLayout } from "src/layouts/GlobalModalLayout";
import { useRecoilValue } from "recoil";

import Auth from "src/components/Auth/Auth";
import { useEvent, useFetchTemplate, userState } from "@wrtn/core";
import { EventLayout } from "src/layouts/EventLayout";
import Head from "next/head";
import { useRouter } from "next/router";
import DesktopUpdateLayout from "src/layouts/DesktopUpdateLayout";

const Chat: React.ComponentType<{
  renderAuth: () => React.ReactElement;
}> = dynamic(() => import("$chat/ChatPage"), {
  ssr: false,
  loading: () => <MFELoadingPage />,
  // loader: () => import("@wrtn/ui/containers/MFE/MFEWrapper")
});

const ChatPage = (props: any) => {
  const { collectEvent } = useEvent();
  const user = useRecoilValue(userState);
  const router = useRouter();
  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  React.useEffect(() => {
    const timer = setTimeout(() => {
      collectEvent("view_main_page", {
        isLogin: !!user,
      });
    }, 1000);

    return () => clearTimeout(timer);
  }, [user]);

  React.useEffect(() => {
    window.sessionStorage.removeItem("cafe24_code");
    window.sessionStorage.removeItem("nhn_code");
    window.sessionStorage.removeItem("login_app");
    window.sessionStorage.removeItem("redirect_url");
    const query = new URLSearchParams(location.search);
    const code = query.get("code");
    const funnel = query.get("funnel");
    const inviter = query.get("inviter");
    if (funnel !== "nhn_commerce" && code) {
      window.sessionStorage.setItem("cafe24_code", code);
    }
    if (funnel === "nhn_commerce" && code) {
      window.sessionStorage.setItem("nhn_code", code);
    }
    const utm_campaign = query.get("utm_campaign");
    const utm_source = query.get("utm_source");
    if (utm_campaign) {
      window.sessionStorage.setItem("utm_campaign", utm_campaign);
    }
    if (utm_source) {
      window.sessionStorage.setItem("utm_source", utm_source);
    }
    if (inviter) {
      window.sessionStorage.setItem("plugin_invite", inviter);
    }
    window.sessionStorage.removeItem("pluginPath");
  }, []);

  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <Wrapper>
        <ChatWrapper>
          <Chat
            renderAuth={() => {
              return <Auth position={"main"} />;
            }}
          />
        </ChatWrapper>
      </Wrapper>
    </>
  );
};

ChatPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>
        <DesktopUpdateLayout>
          <GlobalModalLayout>
            <EventLayout>
              <MainLayout>
                <ChatLNBLayout>{page}</ChatLNBLayout>
              </MainLayout>
            </EventLayout>
          </GlobalModalLayout>
        </DesktopUpdateLayout>
      </AuthCheckLayout>
    </>
  );
};

export default ChatPage;

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};

const Wrapper = styled(FlexWrapper)`
  flex: 1 1 auto;
  width: 100%;
  height: 100%;
`;

const LNBWrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const ChatWrapper = styled.div`
  width: 100%;
  height: 100%;
`;
