import { colors, FlexWrapper, LogoEmailRound, LogoWrtnPurple, newColors, typo } from "@wrtn/ui";
import { GetServerSideProps, GetServerSidePropsContext, PreviewData } from "next";
import { ParsedUrlQuery } from "querystring";
import Auth from "src/components/Auth/Auth";
import LoginProviderButton from "src/components/Button";
import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import styled from "styled-components";
import { useSocialLogin } from "src/hooks/auth";
import React from "react";
import { useRouter } from "next/router";
import { useEvent, useLoginDialog } from "@wrtn/core";
import { useLocalLogin } from "src/hooks/auth/useLocalLogin";


const DesktopPage = () => {
  const position = "desktop"
  const { handleClose } = useLoginDialog();
  const { collectEvent } = useEvent();
  const router = useRouter();

  const {
    handleGoogleLoginSuccess,
    handleGoogleLoginFailure,
    handleKakaoLoginFailure,
    handleKakaoLoginSuccess,
    handleNaverLoginFailure,
    handleNaverLoginSuccess,
    handleAppleLoginFailure,
    handleAppleLoginSuccess
  } = useSocialLogin("desktop");

  React.useEffect(() => {
    window.sessionStorage.setItem("login_app", "dt");
  }, [])


  const localLogin = useLocalLogin();

  return ( 
    <Wrapper>
      <FlexWrapper column>
        <LogoWrtnPurple />
        <Title>뤼튼 데스크탑앱 시작하기</Title>
        <SubTitle>웹에서 로그인/회원가입하시면 데스크탑앱과 연동돼요!</SubTitle>
        {/* <Auth position={"desktop"}/> */}
        <AuthLoginButtons>
        <RoundButton style={{ transform: "scale(1.375) translate(6px, 7px)" }}>
          <LoginProviderButton.Google
            size={"large"}
            type="icon"
            shape="circle"
            onSuccess={handleGoogleLoginSuccess}
            onError={handleGoogleLoginFailure}
            width={"300px"}
          />
        </RoundButton>
        <RoundButton>
          <LoginProviderButton.NaverRound
            onSuccess={handleNaverLoginSuccess}
            onFailure={handleNaverLoginFailure}
            callbackUrl={
              process.env.NEXT_PUBLIC_NAVER_REDIRECT_URL || window.location.href
            }
          />
        </RoundButton>
        <RoundButton>
          <LoginProviderButton.KakaoRound
            onSuccess={handleKakaoLoginSuccess}
            onFail={handleKakaoLoginFailure}
          />
        </RoundButton>
        <RoundButton>
          <LoginProviderButton.Apple
            isDefaultStyle={false}
            onSuccess={handleAppleLoginSuccess}
            onFail={handleAppleLoginFailure}
          />
        </RoundButton>
        <RoundButton
          onClick={() => {
            collectEvent("click_start_platform_btn", {
              platform: "wrtn",
              position: position,
            });
            handleClose();
            router.push({
              pathname: paths.login.local.index(),
              query: router.query
            });
          }}
        >
          <LogoEmailRound />
        </RoundButton>
        </AuthLoginButtons>
        <BottomWrapper>
          <FindWrapper>
            <FindButton onClick={localLogin.handleFindPassword}>
              비밀번호 찾기
            </FindButton>
          </FindWrapper>
          <ServiceGuideWrapper row>
            <GuideText href="https://wrtn.ai/terms">이용약관</GuideText>
            <Divider />
            <GuideText href="https://wrtn.ai/privacy">개인정보처리방침</GuideText>
          </ServiceGuideWrapper>
        </BottomWrapper>
      </FlexWrapper>
    </Wrapper>
  )                            
}

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      success: paths.login.desktop.success(),
    },
  });
};

export default DesktopPage;

export const Wrapper = styled(FlexWrapper)`
  flex: 1 1 auto;
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  background: linear-gradient(180deg, #f3f4ff -24.67%, #ffffff 103.17%);
`;

export const Title = styled.p`
  ${typo({ weight: 700, size: "24px", height: "100%", color: colors.gray_90 })};
  margin-bottom: 16px;
  margin-top: 32px;
`;

export const SubTitle = styled.p`
  ${typo({ weight: 500, size: "16px", height: "100%", color: colors.gray_90 })};
  margin-bottom: 33px;
`;

export const AuthLoginButtons = styled(FlexWrapper)`
  gap: 13px;

  @media (max-width: 767px) {
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const RoundButton = styled.div`
  width: 55px;
  height: 55px;
  border-radius: 100%;
  cursor: pointer;
`;


export const BottomWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 40px;
  margin-top: 24px;
`;

export const FindWrapper = styled(FlexWrapper)`
  display: flex;
  flex-direction: row;
  gap: 10px;
  align-items: center;
  justify-content: center;
`;

export const FindButton = styled.div`
  ${typo({ weight: 500, size: "14px", height: "100%", color: colors.gray_70 })}
  cursor: pointer;
  white-space: nowrap;
  text-decoration-line: underline;
  text-align: center;
`;

export const ServiceGuideWrapper = styled(FlexWrapper)`
  gap: 10.5px;
  justify-content: center;
  align-items: center;
`;

export const GuideText = styled.a`
  ${typo({ weight: 500, size: '14px', height: '100%', color: newColors.GRAY_500 })}
  
  &:hover {
    text-decoration: underline;
  }
`;

export const Divider = styled.div`
  width: 1px;
  height: 14px;
  background-color: ${newColors.GRAY_100};
`;