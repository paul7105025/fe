import React from "react";
import { getCookie } from 'cookies-next';
import { cookieKey } from "src/constants/cookieKey";
import { colors, FlexWrapper, LogoWrtnPurple, typo } from "@wrtn/ui";
import { useRouter } from "next/router";
import { paths } from "src/constants";
import styled from "styled-components";

const DesktopSuccessPage = () => {
  const router = useRouter();
  React.useEffect(() => {
    const refreshToken = getCookie(cookieKey.refreshToken)?.toString().trim();
    const accessToken = getCookie(cookieKey.accessToken)?.toString().trim();
    
    if (!refreshToken || refreshToken.length === 0) router.push(paths.login.desktop.index());
    
    window.location.replace(`wrtn://refresh_token=${refreshToken}&access_token=${accessToken}`)
  }, []);
  // return <></>

  // return <>
  // </>

  return ( 
    <>
    <Wrapper>
      <FlexWrapper column>
        <LogoWrtnPurple />
        <Title>뤼튼 데스크탑앱 로그인 완료</Title>
      </FlexWrapper>
    </Wrapper>
    </>
  )
}

export const getServerSideProps = async () => {
  return {
    props: {}
  }
}

// export const getServerSideProps: GetServerSideProps = async (
//   ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
// ) => {
//   return withAuthCheck(ctx, {
//     redirect: {
//       // success: paths.login.desktop.success(),
//     },
//   });
// };


export default DesktopSuccessPage;

export const Wrapper = styled(FlexWrapper)`
  flex: 1 1 auto;
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  background: linear-gradient(180deg, #f3f4ff -24.67%, #ffffff 103.17%);
`;

export const Title = styled.p`
  ${typo({ weight: 700, size: "24px", height: "100%", color: colors.gray_90 })};
  margin-bottom: 16px;
  margin-top: 32px;
`;

export const SubTitle = styled.p`
  ${typo({ weight: 500, size: "16px", height: "100%", color: colors.gray_90 })};
  margin-bottom: 33px;
`;

export const AuthLoginButtons = styled(FlexWrapper)`
  gap: 13px;

  @media (max-width: 767px) {
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const RoundButton = styled.div`
  width: 55px;
  height: 55px;
  border-radius: 100%;
  cursor: pointer;
`;


export const BottomWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 18px;
  margin-top: 24px;
`;

export const FindWrapper = styled(FlexWrapper)`
  display: flex;
  flex-direction: row;
  gap: 10px;
  align-items: center;
  justify-content: center;
`;

export const FindButton = styled.div`
  ${typo({ weight: 500, size: "14px", height: "100%", color: colors.gray_70 })}
  cursor: pointer;
  white-space: nowrap;
  text-decoration-line: underline;
  text-align: center;
`;