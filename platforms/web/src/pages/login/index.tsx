import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import React, { ReactElement } from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";
import { ParsedUrlQuery } from "querystring";

import { useEvent } from "@wrtn/core/hooks";

import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import LoginProviderButton from "src/components/Button";

import { paths } from "src/constants";

import { withAuthCheck } from "src/hoc";
import { useSocialLogin } from "src/hooks/auth";
import { useLocalLogin } from "src/hooks/auth/useLocalLogin";
import useBlockInAppBrowser from "src/hooks/auth/useBlockInAppBrowser";

import { WrtnLogoGray } from "@wrtn/ui/assets";
import { boxShadow, colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import { AuthCheckLayout } from "src/layouts";
import Head from "next/head";

const token = process.env.NEXT_PUBLIC_MIXPANEL_TOKEN || "";
const NAVER_CLIENT_ID = process.env.NEXT_PUBLIC_NAVER_CLIENT_ID;
const isDev =
  process.env.NODE_ENV !== "production" ||
  process.env.NEXT_PUBLIC_ACTIVE_MIXPANEL === "true";
const GOOGLE_CLIENT_ID = process.env.NEXT_PUBLIC_URL_GOOGLE_CLIENT_ID;
const loginSuccessPath = "/app";

const LoginPage = () => {
  const blockInAppBrowser = useBlockInAppBrowser();

  const {
    collectEvent,
    initEvent,
    collectUserProperties_once,
    collectUserProperties,
  } = useEvent();

  const localLogin = useLocalLogin();

  React.useEffect(() => {
    window.sessionStorage.removeItem("cafe24_code");
    window.sessionStorage.removeItem("nhn_code");
    const query = new URLSearchParams(location.search);
    const code = query.get("code");
    const funnel = query.get("funnel");
    if (!funnel && code) {
      window.sessionStorage.setItem("cafe24_code", code);
    }
    if (funnel === "nhn_commerce" && code) {
      window.sessionStorage.setItem("nhn_code", code);
    }
    const utm_campaign = query.get("utm_campaign");
    const utm_source = query.get("utm_source");
    if (utm_campaign) {
      window.sessionStorage.setItem("utm_campaign", utm_campaign);
    }
    if (utm_source) {
      window.sessionStorage.setItem("utm_source", utm_source);
    }
  }, []);

  const {
    handleGoogleLoginSuccess,
    handleKakaoLoginSuccess,
    handleNaverLoginSuccess,
    handleAppleLoginSuccess,
    handleGoogleLoginFailure,
    handleNaverLoginFailure,
    handleKakaoLoginFailure,
    handleAppleLoginFailure,
  } = useSocialLogin("login");

  React.useEffect(() => {
    if (!isDev) {
      return;
    }
    initEvent(token, () => {
      const search_params = new URLSearchParams(window.location.search);
      const searchMap = {};
      for (const [key, value] of search_params.entries()) {
        searchMap[key] = value;
      }
      collectUserProperties_once("searchParams", JSON.stringify(searchMap));
      collectUserProperties_once("firstTouchDate", new Date());
    });
  }, []);

  return (
    <Wrapper justify="center" align="center" style={{ height: "100%" }}>
      <Content column>
        <Title>뤼튼 시작하기</Title>
        <SubTitle>뤼튼에 오신 걸 환영해요!</SubTitle>
        <LoginContent>
          <LoginProviderButton.Google
            useOneTap
            onSuccess={handleGoogleLoginSuccess}
            onError={handleGoogleLoginFailure}
            width={"300px"}
          />
          <LoginProviderButton.Naver
            onSuccess={handleNaverLoginSuccess}
            onFailure={handleNaverLoginFailure}
            callbackUrl={
              process.env.NEXT_PUBLIC_NAVER_REDIRECT_URL || window.location.href
            }
          />
          <LoginProviderButton.Kakao
            onSuccess={handleKakaoLoginSuccess}
            onFail={handleKakaoLoginFailure}
          />
          <LoginProviderButton.Apple
            isDefaultStyle={true}
            onSuccess={handleAppleLoginSuccess}
            onFail={handleAppleLoginFailure}
          />
          <HorizontalLine />
          <LoginProviderButton.Local onClick={localLogin.handleLoginEmail} />
        </LoginContent>
        <FindWrapper>
          <FindButton onClick={localLogin.handleFindPassword}>
            비밀번호 찾기
          </FindButton>
        </FindWrapper>
        <Caption>
          계정 생성을 완료하면{" "}
          <CurrencyLink target="_blank" rel="noopener noreferrer" href="/terms">
            서비스이용약관
          </CurrencyLink>
          ,{" "}
          <CurrencyLink
            target="_blank"
            rel="noopener noreferrer"
            href="/privacy"
          >
            개인정보처리방침
          </CurrencyLink>
          에 동의하게 됩니다.
        </Caption>
      </Content>
      <BottomLogo>
        <WrtnLogoGray />
      </BottomLogo>
      {/* {isBan && (
        <ModalPortal>
          <Dialog
            disableCancel
            rightButtonLabel="확인"
            title="계정이 일시정지 되었습니다."
            description={["자세한 사항은 관리자에게 문의하세요."]}
            handleRightButton={() =>
              (window.location.href = window.location.origin)
            }
          />
        </ModalPortal>
      )} */}
      {blockInAppBrowser ? (
        <ModalPortal>
          <Dialog
            disableCancel
            rightButtonLabel="확인"
            description={[
              "크롬 / 사파리 등",
              "다른 브라우저를 통해",
              "접속해주세요! ",
            ]}
            handleRightButton={() =>
              (window.location.href = window.location.origin)
            }
            handleClose={() => {
              window.location.href = window.location.origin;
            }}
          />
        </ModalPortal>
      ) : null}
    </Wrapper>
  );
};

LoginPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="고객의 반응을 얻고 매출을 늘리는 경쟁력 있는 카피, 강력한 AI와 함께 10배 빠르게 완성하세요."
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>{page}</AuthCheckLayout>
    </>
  );
};

const LoginLink = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  gap: 48px;
  cursor: pointer;
  background: ${({ type }) => {
    if (type === "naver") return colors.BRAND_NAVER_MAIN;
    else if (type === "local") return colors.WHITE;
    return "";
  }};
  height: 38px;
  border-radius: 5px;
  width: 300px;
  max-width: 400px;
  min-width: 200px;
  padding: 0px 25px;
  cursor: pointer;
  ${typo({ weight: 600, size: "14px", height: "100%" })};
  color: ${({ type }) => {
    if (type === "naver") return colors.WHITE;
    else if (type === "local") return colors.gray_80;
    return "";
  }};
  ${({ type }) => type === "local" && boxShadow.login_box_shadow};

  @media (max-width: 480px) {
    font-size: 14px;
  }
`;

const EmailLoginLink = styled(LoginLink)`
  border: 1px solid #dee4f3;
`;

const Wrapper = styled(FlexWrapper)`
  flex: 1 1 auto;
  display: flex;
  width: 100%;
  height: 100%;
  background: linear-gradient(180deg, #f3f4ff -24.67%, #ffffff 103.17%);
`;

const Content = styled(FlexWrapper)``;

const Title = styled.p`
  ${typo({ weight: 700, size: "24px", height: "100%", color: colors.gray_90 })};
  margin-bottom: 16px;
`;

const SubTitle = styled.p`
  ${typo({ weight: 500, size: "16px", height: "100%", color: colors.gray_90 })};
  margin-bottom: 33px;
`;

const LoginContent = styled.div`
  margin-bottom: 26px;
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

const CurrencyLink = styled.a`
  text-decoration: underline;
  color: ${colors.GRAY_70};

  :active {
    color: ${colors.GRAY_70};
  }
`;

const Caption = styled.p`
  text-align: center;
  ${typo({ weight: 500, size: "13px", height: "150%", color: colors.gray_70 })};

  @media (max-width: 480px) {
    max-width: 300px;
  }
  word-break: keep-all;
`;

const BottomLogo = styled.div`
  position: absolute;
  bottom: 50px;
  left: 50%;
  transform: translateX(-50%);
`;

const HorizontalLine = styled.div`
  height: 1px;
  background-color: ${colors.BLUE_GRAY_LINE};
`;

const FindWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 19px;
  align-items: center;
  padding-bottom: 25px;
`;

const FindButton = styled.div`
  ${typo({ weight: 500, size: "14px", height: "100%", color: colors.gray_70 })}
  cursor: pointer;
  text-decoration-line: underline;
`;

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      success: paths.chat(),
    },
  });
};

export default LoginPage;
