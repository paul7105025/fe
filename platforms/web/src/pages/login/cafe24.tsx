import React from "react";

const client_id = "ofgbs7FrAi7DvxuXsCKuDF";

const Cafe24Page = () => {
  React.useEffect(() => {
    const query = new URLSearchParams(location.search);
    const mall_id = query.get("mall_id");
    const encode_redirect_uri = encodeURIComponent(window.location.origin + "/")
    const scope = "mall.read_store"
    const url = `https://${mall_id}.cafe24api.com/api/v2/oauth/authorize?response_type=code&client_id=${client_id}&redirect_uri=${encode_redirect_uri}&scope=${scope}`
    window.location.href=url;
  }, []);

  return <>
  </>
}

export default Cafe24Page;
