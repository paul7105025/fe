import { useEvent } from "@wrtn/core";
import React, { ReactElement } from "react";
// import { ReactComponent as LogoWrtn } from "../../assets/logos/logo_wrtn.svg";
import EmailLoginSection from "../../../containers/EmailLoginContainer/EmailLoginSection";
import {
  LocalPageBottomLogo,
  LocalPageBox,
  LocalPageInnerWrapper,
  LocalPageWrapper,
} from "../../../components/LocalLoginComponent";
import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import { ParsedUrlQuery } from "querystring";
import { withAuthCheck } from "src/hoc";
import { paths } from "src/constants";
import { AuthCheckLayout } from "src/layouts";
import Head from "next/head";

export const WrtnSignInPage = ({ loginSuccessPath }) => {
  const { collectEvent } = useEvent();

  React.useEffect(() => {
    collectEvent("view_email_login_page");
  }, []);

  return (
    <LocalPageWrapper justify="center">
      <LocalPageBox>
        <LocalPageInnerWrapper>
          <EmailLoginSection />
        </LocalPageInnerWrapper>
      </LocalPageBox>
      <LocalPageBottomLogo>{/* <LogoWrtn /> */}</LocalPageBottomLogo>
    </LocalPageWrapper>
  );
};

WrtnSignInPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>{page}</AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      success: paths.chat(),
    },
  });
};
export default WrtnSignInPage;
