import { useEvent } from "@wrtn/core";
import React, { ReactElement } from "react";
// import { ReactComponent as LogoWrtn } from "../../assets/logos/logo_wrtn.svg";
import EmailSection from "../../../containers/EmailLoginContainer/EmailSection";
import {
  LocalPageBottomLogo,
  LocalPageBox,
  LocalPageInnerWrapper,
  LocalPageWrapper,
} from "../../../components/LocalLoginComponent";
import {
  GetServerSideProps,
  GetServerSidePropsContext,
  PreviewData,
} from "next";
import { ParsedUrlQuery } from "querystring";
import { withAuthCheck } from "src/hoc";
import { paths } from "src/constants";
import { AuthCheckLayout } from "src/layouts";
import Head from "next/head";
import { getCookie } from "cookies-next";
import { useRouter } from "next/router";

export const WrtnLoginPage = () => {
  const { collectEvent } = useEvent();
  const router = useRouter();

  React.useEffect(() => {
    collectEvent("view_start_with_email_page");
  }, []);

  React.useEffect(() => {
    const redirect_url = router.query.redirect_url;
    if (redirect_url) sessionStorage.setItem('redirect_url', typeof redirect_url === "string" ? redirect_url : redirect_url[0])
  }, [])

  return (
    <LocalPageWrapper justify="center">
      <LocalPageBox>
        <LocalPageInnerWrapper style={{ maxWidth: "350px" }}>
          <EmailSection />
        </LocalPageInnerWrapper>
      </LocalPageBox>
      <LocalPageBottomLogo>{/* <LogoWrtn /> */}</LocalPageBottomLogo>
    </LocalPageWrapper>
  );
};

WrtnLoginPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>{page}</AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      success: paths.chat(),
    },
  });
};
export default WrtnLoginPage;
