import React, { ReactElement } from "react";
import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import { ParsedUrlQuery } from "querystring";

import { useEvent } from "@wrtn/core";

import EmailCertSection from "../../../containers/EmailLoginContainer/EmailCertSection";
import {
  LocalPageBottomLogo,
  LocalPageBox,
  LocalPageInnerWrapper,
  LocalPageWrapper,
} from "../../../components/LocalLoginComponent";

import { paths } from "src/constants";
import { withAuthCheck } from "src/hoc";
import { AuthCheckLayout } from "src/layouts";
import Head from "next/head";

export const WrtnCertPage = () => {
  const { collectEvent } = useEvent();

  React.useEffect(() => {
    collectEvent("view_verify_email_page");
  }, []);

  return (
    <LocalPageWrapper justify="center">
      <LocalPageBox style={{ height: "702px" }}>
        <LocalPageInnerWrapper>
          <EmailCertSection />
        </LocalPageInnerWrapper>
      </LocalPageBox>
      <LocalPageBottomLogo>{/* <LogoWrtn /> */}</LocalPageBottomLogo>
    </LocalPageWrapper>
  );
};

WrtnCertPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>{page}</AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      success: paths.chat(),
    },
  });
};

export default WrtnCertPage;
