import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import dynamic from "next/dynamic";
import React, { ReactElement } from "react";
import styled from "styled-components";
import { ParsedUrlQuery } from "querystring";

import { FlexWrapper } from "@wrtn/ui";
import { MFELoadingPage, MFEWrapper } from "@wrtn/ui/containers/MFE";

import { MainLayout, ToolLNBLayout, AuthCheckLayout } from "src/layouts";

import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";
import InappChatLayout from "src/layouts/InappChatLayout";
import { useEvent } from "@wrtn/core";
import { ErrorModalLayout } from "src/layouts/ErrorModalLayout";
import Head from "next/head";

const Tool = dynamic(() => import("$tool/ToolPage"), {
  ssr: false,
  loading: () => <MFELoadingPage />,
});

const ToolPage = (props) => {
  const { collectEvent } = useEvent();
  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  React.useEffect(() => {
    collectEvent("view_tool_page");
    window.sessionStorage.removeItem("pluginPath");
    window.sessionStorage.removeItem("login_app");
  }, []);

  return (
    <Wrapper>
      <ToolWrapper>
        <Tool />
      </ToolWrapper>
    </Wrapper>
  );
};

ToolPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>
        <ErrorModalLayout>
          <MainLayout>
            <InappChatLayout>
              <ToolLNBLayout>{page}</ToolLNBLayout>
            </InappChatLayout>
          </MainLayout>
        </ErrorModalLayout>
      </AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
  });
};

export default ToolPage;

const Wrapper = styled(FlexWrapper)`
  flex: 1 1 auto;
  width: 100%;
  height: 100%;
`;

const ToolWrapper = styled.div`
  width: 100%;
  height: 100%;
`;
