import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import React from "react";
import { ParsedUrlQuery } from "querystring";
import { withAuthCheck } from "src/hoc";
import { terms } from "../templates/terms";

const TermsPage = () => {
  return (
    <div>
      <div dangerouslySetInnerHTML={{ __html: terms }}></div>
    </div>
  );
};

export default TermsPage;

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
    // updateTool: true,
    // updateUser: true,
  });
};
