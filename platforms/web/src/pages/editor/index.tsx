import {
  PreviewData,
  GetServerSideProps,
  GetServerSidePropsContext,
} from "next";
import dynamic from "next/dynamic";
import React, { ReactElement } from "react";
import styled from "styled-components";
import { ParsedUrlQuery } from "querystring";

import { FlexWrapper } from "@wrtn/ui";
import { MFEWrapper, MFELoadingPage } from "@wrtn/ui/containers/MFE";

import { withAuthCheck } from "src/hoc";
import useInitAuth from "src/hooks/auth/useInitAuth";
import { AuthCheckLayout, MainLayout, ChannelLayout } from "src/layouts";
import { useEvent, useIsMobile, showChannelButton, hideChannelButton } from "@wrtn/core";
import Head from "next/head";

const Editor = dynamic(() => import("$editor/EditorDocumentListPage"), {
  ssr: false,
  loading: () => <MFELoadingPage />,
});

const EditorPage = (props) => {
  const { collectEvent } = useEvent();
  const isMobile = useIsMobile();
  useInitAuth({
    accessKey: props.accessKey,
    props,
    options: {
      updateUser: "unique",
      updateTool: "unique",
    },
  });

  React.useEffect(() => {
    collectEvent("view_editor_list_page");
    window.sessionStorage.removeItem("pluginPath");
    window.sessionStorage.removeItem("login_app");
    if (!isMobile) {
      showChannelButton();
    }
    return () => {
      hideChannelButton();
    }
   }, []);
  return (
    <Wrapper>
      <EditorWrapper>
        <Editor />
      </EditorWrapper>
    </Wrapper>
  );
};

EditorPage.getLayout = (page: ReactElement) => {
  return (
    <>
      <Head>
        <meta property="og:title" content="wrtn" />
        <meta property="og:site_name" content="뤼튼" />
        <meta property="og:url" content="https://wrtn.ai" />
        <meta
          property="og:description"
          content="챗gpt보다 나은 한국형 gpt-4 이미지 생성 무제한 무료 Gen AI 챗봇 - 나만의 AI를 직접 만들수도 있습니다"
        />
        <meta property="og:type" content="" />
        <meta property="og:image" content="https://ifh.cc/g/r69tQl.png" />
      </Head>
      <AuthCheckLayout>
        <ChannelLayout>
          <MainLayout>{page}</MainLayout>
        </ChannelLayout>
      </AuthCheckLayout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
) => {
  return withAuthCheck(ctx, {
    redirect: {
      // failure: paths.login.index()
    },
  });
};

export default EditorPage;

const Wrapper = styled(FlexWrapper)`
  flex: 1 1 auto;
  width: 100%;
  height: 100%;
`;

const LNBWrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const EditorWrapper = styled.div`
  width: 100%;
  height: 100%;
`;
