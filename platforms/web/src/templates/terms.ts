export const terms = `
<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><title>뤼튼 서비스 이용약관</title><style>
/* cspell:disable-file */
/* webkit printing magic: print all background colors */
html {
	-webkit-print-color-adjust: exact;
}
* {
	box-sizing: border-box;
	-webkit-print-color-adjust: exact;
}

html,
body {
	margin: 0;
	padding: 0;
}
@media only screen {
	body {
		margin: 2em auto;
		max-width: 900px;
		color: rgb(55, 53, 47);
	}
}

body {
	line-height: 1.5;
	white-space: pre-wrap;
}

a,
a.visited {
	color: inherit;
	text-decoration: underline;
}

.pdf-relative-link-path {
	font-size: 80%;
	color: #444;
}

h1,
h2,
h3 {
	letter-spacing: -0.01em;
	line-height: 1.2;
	font-weight: 600;
	margin-bottom: 0;
}

.page-title {
	font-size: 2.5rem;
	font-weight: 700;
	margin-top: 0;
	margin-bottom: 0.75em;
}

h1 {
	font-size: 1.875rem;
	margin-top: 1.875rem;
}

h2 {
	font-size: 1.5rem;
	margin-top: 1.5rem;
}

h3 {
	font-size: 1.25rem;
	margin-top: 1.25rem;
}

.source {
	border: 1px solid #ddd;
	border-radius: 3px;
	padding: 1.5em;
	word-break: break-all;
}

.callout {
	border-radius: 3px;
	padding: 1rem;
}

figure {
	margin: 1.25em 0;
	page-break-inside: avoid;
}

figcaption {
	opacity: 0.5;
	font-size: 85%;
	margin-top: 0.5em;
}

mark {
	background-color: transparent;
}

.indented {
	padding-left: 1.5em;
}

hr {
	background: transparent;
	display: block;
	width: 100%;
	height: 1px;
	visibility: visible;
	border: none;
	border-bottom: 1px solid rgba(55, 53, 47, 0.09);
}

img {
	max-width: 100%;
}

@media only print {
	img {
		max-height: 100vh;
		max-height: 100dvh;
		object-fit: contain;
	}
}

@page {
	margin: 1in;
}

.collection-content {
	font-size: 0.875rem;
}

.column-list {
	display: flex;
	justify-content: space-between;
}

.column {
	padding: 0 1em;
}

.column:first-child {
	padding-left: 0;
}

.column:last-child {
	padding-right: 0;
}

.table_of_contents-item {
	display: block;
	font-size: 0.875rem;
	line-height: 1.3;
	padding: 0.125rem;
}

.table_of_contents-indent-1 {
	margin-left: 1.5rem;
}

.table_of_contents-indent-2 {
	margin-left: 3rem;
}

.table_of_contents-indent-3 {
	margin-left: 4.5rem;
}

.table_of_contents-link {
	text-decoration: none;
	opacity: 0.7;
	border-bottom: 1px solid rgba(55, 53, 47, 0.18);
}

table,
th,
td {
	border: 1px solid rgba(55, 53, 47, 0.09);
	border-collapse: collapse;
}

table {
	border-left: none;
	border-right: none;
}

th,
td {
	font-weight: normal;
	padding: 0.25em 0.5em;
	line-height: 1.5;
	min-height: 1.5em;
	text-align: left;
}

th {
	color: rgba(55, 53, 47, 0.6);
}

ol,
ul {
	margin: 0;
	margin-block-start: 0.6em;
	margin-block-end: 0.6em;
}

li > ol:first-child,
li > ul:first-child {
	margin-block-start: 0.6em;
}

ul > li {
	list-style: disc;
}

ul.to-do-list {
	text-indent: -1.7em;
}

ul.to-do-list > li {
	list-style: none;
}

.to-do-children-checked {
	text-decoration: line-through;
	opacity: 0.375;
}

ul.toggle > li {
	list-style: none;
}

ul {
	padding-inline-start: 1.7em;
}

ul > li {
	padding-left: 0.1em;
}

ol {
	padding-inline-start: 1.6em;
}

ol > li {
	padding-left: 0.2em;
}

.mono ol {
	padding-inline-start: 2em;
}

.mono ol > li {
	text-indent: -0.4em;
}

.toggle {
	padding-inline-start: 0em;
	list-style-type: none;
}

/* Indent toggle children */
.toggle > li > details {
	padding-left: 1.7em;
}

.toggle > li > details > summary {
	margin-left: -1.1em;
}

.selected-value {
	display: inline-block;
	padding: 0 0.5em;
	background: rgba(206, 205, 202, 0.5);
	border-radius: 3px;
	margin-right: 0.5em;
	margin-top: 0.3em;
	margin-bottom: 0.3em;
	white-space: nowrap;
}

.collection-title {
	display: inline-block;
	margin-right: 1em;
}

.simple-table {
	margin-top: 1em;
	font-size: 0.875rem;
	empty-cells: show;
}
.simple-table td {
	height: 29px;
	min-width: 120px;
}

.simple-table th {
	height: 29px;
	min-width: 120px;
}

.simple-table-header-color {
	background: rgb(247, 246, 243);
	color: black;
}
.simple-table-header {
	font-weight: 500;
}

time {
	opacity: 0.5;
}

.icon {
	display: inline-block;
	max-width: 1.2em;
	max-height: 1.2em;
	text-decoration: none;
	vertical-align: text-bottom;
	margin-right: 0.5em;
}

img.icon {
	border-radius: 3px;
}

.user-icon {
	width: 1.5em;
	height: 1.5em;
	border-radius: 100%;
	margin-right: 0.5rem;
}

.user-icon-inner {
	font-size: 0.8em;
}

.text-icon {
	border: 1px solid #000;
	text-align: center;
}

.page-cover-image {
	display: block;
	object-fit: cover;
	width: 100%;
	max-height: 30vh;
}

.page-header-icon {
	font-size: 3rem;
	margin-bottom: 1rem;
}

.page-header-icon-with-cover {
	margin-top: -0.72em;
	margin-left: 0.07em;
}

.page-header-icon img {
	border-radius: 3px;
}

.link-to-page {
	margin: 1em 0;
	padding: 0;
	border: none;
	font-weight: 500;
}

p > .user {
	opacity: 0.5;
}

td > .user,
td > time {
	white-space: nowrap;
}

input[type="checkbox"] {
	transform: scale(1.5);
	margin-right: 0.6em;
	vertical-align: middle;
}

p {
	margin-top: 0.5em;
	margin-bottom: 0.5em;
}

.image {
	border: none;
	margin: 1.5em 0;
	padding: 0;
	border-radius: 0;
	text-align: center;
}

.code,
code {
	background: rgba(135, 131, 120, 0.15);
	border-radius: 3px;
	padding: 0.2em 0.4em;
	border-radius: 3px;
	font-size: 85%;
	tab-size: 2;
}

code {
	color: #eb5757;
}

.code {
	padding: 1.5em 1em;
}

.code-wrap {
	white-space: pre-wrap;
	word-break: break-all;
}

.code > code {
	background: none;
	padding: 0;
	font-size: 100%;
	color: inherit;
}

blockquote {
	font-size: 1.25em;
	margin: 1em 0;
	padding-left: 1em;
	border-left: 3px solid rgb(55, 53, 47);
}

.bookmark {
	text-decoration: none;
	max-height: 8em;
	padding: 0;
	display: flex;
	width: 100%;
	align-items: stretch;
}

.bookmark-title {
	font-size: 0.85em;
	overflow: hidden;
	text-overflow: ellipsis;
	height: 1.75em;
	white-space: nowrap;
}

.bookmark-text {
	display: flex;
	flex-direction: column;
}

.bookmark-info {
	flex: 4 1 180px;
	padding: 12px 14px 14px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
}

.bookmark-image {
	width: 33%;
	flex: 1 1 180px;
	display: block;
	position: relative;
	object-fit: cover;
	border-radius: 1px;
}

.bookmark-description {
	color: rgba(55, 53, 47, 0.6);
	font-size: 0.75em;
	overflow: hidden;
	max-height: 4.5em;
	word-break: break-word;
}

.bookmark-href {
	font-size: 0.75em;
	margin-top: 0.25em;
}

.sans { font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol"; }
.code { font-family: "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace; }
.serif { font-family: Lyon-Text, Georgia, ui-serif, serif; }
.mono { font-family: iawriter-mono, Nitti, Menlo, Courier, monospace; }
.pdf .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK JP'; }
.pdf:lang(zh-CN) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK SC'; }
.pdf:lang(zh-TW) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK TC'; }
.pdf:lang(ko-KR) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK KR'; }
.pdf .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK JP'; }
.pdf:lang(zh-CN) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK SC'; }
.pdf:lang(zh-TW) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK TC'; }
.pdf:lang(ko-KR) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK KR'; }
.pdf .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK JP'; }
.pdf:lang(zh-CN) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK SC'; }
.pdf:lang(zh-TW) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK TC'; }
.pdf:lang(ko-KR) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK KR'; }
.pdf .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK JP'; }
.pdf:lang(zh-CN) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK SC'; }
.pdf:lang(zh-TW) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK TC'; }
.pdf:lang(ko-KR) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK KR'; }
.highlight-default {
	color: rgba(55, 53, 47, 1);
}
.highlight-gray {
	color: rgba(120, 119, 116, 1);
	fill: rgba(120, 119, 116, 1);
}
.highlight-brown {
	color: rgba(159, 107, 83, 1);
	fill: rgba(159, 107, 83, 1);
}
.highlight-orange {
	color: rgba(217, 115, 13, 1);
	fill: rgba(217, 115, 13, 1);
}
.highlight-yellow {
	color: rgba(203, 145, 47, 1);
	fill: rgba(203, 145, 47, 1);
}
.highlight-teal {
	color: rgba(68, 131, 97, 1);
	fill: rgba(68, 131, 97, 1);
}
.highlight-blue {
	color: rgba(51, 126, 169, 1);
	fill: rgba(51, 126, 169, 1);
}
.highlight-purple {
	color: rgba(144, 101, 176, 1);
	fill: rgba(144, 101, 176, 1);
}
.highlight-pink {
	color: rgba(193, 76, 138, 1);
	fill: rgba(193, 76, 138, 1);
}
.highlight-red {
	color: rgba(212, 76, 71, 1);
	fill: rgba(212, 76, 71, 1);
}
.highlight-gray_background {
	background: rgba(241, 241, 239, 1);
}
.highlight-brown_background {
	background: rgba(244, 238, 238, 1);
}
.highlight-orange_background {
	background: rgba(251, 236, 221, 1);
}
.highlight-yellow_background {
	background: rgba(251, 243, 219, 1);
}
.highlight-teal_background {
	background: rgba(237, 243, 236, 1);
}
.highlight-blue_background {
	background: rgba(231, 243, 248, 1);
}
.highlight-purple_background {
	background: rgba(244, 240, 247, 0.8);
}
.highlight-pink_background {
	background: rgba(249, 238, 243, 0.8);
}
.highlight-red_background {
	background: rgba(253, 235, 236, 1);
}
.block-color-default {
	color: inherit;
	fill: inherit;
}
.block-color-gray {
	color: rgba(120, 119, 116, 1);
	fill: rgba(120, 119, 116, 1);
}
.block-color-brown {
	color: rgba(159, 107, 83, 1);
	fill: rgba(159, 107, 83, 1);
}
.block-color-orange {
	color: rgba(217, 115, 13, 1);
	fill: rgba(217, 115, 13, 1);
}
.block-color-yellow {
	color: rgba(203, 145, 47, 1);
	fill: rgba(203, 145, 47, 1);
}
.block-color-teal {
	color: rgba(68, 131, 97, 1);
	fill: rgba(68, 131, 97, 1);
}
.block-color-blue {
	color: rgba(51, 126, 169, 1);
	fill: rgba(51, 126, 169, 1);
}
.block-color-purple {
	color: rgba(144, 101, 176, 1);
	fill: rgba(144, 101, 176, 1);
}
.block-color-pink {
	color: rgba(193, 76, 138, 1);
	fill: rgba(193, 76, 138, 1);
}
.block-color-red {
	color: rgba(212, 76, 71, 1);
	fill: rgba(212, 76, 71, 1);
}
.block-color-gray_background {
	background: rgba(241, 241, 239, 1);
}
.block-color-brown_background {
	background: rgba(244, 238, 238, 1);
}
.block-color-orange_background {
	background: rgba(251, 236, 221, 1);
}
.block-color-yellow_background {
	background: rgba(251, 243, 219, 1);
}
.block-color-teal_background {
	background: rgba(237, 243, 236, 1);
}
.block-color-blue_background {
	background: rgba(231, 243, 248, 1);
}
.block-color-purple_background {
	background: rgba(244, 240, 247, 0.8);
}
.block-color-pink_background {
	background: rgba(249, 238, 243, 0.8);
}
.block-color-red_background {
	background: rgba(253, 235, 236, 1);
}
.select-value-color-pink { background-color: rgba(245, 224, 233, 1); }
.select-value-color-purple { background-color: rgba(232, 222, 238, 1); }
.select-value-color-green { background-color: rgba(219, 237, 219, 1); }
.select-value-color-gray { background-color: rgba(227, 226, 224, 1); }
.select-value-color-opaquegray { background-color: rgba(255, 255, 255, 0.0375); }
.select-value-color-orange { background-color: rgba(250, 222, 201, 1); }
.select-value-color-brown { background-color: rgba(238, 224, 218, 1); }
.select-value-color-red { background-color: rgba(255, 226, 221, 1); }
.select-value-color-yellow { background-color: rgba(253, 236, 200, 1); }
.select-value-color-blue { background-color: rgba(211, 229, 239, 1); }

.checkbox {
	display: inline-flex;
	vertical-align: text-bottom;
	width: 16;
	height: 16;
	background-size: 16px;
	margin-left: 2px;
	margin-right: 5px;
}

.checkbox-on {
	background-image: url("data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2216%22%20height%3D%2216%22%20viewBox%3D%220%200%2016%2016%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Crect%20width%3D%2216%22%20height%3D%2216%22%20fill%3D%22%2358A9D7%22%2F%3E%0A%3Cpath%20d%3D%22M6.71429%2012.2852L14%204.9995L12.7143%203.71436L6.71429%209.71378L3.28571%206.2831L2%207.57092L6.71429%2012.2852Z%22%20fill%3D%22white%22%2F%3E%0A%3C%2Fsvg%3E");
}

.checkbox-off {
	background-image: url("data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2216%22%20height%3D%2216%22%20viewBox%3D%220%200%2016%2016%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Crect%20x%3D%220.75%22%20y%3D%220.75%22%20width%3D%2214.5%22%20height%3D%2214.5%22%20fill%3D%22white%22%20stroke%3D%22%2336352F%22%20stroke-width%3D%221.5%22%2F%3E%0A%3C%2Fsvg%3E");
}
	
</style></head><body><article id="2f4bb60f-4356-4a3d-ad62-b2770a52fcf3" class="page sans"><header><h1 class="page-title">뤼튼 서비스 이용약관</h1></header><div class="page-body"><h2 id="563eb0f3-1307-45de-a201-1548bc682d20" class=""><strong>제1장 총칙</strong></h2><h3 id="96a75799-3f5a-4b04-8cb7-fa2284666345" class=""><strong>제1조 (목적)</strong></h3><p id="7ff33dbb-d0a0-4220-8499-4aba0b4f5316" class="">이 약관은 주식회사 뤼튼테크놀로지스(이하 ‘회사&#x27;)가 운영하는 뤼튼(wrtn) 서비스 및 이에 부수하는 제반 서비스(통칭하여 이하 ‘서비스’)의 이용조건 및 절차에 관한 사항 및 기타 필요한 사항을 규정함을 목적으로 합니다.</p><p id="9dafb5e3-5541-46eb-a9eb-0d10d3189e15" class="">
</p><h3 id="4ae51af0-3dc2-42b8-bbff-dddc38679959" class=""><strong>제2조 (용어의 정의)</strong></h3><p id="d84df450-65bb-4769-bba1-9d0092976fd1" class="">이 약관에서 사용하는 용어는 다음과 같이 정의합니다.</p><ol type="1" id="faa1ad4d-b375-4e47-bb9f-0af00ec57550" class="numbered-list" start="1"><li>웹사이트: 회사가 서비스를 제공하기 위해 운영하는 웹사이트 &#x27;https://wrtn.ai’</li></ol><ol type="1" id="d054ca32-b7b7-4982-9958-da1ee0dc2c47" class="numbered-list" start="2"><li>회원: 이 약관에 따라 이용계약을 체결하고, 회사가 제공하는 서비스를 이용하는 자</li></ol><ol type="1" id="a4bac173-608c-4025-bd4a-b4fac07b176e" class="numbered-list" start="3"><li>아이디(ID): 회원식별과 회원의 서비스 이용을 위해 회원이 선정하고 회사가 승인하는 문자와 숫자의 조합</li></ol><ol type="1" id="067c7580-803a-49a4-991f-39afb5a1528c" class="numbered-list" start="4"><li>비밀번호(Password): 회원이 통신상의 자신의 비밀을 보호하기 위해 선정한 문자와 숫자의 조합</li></ol><ol type="1" id="da608427-9c21-44a4-b8ca-1166df1b4763" class="numbered-list" start="5"><li>콘텐츠: 회사가 회원에게 제공하는 부호ㆍ문자ㆍ음성ㆍ음향ㆍ화상ㆍ영상ㆍ도형ㆍ색채ㆍ이미지 등(이들의 복합체도 포함)을 말하며, 회원이 서비스가 제공하는 기능을 사용하여, 서비스상 작성된 결과물을 포함함</li></ol><ol type="1" id="b6e7b6a2-cea6-4a48-89ca-018757770775" class="numbered-list" start="6"><li>유료서비스: 서비스 내에서 제공하는 유료 기능</li></ol><p id="2f5987d8-8a76-4085-bdf9-a059461ab8b5" class="">
</p><h3 id="a3e20d8c-c5ba-46d3-9c05-73720f1d9460" class=""><strong>제3조 (약관의 공시 및 효력과 변경 등)</strong></h3><ol type="1" id="d9cbcd7b-e2de-4390-b645-881e11262e37" class="numbered-list" start="1"><li>회사는 이 약관의 내용을 회원이 쉽게 알 수 있도록 서비스의 초기 서비스화면(전면)에 게시합니다. 다만, 약관의 내용은 회원이 연결화면을 통하여 볼 수 있도록 할 수 있습니다.</li></ol><ol type="1" id="c663812b-3992-4463-9b13-8bea6cfc65af" class="numbered-list" start="2"><li>회사는 「전자상거래 등에서의 소비자보호에 관한 법률」, 「약관의 규제에 관한 법률」, 「전자문서 및 전자거래기본법」, 「전자금융거래법」, 「전자서명법」, 「정보통신망 이용촉진 및 정보보호 등에 관한 법률」, 「방문판매 등에 관한 법률」, 「소비자기본법」 등 관련 법을 위배하지 않는 범위에서 이 약관을 개정할 수 있습니다.</li></ol><ol type="1" id="8df893d0-a02b-41dc-b50e-639a53ac5e97" class="numbered-list" start="3"><li>회사가 이 약관을 개정할 경우에는 적용일자 및 개정사유를 명시하여 현행약관과 함께 회사의 초기화면에 그 적용일자 7일 이전부터 적용일자 전일까지 공지합니다. 다만, 회원에게 불리하게 약관내용을 변경하는 경우에는 최소한 30일 이상의 사전 유예기간을 두고 공지합니다.</li></ol><ol type="1" id="fd380484-193f-493a-ba56-d2e68d56ced9" class="numbered-list" start="4"><li>제3항에 따라 공지된 적용일자까지 회원이 명시적으로 계약해지의 의사를 표명하지 않을 경우에는 개정된 약관에 동의하는 것으로 봅니다.</li></ol><ol type="1" id="a7b605af-5fc2-4974-b602-f61fcfeb0116" class="numbered-list" start="5"><li>이 약관에서 정하지 아니한 사항과 이 약관의 해석에 관하여는 전자상거래 등에서의 소비자보호에 관한 법률, 약관의 규제에 관한 법률, 기타 관계법령 또는 상관례에 따릅니다.</li></ol><ol type="1" id="f4347364-7a67-4eff-b692-da3afacb703a" class="numbered-list" start="6"><li>회사는 서비스 이용과 관련하여 범죄를 예방하고 사회윤리를 존중하는 것을 내용으로 하는 회사와 회원이 준수해야 하는 윤리가이드를 서비스 내에 게시할 수 있으며, 해당 윤리가이드는 이 약관의 내용을 이룹니다. 회원은 이 약관에 동의함으로써 윤리가이드의 준수에 동의한 것으로 봅니다.</li></ol><p id="7096fbc1-9ff7-4210-bd59-99891a133d96" class="">
</p><h2 id="cd23e31d-abbd-49d4-8cd2-c510ce5b8a80" class=""><strong>제2장 이용계약</strong></h2><h3 id="b03167dc-6b38-42a0-8bb9-8418c0b8ed89" class=""><strong>제4조 (회원가입 및 관리)</strong></h3><ol type="1" id="377aec49-ce85-45a5-a3be-ca9f272ce196" class="numbered-list" start="1"><li>이용계약은 이용신청자가 회원가입 안내에서 이 약관의 내용에 대하여 동의하고 등록절차(회사의 소정 양식의 가입 신청서 작성)를 통해 서비스 이용 신청을 하고, 회사가 그 신청에 대해서 승낙함으로써 체결됩니다.</li></ol><ol type="1" id="90476fd2-9f83-494f-a16b-501785fe03f2" class="numbered-list" start="2"><li>이용신청자는 반드시 실명과 실제 정보를 사용해야 하며 한 회원은 오직 1건의 이용신청을 할 수 있습니다.</li></ol><ol type="1" id="b3e39499-cc93-46e4-9825-4a76e6b49d7f" class="numbered-list" start="3"><li>실명이나 실제 정보를 입력하지 않은 회원은 법적인 보호를 받을 수 없으며, 서비스 이용이 불가합니다.</li></ol><ol type="1" id="9b1ac8e4-1003-41da-b8ec-3127b8ca1a2c" class="numbered-list" start="4"><li>회원은 회원가입 시 기재한 사항이 변경되었을 경우 온라인으로 수정을 하거나 전자우편 기타 방법으로 회사에 그 변경사항을 알려야 합니다.</li></ol><ol type="1" id="cadb30a1-1c13-46cc-9351-7e8c1186130f" class="numbered-list" start="5"><li>제4항의 변경사항을 회사에 알리지 않아 발생한 불이익에 대하여 회사는 책임지지 않습니다.</li></ol><p id="949ac178-986b-4ca5-8c97-d060c6cddef2" class="">
</p><h3 id="11e0b017-c403-46ed-827a-f10d987646da" class=""><strong>제5조 (이용신청의 승낙)</strong></h3><ol type="1" id="808a45c2-f9a2-4a2c-96fd-a7622d5807cb" class="numbered-list" start="1"><li>회사는 아래 사항에 해당하는 경우에 그 제한사유가 해소될 때까지 승낙을 유보할 수 있습니다.<ol type="a" id="8431ab38-d977-4307-b4fb-448224f327b4" class="numbered-list" start="1"><li>서비스 관련 설비에 여유가 없는 경우</li></ol><ol type="a" id="7af3bf55-a60b-4b5a-a258-3fa6a33d44b1" class="numbered-list" start="2"><li>기술상 또는 업무상 지장이 있는 경우</li></ol><ol type="a" id="6aef9883-378e-4548-b11a-069fc0f50731" class="numbered-list" start="3"><li>기타 회사 사정상 필요하다고 인정되는 경우</li></ol></li></ol><ol type="1" id="1ff758f3-63d9-4f97-ad96-dc0460de9c84" class="numbered-list" start="2"><li>회사는 아래 사항에 해당하는 경우에 승낙을 하지 않을 수 있습니다.<ol type="a" id="594fb1af-7392-49e6-a29b-65bfceb03657" class="numbered-list" start="1"><li>다른 사람의 명의를 사용하여 신청한 경우</li></ol><ol type="a" id="4d16993f-32a6-4ba1-bcdb-f86c883354ef" class="numbered-list" start="2"><li>회원 정보를 허위로 기재하여 신청한 경우</li></ol><ol type="a" id="67d22434-36fe-4061-a2d4-903d68b41c17" class="numbered-list" start="3"><li>사회의 안녕질서 또는 미풍양속을 저해할 목적으로 신청한 경우</li></ol><ol type="a" id="5ac89bbf-0f09-4453-9ce6-156cd4d8babd" class="numbered-list" start="4"><li>서비스 이용권한을 대여 또는 재판매</li></ol><ol type="a" id="ff1cc172-e714-4859-8a5a-5e465ee55c01" class="numbered-list" start="5"><li>이용약관 위반으로 회원 자격을 박탈당하고 1년 이내에 재가입하는 경우</li></ol><ol type="a" id="8d197d2e-491f-45b5-96b3-ffcae86111b8" class="numbered-list" start="6"><li>기타 회사가 정한 이용신청 요건이 미비한 경우</li></ol></li></ol><p id="ffe07367-7a0c-49b5-b1f8-38f9cbfbf2b1" class="">
</p><h2 id="6b89fcd1-e686-4638-9410-c8c7579dd8b9" class=""><strong>제3장 계약 당사자의 의무</strong></h2><h3 id="3dad994c-f959-4ba1-b5bb-bf5f925e1f33" class=""><strong>제6조 (회사의 의무)</strong></h3><ol type="1" id="53ece7d7-5e8e-4d01-a98d-5315dcf5e149" class="numbered-list" start="1"><li>회사는 법령과 이 약관이 금지하거나 공서양속에 반하는 행위를 하지 않으며 이 약관이 정하는 바에 따라 지속적이고, 안정적으로 재화 또는 용역을 제공하는데 최선을 다하여야 합니다.</li></ol><ol type="1" id="2e44226e-0b2c-4a59-808e-7fca7c726300" class="numbered-list" start="2"><li>회사는 회원이 안전하게 서비스를 이용할 수 있도록 개인정보보호를 위해 보안시스템을 갖추어야 하며 개인정보처리방침을 공시하고 준수합니다.</li></ol><ol type="1" id="12eb1c42-7261-457a-8eb1-68541010ca72" class="numbered-list" start="3"><li>회사는 회원으로부터 제기되는 의견이나 불만이 정당하다고 인정될 경우에는 즉시 처리해야 합니다. 단, 즉시 처리가 곤란한 경우에는 회원에게 그 사유와 처리일정을 통보해야 합니다.</li></ol><p id="565e41f9-d246-455d-83cc-c6ace1d6ed82" class="">
</p><h3 id="b7cc4899-0af2-444f-8f52-0a762d04539d" class=""><strong>제7조 (회원의 의무)</strong></h3><ol type="1" id="8b6d9cd9-458e-4d10-9f0d-a877f8cbcdf7" class="numbered-list" start="1"><li>회원은 이 약관 및 회사의 공지사항, 웹사이트 이용안내 등을 숙지하고 준수해야 하며 아래 각 호의 행위를 해서는 안 됩니다.<ol type="a" id="55801b08-f689-464b-a436-650c6beb6dbb" class="numbered-list" start="1"><li>서비스의 신청 또는 변경 시 허위내용의 기재</li></ol><ol type="a" id="03d22b91-05df-4160-90fc-2ffb0c4f3e96" class="numbered-list" start="2"><li>타인의 정보 또는 결제수단의 도용</li></ol><ol type="a" id="fa3aba80-b7b9-4408-9258-16010652b9c6" class="numbered-list" start="3"><li>회사에 게시된 정보의 변경 또는 또는 서비스에 장애를 주는 행위</li></ol><ol type="a" id="99f66156-577e-40a2-ba84-6de3de7c7164" class="numbered-list" start="4"><li>다른 회원의 개인정보 및 계정정보를 무단으로 수집∙저장∙게시∙유포하는 행위</li></ol><ol type="a" id="429ea8a6-2762-43c3-a372-4f4e1cd39034" class="numbered-list" start="5"><li>리버스엔지니어링, 디컴파일, 디스어셈블 및 기타 일체의 가공행위를 통하여 서비스를 복제, 분해 또는 모방 기타 변형하는 행위</li></ol><ol type="a" id="cc2117b4-87d8-4e12-92fe-d5838b29f7db" class="numbered-list" start="6"><li>해킹, 자동 접속 프로그램 등을 사용하는 등 정상적인 용법과 다른 방법으로 서비스를 이용하여 회사의 서버에 부하를 일으켜 회사의 정상적인 서비스를 방해하는 행위</li></ol><ol type="a" id="c46734de-9727-4752-9563-49114c164a66" class="numbered-list" start="7"><li>본인 아닌 제3자에게 계정을 대여, 양도하는 등 접속권한을 부여하는 행위</li></ol><ol type="a" id="747252ba-dcc8-4ce2-8b23-88ac9cea093d" class="numbered-list" start="8"><li>회사가 금지한 정보(컴퓨터 프로그램 등)의 송신 또는 게시</li></ol><ol type="a" id="b6590a4d-f18e-432a-a745-a7ab04d47596" class="numbered-list" start="9"><li>도박 등 사행행위를 하거나 유도하는 행위, 음란⋅저속한 정보를 입력⋅교류⋅게재하거나 음란 사이트를 연결(링크)하는 행위, 수치심⋅혐오감 또는 공포심을 일으키는 말⋅소리⋅글⋅그림⋅사진 또는 영상을 타인에게 전송 또는 유포하는 행위 등 서비스를 불건전하게 이용하는 행위</li></ol><ol type="a" id="a94fc503-d713-4520-8e2b-e3909abb3010" class="numbered-list" start="10"><li>게시판을 도배할 목적에서 동일 또는 유사한 게시글 또는 의미가 없는 게시글을 반복적으로 개시하는 행위</li></ol><ol type="a" id="34a0683b-7a1f-4791-9f88-f0eddc646223" class="numbered-list" start="11"><li>회사의 동의 없이 영리, 영업, 광고, 홍보, 정치활동, 선거운동 등 본래의 용도 이외의 용도로 서비스를 이용하는 행위</li></ol><ol type="a" id="2250db6d-bba5-4ce4-8c72-585251d9e28d" class="numbered-list" start="12"><li>회사의 서비스를 이용하여 얻은 정보를 무단으로 복제∙유통∙조장하거나 상업적으로 이용하는 행위, 알려지거나 알려지지 않은 버그를 악용하여 서비스를 이용하는 행위</li></ol><ol type="a" id="8a011311-d39a-4dc5-9057-0c73b00844b5" class="numbered-list" start="13"><li>타인을 기망하여 이득을 취하는 행위, 회사의 서비스의 이용과 관련하여 타인에게 피해를 입히는 행위</li></ol><ol type="a" id="e88ce2a2-9309-4664-b39d-741d82e9f0c1" class="numbered-list" start="14"><li>회사와 기타 제3자의 저작권 등 지적재산권에 대한 침해</li></ol><ol type="a" id="06e7dba4-3df5-41bd-b20f-d43b65c2dc23" class="numbered-list" start="15"><li>회사 및 기타 제3자의 명예를 손상시키거나 업무를 방해하는 행위</li></ol><ol type="a" id="fa70c2fd-c0dd-4ef6-9f85-5a9f510e8cf0" class="numbered-list" start="16"><li>기타 불법적이거나 부당한 행위</li></ol></li></ol><ol type="1" id="9bb7906c-6ade-4d15-ae7b-7a8179ccef26" class="numbered-list" start="2"><li>회원은 관계법령, 이 약관의 규정, 이용안내 및 서비스와 관련하여 공지한 주의사항, 회사가 통지하는 사항 등을 준수하여야 하며, 기타 회사의 업무에 방해되는 행위를 하여서는 안 됩니다.</li></ol><ol type="1" id="bacc7609-a808-4fa8-94e3-ebd8409a5545" class="numbered-list" start="3"><li>회원은 본인의 아이디, 비밀번호, 결제수단정보 등에 대한 관리책임을 부담하며, 회원의 관리소홀로 인하여 발생한 문제에 대하여 회사는 책임을 지지 않습니다.</li></ol><p id="17fad3ed-aebc-42a2-b2ae-6bc6b7bce7af" class="">
</p><h2 id="a81a4c38-62b0-4ca3-8649-7b90754215bd" class=""><strong>제4장 서비스의 제공 및 이용</strong></h2><h3 id="f7e04546-d56e-4052-a71e-f1586863a156" class=""><strong>제8조 (서비스 이용)</strong></h3><ol type="1" id="78688084-83dd-4c3f-8e11-40616efcc59e" class="numbered-list" start="1"><li>회원은 이 약관의 규정된 사항을 준수해 서비스를 이용합니다.</li></ol><ol type="1" id="70e88aaf-4665-4f5b-a1a9-c3455c97a467" class="numbered-list" start="2"><li>회사가 회원의 이용신청을 승낙한 때부터 회원은 서비스를 이용할 수 있습니다. 단 일부 서비스의 경우에는 지정된 일자 내지 일정 요건 충족 시에 서비스를 이용할 수 있습니다.</li></ol><ol type="1" id="53eedc95-552d-4cdc-bae6-85d7f2cfbaea" class="numbered-list" start="3"><li>서비스의 이용은 연중무휴, 1일 24시간을 원칙으로 합니다. 다만, 회사의 업무상 또는 기술상의 이유로 서비스가 일시 중지될 수 있습니다. 이러한 경우 회사는 사전에 공지하며, 사전에 공지할 수 없는 부득이한 사유가 있는 경우 사후에 이를 공지합니다.</li></ol><ol type="1" id="f377c21b-dc57-4425-b1a5-57b43f7a841e" class="numbered-list" start="4"><li>회사는 회원 별 서비스 이용 가능 기기의 종류, 기기 대 수 등을 설정 또는 변경할 수 있습니다.</li></ol><ol type="1" id="829895d4-a9c7-4510-ace9-984fbcc6fb07" class="numbered-list" start="5"><li>회원은 회사의 서비스에 대하여 아래의 FUP 정책(Fair Use Policy, 공정사용정책)을 따릅니다.<ol type="a" id="4200b54e-23a9-4dc4-9629-4d31fdc38962" class="numbered-list" start="1"><li>FUP (Fair Use Policy, 공정사용정책)이란 무제한 사용을 허락하는 서비스에서 합리적이고 책임 있는 방식으로 서비스를 사용할 것을 약속 받는 정책입니다.</li></ol><ol type="a" id="87e8ea93-867f-4828-8a80-d1ff47149e0d" class="numbered-list" start="2"><li>회원은 직업과 사업 규모에 따라 알맞은 플랜을 선택하여야 하며, 회사는 회원의 비정상적인 API 확장 및 봇 제작, 계정 돌려쓰기 등 악의적 사용을 금지합니다.</li></ol><ol type="a" id="fdc761cf-12ea-460e-af8e-e81dbd757534" class="numbered-list" start="3"><li>비정상적으로 많은 사용량과 계정 로그인 세부 정보를 다른 사용자와 공유하는 것으로 합리적으로 의심되는 활동이 적발되는 경우 사전 통보 없이 계정이 차단 혹은 영구 삭제가 될 수 있습니다. 이에 관련하여 어떠한 환불도 이루어지지 않습니다.</li></ol></li></ol><ol type="1" id="f0980de7-f157-454a-8201-21cd5f31bcee" class="numbered-list" start="6"><li>이 약관에 명시되지 않은 서비스 이용에 관한 사항은 회사가 정해 웹사이트 내에 게시하거나 또는 별도로 공지하는 내용에 따릅니다.</li></ol><p id="b93623e6-41f8-4107-b128-ccfb02ffca59" class="">
</p><h3 id="815fb599-0986-4955-8d02-9fa028b25ab2" class=""><strong>제9조 (유료서비스)</strong></h3><ol type="1" id="7ca37c1d-baef-4c23-9916-07892f5297c3" class="numbered-list" start="1"><li>회사는 회원에게 회사의 서비스의 전부 또는 일부를 유료로 제공할 수 있으며, 유료서비스에 관한 구체적인 사항은 유료서비스 공지사항 또는 별도의 유료서비스 운영정책에서 정한 바에 따릅니다.</li></ol><ol type="1" id="acd455e2-7402-4229-868b-b499a99ccd66" class="numbered-list" start="2"><li>회사는 다음 사항을 해당 유료서비스의 공지사항 또는 별도의 유료서비스 운영정책 등을 통해 회원이 알기 쉽게 표기합니다.</li></ol><ol type="1" id="dab14a3d-b935-4c20-9b34-a962580152f9" class="numbered-list" start="3"><li>유료서비스의 명칭 또는 제호</li></ol><ol type="1" id="0e5e84be-23b6-45b6-b6ca-8f2496fdf1f2" class="numbered-list" start="4"><li>유료서비스의 내용, 이용방법, 이용료, 결제방법 기타 이용조건</li></ol><ol type="1" id="62cb769c-ceef-40ce-a9d5-44bb69903fc0" class="numbered-list" start="5"><li>유료서비스 대금 환불의 조건 및 절차</li></ol><ol type="1" id="3e5e3bd1-9444-4536-83f2-d1da32f84dcb" class="numbered-list" start="6"><li>유료서비스의 중단 및 변경에 관한 사항</li></ol><ol type="1" id="2d3d05d2-b516-4c84-ada1-d6a189212360" class="numbered-list" start="7"><li>유료서비스를 이용하고자 하는 회원은 유료서비스 공지사항 또는 유료서비스 운영정책을 준수하여야 하며, 이 약관의 내용과 유료서비스 공지사항 또는 유료서비스 운영정책의 내용이 상충하는 경우 유료서비스 공지사항 또는 유료서비스 운영정책의 내용이 우선합니다.</li></ol><p id="736d88cc-91df-4af5-b773-3cd82620c4af" class="">
</p><h3 id="6f10e8dd-b6b4-4bca-8e79-f0c8a004b8a2" class=""><strong>제10조 (서비스 제공의 중지)</strong></h3><ol type="1" id="6aeaef79-8fe7-4636-a4b4-eb73cd7344df" class="numbered-list" start="1"><li>회사는 다음 각 호에 해당하는 경우 서비스의 전부 또는 일부의 제공을 중지할 수 있습니다.<ol type="a" id="1eb74b5a-4976-41e5-92d6-afadaf108745" class="numbered-list" start="1"><li>전기통신사업법 상에 규정된 기간통신사업자/인터넷망사업자가 서비스를 중지했을 경우</li></ol><ol type="a" id="7d8656a8-0e1a-4726-97a1-1236c304eb75" class="numbered-list" start="2"><li>정전으로 서비스 제공이 불가능할 경우</li></ol><ol type="a" id="c8319a6c-f4db-4d6f-8afc-ff52593901c7" class="numbered-list" start="3"><li>설비의 이전, 보수 또는 공사로 인해 부득이한 경우</li></ol><ol type="a" id="536f796f-37b2-4dbf-9f7b-d82a5e94ac68" class="numbered-list" start="4"><li>서비스 설비의 장애 또는 서비스 이용의 폭주 등으로 정상적인 서비스 제공이 어려운 경우</li></ol><ol type="a" id="85d6c7da-85e8-468c-be49-07c10a035cdd" class="numbered-list" start="5"><li>전시, 사변, 천재지변 또는 이에 준하는 국가비상사태가 발생하거나 발생할 우려가 있는 경우</li></ol><ol type="a" id="68a2330b-504a-4516-9a8f-689339449018" class="numbered-list" start="6"><li>회사가 서비스 제공에 이용하는 네이버 운영 ‘Clova Studio’ 서버에 문제가 발생하는 경우를 포함하여 외부 제휴업체 측 사유로 정상적인 서비스 제공이 어려운 경우</li></ol></li></ol><ol type="1" id="72b59026-ffa1-4e6d-a57f-f7f0bed71bd8" class="numbered-list" start="2"><li>회사는 서비스의 제공에 필요한 경우 정기점검을 실시할 수 있으며, 정기점검시간은 서비스 제공화면에 공지한 바에 따릅니다.</li></ol><ol type="1" id="08075e52-212f-49dd-b76f-c9a8344b426b" class="numbered-list" start="3"><li>회사는 서비스가 불안정하거나 시급한 개선이 필요한 경우 긴급점검을 실시할 수 있으며, 이 경우 점검 시간을 서비스 내에 공지합니다.</li></ol><ol type="1" id="d627d07e-2a26-4eb7-b531-edc8ca63974c" class="numbered-list" start="4"><li>회사는 상당한 이유 없이 서비스의 제공이 일시적으로 중단됨으로 인하여 회원이 입은 손해에 대하여 배상합니다. 다만, 본 조 제1항 각 호의 사유에 의하여 서비스가 중단된 경우 또는 서비스 중단에 대하여 회사가 고의 또는 과실이 없음을 입증하는 경우에는 그러하지 아니합니다.</li></ol><ol type="1" id="ede92602-b982-48db-bb9e-fb58aa62b669" class="numbered-list" start="5"><li>회사는 영업양도∙분할∙합병 등에 따른 영업의 폐지, 콘텐츠 제공의 계약만료, 당해 서비스의 현저한 수익 악화 등 경영상의 중대한 사유로 인해 서비스를 지속하기 어려운 경우에는 서비스를 중단할 수 있습니다. 이 경우 중단일자 30일 이전까지 중단일자 및 사유를 앱 초기화면 또는 그 연결화면, 회사 홈페이지 등 기타 서비스 제공 화면을 통해 공지하고 회원에게 통지합니다.</li></ol><p id="e6f87254-89bb-4afc-9491-1bfaaf262d6b" class="">
</p><h3 id="83fd28a5-8b7f-4455-8851-ddd6d2271588" class=""><strong>제11조 (회원 탈퇴 및 자격 상실)</strong></h3><ol type="1" id="45794853-6249-466b-a387-50a8a831615e" class="numbered-list" start="1"><li>회원은 회사에 언제든지 이용계약의 해지(탈퇴)를 요청할 수 있으며 회사는 즉시 회원탈퇴를 처리합니다.</li></ol><ol type="1" id="2c4bdbbb-8422-45c4-8e9c-7e1255c2876f" class="numbered-list" start="2"><li>회원이 다음 각호의 사유에 해당하는 경우, 회사는 회원자격을 제한 및 정지시키거나 이용계약을 해지할 수 있습니다.<ol type="a" id="47855043-affc-41e1-913d-e07ff3160f9f" class="numbered-list" start="1"><li>가입신청 시에 허위내용을 등록한 경우</li></ol><ol type="a" id="13f35032-44a2-4ed4-bb9b-8ca015e285c8" class="numbered-list" start="2"><li>회사의 서비스이용대금, 기타 회사의 서비스 이용에 관련하여 회원이 부담하는 채무를 기일에 이행하지 않는 경우</li></ol><ol type="a" id="e77b9dad-54ef-43d7-a858-65e5b53cc503" class="numbered-list" start="3"><li>다른 사람의 서비스 이용을 방해하거나 그 정보를 도용하는 등 전자상거래 질서를 위협하는 경우</li></ol><ol type="a" id="4cdda2d0-93cc-43b6-85a2-ca59338b6e94" class="numbered-list" start="4"><li>서비스를 이용하여 법령 또는 이 약관이 금지하거나 공서양속에 반하는 행위를 하는 경우</li></ol></li></ol><ol type="1" id="782f3e07-f3b8-4258-a076-eb74a0f8788e" class="numbered-list" start="3"><li>회사가 제2항에서 정한 이용제한 조치를 하는 경우에는 다음 각 호의 사항을 회원에게 통지합니다.<ol type="a" id="b79e0227-a243-48a3-ae3f-18af6aaab4d6" class="numbered-list" start="1"><li>이용제한 조치의 사유</li></ol><ol type="a" id="073a7cc3-698c-4e23-a06a-5f0c6ab9c80b" class="numbered-list" start="2"><li>이용제한 조치의 유형 및 기간</li></ol><ol type="a" id="141cecc5-4dcd-4e80-b2e1-764d79fbbd4c" class="numbered-list" start="3"><li>이용제한 조치에 대한 이의신청 방법</li></ol></li></ol><ol type="1" id="4ce69289-75b2-4520-a409-d6373058f7cf" class="numbered-list" start="4"><li>회사는 다음 각 호의 사유에 대한 조사가 완료될 때까지 해당 계정의 서비스 이용을 정지할 수 있습니다.<ol type="a" id="d4a71b6c-5f62-4f7c-af0b-051096a7458a" class="numbered-list" start="1"><li>계정이 해킹 또는 도용당했거나, 범죄에 이용되었다는 정당한 신고가 접수된 경우</li></ol><ol type="a" id="2e19c6fa-8e9a-4191-b916-388877018f80" class="numbered-list" start="2"><li>그 밖에 이에 준하는 사유로 서비스 이용의 잠정조치가 필요한 경우</li></ol></li></ol><ol type="1" id="a53d144b-229d-47a8-915f-b906967711f5" class="numbered-list" start="5"><li>회사가 회원자격을 제한·정지시킨 후, 동일한 행위가 2회 이상 반복되거나 30일 이내에 그 사유가 시정되지 아니하는 경우 회사는 회원자격을 상실시킬 수 있습니다.</li></ol><ol type="1" id="687f0e68-5a36-47c4-bcfe-3fe6a01785b5" class="numbered-list" start="6"><li>회사가 회원자격을 상실시키는 경우에는 회원등록을 말소합니다. 이 경우 회원에게 이를 통지하고, 회원등록 말소 전에 최소한 30일 이상의 기간을 정하여 소명할 기회를 부여합니다.</li></ol><ol type="1" id="8921514d-21e1-4cbb-bf01-e7616b69ba7b" class="numbered-list" start="7"><li>회원이 이 약관을 위반하여 회사가 본 조에 따라 제재 조치를 가한 경우, 회사는 회원에게 서비스를 이용하지 못하게 됨으로 인하여 발생하는 손해를 배상하지 아니하고, 회원의 유료서비스 이용과 관련하여 일체의 환불 또는 보상을 하지 아니합니다.</li></ol><p id="788e90bc-1acf-4252-a551-ecd4a93f2509" class="">
</p><h3 id="d36ade2a-50c5-4005-8ee3-1687adae03bb" class=""><strong>제12조 (정보의 제공)</strong></h3><p id="dff090a5-d8d8-41ed-bb2d-c0089c3ba3ff" class="">회사는 회원이 서비스 이용 중 필요하다고 인정되는 다양한 정보에 대하여 전자메일이나 서신우편 등의 방법으로 회원에게 정보를 제공할 수 있습니다. 단 광고성 정보에 해당하는 경우 회원으로부터 수신 동의를 받습니다</p><p id="715fb7fb-b360-4a9a-acfe-cb90c8778d19" class="">
</p><h3 id="9adff617-1ff5-452e-8d35-de7fc17d6813" class=""><strong>제13조 (광고게재)</strong></h3><ol type="1" id="0a245026-7121-4a76-879f-76089aab7759" class="numbered-list" start="1"><li>회사는 서비스의 운용과 관련하여 서비스 화면, 홈페이지, 전자우편 등에 광고 등을 게재할 수 있습니다.</li></ol><ol type="1" id="40398b8c-472d-40a7-bf6a-371bc4a41969" class="numbered-list" start="2"><li>회사가 제공하는 서비스 중의 배너 또는 링크 등을 통해 타인이 제공하는 광고나 서비스에 연결될 수 있습니다.</li></ol><ol type="1" id="497ea0b1-6f91-4d46-b626-449c250741e4" class="numbered-list" start="3"><li>제 2항에 따라 타인이 제공하는 광고나 서비스에 연결될 경우, 해당 영역에서 제공하는 서비스는 회사의 서비스 영역이 아니므로 회사가 신뢰성, 안정성 등을 보장하지 않으며, 그로 인한 회원의 손해에 대하여도 회사는 책임을 지지 않습니다.</li></ol><p id="0372f568-ed8a-4be2-a02c-5fac6f8150e7" class="">
</p><h3 id="9c4c9c75-0b3d-4aa7-b342-1953d965ef39" class=""><strong>제14조 (게시물 관리)</strong></h3><ol type="1" id="d5b2fe9d-6158-46a0-b964-f2ba3b1e2345" class="numbered-list" start="1"><li>회사는 불법 여부 또는 AI 윤리 규정 위반 여부를 판단하기 위해 콘텐츠를 검토할 수 있으며 회사의 정책 또는 법규를 위반한다고 합리적으로 판단되는 콘텐츠는 삭제하거나 게시를 거부할 수 있습니다.</li></ol><ol type="1" id="aa67d3d1-b135-447b-af14-31476f9170e9" class="numbered-list" start="2"><li>그렇다고 해서 반드시 콘텐츠를 검토한다는 의미는 아니므로 회사에서 콘텐츠를 검토할 것이라고 간주하지 마시기 바랍니다.</li></ol><p id="f59b05b8-d8a3-4efa-9cc3-a84f07b8c54c" class="">
</p><h3 id="2427ed80-600c-42e9-8222-2c4506b31a00" class=""><strong>제15조 (저작권 등의 귀속 등)</strong></h3><ol type="1" id="18f26b2a-9346-4450-84ad-bd79d56c03d0" class="numbered-list" start="1"><li>회원이 콘텐츠와 관련하여 보유하고 있는 모든 지적 재산권의 소유권은 유지됩니다. 즉, 회원이 보유한 콘텐츠는 회원에게 존속됩니다.</li></ol><ol type="1" id="61c23a89-72a0-48e3-a5e0-bb516b817679" class="numbered-list" start="2"><li>회사는 서비스 특성상 회원이 서비스가 제공하는 기능을 사용하여, 서비스상 작성된 결과물이나 결과물 작성과정에 노출된 각종 정보가 타인의 저작권과 기타 지적재산권을 침해하지 않음을 보증하지 않습니다. 회원이 서비스가 제공하는 기능을 사용하여, 서비스상 작성된 결과물의 사용으로 인한 책임은 회원에게 있습니다.</li></ol><ol type="1" id="f20f043a-be6b-410c-a105-abade667a94e" class="numbered-list" start="3"><li>회사는 회원이 서비스 내에 게시한 게시글 및 회원이 서비스가 제공하는 기능을 사용하여, 서비스상 작성된 결과물을 서비스 내 노출, 서비스 홍보를 위한 활용, 서비스 운영, 개선 및 새로운 서비스 개발을 위한 연구 목적을 위하여 저장, 복제, 수정, 공중 송신, 전시, 배포, 2차적 저작물 작성의 방식으로 이용할 수 있습니다.</li></ol><ol type="1" id="04d0574f-bec8-47e4-a04b-72e3f1efd09f" class="numbered-list" start="4"><li>회사는 서비스 이용 약관 및 개인정보처리방침에 설명된 경우를 제외하고는 귀하의 파일 및 데이터를 다른 사람과 공유하지 않습니다.</li></ol><ol type="1" id="de1080c3-236a-4907-a82c-109943f5cada" class="numbered-list" start="5"><li>회원은 자신이 서비스 이용과정에서 입력하는 글, 이미지 기타 자료가 제3자의 지적재산권 기타 권리를 침해하지 않는다는 점을 보증합니다. 회원이 이를 위반하여 제3자와 회원 또는 회사 사이에 소송, 이의제기, 권리주장 기타 일체의 분쟁이 발생하는 경우, 회원은 회사를 면책하고 해당 분쟁을 해결하여야 하며, 이로 인해 회사가 손해를 입은 경우 해당 손해를 배상하여야 합니다.</li></ol><ol type="1" id="bee7889b-4a3a-4a2a-b63c-6a61a0009914" class="numbered-list" start="6"><li>이 조는 회사가 서비스를 운영하는 동안 유효하며, 회원 탈퇴 후에도 지속적으로 적용됩니다.</li></ol><ol type="1" id="d148956b-92ce-468c-8e16-535de444bb25" class="numbered-list" start="7"><li>회사는 저작권법 제103조가 적용되는 경우, 이에 따라 복제 및 전송을 중단할 수 있습니다.</li></ol><p id="df3c2cea-708e-4076-b2b0-2d2c0768e304" class="">
</p><h2 id="facc312e-0e0b-45bf-a231-fce8c16bf3dc" class=""><strong>제5장 (재화의 주문 및 결제 관련)</strong></h2><h3 id="0c90a313-c304-4c06-aa29-b378eda3d2e7" class=""><strong>제16조 (대금결제)</strong></h3><ol type="1" id="ff22fc8b-7b92-4061-91ef-47645473a825" class="numbered-list" start="1"><li>회원은 유료서비스에 대한 “구매(하기)”, “결제(하기)”, “(결제)확인” 등의 버튼을 클릭함으로써 본 이용약관 및 게시된 구매조건에 따라 유료서비스 이용계약이 성립하고, 이용요금이 결제됩니다. 대금의 부과와 납부는 원칙적으로 회원이 선택한 결제수단이 정하는 정책이나 방법에 따릅니다.</li></ol><ol type="1" id="a9e157c5-6d19-4181-a6c1-2c6c9bfa57ae" class="numbered-list" start="2"><li>회원은 유료서비스에 대하여 선불카드, 직불카드, 신용카드 등의 각종 카드 결제 수단을 이용하여 결제할 수 있습니다. 이때 ‘회사’는 회원의 지급방법에 대하여 유료서비스 외 어떠한 명목의 수수료를 추가 징수하지 않습니다.</li></ol><ol type="1" id="f4d52fd5-910a-4cb2-bd64-7f5149d5dc2c" class="numbered-list" start="3"><li>회사가 서비스 내 달리 공지하지 않는 한 회원이 대금을 결제한 때로부터 유료서비스는 개시됩니다.</li></ol><ol type="1" id="9590108c-8977-4361-9cc5-2088d89eb891" class="numbered-list" start="4"><li>회원이 등록한 결제수단을 통하여 1개월을 주기로 이용요금이 자동으로 결제되며, 다음 달 동일한 날짜에 자동으로 결제되고 이용기간이 자동 갱신됩니다. 단, 결제 날짜가 특정 월에 포함되지 않은 날짜로 지정된 경우 해당 월의 마지막 날에 결제됩니다.</li></ol><ol type="1" id="18cf07e3-8e10-411f-b7f2-d067ca38a1e4" class="numbered-list" start="5"><li>회원이 유료서비스 이용 중 상위의 유료서비스(이용요금이 더 많은 유료서비스를 의미)로 변경하는 경우, 변경일자로부터 상위의 유료서비스 이용요금 결제가 이루어지며 이전 유료서비스에 대한 기결제 이용요금은 환불되지 않습니다. 이와 관련한 구체적인 사항은 회사의 유료서비스 공지사항 또는 별도의 유료서비스 운영정책에 따릅니다.</li></ol><ol type="1" id="df1420f4-ec31-42e0-883d-818050b3b3ba" class="numbered-list" start="6"><li>만 19세 미만 미성년자가 유료 서비스 이용계약을 하려면 반드시 이용계약에 관한 법정대리인(부모)의 동의를 받아야 하며, 법정대리인의 이용계약에 관한 동의 의사표시는 서비스에서 정한 필요 서류를 이메일로 서비스의 고객센터에 제출하는 것으로 확인합니다. 만약, 만 19세 미성년자인 회원이 법정대리인의 동의 없이 유료 서비스 이용 계약을 체결하는 경우 본인 또는 법정대리인이 이를 취소할 수 있습니다.</li></ol><p id="4f3bde20-9d36-4fff-afee-51a9812c7fde" class="">
</p><h3 id="9b22ef01-6f34-440d-b168-f9f53d9227db" class=""><strong>제17조 (청약철회 등)</strong></h3><ol type="1" id="5fc02f08-ddda-4430-a3a8-37a9eddc5ff5" class="numbered-list" start="1"><li>회사와 유료서비스의 구매에 관한 계약을 체결한 회원은 해당 유료서비스를 전혀 사용하지 아니하였을 경우에 한하여 결제일과 콘텐츠 이용 가능일 중 늦은 날부터 7일 이내에 청약철회를 할 수 있습니다.</li></ol><ol type="1" id="971d828c-0521-469c-83f2-bd1a8ced64a5" class="numbered-list" start="2"><li>전자상거래 등에서의 소비자보호에 관한 법률 등 관련 법령에서 규정하는 청약철회가 불가능한 서비스의 경우 청약철회권이 제한됩니다. 단, 관련 법령에서 회사에게 청약철회권의 제한을 위한 조치를 취할 것을 요구하는 경우, 회사는 해당 조치를 취합니다.</li></ol><ol type="1" id="37475231-f72a-4764-8062-fffcc7db4bbe" class="numbered-list" start="3"><li>회원은 제1항 및 제2항에도 불구하고 구매한 유료 콘텐츠의 내용이 표시, 광고의 내용과 다르거나 구매계약의 내용과 다르게 이행된 경우에 해당 콘텐츠가 이용 가능하게 된 날부터 3개월 이내, 그 사실을 안 날 또는 알 수 있었던 날부터 30일 이내에 청약철회를 할 수 있습니다.</li></ol><ol type="1" id="df7a479a-2c78-44c3-8dba-14cbeef4aabf" class="numbered-list" start="4"><li>회원이 청약철회를 할 경우 회사는 결제수단의 제공자를 통해 구매내역을 확인합니다. 또한 회사는 회원의 정당한 철회 사유를 확인하기 위해 회원에게서 제공받은 정보를 통해 회원에게 연락하여 추가적인 증빙을 요구할 수 있으며, 이에 대한 확인이 완료될 때까지 거래진행을 중지하거나 확인이 불가한 해당거래를 취소할 수 있습니다.</li></ol><ol type="1" id="56378c2a-06e3-4608-a556-0cba9429da57" class="numbered-list" start="5"><li>기타 본 조에서 정하지 아니한 청약철회 또는 중도해지 등 사유로 인한 유료서비스 이용요금 환불 등에 관한 사항은 회사의 유료서비스 공지사항 또는 별도의 유료서비스 운영정책에 따릅니다.</li></ol><p id="fb4b1653-6c6e-47bf-a6f8-a2fc185b4cbd" class="">
</p><h3 id="ee7f7fda-184c-4b7a-a0d1-5a026da0df10" class=""><strong>제18조 (과오납금의 환급)</strong></h3><ol type="1" id="210ef519-83fc-4904-9ccc-eb889aacc1d8" class="numbered-list" start="1"><li>회사는 과오납금이 발생하는 경우 과오납금을 회원에게 환급합니다. 다만, 과오납금이 회사의 고의 또는 과실 없이 회원의 과실로 인하여 발생한 경우에는 그 환급에 소요되는 실제 비용은 합리적인 범위 내에서 회원이 부담합니다.</li></ol><ol type="1" id="6f3697a7-56d9-478f-9a58-1cf8dd34b068" class="numbered-list" start="2"><li>회사는 회원이 선택한 결제수단에 따라 결제를 취소 또는 해당 결제수단을 통한 환급을 할 수 있습니다.</li></ol><ol type="1" id="233d2ebf-fa8e-4064-a359-b6de9cd26e13" class="numbered-list" start="3"><li>회사는 과오납금의 환급을 처리하기 위해 회원에게서 제공받은 정보를 통해 회원에게 연락할 수 있으며, 필요한 정보의 제공을 요청할 수 있습니다.</li></ol><p id="c77df7ef-b841-4ead-aa34-24e59dc2ced8" class="">
</p><h2 id="878adb59-e806-4a8a-96a1-6e38ccaa1da5" class=""><strong>제6장 (AI 윤리 규정 관련)</strong></h2><h3 id="a0a55092-1b65-46f3-ac70-71476e337431" class=""><strong>제19조 (AI 윤리 규정)</strong></h3><ol type="1" id="cb9cfacb-bab0-45af-a350-afa7f4966e52" class="numbered-list" start="1"><li>회사의 모든 구성원은 AI 윤리를 중요하게 여기며 자체적인 AI 윤리 규정을 준수합니다. AI 윤리 규정은 뤼튼의 서비스를 사용할 때 발생할 수 있는 문제를 예방하고 방지하기 위한 목적으로 제정되었습니다.</li></ol><ol type="1" id="303b1604-fa29-40e5-9487-8776e45ab25a" class="numbered-list" start="2"><li>뤼튼은 사용자에게 친근하고 유용한 AI를 지향하고 있습니다. 그 과정에서 올바른 표현 도출과 개인정보 보호를 우선하고 있으며 잘못된 정보와 편향된 정보가 도출되지 않도록 노력하고 있습니다.</li></ol><p id="93930771-319d-44b4-ab06-c4dca123b060" class="">
</p><h3 id="b8aaad3c-5bb4-4a29-9898-a8a2bab02f86" class=""><strong>제20조 (뤼튼을 사용하는 사용자의 의무)</strong></h3><ol type="1" id="cd3a8714-8831-4e0a-bf1b-c4dfe933173f" class="numbered-list" start="1"><li>회원은 뤼튼을 악의적인 목적으로 사용할 수 없습니다. 또한 회원은 악의적인 사용을 통해 뤼튼테크놀로지스와 뤼튼에 대한 평판 위험을 발생시켜서는 아니됩니다. 악의적 사용이란 뤼튼을 사용하는 과정에서 고의적인 입력값을 통해 부당한 차별적 의견 또는 부당한 가치 판단 등 편향적인 결과값을 발생시키는 것이 대표적이며, 그 밖에도 회원이 악의적으로 뤼튼을 사용하여 발생시키는 문제를 포함합니다.</li></ol><ol type="1" id="0fe50d22-3eaf-4c36-a34b-ef8b7fb8fd42" class="numbered-list" start="2"><li>회원은 서비스 이용 과정에서 발생한 AI 결과값(이하 ‘AI 결과값’)을 SNS 서비스 및 기타 방법에 의해 외부에 공개하며 제3자에게 알리는 경우 출력 데이터에 저작권 침해, 폭력적 표현, 선정적 표현, 차별적 표현, 기타 부적절하거나 제3자에게 기타 권리침해, 불이익, 손해, 불쾌감을 주는 정보가 포함되어 있지 않음을 반드시 확인해야합니다. AI 결과값과 관련하여 법적∙윤리적 문제가 발생하거나, 제3자와 일체의 분쟁이 발생하는 경우 이에 대한 모든 책임은 회원에게 있습니다.</li></ol><ol type="1" id="a3a7e34f-7cf4-4db1-87af-911ffdc2cd80" class="numbered-list" start="3"><li>회원은 뤼튼을 사용하며 발생한 ‘문제가 되거나 문제가 될 수 있다고 판단한 결과물’에 대해서는 발견 즉시 뤼튼테크놀로지스에 해당 내용을 알려야 합니다. 이를 통해 회원은 뤼튼테크놀로지스가 관련 부분을 개선할 수 있도록, 이에 대해 적극적으로 협조할 의무가 있습니다.</li></ol><ol type="1" id="7e859c20-c113-42a9-97c5-5f889eedcae5" class="numbered-list" start="4"><li>본 규정을 준수하지 않은 회원은 제4장에 따라 별도의 공지 없이 이용이 정지되거나 제한될 수 있으며, 회사는 이에 관한 책임을 부담하지 않습니다.</li></ol><ol type="1" id="96d6ea87-d786-4267-9d72-b5af77f921bd" class="numbered-list" start="5"><li>회원이 AI 결과값을 외부에 공개하는 과정에서 회사에 피해를 입힌 경우 그에 대한 법률상 책임을 부담합니다.</li></ol><ol type="1" id="09034759-a39f-482e-b8fa-3d311ebdf928" class="numbered-list" start="6"><li>회원은 입력값과 결과값은 더 나은 서비스 제공을 위한 뤼튼의 엔진 개선에 활용될 수 있으며 모델 평가에 사용될 수 있습니다.</li></ol><p id="74e3e56a-b179-480d-9983-0cd74a2ee4de" class="">
</p><h3 id="cf3173bc-2755-4084-beaf-506fb6923466" class=""><strong>제21조 (AI 윤리 규정의 적용 범위)</strong></h3><p id="415b4222-a884-4ea4-a83a-c29bb7e22519" class="">뤼튼테크놀로지스의 AI 윤리 규정은 지속적으로 개정될 수 있으며, 회사는 이를 공지합니다.</p><p id="6c48f4fa-4767-41a8-8809-972addef8294" class="">
</p><h2 id="77677c6e-1c92-4d9d-8608-4f3e840aafdf" class=""><strong>제7장 기타</strong></h2><h3 id="6f758ace-5c0e-4613-990c-9072c49874cd" class=""><strong>제22조 (면책 및 손해배상)</strong></h3><ol type="1" id="cf7f69dc-e34b-4895-8801-b7ea875d9ddd" class="numbered-list" start="1"><li>회사 또는 회원은 본 약관을 위반하여 상대방에게 손해를 입힌 경우에는 그 손해를 배상할 책임이 있습니다. 다만, 고의 또는 과실이 없는 경우에는 그러하지 아니 합니다.</li></ol><ol type="1" id="da11dfa6-1723-40a2-a89b-79e3496e4f3e" class="numbered-list" start="2"><li>회사는 아래 각 호의 사유로 인하여 회원에게 발생한 손해에 대하여 책임을 지지 않습니다. 다만, 회사의 고의 또는 과실에 의한 경우에는 그러하지 아니합니다.<ol type="a" id="45eedb64-45fc-4450-879a-c813f58117d2" class="numbered-list" start="1"><li>서비스용 설비의 보수, 교체, 정기점검, 공사 등 기타 이에 준하는 사유로 서비스 이용이 불가한 경우(다만 회사의 고의 또는 과실에 의한 경우는 제외)</li></ol><ol type="a" id="c888a49f-c017-4356-bb31-6075f53c25a8" class="numbered-list" start="2"><li>회원의 고의 또는 과실로 인한 서비스 이용의 장애</li></ol><ol type="a" id="835e0ce6-a876-42c0-b4d8-983b0445c48b" class="numbered-list" start="3"><li>다른 회원이 게시한 게시글에 신뢰성·정확성이 부족한 경우</li></ol><ol type="a" id="6dc20fcc-03c7-4e9e-bcfb-010b3782304a" class="numbered-list" start="4"><li>회원간 또는 회원과 타인간 서비스를 매개로 발생한 거래나 분쟁</li></ol><ol type="a" id="704052d8-fdb2-4be6-9cbe-f7bc7b8488a1" class="numbered-list" start="5"><li>무료로 제공되는 서비스 이용</li></ol><ol type="a" id="f9d46bd0-b54b-4fda-bf30-d9c37185b06f" class="numbered-list" start="6"><li>회원이 서비스를 이용을 통하여 기대하는 유익이 실현되지 않은 경우</li></ol><ol type="a" id="9c04caf8-1d29-4634-bcd3-5cda7082de02" class="numbered-list" start="7"><li>회원이 계정 비밀번호, 모바일 기기 비밀번호, 오픈마켓 사업자가 제공하는 비밀번호 등을 관리하지 않아 회원정보의 유출이 발생한 경우(다만 회사의 고의 또는 중과실에 의한 경우는 제외)</li></ol><ol type="a" id="3cce6405-57d6-4355-be56-593ddef36c09" class="numbered-list" start="8"><li>회원이 모바일 기기의 변경, 모바일 기기의 번호 변경, 운영체제(OS) 버전의 변경, 해외 로밍, 통신사 변경 등으로 인해 콘텐츠 전부나 일부의 기능을 이용할 수 없는 경우(다만 회사의 고의 또는 중과실에 의한 경우는 제외)</li></ol><ol type="a" id="bbe69de3-4696-4f28-b623-4566b112e9f2" class="numbered-list" start="9"><li>회원이 회사가 제공하는 콘텐츠나 계정정보를 삭제한 경우(다만 회사의 고의 또는 중과실에 의한 경우는 제외)</li></ol><ol type="a" id="49b61af9-7842-4812-8091-7ebf8924757b" class="numbered-list" start="10"><li>서버에 대한 제3자의 불법적인 접속 또는 서버의 불법적인 이용 및 프로그램을 이용한 비정상적인 접속 방해(다만 회사의 고의 또는 중과실에 의한 경우는 제외)</li></ol><ol type="a" id="e5602841-7376-4d71-a56f-dd278aa7ed51" class="numbered-list" start="11"><li>기타 회사의 귀책사유가 인정되지 않는 사유로써 위 각 호에 준하는 사유</li></ol></li></ol><ol type="1" id="ecc2a0ad-689b-4a74-8ba2-a7db35cc8c70" class="numbered-list" start="3"><li>회사는 AI 결과값의 적법성, 독창성, 배타성, 신뢰도, 정확성, 진실성, 활용 가능성, 특정 목적에의 적합성을 보장하지 아니합니다. 회사는 회원이 서비스를 이용하여 기대하는 수익을 얻지 못한 것에 대하여 책임을 지지 않으며, 그 밖의 서비스를 통하여 얻은 결과물로 인한 손해에 관하여 책임을 지지 않습니다.</li></ol><ol type="1" id="a4a2ae70-3464-4259-aadd-8a472a7117e3" class="numbered-list" start="4"><li>AI 결과값의 이용과 관련하여 발생하는 모든 문제에 대한 책임은 회원에게 있으며, 회사는 이에 대하여 일체의 책임을 지지 아니합니다. </li></ol><ol type="1" id="d2dbbd52-94f5-408c-b5c4-6fdfed7067e8" class="numbered-list" start="5"><li>회사는 회원이 게시판에 게재한 정보, 자료, 내용 등에 관하여 사실의 정확성, 신뢰도 등에 어떠한 책임도 부담하지 않으며 회원은 본인의 책임 아래 서비스를 이용해야 합니다.</li></ol><ol type="1" id="f8f2354d-eaff-469f-9be6-90e485a892e6" class="numbered-list" start="6"><li>회사는 회원이 서비스와 관련하여 게재하였거나, 서비스 내에서 검색 또는 추천된 정보자료, 사실의 신뢰도, 정확성 등의 내용에 관하여는 책임을 지지 않습니다.</li></ol><ol type="1" id="8254f72e-f6ef-48ae-b8f2-07f6f885aba6" class="numbered-list" start="7"><li>회사는 회원이 서비스 이용과 관련하여 회원에게 발생한 손해 중 회원의 고의, 과실에 의한 손해 및 타 회원으로 인해 입게 되는 정신적 피해에 대하여 보상할 책임을 지지 않습니다.</li></ol><ol type="1" id="a3ccff7c-310f-4ebd-8334-3c6f177a5d2b" class="numbered-list" start="8"><li>회원이 게시 또는 전송한 자료 등에 관하여 손해가 발생하거나 자료의 취사선택과 관련해 어떠한 불이익이 발생하더라도 이에 대한 모든 책임은 회원에게 있습니다.</li></ol><ol type="1" id="27463351-8a68-4b60-b1b7-138c65ee3cf6" class="numbered-list" start="9"><li>회원이 이 약관의 규정을 위반함으로써 회사에 손해가 발생하는 경우 이 약관을 위반한 회원은 회사에 발생한 모든 손해를 배상해야 하며 동 손해로부터 회사를 면책시켜야 합니다.</li></ol><p id="d904bacd-455d-4c8f-b240-0e4ec33d4fe5" class="">
</p><h3 id="250a6f2c-4967-4225-b2d8-7aa3e9af7765" class=""><strong>제23조 (회원에 대한 통지)</strong></h3><ol type="1" id="c57b8ab5-b349-47af-8a1f-14066c2ac7a9" class="numbered-list" start="1"><li>회사가 회원에 대한 통지를 하는 경우, 회원의 전자우편 주소, 휴대전화번호, 문자메시지(SMS/MMS) 등으로 할 수 있습니다.</li></ol><ol type="1" id="dd4b0445-0251-4a35-ad56-f31bc055065c" class="numbered-list" start="2"><li>회사가 불특정다수 회원에 대한 통지의 경우 7일이상 회사 홈페이지 내에 게시함으로써 개별 통지에 갈음할 수 있습니다. 다만, 회원 본인의 거래와 관련하여 중대한 영향을 미치는 사항에 대하여는 개별통지를 합니다.</li></ol><p id="96364eaf-d44c-4e27-b3f9-8697d6fc5ec8" class="">
</p><h3 id="c24c8fcc-785c-4a86-8dea-2c9d89e5c6d6" class=""><strong>제24조 (재판권 및 준거법)</strong></h3><ol type="1" id="4dcbc0f9-98c5-42ce-9609-3f55d9f1ff68" class="numbered-list" start="1"><li>이 약관은 대한민국 법률에 따라 규율되고 해석됩니다.</li></ol><ol type="1" id="42e37b14-331e-4fe7-bf8b-1fd36b0ccfd0" class="numbered-list" start="2"><li>회사와 회원은 본 서비스 이용과 관련해 발생한 분쟁을 원만하게 해결하기 위하여 필요한 모든 노력을 해야 합니다.</li></ol><ol type="1" id="6fa35c98-42a6-47b8-9933-010630a171eb" class="numbered-list" start="3"><li>회사와 회원 간에 발생한 분쟁으로 소송이 제기되는 경우에는 민사소송법에 따라 관할권을 가지는 법원을 관할 법원으로 합니다.</li></ol><p id="40f8d3e0-6950-4871-a9a4-91e1f686dc7d" class="">
</p><p id="397ca001-df8c-4189-9ed8-8c8060546d86" class=""><strong>[부칙]</strong></p><p id="9ea2e430-959e-4535-bce5-db0ef8729c51" class=""><strong>이 개정 약관은 2023년 1월 18일(수)부터 적용됩니다.</strong></p><p id="27d215d9-db94-4ce2-bb31-ca0a61515c6a" class=""><strong>뤼튼 서비스 이용 약관은 2022년 4월 1일(금)부터 적용됩니다.</strong></p><p id="833fc175-3230-4eca-8f54-f83b755b08f9" class="">
</p><p id="bf41f899-026c-4087-a1c7-324adf8d8477" class="">본 약관에 대한 저작권은 주식회사 뤼튼테크놀로지스에 있으며 무단 복제, 배포, 전송, 기타 저작권 침해행위를 엄금 합니다.</p></div></article></body></html>
`;
