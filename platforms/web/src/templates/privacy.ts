export const privacy = `
<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><title>뤼튼 개인정보 처리방침</title><style>
/* cspell:disable-file */
/* webkit printing magic: print all background colors */
html {
	-webkit-print-color-adjust: exact;
}
* {
	box-sizing: border-box;
	-webkit-print-color-adjust: exact;
}

html,
body {
	margin: 0;
	padding: 0;
}
@media only screen {
	body {
		margin: 2em auto;
		max-width: 900px;
		color: rgb(55, 53, 47);
	}
}

body {
	line-height: 1.5;
	white-space: pre-wrap;
}

a,
a.visited {
	color: inherit;
	text-decoration: underline;
}

.pdf-relative-link-path {
	font-size: 80%;
	color: #444;
}

h1,
h2,
h3 {
	letter-spacing: -0.01em;
	line-height: 1.2;
	font-weight: 600;
	margin-bottom: 0;
}

.page-title {
	font-size: 2.5rem;
	font-weight: 700;
	margin-top: 0;
	margin-bottom: 0.75em;
}

h1 {
	font-size: 1.875rem;
	margin-top: 1.875rem;
}

h2 {
	font-size: 1.5rem;
	margin-top: 1.5rem;
}

h3 {
	font-size: 1.25rem;
	margin-top: 1.25rem;
}

.source {
	border: 1px solid #ddd;
	border-radius: 3px;
	padding: 1.5em;
	word-break: break-all;
}

.callout {
	border-radius: 3px;
	padding: 1rem;
}

figure {
	margin: 1.25em 0;
	page-break-inside: avoid;
}

figcaption {
	opacity: 0.5;
	font-size: 85%;
	margin-top: 0.5em;
}

mark {
	background-color: transparent;
}

.indented {
	padding-left: 1.5em;
}

hr {
	background: transparent;
	display: block;
	width: 100%;
	height: 1px;
	visibility: visible;
	border: none;
	border-bottom: 1px solid rgba(55, 53, 47, 0.09);
}

img {
	max-width: 100%;
}

@media only print {
	img {
		max-height: 100vh;
		max-height: 100dvh;
		object-fit: contain;
	}
}

@page {
	margin: 1in;
}

.collection-content {
	font-size: 0.875rem;
}

.column-list {
	display: flex;
	justify-content: space-between;
}

.column {
	padding: 0 1em;
}

.column:first-child {
	padding-left: 0;
}

.column:last-child {
	padding-right: 0;
}

.table_of_contents-item {
	display: block;
	font-size: 0.875rem;
	line-height: 1.3;
	padding: 0.125rem;
}

.table_of_contents-indent-1 {
	margin-left: 1.5rem;
}

.table_of_contents-indent-2 {
	margin-left: 3rem;
}

.table_of_contents-indent-3 {
	margin-left: 4.5rem;
}

.table_of_contents-link {
	text-decoration: none;
	opacity: 0.7;
	border-bottom: 1px solid rgba(55, 53, 47, 0.18);
}

table,
th,
td {
	border: 1px solid rgba(55, 53, 47, 0.09);
	border-collapse: collapse;
}

table {
	border-left: none;
	border-right: none;
}

th,
td {
	font-weight: normal;
	padding: 0.25em 0.5em;
	line-height: 1.5;
	min-height: 1.5em;
	text-align: left;
}

th {
	color: rgba(55, 53, 47, 0.6);
}

ol,
ul {
	margin: 0;
	margin-block-start: 0.6em;
	margin-block-end: 0.6em;
}

li > ol:first-child,
li > ul:first-child {
	margin-block-start: 0.6em;
}

ul > li {
	list-style: disc;
}

ul.to-do-list {
	text-indent: -1.7em;
}

ul.to-do-list > li {
	list-style: none;
}

.to-do-children-checked {
	text-decoration: line-through;
	opacity: 0.375;
}

ul.toggle > li {
	list-style: none;
}

ul {
	padding-inline-start: 1.7em;
}

ul > li {
	padding-left: 0.1em;
}

ol {
	padding-inline-start: 1.6em;
}

ol > li {
	padding-left: 0.2em;
}

.mono ol {
	padding-inline-start: 2em;
}

.mono ol > li {
	text-indent: -0.4em;
}

.toggle {
	padding-inline-start: 0em;
	list-style-type: none;
}

/* Indent toggle children */
.toggle > li > details {
	padding-left: 1.7em;
}

.toggle > li > details > summary {
	margin-left: -1.1em;
}

.selected-value {
	display: inline-block;
	padding: 0 0.5em;
	background: rgba(206, 205, 202, 0.5);
	border-radius: 3px;
	margin-right: 0.5em;
	margin-top: 0.3em;
	margin-bottom: 0.3em;
	white-space: nowrap;
}

.collection-title {
	display: inline-block;
	margin-right: 1em;
}

.simple-table {
	margin-top: 1em;
	font-size: 0.875rem;
	empty-cells: show;
}
.simple-table td {
	height: 29px;
	min-width: 120px;
}

.simple-table th {
	height: 29px;
	min-width: 120px;
}

.simple-table-header-color {
	background: rgb(247, 246, 243);
	color: black;
}
.simple-table-header {
	font-weight: 500;
}

time {
	opacity: 0.5;
}

.icon {
	display: inline-block;
	max-width: 1.2em;
	max-height: 1.2em;
	text-decoration: none;
	vertical-align: text-bottom;
	margin-right: 0.5em;
}

img.icon {
	border-radius: 3px;
}

.user-icon {
	width: 1.5em;
	height: 1.5em;
	border-radius: 100%;
	margin-right: 0.5rem;
}

.user-icon-inner {
	font-size: 0.8em;
}

.text-icon {
	border: 1px solid #000;
	text-align: center;
}

.page-cover-image {
	display: block;
	object-fit: cover;
	width: 100%;
	max-height: 30vh;
}

.page-header-icon {
	font-size: 3rem;
	margin-bottom: 1rem;
}

.page-header-icon-with-cover {
	margin-top: -0.72em;
	margin-left: 0.07em;
}

.page-header-icon img {
	border-radius: 3px;
}

.link-to-page {
	margin: 1em 0;
	padding: 0;
	border: none;
	font-weight: 500;
}

p > .user {
	opacity: 0.5;
}

td > .user,
td > time {
	white-space: nowrap;
}

input[type="checkbox"] {
	transform: scale(1.5);
	margin-right: 0.6em;
	vertical-align: middle;
}

p {
	margin-top: 0.5em;
	margin-bottom: 0.5em;
}

.image {
	border: none;
	margin: 1.5em 0;
	padding: 0;
	border-radius: 0;
	text-align: center;
}

.code,
code {
	background: rgba(135, 131, 120, 0.15);
	border-radius: 3px;
	padding: 0.2em 0.4em;
	border-radius: 3px;
	font-size: 85%;
	tab-size: 2;
}

code {
	color: #eb5757;
}

.code {
	padding: 1.5em 1em;
}

.code-wrap {
	white-space: pre-wrap;
	word-break: break-all;
}

.code > code {
	background: none;
	padding: 0;
	font-size: 100%;
	color: inherit;
}

blockquote {
	font-size: 1.25em;
	margin: 1em 0;
	padding-left: 1em;
	border-left: 3px solid rgb(55, 53, 47);
}

.bookmark {
	text-decoration: none;
	max-height: 8em;
	padding: 0;
	display: flex;
	width: 100%;
	align-items: stretch;
}

.bookmark-title {
	font-size: 0.85em;
	overflow: hidden;
	text-overflow: ellipsis;
	height: 1.75em;
	white-space: nowrap;
}

.bookmark-text {
	display: flex;
	flex-direction: column;
}

.bookmark-info {
	flex: 4 1 180px;
	padding: 12px 14px 14px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
}

.bookmark-image {
	width: 33%;
	flex: 1 1 180px;
	display: block;
	position: relative;
	object-fit: cover;
	border-radius: 1px;
}

.bookmark-description {
	color: rgba(55, 53, 47, 0.6);
	font-size: 0.75em;
	overflow: hidden;
	max-height: 4.5em;
	word-break: break-word;
}

.bookmark-href {
	font-size: 0.75em;
	margin-top: 0.25em;
}

.sans { font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol"; }
.code { font-family: "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace; }
.serif { font-family: Lyon-Text, Georgia, ui-serif, serif; }
.mono { font-family: iawriter-mono, Nitti, Menlo, Courier, monospace; }
.pdf .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK JP'; }
.pdf:lang(zh-CN) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK SC'; }
.pdf:lang(zh-TW) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK TC'; }
.pdf:lang(ko-KR) .sans { font-family: Inter, ui-sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, "Apple Color Emoji", Arial, sans-serif, "Segoe UI Emoji", "Segoe UI Symbol", 'Twemoji', 'Noto Color Emoji', 'Noto Sans CJK KR'; }
.pdf .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK JP'; }
.pdf:lang(zh-CN) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK SC'; }
.pdf:lang(zh-TW) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK TC'; }
.pdf:lang(ko-KR) .code { font-family: Source Code Pro, "SFMono-Regular", Menlo, Consolas, "PT Mono", "Liberation Mono", Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK KR'; }
.pdf .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK JP'; }
.pdf:lang(zh-CN) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK SC'; }
.pdf:lang(zh-TW) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK TC'; }
.pdf:lang(ko-KR) .serif { font-family: PT Serif, Lyon-Text, Georgia, ui-serif, serif, 'Twemoji', 'Noto Color Emoji', 'Noto Serif CJK KR'; }
.pdf .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK JP'; }
.pdf:lang(zh-CN) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK SC'; }
.pdf:lang(zh-TW) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK TC'; }
.pdf:lang(ko-KR) .mono { font-family: PT Mono, iawriter-mono, Nitti, Menlo, Courier, monospace, 'Twemoji', 'Noto Color Emoji', 'Noto Sans Mono CJK KR'; }
.highlight-default {
	color: rgba(55, 53, 47, 1);
}
.highlight-gray {
	color: rgba(120, 119, 116, 1);
	fill: rgba(120, 119, 116, 1);
}
.highlight-brown {
	color: rgba(159, 107, 83, 1);
	fill: rgba(159, 107, 83, 1);
}
.highlight-orange {
	color: rgba(217, 115, 13, 1);
	fill: rgba(217, 115, 13, 1);
}
.highlight-yellow {
	color: rgba(203, 145, 47, 1);
	fill: rgba(203, 145, 47, 1);
}
.highlight-teal {
	color: rgba(68, 131, 97, 1);
	fill: rgba(68, 131, 97, 1);
}
.highlight-blue {
	color: rgba(51, 126, 169, 1);
	fill: rgba(51, 126, 169, 1);
}
.highlight-purple {
	color: rgba(144, 101, 176, 1);
	fill: rgba(144, 101, 176, 1);
}
.highlight-pink {
	color: rgba(193, 76, 138, 1);
	fill: rgba(193, 76, 138, 1);
}
.highlight-red {
	color: rgba(212, 76, 71, 1);
	fill: rgba(212, 76, 71, 1);
}
.highlight-gray_background {
	background: rgba(241, 241, 239, 1);
}
.highlight-brown_background {
	background: rgba(244, 238, 238, 1);
}
.highlight-orange_background {
	background: rgba(251, 236, 221, 1);
}
.highlight-yellow_background {
	background: rgba(251, 243, 219, 1);
}
.highlight-teal_background {
	background: rgba(237, 243, 236, 1);
}
.highlight-blue_background {
	background: rgba(231, 243, 248, 1);
}
.highlight-purple_background {
	background: rgba(244, 240, 247, 0.8);
}
.highlight-pink_background {
	background: rgba(249, 238, 243, 0.8);
}
.highlight-red_background {
	background: rgba(253, 235, 236, 1);
}
.block-color-default {
	color: inherit;
	fill: inherit;
}
.block-color-gray {
	color: rgba(120, 119, 116, 1);
	fill: rgba(120, 119, 116, 1);
}
.block-color-brown {
	color: rgba(159, 107, 83, 1);
	fill: rgba(159, 107, 83, 1);
}
.block-color-orange {
	color: rgba(217, 115, 13, 1);
	fill: rgba(217, 115, 13, 1);
}
.block-color-yellow {
	color: rgba(203, 145, 47, 1);
	fill: rgba(203, 145, 47, 1);
}
.block-color-teal {
	color: rgba(68, 131, 97, 1);
	fill: rgba(68, 131, 97, 1);
}
.block-color-blue {
	color: rgba(51, 126, 169, 1);
	fill: rgba(51, 126, 169, 1);
}
.block-color-purple {
	color: rgba(144, 101, 176, 1);
	fill: rgba(144, 101, 176, 1);
}
.block-color-pink {
	color: rgba(193, 76, 138, 1);
	fill: rgba(193, 76, 138, 1);
}
.block-color-red {
	color: rgba(212, 76, 71, 1);
	fill: rgba(212, 76, 71, 1);
}
.block-color-gray_background {
	background: rgba(241, 241, 239, 1);
}
.block-color-brown_background {
	background: rgba(244, 238, 238, 1);
}
.block-color-orange_background {
	background: rgba(251, 236, 221, 1);
}
.block-color-yellow_background {
	background: rgba(251, 243, 219, 1);
}
.block-color-teal_background {
	background: rgba(237, 243, 236, 1);
}
.block-color-blue_background {
	background: rgba(231, 243, 248, 1);
}
.block-color-purple_background {
	background: rgba(244, 240, 247, 0.8);
}
.block-color-pink_background {
	background: rgba(249, 238, 243, 0.8);
}
.block-color-red_background {
	background: rgba(253, 235, 236, 1);
}
.select-value-color-pink { background-color: rgba(245, 224, 233, 1); }
.select-value-color-purple { background-color: rgba(232, 222, 238, 1); }
.select-value-color-green { background-color: rgba(219, 237, 219, 1); }
.select-value-color-gray { background-color: rgba(227, 226, 224, 1); }
.select-value-color-opaquegray { background-color: rgba(255, 255, 255, 0.0375); }
.select-value-color-orange { background-color: rgba(250, 222, 201, 1); }
.select-value-color-brown { background-color: rgba(238, 224, 218, 1); }
.select-value-color-red { background-color: rgba(255, 226, 221, 1); }
.select-value-color-yellow { background-color: rgba(253, 236, 200, 1); }
.select-value-color-blue { background-color: rgba(211, 229, 239, 1); }

.checkbox {
	display: inline-flex;
	vertical-align: text-bottom;
	width: 16;
	height: 16;
	background-size: 16px;
	margin-left: 2px;
	margin-right: 5px;
}

.checkbox-on {
	background-image: url("data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2216%22%20height%3D%2216%22%20viewBox%3D%220%200%2016%2016%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Crect%20width%3D%2216%22%20height%3D%2216%22%20fill%3D%22%2358A9D7%22%2F%3E%0A%3Cpath%20d%3D%22M6.71429%2012.2852L14%204.9995L12.7143%203.71436L6.71429%209.71378L3.28571%206.2831L2%207.57092L6.71429%2012.2852Z%22%20fill%3D%22white%22%2F%3E%0A%3C%2Fsvg%3E");
}

.checkbox-off {
	background-image: url("data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2216%22%20height%3D%2216%22%20viewBox%3D%220%200%2016%2016%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Crect%20x%3D%220.75%22%20y%3D%220.75%22%20width%3D%2214.5%22%20height%3D%2214.5%22%20fill%3D%22white%22%20stroke%3D%22%2336352F%22%20stroke-width%3D%221.5%22%2F%3E%0A%3C%2Fsvg%3E");
}
	
</style></head><body><article id="319492f0-fef9-49cb-842c-c41ac6ece515" class="page sans"><header><h1 class="page-title">뤼튼 개인정보 처리방침</h1></header><div class="page-body"><p id="618deb27-691c-4cd1-8fb6-37c5e8b803de" class="">뤼튼테크놀로지스(이하 ‘회사’)는 관련 법령에 따라 뤼튼(wrtn) 서비스 및 이에 부수하는 제반 서비스(통칭하여 이하 ‘서비스’)를 이용하는 이용자의 개인정보를 보호하고, 이와 관련한 고충을 신속하고 원활하게 처리하기 위하여 다음과 같이 개인정보 처리방침을 수립·공개합니다.</p><p id="47fe13b7-f63e-4cfc-b21d-14122da60059" class="">
</p><h3 id="a741e357-9258-4ad4-9c34-b81e5d26f92c" class=""><strong>제1조 (개인정보의 처리 목적)</strong></h3><p id="d4252b84-1314-4471-bdd6-20b08bd6aa70" class="">회사는 다음의 목적을 위하여 개인정보를 처리합니다.</p><ol type="1" id="d8955753-89fa-4df2-bd69-e320d3ddb5fd" class="numbered-list" start="1"><li>회원 가입 및 관리: 회원가입의사 확인, 본인 식별인증, 연령인증, 회원자격 유지 및 관리, 서비스 부정이용 방지 만14세 미만 아동의 개인정보 처리 시 법정대리인의 동의여부 확인, 각종 고지통지</li></ol><ol type="1" id="681f488c-e44c-46a7-ba92-5f19a4b8c2b0" class="numbered-list" start="2"><li>재화 또는 서비스 제공: 물품 배송, 기본/맞춤형 서비스 제공, 계약서청구서 발송, 본인인증, 연령인증, 요금결제 및 정산, 채권추심</li></ol><ol type="1" id="cd03f2eb-6eae-4e9c-8d82-ee2c6a368d3c" class="numbered-list" start="3"><li>고충처리: 이용자의 신원 확인, 고충사항 확인, 사실조사를 위한 연락통지, 처리결과 통보</li></ol><ol type="1" id="23eba095-e80c-4a5f-93d2-e3bb6d867e10" class="numbered-list" start="4"><li>마케팅 및 광고에의 활용: 맞춤형 광고 제공, 이벤트 및 광고성 정보 제공 및 참여기회 제공</li></ol><ol type="1" id="c46e5e4e-2bfe-45e6-8a0a-24bfaad98336" class="numbered-list" start="5"><li>서비스 개선 및 개발: 기존 서비스 개선 및 신규 서비스, 맞춤형 서비스 개발</li></ol><ol type="1" id="64253ac4-041f-46d3-a684-9996d8067bdf" class="numbered-list" start="6"><li>가명정보의 활용: 통계작성, 과학적 연구, 공익적 기록보존 등을 위한 가명처리 및 가명정보의 활용</li></ol><p id="cc633815-ee2e-48ec-b0b7-8b8ef864df27" class="">
</p><h3 id="1eae0e06-5b8f-4ea0-b2a9-632f97ff0073" class=""><strong>제2조 (처리하는 개인정보 항목)</strong></h3><p id="574e5e2d-0d1b-4af9-8c47-57f86d22e8ee" class="">① 회사는 서비스 이용자에 대하여 다음의 개인정보항목을 수집하여 처리하고 있습니다.</p><ol type="1" id="ef13476d-4c26-46ea-9478-ba9cc8b5da4a" class="numbered-list" start="1"><li>회원 가입 시 기본수집사항(필수항목): 이메일, 로그인ID, 이름, 프롬프트에 입력한 텍스트 내용 정보와 AI 엔진이 출력한 텍스트 내용 정보</li></ol><ol type="1" id="f62ed59c-8c88-41e4-bed6-014d7b0891ca" class="numbered-list" start="2"><li>만 14세 미만 아동의 회원 가입 시 기본수집사항(필수항목): 법정대리인 이름, 법정대리인 휴대전화번호</li></ol><ol type="1" id="891d26fd-9d87-4328-89cd-4a92c7e26f29" class="numbered-list" start="3"><li>회원 가입 선택 수단에 따라 회원 가입 승인 목적에서 제휴사로부터 추가로 제공받는 사항</li></ol><table id="dab0b4d8-f0a6-422d-b1f1-e094ae3f5ca9" class="simple-table"><tbody><tr id="4b656e81-cf0b-451a-a0b6-5849ed14ff4e"><td id="Oc~f" class="" style="width:311px">회원 가입 시 이용자가 선택한 제휴사/서비스</td><td id="BjZ~" class="" style="width:323px">필수 제공 항목</td><td id="S{;L" class="">선택 제공 항목</td></tr><tr id="0745d55a-6c52-43fd-922b-1a95140992a0"><td id="Oc~f" class="" style="width:311px">Kakaotalk으로 회원가입(제휴사: 주식회사 카카오)</td><td id="BjZ~" class="" style="width:323px">이메일, 이름</td><td id="S{;L" class="">연령대, 성별</td></tr><tr id="2d449e44-3bcc-4813-b4fb-ff8c1a681d06"><td id="Oc~f" class="" style="width:311px">Google ID로 회원가입(제휴사: Google LLC.)</td><td id="BjZ~" class="" style="width:323px">이메일, 이름, 프로필 이미지, 로케일(사용지역 및 언어)</td><td id="S{;L" class=""></td></tr><tr id="891a6335-5101-4373-90d4-9d8ebc8ebfe8"><td id="Oc~f" class="" style="width:311px">NAVER ID로 회원가입(제휴사: 네이버 주식회사)</td><td id="BjZ~" class="" style="width:323px">이메일, 이름</td><td id="S{;L" class="">연령대, 성별</td></tr></tbody></table><ol type="1" id="87574017-cbad-4060-9696-bddfadee2daa" class="numbered-list" start="4"><li>유료서비스 이용 시 수집사항<ul id="b025771d-ac06-42dc-beb0-879204d06c7e" class="bulleted-list"><li style="list-style-type:disc">신용카드 결제 시 : 카드사명, 카드번호, 이메일, 생년월일, 사업자등록번호</li></ul><ul id="70d89aeb-8457-4c23-bd4a-d87123f9ddd6" class="bulleted-list"><li style="list-style-type:disc">계좌이체 시 : 예금주명, 계좌번호, 계좌보유은행</li></ul><ul id="78192576-cc25-4e80-992e-ddb37c011549" class="bulleted-list"><li style="list-style-type:disc">휴대전화번호 결제 시 : 휴대전화번호, 통신사 정보</li></ul></li></ol><ol type="1" id="130df05e-6b21-443e-979d-fa11ac8cc070" class="numbered-list" start="5"><li>중복 계정 생성을 방지하기 위한 수집사항(필수항목) : 휴대전화번호</li></ol><ol type="1" id="fe52d933-79de-46f0-814c-6084685894f5" class="numbered-list" start="6"><li>서비스 이용과정에서 아래 개인정보 항목이 자동으로 : IP주소, 쿠키, 서비스 이용 기록(방문 및 이용기록, 불량 이용기록 등), 기기정보(휴대폰 모델명, OS 명 및 버전정보), 광고식별자</li></ol><ol type="1" id="472fcba9-f475-44bc-84e9-873aca0173d8" class="numbered-list" start="7"><li>고충처리 시: 이용자로부터 위 각 정보 중 필요한 항목 및 해당 고충처리에 필요한 별개 항목을 수집 및 처리</li></ol><p id="b0b52d55-5c7f-4c8a-9782-18f181a6b83f" class="">
</p><h3 id="cb443cc4-63a0-472f-b3e3-a28ec917f9d5" class=""><strong>제3조 (개인정보의 처리 및 보유기간)</strong></h3><p id="faf77af4-1b8b-4f37-8359-45e25cc6fea3" class="">① 회사는 개인정보의 수집 및 이용목적이 달성되거나, 이용자가 서비스를 탈퇴 또는 이용자격을 상실할 경우에는 별도의 요청이 없더라도 수집된 이용자의 정보를 지체없이 삭제 및 파기합니다. 다만, 회원 탈퇴 또는 이용자격 상실에도 불구하고 다음의 정보에 대해서는 아래의 이유로 보존합니다.</p><ol type="1" id="f3fb8751-d237-4c5f-9b3d-e9478c9dc10c" class="numbered-list" start="1"><li>관계 법령 위반에 따른 수사조사 등이 진행 중인 경우에는 해당 수사·조사 종료 시까지</li></ol><ol type="1" id="4f52e016-ed59-4758-b4b3-e17d24e97625" class="numbered-list" start="2"><li>앱 이용에 따른 채권·채무관계 잔존 시에는 해당 채권·채무관계 정산 시까지</li></ol><ol type="1" id="00572b8a-3d3a-4191-ad3c-ff9a3d83becd" class="numbered-list" start="3"><li>회사가 이용약관에 따라 서비스 이용계약을 해지한 경우 부정한 재가입 및 서비스 이용을 방지하기 위하여 서비스</li></ol><p id="6eb32036-288d-4c34-bbd6-d0aa7c67507a" class="">② 전항에도 불구하고 회사는 다음의 사유에 해당하는 경우에는 해당 기간 종료 시까지 보존합니다.</p><ol type="1" id="61606a10-2bc8-4d60-9bbe-484ee9be35eb" class="numbered-list" start="1"><li>서비스 이용 관련 개인정보 (로그기록): 「통신비밀보호법」상 보존기간인 3개월</li></ol><ol type="1" id="59ccd940-6c68-4f15-8e7f-316cddcd16de" class="numbered-list" start="2"><li>계약 또는 청약철회 등에 관한 기록 및 대금결제 및 재화 등의 공급에 관한 기록: 「전자상거래 등에서의 소비자보호에 관한 법률」상 보존기간인 5년</li></ol><ol type="1" id="27c1ec99-c6b8-499f-b646-1fb4d9514a96" class="numbered-list" start="3"><li>소비자의 불만 또는 분쟁처리에 관한 기록: 「전자상거래 등에서의 소비자보호에 관한 법률」상 보존기간인 3년</li></ol><ol type="1" id="54883c16-ab6f-40bf-a34a-32e5db213dd4" class="numbered-list" start="4"><li>표시 광고에 관한 기록: 「전자상거래 등에서의 소비자보호에 관한 법률」상 보존기간인 6개월</li></ol><ol type="1" id="d7a3421a-85f0-4de2-833b-fc656a07578b" class="numbered-list" start="5"><li>세법이 규정하는 모든 거래에 관한 장부 및 증빙서류: 「국세기본법」상 보존기간인 5년</li></ol><p id="b9cf85e8-ac61-4161-a0d6-459fb546bffc" class="">③ 회사는 1년 또는 이용자가 별도로 정한 기간(3년) 동안 서비스를 이용하지 않은 이용자의 개인정보를 별도로 분리보관 또는 삭제하여 관리합니다.</p><p id="55087221-b758-4ea0-81ce-df2a1e2b5010" class="">
</p><h3 id="35cc2c15-ce47-4d06-b4da-667763ada9ab" class=""><strong>제4조 (개인정보의 제3자 제공)</strong></h3><p id="204ad1ab-d5c1-4a62-9f2a-e4cc421d10c8" class="">회사는 이용자의 동의를 받거나 개인정보 보호법 또는 다른 법률의 특별한 규정이 있는 경우에만 개인정보를 제3자에게 제공합니다.</p><p id="dec6b538-9c11-4cf8-a445-1f7907eb4614" class="">
</p><h3 id="41453fcb-9d8f-4a67-aa45-90b9f7517679" class=""><strong>제5조 (개인정보처리의 위탁)</strong></h3><p id="aa4318cb-95ac-4f9c-8549-d91bf42ad1b2" class="">① 회사는 원활한 개인정보 업무처리를 위하여 다음과 같이 개인정보 처리업무를 위탁하고 있습니다.</p><table id="31a7b9fd-6bb7-4e5b-9ef2-68e35d553d60" class="simple-table"><tbody><tr id="7cb8580b-ccae-454b-921f-e552e0950146"><td id="ETQ?" class="">위탁받는 자(수탁자)</td><td id="=orX" class="" style="width:486px">위탁하는 업무의 내용</td></tr><tr id="72e27eec-3710-4d72-ab47-c198f3657d22"><td id="ETQ?" class="">㈜채널코퍼레이션, ㈜카카오</td><td id="=orX" class="" style="width:486px">회원제 서비스 이용에 따른 본인확인, 불만처리 등 민원처리</td></tr><tr id="4a7e9a32-6602-4720-99cb-b353d748d6f3"><td id="ETQ?" class="">㈜카카오, ㈜버킷리스트, 스티비㈜</td><td id="=orX" class="" style="width:486px">고지사항 전달, 신규 서비스(제품) 개발 및 맞춤 서비스 제공, 이벤트 및 광고성 정보 제공 및 참여기회 제공</td></tr><tr id="a705a909-1e5e-46de-be80-5e4da6a7ae07"><td id="ETQ?" class="">Amazon Web Services Inc.</td><td id="=orX" class="" style="width:486px">개인정보 처리를 위한 인프라 관리</td></tr><tr id="b40b5b62-9e17-45f2-aa3d-e1f3e314a26f"><td id="ETQ?" class="">토스페이먼츠 주식회사</td><td id="=orX" class="" style="width:486px">유료 서비스 제공을 위한 결제 위탁</td></tr></tbody></table><p id="9b4b9d32-12c1-48f6-81fc-974c7e3b5a33" class="">② 회사는 위탁계약 체결 시 개인정보 보호법 제26조에 따라 위탁업무 수행목적 외 개인정보 처리금지, 기술적∙관리적 보호조치, 재위탁 제한, 수탁자에 대한 관리∙감독 등 책임에 관한 사항을 계약서 등 문서에 명시하고, 수탁자가 개인정보를 안전하게 처리하는지를 감독하고 있습니다.</p><p id="6d3e8868-66c7-417e-9dc8-8c6155a35526" class="">③ 회사는 아래와 같이 국외의 제3자에게 개인정보 처리업무를 위탁하고 있습니다.</p><ol type="1" id="b836088d-99e2-4450-b8cf-e82740183b4e" class="numbered-list" start="1"><li>Google LLC.</li></ol><ul id="b8ed774d-6a98-4957-baaf-7451f5d77909" class="bulleted-list"><li style="list-style-type:disc">이전되는 개인정보 항목</li></ul><ul id="4312be2e-91e4-4dc4-b26a-d506e959326a" class="bulleted-list"><li style="list-style-type:disc">이전되는 국가: 미국</li></ul><ul id="fc7241f4-c847-4d91-b824-fedd1415f61c" class="bulleted-list"><li style="list-style-type:disc">이전일시 및 이전방법: 서비스 이용 시점에 정보통신망을 통해 수시로 이전</li></ul><ul id="f2a801ab-4516-40f2-86c2-6f92bf5d8123" class="bulleted-list"><li style="list-style-type:disc">이전받는 자의 성명/연락처: Google LLC. / googlekrsupport@google.com</li></ul><ul id="70add97a-13de-45f0-a2a2-bd5c8b4f0f10" class="bulleted-list"><li style="list-style-type:disc">이전받는 자의 개인정보 이용 목적: 개인정보 및 사용자 데이터 분석</li></ul><ul id="2d5bd476-3a8c-4578-bab5-45382ce8fd0a" class="bulleted-list"><li style="list-style-type:disc">이전받는 자의 보유ㆍ이용 기간: 회원 탈퇴 또는 이용자격 상실 이후 즉시 파기(단, 제3조 제2항 또는 제3항이 적용되는 경우 그에 따름)</li></ul><p id="b9012031-086f-4265-87da-03203a35353c" class="">
</p><h3 id="1f026f4f-a097-45c3-9e55-f9bf204a7ef7" class=""><strong>제6조 (수집목적과 합리적으로 관련된 범위 내의 개인정보 이용 및 제공)</strong></h3><p id="033f2651-07a2-46e1-a1fa-fae34e4b4c63" class="">회사는 당초 수집 목적과 합리적인 범위 내에서 아래 각 기준을 고려하여, 이용자의 동의 없이 개인정보를 이용 또는 제3자에게 제공할 수 있습니다.</p><ol type="1" id="fc076a12-2c4e-41f1-a3d0-ef0e8e11096b" class="numbered-list" start="1"><li>당초 수집 목적과 관련성이 있는지 여부: 당초 수집 목적과 추가적 이용·제공 목적이 성질이나 경향에 있어 연관이 있는지 등을 고려하여 따라 판단</li></ol><ol type="1" id="27a8ab48-480a-405e-bcb8-3df32590fbe1" class="numbered-list" start="2"><li>개인정보를 수집한 정황 또는 처리 관행에 비추어 볼 때 개인정보의 추가적인 이용 또는 제공에 대한 예측 가능성이 있는지 여부: 개인정보처리자와 이용자 간의 관계, 기술 수준 및 발전 속도, 상당한 기간동안 정립된 일반적인 사정(관행) 등을 고려하여 판단</li></ol><ol type="1" id="3791659a-91bc-423e-9dcf-5b78b4260ca5" class="numbered-list" start="3"><li>이용자의 이익을 부당하게 침해하는지 여부: 추가적인 이용 목적과의 관계에서 이용자의 이익이 실질적으로 침해되는지 및 해당 이익 침해가 부당한지 등을 고려하여 판단</li></ol><ol type="1" id="fa8fe207-765b-4d4a-b76f-03b16b1a993c" class="numbered-list" start="4"><li>가명처리 또는 암호화 등 안전성 확보에 필요한 조치를 하였는지 여부: 침해 가능성을 고려한 안전 조치가 취해지는지 등을 고려하여 판단</li></ol><p id="8b62083d-ad52-4774-b7fd-f7a2a091f457" class="">
</p><h3 id="a5307449-f5ea-4fc4-acd9-075e76cb66be" class=""><strong>제7조 (이용자와 법정대리인의 권리·의무 및 행사방법)</strong></h3><p id="55349d8c-d28f-45c5-8820-7491feb3416c" class="">① 이용자는 회사에 대해 언제든지 개인정보 열람·정정·삭제·처리정지 요구 등의 권리를 행사할 수 있습니다.</p><p id="60860898-8ad4-415d-9c9b-558a8450e444" class="">② 제1항에 따른 권리 행사는 서면, 전자우편 등을 통하여 하실 수 있으며, 회사는 이에 대해 지체 없이 조치하겠습니다.</p><p id="38c227de-e5b9-4632-86b8-708422e0e5c6" class="">③ 제1항에 따른 권리 행사는 이용자의 법정대리인이나 위임을 받은 자 등 대리인을 통하여서 하실 수 있습니다. 이 경우 수임인에 대한 위임사실을 확인할 수 있는 위임장을 제출하셔야 합니다.</p><p id="8a1f4d4e-9dcc-4ec0-9032-f3b5b63c13de" class="">④ 개인정보 보호법 등 관계 법령에서 정하는 바에 따라 이용자의 개인정보 열람·정정·삭제·처리정지 요구 등의 권리 행사가 제한될 수 있습니다.</p><p id="de41dd8b-b3a6-4113-98f2-dd4f22798eb8" class="">⑤ 개인정보의 정정 및 삭제 요구는 다른 법령에서 그 개인정보가 수집 대상으로 명시되어 있는 경우에는 그 삭제를 요구할 수 없습니다.</p><p id="2ce3fd84-0665-479f-812c-ce51e16b6e4d" class="">⑥ 회사는 이용자 권리에 따른 열람의 요구, 정정·삭제의 요구, 처리정지의 요구 시 열람 등 요구를 한 자가 본인이거나 정당한 대리인인지를 확인합니다.</p><p id="5e5532ad-4ed9-489b-ad0a-47ba2d0556d3" class="">
</p><h3 id="1f72b455-0607-4b75-9202-32e96f0a7daf" class=""><strong>제8조 (개인정보의 파기)</strong></h3><p id="03d4d776-1f67-4e89-9b69-5e96acfbb642" class="">① 회사는 개인정보 보유기간의 경과, 처리목적 달성 등 개인정보가 불필요하게 되었을 때에는 지체없이 해당 개인정보를 파기합니다.</p><p id="68bee750-56e6-4f20-9f57-f720536f2cc2" class="">② 이용자로부터 동의 받은 개인정보 보유기간이 경과하거나 처리목적이 달성되었음에도 불구하고 제3조 제2항에 기재된 법령에 따라 개인정보를 계속 보존하여야 하는 경우에는, 해당 개인정보를 별도의 데이터베이스(DB)로 옮기거나 보관장소를 달리하여 보존합니다.</p><p id="0f964e1e-489f-46fb-bd46-096c41f26e3a" class="">③ 개인정보 파기의 절차 및 방법은 다음과 같습니다.</p><ol type="1" id="bd23e9f1-8390-4365-ac97-6c182cd2c795" class="numbered-list" start="1"><li>파기절차: 회사는 파기 사유가 발생한 개인정보를 선정하고, 회사의 개인정보 보호책임자의 승인을 받아 개인정보를 파기합니다.</li></ol><ol type="1" id="1bb8d35e-6bda-4f6c-a385-2495c4d7365c" class="numbered-list" start="2"><li>파기방법: 회사는 전자적 파일 형태로 기록·저장된 개인정보는 기록을 재생할 수 없도록 기술적 방법을 이용하여 파기하며, 종이 문서에 기록·저장된 개인정보는 분쇄기로 분쇄하거나 소각하여 파기합니다.</li></ol><p id="f02c721a-57ca-464d-9d4a-8df8ea54afa8" class="">
</p><h3 id="30e519a2-bbf5-474b-82ef-ebd21ce8a710" class=""><strong>제9조 (개인정보의 안전성 확보조치)</strong></h3><p id="58153d9d-0b84-476d-aee7-56d8062f31b3" class="">회사는 개인정보의 안전성 확보를 위해 다음과 같은 조치를 취하고 있습니다.</p><ol type="1" id="f0a94151-7592-4b9b-ba97-5af192135a59" class="numbered-list" start="1"><li>관리적 조치: 내부관리계획 수립·시행, 정기적 직원 교육 등</li></ol><ol type="1" id="8dbb75e4-ad54-4cb7-ae5c-3ecfdf43fe25" class="numbered-list" start="2"><li>기술적 조치: 해킹 등에 대비한 기술적 대책개인정보의 암호화, 접속기록의 보관 및 위변조 방지 등</li></ol><ol type="1" id="fb702e78-15b3-4041-88c9-5eb86fe54aa0" class="numbered-list" start="3"><li>물리적 조치: 서버실, 자료보관실 등의 접근통제 등</li></ol><p id="bc013d6c-c131-4213-be1d-f2adb52d0c72" class="">
</p><h3 id="07b8247e-8b9b-44d3-9ebf-748cd7dd76fe" class=""><strong>제10조 (개인정보 자동 수집 장치의 설치∙운영 및 거부에 관한 사항)</strong></h3><p id="42d6be5a-0548-42b1-9884-1e5562482e89" class="">회사는 이용자에게 개별적인 맞춤서비스를 제공하기 위해 아래와 같이 이용정보를 저장하고 수시로 불러오는 ‘쿠키(cookie)’를 사용합니다. 쿠키는 웹사이트를 운영하는데 이용되는 서버가 이용자의 컴퓨터 브라우저에게 보내는 소량의 정보이며 이용자들의 PC 컴퓨터 내의 하드디스크에 저장되기도 합니다.</p><ol type="1" id="79344419-310c-497d-bc0c-2307aafc44f2" class="numbered-list" start="1"><li>쿠키의 사용목적: 이용자의 접속 빈도나 방문 시간 등을 분석, 이용자의 서비스 사용 패턴 파악 및 자취 추적, 보안 접속 여부, 이용자 규모 등을 통해 보안 관리 및 서비스 개선 및 신규 서비스 개발, 맞춤형 서비스 및 광고 제공에 이용합니다.</li></ol><ol type="1" id="1eb97b50-8e3b-4931-9fdb-4ff3f8dd9501" class="numbered-list" start="2"><li>쿠키의 설치∙운영 및 거부: 서비스 이용자는 쿠키 설치에 대한 선택권을 가지고 있습니다. 따라서, 이용자는 웹 브라우저에서 옵션의 설정을 변경함으로써 쿠키의 저장을 거부할 수 있습니다.</li></ol><ol type="1" id="4b337ea6-5b71-489c-84a1-5c1d628640de" class="numbered-list" start="3"><li>쿠키 저장을 거부할 경우 일부 서비스 이용에 어려움이 발생할 수 있습니다.</li></ol><p id="e694d8ce-3df4-43e5-bc25-2496fc2bb774" class="">
</p><h3 id="deb037c5-9629-4ddb-907d-3230479ccc09" class=""><strong>제11조 (개인정보 보호책임자)</strong></h3><p id="297fdb5f-b452-4f49-b683-35daccad4a22" class="">① 회사는 개인정보 처리에 관한 업무를 총괄해서 책임지고, 개인정보 처리와 관련한 이용자의 불만처리 및 피해구제 등을 위하여 아래와 같이 개인정보 보호책임자를 지정하고 있습니다.</p><ul id="87684f72-f43a-40f3-a2fb-32f23b1494a0" class="bulleted-list"><li style="list-style-type:disc">개인정보 보호 책임자<ul id="0ee1a91d-f59a-480b-aa8b-b0153e5d58c1" class="bulleted-list"><li style="list-style-type:circle">성명: 이세영</li></ul><ul id="b31c90e3-5492-4969-90d1-33d54cf51c7d" class="bulleted-list"><li style="list-style-type:circle">직책: 대표이사</li></ul><ul id="6943cbb5-9dd0-4681-ace7-fc437fb76567" class="bulleted-list"><li style="list-style-type:circle">연락처: 02-499-1885, contact@wrtn.io</li></ul></li></ul><p id="80b00c52-e25c-4263-a38d-7849212880b5" class="">② 이용자는 회사의 서비스(또는 사업)을 이용하시면서 발생한 모든 개인정보 보호 관련 문의, 불만처리, 피해구제 등에 관한 사항을 개인정보 보호책임자 및 아래 담당부서로 문의하실 수 있습니다.</p><ul id="65c41694-2a7f-4033-b799-dc318834b082" class="bulleted-list"><li style="list-style-type:disc">부서명: 뤼튼테크놀로지스 CX팀</li></ul><ul id="5c68d35d-0040-4eb6-833a-45c8e75408db" class="bulleted-list"><li style="list-style-type:disc">연락처: 02-499-1885, hello@wrtn.ai</li></ul><p id="f55c80b3-588b-4781-a3b8-bb71bf908fb0" class="">
</p><h3 id="8c4ce651-e2f7-4a2f-b4ad-a5bee0b21dd0" class=""><strong>제12조 (권익침해 구제방법)</strong></h3><p id="63fdb5ac-0260-4b9d-9911-fb4463dccda5" class="">① 이용자는 개인정보침해로 인한 구제를 받기 위하여 개인정보분쟁조정위원회, 한국인터넷진흥원 개인정보침해신고센터 등에 분쟁해결이나 상담 등을 신청할 수 있습니다. 이 밖에 기타 개인정보침해의 신고, 상담에 대하여는 아래의 기관에 문의하시기 바랍니다.</p><ul id="5e9ca9b1-687c-49ef-a042-c7bcfcff62fb" class="bulleted-list"><li style="list-style-type:disc">개인정보분쟁조정위원회 : (국번없이) 1833-6972 (www.kopico.go.kr)</li></ul><ul id="660cbd09-bd95-4a66-a636-86e69c9a6aba" class="bulleted-list"><li style="list-style-type:disc">개인정보침해신고센터 : (국번없이) 118 (privacy.kisa.or.kr)</li></ul><ul id="5dae5d3c-4201-4177-baa8-09ecd1c229c5" class="bulleted-list"><li style="list-style-type:disc">대검찰청 : (국번없이) 1301 (www.spo.go.kr)</li></ul><ul id="e48cc536-8614-4c45-8b37-62782cf53f2e" class="bulleted-list"><li style="list-style-type:disc">경찰청 : (국번없이) 182 (ecrm.cyber.go.kr)</li></ul><p id="5ef94588-6f6c-48b9-abe7-dfb8a16dafee" class="">② 회사는 이용자의 개인정보자기결정권을 보장하고, 개인정보침해로 인한 상담 및 피해 구제를 위해 노력하고 있으며, 신고나 상담이 필요한 경우 제11조 제2항에 기재된 담당부서로 연락해 주시기 바랍니다.</p><p id="6918c04b-0758-4289-9c60-4fdb26c388af" class="">③ 개인정보 보호법 제35조(개인정보의 열람), 제36조(개인정보의 정정·삭제), 제37조(개인정보의 처리정지 등)의 규정에 의한 요구에 대하여 공공기관의 장이 행한 처분 또는 부작위로 인하여 권리 또는 이익의 침해를 받은 자는 행정심판법이 정하는 바에 따라 행정심판을 청구할 수 있습니다.</p><ul id="edb2170b-ceec-492d-a5a7-b24664d2cc49" class="bulleted-list"><li style="list-style-type:disc">중앙행정심판위원회 : (국번없이) 110 (www.simpan.go.kr)</li></ul><p id="3cf9a0e6-786d-4ee3-abdd-d87ce8c5185a" class="">
</p><h3 id="65b649cc-130b-461a-9480-d92d51700b53" class=""><strong>제13조 (개인정보 처리방침의 변경)</strong></h3><p id="7169da01-e713-40f4-9ba9-7ce529963b13" class="">회사는 법률이나 서비스의 변경사항을 반영하기 위한 목적 등으로 개인정보처리방침을 수정할 수 있습니다. 개인정보처리방침이 변경되는 경우 회사는 변경 사항 최소한 효력발생일 7일 이전에 게시하며, 변경된 개인정보처리방침은 기재된 효력발생일에 그 효력이 발생합니다. 다만, 수집하는 개인정보의 항목, 이용목적의 변경 등과 같이 이용자 권리의 중대한 변경이 발생할 때에는 최소 30일 전에 미리 알려드리겠습니다.</p><p id="c578369c-4ab3-41ec-9a60-bbbe5bb6bfa5" class="">
</p><p id="a5f33642-295f-461a-b395-18e818506dc4" class=""><strong>이 개정 약관은 2023년 1월 18일(수)부터 적용됩니다.</strong></p><p id="5c8fec9a-3c82-41a9-aa8f-84d7d7862562" class=""><strong>뤼튼 개인정보 처리방침은 2022년 9월 1일(목)부터 적용됩니다.</strong></p><p id="a8cf4628-c1e1-43eb-8d4b-ebd71b459e4f" class="">
</p><p id="824ad84b-0c8c-44e8-9931-c5db7ee5781d" class="">본 처리방침에 대한 저작권은 주식회사 뤼튼테크놀로지스에 있으며 무단 복제, 배포, 전송, 기타 저작권 침해행위를 엄금 합니다.</p></div></article></body></html>
`;
