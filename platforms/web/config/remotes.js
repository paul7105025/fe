const remotesComponentsName = [
  // ["login", process.env.MODULE_FEDERATION_LOGIN_PATH],
  ["toolEditor", process.env.MODULE_FEDERATION_EDITOR_PATH],
  // ["toolImage", process.env.MODULE_FEDERATION_IMAGE_PATH],
  // ["toolSingle", process.env.MODULE_FEDERATION_SINGLE_PATH],
  // ["toolStep", process.env.MODULE_FEDERATION_STEP_PATH],
  // ["toolBundle", process.env.MODULE_FEDERATION_BUNDLE_PATH],
  // ["toolChat", process.env.MODULE_FEDERATION_CHAT_PATH]
];

/* You only need to edit the above variables. */
/* ########################################## */

const _remoteMap = new Map();
remotesComponentsName.forEach((v) => {
  _remoteMap.set(v[0], `${v[0]}@${v[1]}/remoteEntry.js`);
})
const remotes = Object.fromEntries(_remoteMap);
module.exports = remotes;