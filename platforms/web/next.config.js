/** @type {import('next').NextConfig} */
const { NextFederationPlugin } = require("@module-federation/nextjs-mf");
const remotes = require("./config/remotes.js");
const svgrloader = require("@wrtn/loaderconfig");
const { dependencies } = require("./package.json");
const { withSentryConfig } = require("@sentry/nextjs");
// console.log(svgrloader)

// console.log("gu")
// const withTM = require('next-transpile-modules')(['@wrtn/ui']);

// const packages = []
// const localDependencies = Object.keys(dependencies).filter(v => v.startsWith("@wrtn/"))
// localDependencies.forEach(v => packages.push(path.dirname(require.resolve(v))))

const nextConfig = {
  async redirects() {
    return [
      {
        source: "/app/:slug*",
        destination: "/",
        permanent: true,
      },
    ];
  },
  output: 'standalone',
  transpilePackages: ["@wrtn/ui"],
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },
  webpack: (config, context) => {
    const { isServer } = context;
    // console.log(svgrloa/der)
    config.module.rules.push(svgrloader);
    // config.output.publicPath='auto'

    // config.optimization
    config.plugins.push(
      new NextFederationPlugin({
        name: "app",
        filename: "remoteEntry.js",
        remotes: {
          $editor: `toolEditor@${process.env.MODULE_FEDERATION_EDITOR_PATH}/remoteEntry.js`,
          $tool: `toolSingle@${process.env.MODULE_FEDERATION_SINGLE_PATH}/remoteEntry.js`,
          $chat: `toolChat@${process.env.MODULE_FEDERATION_CHAT_PATH}/remoteEntry.js`,
        },
        shared: {
          recoil: {
            eager: true,
            singleton: true,
          },
          "styled-component": {
            singleton: true,
          },
          "react-alert": {
            eager: true,
            singleton: true,
          },
          "mixpanel-browser": {
            eager: true,
            singleton: true,
          },
          "react-google-recaptcha-v3": {
            eager: true,
            singleton: true,
          },
        },
      })
    );

    return config;
  },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'wrtn-image-user-output.s3.ap-northeast-2.amazonaws.com',
        port: '',
        pathname: '**',
      },
    ],
  },
};

module.exports = withSentryConfig(nextConfig);

