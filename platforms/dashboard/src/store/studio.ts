import { atom } from "recoil";


export const currentCategoryState = atom<string>({
  key: '@wrtn/dashboard/currentCategoryState',
  default: '전체'
});

export const currentTabState = atom<string>({
  key: '@wrtn/dashbaord/currentTabState',
  default: '툴'
});


export const currentSearchState = atom<string | null>({
  key: '@wrtn/dashboard/currentSearchState',
  default: null
})
