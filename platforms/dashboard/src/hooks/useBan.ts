import React from "react";
import { useAlert } from "react-alert";
import { useRecoilState } from "recoil";
import { mutate } from "swr";
import { dashboardAPI } from "../services";

const useBan = () => {
  const { banList, mutate } = dashboardAPI.useGetBanUser();
  const alert = useAlert();

  const ban = React.useCallback(async (userId: string, isBanned: boolean) => {
    if (isBanned) {
      const res = await dashboardAPI.deleteUserBan(userId);
      if (res?.status !== 200) {
        alert.removeAll();
        alert.show("차단 해제하는데 문제가 발생했습니다.")
        return ;
      }
      mutate(banList.filter((v: any) => v._id !== userId));
      alert.removeAll();
      alert.show("차단을 해제했습니다");
    } else {
      const res = await dashboardAPI.postUserBan(userId)
      if (res?.status !== 201) {
        alert.removeAll();
        alert.show("차단하는 과정에 문제가 발생했습니다.")
        return ;
      }
      mutate(banList.concat([{
        _id: userId
      }]));
      alert.removeAll();
      alert.show("해당 유저를 차단했습니다.");
    }
  }, []);

  const checkIsBanned = React.useCallback(
    (id: string) => banList.find((v: any) => v._id === id),
    [banList]
  );

  return { banList, ban, checkIsBanned };
};

export default useBan;
