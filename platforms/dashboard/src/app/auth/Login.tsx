'use client'

import { colors, FlexWrapper } from "@wrtn/ui";
import styled from 'styled-components';
import { CredentialResponse, GoogleLogin } from "@react-oauth/google";
import { postAuthGoogle } from "@wrtn/dashboard/services/auth";
import { syncHeader } from "@wrtn/dashboard/services";
import { useRouter } from "next/navigation";

import { useAlert } from 'react-alert';
import { setCookie } from "cookies-next";

export const Login = () => {
  const router = useRouter();
  const alert = useAlert();

  const handleGoogleLoginSuccess = async ({ credential }: CredentialResponse) => {
    const res = await postAuthGoogle({ token: credential || "" });
    //TODO: API요청 후 처리에 대한 모듈화 필요
    if (res?.status !== 201) {
      alert.removeAll();
      alert.show('다시 시도해주세요.')
    }
    syncHeader({
      key: "Authorization",
      value: `Bearer ${res.data.accessToken}`,
    });

    setCookie('dashboard_access_token', res.data.accessToken);

    router.push('/studio')
  };

  const onLoginGoogleFailure = () => {};

  return (
    <Wrapper>
      <Title>Wrtn Dashboard</Title>
      <GoogleLogin
        useOneTap
        onSuccess={handleGoogleLoginSuccess}
        onError={onLoginGoogleFailure}
        width={"320"}
      />
    </Wrapper>
  )
}

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100vh;
  flex-direction: column;
  gap: 50px;
`;

const Title = styled.h1`
  color: ${colors.GRAY_90};
`;
