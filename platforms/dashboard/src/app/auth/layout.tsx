'use client';

import { syncHeader } from '@wrtn/dashboard/services';
import { cookies } from 'next/headers';
import React from 'react';

const Layout = ({
  children
}: {
  children: React.ReactNode
}) => {
  return (
    <>
      {children}
    </>
  )
}

export default Layout;