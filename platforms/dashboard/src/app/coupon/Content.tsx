'use client';

import { couponType } from "@wrtn/core";
import CouponCreateModal from "@wrtn/dashboard/components/CouponCreateModal";
import CouponEditModal from "@wrtn/dashboard/components/CouponEditModal";
import { CouponItem } from "@wrtn/dashboard/components/CouponItem";
import { dashboardAPI } from "@wrtn/dashboard/services";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import dayjs from "dayjs";
import React from "react";
import styled from "styled-components";

const useCouponModal = () => {
  const [selectedCoupon, setSelectedCoupon] = React.useState<couponType | null>(
    null
  );
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [isCreate, setIsCreate] = React.useState(false);
  const [isEditing, setIsEditing] = React.useState(false);

  const openCreateModal = () => {
    setSelectedCoupon({
      name: "",
      type: "discount",
      description: "",
      dueDate: dayjs().add(1, 'month').toISOString(),

      quantity: 1,
      count: 1,
    });
    setIsModalOpen(true);
    setIsCreate(true);
  };

  const openEditModal = (item: couponType) => {
    setSelectedCoupon({
      ...item,
      dueDate: item.dueDate.slice(0, 16),
    });
    setIsModalOpen(true);
    setIsCreate(false);
  };

  const closeModal = () => {
    setSelectedCoupon(null);
    setIsModalOpen(false);
    setIsEditing(false);
  };

  const isOpenCreateModal = React.useMemo(() => {
    return isModalOpen && selectedCoupon && isCreate;
  }, [isModalOpen, selectedCoupon, isCreate]);

  const isOpenEditModal = React.useMemo(() => {
    return isModalOpen && selectedCoupon && !isCreate;
  }, [isModalOpen, selectedCoupon, isCreate]);

  const toggleEdit = () => {
    setIsEditing(true);
  }

  return {
    openCreateModal,
    openEditModal,
    closeModal,
    isOpenCreateModal,
    isOpenEditModal,
    toggleEdit,
    selectedCoupon
  }
}

export const Content = () => {

  const { couponList } = dashboardAPI.useGetCoupon();

  const {
    openCreateModal,
    openEditModal,
    closeModal,
    isOpenEditModal,
    isOpenCreateModal,
    toggleEdit,
    selectedCoupon
  } = useCouponModal();

  const handleClickCreate = () => {
    openCreateModal();
  }

  const handleClickEdit = (v: any) => {
    openEditModal(v);
  }

  const closeWithRefresh = () => {
    closeModal();
  }
  return (
    <Wrapper column>
      <RowWrapper>
        <CreateButton onClick={handleClickCreate}>쿠폰 생성하기</CreateButton>
      </RowWrapper>
      <ScrollWrapper>
        <CouponItem />
        {couponList.map((item: any, index: number) => {
          return (
            <CouponItem key={item._id} item={item} openModal={handleClickEdit} />
          );
        })}
      </ScrollWrapper>
      {isOpenCreateModal && (
        <ModalPortal onClose={closeModal}>
          <CouponCreateModal
            indexing={couponList?.length || 0}
            // @ts-ignore
            item={selectedCoupon}
            onClose={closeModal}
            onCloseWithRefresh={closeWithRefresh}
            toggleEdit={toggleEdit}
          />
        </ModalPortal>
      )}
      {isOpenEditModal && (
        <ModalPortal onClose={closeModal}>
          <CouponEditModal
            // @ts-ignore
            item={selectedCoupon}
            onClose={closeModal}
            onCloseWithRefresh={closeWithRefresh}
            toggleEdit={toggleEdit}
          />
        </ModalPortal>
      )}
    </Wrapper>
  )
}



const Wrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
  height: 100%;
  flex-direction: column;
  justify-content: flex-start;
  padding: 20px 10px 0px;
  gap: 10px;
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 0px 20px;
  gap: 30px;
`;

const CreateButton = styled(FlexButton)`
  white-space: nowrap;
  min-width: 130px;
  padding: 15px 10px;
  border-radius: 5px;
  background-color: ${colors.FEEDBACK_GREEN};

  ${typo({
    size: "14px",
    weight: "500",
    color: colors.WHITE,
  })}

  cursor: pointer;
`;

const ScrollWrapper = styled(FlexWrapper)`

  width: 100%;

  padding: 20px;

  /* flex-direction: column; */
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
  overflow-y: auto;
`;
