import StoreCard from "@wrtn/dashboard/components/StoreCard";
import { useAdminTool } from "@wrtn/dashboard/services";
import { useAdminToolRecommend } from "@wrtn/dashboard/services/useAdminToolRecommend";
import { currentCategoryState, currentSearchState } from "@wrtn/dashboard/store/studio";
import { newColors, newTypo } from "@wrtn/ui";
import { useRecoilValue } from "recoil";
import styled from "styled-components";
import { RecommendTool, Tool } from "../types/tool";
import { deleteAdminTool, postAdminToolRecommendCancel, postAdminToolRecommendRegister, putAdminToolPostingStart, putAdminToolPostingStop } from "@wrtn/dashboard/services/admin";
import { useAlert } from "react-alert";
import { useIntersect, useModalOpen } from "@wrtn/core";
import React from "react";
import { ToolDeleteCheckModal } from "@wrtn/dashboard/components/ToolDeleteCheckModal";


interface StudioContentProps {
  // onClickRecommend: (tool: Tool, isRecommend: boolean) => void;
  // onClickRemove: (tool: Tool) => void;
  // onClickUpload: (tool: Tool) => void;
  page: number;
  selectedDeleteTool?: Tool;
  setSelectedDeleteTool: Function;
  isLastPage: boolean;
  loadPage: Function
}

export const StudioContent = ({
  // onClickRecommend,
  // onClickRemove,
  // onClickUpload,
  page,
  selectedDeleteTool,
  setSelectedDeleteTool,
  isLastPage,
  loadPage,
}: StudioContentProps) => {

  const currentCategory = useRecoilValue(currentCategoryState);
  const currentSearch = useRecoilValue(currentSearchState);
  const [alreadyLoad, setAlreadyLoad] = React.useState(false);

  const { toolList, maxPage, isLoading, mutate } = useAdminTool({
    page: page,
    limit: 24,
    category: currentCategory,
    title: currentSearch || "",
  });

  const { toolRecommendList, mutate: mutateToolRecommend } = useAdminToolRecommend();

  const alert = useAlert();
  // const openModal = useModalOpen({ modalId: 'deleteStudioTool' });
  const [openModal, setOpenModal] = React.useState(false);

  const handleClickRecommend = async (v: Tool, isRecommend: boolean) => {
    if (isRecommend) {
      const res = await postAdminToolRecommendCancel(v._id);
      if (res.status !== 201) {
        alert.removeAll();
        alert.show("툴 추천을 취소하는 과정에 오류가 발생했습니다.")
        return ;
      }
      mutateToolRecommend({
          data: toolRecommendList.filter((tool: RecommendTool) => tool.toolId._id !== v._id)
      })
    } else {
      const res = await postAdminToolRecommendRegister(v._id);
      if (res.status !== 201) {
        alert.removeAll();
        alert.show("툴 추천을 등록하는 과정에 오류가 발생했습니다.")
        return ;
      }
      mutateToolRecommend({
        data: [...toolRecommendList, {
          _id: v._id,
          toolId: {
            _id: v._id
          },
          createdAt: "",
          updatedAt: ""
        }]
    })
    }
  }

  const handleClickUpload = async (v: Tool) => {
    if (v.isTemporarySave) {
      const res = await putAdminToolPostingStart(v._id);

      if (res?.status !== 201) {
        alert.removeAll();
        alert.show("툴을 공개하는 과정에 오류가 발생했습니다.")
        return ;
      }
      mutate({
        data: {
          toolList: toolList.map((tool: Tool) => {
            if (tool._id === v._id) {
              return ({
                ...tool,
                isTemporarySave: true
              });
            }
            return tool;
          }),
          maxPage,
        },
      });
    } else { 
      const res = await putAdminToolPostingStop(v._id);
      if (res?.status !== 200) {
        alert.removeAll();
        alert.show("툴을 비공개하는 과정에 오류가 발생했습니다.")
        return ;
      }
      mutate({
        data: {
          toolList: toolList.map((tool: Tool) => {
            if (tool._id === v._id) {
              return ({
                ...tool,
                isTemporarySave: false
              });
            }
            return tool;
          }),
          maxPage,
        },
      });
    }
  }

  const handleClickRemove = (v: Tool) => {
    // setIsOpenDeleteModal(true)
    setOpenModal(true)
    setSelectedDeleteTool(v);
  }

  const handleDelete = async (v: Tool) => {
    const res = await deleteAdminTool(v._id);
    if (res.status !== 200) {
      alert.removeAll();
      alert.show("툴을 삭제하는 과정에 오류가 발생했습니다.")
      return ;
    }
    mutate({
      data: {
        toolList: toolList.filter((tool: Tool) => tool._id !== v._id),
        maxPage,
      }
    })
    mutateToolRecommend({
      data: toolRecommendList.filter((tool: RecommendTool) => tool.toolId._id !== v._id)
    })
  }

  const ref = useIntersect(async (entry, observer) => {
    observer.unobserve(entry.target);
    if (isLastPage && !isLoading && !alreadyLoad) {
      if (toolList.length !== 0) loadPage();
      setAlreadyLoad(true)
    }
  });

  return (
    <>
    { (currentCategory === "추천" ? toolRecommendList.map((v : RecommendTool) => v.toolId) : toolList).map((v: Tool) => {
        const isRecommend = toolRecommendList.find((rec: RecommendTool) => rec.toolId._id === v._id);

        return (
          <CardWrapper key={v._id}>
            <StoreCard
              icon={v.icon}
              type={v.type}
              title={v.title}
              description={v.description}
              author={v.userName || ""}
              createdAt={v.createdAt}
              viewCount={v.views}
              genCount={v.usedNum}
              exportCount={0}
              favoriteCount={v.likes}
              isTemporarySave={v.isTemporarySave}
              isDeleted={v.isDeleted}
              isRecommend={isRecommend}
              onClick={() => { }}
            />
            <BottomWrapper>
              <RecommendButton onClick={() => { handleClickRecommend(v, isRecommend) }}>
                { isRecommend ? "- 추천 툴 해제" : "+ 추천 툴 등록"}
              </RecommendButton>
              <ButtonWrapper>
                <UploadButton isActive={!v.isTemporarySave} onClick={() => handleClickUpload(v)}>{ v.isTemporarySave ? "게시 활성화" : "게시 중단" }</UploadButton>
                <RemoveButton onClick={() => handleClickRemove(v)}>- 삭제</RemoveButton>
              </ButtonWrapper>
            </BottomWrapper>
          </CardWrapper>
        )
      })
    }
    { isLastPage && 
      <Target ref={ref} />
    }
    { openModal ?
      <ToolDeleteCheckModal
        onClose={() => {
          setOpenModal(false)
          setSelectedDeleteTool(null)
        }}
        handleDelete={() => {
          if (selectedDeleteTool) handleDelete(selectedDeleteTool);
          setSelectedDeleteTool(null)

          setOpenModal(false)
        }}
      /> : <></>
    } 
    </>
  )
}

const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const BottomWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-top: 15px;
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 11px;
`;

const RemoveButton = styled.div`
  cursor: pointer;
  padding: 4px 8px;
  ${newTypo('body-16-semi')};
  color: ${newColors.WHITE};
  background: #FF6565;
  border-radius: 5px;
`;

const UploadButton = styled.div<{ isActive: boolean }>`
  cursor: pointer;
  padding: 4px 8px;
  ${newTypo('body-16-semi')};
  color: ${newColors.WHITE};
  background: ${props => props.isActive ? "#FF6565" : "#7CCF8A"};
  border-radius: 5px;
`;

const RecommendButton = styled.div`
  cursor: pointer;
  ${newTypo('body-16-semi')};
  color: ${newColors.WHITE};
`;

const Target = styled.div`
  width: 100%;
  height: 10px;
`;
