'use client'
import { useState, useMemo } from 'react';
import { SearchInput } from "@wrtn/ui/components/SearchInput";
import { useSetRecoilState } from 'recoil';
import { currentSearchState } from '@wrtn/dashboard/store/studio';
import { debounce } from "lodash"
import { useNonInitialEffect } from '@wrtn/core';


const Search = () => {
  const [input, setInput] = useState("");
  const setCurrentSearch = useSetRecoilState(currentSearchState);

  const inputSync = useMemo(() => debounce((input: string) => {
    // setCurrentStorePage(1);
    setCurrentSearch(input);
  }, 1000), []);

  useNonInitialEffect(() => {
    inputSync(input);
  }, [input]);

  return (
    <>
      <SearchInput 
        placeholder={"툴 / 봇 이름 & 유저 아이디 검색"}
        value={input || ""}
        onChange={(e) => setInput(e.target.value)}
        long={false}
      />
    </>
  )
}

export default Search;