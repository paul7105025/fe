'use client'

import { currentCategoryState } from '@wrtn/dashboard/store/studio';
import { newColors } from '@wrtn/ui';
import { useRecoilState } from 'recoil';
import styled from 'styled-components';

const categoryList = ["전체", "추천", "범용/일반", "블로그", "마케팅", "쇼핑몰", "학생", "업무용", "유튜브", "글짓기", "교육", "엔터테인먼트", "기타"]
const Category = () => {
  const [currentCategory, setCurrentCategory] = useRecoilState(currentCategoryState);

  return (
    <>
      {
        categoryList.map(v => (
          <Button 
            key={v}
            disabled={v === currentCategory}
            onClick={() => {
              setCurrentCategory(v);
            }}
          >{v}</Button>
        ))
      }
    </>
  )
}

export default Category;

const Button = styled.button`
  padding: 8px 18px;
  background: none;
  border: none;
  color: ${newColors.GRAY_200};
  font-weight: 500;
  font-size: 14px;
  &:hover {
    cursor: pointer;
  }
  &:disabled {
    border: 1px solid ${newColors.WHITE};
    border-radius: 40px;
    font-weight: 600;
    color: ${newColors.WHITE};
  }
`;