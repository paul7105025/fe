'use client'
import { StyledComponentsRegistry, AlertRegistry, GoogleOauthRegistry, RecoilRegistry } from "@wrtn/dashboard/lib/registry";
import AuthLayout from "@wrtn/dashboard/layout/AuthLayout";
import styled from 'styled-components';
import Content from "./Content";
import Header from "./Header";

const StudioPage = () => {
  return (
    <Wrapper>
      <Header />
      <ContentWrapper>
        <Content /> 
      </ContentWrapper>
    </Wrapper>
  )
}

export default StudioPage;

const Wrapper = styled.div`
  // padding-top: 40px 20px;
  margin: 0px auto;
  max-width: 1440px;
  height: calc(100% - 50px);
`;

const ContentWrapper = styled.div`
  overflow: scroll;
  height: calc(100vh - 180px);
`;