'use client';

import { currentTabState } from '@wrtn/dashboard/store/studio';
import { newColors, newTypo } from '@wrtn/ui';
import { useRecoilState, useRecoilStoreID } from 'recoil';
import styled from 'styled-components'
const Tab = () => {
  const [currentTab, setCurrentTab] = useRecoilState(currentTabState);

  return (
    <Wrapper>
      <Button onClick={() => { setCurrentTab('툴') }} isActive={currentTab === '툴'}>툴</Button>
      <Button onClick={() => { setCurrentTab('챗봇') }} isActive={currentTab === '챗봇'}>챗봇</Button>
      
    </Wrapper>
  )
}

export default Tab;

const Button = styled.div<{ isActive?: boolean }>`
  cursor: pointer;
  padding: 8px 16px;
  ${newTypo('content-20-bold')};

  color: ${props => props.isActive ? newColors.WHITE : newColors.GRAY_400};
  &:hover { 
    color: ${newColors.WHITE};
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 20px;
`;