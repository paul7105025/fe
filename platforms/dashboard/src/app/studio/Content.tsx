'use client';

import React from 'react'
import StoreCard from "@wrtn/dashboard/components/StoreCard";
import { useAdminTool } from "@wrtn/dashboard/services";
import { deleteAdminTool, postAdminToolRecommendCancel, postAdminToolRecommendRegister, putAdminToolPostingStart, putAdminToolPostingStop } from "@wrtn/dashboard/services/admin";
import styled from 'styled-components';
import { useAlert } from 'react-alert';
import { newColors, newTypo } from "@wrtn/ui";
import { RecoilRoot, useRecoilStoreID, useRecoilValue } from "recoil";
import { currentCategoryState, currentSearchState } from "@wrtn/dashboard/store/studio";
import { useAdminToolRecommend } from "@wrtn/dashboard/services/useAdminToolRecommend";
import { useModalOpen } from "@wrtn/core";
import { ToolDeleteCheckModal } from "@wrtn/dashboard/components/ToolDeleteCheckModal";
import { RecoilRegistry } from '@wrtn/dashboard/lib/registry';
import { StudioContent } from './StudioContent';
import { RecommendTool, Tool } from '../types/tool';


const Content = () => {
  const [selectDeleteTool, setSelectDeleteTool] = React.useState<Tool | null>(null);
  const alert = useAlert();
  
  const [page, setPage] = React.useState(1);
  const currentCategory = useRecoilValue(currentCategoryState);
  const currentSearch = useRecoilValue(currentSearchState);
  const openModal = useModalOpen({ modalId: 'deleteStudioTool' });
  // const [isOpenDeleteModal, setIsOpenDeleteModal] = React.useState<boolean>(false);
  // const { toolList, maxPage, isLoading, mutate } = useAdminTool({
  //   page: page,
  //   limit: 24,
  //   category: currentCategory,
  //   title: currentSearch || "",
  // });

  const { toolRecommendList, mutate: mutateToolRecommend } = useAdminToolRecommend();

  // const handleClickCard = (v: Tool) => {
  //   // navigate
  // }

  // const handleClickRecommend = async (v: Tool, isRecommend: boolean) => {
  //   if (isRecommend) {
  //     const res = await postAdminToolRecommendCancel(v._id);
  //     if (res.status !== 201) {
  //       alert.removeAll();
  //       alert.show("툴 추천을 취소하는 과정에 오류가 발생했습니다.")
  //       return ;
  //     }
  //     mutateToolRecommend({
  //         data: toolRecommendList.filter((tool: RecommendTool) => tool.toolId._id !== v._id)
  //     })
  //   } else {
  //     const res = await postAdminToolRecommendRegister(v._id);
  //     if (res.status !== 201) {
  //       alert.removeAll();
  //       alert.show("툴 추천을 등록하는 과정에 오류가 발생했습니다.")
  //       return ;
  //     }
  //     mutateToolRecommend({
  //       data: [...toolRecommendList, {
  //         _id: v._id,
  //         toolId: {
  //           _id: v._id
  //         },
  //         createdAt: "",
  //         updatedAt: ""
  //       }]
  //   })
  //   }
  // }

  // const handleClickUpload = async (v: Tool) => {
  //   if (v.isTemporarySave) {
  //     const res = await putAdminToolPostingStart(v._id);

  //     if (res?.status !== 201) {
  //       alert.removeAll();
  //       alert.show("툴을 공개하는 과정에 오류가 발생했습니다.")
  //       return ;
  //     }
  //     mutate({
  //       data: {
  //         toolList: toolList.map((tool: Tool) => {
  //           if (tool._id === v._id) {
  //             return ({
  //               ...tool,
  //               isTemporarySave: true
  //             });
  //           }
  //           return tool;
  //         }),
  //         maxPage,
  //       },
  //     });
  //   } else { 
  //     const res = await putAdminToolPostingStop(v._id);
  //     if (res?.status !== 200) {
  //       alert.removeAll();
  //       alert.show("툴을 비공개하는 과정에 오류가 발생했습니다.")
  //       return ;
  //     }
  //     mutate({
  //       data: {
  //         toolList: toolList.map((tool: Tool) => {
  //           if (tool._id === v._id) {
  //             return ({
  //               ...tool,
  //               isTemporarySave: false
  //             });
  //           }
  //           return tool;
  //         }),
  //         maxPage,
  //       },
  //     });
  //   }
  // }

  // const handleClickRemove = (v: Tool) => {
  //   // setIsOpenDeleteModal(true)
  //   openModal.open();
  //   setSelectDeleteTool(v);
  // }

  // const handleDelete = async (v: Tool) => {
  //   const res = await deleteAdminTool(v._id);
  //   if (res.status !== 200) {
  //     alert.removeAll();
  //     alert.show("툴을 삭제하는 과정에 오류가 발생했습니다.")
  //     return ;
  //   }
  //   mutate({
  //     data: {
  //       toolList: toolList.filter((tool: Tool) => tool._id !== v._id),
  //       maxPage,
  //     }
  //   })
  //   mutateToolRecommend({
  //     data: toolRecommendList.filter((tool: RecommendTool) => tool.toolId._id !== v._id)
  //   })
  // }

  const handleDelete = async (v: Tool, callback?: Function) => {
    const res = await deleteAdminTool(v._id);
    if (res.status !== 200) {
      alert.removeAll();
      alert.show("툴을 삭제하는 과정에 오류가 발생했습니다.")
      return ;
    }
    // mutate({
    //   data: {
    //     toolList: toolList.filter((tool: Tool) => tool._id !== v._id),
    //     maxPage,
    //   }
    // })
    mutateToolRecommend({
      data: toolRecommendList.filter((tool: RecommendTool) => tool.toolId._id !== v._id)
    })
  }

  const pages: React.ReactElement[] = [];
  for (let i = 1; i < page + 1; i++) {
    pages.push(
      <>
        <StudioContent
          key={i}
          page={i}
          selectedDeleteTool={selectDeleteTool || undefined}
          setSelectedDeleteTool={setSelectDeleteTool}
          loadPage={() => setPage(c => c + 1)}
          isLastPage={page === i}
          // onClickRecommend={ (v: Tool, isRecommend: boolean) => handleClickRecommend(v, isRecommend) }
          // onClickRemove={ (v: Tool) => handleClickRemove(v) }
          // onClickUpload={ (v: Tool) => handleClickUpload(v) }
        />
      </>
    )
  }
  
  return (
    <>
      <Grid>
        {
          pages
        }
      </Grid>
      {/* { openModal.isOpen ?
        <ToolDeleteCheckModal 
          onClose={() => {
            openModal.close();
            setSelectDeleteTool(null)
          }}
          handleDelete={() => {
            if (selectDeleteTool) handleDelete(selectDeleteTool);
            setSelectDeleteTool(null)

            openModal.close()
          }}
        /> : <></>
      }  */}
    </>
  )
}

export default Content;

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(342px, 1fr));
  gap: 24px 22px;

`;  
