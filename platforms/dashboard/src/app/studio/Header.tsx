'use client';

import styled from 'styled-components';
import Category from "./Category";
import Search from './Search';
import Tab from './Tab';


const Header = () => {
  return (
    <Wrapper>
      <UpperWrapper>
        <Tab />
        <Search />
      </UpperWrapper>
      <div>
        <Category />
      </div>
    </Wrapper>
  )
}

export default Header;

const UpperWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  gap
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 36px;
`;