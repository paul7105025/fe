import { DashboardLayout } from '@wrtn/dashboard/layout/DashboardLayout';

const Layout = ({ children }: { children: React.ReactNode}) => {
  return (
    <div>
      <DashboardLayout />
      <div style={{background: '#515465', height: '100%' }}>{children}</div>
    </div>

  )
}

export default Layout;