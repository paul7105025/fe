'use client';
import React from 'react';
import { dashboardAPI } from "@wrtn/dashboard/services";
import { useRouter } from "next/navigation";
import styled from 'styled-components'
import { useIntersect } from "@wrtn/core";
import useBan from '@wrtn/dashboard/hooks/useBan';
import UserCard from '@wrtn/dashboard/components/UserCard';

interface UserPageProps {
  page: number;
  search: string;
  isLastPage: boolean;
  loadPage: () => void;
}

export const UserPage = ({ page, search, isLastPage, loadPage }: UserPageProps) => {
  const router = useRouter();
  const { userList, isLoading } = dashboardAPI.useGetUser({ page, q: search});
  const [alreadyLoad, setAlreadyLoad] = React.useState(false);

  const ref = useIntersect(async (entry, observer) => {
    observer.unobserve(entry.target);
    if (isLastPage && !isLoading && !alreadyLoad) {
      if (userList.length !== 0) loadPage();
      setAlreadyLoad(true)
    }
  });

  const { checkIsBanned, ban, banList } = useBan();

  return (
    <>
      {userList.map((user: any) => {
        return (
          <UserCard
            key={user._id}
            user={user}
            ban={ban}
            isBanned={checkIsBanned(user?._id)}
            move={() => router.push(`/user/${user._id}`)}
            isList={true}
          />
        )
      })}
      { isLastPage && 
        <Target ref={ref} />
      }
    </>
  )
}

const Target = styled.div`
  width: 100%;
  height: 10px;
`;
