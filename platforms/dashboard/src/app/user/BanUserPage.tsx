
'use client';
import React from 'react';
import { useRouter } from "next/navigation";
import UserCard from "@wrtn/dashboard/components/UserCard";
import useBan from '@wrtn/dashboard/hooks/useBan';


export const BanUserPage = () => {
  const router = useRouter();
  const { banList, ban } = useBan();

  return (
    <>
      { banList.map((user: any) => (
        <UserCard 
          key={user._id}
          user={user}
          ban={ban}
          isBanned={true}
          move={() => router.push(`/user/${user._id}`)}
          isList={true}  
        />
      ))
      }
    </>
  );
}