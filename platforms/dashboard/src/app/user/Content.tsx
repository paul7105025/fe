'use client';
import React from "react";
import styled from "styled-components";
// import SearchInput from "../components/SearchInput";
import { useIntersect } from "@wrtn/core";
// import { getUser } from "../services/user";
// import UserCard from "../components/UserCard";
// import _ from "lodash";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
// import useBan from "../hooks/useBan";
import { useRouter } from "next/navigation";
import { dashboardAPI } from "@wrtn/dashboard/services";
import { UserPage } from "./UserPage";
import SearchInput from "@wrtn/dashboard/components/SearchInput";
import { BanUserPage } from "./BanUserPage";

const ALL_USER = "all";
const BANNED_USER = "banned";

const Content = () => {
  const router = useRouter();

  const [search, setSearch] = React.useState("");
  const [page, setPage] = React.useState(1);
  const [stop, setStop] = React.useState(false);

  const [mode, setMode] = React.useState(ALL_USER);

  const pages: React.ReactElement[] = [];

  for (let i = 1; i < page + 1; i++) {
    pages.push(
      <>
        <UserPage key={i} page={i} search={search} isLastPage={page === i} loadPage={() => setPage(c => c+1)}/>
      </>
    )
  }
  const handleSearch = () => {
    setPage(1);
    setStop(false);
    setMode(ALL_USER);
  };

  const changeMode = () => {
    if (mode === ALL_USER) {
      setMode(BANNED_USER);
    } else {
      setMode(ALL_USER);
    }
  };

  // const renderData = React.useMemo(() => {
  //   if (mode === ALL_USER) {
  //     return data;
  //   } else {
  //     return bannedList;
  //   }
  // }, [data, bannedList, mode]);

  React.useEffect(() => {
    if (mode === ALL_USER) {
      setPage(1);
      setStop(false);
    } else {
      // updateList();
    }
  }, [mode]);


  return (
    <Wrapper>
      <RowWrapper>
        <SearchInput
          search={search}
          setSearch={setSearch}
          placeholder="이름, 이메일, 직장, 전화번호"
          onKeyDown={(e: any) => {
            if (e.key === "Enter") {
              handleSearch();
            }
          }}
        />
        <ToggleButton mode={mode} onClick={changeMode}>
          {mode === ALL_USER ? "전체 유저" : "차단된 유저"}
        </ToggleButton>
      </RowWrapper>
      <ScrollWrapper> 
        { mode === ALL_USER && 
          pages 
        }
        { mode === BANNED_USER &&
          <BanUserPage />
        }
      </ScrollWrapper>
    </Wrapper>
  );
};

export default Content;


const RowWrapper = styled(FlexWrapper)`
width: 100%;
padding: 0px 20px;
gap: 30px;
`;

const Wrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
  height: calc(100% - 100px);
  flex-direction: column;
  justify-content: flex-start;
  padding: 20px 10px 0px;
  gap: 10px;
`;

const ToggleButton = styled(FlexButton)<{ mode: string }>`
  white-space: nowrap;
  min-width: 130px;
  padding: 15px 10px;
  border-radius: 5px;
  background-color: ${({ mode }) =>
    mode === ALL_USER ? colors.FEEDBACK_GREEN : colors.FEEDBACK_RED};

  ${typo({
    size: "14px",
    weight: "500",
    color: colors.WHITE,
  })}

  cursor: pointer;
`;

const ScrollWrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
  padding: 20px 0px 50px;

  flex-direction: column;
  justify-content: flex-start;
  overflow-y: auto;

  gap: 15px;
`;