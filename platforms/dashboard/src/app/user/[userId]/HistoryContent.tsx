'use client';
import React from 'react';
import { dashboardAPI } from "@wrtn/dashboard/services";
import styled from 'styled-components'
import { useIntersect, userType } from "@wrtn/core";
import useBan from '@wrtn/dashboard/hooks/useBan';
import { HistoryCard } from '@wrtn/dashboard/components/HistoryCard';

interface HistoryContentProps {
  userId: string;
  page: number;
  user: userType;
  isLastPage: boolean;
  loadPage: () => void;
}

export const HistoryContent = ({ userId, page, user, isLastPage, loadPage }: HistoryContentProps) => {
  const { userHistoryList, isLoading } = dashboardAPI.useGetUserHistory({ userId, page, limit: 10 });
  const [alreadyLoad, setAlreadyLoad] = React.useState(false);

  const ref = useIntersect(async (entry, observer) => {
    observer.unobserve(entry.target);
    if (isLastPage && !isLoading && !alreadyLoad) {
      if (userHistoryList.length !== 0) loadPage();
      setAlreadyLoad(true)
    }
  });

  const { checkIsBanned, ban, banList } = useBan();

  return (
    <>
      {userHistoryList.map((data: any) => {
        return (
          <HistoryCard
            key={data._id}
            user={user}
            tool={data.tool}
            inputs={data.inputs}
            options={data.options || [""]}
            output={data.output}
            reportReason={data.reportReason || ""}
            createdAt={data.createdAt}
            isBanned={checkIsBanned(userId)}
            ban={ban}
          />
        )
      })}
      { isLastPage && 
        <Target ref={ref} />
      }
    </>
  )
}

const Target = styled.div`
  width: 100%;
  height: 10px;
`;
