'use client';

import UserCard from '@wrtn/dashboard/components/UserCard';
import useBan from '@wrtn/dashboard/hooks/useBan';
import { dashboardAPI } from '@wrtn/dashboard/services';
import { colors, FlexButton, FlexWrapper, typo } from '@wrtn/ui';
import { useParams, usePathname, useRouter } from 'next/navigation';
import React from 'react';
import styled, { keyframes } from 'styled-components';
import { HistoryContent } from './HistoryContent';
import { useDashboardUserLatestActive } from '@wrtn/dashboard/services/dashboard/useDashboardUserLatestActive';
import { useDashboardUserHistoryCount } from '@wrtn/dashboard/services/dashboard/useDashboardUserHistoryCount';

export const Content = () => {
  const router = useRouter();
  const params = useParams();
  const [page, setPage] = React.useState(1);

  const pages: React.ReactElement[] = [];

  const { user } = dashboardAPI.useGetUserId({ userId: params.userId })

  for (let i = 1; i < page + 1; i++) {
    pages.push(
      <>
        <HistoryContent key={i} page={i} user={user} userId={params.userId} isLastPage={page === i} loadPage={() => setPage(c => c + 1)} />
      </>
    )
  }

  const { ban, checkIsBanned } = useBan();
  const isBanned = checkIsBanned(params.userId);

  const handleClickWithdraw = () => {

  }

  const handleClickBan = () => {

  }


  console.log(params.userId)
  const { userLatestActive } = useDashboardUserLatestActive({
    userId: params.userId,
  })

  const { userHistoryCount } = useDashboardUserHistoryCount({
    userId: params.userId
  })

  return (
    <Wrapper>
      <RowWrapper style={{ maxWidth: 720, gap: 10, padding: "5px 0px" }}>
        <BlockButton isBanned={isBanned} onClick={handleClickBan}>
          {isBanned ? "차단 해제" : "차단"}
        </BlockButton>
        <WithDrawButton onClick={handleClickWithdraw}>회원 탈퇴</WithDrawButton>
      </RowWrapper>
      <RowWrapper justify="center">
        {user && (
          <UserCard
            key={params.userId}
            user={user}
            ban={ban}
            isBanned={isBanned}
            userWordCount={0}
            // userWordCount={userWordCount}
            userHistoryCount={userHistoryCount}
            userLatestActive={userLatestActive}
            isList={false}
          />
        )}
      </RowWrapper>
      <ScrollWrapper> 
        { pages }
      </ScrollWrapper>
    </Wrapper>
  )
}

const fadeout = keyframes`
  0%{
    opacity: 0;
  }
  100%{
    opacity: 1;
  }
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 0px 20px;
  gap: 30px;
`;

const Wrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
  height: calc(100% - 100px);
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding: 20px 10px 0px;
  gap: 10px;
`;

const ScrollWrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
  padding: 20px 0px 50px;

  flex-direction: column;
  justify-content: flex-start;
  overflow-y: auto;

  gap: 15px;
`;

const ControlButton = styled(FlexButton)`
  flex: 1;

  border-radius: 10px;
  padding: 10px 20px;

  ${typo({
    size: "12px",
    height: "12px",
    color: colors.WHITE,
    weight: "500",
  })}

  cursor: pointer;

  animation-duration: 0.8s;
  animation-name: ${fadeout};
`;

const BlockButton = styled(ControlButton)<{ isBanned: boolean }>`
  background-color: ${({ isBanned }) =>
    isBanned ? colors.FEEDBACK_GREEN : colors.FEEDBACK_RED};
`;

const WithDrawButton = styled(ControlButton)`
  background-color: ${colors.BLACK};
`;