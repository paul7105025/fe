export declare type Tool = {
  _id: string
  icon: string
  type: string
  title: string
  description: string
  userName?: string
  createdAt: string
  views: number
  usedNum: number
  likes: number
  isDeleted: boolean
  isTemporarySave: boolean
}

export declare type RecommendTool = {
  _id: string
  toolId: Tool
  createdAt: string
  updatedAt: string
}