import './global.css'
import AuthLayout from "../layout/AuthLayout";
import { StyledComponentsRegistry, AlertRegistry, GoogleOauthRegistry, RecoilRegistry } from "../lib/registry";

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="ko">
      <body style={{'height': '100vh', 'width': '100%', 'margin': '0px'}}>
        <RecoilRegistry>
          <AlertRegistry>
            <GoogleOauthRegistry>
              <StyledComponentsRegistry>
                <AuthLayout>
                  <div id="__next"/>
                  {children}
                </AuthLayout>
              </StyledComponentsRegistry>
            </GoogleOauthRegistry>
          </AlertRegistry>
        </RecoilRegistry>
      </body>
    </html>
  )
}

