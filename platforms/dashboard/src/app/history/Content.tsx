'use client'

import useBan from "@wrtn/dashboard/hooks/useBan";
import { dashboardAPI } from "@wrtn/dashboard/services";
import { useParams, useRouter } from "next/navigation";
import React from "react";
import { HistoryContent } from "./HistoryContent";
import styled from "styled-components";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui";
import SearchInput from "@wrtn/dashboard/components/SearchInput";


export const Content = () => {
  const router = useRouter();
  const params = useParams();
  const [page, setPage] = React.useState(1);

  const [searchToolId, setSearchToolId] = React.useState("");
  const [reported, setReported] = React.useState(false);

  const pages: React.ReactElement[] = [];

  for (let i = 1; i < page + 1; i++) {
    pages.push(
      <>
        <HistoryContent
          toolId={searchToolId} 
          reported={reported}
          key={i} 
          page={i} 
          isLastPage={page === i} 
          loadPage={() => setPage(c => c + 1)} 
        />
      </>
    )
  }

  const handleKeyDownSearch = (e: any) => {
    if (e.key === "Enter") {
      setSearchToolId(e.target.value);
      setPage(1);
    }
  }

  return (
    <Wrapper>
      <RowWrapper>
        <SearchInput
          search={searchToolId}
          setSearch={setSearchToolId}
          placeholder="툴 아이디를 툴 빌더에서 복사 후 엔터"
          onKeyDown={handleKeyDownSearch}
        />
        <ToggleButton
          reported={reported}
          onClick={() => setReported((res) => !res)}
        >
          {!reported ? "전체 히스토리" : "신고된 히스토리"}
        </ToggleButton>
      </RowWrapper>
      <ScrollWrapper>
        { pages }
      </ScrollWrapper>
    </Wrapper>
  )
}

const Wrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
  height: 100%;
  flex-direction: column;
  justify-content: flex-start;
  padding: 10px 0px 0px;
  gap: 10px;
`;

const ScrollWrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
  padding: 20px 10px 50px;

  flex-direction: column;
  justify-content: flex-start;
  overflow-y: auto;

  gap: 15px;
`;

const ErrorText = styled.div`
  color: white;
  font-size: 60px;
`;

const Loading = styled.div`
  color: white;
`;

const Target = styled.div`
  width: 100%;
  height: 10px;
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 0px 20px;
  gap: 30px;
`;

const ToggleButton = styled(FlexButton)<{ reported: boolean }>`
  white-space: nowrap;
  min-width: 130px;
  padding: 15px 10px;
  border-radius: 5px;
  background-color: ${({ reported }) =>
    !reported ? colors.FEEDBACK_GREEN : colors.FEEDBACK_RED};

  ${typo({
    size: "14px",
    weight: "500",
    color: colors.WHITE,
  })}

  cursor: pointer;
`;
