'use client';
import React from 'react';
import { dashboardAPI } from "@wrtn/dashboard/services";
import styled from 'styled-components'
import { useIntersect, userType } from "@wrtn/core";
import { HistoryCard } from '@wrtn/dashboard/components/HistoryCard';
import useBan from '@wrtn/dashboard/hooks/useBan';

interface HistoryContentProps {
  toolId: string;
  reported: boolean;
  page: number;
  isLastPage: boolean;
  loadPage: () => void;
}

export const HistoryContent = ({ toolId, page, isLastPage, loadPage, reported }: HistoryContentProps) => {
  const { historyList, isLoading } = dashboardAPI.useGetHistory({ page, limit: 10, reported, toolId });
  const [alreadyLoad, setAlreadyLoad] = React.useState(false);

  const ref = useIntersect(async (entry, observer) => {
    observer.unobserve(entry.target);
    if (isLastPage && !isLoading && !alreadyLoad) {
      if (historyList.length !== 0) loadPage();
      setAlreadyLoad(true)
    }
  });

  const { checkIsBanned, ban, banList } = useBan();

  return (
    <>
      {historyList.map((data: any) => {
        return (
          <HistoryCard
            key={data._id}
            user={data.user}
            tool={data.tool}
            inputs={data.inputs}
            options={data.options || [""]}
            output={data.output}
            reportReason={data.reportReason || ""}
            createdAt={data.createdAt}
            isBanned={checkIsBanned(data.user._id)}
            ban={ban}
          />
        )
      })}
      { isLastPage && 
        <Target ref={ref} />
      }
    </>
  )
}

const Target = styled.div`
  width: 100%;
  height: 10px;
`;
