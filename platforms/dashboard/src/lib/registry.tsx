'use client';
 
import React, { useState } from 'react';
import { useServerInsertedHTML } from 'next/navigation';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
import { RecoilRoot, useRecoilBridgeAcrossReactRoots_UNSTABLE, useRecoilStoreID } from 'recoil';
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import { ToastMessage } from "@wrtn/ui/components/ToastMessage";
import { GoogleOAuthProvider } from "@react-oauth/google";

const googleClientId = process.env.NEXT_PUBLIC_URL_GOOGLE_CLIENT_ID || "";

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};
 
export function StyledComponentsRegistry({
  children,
}: {
  children: React.ReactNode;
}) {
  // Only create stylesheet once with lazy initial state
  // x-ref: https://reactjs.org/docs/hooks-reference.html#lazy-initial-state
  const [styledComponentsStyleSheet] = useState(() => new ServerStyleSheet());
 
  useServerInsertedHTML(() => {
    const styles = styledComponentsStyleSheet.getStyleElement();
    // @ts-ignore
    styledComponentsStyleSheet.instance.clearTag();
    return <>{styles}</>;
  });
 
  if (typeof window !== 'undefined') return <>{children}</>;
 
  return (
    <StyleSheetManager sheet={styledComponentsStyleSheet.instance}>
      {children}
    </StyleSheetManager>
  );
}

export const RecoilRegistry = ({
  children
}: {
  children: React.ReactNode;
}) => {
  const id = useRecoilStoreID();
  console.log(id)
  return (
    <RecoilRoot>
      {children}
    </RecoilRoot>
  )
}

export const AlertRegistry = ({
  children
}: {
  children: React.ReactNode;
}) => {
  return (
    <AlertProvider template={ToastMessage} {...alertOptions}>
      {children}
    </AlertProvider>
  )
}

export const GoogleOauthRegistry = ({
  children
}: {
  children: React.ReactNode;
}) => {
  return (
    <GoogleOAuthProvider clientId={googleClientId}>
      {children}
    </GoogleOAuthProvider>
  )
}