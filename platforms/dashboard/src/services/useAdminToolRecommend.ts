import useSWR from 'swr';
import { useStoreFetcher } from '.';



export const useAdminToolRecommend = () => {
  const { defaultFetcher } = useStoreFetcher();
  const { data, isValidating, isLoading, mutate } = useSWR(`/admin/tool/recommend/all`, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: []
    }
  });
  return ({
    toolRecommendList: data.data,
    mutate,
    isValidating,
    isLoading
  })
}
