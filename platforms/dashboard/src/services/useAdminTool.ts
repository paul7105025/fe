
import useSWR from 'swr';
import { useStoreFetcher } from '.';


interface useAdminToolProps {
  page: number
  limit: number
  title?: string
  category?: string
}


export const useAdminTool = ({
  page,
  limit,
  title,
  category,
}: useAdminToolProps ) => {
  const { defaultFetcher } = useStoreFetcher();
  const _category = category === "전체" ? null : category;
  const query = `?${page ? 'page=' + page + '&' : ''}${limit ? 'limit=' + limit + '&' : ''}${title ? 'title=' + title + '&' : ''}${_category ? 'category=' + _category + '&' : ''}`;
  const { data, isValidating, isLoading, mutate } = useSWR(`/admin/tool/` + query, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: {
        toolList: [],
        maxPage: 0,
      },
    }
  });
  return ({
    toolList: data.data.toolList,
    maxPage: data.data.maxPage,
    mutate,
    isValidating,
    isLoading
  })
}
