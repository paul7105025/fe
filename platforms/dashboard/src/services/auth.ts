import { authInstance } from ".";

export const postAuthGoogle = async ({ token }: { token: string }) => {
  const url = `/auth/google`;
  return await authInstance
    .post(url, {
      authorizationToken: token,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};