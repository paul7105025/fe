import { dashboardInstance } from "..";

export const postDashboardUserBan = async (userId: string) => {
  const url = `/user/ban/${userId}`;
  return await dashboardInstance
    .post(url)
    .then(res => res)
    .catch(err => err?.response || err)
}

export const deleteDashboardUserBan = async (userId: string) => {
  const url = `/user/ban/${userId}`;
  return await dashboardInstance
    .delete(url)
    .then(res => res)
    .catch(err => err?.response || err)
}

interface postDashboardUserRewardByIdProps{
  userId: string
  message: string
  count: string
}

export const postDashboardUserRewardById = async ({ userId, message, count }: postDashboardUserRewardByIdProps) => {
  const url = `/user/${userId}/reward`;
  return await dashboardInstance
    .post(url, {
      message, count : Number(count)
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
interface postDashboardUserRefundByIdProps{
  userId: string
  message: string
}

export const postDashboardUserRefundById = async ({ userId, message }: postDashboardUserRefundByIdProps) => {
  const url = `/user/${userId}/refund`;
  return await dashboardInstance
  .post(url, {
    message
  })
  .then((res) => {
    return res;
  })
  .catch((err) => {
    return err?.response || err;
  })
}