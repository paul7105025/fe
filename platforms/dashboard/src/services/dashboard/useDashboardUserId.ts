import { useStoreFetcher } from "..";
import useSWR from 'swr';

interface useDashboardUserIdProps {
  userId: string;
}

export const useDashboardUserId = ({
  userId
}: useDashboardUserIdProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/user/${userId}`;
  const params = {
  };
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: null
  });

  return ({
    user: data,
    mutate,
    isValidating,
    isLoading
  })
  
}