import useSWR from 'swr';
import { useStoreFetcher } from '..';

interface useDashboardUserHistoryCountProps {
  userId: string
}
export const useDashboardUserHistoryCount = ({
  userId
}: useDashboardUserHistoryCountProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/user/${userId}/history/count`;
  const { data, isValidating, isLoading, mutate } = useSWR([url], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: 0,
    }
  })

  return ({
    userHistoryCount: data.toString() as number,
  })
}