import { useStoreFetcher } from "..";
import useSWR from 'swr';

interface useDashboardCouponUserProps {
  couponId: string
}

export const useDashboardCouponUser = ({
  couponId
}: useDashboardCouponUserProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/coupon/${couponId}/user-coupon`;
  const params = {
  };
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: []
  });

  return ({
    couponUserList: data,
    mutate,
    isValidating,
    isLoading
  })
  
}