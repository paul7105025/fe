import { useStoreFetcher } from "..";
import useSWR from 'swr';

interface useDashboardCouponIdProps {
  couponId: string
}

export const useDashboardCouponId = ({
  couponId
}: useDashboardCouponIdProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/coupon/${couponId}`;
  const params = {
  };
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: null
  });

  return ({
    coupon: data,
    mutate,
    isValidating,
    isLoading
  })
  
}