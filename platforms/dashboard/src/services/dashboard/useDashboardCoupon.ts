import { useStoreFetcher } from "..";
import useSWR from 'swr';

export const useDashboardCoupon = () => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/coupon`;
  const params = {
  };
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: []
  });

  return ({
    couponList: data,
    mutate,
    isValidating,
    isLoading
  })
  
}