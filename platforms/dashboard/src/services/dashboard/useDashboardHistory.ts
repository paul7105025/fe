import useSWR from 'swr';
import { useStoreFetcher } from "..";

interface useDashboardHistoryProps {
  page: number,
  limit?: number,
  reported?: boolean,
  toolId?: string,
}

export const useDashboardHistory = ({
  page,
  limit=10,
  reported,
  toolId
}: useDashboardHistoryProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/history`;
  const params = {
    ...(page ? { page: page } : { }),
    ...(limit ? { limit: limit } : { }),
    ...(reported ? { reported: reported ? 'y' : 'n' } : { }),   
    ...(toolId ? { toolId: toolId } : { }),   
  };
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: []
  });

  return ({
    historyList: data,
    mutate,
    isValidating,
    isLoading
  })
  
}