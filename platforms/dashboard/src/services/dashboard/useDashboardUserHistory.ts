// https://dashboard-api.wrtn.ai/user/6465c40119256f7a138020f6/history?page=2&limit=10

import { useStoreFetcher } from "..";
import useSWR from 'swr';

interface useDashboardUserHistoryProps {
  userId: string,
  page: number,
  limit?: number,
}

export const useDashboardUserHistory = ({
  userId,
  page,
  limit=10,
}: useDashboardUserHistoryProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/user/${userId}/history`;
  const params = {
    ...(page ? { page: page } : { }),
    ...(limit ? { limit: limit } : { }),   
  };
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: []
  });

  return ({
    userHistoryList: data,
    mutate,
    isValidating,
    isLoading
  })
  
}