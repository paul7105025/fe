import useSWR from 'swr';
import { useStoreFetcher } from '..';

interface useDashboardUserLatestActiveProps {
  userId: string
}
export const useDashboardUserLatestActive = ({
  userId
}: useDashboardUserLatestActiveProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/user/${userId}/active/latest`;
  const { data, isValidating, isLoading, mutate } = useSWR([url], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: "",
    }
  })

  return ({
    userLatestActive: data as string,
  })
}