import { useStoreFetcher } from "..";
import useSWR from 'swr';

interface useDashboardUserProps {
  page: number,
  q?: string
}

export const useDashboardUser = ({
  page,
  q,
}: useDashboardUserProps) => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/user`;
  const params = {
    ...(page ? { page: page } : { }),
    ...(q ? { q: q } : { }),   
  };
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: []
  });

  return ({
    userList: data,
    mutate,
    isValidating,
    isLoading
  })
  
}