import { useStoreFetcher } from "..";
import useSWR from 'swr';


export const useDashboardUserBan = () => {
  const { dashboardFetcher } = useStoreFetcher();
  const url = `/user/ban`;
  const params = {};
  const { data, isValidating, isLoading, mutate } = useSWR([url, params], dashboardFetcher, {
    keepPreviousData: true,
    fallbackData: []
  });

  return ({
    banList: data,
    mutate,
    isValidating,
    isLoading
  })
  
}