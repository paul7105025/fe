import { useStoreFetcher } from ".";
import useSWR from 'swr';

interface useGetHistoryProps {
  page: number;
  query?: string;
  reported?: boolean;
  toolId?: string;
}

export const useGetHistory = ({
  page,
  query,
  reported,
  toolId,
}: useGetHistoryProps) => {
  const { defaultFetcher } = useStoreFetcher();
  const _query = `?${page ? 'page=' + page.toString() + '&' : ''}${query ? 'q=' + query + '&' : ''}${reported ? 'reported=y&' : 'reported=n&'}${toolId ? 'toolId=' + toolId + '&' : ''}`;
  const { data, isValidating, isLoading, mutate } = useSWR(`/history` + _query, defaultFetcher, {
    keepPreviousData: true,
    fallbackData: {
      data: []
    }
  });
  return ({
    historyList: data.data,
    mutate,
    isValidating,
    isLoading
  })
}
