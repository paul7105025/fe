import { dashboardInstance } from ".";

export const postCoupon = async ({
  coupon,
}: {
  coupon: any
}) => {
  const url = `/coupon`;

  return await dashboardInstance
    .post(url, {
      ...coupon,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const putCouponId = async ({
  couponId,
  coupon,
}: {
  couponId: string;
  coupon: {
    [key: string]: string | number | boolean | Date | string[];
  };
}) => {
  const url = `/coupon/${couponId}`;

  return await dashboardInstance
    .put(url, {
      ...coupon,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const deleteCouponId = async ({ couponId }: { couponId: string }) => {
  const url = `/coupon/${couponId}`;

  return await dashboardInstance
    .delete(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
