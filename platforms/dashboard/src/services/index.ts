import axios from 'axios'
import { deleteCouponId, postCoupon, putCouponId } from './coupon';
import { deleteDashboardUserBan, postDashboardUserBan, postDashboardUserRefundById, postDashboardUserRewardById } from './dashboard/dashboard';
import { useDashboardCoupon } from './dashboard/useDashboardCoupon';
import { useDashboardCouponId } from './dashboard/useDashboardCouponId';
import { useDashboardCouponUser } from './dashboard/useDashboardCouponUser';
import { useDashboardHistory } from './dashboard/useDashboardHistory';
import { useDashboardUser } from './dashboard/useDashboardUser';
import { useDashboardUserBan } from './dashboard/useDashboardUserBan';
import { useDashboardUserHistory } from './dashboard/useDashboardUserHistory';
import { useDashboardUserId } from './dashboard/useDashboardUserId';
import { useAdminTool } from './useAdminTool';
import { useAdminToolRecommend } from './useAdminToolRecommend';
const admin_server_url = process.env.NEXT_PUBLIC_ADMIN_SERVER_API || "";
const auth_server_url = process.env.NEXT_PUBLIC_AUTH_SERVER_URL || "";
const dashboard_server_url = process.env.NEXT_PUBLIC_DASHBOARD_SERVER_URL || "";

export const adminInstance = axios.create({
  baseURL: admin_server_url,
});

export const authInstance = axios.create({
  baseURL: auth_server_url,
});

export const dashboardInstance = axios.create({
  baseURL: dashboard_server_url,
});

export const useStoreFetcher = () => {
  const defaultFetcher = (url: string) => adminInstance.get(url).then(res => res.data);
  const dashboardFetcher = ([url, params]: [url: string, params?: any]) => dashboardInstance.get(url, {
    ...(params ? { params: params } : {})
  }).then(res => res.data)
  
  return {
    defaultFetcher,
    dashboardFetcher,
  }
}

export const syncHeader = ({ key, value }: { key: string, value: string }) => {
  adminInstance.defaults.headers.common[key] = value;
  dashboardInstance.defaults.headers.common[key] = value;
}

export const adminAPI = {
  useGetTool: useAdminTool,
  useGetToolRecommend: useAdminToolRecommend
}

export const dashboardAPI = {
  useGetUser: useDashboardUser,
  useGetBanUser: useDashboardUserBan,
  useGetUserHistory: useDashboardUserHistory,
  useGetHistory: useDashboardHistory,
  useGetUserId: useDashboardUserId,
  useGetCoupon: useDashboardCoupon,
  useGetCouponId: useDashboardCouponId,
  useGetCouponUser: useDashboardCouponUser,
  postUserBan: postDashboardUserBan,
  deleteUserBan: deleteDashboardUserBan,
  postCoupon: postCoupon,
  deleteCouponId: deleteCouponId,
  putCouponId: putCouponId,
  postUserRewardById: postDashboardUserRewardById,
  postUserRefundById: postDashboardUserRefundById,
}

export * from './useAdminTool';