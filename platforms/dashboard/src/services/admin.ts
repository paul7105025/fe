import { adminInstance } from ".";

export const putAdminToolPostingStart = async (toolId: string) => {
  const url = `/admin/tool/${toolId}/posting-start`;
  return await adminInstance
    .put(url)
    .then(res => res)
    .catch(err => err?.response || err);
}

export const putAdminToolPostingStop = async (toolId: string) => {
  const url = `/admin/tool/${toolId}/posting-stop`;
  return await adminInstance
    .put(url)
    .then(res => res)
    .catch(err => err?.response || err);
}

export const postAdminToolRecommendCancel = async (toolId: string) => {
  const url = `/admin/tool/${toolId}/recommend/cancel`;
  return await adminInstance
    .post(url)
    .then(res => res)
    .catch(err => err?.response || err);
}

export const postAdminToolRecommendRegister = async (toolId: string) => {
  const url = `/admin/tool/${toolId}/recommend/register`;
  return await adminInstance
    .post(url)
    .then(res => res)
    .catch(err => err?.response || err);
}

export const deleteAdminTool = async (toolId: string) => {
  const url = `/admin/tool/${toolId}`;
  return await adminInstance
    .delete(url)
    .then(res => res)
    .catch(err => err?.response || err);
}
