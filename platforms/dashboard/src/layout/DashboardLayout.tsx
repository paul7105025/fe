'use client'

import { newColors, newTypo } from "@wrtn/ui";
import Link from "next/link"
import styled from 'styled-components';
import { usePathname } from 'next/navigation';


export const DashboardLayout = () => {
  const pathname = usePathname()
  return (
    <Wrapper>
      <LinkButton href={'/history'} isActive={pathname === '/history'}>
        전체
      </LinkButton>
      <LinkButton href={'/user'} isActive={pathname.startsWith('/user')}>
        유저
      </LinkButton>
      <LinkButton href={'/studio'} isActive={pathname === '/studio'}>
        스튜디오 관리
      </LinkButton>
      <LinkButton href={'/coupon'} isActive={pathname === '/coupon'}>
        쿠폰
      </LinkButton>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  height: 50px;
  background-color: ${newColors.GRAY_400};
  border-bottom: 5px solid ${newColors.GRAY_800};
`;

const LinkButton = styled(Link)<{ isActive?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50%;
  height: 100%;
  ${newTypo('content-16-semi')};
  text-decoration: initial;

  color: ${({ isActive }) => (isActive ? newColors.WHITE : newColors.GRAY_700)};
  background-color: ${({ isActive }) =>
  isActive ? newColors.GRAY_700 : newColors.GRAY_400};

  &:hover {
    color: ${newColors.WHITE};
    background-color: ${newColors.GRAY_800};
  }
`;