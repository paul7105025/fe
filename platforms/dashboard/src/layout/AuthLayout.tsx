'use client'

import React from 'react';
import { cookies } from "next/headers"
import { syncHeader } from "../services";
import { useRouter } from 'next/navigation';
import { getCookie } from 'cookies-next';


const AuthLayout = ({
  children
}: {
  children: React.ReactNode
}) => {
  const router = useRouter();
  React.useEffect(() => {
    const accessToken = getCookie('dashboard_access_token')?.toString();
    console.log(accessToken)
    if (accessToken) {
      syncHeader({
        key: 'Authorization',
        value: 'Bearer ' + accessToken
      });
    } else {
      router.push('/auth')
    }
  }, [])

  return (
    <>
      {children}
    </>
  )
}

export default AuthLayout;