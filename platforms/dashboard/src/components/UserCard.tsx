'use client';

import React from "react";
import styled, { keyframes } from "styled-components";

import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import {
  colors,
  FlexButton,
  FlexWrapper,
  InputStyle,
  typo,
} from "@wrtn/ui/styles";

import { IconLoginKakao, IconLoginNaver } from "@wrtn/ui/assets";
// import dayjs from "dayjs";
import { useAlert } from "react-alert";
// import { postUserRewardById, postUserRefundById } from "src/services";
// import Google from "../assets/icon_google_login.svg";
import { IconGoogle } from '@wrtn/ui';
import dayjs from "dayjs";
import { dashboardAPI } from "../services";

interface UserCardProps {
  user: any
  isBanned: boolean
  ban: any
  move?: any
  userWordCount?: number
  userHistoryCount?: number
  userLatestActive?: string
  isList: boolean
}

const UserCard = ({
  user,
  isBanned = false,
  ban,
  move,
  userWordCount,
  userHistoryCount,
  userLatestActive,
  isList,
}: UserCardProps) => {
  const alert = useAlert();
  const userCreatedDate = React.useMemo(
    () => new Date(user.createdAt),
    [user.createdAt]
  );

  const getLoginProvider = React.useCallback((provider: string) => {
    if (provider === "google") return <GoogleIcon />;
    else if (provider === "kakao") return <KaKaoIcon />;
    else if (provider === "naver") return <NaverIcon />;
  }, []);

  const handleBan = React.useCallback(() => {
    if (!isBanned && window.confirm("정말로 차단하시겠습니까?")) {
      ban(user._id, false);
    } else if (isBanned && window.confirm("정말로 차단을 해제하시겠습니까?")) {
      ban(user._id, true);
    }
  }, [isBanned]);

  const [isUserCreditOpened, setIsUserCreditOpened] = React.useState(false);

  const submitCredit = async () => {
    const res = await dashboardAPI.postUserRewardById({
      userId: user._id,
      count: creditBody.credit,
      message: creditBody.message,
    });

    if (res?.status === 201) {
      setIsUserCreditOpened(false);
      alert.removeAll();
      alert.show("보상 지급이 완료되었습니다.");
    } else {
      alert.removeAll();
      alert.show(res?.message || "오류가 발생했습니다.");
    }
  };

  const [creditBody, setCreditBody] = React.useState({
    credit: "",
    message: "",
  });

  const [isUserRefundOpened, setIsUserRefundOpened] = React.useState(false);

  const submitRefund = async () => {
    const res = await dashboardAPI.postUserRefundById({
      userId: user._id,
      message: refundBody.message,
    });

    if (res?.status === 201) {
      setIsUserRefundOpened(false);
      alert.removeAll();
      alert.show("환불이 완료되었습니다.");
    } else {
      alert.removeAll();
      alert.show(res?.data.message || "오류가 발생했습니다.");
    }
  };

  const [refundBody, setRefundBody] = React.useState({
    message: "환불",
  });

  return (
    <Wrapper
      isBanned={isBanned ? true : false}
      isAuthorized={user.number.length > 0 ? true : false}
      onClick={move}
    >
      <RowWrapper>
        <ToolMetaWrapper>
          {getLoginProvider(user.provider)}
          <FormName>
            <strong style={{ color: "black" }}>
              {user.name}
              {user.number.length < 1 ? " (미인증)" : ""}
            </strong>
          </FormName>
          <FormName>{user.company ? user.company + " | " : ""}</FormName>
          <FormName>{user.job}</FormName>
          {/* <FormName>{user.job.map((j) => j + " ")}</FormName> */}
        </ToolMetaWrapper>
        <UserBlockButton onClick={handleBan} isBanned={isBanned}>
          {isBanned ? "차단 해제" : "차단"}
        </UserBlockButton>
      </RowWrapper>
      <RowWrapper>
        <ToolMetaWrapper>
          <UserInfoText>{user.email}</UserInfoText> |
          <UserInfoText>
            {user.number.length > 0
              ? user.number.slice(0, 3) +
                "-" +
                user.number.slice(3, 7) +
                "-" +
                user.number.slice(7)
              : "미인증 사용자"}
          </UserInfoText>{" "}
          |
          <UserInfoText>
            가입입 : {dayjs(userCreatedDate).format("YYYY-MM-DD")}
          </UserInfoText>
          |
          <UserInfoText>
            현재 플랜 : {user.plan} 다음 플랜 : {user.nextPlan}
          </UserInfoText>
        </ToolMetaWrapper>
      </RowWrapper>
      {isList ? null : (
        <>
          <RowWrapper>
            <ToolMetaWrapper></ToolMetaWrapper>
          </RowWrapper>
          <RowWrapper>
            <UserInfoText>
              결제 일자 : {dayjs(user.paymentDate).format("YYYY-MM-DD HH:mm")}
            </UserInfoText>
            <UserInfoText>총 사용 글자수 : {userWordCount}</UserInfoText>
            <UserInfoText>총 생성 횟수 : {userHistoryCount}</UserInfoText>
            <UserInfoText>마지막 활성 일자 : {userLatestActive}</UserInfoText>
            <div style={{ flex: 1 }} />
            <UserCreditButton onClick={() => setIsUserCreditOpened(true)}>
              크레딧 보상
            </UserCreditButton>
            <UserRefundButton onClick={() => setIsUserRefundOpened(true)}>
              환불 하기
            </UserRefundButton>
          </RowWrapper>
        </>
      )}
      {isUserRefundOpened && (
        <ModalPortal onClose={() => setIsUserRefundOpened(false)}>
          <RefundModalWrapper>
            <RowWrapper>
              <RefundText>환불 사유</RefundText>
              <RefundInput
                value={refundBody.message}
                onChange={(e) => setRefundBody({ message: e.target.value })}
              />
            </RowWrapper>
            <RowWrapper>
              <AcceptButton onClick={() => submitRefund()}>확인</AcceptButton>
              <CancelButton onClick={() => setIsUserRefundOpened(false)}>
                취소
              </CancelButton>
            </RowWrapper>
          </RefundModalWrapper>
        </ModalPortal>
      )}
      {isUserCreditOpened && (
        <ModalPortal onClose={() => setIsUserCreditOpened(false)}>
          <CreditModalWrapper>
            <RowWrapper>
              <CreditText>메세지</CreditText>
              <CreditInput
                value={creditBody.message}
                onChange={(e) =>
                  setCreditBody({ ...creditBody, message: e.target.value })
                }
              />
            </RowWrapper>
            <RowWrapper>
              <CreditText>크레딧</CreditText>
              <CreditInput
                value={creditBody.credit}
                onChange={(e) =>
                  setCreditBody({ ...creditBody, credit: e.target.value })
                }
              />
            </RowWrapper>
            <RowWrapper>
              <AcceptButton onClick={() => submitCredit()}>확인</AcceptButton>
              <CancelButton onClick={() => setIsUserCreditOpened(false)}>
                취소
              </CancelButton>
            </RowWrapper>
          </CreditModalWrapper>
        </ModalPortal>
      )}
    </Wrapper>
  );
};

export default UserCard;

const fadeout = keyframes`
  0%{
    opacity: 0;
  }
  100%{
    opacity: 1;
  }
`;

const Wrapper = styled(FlexWrapper)<{ isBanned: boolean, isAuthorized: boolean }>`
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  max-width: 720px;
  cursor: pointer;

  background-color: ${({ isBanned, isAuthorized }) =>
    isBanned ? colors.GRAY_55 : isAuthorized ? colors.WHITE : colors.GRAY_50};
  padding: 10px 10px 12px;
  gap: 10px;
  border-radius: 5px;

  outline: ${({ isBanned }) => (isBanned ? "1.5px solid #FF0000" : "none")};

  animation-duration: 0.4s;
  animation-name: ${fadeout};

  &:hover {
    background-color: ${colors.BLUE_GRAY_LINE};
  }
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: space-between;
  gap: 20px;
`;

const FormName = styled.span`
  ${typo({
    size: "14px",
    height: "15px",
    weight: "500",
    color: colors.GRAY_70,
  })}
`;

const ToolMetaWrapper = styled(FlexWrapper)`
  justify-content: flex-start;
  gap: 5px;
  align-items: center;
`;

const UserBlockButton = styled(FlexButton)<{ isBanned: boolean }>`
  padding: 5px 10px;
  background-color: ${({ isBanned }) =>
    isBanned ? colors.FEEDBACK_GREEN : colors.FEEDBACK_RED};
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.WHITE,
    weight: "500",
  })}
`;

const UserRefundButton = styled(FlexButton)`
  padding: 5px 10px;
  background-color: ${colors.TAG_RED};
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.WHITE,
    weight: "500",
  })}
`;

const UserCreditButton = styled(FlexButton)`
  padding: 5px 10px;
  background-color: ${colors.TAG_GREEN};
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.WHITE,
    weight: "500",
  })}
`;

const UserInfoText = styled.span`
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.GRAY_80,
    weight: "500",
  })}
`;

const GoogleIcon = styled(IconGoogle)`
  top: 45px;
  right: 19px;
  height: 14px;
  width: 14px;
`;

const NaverIcon = styled(IconLoginNaver)`
  width: 14px;
  height: 15px;
  path {
    fill: #2db400;
  }
`;

const KaKaoIcon = styled(IconLoginKakao)`
  width: 15px;
  height: 14px;
`;

const RefundModalWrapper = styled(FlexWrapper)`
  flex-direction: column;
  background-color: ${colors.WHITE};
  padding: 30px;
  border-radius: 5px;
  gap: 25px;
  max-width: 480px;
  width: 100%;
`;

const CreditModalWrapper = styled(FlexWrapper)`
  flex-direction: column;
  background-color: ${colors.WHITE};
  padding: 30px;
  border-radius: 5px;
  gap: 10px;
  max-width: 480px;
  width: 100%;
`;

const CreditText = styled.p`
  flex: 0 0 50px;
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.GRAY_80,
    weight: "500",
  })}
`;
const RefundText = styled.p`
  flex: 0 0 50px;
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.GRAY_90,
    weight: "500",
  })}
`;

const CreditInput = styled.input`
  ${InputStyle}
`;

const RefundInput = styled.input`
  ${InputStyle}
`;

const AcceptButton = styled(FlexButton)`
  background-color: ${colors.GRAY_30};
  padding: 5px;
  border-radius: 5px;

  cursor: pointer;
`;

const CancelButton = styled(FlexButton)`
  background-color: ${colors.GRAY_30};
  padding: 5px;
  border-radius: 5px;

  cursor: pointer;
`;