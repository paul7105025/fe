import { useDelayUpdate } from "@wrtn/core";
import { FlexWrapper, InputStyle } from "@wrtn/ui/styles";
import React from "react";
import styled from "styled-components";

interface SearchInputProps {
  search: string;
  setSearch: (val: any) => void;
  placeholder: string;
  onKeyDown: (e: any) => void;
}
const SearchInput = ({ 
  search, 
  setSearch, 
  placeholder, 
  onKeyDown 
}: SearchInputProps) => {
  const [value, setValue] = React.useState("");

  const handleChange = useDelayUpdate(
    (e: any) => setValue(e.target.value),
    (e: any) => setSearch(e.target.value), 250
  );

  return (
    <Wrapper>
      <Input
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
        onKeyDown={onKeyDown}
      />
      {}
    </Wrapper>
  );
};

export default SearchInput;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
`;

const Input = styled.input`
  ${InputStyle}
`;

