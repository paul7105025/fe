'use client';

import { colors, FlexButton, Icon } from "@wrtn/ui";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import styled from "styled-components";
// import { FlexButton } from "@/styles/container";
// import { colors, newColors } from "@/styles/color";

type ToolDeleteCheckModalProps = {
  onClose?: () => void;
  handleDelete?: React.MouseEventHandler<HTMLButtonElement>;
};

export const ToolDeleteCheckModal = ({
  onClose,
  handleDelete,
}: ToolDeleteCheckModalProps) => {
  return (
    <>
      <ModalPortal onClose={onClose} zIndex={99}>
        <Wrapper>
          <CloseButton onClick={onClose}>
            <span><Icon icon="multiply" color={colors.gray_80} size={13}/></span>
          </CloseButton>
          <IconWrapper>
            <DeleteIcon src="../assets/icon_trash.svg" />
          </IconWrapper>
          <Header />
          <ContentWrapper>
            <Title>툴을 삭제하시겠어요?</Title>
            <SeondText>
              툴을 삭제하면 스튜디오 및 AI 스토어에서 <br /> 전부 사라지며, 되돌릴
              수 없어요.
            </SeondText>
            <ButtonWrapper>
              <CancelButton onClick={onClose}>취소하기</CancelButton>
              <DeleteButton onClick={handleDelete}>삭제하기</DeleteButton>
            </ButtonWrapper>
          </ContentWrapper>
        </Wrapper>
      </ModalPortal>
    </>
  );
};

const Wrapper = styled.div`
  max-width: 395px;
  width: 100%;
  background: #ffffff;
  position: relative;
  border-radius: 20px;
`;

const CloseButton = styled(FlexButton)`
  position: absolute;
  top: 21px;
  right: 21px;
  cursor: pointer;
`;

const Close = styled.img`
  width: 20px;
  height: 20px;
`;

const Header = styled.div`
  width: 100%;
  height: 57px;
  background: #f2f7ff;
  border-radius: 20px 20px 0px 0px;
`;

const IconWrapper = styled(FlexButton)`
  position: absolute;
  top: 28px;
  left: 32px;
  background: ${colors.WHITE};

  width: 63px;
  height: 63px;

  border-radius: 50%;
`;

const DeleteIcon = styled.img`
  width: 36px;
  height: 36px;
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 32px 28px 0px 28px;
  align-items: center;
  justify-content: center;
`;

const Title = styled.div`
  height: 18px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 100%;
  text-align: center;
  color: ${colors.GRAY_90};
`;

const SeondText = styled.div`
  margin-top: 22px;
  height: 48px;
  font-family: "Pretendard";
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 150%;
  text-align: center;
  color: ${colors.GRAY_80};
`;

const ButtonWrapper = styled.div`
  margin-top: 33px;
  margin-bottom: 18px;
  width: 100%;
  display: flex;
`;

const CancelButton = styled.button`
  border: none;
  /* flex: 1; */
  cursor: pointer;
  background: none;
  width: 169px;
`;

const DeleteButton = styled.button`
  border: none;
  /* flex: 1; */
  cursor: pointer;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 12px 60px;
  gap: 10px;
  width: 169px;
  height: 40px;
  background: #f2f7ff;
  border-radius: 5px;
  font-weight: 600;
  color: ${colors.ACTION_BLUE};
`;
