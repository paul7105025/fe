import React from "react";
import styled from "styled-components";

import { couponType, COUPON_CONDITION_RECORD, COUPON_TYPE_RECORD } from "@wrtn/core";

import dayjs from 'dayjs'                                                                                                                                                                                                                                              

import {
  colors,
  FlexButton,
  FlexWrapper,
  AutoSizeTextArea,
  typo,
} from "@wrtn/ui/styles";
import { random } from "lodash";
import { dashboardAPI } from "../services";

interface CouponEditModalProps {
  indexing: number;
  item: couponType;
  onClose: () => void;
  onCloseWithRefresh: () => void;
  toggleEdit: () => void;
}

// const TODAY = moment().toDate();
const TODAY = dayjs().locale("ko");
const MAX_RANDOM_CODE = 100000;

const batchBillingTime = (day: dayjs.Dayjs): dayjs.Dayjs => dayjs(day).locale("ko").set('second', 0).set('minute', 0).set('hour', 10);

const generateRandomString = (num: number): string => {
  const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let result = '';
  const charactersLength = characters.length;
  for (let i = 0; i < num; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}

const generateRandomCodeList = (count: number, prefix?: string, ) => {
  let codeList: string[] = [];
  while (codeList.length < count) {
    const randCode = generateRandomString(7);
    if (!codeList.find(v => v === randCode)) {
      codeList.push(randCode);
    }
  }
  return codeList.map(v => prefix + v);
}


type couponCreateTempType = couponType & {
  codeType: string;
  randomCodeCount: number;
  fixedCode: string;
}

const useSaveCoupon = (currentCoupon: couponCreateTempType) => {

  const canSave = React.useMemo(() => {
    if (currentCoupon.codeType === "random" && currentCoupon.randomCodeCount === 1) return false;
    if (currentCoupon.codeType === "fixed" && currentCoupon?.fixedCode?.length === 0) return false;
    if (!(currentCoupon.name?.length > 0)) return false
    if (!(currentCoupon.description?.length > 0)) return false
    if (currentCoupon.type === "discount") {
      if (currentCoupon?.percent === undefined) return false;
      if (currentCoupon.percent % 10 !== 0 && 
        (currentCoupon.percent > 100 || currentCoupon.percent < 0)
      ) 
        return false;
    }
    return true;
  }, [currentCoupon])

  const onSave = async (indexing: number, successFunction: any) => {
    let { fixedCode, randomCodeCount, codeType , ...value } = currentCoupon;

    if (value.type === "experience") {
      //remove quantity 
      const { percent, ...temp } = value;
      value = temp;
    }
    // validation
    if (currentCoupon.codeType === "fixed") {
      value.code = [currentCoupon.fixedCode];
    }
    if (currentCoupon.codeType === "random") {
      if (currentCoupon.randomCodeCount === 1) return alert("쿠폰 생성에 실패했습니다. 유효하지 않은 쿠폰 정보입니다.");
      const randomCodes: string[] = generateRandomCodeList(currentCoupon.randomCodeCount, indexing.toLocaleString('en-US', {
        minimumIntegerDigits: 4,
        useGrouping: false,
      }));
      value.code = randomCodes;
    }
    if (!(value.name?.length > 0)) return alert("쿠폰 생성에 실패했습니다. 유효하지 않은 쿠폰 정보입니다.");
    if (!(value.description?.length > 0)) return alert("쿠폰 생성에 실패했습니다. 유효하지 않은 쿠폰 정보입니다.");
    const res = await dashboardAPI.postCoupon({
      coupon: value,
    });

    if (res?.status === 200 || res?.status === 201) {
      successFunction();
    } else {
      alert("쿠폰 생성에 실패했습니다. 유효하지 않은 쿠폰 정보입니다.");
    }
  };

  return {
    onSave,
    canSave,
  }
}

const useCouponTempValue = (currentCoupon: couponType) => {
  // const [fixedCode, setFixedCode] = React.useState("");
  // const [codeType, setCodeType] = React.useState<string>("fixed");
  // const [randomCodeCount, setRandomCodeCount] = React.useState(1);

  const [currentTempCoupon, setCurrentTempCoupon] = React.useState<couponCreateTempType>({
    ...currentCoupon,
    fixedCode: "",
    codeType: "fixed",
    randomCodeCount: 1,
  })


  React.useEffect(() => {
    setCurrentTempCoupon(c => ({
      ...c,
      ...currentCoupon
    }))
  }, [currentCoupon])

  return [currentTempCoupon, setCurrentTempCoupon] as const;
}

const CouponCreateModal = ({
  indexing,
  item,
  onClose,
  onCloseWithRefresh,
  toggleEdit,
}: CouponEditModalProps) => {
  const [currentCoupon, setCurrentCoupon] = React.useState<couponType>(item);
  const [currentTempCoupon, setCurrentTempCoupon] = useCouponTempValue(currentCoupon);

  const {
    canSave,
    onSave,
  } = useSaveCoupon(currentTempCoupon);

  const isFixedCodeType = currentTempCoupon.codeType === "fixed";
  const isRandomCodeType = currentTempCoupon.codeType === "random";


  const onChangeCoupon = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    type: keyof couponType
  ) => {
    toggleEdit();
    setCurrentCoupon({
      ...currentCoupon,
      [type]: e.target.value,
    });
  };

  const onChangeCoupon_Number = (
    value: number,
    type: keyof couponType
  ) => {
    toggleEdit();
    setCurrentCoupon({
      ...currentCoupon,
      [type]: value
    })
  }

  const onChangeCouponDueDate = (date: string) => {
    if (date) {
      // const newDate = new Datec(date);
      const newDate = batchBillingTime(dayjs(date));
      if (newDate.isBefore(TODAY)) {
        setCurrentCoupon({
          ...currentCoupon,
          dueDate: TODAY.toISOString(),
        });
      } else {
        setCurrentCoupon({
          ...currentCoupon,
          dueDate: newDate.toISOString(),
        });
      }
    }
  };

  const handleChangeCouponTypeSelect = (e: any) => {
    setCurrentCoupon((c) => ({
      ...c,
      type: e.target.value
    }));
  }
  const handleChangeCouponConditionSelect = (e: any) => {
    if (e.target.value === "no-condition") {
      setCurrentCoupon((c) => {
        const { condition, ...rest } = c;

        return rest;
      })
      return ;
    }

    setCurrentCoupon((c) => ({
      ...c,
      condition: e.target.value
    }));
  }


  React.useEffect(() => {
    setCurrentCoupon((c) => {
      const { condition, ...rest } = c;
      return rest;
    });
  }, [currentCoupon?.type])

  return (
    <Wrapper column>
      <CloseButton onClick={onClose}>닫기</CloseButton>
      <SaveButton onClick={() => onSave(indexing, onCloseWithRefresh)} disabled={!canSave}>저장하기</SaveButton>
      <CouponLabel>
        쿠폰 이름 :
        <CouponInput
          required
          placeholder="쿠폰 이름"
          value={currentCoupon.name}
          onChange={(e) => onChangeCoupon(e, "name")}
        />
      </CouponLabel>
      <CouponLabel>
        쿠폰 설명 :
        <CouponDescription
          rows={1}
          required
          placeholder="쿠폰 설명"
          value={currentCoupon.description}
          onChange={(e) => onChangeCoupon(e, "description")}
        />
      </CouponLabel>
      <CouponLabel>
        쿠폰 만료일 :
        <CouponInput
          required
          type="date"
          placeholder="쿠폰 만료일"
          // value={dayjs(currentCoupon.dueDate).locale("ko").format('YYYY-MM-DDThh:mm')}
          value={dayjs(currentCoupon.dueDate).locale("ko").format('YYYY-MM-DD')}
          min={TODAY.format('YYYY-MM-DD')}
          onChange={(e) => onChangeCouponDueDate(e.target.value)}
        />
      </CouponLabel>
      <CouponLabel>
        쿠폰 타입 :
        <CouponSelect key={"coupon-type-select"} onChange={handleChangeCouponTypeSelect} value={currentCoupon.type}>
          {/* <CouponOption key={"discount-price"} value={"discount-price"}>{COUPON_TYPE_RECORD["discount-price"]}</CouponOption> */}
          <CouponOption key={"discount"}value={"discount"}>{COUPON_TYPE_RECORD["discount"]}</CouponOption>
          <CouponOption key={"experience"}value={"experience"}>{COUPON_TYPE_RECORD["experience"]}</CouponOption>
        </CouponSelect>
      </CouponLabel>
      <CouponLabel>
        쿠폰 조건 :
        <CouponSelect key={"coupon-condition-select"} onChange={handleChangeCouponConditionSelect} value={currentCoupon.condition}>
          <CouponOption key={"no-condition"} value={"no-condition"}>조건 없음</CouponOption>
          { currentCoupon.type === "discount" && <CouponOption key={"first-payment"} value={"first-payment"}>{COUPON_CONDITION_RECORD["first-payment"]}</CouponOption> }
          { currentCoupon.type === "experience" && <CouponOption key={"first-coupon"} value={"first-coupon"}>{COUPON_CONDITION_RECORD["first-coupon"]}</CouponOption> }
          {/* <CouponOption key={"experience"}value={"experience"}>{COUPON_TYPE_RECORD["experience"]}</CouponOption> */}
        </CouponSelect>
      </CouponLabel>
      

      { (currentCoupon.type === "discount") &&
        <CouponLabel>
          쿠폰 할인할 비율(%) :
          <CouponInput
            type="number"
            min={0}
            max={100}
            step={10}
            placeholder="10"
            value={currentCoupon?.percent || 0}
            onChange={(e) => onChangeCoupon_Number(Number(e.target.value), "percent")}
            key={"discount-percent-value"}
          />
        </CouponLabel> 
      }
      { currentCoupon.type !== "experience" &&
        <CouponLabel>
          쿠폰 할인 혜택 기간(달)
          <CouponInput
            type="number"
            placeholder="N 달"
            value={currentCoupon.count}
            onChange={(e) => onChangeCoupon_Number(Number(e.target.value), "count")}
          />
        </CouponLabel>
      }
      { currentCoupon.type === "experience" &&
        <CouponLabel>
          며칠 동안 무료 체험인지(일)
          <CouponInput
            type="number"
            placeholder="N일"
            value={currentCoupon.count}
            onChange={(e) => onChangeCoupon_Number(Number(e.target.value), "count")}
          />
        </CouponLabel>
      }

      <CouponLabel>
        코드 타입 :
        <CouponSelect 
          key={"code-type-select"} 
          onChange={(e) => {
            setCurrentTempCoupon(c => ({
              ...c,
              codeType: e.target.value
            }));
          }} 
          value={currentTempCoupon.codeType}>
          {/* <CouponOption key={"discount-price"} value={"discount-price"}>{COUPON_TYPE_RECORD["discount-price"]}</CouponOption> */}
          <CouponOption key={"fixed"} value={"fixed"}>{"지정 코드 생성"}</CouponOption>
          <CouponOption key={"random"} value={"random"}>{"난수 코드 생성"}</CouponOption>
        </CouponSelect>
      </CouponLabel>
      { isRandomCodeType &&
        <CouponLabel>
          생성할 난수 코드 개수
          <CouponInput
            type="number"
            placeholder="11"
            value={currentTempCoupon.randomCodeCount}
            onChange={(e) => {
              const num = Number(e.target.value)
              const limitNum = num > MAX_RANDOM_CODE ? MAX_RANDOM_CODE : num;
              // setRandomCodeCount(limitNum);
              setCurrentTempCoupon(c => ({
                ...c,
                randomCodeCount: limitNum
              }));
              onChangeCoupon_Number(limitNum, "quantity");
            }}
            key={"code-type-value"}
          />
        </CouponLabel> 
      }
      { isFixedCodeType &&
        <CouponLabel>
          지정 코드
          <CouponInput
            type="string"
            placeholder="WRTNTECHNOLOGIES"
            value={currentTempCoupon.fixedCode}
            onChange={(e) => {
              // setFixedCode(e.target.value)
              setCurrentTempCoupon(c => ({
                ...c,
                fixedCode: e.target.value
              }));
            }}
            key={"code-type-value"}
          />
        </CouponLabel> 
      }
      { isFixedCodeType &&
        <CouponLabel>
          몇 번 사용 가능한 지(-1 시 무제한)
          <CouponInput
            type="number"
            placeholder="11"
            value={currentTempCoupon.randomCodeCount}
            onChange={(e) => {
              const num = Number(e.target.value)
              const limitNum = num > MAX_RANDOM_CODE ? MAX_RANDOM_CODE : num;
              // setRandomCodeCount(limitNum);
              setCurrentTempCoupon(c => ({
                ...c,
                randomCodeCount: limitNum,
              }));
              onChangeCoupon_Number(limitNum, "quantity");
            }}
            key={"code-type-value"}
          />
        </CouponLabel> 
      }
    </Wrapper>
  );
};

export default CouponCreateModal;

const Wrapper = styled(FlexWrapper)`
  padding: 50px 30px;

  background-color: ${colors.WHITE};
  border-radius: 10px;

  justify-content: flex-start;
  align-items: flex-start;

  gap: 15px;
`;

const DefaultButton = styled(FlexButton)`
  color: white;
  border-radius: 5px;

  position: absolute;

  padding: 5px 10px;

  white-space: nowrap;
`;

const CloseButton = styled(DefaultButton)`
  background-color: black;

  top: 10px;
  right: 20px;
`;

const SaveButton = styled(DefaultButton)`
  background-color: ${colors.FEEDBACK_GREEN};

  bottom: 10px;
  left: 20px;
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
    background-color: ${colors.GRAY_30};
  }
`;

const CouponLabel = styled.label`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 5px;

  width: 100%;

  ${typo({
    size: "14px",
    weight: "500",
    color: colors.BLACK,
  })}

  white-space: nowrap;
  position: relative;
`;

const CouponInput = styled.input`
  padding: 4px 6px;
  background-color: ${colors.BACKGROUND};

  flex: 1;

  ${typo({
    size: "14px",
    height: "26px",
    color: colors.GRAY_90,
  })}
`;

const CouponDescription = styled(AutoSizeTextArea)`
  padding: 4px 6px;
  background-color: ${colors.BACKGROUND};
  width: 100%;

  ${typo({
    size: "14px",
    height: "26px",
    color: colors.GRAY_90,
  })}
`;


const CouponSelect = styled.select`

`;
const CouponOption = styled.option`

`