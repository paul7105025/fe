import styled from "styled-components";
import dayjs from 'dayjs';
import { couponType } from "@wrtn/core";
import { colors, newColors, FlexWrapper, typo } from "@wrtn/ui/styles";
import { COUPON_TYPE_RECORD, COUPON_CONDITION_RECORD } from '@wrtn/core/records'
interface CouponItemProps {
  item?: couponType;
  openModal?: (item: couponType) => void;
}

export const CouponItem = ({ item, openModal }: CouponItemProps) => {
  const onClick = () => {
    if (item && openModal)
    openModal(item);
  };

  if (!item) {
    return (
      <Header row >
        <CouponLabel>쿠폰 코드</CouponLabel>
        <CouponLabel>쿠폰 이름</CouponLabel>
        <CouponLabel>쿠폰 설명</CouponLabel>
        <CouponLabel>쿠폰 타입</CouponLabel>
        <CouponLabel>쿠폰 조건</CouponLabel>
        <CouponLabel>코드 타입</CouponLabel>
        <CouponLabel>쿠폰 만료일</CouponLabel>
        <CouponLabel>쿠폰 상태</CouponLabel>
        {/* {item.percent && <CouponLabel>쿠폰 할인율 : {item.percent}</CouponLabel>} */}
        {/* {item.count && <CouponLabel>쿠폰 횟수 : {item.count}</CouponLabel>} */}
      </Header>
    );
  }

  return (
    <Wrapper row onClick={onClick}>
      <CouponLabel>{item?.code?.at(0)}</CouponLabel>
      <CouponLabel>{item.name}</CouponLabel>
      <CouponLabel>{item.description}</CouponLabel>
      <CouponLabel>{COUPON_TYPE_RECORD[item.type]}</CouponLabel>
      <CouponLabel>{item?.condition && COUPON_CONDITION_RECORD[item.condition]}</CouponLabel>
      <CouponLabel>{item?.code?.length === 1 ? "지정" : item?.code?.length === undefined ? "코드 없음" : "난수"}</CouponLabel>
      <CouponLabel>{dayjs(item.dueDate).format("YYYY-MM-DD")}</CouponLabel>
      <CouponLabel>{dayjs(item.dueDate).isBefore(new Date()) ? "기간 만료됨" : "배포 중"}</CouponLabel>
      {/* {item.percent && <CouponLabel>쿠폰 할인율 : {item.percent}</CouponLabel>} */}
      {/* {item.count && <CouponLabel>쿠폰 횟수 : {item.count}</CouponLabel>} */}
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  background-color: transparent;
  padding: 15px 15px;

  width: 100%;
  gap: 5px;

  cursor: pointer;
  &:hover {
    background-color: ${newColors.GRAY_500};
  }
`;

const Header = styled(Wrapper)`
  &:hover { 
    background-color: initial;
  }
`

const CouponLabel = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 5px;

  width: 100%;
  height: 100%;
  overflow: scroll;

  ${typo({
    size: "14px",
    weight: "500",
    color: newColors.WHITE,
  })}

  white-space: nowrap;
  position: relative;

`;

const CouponName = styled.div``;

const CouponDescription = styled.div``;

const CouponType = styled.div``;

const CouponCode = styled.div``;

const CouponDiscount = styled.div``;
