import React from "react";
import styled from "styled-components";
import { CSVLink } from 'react-csv';

import { couponType, COUPON_CONDITION_RECORD, COUPON_TYPE_RECORD, userCouponType } from "@wrtn/core";

import dayjs from 'dayjs'                                                                                                                                                                                                                                              

import {
  colors,
  FlexButton,
  FlexWrapper,
  AutoSizeTextArea,
  typo,
} from "@wrtn/ui/styles";
import { CouponItem } from "./CouponItem";
import { dashboardAPI } from "../services";
import { UserCouponItem } from "./UserCouponItem";

interface CouponEditModalProps {
  item: couponType;
  onClose: () => void;
  onCloseWithRefresh: () => void;
  toggleEdit: () => void;
}

const TODAY = dayjs().locale("ko");
const MAX_RANDOM_CODE = 100000;

const batchBillingTime = (day: dayjs.Dayjs): dayjs.Dayjs => dayjs(day).locale("ko").set('second', 0).set('minute', 0).set('hour', 10);

const CouponEditModal = ({
  item,
  onClose,
  onCloseWithRefresh,
  toggleEdit,
}: CouponEditModalProps) => {
  const [currentCoupon, setCurrentCoupon] = React.useState<couponType>(item);
  const [codeType, setCodeType] = React.useState<string>(currentCoupon?.code?.length === 1 ? "fixed" : currentCoupon?.code?.length === undefined ? "no-code" : "random");
  const [randomCodeCount, setRandomCodeCount] = React.useState(currentCoupon?.quantity);

  const { couponUserList } = dashboardAPI.useGetCouponUser({ couponId: item?._id || "" });

  const onChangeCoupon = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    type: keyof couponType
  ) => {
    toggleEdit();
    setCurrentCoupon({
      ...currentCoupon,
      [type]: e.target.value,
    });
  };

  const onChangeCoupon_Number = (
    value: number,
    type: keyof couponType
  ) => {
    toggleEdit();
    console.log(value)
    setCurrentCoupon({
      ...currentCoupon,
      [type]: value
    })
  }

  const onChangeCouponDueDate = (date: string) => {
    if (date) {
      const newDate = batchBillingTime(dayjs(date));
      if (newDate.isBefore(TODAY)) {
        setCurrentCoupon({
          ...currentCoupon,
          dueDate: TODAY.toISOString(),
        });
      } else {
        setCurrentCoupon({
          ...currentCoupon,
          dueDate: newDate.toISOString(),
        });
      }
    }
  };

  const handleChangeCouponTypeSelect = (e: any) => {
    setCurrentCoupon((c) => ({
      ...c,
      type: e.target.value
    }));
  }

  const handleChangeCouponConditionSelect = (e: any) => {
    if (e.target.value === "no-condition") {
      setCurrentCoupon((c) => {
        const { condition, ...rest } = c;

        return rest;
      })
      return ;
    }
    setCurrentCoupon((c) => ({
      ...c,
      condition: e.target.value
    }));
  }

  React.useEffect(() => {
    // if ()
    setCurrentCoupon((c) => {
      const { condition, ...rest } = c;
      return rest;
    });
  }, [currentCoupon?.type])

  const onSave = async () => {
    if (currentCoupon._id) {
      const res = await dashboardAPI.putCouponId({
        couponId: currentCoupon._id,
        coupon: currentCoupon,
      });

      if (res?.status === 200 || res?.status === 201) {
        onCloseWithRefresh();
      } else {
        alert("쿠폰 저장에 실패했습니다. 유효하지 않은 쿠폰 정보입니다.");
      }
    } else {
      alert("쿠폰 저장에 실패했습니다. 유효하지 않은 쿠폰 정보입니다.");
    }
  };

  const onDelete = async () => {
    if (currentCoupon._id) {
      if (window.confirm("정말로 삭제하시겠습니까?")) {
        const res = await dashboardAPI.deleteCouponId({ couponId: currentCoupon._id });

        if (res?.status === 200 || res?.status === 201) {
          onCloseWithRefresh();
        }
      }
    }
  };

  return (
    <Wrapper column>
      <CloseButton onClick={onClose}>닫기</CloseButton>
      {/* <SaveButton onClick={onSave}>저장하기</SaveButton> */}
      {/* <DeleteButton onClick={onDelete}>삭제하기</DeleteButton> */}
      <CouponInfoWrapper row>
        <CouponInfoSubWrapper column>
        {currentCoupon._id && (
          <CouponLabel>
            쿠폰 아이디 : <CouponInput value={currentCoupon._id} disabled />
          </CouponLabel>
        )}
        <CouponLabel>
          쿠폰 이름 :
          <CouponInput
            disabled
            placeholder="쿠폰 이름"
            value={currentCoupon.name}
            onChange={(e) => onChangeCoupon(e, "name")}
          />
        </CouponLabel>
        <CouponLabel>
          쿠폰 설명 :
          <CouponDescription
            disabled
            rows={1}
            placeholder="쿠폰 설명"
            value={currentCoupon.description}
            onChange={(e) => onChangeCoupon(e, "description")}
          />
        </CouponLabel>
        <CouponLabel>
          쿠폰 만료일 :
          <CouponInput
            required
            type="date"
            placeholder="쿠폰 만료일"
            // value={dayjs(currentCoupon.dueDate).locale("ko").format('YYYY-MM-DDThh:mm')}
            value={dayjs(currentCoupon.dueDate).locale("ko").format('YYYY-MM-DD')}
            min={TODAY.format('YYYY-MM-DD')}
            onChange={(e) => onChangeCouponDueDate(e.target.value)}
          />
        </CouponLabel>
        </CouponInfoSubWrapper>
        <CouponInfoSubWrapper column>
          <CouponLabel>
            쿠폰 타입 :
            <CouponSelect disabled key={"coupon-type-select"} onChange={handleChangeCouponTypeSelect} value={currentCoupon.type}>
              {/* <CouponOption key={"discount-price"} value={"discount-price"}>{COUPON_TYPE_RECORD["discount-price"]}</CouponOption> */}
              <CouponOption key={"discount"}value={"discount"}>{COUPON_TYPE_RECORD["discount"]}</CouponOption>
              <CouponOption key={"experience"}value={"experience"}>{COUPON_TYPE_RECORD["experience"]}</CouponOption>
            </CouponSelect>
          </CouponLabel>
          <CouponLabel>
            쿠폰 조건 :
            <CouponSelect disabled key={"coupon-condition-select"} onChange={handleChangeCouponConditionSelect} value={currentCoupon.condition}>
              <CouponOption key={"no-condition"} value={"no-condition"}>조건 없음</CouponOption>
              <CouponOption key={"first-payment"} value={"first-payment"}>{COUPON_CONDITION_RECORD["first-payment"]}</CouponOption>
              <CouponOption key={"first-coupon"} value={"first-coupon"}>{COUPON_CONDITION_RECORD["first-coupon"]}</CouponOption>
              {/* <CouponOption key={"experience"}value={"experience"}>{COUPON_TYPE_RECORD["experience"]}</CouponOption> */}
            </CouponSelect>
          </CouponLabel>

          {/* { (currentCoupon.type === "discount-price") &&
            <CouponLabel>
              쿠폰 할인하는 금액(원) :
              <CouponInput
                type="number"
                min={0}
                max={100}
                placeholder="10000"
                value={currentCoupon.percent}
                onChange={(e) => onChangeCoupon(e, "percent")}
              />
            </CouponLabel> 
          } */}
          { (currentCoupon.type === "discount") &&
            <CouponLabel>
              쿠폰 할인할 비율(%) :
              <CouponInput
                disabled
                type="number"
                min={0}
                max={100}
                step={10}
                placeholder="10"
                value={currentCoupon.percent}
                onChange={(e) => onChangeCoupon_Number(Number(e.target.value), "percent")}
              />
            </CouponLabel> 
          }
          { currentCoupon.type !== "experience" &&
            <CouponLabel>
              쿠폰 할인 혜택 기간(달)
              <CouponInput
                disabled
                type="number"
                placeholder="N 달"
                value={currentCoupon.count}
                onChange={(e) => onChangeCoupon_Number(Number(e.target.value), "count")}
              />
            </CouponLabel>
          }
          { currentCoupon.type === "experience" &&
            <CouponLabel>
              며칠 동안 무료 체험인지(일)
              <CouponInput
                disabled
                type="number"
                placeholder="N일"
                value={currentCoupon.count}
                onChange={(e) => onChangeCoupon_Number(Number(e.target.value), "count")}
              />
            </CouponLabel>
          }
        </CouponInfoSubWrapper>
        <CouponInfoSubWrapper column>
          <CouponLabel>
            코드 타입 :
            <CouponSelect disabled key={"code-type-select"} onChange={(e) => setCodeType(e.target.value)} value={codeType}>
              {/* <CouponOption key={"discount-price"} value={"discount-price"}>{COUPON_TYPE_RECORD["discount-price"]}</CouponOption> */}
              <CouponOption key={"fixed"}value={"fixed"}>{"지정 코드 생성"}</CouponOption>
              <CouponOption key={"random"}value={"random"}>{"난수 코드 생성"}</CouponOption>
            </CouponSelect>
          </CouponLabel>
          { codeType === "random" &&
            <CouponLabel>
              생성할 난수 코드 개수
              <CouponInput
                disabled              
                type="number"
                placeholder="11"
                value={randomCodeCount}
                onChange={(e) => {
                  setRandomCodeCount(Number(e.target.value));
                  onChangeCoupon_Number(Number(e.target.value), "quantity");
                }}
                key={"code-type-value"}
              />
            </CouponLabel> 
          }
          { codeType === "random" &&
            <CouponLabel>
              {/* <button onClick={() => {
                console.log(currentCoupon.code)
                CSVL
              }}>난수 코드 다운로드하기</button> */}
              {/* @ts-ignore */}
              <CSVLink data={currentCoupon?.code?.map(v => ({ code: v }))}>난수 코드 다운로드하기</CSVLink>
          </CouponLabel> 
          }
          { codeType === "fixed" &&
            <CouponLabel>
              지정 코드
              <CouponInput
                disabled
                type="string"
                placeholder="11"
                value={currentCoupon.code}
                onChange={(e) => {
                  setRandomCodeCount(Number(e.target.value));
                  onChangeCoupon_Number(Number(e.target.value), "quantity");
                }}
                key={"code-type-value"}
              />
            </CouponLabel> 
          }
          { codeType === "fixed" &&
            <CouponLabel>
              몇 번 사용 가능한 지(-1 시 무제한)
              <CouponInput
                disabled
                type="number"
                placeholder="11"
                value={randomCodeCount}
                onChange={(e) => {
                  setRandomCodeCount(Number(e.target.value));
                  onChangeCoupon_Number(Number(e.target.value), "quantity");
                }}
                key={"code-type-value"}
              />
            </CouponLabel> 
          }
        </CouponInfoSubWrapper>
      </CouponInfoWrapper>
      <UserCouponWrapper column>
        {/* <CreateUserCouponButton onClick={handleCreateUserCoupon}>새 코드 추가하기</CreateUserCouponButton> */}
        <ScrollWrapper>
          <CouponItem />
          { couponUserList?.length > 0 ? 
            // 지정 코드
            <>
              {
                couponUserList.map((v: any) => <UserCouponItem key={v.couponId + v.userId} item={v}/>) 
              }
            </>
            : 
            <>
              사용한 유저 기록이 없습니다.
            </>
          }
          {/* <UserCouponItem  */}

        </ScrollWrapper>
      </UserCouponWrapper>
    </Wrapper>
  );
};

export default CouponEditModal;

const Wrapper = styled(FlexWrapper)`
  padding: 50px 30px;

  background-color: ${colors.WHITE};
  border-radius: 10px;

  justify-content: flex-start;
  align-items: flex-start;

  gap: 15px;
  height: 800px;
  width: 1200px;
`;

const DefaultButton = styled(FlexButton)`
  color: white;
  border-radius: 5px;

  position: absolute;

  padding: 5px 10px;

  white-space: nowrap;
`;

const CloseButton = styled(DefaultButton)`
  background-color: black;

  top: 10px;
  right: 20px;
`;

const CreateUserCouponButton = styled(DefaultButton)`
  background-color: black;

  top: -10px;
  right: 20px;
`;

const SaveButton = styled(DefaultButton)`
  background-color: ${colors.FEEDBACK_GREEN};

  bottom: 10px;
  left: 20px;
`;

const DeleteButton = styled(DefaultButton)`
  background-color: ${colors.FEEDBACK_RED};

  bottom: 10px;
  right: 20px;
`;

const CouponLabel = styled.label`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 5px;

  ${typo({
    size: "14px",
    weight: "500",
    color: colors.BLACK,
  })}

  white-space: nowrap;
  position: relative;
`;

const CouponInput = styled.input`
  padding: 4px 6px;
  background-color: ${colors.BACKGROUND};

  flex: 1;

  ${typo({
    size: "14px",
    height: "26px",
    color: colors.GRAY_90,
  })}
`;

const CouponSelect = styled.select`

`;
const CouponOption = styled.option`

`

const CouponDescription = styled(AutoSizeTextArea)`
  padding: 4px 6px;
  background-color: ${colors.BACKGROUND};
  width: 100%;

  ${typo({
    size: "14px",
    height: "26px",
    color: colors.GRAY_90,
  })}
`;


const CouponInfoWrapper = styled(FlexWrapper)`
  padding: 20px 30px;
  height: 300px;
  width: 100%;

  background-color: ${colors.LIGHT_BLUE_10};
  border-radius: 10px;

  justify-content: flex-start;
  align-items: flex-start;

  gap: 15px;
`;

const UserCouponWrapper = styled(FlexWrapper)`
  height: 380px;
  width: 100%;
`;

const ScrollWrapper = styled(FlexWrapper)`
  width: 100%;

  padding: 0px 20px 20px 20px;
  margin-top: 20px;

  /* flex-direction: column; */
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
  overflow-y: auto;

  gap: 15px;
`;


const CouponInfoSubWrapper = styled(FlexWrapper)`
  gap: 15px;
  align-items: flex-start;
  justify-content: flex-start;
`;