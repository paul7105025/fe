'use client';

import React from "react";
import styled, { keyframes } from "styled-components";

import {
  colors,
  FlexButton,
  FlexWrapper,
  SmallTag,
  typo,
} from "@wrtn/ui/styles";
import { TOOL_ICON_SELECTOR, TOOL_TAG_SELECTOR } from "@wrtn/ui/types";
import { Carousel } from "@wrtn/ui/assets";
import { dateResultFormat, ToolType, userType } from "@wrtn/core";
import { useRouter } from "next/navigation";

interface HistoryCardProps {
  user: userType
  tool: ToolType
  inputs: string
  options: string[]
  output: string
  createdAt: string
  reportReason: string
  isBanned: boolean
  ban: (userId: string, isBan: boolean) => void
}

export const HistoryCard = ({
  user,
  tool,
  inputs,
  options,
  output,
  createdAt,
  reportReason,
  isBanned = false,
  ban,
}: HistoryCardProps) => {
  const router = useRouter();

  const [isCollapsed, setIsCollapsed] = React.useState(false);

  const historyDate = React.useMemo(() => new Date(createdAt), [createdAt]);

  const handleBan = React.useCallback(() => {
    if (!isBanned && window.confirm("정말로 차단하시겠습니까?")) {
      ban(user._id, false);
    } else if (isBanned && window.confirm("정말로 차단을 해제하시겠습니까?")) {
      ban(user._id, true);
    }
  }, [isBanned]);

  const integratedInputs = React.useMemo(() => {
    let result: string[] = [];

    let inputIndex = 0;
    let optionIndex = 0;

    tool?.forms?.forEach((form: any) => {
      if (form.type === "option_chips") {
        result.push(options[optionIndex]);
        optionIndex++;
      } else {
        result.push(inputs[inputIndex]);
        inputIndex++;
      }
    });

    return result;
  }, [tool, inputs, options]);

  React.useEffect(() => {
    if (reportReason.length > 0) {
      setIsCollapsed(true);
    }
  }, [reportReason]);

  return (
    <Wrapper
      isOutput={!!output}
      isReported={reportReason.length > 0 ? true : false}
    >
      <RowWrapper>
        <ToolMetaWrapper>
          
          <ToggleItemIcon>{TOOL_ICON_SELECTOR[tool.icon]}</ToggleItemIcon>
          {tool.tag && tool.tag !== "none" && (
            <SmallTag color={TOOL_TAG_SELECTOR[tool.tag]?.color}>
              {TOOL_TAG_SELECTOR[tool.tag]?.name}
            </SmallTag>
          )}
          <ToggleItemLabel selected={false}>{tool.name}</ToggleItemLabel>
          <DateSpan>{dateResultFormat(historyDate.toString())}</DateSpan>
        </ToolMetaWrapper>
        <UserBlockButton onClick={handleBan} isBanned={isBanned}>
          {isBanned ? "차단 해제" : "차단"}
        </UserBlockButton>
      </RowWrapper>
      {tool?.forms?.map((form: any, index: number) => {
        return (
          <ColumnWrapper key={form._id}>
            <FormName>{form.label}</FormName>
            <FormInput>{integratedInputs[index]}</FormInput>
          </ColumnWrapper>
        );
      })}
      {reportReason.length > 0 && (
        <ColumnWrapper>
          <FormOutputName style={{ color: colors.HOT_RED }}>
            신고 사유
          </FormOutputName>
          <FormOutput>{reportReason}</FormOutput>
        </ColumnWrapper>
      )}
      {isCollapsed && output && (
        <ColumnWrapper>
          <FormOutputName>아웃풋</FormOutputName>
          <FormOutput>{output}</FormOutput>
        </ColumnWrapper>
      )}
      <RowWrapper onClick={() => setIsCollapsed(!isCollapsed)}>
        <UserInfoText onClick={() => router.push(`/app/user/${user?._id}`)}>
          {user?.email}
        </UserInfoText>
        {output && (
          <ToggleItemButton>
            {isCollapsed ? <UpCarousel /> : <DownCarousel />}
          </ToggleItemButton>
        )}
      </RowWrapper>
    </Wrapper>
  );
};

const fadeout = keyframes`
  0%{
    opacity: 0;
  }
  100%{
    opacity: 1;
  }
`;

const Wrapper = styled(FlexWrapper)<{ isReported: boolean, isOutput: boolean }>`
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  max-width: 1080px;

  background-color: ${({ isReported, isOutput }) =>
    !isOutput ? colors.TAG_RED : isReported ? colors.GRAY_02 : colors.WHITE};
  padding: 10px 10px 5px;
  gap: 10px;
  border-radius: 5px;

  animation-duration: 0.4s;
  animation-name: ${fadeout};
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: space-between;
`;

const ColumnWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  gap: 7px;
`;

const FormName = styled.span`
  ${typo({
    size: "6px",
    height: "6px",
    weight: "500",
    color: colors.GRAY_70,
  })}

  background-color: ${colors.WHITE};
  padding: 5px 10px;
`;

const FormInput = styled.div`
  width: 100%;
  padding: 8px;
  background-color: ${colors.BACKGROUND};
  ${typo({
    size: "12px",
    height: "16px",
    weight: "normal",
    color: colors.GRAY_80,
  })}
`;

const ToolMetaWrapper = styled(FlexWrapper)`
  justify-content: flex-start;
  gap: 3px;

  background-color: ${colors.WHITE};
  padding: 5px 10px 5px 0px;
`;

const ToggleItemIcon = styled(FlexWrapper)`
  /* width: 18px; */
  height: 16px;

  max-width: 20px;
  > svg {
    height: 16px;
  }
`;

const ToggleItemLabel = styled.p<{ selected: boolean }>`
  flex: 1;
  text-align: left;
  ${(props) =>
    typo({
      size: "14px",
      height: "14px",
      color: props.selected ? colors.GRAY_90 : colors.GRAY_80,
      weight: props.selected ? "600" : "500",
    })};
  min-height: 14px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

const DateSpan = styled.span`
  ${typo({
    weight: "500",
    size: "12px",
    height: "145%",
    color: colors.gray_60,
  })};
`;

const UserBlockButton = styled(FlexButton)<{ isBanned: boolean }>`
  padding: 5px 10px;
  background-color: ${({ isBanned }) =>
    isBanned ? colors.FEEDBACK_GREEN : colors.FEEDBACK_RED};
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.WHITE,
    weight: "500",
  })}
`;

const FormOutputName = styled.span`
  margin-top: 10px;
  ${typo({
    size: "6px",
    height: "6px",
    weight: "bold",
    color: colors.GRAY_70,
  })}
`;

const FormOutput = styled.div`
  width: 100%;
  padding: 8px;
  background-color: ${colors.BACKGROUND};
  ${typo({
    size: "12px",
    height: "16px",
    weight: "normal",
    color: colors.GRAY_90,
  })}
`;

const UserInfoText = styled.span`
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.GRAY_80,
    weight: "500",
  })}

  cursor: pointer;
  &:hover {
    text-decoration: underline;
    color: ${colors.ACTION_BLUE};
  }
`;

const UpCarousel = styled(Carousel)`
  transform: rotate(270deg);
`;

const DownCarousel = styled(Carousel)`
  transform: rotate(90deg);
`;

const ToggleItemButton = styled(FlexButton)``;
