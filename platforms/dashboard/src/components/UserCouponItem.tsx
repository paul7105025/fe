import styled from "styled-components";
import dayjs from 'dayjs';
import { couponType, userCouponType } from "@wrtn/core";
import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import { userCouponStatusRecord } from '@wrtn/core/records'
interface UserCouponItemProps {
  item?: userCouponType;
  openModal?: (item: userCouponType) => void;
}

export const UserCouponItem = ({ item, openModal }: UserCouponItemProps) => {
  const onClick = () => {
    if (item && openModal)
    openModal(item);
  };

  if (!item) {
    return (
      <Header row >
        <CouponLabel>코드</CouponLabel>
        <CouponLabel>사용한 유저</CouponLabel>
        <CouponLabel>얼마나 남았는지</CouponLabel>
        {/* <CouponLabel>쿠폰 코드 : {item.code}</CouponLabel> */}
        <CouponLabel>현재 상태</CouponLabel>
        {/* <CouponLabel>쿠폰 만료일</CouponLabel> */}
        {/* <CouponLabel>쿠폰 상태</CouponLabel> */}
        {/* {item.percent && <CouponLabel>쿠폰 할인율 : {item.percent}</CouponLabel>} */}
        {/* {item.count && <CouponLabel>쿠폰 횟수 : {item.count}</CouponLabel>} */}
      </Header>
    );
  }

  return (
    <Wrapper row onClick={onClick}>
      <CouponLabel>{item.code}</CouponLabel>
      <CouponLabel>{item.userId}</CouponLabel>
      <CouponLabel>{item.count}</CouponLabel>
      {/* <CouponLabel>쿠폰 코드 : {item.code}</CouponLabel> */}
      <CouponLabel>{userCouponStatusRecord[item.status]}</CouponLabel>
      {/* <CouponLabel>{dayjs(item.dueDate).format("YYYY-MM-DD")}</CouponLabel>
      <CouponLabel>{dayjs(item.dueDate).isBefore(new Date()) ? "기간 만료됨" : "배포 중"}</CouponLabel> */}
      {/* {item.percent && <CouponLabel>쿠폰 할인율 : {item.percent}</CouponLabel>} */}
      {/* {item.count && <CouponLabel>쿠폰 횟수 : {item.count}</CouponLabel>} */}
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  background-color: ${colors.WHITE};
  padding: 10px 15px;

  width: 100%;
`;

const Header = styled(Wrapper)`
  background-color: ${colors.LIGHT_BLUE_10};
`

const CouponLabel = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;

  width: 100%;

  ${typo({
    size: "14px",
    weight: "500",
    color: colors.BLACK,
  })}

  white-space: nowrap;
  position: relative;
`;

const CouponName = styled.div``;

const CouponDescription = styled.div``;

const CouponType = styled.div``;

const CouponCode = styled.div``;

const CouponDiscount = styled.div``;
