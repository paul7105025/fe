import { dateCalculator, favoriteStoreToolListState } from '@wrtn/core';
import { boxShadow, colors, FilledStar, FlexWrapper, Icon, newColors, newTypo, Star, STORE_ICON_SELECTOR } from '@wrtn/ui';
import styled from 'styled-components'
// import { StoreCardTypeLabel } from './StoreCardTypeLabel';
import React from 'react';
import { formatNumber } from '@wrtn/core';

interface StoreCardProps {
  icon: string,
  type: string,
  title: string,
  description: string,
  author: string,
  createdAt: string,
  viewCount: number,
  genCount: number,
  exportCount: number,
  favoriteCount: number,
  isTemporarySave: boolean,
  isDeleted: boolean,
  isRecommend: boolean,
  onClick: () => void
};

export const StoreCard = ({ 
  icon,
  type,
  title,
  description,
  author,
  createdAt,
  viewCount,
  genCount,
  exportCount,
  favoriteCount,
  isTemporarySave,
  isDeleted,
  isRecommend,
  onClick,
}: StoreCardProps) => {
  return (
    <Wrapper onClick={onClick} isDisabled={isTemporarySave} isRecommend={isRecommend}>
      {/* header */}
      <Content>
      <FlexWrapper justify={"space-between"} align="center" style={{paddingBottom: '17px'}}>
        <IconWrapper>
          {/* @ts-ignore */}
          {STORE_ICON_SELECTOR[icon]}
        </IconWrapper>
        <FlexWrapper>
        </FlexWrapper>

      </FlexWrapper>
      {/* title */}
      <FlexWrapper style={{gap: '9px', paddingBottom: '12px'}}>
        {/* <StoreCardTypeLabel type={type}/> */}
        <Title>{title}</Title>
      </FlexWrapper>
      {/* description */}
      <Desc>
        { description }
      </Desc>
      {/* author */}
      <Author>
        { author }
      </Author>
      </Content>
      {/* divider */}
      <Divider isDisabled={isTemporarySave}/>
      {/* footer */}
      <Footer justify="space-between">
        <FlexWrapper style={{gap: '12px'}}>
          <Count><Icon icon="eye" size={13}/> {formatNumber(viewCount)}</Count>
          <Count><Icon icon="star" size={13}/> {formatNumber(favoriteCount)}</Count>
          <Count><Icon icon="play" size={13}/> {formatNumber(genCount)}</Count>
          <Count><Icon icon="external-link-alt" size={13}/> {formatNumber(exportCount)}</Count>
        </FlexWrapper>
        <CreateAt>
          { dateCalculator(createdAt.toString()) }
        </CreateAt>
      </Footer>
    </Wrapper>
  )
}

export default StoreCard;

const Wrapper = styled.div<{ isDisabled: boolean, isRecommend: boolean }>`
  max-height: 243px;
  min-width: 342px;
  border-radius: 8px;
  border: ${props => props.isRecommend ? `5px solid #FFCF24`: `1px solid ${colors.WHITE_BOLDER_LINE}`};
  background: ${props => props.isDisabled ? newColors.GRAY_400 : newColors.WHITE };

  cursor: pointer;
  :hover {
    ${boxShadow.basic_card_shadow};
  }
`;

const Title = styled.div`
  ${newTypo("heading-18-semi")};
  color: ${newColors.GRAY_800};
  min-height: 18px;
`;

const Desc = styled.div`
  min-height: 52px;
  ${newTypo("body-16-med")};
  color: ${newColors.GRAY_600};
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

const Author = styled.div`
  min-height: 22px;
  ${newTypo("body-14-med")};
  color: ${newColors.GRAY_500};
`;

const Divider = styled.div<{ isDisabled: boolean }>`
  width: 100%;
  height: 1px;
  background-color: ${props => props.isDisabled ? newColors.WHITE : colors.WHITE_BOLDER_LINE};
`;


const Content = styled.div`
  padding: 18px 24px;
`;

const Footer = styled(FlexWrapper)`
  padding: 12px 24px;
`;

const CreateAt = styled.div`
  ${newTypo("body-14-med")};
  color: ${newColors.GRAY_500};
`;

const Count = styled(FlexWrapper)`
  gap: 5px;
  ${newTypo("body-12-med")};
  color: ${newColors.GRAY_600};
`;

const IconWrapper = styled.div`
  min-height: 25px;
`;