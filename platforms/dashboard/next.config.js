const path = require("path")
const svgrloader = require("@wrtn/loaderconfig");
const { dependencies } = require("./package.json");

const packages = []
const localDependencies = Object.keys(dependencies).filter(v => v.startsWith('@wrtn/'));

localDependencies.forEach(v => packages.push(path.dirname(require.resolve(v))));
/** @type {import('next').NextConfig} */
const nextConfig = {
  transpilePackages: ["@wrtn/ui", "@wrtn/core"],
  compiler: {
    styledComponents: true,
  },
  reactStrictMode: true,
  experimental: {
    appDir: true
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  webpack: (config, context) => {
    // const { isServer, defaultLoaders } = context;
    // const match = defaultLoaders;
    // console.log(config.output)
    // config.output.publicPath='auto'
    config.module.rules.push(svgrloader);
    // console.log(config.resolve.alias);
    // console.log(defaultLoaders)
    // console.log(config.module)
    // if (!defaultLoaders.loader) defaultLoaders.loader = {}
    // console.log(defaultLoaders)
    // if (!defaultLoaders.include) defaultLoaders.loader.include = ''
    // console.log(defaultLoaders)

    // console.log(config.module.rules)
    // console.log
    // console.log(config.module.rules.find(v => v?.issuerLayer === 'app-client'))
    // const appClient = config.module.rules.find(v => v?.issuerLayer === 'app-client');
    // config.module.rules.find(v => v?.issuerLayer === 'app-client').include = [];
    // console.log(config.module.rules.find(v => v?.issuerLayer === 'app-client').include)
    // console.log(match.options.nextConfig.transpilePackages)
    // console.log(defaultLoaders)
    // console.log(config.module)
    // console.log(config.module.rules)
    // const include = Array.isArray(match.loader.include) ? match.loader.include : [match.loader.include];

    // match.loader.include = include.concat(localDependencies)
    // config.module.rules.find(v => v?.issuerLayer === 'app-client').include = config.module.rules.find(v => v?.issuerLayer === 'app-client').include.concat(packages)

    // console.log(match.loader)
    return config;
  }
}

module.exports = nextConfig
