import React from "react";
import { useRecoilState } from "recoil";

import { accessTokenState } from "../../stores";
import { syncHeader, addInterceptors } from "../../services";
import { useLoginDialog } from "./useLoginDialog";
import { useModalOpen } from "..";
import { useAlert } from "react-alert";
import { SERVICE_UNAVAILABLE_MESSAGE } from "../../constants/error";

export const useSync = () => {
  const [accessToken, setAccessToken] = useRecoilState(accessTokenState);
  const { handleOpen } = useLoginDialog();
  const banAbuseModal = useModalOpen({ modalId: "banAbuseModal" });
  const alert = useAlert();

  React.useEffect(() => {
    syncHeader({
      key: "Authorization",
      value: `Bearer ${accessToken}`,
    });
    addInterceptors(setAccessToken, handleOpen, banAbuseModal.open, (response) => {
      alert.removeAll();
      if (response.data.message.includes("정책상")) {
        alert.show(SERVICE_UNAVAILABLE_MESSAGE);
      } else {
        alert.show("새벽 시간에는 서버 정비 작업으로 인해 간헐적으로 생성이 원활하지 않을 수 있습니다. 이용에 불편함을 드려 죄송합니다.")
      }
    }, (response) => {
      alert.removeAll();
      if(response.data.message.includes("과도한")) {
        alert.show(response.data.message);
      }
    });
  }, []);
};
