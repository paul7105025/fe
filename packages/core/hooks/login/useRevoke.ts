import React from "react";
import { useEvent } from "..";
import { useLogout } from "./useLogout";

const loadGoogle = () => {
  return new Promise((resolve) => {
    const js = document.createElement("script");
    js.id = "google-sdk";
    js.src = "https://accounts.google.com/gsi/client";
    js.onload = resolve;
    document.body.append(js);
  });
};

const loadKakao = () => {
  return new Promise((resolve) => {
    const js = document.createElement("script");

    js.id = "kakao-sdk";
    js.src = "//developers.kakao.com/sdk/js/kakao.min.js";
    js.onload = resolve;

    document.body.append(js);
  });
};

const googleRevoke = async (sub: string, callback) => {
  window.focus();
  // @ts-ignore
  window.google.accounts.id.revoke(sub, callback);
};

const naverRevoke = async (callback) => {
  // TODO: 서버에서 회원 탈퇴 API 필요
  window["naver"] = null;
  return await callback();
};

// 향후 지원하지 않는 방식일 수 있습니다.
const kakaoRevoke = async (callback) => {
  await loadKakao();
  // @ts-ignore
  window.Kakao.init("1a46f08d916c3f63913205110dc509ab");
  // @ts-ignore
  window.Kakao.Auth.login({
    throughTalk: true,
    persistAccessToken: true,
    success: () => {
      // @ts-ignore
      window.Kakao.API.request({
        url: "/v1/user/unlink",
      }).then(callback);
      callback();
    },
  });
};

const useRevoke = () => {
  const logout = useLogout();
  const { collectUserProperties } = useEvent();

  return React.useCallback(
    async (user, sub?: string | undefined) => {
      if (user?.provider === "google")
        return googleRevoke(sub || "", (d) => {
          collectUserProperties("withdraw_date", new Date());
          logout(user);
        });
      if (user?.provider === "naver")
        return naverRevoke(() => {
          collectUserProperties("withdraw_date", new Date());
          logout(user);
        });
      if (user?.provider === "kakao")
        return kakaoRevoke((res) => {
          collectUserProperties("withdraw_date", new Date());
          logout(user);
        });
    },
    [logout]
  );
};

export default useRevoke;
