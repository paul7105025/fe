import { googleLogout } from "@react-oauth/google";
import { syncHeader } from "../../services";
import { accessTokenState, userState } from "../../stores/login";
import { removeCookie } from "../../utils/cookies";
import React from "react";
import { useResetRecoilState, useSetRecoilState } from "recoil";
import { userType } from "../../types/user";
import { chatListState, currentChatState } from "../../stores/chat";

// 향후 지원하지 않는 방식일 수 있습니다.
const naverLogout = async () => {
  const win = window.open("https://nid.naver.com/nidlogin.logout", "_blank");
  return setTimeout(() => {
    if (win) {
      win.close();
    }
  }, 50);
};

// 향후 지원하지 않는 방식일 수 있습니다.
const kakaoLogout = async () => {
  const win = window.open(
    "https://accounts.kakao.com/logout?continue=https%3A%2F%2Fcs.kakao.com%2Fhelps%3Fcategory%3D166%26locale%3Dko%26service%3D52",
    "_blank"
  );
  return setTimeout(() => {
    if (win) win.close();
  }, 50);
};

export const useLogout = () => {
  const setUser = useSetRecoilState(userState);
  const setAccessToken = useSetRecoilState(accessTokenState);
  const resetChatList = useResetRecoilState(chatListState);
  const resetCurrentChat = useResetRecoilState(currentChatState);

  return async (user: userType | null) => {
    syncHeader({
      key: "Authorization",
      value: "",
    });

    removeCookie("access_token", {
      domain: "wrtn.ai"
    });
    removeCookie("access_token");
    removeCookie("refresh_token", {
      domain: "wrtn.ai"
    });
    removeCookie("refresh_token");
    setAccessToken("");
    setUser(null);
    resetChatList();
    resetCurrentChat();

    if (user?.provider === "google") {
      return googleLogout();
    }
    if (user?.provider === "naver") return await naverLogout();
    if (user?.provider === "kakao") return await kakaoLogout();
  };
};
