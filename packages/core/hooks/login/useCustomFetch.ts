import axios from "axios";
import { useRecoilState, useRecoilValue } from "recoil";
import { useLoginDialog } from "./useLoginDialog";
import { accessTokenState, userState } from "../../stores";
import { getCookie } from "../../utils";
import { syncHeader } from "../../services";

const baseURL = process.env.REACT_APP_LAMBDA_URL ?? "http://localhost:3001";
const serverUrl =
  process.env.NEXT_PUBLIC_API_SERVER_URL ||
  process.env.REACT_APP_API_SERVER_URL;

const store_server_url =
  process.env.NEXT_PUBLIC_STORE_API_SERVER_URL ||
  process.env.REACT_APP_STORE_API_SERVER_URL;

type OnRequestType = (url: string, options: RequestInit) => { url; options };
type OnResponseType = (
  url: string,
  options: RequestInit,
  res: Response
) => Promise<Response>;

function fetchInterceptor(
  onRequest: OnRequestType,
  onResponse: OnResponseType
) {
  return function (url: string, options: any) {
    if (onRequest) {
      const onRequestResult = onRequest(url, options);

      url = onRequestResult.url || url;
      options = onRequestResult.options || options;
    }

    return fetch(url, options).then((response) => {
      if (onResponse) {
        return onResponse(url, options, response);
      }

      return response;
    });
  };
}

export const useCustomFetch = () => {
  const [accessToken, setAccessToken] = useRecoilState(accessTokenState);
  const { handleOpen } = useLoginDialog();
  const refreshToken = getCookie("refresh_token", {});
  const user = useRecoilValue(userState);

  const customStudioFetch = fetchInterceptor(
    (_url, _options) => {
      const fullUrl = `${store_server_url}${_url}&user=${user ? user.email : ""}`;
      const { headers, ...options } = _options;

      return {
        url: fullUrl,
        options: {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
            ...headers,
          },
          ...options,
        },
      };
    },
    async (url, options, res) => {
      if (!res.ok && res.status === 401) {
        const response = await axios
          .post(serverUrl + "/auth/refresh", null, {
            headers: {
              Refresh: refreshToken,
            },
          })
          .then((res) => res)
          .catch((err) => err?.response || err);

        if (response?.status === 201) {
          const { accessToken } = response.data.data;

          setAccessToken(accessToken);
          syncHeader({
            key: "Authorization",
            value: `Bearer ${accessToken}`,
          });

          // console.log(url)
          const partialUrl = url.replace(store_server_url ?? "", "");
          return customStudioFetch(partialUrl, {
            ...options,
            headers: { Authorization: `Bearer ${accessToken}` },
          });
        } else {
          handleOpen();
          // console.log(res)
          return Promise.reject(res)
        }
      }

      return res;
    }
  );

  const customFetch = fetchInterceptor(
    (_url, _options) => {
      const fullUrl = `${baseURL}${_url}&user=${user ? user.email : ""}`;
      const { headers, ...options } = _options;

      return {
        url: fullUrl,
        options: {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
            ...headers,
          },
          ...options,
        },
      };
    },
    async (url, options, res) => {
      if (!res.ok && res.status === 401) {
        const response = await axios
          .post(serverUrl + "/auth/refresh", null, {
            headers: {
              Refresh: refreshToken,
            },
          })
          .then((res) => res)
          .catch((err) => err?.response || err);

        if (response?.status === 201) {
          const { accessToken } = response.data.data;

          setAccessToken(accessToken);
          syncHeader({
            key: "Authorization",
            value: `Bearer ${accessToken}`,
          });

          // console.log(url)
          const partialUrl = url.replace(baseURL, "");
          return customFetch(partialUrl, {
            ...options,
            headers: { Authorization: `Bearer ${accessToken}` },
          });
        } else {
          handleOpen();
        }
      }

      return res;
    }
  );

  return {
    customFetch,
    customStudioFetch,
  };
};
