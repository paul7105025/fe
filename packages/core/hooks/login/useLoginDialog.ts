import { useRecoilState } from "recoil";
import { useEvent } from "..";
import { loginDialogState } from "../../stores/login";

export const useLoginDialog = () => {
  const [open, setOpen] = useRecoilState(loginDialogState);
  const { collectEvent } = useEvent();

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    setOpen((c) => !c);
  };

  const handleOpen = () => {
    setOpen(true);

    const position = window.location.pathname.startsWith("/tool")
      ? "tool"
      : window.location.pathname.startsWith("/editor")
      ? "editor"
      : window.location.pathname.startsWith("/about")
      ? "about"
      : window.location.pathname.startsWith("/plugins")
      ? "plugins"
      : window.location.pathname.startsWith("/store/")
      ? "store"
      : window.location.pathname === "/"
      ? "main"
      : null;

    if (!position) return;

    collectEvent("view_start_modal", {
      position: position,
    });
  };

  return {
    openDialog: open,
    handleClick,
    handleClose,
    handleOpen,
  };
};
