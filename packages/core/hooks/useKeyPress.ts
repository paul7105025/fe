import React from "react";

export const useKeyPress = (targetKey: string, callback: Function) => {
  function downHandler({ key }) {
    if (key === targetKey) {
      callback();
    }
  }

  const upHandler = ({ key }) => {
    if (key === targetKey) {
      callback();
    }
  };

  React.useEffect(() => {
    window.addEventListener("keydown", downHandler);
    window.addEventListener("keyup", upHandler);

    return () => {
      window.removeEventListener("keydown", downHandler);
      window.removeEventListener("keyup", upHandler);
    };
  }, []);
};
