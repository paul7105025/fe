import React from "react";
import { useRecoilState } from "recoil";

import { recaptchaState } from "../stores";

export const useReCaptcha = () => {
  const [recaptcha, setRecaptcha] = useRecoilState(recaptchaState);

  React.useEffect(() => {
    setRecaptcha((r) => ({
      ...r,
      isVerified: false,
    }));
  }, [window.location]);

  return {
    isVerified: recaptcha.isVerified,
    openRecaptcha: recaptcha.openRecaptcha,
    registerByRecaptcha: recaptcha.registerByRecaptcha,
  };
};
