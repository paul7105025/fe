import React from "react";

export const useNonInitialEffect = (effect: Function, deps: [...any]) => {
  // const initialRender = React.useRef(true);
  const [initialRender, setInitialRender] = React.useState(true);

  React.useEffect(() => {
    let effectReturns = () => {};

    if (initialRender) {
      setInitialRender(false);
    } else {
      effectReturns = effect();
    }

    if (effectReturns && typeof effectReturns === "function") {
      return effectReturns;
    }
  }, deps);
};
