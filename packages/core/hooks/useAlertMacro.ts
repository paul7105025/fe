import React from "react";
import { atom, useRecoilState } from "recoil";

const macroCountState = atom({
  key: "macroCountState",
  default: 0,
});

export const useAlertMacro = () => {
  const [count, setCount] = useRecoilState(macroCountState);
  const [showMacroModal, setShowMacroModal] = React.useState(false);

  const savedValue = React.useRef<string>("");

  const handleMacroModal = (value: string) => {
    if (value !== savedValue.current) {
      savedValue.current = value;
      setCount(0);
      return;
    }

    setCount((c) => c + 1);
  };

  const closeMacroModal = () => setCount(0);

  React.useEffect(() => {
    if (count === 10) {
      setShowMacroModal(true);
    }
  }, [count]);

  return {
    showMacroModal,
    handleMacroModal,
    closeMacroModal,
  };
};
