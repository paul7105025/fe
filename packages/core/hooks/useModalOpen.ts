import { useRecoilState } from "recoil";
import { modalResolveState, modalOpenState } from "../stores/modal";

export const useModalOpen = ({ modalId }) => {
  const [isOpen, setIsOpen] = useRecoilState(modalOpenState(modalId));
  const [resolved, setResolved] = useRecoilState(modalResolveState);

  const close = () => {
    setIsOpen(false);
  };
  const open = () => {
    setIsOpen(true);
  };
  const resolve = () => {
    setResolved(true);
  };

  return {
    isOpen,
    resolved,
    setResolved,
    close,
    open,
    resolve,
  };
};
