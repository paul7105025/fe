import React from "react";

export const useTimeout = (
  callback: Function,
  delay: number = 1000,
  deps: [any]
) => {
  const callbackRef = React.useRef(callback);

  React.useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  React.useEffect(() => {
    const id = setTimeout(() => callbackRef.current(), delay);
    return () => clearTimeout(id);
  }, [...deps]);
};

export const useInterval = (callback: Function, delay: number = 1000) => {
  const callbackRef = React.useRef(callback);

  React.useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  React.useEffect(() => {
    if (delay !== null) {
      const id = setInterval(() => callbackRef.current(), delay);
      return () => clearInterval(id);
    }
  }, [delay]);
};
