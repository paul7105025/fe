import { useClickOutside } from ".";
import React from "react";

type Position = {
  top?: number;
  left?: number;
  bottom?: number;
  right?: number;
};

interface useHoverMenuProps {
  position?: Position;
  closeCallBack?: Function;
  disableOutsideClick?: boolean;
}

const useHoverMenu = ({
  position,
  closeCallBack,
  disableOutsideClick,
}: useHoverMenuProps) => {
  const [hover, setHover] = React.useState(false);
  const parentRef = React.useRef<any>(null);
  const childRef = React.useRef<any>(null);

  const handleMouseOver = () => {
    setHover(true);
  };

  const handleMouseOut = (callback?: Function) => {
    setHover(false);
    if (callback) callback();
  };

  const handleClick = () => {
    setHover((c) => !c);
  };

  useClickOutside([childRef, parentRef], "mousedown", () => {
    handleMouseOut(closeCallBack);
  });

  const childPosition = {
    ...(position?.top && {
      top: parentRef.current?.getBoundingClientRect().top + position?.top,
    }),
    ...(position?.left && {
      left: parentRef.current?.getBoundingClientRect().left + position?.left,
    }),
    ...(position?.bottom && {
      bottom:
        parentRef.current?.getBoundingClientRect().bottom + position?.bottom,
    }),
    ...(position?.right && {
      left: parentRef.current?.getBoundingClientRect().right + position?.right,
    }),
  };

  const open = (renderFunc) => {
    if (!hover) return null;

    return renderFunc({ position: childPosition, childRef });
  };

  return {
    parentRef,
    childRef,
    handleMouseOut,
    handleMouseOver,
    handleClick,
    // position: childPosition,
    hover,
    open,
  };
};

export default useHoverMenu;
