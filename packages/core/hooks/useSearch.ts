import React from "react";
import * as hangul from "hangul-js";

// import { TOOL_TAG_RECORD, ToolType, useFavoriteIdList } from "@wrtn/core";
// import { search } from "hangul-js";
import { ToolType } from "../types/tool";
import { TOOL_TAG_RECORD } from "../records";
import { useFavoriteIdList } from "./useFavoriteIdList";

/**
 * 입력한 키워드를 포함한 툴을 찾습니다.
 * @param {string} searchKeyword - 찾고자 하는 키워드
 * @param {Array<ToolType>} toolList - 전체 툴 리스트
 * @returns {Array<ToolType>} - 키워드가 포함된 툴 리스트
 */

export const useToolListSearch = (
  searchKeyword: string,
  toolList: Array<ToolType>
): Array<ToolType> => {
  const [searchResult, setSearchResult] = React.useState<Array<ToolType>>([]);
  const timerRef = React.useRef<NodeJS.Timeout>();

  const searcher = React.useMemo(
    () => new hangul.Searcher(searchKeyword.toLowerCase()),
    [searchKeyword]
  );

  const search = React.useCallback(() => {
    timerRef.current = setTimeout(() => {
      setSearchResult(
        toolList.filter(
          (item) =>
            searcher.search(item.name.toLowerCase()) > -1 ||
            searcher.search(TOOL_TAG_RECORD[item?.tag]?.name.toLowerCase()) > -1
        )
      );
    }, 10);
  }, [searcher, toolList]);

  React.useEffect(() => {
    search();
    return () => clearTimeout(timerRef.current);
  }, [searcher]);

  return searchResult;
};

export const useCategorySearch = (
  searchKeyword: string,
  toolList: Array<ToolType>,
  categoryList: Array<{
    category: string;
    label: string;
  }>
): Array<{
  category: string;
  data: Array<ToolType>;
}> => {
  const searched = useToolListSearch(searchKeyword, toolList);

  const searchedCategory = React.useMemo(() => {
    return categoryList
      .map((item) => ({
        category: item.category,
        data: searched.filter((tool) => tool.category === item.category),
      }))
      .filter((category) => category.data.length > 0);
  }, [searched, categoryList]);

  return searchedCategory;
};

export const useFavoriteToolListSearch = (
  searchKeyword: string,
  allToolList: Array<ToolType>
): Array<ToolType> => {
  const { favoriteIdList } = useFavoriteIdList();

  const searched = useToolListSearch(
    searchKeyword,
    allToolList
  );

  const searchedFavorite = React.useMemo(
    () => searched.filter((tool) => favoriteIdList.find(v => v === tool._id))
  , [searched, favoriteIdList]);

  return searchedFavorite;
};

/**
 * 입력한 키워드를 포함한 툴 리스트 중 최근에 추가된 것들을 보여줍니다.
 * @param {string} searchKeyword
 * @param {Array<ToolType>} toolList
 * @returns {Array<ToolType>} - 최근에 추가된 툴 리스트
 *
 */

export const useNewToolListSearch = (
  searchKeyword: string,
  toolList: Array<ToolType>
): Array<ToolType> => {
  const searched = useToolListSearch(searchKeyword, toolList);

  const searchedNew = React.useMemo(
    () => searched.filter((tool) => tool.label === "new"),
    [searched]
  );

  return searchedNew;
};
