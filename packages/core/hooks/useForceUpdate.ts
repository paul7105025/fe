import React from "react";

export const useForceUpdate = <T>(
  originValue: T,
  currentValue: T,
  setValueFunc: (value: T) => void
) => {
  React.useEffect(() => {
    if (originValue && originValue !== currentValue) {
      setValueFunc(originValue);
    }
  }, [originValue]);
};
