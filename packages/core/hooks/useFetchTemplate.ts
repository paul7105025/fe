import { useSetRecoilState } from "recoil";
import { getTemplate } from "../services/template"
import { templateToolListState } from "../stores/tool";


export const useFetchTemplate = () => {
  const setTemplateToolList = useSetRecoilState(templateToolListState);

  const fetchTemplate = async (successCallback?: () => void) => {
    const res = await getTemplate();
    if (res?.status === 200) {
      setTemplateToolList(res?.data?.data || []);
      if (successCallback) successCallback();
    }
  }
  return {
    fetchTemplate
  }
}

export default useFetchTemplate;