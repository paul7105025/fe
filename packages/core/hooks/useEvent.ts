import mixpanel from 'mixpanel-browser';
import { useRecoilValue } from 'recoil';
import { mixpanelLoadedState } from '../stores/mixpanel';
import React from 'react';
import { updateChannelUser } from '.';
import { userState } from '../stores/login';
import { fromActiveDate } from '../utils/event';

const isCollect = process.env.REACT_APP_ACTIVE_MIXPANEL === "true" || process.env.NEXT_PUBLIC_ACTIVE_MIXPANEL === "true";

const getUserAgent = () => {
  // web / window_desktop / mac_desktop
  // 주의! safari 등 일부 브라우저에서는 지원하지 않는 기능.
  // 데스크탑 앱은 chrome based라 os 추적용으로만 사용합니다.
  // "Android", "Chrome OS", "Chromium OS", "iOS", "Linux", "macOS", "Windows", "Unknown"
  const ug = window.navigator.userAgent;
  // @ts-ignore
  const ugd = window.navigator?.userAgentData;
  if (!ugd) return "web"; // userAgentData 지원하지 않는 브라우저는 web으로 간주
  if (!ug.includes("wrtn-desktop/")) return "web"; // desktop으로 접속하지 않으면 web으로 간주
  if (ugd.platform === "Windows") return "window_desktop"
  if (ugd.platform === "macOS") return "mac_desktop"
  return "web";
}

export const useEvent = () => {
  const isMixpanelLoadedDep = useRecoilValue(mixpanelLoadedState);
  const user = useRecoilValue(userState);

  const initEvent = (mpToken: string, callback?: () => void) => {
    mixpanel.init(mpToken ,{
      loaded: () => {
        if (callback) callback();
      }
    })
  }

  const collectUserProperties_once = (name: string, to: any) => {
    if (!isCollect) { return }
    if (isMixpanelLoaded()) {
      mixpanel.people.set_once(name, to);
    }
    updateChannelUser({
      profile: {
        [name]: to
      }
    });
  }

  const collectUserProperties = (name: string, to: any) => {
    if (!isCollect) { return }
    if (isMixpanelLoaded()) {
      mixpanel.people.set(name, to);
    }
    updateChannelUser({
      profile: {
        [name]: to
      }
    });
  }

  const collectUserProperties_increment = (name: string, val: number) => {
    if (!isCollect) { return }
    if (isMixpanelLoaded()) {
      mixpanel.people.increment(name, val);
    }
    // TODO: increment 데이터는 수집을 못해요 
    // updateChannelUser({
    //   profile: {
    //     [name]: val
    //   }
    // });
  }

  const collectEvent = (name: string, properties?: any) => {
    if (!isCollect) { return }
    if (isMixpanelLoaded()) {
      getUserAgent();
      mixpanel.track(name, {
        platform_type: getUserAgent(),
        newbie: fromActiveDate(user?.createdAt),
        ...properties
      })
    }
  }

  const isMixpanelLoaded = ():boolean => {
    if (!isCollect) { return false; }
    try {
      mixpanel?.get_distinct_id()
      return true;
    } catch (err) {
      return false;
    }
  }


  return {
    initEvent,
    collectEvent,
    collectUserProperties, 
    collectUserProperties_once, 
    collectUserProperties_increment
  };
}

export default useEvent;