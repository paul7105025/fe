import _ from "lodash";
import React from "react";
import { useRecoilValue } from "recoil";
import { TOOL_CATEGORY_RECORD } from "../records";
import { bundleListState } from "../stores/bundle";
import { stepListState } from "../stores/step";
import { recommendToolState, toolListState } from "../stores/tool";
import { ToolType } from "../types/tool";
import { useFavoriteIdList } from "./useFavoriteIdList";
import { useToolListSearch } from "./useSearch";

export const useFetchToolList = () => {
  const toolList = useRecoilValue(toolListState);
  const stepList = useRecoilValue(stepListState);
  const bundleList = useRecoilValue(bundleListState);

  const recommendToolList = useRecoilValue(recommendToolState);

  const allToolList: ToolType[] = React.useMemo(() => {
    return [...stepList, ...bundleList, ...(toolList || [])];
  }, [toolList, stepList, bundleList]);

  const chatToolList: ToolType[] = React.useMemo(() => {
    return toolList || [];
  }, [toolList]);

  return { allToolList, recommendToolList, chatToolList };
};

export const useCategoryList = (allToolList: ToolType[]) => {
  const categoryList = React.useMemo(() => {
    return _.uniqBy(
      allToolList.map((tool) => ({
        category: tool.category,
        label: TOOL_CATEGORY_RECORD[tool.category],
      })),
      "category"
    );
  }, [allToolList]);

  const tempCategoryList = [
    {
      category: "blog",
      label: "블로그",
    },
    {
      category: "marketing",
      label: "마케팅",
    },
    {
      category: "self_employment",
      label: "쇼핑몰",
    },
    {
      category: "student",
      label: "학생",
    },
    {
      category: "work",
      label: "업무용",
    },
    {
      category: "youtube",
      label: "유튜브",
    },
    {
      category: "normal",
      label: "범용/일반",
    },
    {
      category: "misc",
      label: "기타",
    },
  ];

  if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
    return { categoryList };
  } else {
    return { categoryList: tempCategoryList };
  }
};

export const useHomeCategory = (
  allToolList: ToolType[],
  recommendToolList: ToolType[],
  currentCategory: string,
  searchInput: string
) => {
  const searchedAllToolList = useToolListSearch(searchInput, allToolList);
  const searchedRecommendToolList = useToolListSearch(
    searchInput,
    recommendToolList
  );

  const { favoriteIdList } = useFavoriteIdList();

  const currentCategoryToolList = React.useMemo(() => {
    switch (currentCategory) {
      case "recommend":
        return _.filter(allToolList, { label: "new" });
      case "favorite":
        return allToolList.filter((tool) => favoriteIdList.includes(tool._id));
      default: // other category
        return allToolList.filter((tool) => tool.category === currentCategory);
    }
  }, [allToolList, favoriteIdList, currentCategory]);

  return {
    searchedAllToolList,
    currentCategoryToolList,
    searchedRecommendToolList,
  };
};
