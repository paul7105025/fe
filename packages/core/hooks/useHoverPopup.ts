import { useRef, useState } from "react";


export const useHoverPopup = () => {
  const hoverRef = useRef<any>();
  const [isHover, setIsHover] = useState<boolean>(false);

  const onMouseEnter = () => {
    setIsHover(true);
  }

  const onMouseLeave = () => {
    setIsHover(false);
  }

  const open = (renderFunc) => {
    if (!isHover) return null;
    return (renderFunc({

    }))
  }

  return {
    hoverRef,
    open,
    onMouseEnter,
    onMouseLeave
  }
}


export default useHoverPopup;