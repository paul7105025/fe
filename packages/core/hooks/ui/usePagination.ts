import React from "react";

export const usePagination = ({ maxPage }: { maxPage: number }) => {
  const [page, setPage] = React.useState(1);

  const isPrevDisabled = React.useMemo(() => {
    return page === 1 ? false : true;
  }, [page]);

  const isNextDisabled = React.useMemo(() => {
    return page < maxPage ? false : true;
  }, [page, maxPage]);

  const handleNext = () => {
    if (!isNextDisabled) {
      setPage(page + 1);
    }
  };

  const handlePrev = () => {
    if (!isPrevDisabled) {
      setPage(page - 1);
    }
  };

  const getList = React.useCallback(() => {
    return Array.from({ length: maxPage }, (v, i) => i + 1);
  }, [maxPage]);

  return {
    page,
    isNextDisabled,
    isPrevDisabled,
    setPage,
    handleNext,
    handlePrev,
    getList,
  };
};
