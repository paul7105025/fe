import React from "react";

export const useScrollLock = (lock: string) => {
  React.useEffect(() => {
    document.body.classList[lock ? "add" : "remove"]("body-scroll-lock");

    return () => {
      document.body.classList.remove("body-scroll-lock");
    };
  }, [lock]);
};
