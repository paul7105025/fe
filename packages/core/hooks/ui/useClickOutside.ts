import React from "react";

export const useClickOutside = (
  ref: React.MutableRefObject<any> | React.MutableRefObject<any>[],
  event: string,
  callBack: Function
) => {
  React.useEffect(() => {
    function handleClickOutside(event: { target: any }) {
      if (Array.isArray(ref)) {
        let isOutside = true;

        ref.forEach((ref) => {
          if (ref.current && ref.current.contains(event.target)) {
            isOutside = false;
          }
        });

        if (isOutside) {
          callBack();
        }
      } else {
        if (ref?.current && !ref?.current?.contains(event.target)) {
          callBack();
        }
      }
    }
    // Bind the event listener
    document.addEventListener(event, handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener(event, handleClickOutside);
    };
  }, [ref, callBack]);
};
