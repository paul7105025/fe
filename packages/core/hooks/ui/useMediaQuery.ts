import { useEffect, useState } from "react";

const useUnmountedMediaQuery = (query: string): boolean => {
  const getMatches = (query: string): boolean => {
    // Prevents SSR issues
    if (typeof window !== "undefined") {
      return window.matchMedia(query).matches;
    }
    return false;
  };

  const [matches, setMatches] = useState<boolean>(getMatches(query));

  function handleChange() {
    setMatches(getMatches(query));
  }

  useEffect(() => {
    const matchMedia = window.matchMedia(query);

    // Triggered at the first client-side load and if query changes
    handleChange();

    // Listen matchMedia

    matchMedia.addEventListener("change", handleChange);

    return () => {
      matchMedia.removeEventListener("change", handleChange);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);

  return matches;
};

export const useMediaQuery = (query: string) => {
  const [mounted, setMounted] = useState(false);
  const value = useUnmountedMediaQuery(query);
  useEffect(() => {
    setMounted(true);
  }, []);

  return mounted ? value : false;
};

export const useIsMobile = () => {
  return useMediaQuery(`(max-width : 767px)`);
};

export const useIs1440 = () => {
  return useMediaQuery(`(max-width : 1440px)`);
};

export const useIs1280 = () => {
  return useMediaQuery(`(max-width : 1280px)`);
};

export const useIsPreMobile = () => {
  return useMediaQuery(`(max-width : 1160px)`);
};

export const useIs1024 = () => {
  return useMediaQuery(`(max-width : 1023px)`);
};

export const useIs540 = () => {
  return useMediaQuery(`(max-width : 540px)`);
};

export const useIs960 = () => {
  return useMediaQuery(`(max-width : 960px)`);
};

export const useIs480 = () => {
  return useMediaQuery(`(max-width : 480px)`);
};
