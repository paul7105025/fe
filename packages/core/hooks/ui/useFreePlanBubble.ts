import React from "react";

import { useRecoilState, useRecoilValue } from "recoil";

import { useTodayMetric } from "..";

import {
  userState,
  isClosingState,
  isNoCreditModalOpenState,
} from "../../stores";

const FREE_BUBBLE_TRIGGER = 20;

export const useFreePlanBubble = () => {
  const [isNoCreditModalOpen, setIsNoCreditModalOpen] = useRecoilState(
    isNoCreditModalOpenState
  );
  const [isClosing, setIsClosing] = useRecoilState(isClosingState);
  const lastMetric = React.useRef(0);
  const user = useRecoilValue(userState);

  const { todayMetric, updateTodayMetric } = useTodayMetric();

  const closeNoCreditModal = () => {
    setIsClosing(true);
    setTimeout(() => {
      setIsClosing(false);
      setIsNoCreditModalOpen(false);
    }, 490);
  };

  const openNoCreditModal = () => {
    setIsNoCreditModalOpen(true);
  };

  React.useEffect(() => {
    setIsNoCreditModalOpen(false);
    setIsClosing(false);
  }, [window.location.href]);

  React.useEffect(() => {
    if (
      user?.plan === "FREE" &&
      lastMetric.current !== 0 &&
      todayMetric !== 0 &&
      todayMetric % FREE_BUBBLE_TRIGGER === 0 &&
      lastMetric.current !== todayMetric
    ) {
      openNoCreditModal();
    }
    lastMetric.current = todayMetric;
  }, [todayMetric]);

  return {
    isNoCreditModalOpen,
    isClosing,
    closeNoCreditModal,
    updateTodayMetric,
  };
};
