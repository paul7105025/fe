import React from "react";
import _ from "lodash";

import { StepHistoryType, StepToolType } from "../../types";

export const useStep = ({ step }: { step: StepToolType }) => {
  const links = React.useMemo(() => step.links || [], [step]);
  const outputForms = React.useMemo(() => step.outputForms || [], [step]);

  const [stepHistory, setStepHistory] = React.useState<Array<StepHistoryType>>(
    []
  );

  const [currentStep, setCurrentStep] = React.useState<number>(0);

  React.useEffect(() => {
    setCurrentStep(0);
  }, [step]);

  const scanOutputForm = React.useMemo(() => {
    if (outputForms && outputForms.length > 0) {
      return outputForms.map((singleOutputForm) => {
        if (singleOutputForm.kind === "outputToInput") {
          return {
            label: singleOutputForm.label,
            value: _.find(
              stepHistory,
              (singleStepHistory) =>
                singleStepHistory.toolId === singleOutputForm.from.toolId
            )?.outputData,
          };
        } else if (singleOutputForm.kind === "inputToInput") {
          return {
            label: singleOutputForm.label,
            value: _.find(
              stepHistory,
              (singleStepHistory) =>
                singleStepHistory.inputId === singleOutputForm.from.inputId
            )?.inputData,
          };
        } else
          return {
            label: singleOutputForm.label,
            value: "",
          };
      });
    } else return [];
  }, [outputForms, stepHistory]);

  const saveStepHistoryStore = React.useCallback(
    (data: StepHistoryType[]) => {
      setStepHistory((prev) =>
        _.uniqWith(
          [...data, ...prev],
          (a, b) => a.toolId === b.toolId && a.inputId === b.inputId
        )
      );
    },
    [setStepHistory]
  );

  const deleteStepHistoryStore = React.useCallback(
    (outputId: string) => {
      setStepHistory((prev) =>
        prev.filter((item) => {
          return item.outputId !== outputId;
        })
      );
    },
    [setStepHistory]
  );

  const scanLink = React.useCallback(
    (form: any) => {
      const findLink = _.find(links, (link) => link?.to?.inputId === form._id);
      if (findLink && findLink !== undefined) {
        if (findLink.kind === "outputToInput") {
          return {
            ...form,
            value: _.find(
              stepHistory,
              (singleStepHistory) =>
                singleStepHistory.toolId === findLink.from.toolId
            )?.outputData,
          };
        } else if (findLink.kind === "inputToInput") {
          return {
            ...form,
            value: _.find(
              stepHistory,
              (singleStepHistory) =>
                singleStepHistory.inputId === findLink.from.inputId
            )?.inputData,
          };
        } else return form;
      } else {
        return form;
      }
    },
    [links, stepHistory]
  );

  const moveToNextStep = React.useCallback(
    () => setCurrentStep(currentStep + 1),
    [currentStep]
  );

  return {
    currentStep,
    setCurrentStep,

    scanOutputForm,
    stepHistory,
    scanLink,
    saveStepHistoryStore,
    deleteStepHistoryStore,
    moveToNextStep,
  };
};
