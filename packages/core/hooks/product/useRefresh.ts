import { rest } from "lodash";
import React from "react";
import { useSetRecoilState } from "recoil";

import {
  getCouponList,
  getStep,
  getTool,
  getBundle,
  getToolFavorite,
  getToolRecommend,
  getUserWordCount,
  getMetric,
  getUser,
  getChat,
} from "../../services";

import {
  chatListState,
  toolListState,
  toolLoadingState,
  favoriteIdListState,
  recommendToolState,
  stepListState,
  bundleListState,
  couponListState,
  metricState,
  userState,
  blockChatLimitState,
} from "../../stores";

export const useRefresh = () => {
  const setToolList = useSetRecoilState(toolListState);
  const setStepList = useSetRecoilState(stepListState);
  const setBundleList = useSetRecoilState(bundleListState);
  const setChatList = useSetRecoilState(chatListState);

  const setUser = useSetRecoilState(userState);
  const setMetric = useSetRecoilState(metricState);

  const setToolLoading = useSetRecoilState(toolLoadingState);
  const setFavoriteIdList = useSetRecoilState(favoriteIdListState);
  const setRecommendTool = useSetRecoilState(recommendToolState);

  const setCouponList = useSetRecoilState(couponListState);

  const fetchTool = React.useCallback(async () => {
    try {
      setToolLoading(true);

      const toolRes = await getTool();

      if (toolRes?.status === 200) {
        const { data } = toolRes.data;
        setToolList(data);
      }

      const stepRes = await getStep();
      if (stepRes?.status === 200) {
        const { data } = stepRes.data;
        setStepList(data);
      }

      const bundleRes = await getBundle();
      if (bundleRes?.status === 200) {
        const { data } = bundleRes.data;

        setBundleList(data);
      }
    } catch (error) {
      //error
    } finally {
      setToolLoading(false);
    }
  }, []);

  const fetchUser = async () => {
    const res = await getUser();
    if (res?.status === 200 || res?.status === 201) {
      const { data } = res.data;
      setUser(data);
    }
  };

  const fetchFavorite = React.useCallback(async () => {
    const res = await getToolFavorite();
    if (res?.status === 200) {
      const { data } = res.data;
      setFavoriteIdList(data?.toolIdList || []);
    }
  }, []);

  const fetchRecommend = React.useCallback(async () => {
    const res = await getToolRecommend();

    if (res?.status === 200) {
      const { data } = res.data;
      setRecommendTool(data);
    }
  }, []);

  const fetchMetric = async () => {
    const res = await getMetric();
    if (res?.status === 200 || res?.status === 201) {
      const { data } = res.data;
      setMetric(data);
    }
  };

  const fetchCoupon = async () => {
    const res = await getCouponList();
    if (res?.status === 200) {
      const { data } = res.data;
      setCouponList(data);
    }
  };

  const fetchChat = async () => {
    const res = await getChat();
    if (res?.status === 200) {
      const { data } = res.data;
      setChatList(data);
    }
  };

  return {
    fetchTool,
    fetchFavorite,
    fetchRecommend,
    fetchUser,
    fetchMetric,
    fetchCoupon,
    fetchChat,
  };
};

export default useRefresh;
