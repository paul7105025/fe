import _ from "lodash";
import React from "react";
import { useAlert } from "react-alert";
import { SetterOrUpdater, useRecoilState, useSetRecoilState } from "recoil";

import {
  useEvent,
  useFreePlanBubble,
  useGenerateFilter,
  useModalOpen,
  useReCaptcha,
  useRefresh,
} from "..";

import {
  GENERATE_FAIL,
  GENERATE_FAIL_TOO_MANY_REQUEST,
  GENERATE_INVALID_INPUT,
} from "../../constants/error";
import { getImage, postGenerate, putUser } from "../../services";
import { outputLoadingState, userState } from "../../stores";
import {
  FormMetadataType,
  messageType,
  OutputMetadata,
  SavedValueType,
  ToolType,
  userType,
} from "../../types";
import { getLocal, manufactureOutput, setLocal } from "../../utils";
import { useTagManager } from "./useTagManager";

const imageValueSelector = {
  기본: "normal",
  팝아트: "pop-art",
  유화: "oil",
  애니메이션: "animation",
  수채화: "water",
};

const checkActive = async (
  user: userType,
  setUser: SetterOrUpdater<userType | null>
) => {
  if (user.isActive) {
    return;
  } else {
    await putUser({
      data: {
        isActive: true,
      },
    });
    setUser({
      ...user,
      isActive: true,
    });
  }
};

const saveFormLocal = ({ forms }: { forms: Array<FormMetadataType> }) => {
  const savedValue = getLocal<SavedValueType>("wrtn-saved-value") || [];

  const currentValue: SavedValueType = forms.map((form) => ({
    label: form.label,
    value: form.value,
  }));

  const newValue = _.uniqBy([...currentValue, ...savedValue], "label");

  setLocal("wrtn-saved-value", newValue);
};

/**
 * Forms를 받아 문장을 생성합니다.
 * @param {object} forms
 * @param {string} toolId
 * @param {number} count - 한 번에 생성할 개수
 * @returns {object} - 성공적으로 호출했다면, forms 형태와 동일합니다.
 */

interface onGenerateWithCountProps {
  forms: Array<FormMetadataType>;
  toolId: string;
  count?: number;
  tool?: ToolType;
  type?: string;
  id?: string;
}

//TODO: 순차적 생성 비활성화 - 추후 UI/UX 논의 후 활성화 예정

const redoWithCount = async (callback: () => Promise<any>, count: number) => {
  for (let i = 0; i < count; i++) {
    await callback();
  }
};

const _handleFormat = (val: OutputMetadata) => {
  return {
    ...val,
    inputs: [...val.inputs.map((v) => (v === "''" ? "" : v))],
  } as OutputMetadata;
};

/**
 * 문장을 생성합니다.
 * @param {object[]} props.forms
 * @param {function} props.setOutput
 * @returns
 */

interface useGenerateProps {
  tool: ToolType;
  forms: Array<FormMetadataType>;
  setOutput: React.Dispatch<React.SetStateAction<OutputMetadata[]>>;
}

type handleGenerateFuncProps = {
  count: number;
  toolId: string;
  type?: string;
  id?: string;
};

type handleGenerateProps = {
  count: number;
  toolId: string;
  type?: string;
  id?: string;
};

export const useGenerate = ({ tool, forms, setOutput }: useGenerateProps) => {
  const toolId = React.useMemo(() => tool?._id, [tool]);
  const toolName = React.useMemo(() => tool?.name, [tool]);

  const setOutputLoading = useSetRecoilState(outputLoadingState);
  const [user, setUser] = useRecoilState(userState);

  const blockChatGPT4LimitModal = useModalOpen({
    modalId: "blockChatGPT4Limit",
  });
  const alert = useAlert();
  const {
    collectUserProperties_once,
    collectUserProperties,
    collectUserProperties_increment,
  } = useEvent();
  const generateFilter = useGenerateFilter({ forms, toolId });

  const { fetchMetric } = useRefresh();

  const { collectEvent } = useEvent();
  const tagManager = useTagManager();

  const { updateTodayMetric } = useFreePlanBubble();
  const { openRecaptcha } = useReCaptcha();

  const onGenerateWithCount = async ({
    forms,
    toolId,
    count = 1,
    tool, // as parent
    type,
    id,
  }: onGenerateWithCountProps) => {
    let result: Array<OutputMetadata> = [];

    for (let i = 0; i < count; i++) {
      const res = await postGenerate({
        toolId: toolId,
        data: manufactureOutput(forms),
        type,
        id,
        user,
      });

      console.log(res);

      if (res?.status === 400 || res?.response?.status === 400) {
        throw new Error("400");
      } else if (res?.status === 429 || res?.response?.status === 429) {
        throw new Error("429");
      } else if (res?.status === 402 || res?.response?.status === 402) {
        throw new Error("402");
      } else if (res?.status === 444 || res?.response?.status === 444) {
        throw new Error("444");
      }

      if (res.data) {
        const currentCategory = forms?.find((v) => v.label === "카테고리");
        const currentToneAndManner = forms?.find(
          (v) => v.label === "톤앤 매너"
        );

        collectUserProperties_increment(
          "amount_generate_accum",
          res.data.data.output.replace(/ /g, "").length
        );
        collectUserProperties_once("first_generate_date", new Date());
        collectUserProperties_increment("count_generate_accum", 1);
        collectUserProperties("last_generate_date", new Date());
        tagManager({
          event: "generate_done",
          data: {
            feature_menu: "tool",
            generate_position: "tool",
            tool_open_position: "",
            re_generate: false,
            feature_category: tool?.category,
            feature_name: tool?.name,
            feature_tag: tool?.tag,
            feature_form: tool?.kind,
            prompt_id: [toolId],
            feature_prompt: toolName,
            generate_character: res.data.data.output.replace(/ /g, "").length,
            generate_token: "", // TODO: generate_token
            generate_model: "", // TODO: generate model
            input_category: currentCategory ? currentCategory.value : "",
            input_tone_and_manner: currentToneAndManner
              ? currentToneAndManner.value
              : "",
            input_continue_option: "",
            input_style: "",
            input_ratio: "",
          },
        });
        collectEvent("generate_done", {
          feature_menu: "tool",
          generate_position: "tool",
          tool_open_position: "",
          re_generate: false,
          feature_category: tool?.category,
          feature_name: tool?.name,
          feature_tag: tool?.tag,
          feature_form: tool?.kind,
          prompt_id: [toolId],
          feature_prompt: toolName,
          generate_character: res.data.data.output.replace(/ /g, "").length,
          generate_token: "", // TODO: generate_token
          generate_model: "", // TODO: generate model
          input_category: currentCategory ? currentCategory.value : "",
          input_tone_and_manner: currentToneAndManner
            ? currentToneAndManner.value
            : "",
          input_continue_option: "",
          input_style: "",
          input_ratio: "",
        });

        if (user?.isNewbie) {
          await putUser({
            data: {
              isNewbie: false,
            },
          });
        }

        if (res?.status === 201) {
          result = [...result, _handleFormat(res.data.data as OutputMetadata)];
        }
      }
    }
    return result;
  };

  const onGenerateAtChat = async ({
    forms,
    toolId,
    count = 1,
    tool, // as parent
    type,
    id,
  }: onGenerateWithCountProps) => {
    let result: Array<messageType> = [];

    for (let i = 0; i < count; i++) {
      const res = await postGenerate({
        toolId: toolId,
        data: manufactureOutput(forms),
        type,
        id,
        user,
      });

      if (res?.status === 201) {
        result = [...result, res.data.data as messageType];
      } else if (res?.status === 400) {
        throw new Error("400");
      } else if (res?.status === 429) {
        throw new Error("429");
      } else if (res?.status === 402) {
        throw new Error("402");
      } else if (res?.status === 403) {
        throw new Error("403");
      } else if (res?.status === 444) {
        throw new Error("444");
      }
    }
    return result;
  };

  // handleGenerate 생성 부분만. 로딩, 에러 처리를 뺀 부분
  const handleGenerateFunc = React.useCallback(
    async ({ count, toolId, type, id }: handleGenerateFuncProps) => {
      try {
        setOutputLoading(true);

        if (user) {
          await checkActive(user, setUser);
        }

        const isToxicity = await generateFilter.handleCheckToxicityGenerate({
          isImage: false,
        });

        if (isToxicity) return;
        const res = await onGenerateWithCount({
          forms,
          toolId,
          count,
          tool,
          type,
          id,
        });

        if (res.length !== count) {
          setOutputLoading(false);
          throw new Error("failed");
        }

        saveFormLocal({ forms });
        setOutput((currVal) => [...res, ...currVal]);

        updateTodayMetric(1);

        return true;
      } catch (err: any) {
        setOutputLoading(false);
        if (err?.message === "429") {
          throw new Error("failed");
        } else if (err?.message === "400") {
          throw new Error("400");
        } else if (err?.message === "401") {
          throw new Error("401");
        } else if (err?.status === "444") {
          openRecaptcha();
        } else {
          throw new Error("failed");
        }
      } finally {
        setOutputLoading(false);
        fetchMetric();
      }
    },
    [forms, generateFilter]
  );

  const handleGenerate = React.useCallback(
    async ({ count, toolId, type, id }: handleGenerateProps) => {
      try {
        setOutputLoading(true);

        if (user) {
          await checkActive(user, setUser);
        }

        const isToxicity = await generateFilter.handleCheckToxicityGenerate({
          isImage: false,
        });

        if (isToxicity) return;

        const res = await onGenerateWithCount({
          forms,
          toolId,
          count,
          tool,
          type,
          id,
        });
        if (res.length !== count) {
          alert.removeAll();
          alert.show(GENERATE_FAIL);
        }

        saveFormLocal({ forms });
        setOutput((currVal) => [...res, ...currVal]);

        updateTodayMetric(1);

        return true;
      } catch (err: any) {
        console.log(err);
        if (err?.message === "429") {
          alert.removeAll();
          alert.error(GENERATE_FAIL_TOO_MANY_REQUEST);
        } else if (err?.message === "400") {
          alert.removeAll();
          alert.show(GENERATE_INVALID_INPUT);
        } else if (err?.message === "401") {
          alert.removeAll();
        } else if (err?.message === "402") {
          //
        } else if (err?.message === "444") {
          openRecaptcha();
        } else {
          alert.removeAll();
          alert.show(GENERATE_FAIL);
        }
        return false;
      } finally {
        setOutputLoading(false);
        fetchMetric();
      }
    },
    [forms, generateFilter]
  );

  const handleGenerateImage = React.useCallback(async () => {
    try {
      setOutputLoading(true);

      const isToxicity = await generateFilter.handleCheckToxicityGenerate({
        isImage: true,
      });

      if (isToxicity) return;

      const res = await getImage({
        text: forms[0].value,
        type: imageValueSelector[forms[1].value as number],
        size: forms[2].value,
      });

      if (res?.status === 200 || res?.status === 201) {
        saveFormLocal({ forms });
        setOutput((currVal) => [...currVal, res.data]);

        updateTodayMetric(1);

        return true;
      } else {
        throw new Error("failed");
      }
    } catch (err: any) {
      if (err?.message === "429") {
        alert.removeAll();
        alert.error(GENERATE_FAIL_TOO_MANY_REQUEST);
      } else if (err?.message === "400") {
        alert.removeAll();
        alert.show(GENERATE_INVALID_INPUT);
      } else {
        alert.removeAll();
        alert.show(GENERATE_FAIL);
      }
      return false;
    } finally {
      setOutputLoading(false);
      fetchMetric();
    }
  }, [forms, generateFilter]);

  const handleGenerateAtChat = React.useCallback(
    async ({ count, toolId, type, id }: handleGenerateProps) => {
      try {
        setOutputLoading(true);

        if (user) {
          await checkActive(user, setUser);
        }
        const isToxicity = await generateFilter.handleCheckToxicityGenerate({
          isImage: false,
        });

        if (isToxicity) return;

        const res = await onGenerateAtChat({
          forms,
          toolId,
          count,
          tool,
          type,
          id,
        });
        if (res.length !== count) {
          alert.removeAll();
          alert.show(GENERATE_FAIL);
        }
        saveFormLocal({ forms });

        updateTodayMetric(1);

        return res;
      } catch (err: any) {
        if (err?.message === "429") {
          alert.removeAll();
          alert.error(GENERATE_FAIL_TOO_MANY_REQUEST);
        } else if (err?.message === "400") {
          alert.removeAll();
          alert.show(GENERATE_INVALID_INPUT);
        } else if (err?.message === "401") {
          alert.removeAll();
        } else if (err?.message === "402") {
          //
        } else if (err?.message === "403") {
          blockChatGPT4LimitModal.open();
        } else if (err?.message === "444") {
          openRecaptcha();
        } else {
          alert.removeAll();
          alert.show(GENERATE_FAIL);
        }
        return [];
      } finally {
        setOutputLoading(false);
        fetchMetric();
      }
    },
    [forms, generateFilter]
  );

  return {
    handleGenerate,
    handleGenerateAtChat,
    handleGenerateFunc,
    handleGenerateImage,
    // blockAlert,
    // removeBlockAlert,
  };
};
