import React from "react";

export const useReset = <T extends string | Array<any>>(
  defaultValue: T,
  value: T,
  setInnerValue: (arg0: T) => void
) => {
  React.useEffect(() => {
    if (value?.length < 1) {
      setInnerValue(defaultValue);
    }
  }, [value]);
};
