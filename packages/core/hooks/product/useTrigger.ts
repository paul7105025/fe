import React from "react";
import { useRecoilState, useSetRecoilState } from "recoil";

import { Kind } from "../../types";
import { getLocal, setLocal } from "../../utils";
import { TRIGGER_AUTO_MAKE, TRIGGER_EVALUATE } from "../../constants";
import { currentCountScoreState, triggerGenerateCountState, triggerState } from "../../stores";

/**
 * 최초 1회 생성 시 생성 더 해보라는 안내 문구 트리거
 * @returns
 */
export const useTriggerAutoMake = () => {
  const [isTriggerAutoMake, setIsTriggerAutoMake] = useRecoilState(
    triggerState(TRIGGER_AUTO_MAKE)
  );

  const handleAutoMake = () => {
    const trigger = getLocal(TRIGGER_AUTO_MAKE);
    if (!trigger) {
      setLocal(TRIGGER_AUTO_MAKE, true);
      setIsTriggerAutoMake(true);
    }
  };

  const handleCloseAutoMake = () => {
    setIsTriggerAutoMake(false);
  };

  return {
    isTriggerAutoMake,
    handleAutoMake,
    handleCloseAutoMake,
  };
};

export const useTriggerEvaluate = () => {
  const [isTriggerEvaluate, setIsTriggerEvaluate] = useRecoilState(
    triggerState(TRIGGER_EVALUATE)
  );

  const handleEvaluate = () => {
    const trigger = getLocal<number>(TRIGGER_EVALUATE);
    if (trigger === undefined || trigger === null)
      return setLocal(TRIGGER_EVALUATE, 2);
    if (trigger === 2 || trigger === 6) {
      setIsTriggerEvaluate(true);
    }
    setLocal(TRIGGER_EVALUATE, trigger + 1);
  };

  const handleCloseEvaluate = () => {
    setIsTriggerEvaluate(false);
  };

  return {
    isTriggerEvaluate,
    handleEvaluate,
    handleCloseEvaluate,
  };
};

/**
 * 현재 툴의 총 생성 개수를 계산하는 트리거 전용 훅
 * @param {*} toolId
 * @param {*} kind
 */
export const useTriggerGenerateCount = (toolId: string, kind: Kind) => {
  const [currentCount, setCurrentCount] = useRecoilState(
    currentCountScoreState
  );

  const setTriggerGenerateCount = useSetRecoilState(triggerGenerateCountState);

  React.useEffect(() => {
    if (currentCount[toolId] === undefined) {
      setCurrentCount((currVal) => ({
        ...currVal,
        [toolId]: 0,
      }));
    }
  }, [toolId, currentCount]);

  const handleIncreaseCount = (count = 1) => {
    if (kind !== "step") {
      if (currentCount[toolId] !== undefined) {
        setCurrentCount({
          ...currentCount,
          [toolId]: currentCount[toolId] + count,
        });
        setTriggerGenerateCount(c => c + 1); // count로 세면 안됩니다.
      } else {
        setCurrentCount({
          ...currentCount,
          [toolId]: 0,
        });
      }
    }
  };

  return {
    currentCount,
    handleIncreaseCount,
  };
};
