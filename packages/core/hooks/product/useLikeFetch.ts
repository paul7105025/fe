import React from "react";

import { getHistory } from "../../services";
import { OutputMetadata } from "../../types";

export const useLikeFetch = ({
  toolId,
  tab,
}: {
  toolId: string;
  tab: string;
}) => {
  const [likeList, setLikeList] = React.useState<Array<OutputMetadata>>([]);

  const [loading, setLoading] = React.useState(false);
  const [index, setIndex] = React.useState(1);
  const [hasMore, setHasMore] = React.useState(true);

  const fetchLikeList = React.useCallback(
    async (toolId: string, index: number, isMore: boolean) => {
      setLoading(true);
      try {
        const res = await getHistory({
          page: index,
          limit: 10,
          liked: "y",
          toolId,
        });

        if (res && res.data && res.data.data) {
          if (isMore) {
            setLikeList((prev) => [...prev, ...res.data.data]);
          } else {
            setLikeList(res.data.data);
          }
          if (res.data.data.length < 10) {
            setHasMore(false);
          } else {
            setHasMore(true);
          }
        } else {
          setHasMore(false);
        }
      } catch (error) {
        //error
      } finally {
        setLoading(false);
      }
    },
    []
  );

  const fetchMore = React.useCallback(async () => {
    if (hasMore && !loading) {
      await fetchLikeList(toolId, index + 1, true);
      setIndex((prev) => prev + 1);
    }
  }, [hasMore, index, toolId, loading, fetchLikeList]);

  const refetchLikeList = () => {
    fetchLikeList(toolId, 1, false);
    setIndex(1);
  };

  React.useEffect(() => {
    refetchLikeList();
  }, [toolId]);

  React.useEffect(() => {
    if (tab === "좋아요") {
      refetchLikeList();
    }
  }, [tab, toolId, fetchLikeList]);

  return { likeList, setLikeList, fetchMore, refetchLikeList };
};
