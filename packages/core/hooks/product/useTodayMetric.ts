import React from "react";
import dayjs from "dayjs";
import { useRecoilState } from "recoil";

import { todayMetricState } from "../../stores/usage";

import { setLocal, getLocal } from "../../utils/localStorage";

type LocalMetric = {
  date: string;
  metric: number;
};

export const useTodayMetric = () => {
  const [todayMetric, setTodayMetric] = useRecoilState(todayMetricState);

  React.useEffect(() => {
    const today = dayjs();
    const savedTodayMetric = getLocal<LocalMetric>("savedMetric") || {
      date: today.format("YYYY-MM-DD"),
      metric: 0,
    };
    if (today.isSame(savedTodayMetric.date, "day")) {
      setTodayMetric(savedTodayMetric.metric);
    } else {
      setTodayMetric(0);
    }
  }, []);

  const updateTodayMetric = (metric: number) => {
    setTodayMetric((m) => {
      const today = dayjs();

      setLocal("savedMetric", {
        date: today.format("YYYY-MM-DD"),
        metric: m + metric,
      });

      return m + metric;
    });
  };

  return {
    todayMetric,
    updateTodayMetric,
  };
};
