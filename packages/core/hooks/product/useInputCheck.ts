import React from "react";
import { useAlert } from "react-alert";

import { textInputRegExp } from "../../utils";

export const useInputCheck = (value: any) => {
  const alert = useAlert();

  const isTestSuccess = React.useMemo(() => false, [value]);

  React.useEffect(() => {
    if (isTestSuccess) {
      alert.removeAll();
      alert.error(
        "잘못된 형식입니다.\n 특수문자 { , } , [ , ] , $ 는 사용할 수 없습니다."
      );
    } else {
      alert.removeAll();
    }
  }, [isTestSuccess]);

  return isTestSuccess;
};
