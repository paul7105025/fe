import React from "react";
import TagManager from "react-gtm-module";

export const useTagManager = () => {

  return React.useCallback(({ event, data } : { event : string, data? : any | null }) => {
    const userString = window.sessionStorage.getItem("current_user") || "";

    try {
      const user = JSON.parse(userString);
      const { _id, job, purpose, provider, createdAt } = user;
      if (!_id || !job || !purpose || !provider || !createdAt) {
        throw new Error("error");
      }

      TagManager.dataLayer({
        dataLayer: {
          user_id: user._id,
          crm_id: user._id,
          user_job: user.job,
          user_purpose: user.purpose,
          login_provider: user.provider,
          sign_up_date: user.createdAt,
          event: event,
          referrer: document.referrer,
          page_url: window.location.pathname,
          ...data,
          ...user,
        },
        dataLayerName: "PageDataLayer",
      });
    } catch (e) {
      TagManager.dataLayer({
        dataLayer: {
          event: event,
          referrer: document.referrer,
          page_url: window.location.pathname,
          ...data,
        },
        dataLayerName: "PageDataLayer",
      });
    }
  }, []);
};

export const useClickOutputMenu = ({ tagManager }) => {
  const clickOutputMenuTag = (type: string, data: any) =>
    tagManager({
      event: "click_output_menu",
      data: {
        btn_type: type,
        ...data,
      },
    });

  return { clickOutputMenuTag };
};
