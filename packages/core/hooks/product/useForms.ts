import React from "react";
import _ from "lodash";

import { useInputCheck, useTagManager } from "./";
import { FORM_DEFAULT_VALUE_RECORD } from "../../records";
import {
  checkDisabled,
  resetAllValue,
  getLocal,
  removeLocal,
} from "../../utils";
import {
  FormMetadataType,
  FormOptions,
  Kind,
  OutputMetadata,
  SavedValueType,
  ToolType,
} from "../../types";

interface useFormsInterface {
  tool: ToolType | null;
  kind: Kind;
  scanLink?: (form: FormMetadataType) => FormMetadataType;
}

export const useForms = ({
  tool = null,
  kind = "tool",
  scanLink,
}: useFormsInterface) => {
  const toolId = React.useMemo(() => tool && tool._id, [tool]);

  const [forms, setForms] = React.useState<Array<FormMetadataType>>([]);

  const tagManager = useTagManager();

  //init forms value

  React.useEffect(() => {
    try {
      if (tool && tool.forms) {
        const savedValue = getLocal<SavedValueType>("wrtn-saved-value") || [];

        setForms(
          tool.forms.map((singleForm) => {
            const form: FormMetadataType =
              kind === "step" && scanLink ? scanLink(singleForm) : singleForm;

            if (
              form.value &&
              typeof form.value !== "number" &&
              form.value.length > 0
            )
              return form;

            if (form.type === "input_keyword")
              return { ...form, value: FORM_DEFAULT_VALUE_RECORD[form.type] };

            const findValue = _.find(savedValue, { label: form.label });

            if (
              (form.type === "option_chips" ||
                form.type === "light_option_chips") &&
              findValue
            ) {
              const option = form.option as FormOptions<"option_chips">;
              const chips = option.chips.map((chip) => chip.value);
              if (_.includes(chips, findValue.value)) {
                return { ...form, value: findValue.value };
              } else {
                return { ...form, value: FORM_DEFAULT_VALUE_RECORD[form.type] };
              }
            }

            return {
              ...form,
              value: findValue?.value || FORM_DEFAULT_VALUE_RECORD[form.type],
            };
          })
        );
      }
    } catch (error) {
      removeLocal("wrtn-saved-value");
    }
  }, [tool, kind]);

  const exampleForms = React.useCallback(() => {
    setForms((forms) =>
      forms.map((form) => {
        if (
          form.type === "option_chips" ||
          form.type === "light_option_chips"
        ) {
          const option = form.option as FormOptions<"option_chips">;
          return {
            ...form,
            value: option.chips[0].value,
          };
        }

        let placeholder: any = null;
        if (form.option.hasOwnProperty("placeholder")) {
          const option = form.option as FormOptions<
            "input" | "label" | "input_keyword"
          >;
          placeholder = option.placeholder;
        }

        if (placeholder)
          return {
            ...form,
            value: form.type === "input_keyword" ? [placeholder] : placeholder,
          };

        return {
          ...form,
        };
      })
    );
  }, [toolId]);

  const resetForms = React.useCallback(() => {
    tagManager({
      event: "click_input_delete_all_btn",
      data: {
        tool_id: toolId,
        tool_tag: tool?.tag,
        category: tool?.category,
      },
    });
    setForms((forms) => resetAllValue(forms));
  }, [toolId]);

  const syncForms = React.useCallback(
    (input: Array<any>, option: Array<any>) => {
      if (
        tool &&
        tool.kind === "tool" &&
        tool.forms &&
        input.toString() !== tool.forms.map((form) => form.value).toString()
      ) {
        let optionIdx = 0;

        const newForms = tool.forms.map((form, index) => {
          let newValue;

          const targetInput = input[index];
          const { type } = form;

          if (targetInput) {
            if (type === "input_keyword") {
              newValue = targetInput
                .split(", ")
                .map((v: string) => v.replace(/^'+|'+$/g, ""))
                .filter((v: string | any[]) => v.length > 0);
            } else if (type === "input_divided") {
              newValue = targetInput
                .split("는 ")
                .map((v: string) => v.replace(/^'+|'+$/g, ""))
                .filter((v: string | any[]) => v.length > 0);
            } else {
              newValue = targetInput.replace(/^'|'$/g, "");
            }
          } else {
            if (
              type === "option_chips"
              // type === FORM_TYPE_LIGHT_OPTION_CHIPS
            ) {
              newValue = option[optionIdx];
              optionIdx++;
            } else {
              newValue = FORM_DEFAULT_VALUE_RECORD[type];
            }
          }

          return {
            ...form,
            value: newValue,
          };
        });

        if (
          forms.map((form) => form.value).toString() !==
          newForms.map((form) => form.value).toString()
        ) {
          setForms(newForms);
        }
      }
    },
    [tool, forms]
  );

  const updateForms = React.useCallback((id, newForm) => {
    setForms((forms) => forms.map((form) => (form.id === id ? newForm : form)));
  }, []);

  const returnHistoryStore = React.useCallback(
    (output: OutputMetadata) => {
      return forms.map((form, index) => ({
        toolId: toolId,
        inputId: form._id,
        outputId: output._id,
        inputData: output.inputs[index],
        outputData: output.output,
      }));
    },
    [forms, toolId]
  );

  const isDisabled =
    useInputCheck(forms.map((v) => v?.value)) || checkDisabled(forms);

  return {
    forms,
    isDisabled,
    updateForms,
    exampleForms,
    resetForms,
    syncForms,
    returnHistoryStore,
  };
};
