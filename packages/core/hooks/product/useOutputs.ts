import _ from "lodash";
import React from "react";
import { useSetRecoilState } from "recoil";

import { OutputMetadata } from "../../types";
import { isGeneratedState } from "../../stores";
import { getLocal, setLocal } from "../../utils";

type outputsType = Array<OutputMetadata>;

type tempOutputsType = Array<{
  id: string;
  outputs: outputsType;
}>;

export const useOutputs = ({ toolId }) => {
  const [output, setOutput] = React.useState<outputsType>([]);

  const setIsGenerated = useSetRecoilState<boolean>(isGeneratedState);

  const currentId = React.useRef(toolId);

  const updateTempOutput = (
    outputs: outputsType,
    prevId: string,
    isReturn: boolean = false
  ) => {
    const tempOutput = getLocal<tempOutputsType>("wrtn-temp-output") || [];
    let temp = [...tempOutput];

    if (prevId && temp.findIndex((v) => v.id === prevId) < 0) {
      temp.push({
        id: prevId,
        outputs: [],
      });
    }

    temp = temp.map((v) => {
      if (v.id !== prevId) return v;
      if (isReturn && outputs.length > 0) return { ...v, outputs: outputs };
      if (isReturn && outputs.length === 0) return { ...v };
      return { ...v, outputs: outputs };
    });

    return _.uniqBy(temp, "id");
  };

  const deleteTempOutput = (id: string) => {
    const tempOutput = getLocal<tempOutputsType>("wrtn-temp-output") || [];

    const temp = tempOutput.map((tool_output) => {
      if (tool_output.id !== toolId) return tool_output;
      const newOutputs = tool_output.outputs.filter((v) => v._id !== id);
      return { ...tool_output, outputs: newOutputs };
    });

    setLocal("wrtn-temp-output", temp);
  };

  const handleDelete = (id: string) => {
    const index = output.findIndex((v) => v._id === id);
    setOutput([...output.slice(0, index), ...output.slice(index + 1)]);
    deleteTempOutput(id);
  };

  const handleUpdate = (id: string, object: OutputMetadata) => {
    const index = output.findIndex((v) => v._id === id);
    if (index < 0) return;
    setOutput([...output.slice(0, index), object, ...output.slice(index + 1)]);
  };

  React.useEffect(() => {
    currentId.current = toolId;
    return () => {
      setOutput([]);
    };
  }, []);

  React.useEffect(() => {
    const savedOutput = getLocal<tempOutputsType>("wrtn-temp-output") || [];
    const singleOutput = savedOutput.find(
      (v) => v.id === currentId.current
    )?.outputs;
    if (singleOutput) setOutput(singleOutput);
    else setOutput([]);
  }, [currentId.current]);

  React.useEffect(() => {
    if (currentId.current === "0") {
      currentId.current = toolId;
    } else if (toolId !== currentId.current) {
      setLocal("wrtn-temp-output", updateTempOutput(output, currentId.current));
      setOutput([]);

      currentId.current = toolId;
    }
  }, [toolId]);

  React.useEffect(() => {
    if (output.length > 0) {
      setIsGenerated(true);
    }
    setLocal(
      "wrtn-temp-output",
      updateTempOutput(output, currentId.current, true)
    );
  }, [output]);

  return { output, setOutput, deleteTempOutput, handleDelete, handleUpdate };
};
