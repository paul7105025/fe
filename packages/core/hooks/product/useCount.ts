import React from "react";
import { useNonInitialEffect } from "..";

import { WrtnCountType } from "../../types";
import { getLocal, setLocal } from "../../utils";

export const useCount = (toolId: string) => {
  const [count, setCount] = React.useState(1);

  const increment = React.useCallback(() => setCount((prev) => prev + 1), []);
  const decrement = React.useCallback(() => setCount((prev) => prev - 1), []);
  const reset = React.useCallback(() => setCount(1), []);

  // React.useEffect(() => {
  //   const savedCount = getLocal<WrtnCountType>("wrtn-count");
  //   if (savedCount && savedCount.length > 0) {
  //     const findCount = savedCount.find((item) => item.toolId === toolId);

  //     if (findCount) {
  //       setCount(findCount.count);
  //     } else {
  // setCount(1);
  //   }
  // }
  // }, [toolId]);

  // useNonInitialEffect(() => {
  //   const savedCount = getLocal<WrtnCountType>("wrtn-count");
  //   if (savedCount && savedCount.length > 0) {
  //     const findCount = savedCount.find((item) => item.toolId === toolId);

  //     if (findCount) {
  //       const newCount = savedCount.map((item) =>
  //         item.toolId === toolId ? { ...item, count } : item
  //       );
  //       setLocal("wrtn-count", newCount);
  //     } else {
  //       const newCount = [...savedCount, { toolId, count }];
  //       setLocal("wrtn-count", newCount);
  //     }
  //   } else {
  //     setLocal("wrtn-count", [{ toolId, count }]);
  //   }
  // }, [count]);

  return { count, increment, decrement, reset };
};
