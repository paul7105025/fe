import { useAlert } from "react-alert";

import { useTagManager } from "./";
import { deleteHistory_Id, putHistory_Id } from "../../services";

export const useHistoryAPI = ({ history, setHistory }) => {
  const tagManager = useTagManager();
  const alert = useAlert();

  const buildTagManager = (btn_type) => {
    tagManager({
      event: "click_output_menu",
      data: history.tool
        ? {
            btn_type: btn_type,
            tool_id: history.tool._id,
            tool_tag: history.tool.tag,
            tool_cat: history.tool.category,
            output_created_date: history.createdAt,
            output_id: history._id,
            output_count: 0,
          }
        : {
            btn_type: btn_type,
            tool_id: history.toolId,
            output_created_date: history.createdAt,
            output_id: history._id,
            output_count: 0,
          },
    });
  };

  const Reset = async (callback?: Function) => {
    buildTagManager("reset");
    const res = await putHistory_Id({
      historyId: history._id,
      data: {
        output: history.originOutput,
      },
    });

    if (res?.status === 200) {
      setHistory({
        ...history,
        output: history.originOutput,
      });
      if (typeof callback === "function") {
        callback();
      }
    }
  };

  const Like = async (callback?: Function) => {
    buildTagManager(history.liked ? "delete_like" : "add_like");
    const res = await putHistory_Id({
      historyId: history._id,
      data: {
        liked: !history.liked,
      },
    });

    if (res?.status === 200) {
      setHistory({
        ...history,
        liked: !history.liked,
      });
      if (typeof callback === "function") {
        callback();
      }
    }
  };

  const Dislike = async (callback?: Function) => {
    buildTagManager(history.disliked ? "delete_dislike" : "add_like");
    const res = await putHistory_Id({
      historyId: history._id,
      data: {
        disliked: !history.disliked,
      },
    });

    if (res?.status === 200) {
      setHistory({
        ...history,
        disliked: !history.disliked,
      });
      if (typeof callback === "function") {
        callback();
      }
    }
  };

  const Report = async (callback) => {
    buildTagManager("report");
    if (typeof callback === "function") {
      callback();
    }
  };

  const Copy = async (callback?: Function) => {
    buildTagManager("copy");
    navigator.clipboard.writeText(history.output);
    alert.removeAll();
    alert.show("결과물을 복사했어요.");
    if (typeof callback === "function") {
      callback();
    }
  };

  const Remove = async (callback?: Function) => {
    buildTagManager("delete");
    const res = await deleteHistory_Id({ historyId: history._id });
    if (res.status === 200) {
      alert.removeAll();
      alert.show("결과물을 삭제했어요.");
      if (typeof callback === "function") {
        callback();
      }
    }
  };

  const Write = async (value: string, callback?: Function) => {
    const res = await putHistory_Id({
      historyId: history._id,
      data: {
        output: value,
      },
    });

    tagManager({
      event: "update_output",
      data: {
        tool_id: history.toolId,
      },
    });
    if (res.status === 200) {
      if (typeof callback === "function") {
        callback();
      }
    }
  };

  const Click = async (callback?: Function) => {
    tagManager({
      event: "click_output_menu",
      data: {
        btn_type: "view_input",
        tool_id: history.toolId,
      },
    });
    if (typeof callback === "function") {
      callback();
    }
  };

  return {
    Reset,
    Like,
    Copy,
    Remove,
    Write,
    Report,
    Dislike,
    Click,
  };
};
