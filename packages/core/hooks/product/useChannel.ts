import React from "react";
import ChannelService from "../../utils/ChannelService";

declare global {
  interface Window {
    ChannelIO: any;
    ChannelIOInitialized: any;
    attachEvent: any;
  }
}

export const useChannelBoot = (pluginKey: string) => {
  const [first, setFirst] = React.useState(true);

  React.useEffect(() => {
    if (first) {
      ChannelService.boot({
        pluginKey,
        language: "ko",
        hideChannelButtonOnBoot: true,
        // window.location.pathname === "/app/login" ? false : true,"
        customLauncherSelector: ".channel-custom-btn"
      });
      setFirst(false);
    }
  }, [first]);
};

export const showMessenger = () => {
  if (typeof window !== 'undefined' && navigator.language.includes("zh")) return ;
  window.ChannelIO("showMessenger");
};

export const showChannelButton = () => {
  if (typeof window !== 'undefined' && navigator.language.includes("zh")) return ;
  window.ChannelIO("showChannelButton");
};
export const hideChannelButton = () => {
  window.ChannelIO("hideChannelButton");
};

export const updateChannelUser = (userInfo, callback?) => {
  window.ChannelIO("updateUser", userInfo, callback);
};
