import React from "react";

/**
 * 저장할 필요가 없는 입력을 처리하는 로직.
 * @param {any} originValue
 * @returns
 */
export const useTempInput = (originValue: string) => {
  const [value, setValue] = React.useState("");

  const handleChange = (e: string) => {
    setValue(e);
  };

  React.useEffect(() => {
    setValue(originValue);
  }, [originValue]);

  return {
    value,
    handleChange,
  };
};

export const useTempArray = (
  originValue: Array<string> | null,
  formatter: (val: Array<string>) => string[]
) => {
  const [value, setValue] = React.useState<Array<string>>([]);

  const handleChange = (e: Array<string>) => {
    setValue(e);
  };

  React.useEffect(() => {
    setValue(formatter(originValue || []));
  }, [originValue]);

  return {
    value,
    handleChange,
  };
};
