import { useAlert } from "react-alert";
import { useRecoilState } from "recoil";

import { FormMetadataType } from "../../types";
import { GENERATE_INVALID_INPUT } from "../../constants";
import { manufactureOutput, getJosa } from "../../utils";
import { postToxicity, postToxicityImage } from "../../services";
import { cachedFormsListState, cachedHateFormsListState } from "../../stores";
import { useLoginDialog } from "..";

interface GenerateFilterProps {
  forms: Array<FormMetadataType>;
  toolId: string;
}

// forms을 필터에 제공할 문자열로 변환합니다.
const toxicityFormat = (toolId: string, forms: Array<FormMetadataType>) => {
  let toxicityText = "";

  const manufacturedInput = manufactureOutput(forms);

  for (const [index, form] of forms.entries()) {
    if (
      toolId === "636ca1bcb4e55b77f3e027f8" &&
      (form.label === "타겟 연령" || form.label === "타겟 성별")
    ) {
      //skip
    } else if (typeof form.value !== "number" && form.value?.length > 0) {
      toxicityText += `${index === 0 ? "" : " "}`;
      toxicityText += `${
        form.label !== "모집 부문" ? getJosa(form.label, "은/는") : ""
      }`;
      toxicityText += `${
        form.type === "option_chips"
          ? " " + form.value
          : " " + manufacturedInput.input[index]
      }`;
    }
    toxicityText += `${index === forms.length - 1 ? " 입니다." : ""}`;
  }
  return toxicityText.trim();
};

export const useGenerateFilter = ({ forms, toolId }: GenerateFilterProps) => {
  const alert = useAlert();

  // 한 번 튜닙 필터를 거친 인풋은 캐싱해서 튜닙 필터를 사용하지 않게 막습니다. 새로고침하기 전까지만 유지됩니다.

  // 튜닙 필터를 통과한 일반 인풋 캐싱
  const [cachedFormsList, setCachedFormsList] =
    useRecoilState(cachedFormsListState);
  // 튜닙 필터를 통과하지 못한 혐오 인풋 캐싱
  const [cachedHateFormsList, setCachedHateFormsList] = useRecoilState(
    cachedHateFormsListState
  );

  const { handleOpen } = useLoginDialog();

  const formattedText = toxicityFormat(toolId, forms);

  const isPassedCachedForms =
    cachedFormsList.findIndex((v) => v === formattedText) !== -1;

  const isHatedCachedForms =
    cachedHateFormsList.findIndex((v) => v === formattedText) !== -1;

  const returnToxicityResult = (res) => {
    if (res === undefined) {
      handleOpen();
      throw new Error("401");
    }
    if (res?.status === 401) {
      handleOpen();
      throw new Error("401");
    }
    if (res?.status === 200) {
      const { isToxicity } = res.data;
      if (isToxicity === true) {
        setCachedHateFormsList((currVal) => [...currVal, formattedText]);
        return true;
      } else {
        setCachedFormsList((currVal) => [...currVal, formattedText]);
        return false;
      }
    } else {
      throw new Error("429");
    }
  };

  const checkToxicity = async () => {
    const manufacturedInput = manufactureOutput(forms);

    const postData = toolId
      ? { toolId: toolId, text: formattedText, inputs: manufacturedInput.input }
      : { text: formattedText, inputs: manufacturedInput.input };
    const res = await postToxicity(postData);

    return returnToxicityResult(res);
  };

  const checkToxicityImage = async () => {
    const manufacturedInput = manufactureOutput(forms);

    const postData = {
      text: formattedText,
      inputs: manufacturedInput.input,
    };
    const res = await postToxicityImage(postData);

    return returnToxicityResult(res);
  };

  const handleCheckToxicityGenerate = async ({ isImage = false }) => {
    let isToxicity = false;

    if (!isPassedCachedForms && !isHatedCachedForms) {
      if (isImage) {
        isToxicity = await checkToxicityImage();
      } else {
        isToxicity = await checkToxicity();
      }
    }

    if (isHatedCachedForms) isToxicity = true;

    if (isToxicity) {
      alert.removeAll();
      alert.show(GENERATE_INVALID_INPUT);
    }
    return isToxicity;
  };

  return {
    isPassedCachedForms,
    isHatedCachedForms,
    handleCheckToxicityGenerate,
  };
};
