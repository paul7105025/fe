import React from "react";
import dayjs from "dayjs";

export const useTimeCounter = (id: string) => {
  const [trigger, setTrigger] = React.useState(false);
  const startTime = React.useMemo(() => new Date(), [trigger, id]);

  const reset = () => {
    setTrigger(!trigger);
  };

  const time = () => {
    const currentTime = new Date();
    return dayjs(currentTime).diff(dayjs(startTime), "second");
  };

  return {
    time,
    reset,
  };
};
