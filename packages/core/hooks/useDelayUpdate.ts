import React from "react";

//구 useTimer

export const useDelayUpdate = (
  setInput: (arg: any, ...theArgs: any) => void,
  setData: (arg: any, ...theArgs: any) => void,
  delayTime = 250
) => {
  const timer = React.useRef<number | null>(null);

  const inputChanged = <T>(value: T, ...theArgs: any): void => {
    if (timer.current !== null) {
      window.clearTimeout(timer.current);
    }

    setInput(value, ...theArgs);
    timer.current = window.setTimeout(() => {
      setData(value, ...theArgs);
    }, delayTime);
  };

  return inputChanged;
};
