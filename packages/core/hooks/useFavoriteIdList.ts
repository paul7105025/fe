import { useRecoilState } from "recoil";

import { favoriteIdListState } from "../stores/tool";
import { deleteTool_IdFavorite, postTool_IdFavorite } from "../services/tool";
import { useLoginDialog } from "./login";

export const useFavoriteIdList = () => {
  const [favoriteIdList, setFavoriteIdList] =
    useRecoilState(favoriteIdListState);

  const { handleOpen } = useLoginDialog();

  const handleRemoveFavorite = async (toolId: string) => {
    if (favoriteIdList.includes(toolId)) {
      const res = await deleteTool_IdFavorite({ toolId });
      if (res?.status === 200) {
        setFavoriteIdList(favoriteIdList.filter((v) => v !== toolId));
      } else if (res?.status === 401) {
        handleOpen();
      }
    }
  };

  const handleAddFavorite = async (toolId: string) => {
    if (!favoriteIdList.includes(toolId)) {
      const res = await postTool_IdFavorite({ toolId });
      if (res?.status === 201) {
        setFavoriteIdList([...favoriteIdList, toolId]);
      } else if (res?.status === 401) {
        handleOpen();
      }
    }
  };

  const isFavorite = (id: string) => {
    return favoriteIdList.includes(id);
  };

  const handleClickFavorite = (toolId: string) => {
    if (toolId) {
      if (isFavorite(toolId)) {
        handleRemoveFavorite(toolId);
      } else {
        handleAddFavorite(toolId);
      }
    }
  };

  return {
    handleRemoveFavorite,
    handleAddFavorite,
    handleClickFavorite,
    favoriteIdList,
    isFavorite,
  };
};
