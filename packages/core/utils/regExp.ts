export const textInputRegExp: RegExp = /[\{\}\[\]$]/gi;
export const emailRegExp: RegExp = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,4})+$/;
export const passwordRegExp: RegExp =
  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,15}$/;
export const phoneRegExp: RegExp = /^[\d]{11}$/;
