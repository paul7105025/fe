import { messageType } from "../types/chat";

/**
 * http요청을 스트림으로 처리하는 함수
 * @param stream - 스트림 객체
 * @param updateTempMessage - 스트림을 받는 동안 변경될 텍스트를 업데이트 해주는 함수
 * @param updateCompleteMessage - 스트림이 다 끝났을 때 전체 메시지(혹은 에러객체)를 업데이트 해주는 함수
 */
export async function handleStream(
  stream: ReadableStreamDefaultReader<Uint8Array>,
  updateTempMessage: (input: string) => void,
  updateCompleteMessage: (msg: messageType | null, errorObj?: any) => void
) {
  let totalMessage = "";
  let messageObj;

  try {
    while (true) {
      const { value, done } = await stream.read();
      console.log(value, done)
      if (done) break;

      const str = new TextDecoder().decode(value);
      console.log(str)
      const lines = str
        .toString()
        .split("\n")
        .filter((line) => line.trim() !== "");

      lines.forEach((line) => {
        const message = line.replace(/^data: /, "");

        if (message === "[DONE]") return;
        // console.log(message);

        if (message === "event: error") {
          // 500 에러
          const dataLine = lines.find((l) => l.includes("data"));
          const dataObj = dataLine?.replace(/^data: /, "");

          const errorParsed = dataObj && JSON.parse(dataObj);

          const errorObj = {
            status: errorParsed?.status ?? 500,
            message: errorParsed?.message ?? "",
          };

          return updateCompleteMessage(null, errorObj);
        }

        // data가 있을 때
        if (message && typeof message === "string") {
          try {
            const parsed = JSON.parse(message);
            console.log(parsed)

            const isServerError = parsed?.message && parsed.statusCode > 400;
            const isImageMessage = !!parsed.image;

            if (isServerError) {
              // 401 에러(Unauthorized)
              const errorObj = {
                status: parsed.statusCode ?? 500,
                message: parsed.message ?? "",
              };
              return updateCompleteMessage(null, errorObj);
            }

            if (isImageMessage) {
              // 이미지의 경우 여기로
              messageObj = { ...parsed };
              totalMessage = parsed.content;

              return;
            }

            if (parsed.end) {
              return;
            }

            // CASE3: stream 응답일 경우
            const txt = parsed.chunk ?? "";
            if (parsed.message) {
              messageObj = { ...parsed.message };
            }

            totalMessage = `${totalMessage ?? ""}${txt}`;
            if (totalMessage.length % 10 === 0) {
              updateTempMessage(totalMessage);
            }
          } catch (err) {
            // NOTE: `data: `로 시작하지 않는 line의 경우 파싱 에러가 생길 수 있음
            // 그렇지만 에러로 취급하지 않음 & 에러 던질필요 없음
            // console.error(err)
          }
        } // when there are data on the line
      }); // multi lines on one chunk
    } // finish chunk
  } catch (err) {
    // console.error(err);
  } finally {
    updateTempMessage(totalMessage);
    await wait(2000);

    updateCompleteMessage({
      ...messageObj,
      content: totalMessage,
    });
  }
}

export async function handleFileStream(
  stream: ReadableStreamDefaultReader<Uint8Array>,
  updateTempMessage: (input: string) => void,
  updateCompleteMessage: (msg: messageType | null, errorObj?: any) => void
) {
  let totalMessage = "";
  let messageObj;
  const reference: any[] = [];

  try {
    while (true) {
      const { value, done } = await stream.read();
      if (done) break;

      const str = new TextDecoder().decode(value);
      console.log(str)
      const lines = str
        .toString()
        .split("\n")
        .filter((line) => line.trim() !== "");

      lines.forEach((line) => {
        const message = line.replace(/^data: /, "");
        console.log(message)
        if (message === "[DONE]") return;

        if (message === "event: error") {
          // 500 에러
          const dataLine = lines.find((l) => l.includes("data"));
          const dataObj = dataLine?.replace(/^data: /, "");

          const errorParsed = dataObj && JSON.parse(dataObj);

          const errorObj = {
            status: errorParsed?.status ?? 500,
            message: errorParsed?.message ?? "",
          };

          return updateCompleteMessage(null, errorObj);
        }

        // data가 있을 때
        if (message && typeof message === "string") {
          try {
            const parsed = JSON.parse(message);
            // console.log(parsed)
            console.log(parsed)
            const isServerError = parsed?.message && parsed.statusCode > 400;

            if (isServerError) {
              // 401 에러(Unauthorized)
              const errorObj = {
                status: parsed.statusCode ?? 500,
                message: parsed.message ?? "",
              };
              return updateCompleteMessage(null, errorObj);
            }

            const rawData = parsed?.content?.choices[0]?.delta;
            const txt = rawData?.token ?? rawData?.metadata.pages[0].page_content ?? ""
            // console.log(rawData)

            if (rawData?.metadata) {
              const pages = rawData.metadata.pages.map((p) => p.metadata.page);
              if (pages) reference.push(...pages);
            }

            totalMessage = `${totalMessage ?? ""}${txt}`;
            if (totalMessage.length % 20 === 0) {
              updateTempMessage(totalMessage);
            }
          } catch (err) {
            // console.error(err);
          }
        } // when there are data on the line
      }); // multi lines on one chunk
    } // finish chunk
  } catch (err) {
    // console.error(err);
  } finally {
    updateTempMessage(totalMessage);
    await wait(2000);

    // console.log(reference);
    updateCompleteMessage({
      ...messageObj,
      content: totalMessage,
      reference,
    });
  }
}

// 답변이 너무 빨리와서 마지막에 약간의 딜레이를 일부러 줌
const wait = (time: number) =>
  new Promise<void>((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, time);
  });
