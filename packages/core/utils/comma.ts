export const comma = (num: number | null) => {
  var regexp = /\B(?=(\d{3})+(?!\d))/g;
  if (num === null) return "";
  return num.toString().replace(regexp, ",");
};
