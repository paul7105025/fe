export const getLocal = <T>(key: string) => {
  const savedString = localStorage.getItem(key);

  const saved: T | null =
    typeof savedString === "string" ? JSON.parse(savedString) : null;
  return saved;
};

export const setLocal = (key: string, value: any) => {
  localStorage.setItem(key, JSON.stringify(value));
};

export const removeLocal = (key: string) => {
  localStorage.removeItem(key);
};
