import { Cookies } from "react-cookie";
import { CookieGetOptions, CookieSetOptions } from "universal-cookie";

//I LOVE COOKIE

const cookies = new Cookies();

export const setCookie = (
  name: string,
  value: any,
  options?: CookieSetOptions | undefined
) => {
  return cookies.set(name, value, { ...options });
};

export const getCookie = (name: string, options?: CookieGetOptions) => {
  return cookies.get(name, options);
};

export const removeCookie = (name: string, options?: CookieSetOptions) => {
  return cookies.remove(name, options);
};
