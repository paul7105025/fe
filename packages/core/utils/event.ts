
export const fromActiveDate = (activeDate?: string ) => {
  if (!activeDate) return null;
  const today = new Date()
  const day = new Date(activeDate)

  const diff = Math.ceil(Math.abs(today.getTime() - day.getTime()) / (1000 * 3600 * 24));

  if (diff <= 1) return "new1"
  if (diff <= 7) return "new7";
  if (diff <= 30) return "new30";
  return "revisit";
}