/* eslint-disable */
/* no-prototype-builtins */
Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var NAVER_ID_SDK_URL = "https://static.nid.naver.com/js/naveridlogin_js_sdk_2.0.2-nopolyfill.js";
var isServer = function () { return typeof window === "undefined"; };
/**
 * 이 함수는 브라우저 환경에서만 호출이 되야 한다. window 객체에 직접 접근한다.
 * @param props
 */
var initLoginButton = function (props) {
    if (isServer()) {
        return;
    }
    var clientId = props.clientId, callbackUrl = props.callbackUrl, onSuccess = props.onSuccess, onFailure = props.onFailure;
    var naver = window["naver"];
    var naverLogin = new naver.LoginWithNaverId({
        callbackUrl: callbackUrl,
        clientId: clientId,
        isPopup: true,
        loginButton: { color: "green", type: 3, height: 60 },
    });
    naverLogin.init();
    if (!window.opener) {
        naver.successCallback = function (data) { return onSuccess(data); };
        naver.failureCallback = function (status) { return onFailure(status); };
    } else {
        naverLogin.getLoginStatus(function (status) {
            if (status) {
                if(typeof window.opener.naver?.successCallback !== "function"){
                    window.opener.naver.successCallback = function (data) { return onSuccess(data); };
                }
                window.opener.naver.successCallback(naverLogin);
            }
            else {
                if (typeof window.opener.naver?.failureCallback !== 'function') {
                    window.opener.naver.failureCallback = function (status) { return onFailure(status); };
                }
                window.opener.naver.failureCallback(status);
            }
            window.close();
        });
    }
};
var appendNaverButton = function () {
    if (document && document.querySelectorAll("#naverIdLogin").length === 0) {
        var naverId = document.createElement("div");
        naverId.id = "naverIdLogin";
        naverId.style.position = "absolute";
        naverId.style.top = "-10000px";
        document.body.appendChild(naverId);
    }
};
var loadScript = function (props) {
    if (document && document.querySelectorAll("#naver-login-sdk").length === 0) {
        var script = document.createElement("script");
        script.id = "naver-login-sdk";
        script.src = NAVER_ID_SDK_URL;
        script.onload = function () { return initLoginButton(props); };
        document.head.appendChild(script);
    }
};
var LoginNaver = /** @class */ (function (_super) {
    __extends(LoginNaver, _super);
    function LoginNaver() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoginNaver.prototype.componentDidMount = function () {
        if (isServer()) {
            return;
        }
        appendNaverButton();
        loadScript(this.props);
    };
    LoginNaver.prototype.render = function () {
        var render = this.props.render;
        return render({
            onClick: function () {
                if (!document ||
                    !document.querySelector("#naverIdLogin").firstChild)
                    return;
                var naverLoginButton = document.querySelector("#naverIdLogin").firstChild;
                naverLoginButton.href = "javascript:;"
                naverLoginButton.click();
            },
        });
    };
    return LoginNaver;
}(React.Component));

exports.default = LoginNaver;