export const getFunnel = () => {
  const cafe24code =
    window.sessionStorage.getItem("cafe24_code") || undefined;
  const utm_campaign =
    window.sessionStorage.getItem("utm_campaign") || undefined;
  const utm_source = window.sessionStorage.getItem("utm_source") || undefined;
  let kakaostylecode: string | undefined = undefined;
  let nhncode = window.sessionStorage.getItem("nhn_code") || undefined;
  if (utm_source === "kakaostyle")
    kakaostylecode =
      utm_campaign === "230405"
        ? "kakao-style-30"
        : utm_campaign === "230410_50"
        ? "kakao-style-50"
        : undefined;

  let ret;
  if (cafe24code) {
    ret = `cafe24:${cafe24code}`;
  } else if (kakaostylecode) {
    ret = kakaostylecode;
  }
  if (nhncode) {
    ret = `nhn:${nhncode}`;
  }
  return ret;
}

export default getFunnel;