export const getUserAgent = () => {
  // web / window_desktop / mac_desktop
  // 주의! safari 등 일부 브라우저에서는 지원하지 않는 기능.
  // 데스크탑 앱은 chrome based라 os 추적용으로만 사용합니다.
  // "Android", "Chrome OS", "Chromium OS", "iOS", "Linux", "macOS", "Windows", "Unknown"
  const ug = window.navigator.userAgent;
  // @ts-ignore
  const ugd = window.navigator?.userAgentData;
  if (!ugd) return "web"; // userAgentData 지원하지 않는 브라우저는 web으로 간주
  if (!ug.includes("wrtn-desktop/")) return "web"; // desktop으로 접속하지 않으면 web으로 간주
  if (ugd.platform === "Windows") return "window_desktop"
  if (ugd.platform === "macOS") return "mac_desktop"
  return "web";
}

export const getPlatform = () => {
  const login_app = window.sessionStorage.getItem("login_app");
  const ug = window.navigator.userAgent;
  // @ts-ignore
  const ugd = window.navigator?.userAgentData;
  if (!ugd) return null; // userAgentData 지원하지 않는 브라우저는 web으로 간주
  if (!ug.includes("wrtn-desktop/") && (!login_app || login_app !== "dt")) return "web"; // desktop으로 접속하지 않으면 web으로 간주
  if (ugd.platform === "Windows") return "desktop_win"
  if (ugd.platform === "macOS") return "desktop_mac"
  return null;
}

export default getUserAgent;