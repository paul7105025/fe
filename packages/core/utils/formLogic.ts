//추후 코어 내부 로직으로 이동 예정

import { FORM_DEFAULT_VALUE_RECORD } from "../records";
import {
  FormArrayType,
  FormArrayTypeList,
  FormMetadata,
  FormMetadataType,
  FormOptions,
  FormStringType,
  FormType,
  FormValue,
} from "../types";

type KeysOfUnion<T> = T extends T ? keyof T : never;

interface formInterface<T extends FormType> {
  value: FormValue<T>;
  form: FormMetadata<T>;
  onChange: (data: FormMetadata<T>) => void;
  index?: number;
  option?: KeysOfUnion<FormOptions<T>>;
  guideOption?: "text" | "tag" | "isVisible";
}

interface formExpandableInterface<T extends FormType, K>
  extends formInterface<T> {
  e: K;
}

interface addFormInterface {
  value: string;
  form: FormMetadata<FormArrayType>;
  onChange: (data: FormMetadata<FormArrayType>) => void;
}

export const onChangeTextValue = ({
  value,
  form,
  onChange,
}: formInterface<FormStringType>) => {
  onChange({
    ...form,
    value: value,
  });
};

export const onChangeListValue = ({
  value,
  form,
  onChange,
  index,
}: formInterface<FormArrayType>) => {
  if (typeof index === "number") {
    onChange({
      ...form,
      value: [
        ...form.value.slice(0, index),
        ...value,
        ...form.value.slice(index + 1),
      ],
    });
  }
};

export const onPlusListValue = ({
  value,
  form,
  onChange,
}: addFormInterface) => {
  if (form.value.length > 0) {
    onChange({
      ...form,
      value: [...form.value, value],
    });
  } else {
    onChange({
      ...form,
      value: [value],
    });
  }
};

export const onMinusListValue = ({
  value,
  index,
  form,
  onChange,
}: formInterface<FormArrayType>) => {
  if (typeof index === "number") {
    onChange({
      ...form,
      value: [...form.value.slice(0, index), ...form.value.slice(index + 1)],
    });
  }
};

export const onChangeGuide = ({
  value,
  guideOption,
  form,
  onChange,
}: formExpandableInterface<FormType, string>) => {
  if (guideOption) {
    onChange({
      ...form,
      guide: {
        ...form.guide,
        [guideOption]: value,
      },
    });
  }
};

export const onChangeLabel = ({
  value,
  form,
  onChange,
}: formInterface<FormType>) => {
  onChange({
    ...form,
    label: value as string,
  });
};

export const onChangeOption = <T extends FormType>({
  value,
  option,
  form,
  onChange,
}: formInterface<T>) => {
  if (option) {
    onChange({
      ...form,
      option: {
        ...form.option,
        [option]: value,
      },
    });
  }
};

export const onPlusValueAndOption = ({
  value,
  option,
  form,
  onChange,
}: formExpandableInterface<
  "input_expandable" | "input_keyword",
  FormMetadata<
    "input_expandable" | "input_keyword"
  >["option"][keyof FormOptions<"input_expandable" | "input_keyword">]
>) => {
  if (option !== undefined) {
    onChange({
      ...form,
      value: [...form.value, ""],
      option: {
        ...form.option,
        [option]: [...form.option[option], value],
      },
    });
  }
};

export const onMinusValueAndOption = ({
  index,
  option,
  form,
  onChange,
}: formExpandableInterface<
  "input_expandable" | "input_keyword",
  FormMetadata<
    "input_expandable" | "input_keyword"
  >["option"][keyof FormOptions<"input_expandable" | "input_keyword">]
>) => {
  if (typeof index === "number" && option !== undefined) {
    onChange({
      ...form,
      value: [...form.value.slice(0, index), ...form.value.slice(index + 1)],
      option: {
        ...form.option,
        [option]: [
          ...form.option[option].slice(0, index),
          ...form.option[option].slice(index + 1),
        ],
      },
    });
  }
};

export const resetAllValue = (
  forms: Array<FormMetadataType>
): Array<FormMetadataType> => {
  return forms.map((form) => ({
    ...form,
    value: FORM_DEFAULT_VALUE_RECORD[form.type],
  }));
};

export const checkDisabled = (forms: Array<FormMetadataType>) => {
  let disabled = false;

  forms.forEach((form) => {
    if (Array.isArray(form.value) && form.type === "input_divided") {
      form.value.forEach((value) => {
        if (value === "") {
          disabled = true;
        }
      });
    }

    if (form.type !== "input_keyword" && typeof form.value === "string") {
      if (form.value.trim().length === 0) {
        disabled = true;
      }
    }
  });

  return disabled;
};

export const manufactureOutput = (forms: Array<FormMetadataType>) => {
  return {
    input: forms
      .map((form) => {
        if (form.type === "input_keyword" || form.type === "input_divided") {
          if (Array.isArray(form.value) && form.value.length !== 0) {
            return form.value.map((v) => "'" + v + "'").join(", ");
          } else return "";
        } else if (form.type === "option_chips") {
          return null;
        } else return form.value;
      })
      .filter((v) => v || v === ""),
    option: forms
      .map((form) => (form.type !== "option_chips" ? null : form.value))
      .filter((v) => v),
  };
};
