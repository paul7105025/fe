const Josa = {
  "이/가": ["이", "가"],
  "을/를": ["을", "를"],
  "은/는": ["은", "는"],
  "와/과": ["와", "과"],
  "으로/로": ["으로", "로"],
};

/**
 * 맞춤법에 맞는 조사를 가져와 반환합니다.
 * @param text
 * @param josa
 * @returns {string} 사람, 은/는 -> 사람은
 *
 * @source https://github.com/naradesign/hangul.josa.js/blob/main/hangul.josa.js
 * @license MIT Copyright (c) 2016 ChanMyeong Jeong
 */

export const getJosa = (text: string, josa: keyof typeof Josa) => {
  const trimed = text.replace(/\(.*\)/gi, "").replace(/[^가-힣a-z\d]/gi, "");
  const final =
    (/[가-힣]$/.test(trimed) &&
      (trimed.substr(-1).charCodeAt(0) - 0xac00) % 28 > 0) ||
    /[가-힣]\d*[013678]$/.test(trimed) ||
    /[a-z]\d*[1789]$/i.test(trimed) ||
    /([clmnp]|[blnt](e)|[co](k)|[aeiou](t)|mb|ng|lert)$/i.test(trimed);

  if (!/([가-힣]|\d|[a-z])$/i.test(trimed)) {
    return text + `${Josa[josa].join("(")})`;
  }

  return text + `${Josa[josa][final ? 0 : 1]}`;
};
