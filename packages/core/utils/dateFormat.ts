import dayjs from "dayjs";

export const dateCalculator = (value: string) => {
  const today = new Date();
  const timeValue = new Date(value);

  const betweenTime = Math.floor(
    (today.getTime() - timeValue.getTime()) / 1000 / 60
  );
  if (betweenTime < 1) return "방금 전";
  else if (betweenTime < 60) {
    return `${betweenTime}분전`;
  }

  const betweenTimeHour = Math.floor(betweenTime / 60);
  if (betweenTimeHour < 24) {
    return `${betweenTimeHour}시간전`;
  }

  const betweenTimeDay = Math.floor(betweenTime / 60 / 24);
  if (betweenTimeDay <= 6) {
    return `${betweenTimeDay}일전`;
  }

  return dayjs(value).format("YYYY.MM.DD").toString();
};

export const dayCalculator = (value: string) => {
  const today = new Date();
  const timeValue = new Date(value);

  const betweenTime = Math.floor(
    (today.getTime() - timeValue.getTime()) / 1000 / 60
  );

  const betweenTimeHour = Math.ceil(betweenTime / 60);

  const betweenTimeDay = Math.ceil(betweenTimeHour / 24);

  if (betweenTimeHour < today.getHours()) {
    return "오늘";
  } else if (
    betweenTimeHour >= today.getHours() &&
    betweenTimeHour <= 48 + today.getHours()
  ) {
    return "어제";
  } else if (
    betweenTimeDay >= 48 + today.getHours() &&
    betweenTimeDay <= 120 + today.getHours()
  ) {
    return `${betweenTimeDay}일전`;
  } else {
    return `${timeValue.getMonth() + 1}월 ${timeValue.getDate()}일`;
  }
};

export const timeCalculator = (value: string) => {
  const timeValue = new Date(value);

  return timeValue.getHours() >= 12
    ? timeValue.getHours() === 12
      ? `오후 ${timeValue.getHours().toString().padStart(2, "0")}:${timeValue
          .getMinutes()
          .toString()
          .padStart(2, "0")}`
      : `오후 ${(timeValue.getHours() - 12)
          .toString()
          .padStart(2, "")}:${timeValue
          .getMinutes()
          .toString()
          .padStart(2, "0")}`
    : `오전 ${timeValue.getHours().toString().padStart(2, "")}:${timeValue
        .getMinutes()
        .toString()
        .padStart(2, "0")}`;
};

export const dateResultFormat = (date: string) => {
  return `${dayCalculator(date)} ${timeCalculator(date)}`;
};
