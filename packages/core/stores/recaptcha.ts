import { atom } from "recoil";

export const recaptchaState = atom({
  key: "recaptchaState",
  default: {
    isVerified: false,
    openRecaptcha: () => {},
    registerByRecaptcha: () => {},
  },
});
