import { atom, selector } from "recoil";

export type inappChatState = null | {
  isOpenChat: true
}

export const inappChatOpenState = atom<boolean>({
  key: 'inappChatOpenState',
  default: false,
})

export const inappChatDefaultValueState = atom<string | null>({
  key: 'inappChatDefaultValueState',
  default: null,
})