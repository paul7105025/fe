import { atom } from "recoil";
import { IsBlockIPLoginState, IsBlockWithDrawLoginState, IsDuplicateLoginState } from "../types/login";
import { userType } from "../types";

export const userState = atom<userType | null>({
  key: "userState",
  default: null,
});

export const accessTokenState = atom<string | null>({
  key: "accessTokenState",
  default: null,
});

export const openCertState = atom<boolean>({
  key: "openCertState",
  default: false,
});

export const isDuplicateLoginState = atom<IsDuplicateLoginState>({
  key: "isDuplicateLoginState",
  default : null,
})

export const isWithdrawalUserState = atom<{
  provider: string;
  email: string;
} | null>({
  key: "isWithdrawalUserState",
  default: null,
});

export const isBlockIPState = atom<IsBlockIPLoginState>({
  key: 'isBlockIPState',
  default: null,
})

export const isBlockWithdrawState = atom<IsBlockWithDrawLoginState>({
  key: 'isBlockWithdrawState',
  default: null,
});

export const loginDialogState = atom({
  key: 'loginDialogState',
  default: false,
})