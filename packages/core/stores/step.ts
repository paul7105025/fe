import { atom } from "recoil";
import { StepToolType } from "../types/tool";

export const stepListState = atom<Array<StepToolType>>({
  key: "stepListState",
  default: [],
});
