import { atom } from "recoil";

export const metricState = atom<number>({
  key: "metricState",
  default: 0,
});
