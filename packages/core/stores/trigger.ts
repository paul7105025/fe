import { atom, atomFamily, selector, useRecoilCallback } from "recoil";

// 트리거 사용되었는지 여부 저장하는 공간입니다.
export const triggerState = atomFamily({
  key: "triggerState",
  default: () => false,
});

// INFO: 트리거 내부 구조

// 툴 생성 시 스코어 트리거를 위한 State입니다
export const currentCountScoreState = atom({
  key: "currentCountScoreState",
  default: {},
});

// 생성 시 총 합계
export const triggerGenerateCountState = atom<number>({
  key: 'triggerGenerateCountState',
  default: 0,
});

export const triggerWrtnChatState = selector<boolean>({
  key: 'triggerWrtnChatState',
  get: ({ get }) => {
    const count = get(triggerGenerateCountState);
    if (count === 1 || (count > 0 && count % 5 === 0)) return true;
    return false;
  }
})