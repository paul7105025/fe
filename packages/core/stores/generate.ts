import { atom } from "recoil";

export const cachedFormsListState = atom<Array<string>>({
  key: "cachedFormsListState",
  default: [],
});

export const cachedHateFormsListState = atom<Array<string>>({
  key: "cachedHateFormsListState",
  default: [],
});

export const isGeneratedState = atom<boolean>({
  key: "isGeneratedState",
  default: false,
});
