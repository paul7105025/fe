import { atom } from "recoil";
import { StoreToolFormType, StoreToolType } from "../types/store";

export const currentStoreCategoryFilterState = atom({
  key: 'currentStoreCategoryFilterState',
  default: '전체'
})

export const currentStoreListLiveTrendState = atom<StoreToolType[]>({
  key: 'currentStoreListLiveTrendState',
  default: []
});

export const currentStoreToolListState = atom<StoreToolType[]>({
  key: 'currentStoreToolListState',
  default: []
})

export const currentStoreSearchState = atom({
  key: 'currentStoreSearchState',
  default: ""
})

export const favoriteStoreToolListState = atom<string[]>({
  key: 'favoriteStoreToolListState',
  default: []
});

export const favoriteStoreChatbotListState = atom<string[]>({
  key: 'favoriteStoreChatbotListState',
  default: []
});

export const currentStoreDetailToolFormsState = atom<StoreToolFormType[]>({
  key: "currentStoreDetailToolFormsState",
  default: []
})

export const currentStoreDetailToolOutputsState = atom<string[]>({
  key: "currentStoreDetailToolOutputsState",
  default: []
})

export const currentStorePageState = atom<number>({
  key: 'currentStorePageState',
  default: 1,
});

export const currentStoreTabState = atom<"전체" | "일반" | string>({
  key: 'currentStoreTabState',
  default: "전체"
});

export const currentStoreTabIdState = atom<string | null>({
  key: 'currentStoreTabIdState',
  default: null
});

export const currentStoreTypeState = atom<'tool' | 'chatbot'>({
  key: 'currentStoreTypeState',
  default: 'tool'
});