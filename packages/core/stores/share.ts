import { atom } from "recoil";

export const likedChatIdsState = atom<string[] | null>({
  key: "likedChatIdsState",
  default: null,
})