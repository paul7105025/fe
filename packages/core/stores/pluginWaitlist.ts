import { atom } from "recoil";
import { pluginWaitlisterType } from "../types";

export const pluginWaitlisterState = atom<pluginWaitlisterType | null>({
  key: "pluginWaitlisterState",
  default: null,
});
