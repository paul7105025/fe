import { atom } from "recoil";

export const isNoCreditModalOpenState = atom<boolean>({
  key: "isNoCreditModalOpenState",
  default: false,
});

export const isClosingState = atom<boolean>({
  key: "isClosingState",
  default: false,
});

export const todayMetricState = atom<number>({
  key: "todayMetricState",
  default: 0,
});
