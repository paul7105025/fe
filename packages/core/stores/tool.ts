import { atom, selector } from "recoil";

import { ToolType } from "../types";
import { getTool_Id } from "../services";

export const favoriteIdListState = atom<Array<string>>({
  key: "favoriteIdListState",
  default: [],
});

export const favoriteToolListState = selector<Array<ToolType>>({
  key: "favoriteToolListState",
  get: ({ get }) => {
    const favoriteIdList = get(favoriteIdListState);
    const toolList = get(toolListState);

    if (!favoriteIdList || !toolList) return [];
    return toolList.filter((tool) => favoriteIdList.includes(tool._id));
  },
});

// toolState의 초깃값을 유저의 데이터를 관장하는 상위 프레임 워크 페이지를 만들어 그곳에서 초깃값 세팅을 할 수 있도록 할 계획입니다.
// export const toolState = atom({
//   key: "toolState",
//   default: null,
// });

/* */
export const toolIdState = atom<string>({
  key: "toolIdState",
  default: "",
});

export const toolState = selector<ToolType | null>({
  key: "toolState",
  get: async ({ get }) => {
    const toolId = get(toolIdState);
    if (!toolId || toolId === "")
      return {
        _id: "",
      };
    const res = await getTool_Id({ toolId });
    if (res?.status === 200) {
      return res.data.data;
    }
    return null; // 혹은 기본값
  },
});

export const toolListState = atom<Array<ToolType> | null>({
  key: "toolListState",
  default: [],
});

export const toolLoadingState = atom<boolean>({
  key: "toolLoadingState",
  default: false,
});

export const recommendToolState = atom<Array<ToolType>>({
  key: "recommendToolState",
  default: [],
});

export const currentToolState = atom<ToolType | null>({
  key: "currentToolState",
  default: null,
});

export const outputLoadingState = atom<boolean>({
  key: "outputLoadingState",
  default: false,
});

type templateToolType = {
  _id: string;
  name: string;
  content: string;
}
export const templateToolListState = atom<templateToolType[]>({
  key: 'templateToolListState',
  default: [],
})

export const selectedTemplateState = atom<{ id: string, name: string, content: string}>({
  key: 'selectedTemplateState',
  default: {
    id: "",
    name: "",
    content: ""
  },
})