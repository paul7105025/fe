import { atom } from "recoil";


export const isDialogOpenState = atom<boolean>({
  key: 'isDialogOpenState',
  default: false,
});