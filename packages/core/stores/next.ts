import { atom } from "recoil";

export const nextRouteState = atom<any>({
  key: "nextRouteState",
  default: null,
});
