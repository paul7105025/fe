import { atom } from "recoil"

export const accessKeyState = atom({
  key: "accessKeyState",
  default: ""
});