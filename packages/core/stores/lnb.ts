import { atom, selector } from "recoil";

type LNBMenu = "chat" | "store" | "tool";

export const currentLNBMenuState = atom<LNBMenu>({
  key: 'currentLNBMenuState',
  default: "chat"
});

export const currentLNBOpenState = atom<boolean>({
  key: 'currentLNBOpenState',
  default: true,
})