import { atom, atomFamily } from "recoil";

// export const favoriteToolDialogState = atom({
//   key: 'favoriteToolDialogState',
//   default: false,
// })

export const modalOpenState = atomFamily({
  key: "modalOpenState",
  default: false,
});

export const modalResolveState = atom({
  key: "modalResolveState",
  default: false,
});
