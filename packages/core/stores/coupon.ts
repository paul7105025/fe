import { atom } from "recoil";

export const couponListState = atom({
  key: "couponListState",
  default: [],
});
