import { atom } from "recoil";
import { BundleToolType } from "../types/tool";

export const bundleListState = atom<Array<BundleToolType>>({
  key: "bundleListState",
  default: [],
});
