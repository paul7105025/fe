import { atom } from "recoil";

export const mixpanelLoadedState = atom({
  key: 'mixpanelLoadedState',
  default: false,
})