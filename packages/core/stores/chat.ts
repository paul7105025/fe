import { atom } from "recoil";
import { allMessageType, chatType, fileType } from "../types";

export const TOGGLE_SPEED = 0;
export const TOGGLE_PERFORMANCE = 1;
export const TOGGLE_BARD = 2;

export const chatListState = atom<Array<chatType>>({
  key: "chatListState",
  default: [],
});

export const blockChatUsageState = atom({
  key: "blockChatUsageState",
  default: false,
});

export const blockChatLimitState = atom({
  key: "blockChatLimitState",
  default: false,
});

export const hoverChatGuideState = atom<boolean>({
  key: "hoverChatGuideState",
  default: false,
});

export const currentToolChatIdState = atom<string | null>({
  key: "currentToolChatIdState",
  default: null,
});

export const chatToggleState = atom<number>({
  key: "chatToggleState",
  default: TOGGLE_SPEED,
});

export const chipSuggestOpenState = atom<boolean>({
  key: "chipSuggestOpenState",
  default: false,
});

export const currentChatState = atom<chatType | null>({
  key: "currentChatState",
  default: null,
});

export const allMessagesState = atom<Array<allMessageType>>({
  key: "allMessagesState",
  default: [],
});

export const chatLoadingState = atom<boolean>({
  key: "chatLoadingState",
  default: false,
});

// file
export const currentFileState = atom<fileType | null>({
  key: "currentFileState",
  default: null,
});

export const allFilesState = atom<Array<fileType>>({
  key: "allFilesState",
  default: [],
});

export const fileLoadingProgressState = atom<number>({
  key: "fileLoadingProgressState",
  default: 0,
});

export type fileLoadingTimerType = {
  intervalId: NodeJS.Timer;
  loadingId: string;
  fileId: string;
};

export const fileLoadingTimerState = atom<fileLoadingTimerType | null>({
  key: "fileLoadingTimerState",
  default: null,
});

export const fileSuggestChipsState = atom<string[] | null>({
  key: "fileSuggestChipsState",
  default: null,
});

export type chatInputToolSourceType = "leftbar" | "chat_field";

export const chatInputToolSourceState = atom<chatInputToolSourceType>({
  key: "global/chatInputToolSourceState",
  default: "leftbar",
});

export const streamMessageState = atom<string | null>({
  key: "streamMessageState",
  default: "",
});

export const streamAbortState = atom<{
  startTime: Date;
  controller: AbortController;
} | null>({
  key: "streamAbortState",
  default: null,
});

export type chatInputType = "tool" | "default" | "search";

export const chatInputTypeState = atom<chatInputType>({
  key: "@wrtn/chat/chatInputTypeState",
  default: "default",
});
