import { userSurveyType } from "../types/user";

export const SurveyStepList = ["STEP_TERMS", "STEP_JOB_SURVEY"] as const;

export type SurveyStep = (typeof SurveyStepList)[number];

export const STEP_NAME_RECORD: Record<SurveyStep, string> = {
  STEP_TERMS: "",
  STEP_JOB_SURVEY: "직업",
};

export const SURVEY_TEXT_BY_STEP: Record<SurveyStep, string> = {
  STEP_TERMS: "",
  STEP_JOB_SURVEY: "job",
};

export const surveyItemList = {
  STEP_TERMS: [],
  STEP_JOB_SURVEY: [
    "학생",
    "개발자",
    "마케터",
    "창작자, 블로거, 작가",
    "사무직, 회사원",
    "자영업자, 프리랜서",
    "교사, 강사",
    "기타",
  ],
};

export const survey = {
  STEP_TERMS: null,
  STEP_JOB_SURVEY: {
    value: "job",
    items: surveyItemList["STEP_JOB_SURVEY"],
    title: "어떤 일을 하고 계신가요?",
    subTitle: "",
    etc: "현재 하고 계시는 일을 알려주세요!",
    max: 1,
  },
};
