export const GENERATE_INVALID_INPUT = "내용을 다시 작성해주세요.";
export const GENERATE_FAIL =
  "일시적으로 생성에 실패했어요. 다시 시도해주세요.";

export const DEFAULT_ERROR_MESSAGE =
  "다시 시도해주세요.";

export const SERVER_ERROR_MESSAGE =
  "잠시후 다시 시도해주세요.";

export const SERVICE_UNAVAILABLE_MESSAGE =
  "중국어 입력은 지원하지 않습니다.";

export const GENERATE_FAIL_TOO_MANY_REQUEST =
"사용량이 많아 일시적으로 생성에 실패했어요. 다시 시도해주세요.";

export const ANALYZE_FAIL = "분석에 실패했어요.";
