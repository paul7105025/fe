import { modalSurveyType } from "../types";

export const withdrawSurvey: modalSurveyType = {
  title: "탈퇴하시는 이유가 무엇인가요?",
  subTitle: "탈퇴하려는 이유가 무엇인지 알려주세요.",
  survey: [
    {
      id: 1,
      type: "button",
      name: "뤼튼을 그만 사용하려고 해요",
    },
    {
      id: 2,
      type: "button",
      name: "다른 서비스를 사용하려고 해요",
    },
    {
      id: 3,
      type: "button",
      name: "계정이 여러개에요",
    },
  ],
};

export const withdrawDetailSurvey: modalSurveyType = {
  title: "뤼튼을 떠나신다니 아쉬워요.",
  subTitle:
    "탈퇴하시려는 이유를 조금 더 자세히 알려주세요.\n더 나은 서비스를 위한 자료로 사용하겠습니다.",
  survey: [
    {
      id: 1,
      type: "select",
      name: "자주 사용하지 않아요",
      select: [
        "1주에 1회 정도",
        "2주에 1번 정도",
        "3주에 1~2번 정도",
        "한 달에 1~2번 정도",
        "기타",
      ],
      placeholder: "이용 빈도를 알려주세요.",
    },
    {
      id: 2,
      type: "select",
      name: "결과물이 만족스럽지 않아요",
      select: [
        "문법이 부자연스러워요",
        "반복되는 결과물이 많이 나와요",
        "맥락에 맞지 않는 결과물이 나와요",
        "이상한 결과물이 나와요 (반복적인 문자열 등)",
        "기타",
      ],
      placeholder: "결과물이 만족스럽지 않은 이유를 알려주세요.",
    },
    {
      id: 3,
      type: "input",
      name: "필요한 기능이 없어요",
      placeholder: "필요한 툴, 기능이 있다면 알려주세요.",
    },
    {
      id: 4,
      type: "input",
      name: "서비스 이용 과정이 불편해요",
      placeholder: "어떤 부분에서 불편함을 느끼셨는지 알려주세요.",
    },
    {
      id: 5,
      type: "input",
      name: "원하는 요금제가 없어요",
      placeholder: "원하는 요금제가 있다면 알려주세요.",
    },
    {
      id: 6,
      type: "input",
      name: "기타",
      placeholder: "탈퇴하시려는 이유를 알려주세요.",
    },
  ],
};

export const cancelSubscribeSurvey: modalSurveyType = {
  title: "구독을 취소하려는 이유가 무엇인가요?",
  subTitle: "구독을 취소하려는 이유가 무엇인지 알려주세요.",
  survey: [
    {
      id: 1,
      type: "button",
      name: "자주 사용하지 않아요",
    },
    {
      id: 2,
      type: "button",
      name: "구독 중인 요금제의 가격이 부담스러워요.",
    },
    {
      id: 3,
      type: "button",
      name: "다른 서비스를 이용하려고 해요",
    },
    {
      id: 4,
      type: "input",
      name: "기타",
      placeholder: "구독 취소에 대한 이유를 알려주세요.",
    },
  ],
};
