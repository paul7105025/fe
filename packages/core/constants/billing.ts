import { PlanType, PlanDiscountType } from "../types/billing";

export const PLAN_LEVEL: Record<PlanType, number> = {
  FREE: 0,
  PLUS: 1,
};

export const PLAN_COLOR: Record<PlanType, string> = {
  FREE: "#A1A5B9;",
  PLUS: "#5A2CDA",
};

export const PLAN_NAME: Record<PlanType, string> = {
  FREE: "무료",
  PLUS: "플러스",
};

export const PLAN_DESC: Record<PlanType, string> = {
  FREE: "누구나 무료로 뤼튼의 다양한 기능을\n사용할 수 있습니다.  ",
  PLUS: "프리미엄 작업 기능들을 사용하여\n더 크게 생산성 을 향상시킬 수 있습니다.  ",
};

export const PLAN_ORIGINAL_PRICE: Record<PlanType, number> = {
  FREE: 0,
  PLUS: 44900,
};

export const PLAN_PRICE_UNIT: Record<PlanType, string> = {
  FREE: "/ 월",
  PLUS: "/ 월, VAT 포함",
};

export const PLAN_PRICE: Record<PlanType | PlanDiscountType, number> = {
  FREE: 0,
  PLUS: 34900,
  PLUS_DISCOUNT_5_PERCENT: 33155,
  PLUS_DISCOUNT_10_PERCENT: 31410,
  PLUS_DISCOUNT_20_PERCENT: 27920,
  PLUS_DISCOUNT_30_PERCENT: 24430,
  PLUS_DISCOUNT_40_PERCENT: 20940,
  PLUS_DISCOUNT_50_PERCENT: 17450,
  PLUS_DISCOUNT_60_PERCENT: 13960,
  PLUS_DISCOUNT_70_PERCENT: 10470,
  PLUS_DISCOUNT_80_PERCENT: 6980,
  PLUS_DISCOUNT_90_PERCENT: 3490,
  PLUS_DISCOUNT_100_PERCENT: 0, // 무료로 결제
};

export const PLAN_AMOUNT: Record<PlanType, string> = {
  FREE: "inf",
  PLUS: "inf",
};

export const PLAN_AMOUNT_CONDITION: Record<PlanType, string> = {
  FREE: "생성 속도 제한 있음",
  PLUS: "더 빠른 속도로 생성 가능\n트래픽 과중 시 우선 접근 보장",
};

export const PLAN_MAX_GENERATE: Record<PlanType, number> = {
  FREE: 1,
  PLUS: 3,
};

export const PLAN_HISTORY_DEADLINE: Record<PlanType, number> = {
  FREE: 7,
  PLUS: 90,
};

export const PLAN_CS_PREFERENCE: Record<PlanType, boolean> = {
  FREE: false,
  PLUS: true,
};

export const PLAN_NEW_FEATURE_PREFERENCE: Record<PlanType, boolean> = {
  FREE: false,
  PLUS: true,
};

export const PLAN_DOWNLOAD_AVAILABLE: Record<PlanType, boolean> = {
  FREE: false,
  PLUS: true,
};

export const PLAN_MODIFY_AVAILABLE: Record<PlanType, boolean> = {
  FREE: false,
  PLUS: true,
};

export const PLAN_HREF: Record<PlanType, string> = {
  FREE: "",
  PLUS: "/setting/billing?plan=PLUS",
};

export const PLAN_INFO = {
  FREE: {
    level: PLAN_LEVEL.FREE,
    color: PLAN_COLOR.FREE,
    name: PLAN_NAME.FREE,
    desc: PLAN_DESC.FREE,
    originalPrice: PLAN_ORIGINAL_PRICE.FREE,
    price: PLAN_PRICE.FREE,
    priceUnit: PLAN_PRICE_UNIT.FREE,
    amount: PLAN_AMOUNT.FREE,
    amountCondition: PLAN_AMOUNT_CONDITION.FREE,
    maxGenerate: PLAN_MAX_GENERATE.FREE,
    historyDeadline: PLAN_HISTORY_DEADLINE.FREE,
    csPreference: PLAN_CS_PREFERENCE.FREE,
    newFeaturePreference: PLAN_NEW_FEATURE_PREFERENCE.FREE,
    downloadAvailable: PLAN_DOWNLOAD_AVAILABLE.FREE,
    modifyAvailable: PLAN_MODIFY_AVAILABLE.FREE,
    href: PLAN_HREF.FREE,
  },
  PLUS: {
    level: PLAN_LEVEL.PLUS,
    color: PLAN_COLOR.PLUS,
    name: PLAN_NAME.PLUS,
    desc: PLAN_DESC.PLUS,
    originalPrice: PLAN_ORIGINAL_PRICE.PLUS,
    price: PLAN_PRICE.PLUS,
    priceUnit: PLAN_PRICE_UNIT.PLUS,
    amount: PLAN_AMOUNT.PLUS,
    amountCondition: PLAN_AMOUNT_CONDITION.PLUS,
    maxGenerate: PLAN_MAX_GENERATE.PLUS,
    historyDeadline: PLAN_HISTORY_DEADLINE.PLUS,
    csPreference: PLAN_CS_PREFERENCE.PLUS,
    newFeaturePreference: PLAN_NEW_FEATURE_PREFERENCE.PLUS,
    downloadAvailable: PLAN_DOWNLOAD_AVAILABLE.PLUS,
    modifyAvailable: PLAN_MODIFY_AVAILABLE.PLUS,
    href: PLAN_HREF.PLUS,
  },
};
