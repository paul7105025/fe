export const REFUND_INFO_HREF =
  "https://wrtn.notion.site/4-8fccf221fdaf4074986156c7d7c2aa29";
export const WRTN_COMMUNITY_HREF = "https://wrtn.imweb.me/tutorial/?q=YToxOntzOjEyOiJrZXl3b3JkX3R5cGUiO3M6MzoiYWxsIjt9&bmode=view&idx=15117511&t=board";
export const WRTN_OPEN_KAKAO_HREF = "https://open.kakao.com/o/gPjTh04e";
export const WRTN_KAKAO_CHAT_HREF = "https://pf.kakao.com/_xeCwixj";
export const WRTN_ANDROID_HREF =
  "https://play.google.com/store/apps/details?id=com.wrtn.app";
export const WRTN_IOS_HREF =
  "https://apps.apple.com/kr/app/%EB%A4%BC%ED%8A%BC/id6448556170";
export const WRTN_MAC_HREF = "";
export const WRTN_WIDNOW_HREF = "";

export const WRTN_BLOG_HREF = "https://wrtn.ai/blog/";
export const WRTN_NAVER_BLOG_HREF = "https://blog.naver.com/wrtntechnologies";
export const WRTN_COURIER_HREF = "https://wrtn.career.greetinghr.com/";

export const WRTN_USER_GUIDE_HREF = "https://wrtn.imweb.me/";

export const WRTN_PRIVACY_POLICY_HREF = "https://wrtn.ai/privacy/";
export const WRTN_MARKETING_POLICY_HREF = " https://wrtn.ai/terms-marketing/";
