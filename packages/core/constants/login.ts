export const LOGIN_EMAIL = 0
export const LOGIN_PASSWORD = 1
export const LOGIN_LOGIN = 2
export const LOGIN_CERT = 3
export const FIND_EMAIL = 4
export const FIND_PASSWORD = 5
export const FIND_RESET_PASSWORD = 6
export const FIND_EMAIL_RESULT = 7


export const CERT_MAIL_INFO_DEFAULT = "";
export const CERT_MAIL_INFO_SEND = "메일로 인증코드를 발송했어요!";
export const CERT_MAIL_INFO_INVALID = "메일 주소가 유효하지 않아요.";
export const CERT_MAIL_INFO_EXISTED = "해당 메일 주소로 등록된 계정이 이미 있어요.";
export const CERT_MAIL_INFO_EXPIRED = "인증코드가 만료되었어요.";
export const CERT_MAIL_INFO_KAKAO = "해당 메일은 카카오 계정으로 가입되어 있어요.";
export const CERT_MAIL_INFO_GOOGLE = "해당 메일은 구글 계정으로 가입되어 있어요.";
export const CERT_MAIL_INFO_NAVER = "해당 메일은 네이버 계정으로 가입되어 있어요.";
export const CERT_MAIL_INFO_UNKNOWN = "등록되지 않은 이메일 주소에요.";

export const CERT_CODE_INFO_DEFAULT = "";
export const CERT_CODE_INFO_INVALID = "인증코드가 일치하지 않아요!";

export const CERT_PW_INFO_DEFAULT = "";
export const CERT_PW_INFO_INVALID = "비밀번호가 일치하지 않아요.";