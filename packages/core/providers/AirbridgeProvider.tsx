import airbridge from "airbridge-web-sdk-loader";
import React from "react";

const token = process.env.NEXT_PUBLIC_AIRBRIDGE_WEB_TOKEN || process.env.REACT_APP_AIRBRIDGE_WEB_TOKEN || "";

export const AirbridgeProvider = ({ children }) => {
  React.useEffect(() => {
    console.log(process.env.REACT_APP_AIRBRIDGE_WEB_TOKEN)

    airbridge.init({
      app: "wrtn",
      webToken: token,
    });
  }, []);

  return children;
};
