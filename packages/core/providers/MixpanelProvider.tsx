import React from "react";
import mixpanel from 'mixpanel-browser';
import { useRecoilState, useRecoilValue } from "recoil";

import { userState } from "../stores/login";
import { mixpanelLoadedState } from "../stores/mixpanel";

import { updateChannelUser } from "../hooks/product";
import { userType } from "../types/user";

const token = process.env.NEXT_PUBLIC_MIXPANEL_TOKEN || process.env.REACT_APP_MIXPANEL_TOKEN || "";
const isDev = process.env.NODE_ENV !== "production" || (process.env.NEXT_PUBLIC_ACTIVE_MIXPANEL === "true") || (process.env.REACT_APP_ACTIVE_MIXPANEL === "true");

export const MixpanelProvider = ({ children }) => {
  const user = useRecoilValue(userState);

  const [mixpanelLoaded, setMixpanelLoaded] = useRecoilState(mixpanelLoadedState);

  const updateUser = (user: userType) => {
    updateChannelUser({
      profile: {
        "current_plan": user.plan,
        "next_plan": user.nextPlan,
        "login_provider": user.provider,
        "job": user.job,
        "company": user.company,
        "required_terms": user?.serviceTerm && user?.privacyTerm ? true : false,
        "marketing_terms": user?.marketingTerm ? true : false,
      },
      profileOnce: {
        "signup_date": user?.agreementDate,
        "signup_period": 0,
        "count_generate_accum": 0,
        "amount_generate_accum": 0,
        "purchase_count_accum": 0,
        "purchase_amount_accum": 0,
      }
    });

    mixpanel?.identify(user?._id || "");
      
    mixpanel.people.set({
      "current_plan": user.plan,
      "login_provider": user.provider,
      "job": user.job,
      "company": user.company,
      "required_terms": user?.serviceTerm && user?.privacyTerm ? true : false,
      "marketing_terms": user?.marketingTerm ? true : false,
      "signup_date": user?.agreementDate,
      "user_id": user?._id,
      "is_login": true,
    })
    mixpanel.people.set_once({
      "signup_period": 0,
      "count_generate_accum": 0,
      "amount_generate_accum": 0,
      "purchase_count_accum": 0,
      "purchase_amount_accum": 0,
    })
  }

  const updateUnregisterUser = () => {
    mixpanel.people.set({
      "is_login": true,
    })
  }

  React.useEffect(() => {
    if (!isDev) { return ; }
      (async() => {
        if (user && mixpanel) {
          try { 
            updateUser(user);
          } catch {
             
          }
        }
      })();
  }, [user, mixpanelLoaded]);

  React.useEffect(() => {
  if (!isDev) { return }
    try {
      mixpanel?.get_distinct_id()
      setMixpanelLoaded(true);
    } catch {
      mixpanel.init(token, {
        loaded: (mp) => {
          const search_params = new URLSearchParams(window.location.search)
          const searchMap = {}
          for (const [key, value] of search_params.entries()) {
            searchMap[key] = value
          }
          mixpanel.people.set_once({
            searchParams: JSON.stringify(searchMap),
            firstTouchDate: new Date(),
          })
          updateChannelUser({
            profileOnce: {
              searchParams: JSON.stringify(searchMap),
              firstTouchDate: new Date(),
            }
          });
          setMixpanelLoaded(true);
        }
      })
    }
  }, [])

  return children;
};

export default MixpanelProvider;