import React from "react";
import Reaptcha from "reaptcha";
import { useSetRecoilState } from "recoil";

import { postRecaptcha } from "../services/recaptcha";
import { recaptchaState } from "../stores";

const SITE_KEY =
  process.env.NEXT_PUBLIC_RECAPTCHA_V2_SITE_KEY ||
  process.env.REACT_APP_RECAPTCHA_V2_SITE_KEY ||
  "";

export const RecaptchaProvider = ({ children }: React.PropsWithChildren) => {
  const [type, setType] = React.useState<"generate" | "register">("generate");
  const [isReady, setIsReady] = React.useState<boolean>(false);
  const [isRender, setIsRender] = React.useState<boolean>(false);

  const setRecaptcha = useSetRecoilState(recaptchaState);

  const setIsVerified = (isVerified: boolean) => {
    setRecaptcha((r) => ({ ...r, isVerified }));
  };

  const recaptchaRef = React.useRef<Reaptcha>(null);

  const executeRecaptcha = () => {
    setTimeout(() => {
      recaptchaRef?.current?.execute();
    }, 200);
  };

  const verifyRecaptcha = async (token: string) => {
    if (type === "generate") {
      const res = await postRecaptcha(token);

      if (res.status === 200) {
        //

        setIsVerified(true);
      } else {
        recaptchaRef?.current?.reset();
        setIsVerified(false);
      }
    } else {
      recaptchaRef?.current?.reset();
      setIsVerified(true);
    }
  };

  const openRecaptcha = () => {
    setType("generate");
    setIsVerified(false);

    executeRecaptcha();
  };

  const registerByRecaptcha = () => {
    setType("register");
    setIsVerified(false);

    executeRecaptcha();
  };

  React.useEffect(() => {
    setRecaptcha((r) => ({ ...r, openRecaptcha, registerByRecaptcha }));
  }, [isReady, isRender]);

  return (
    <>
      <Reaptcha
        ref={recaptchaRef}
        sitekey={SITE_KEY}
        onVerify={verifyRecaptcha}
        onLoad={() => setIsReady(true)}
        onRender={() => setIsRender(true)}
        size="invisible"
      />
      {children}
    </>
  );
};
