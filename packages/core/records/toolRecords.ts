import { ToolStatus, ToolTab, ToolTag, ImageCategory } from "../types";

export const TOOL_STATUS_RECORD: Record<ToolStatus, string> = {
  deploy: "배포",
  write: "작성",
  stop: "중지",
  delete: "삭제",
};

export const TOOL_CATEGORY_RECORD: Record<string, string> = {
  new: "신규",
  detail_page: "상세페이지",
  step: "단계별 글쓰기",
  bundle: "패키지",
  copywriting: "카피라이팅",
  product_introduction: "제품 소개",
  startup: "스타트업",
  email: "이메일",
  creation: "창작",
  customer_service: "고객 응대",
  kakao_talk: "카카오톡",
  sns: "SNS",
  writing: "글쓰기",
  none: "없음",
  all: "전체",
  new_year: "신년",
  never: "=====위에꺼만 쓰세요=====",
  //
  favorite: "즐겨찾기",
  blog: "블로그",
  marketing: "마케팅",
  self_employment: "쇼핑몰",
  work: "업무용",
  youtube: "유튜브",
  normal: "범용/일반",
  student: "학생",
  misc: "기타",
};

export const IMAGE_CATEGORY_RECORD: Record<ImageCategory, string> = {
  normal: "일반",
  "pop-art": "팝아트",
  water: "물감",
  oil: "유화",
  animation: "애니메이션",
};

export const ALL_CATEGORY_SELECTOR = {
  ...TOOL_CATEGORY_RECORD,
  ...IMAGE_CATEGORY_RECORD,
};

export const TOOL_TAB_RECORD: Record<ToolTab, string> = {
  liked: "좋아요",
  all: "전체",
  image: "이미지",
};

export const TOOL_TAG_RECORD: Record<
  ToolTag,
  {
    name: string;
    color: string;
  }
> = {
  none: {
    name: "없음",
    color: "BLACK",
  },
  title: { name: "제목", color: "TAG_BLUE" },
  main_text: { name: "본문", color: "TAG_PURPLE" },
  description: {
    name: "설명",
    color: "TAG_YELLOW",
  },
  title_title: {
    name: "타이틀",
    color: "TAG_RED",
  },
  intro_subject: {
    name: "서론",
    color: "TAG_GREEN",
  },
  main_subject: {
    name: "본론",
    color: "TAG_ORANGE",
  },
  summary: {
    name: "개요",
    color: "TAG_INDIGO",
  },
  index: {
    name: "목차",
    color: "TAG_LIGHT_GRASS",
  },
  general: {
    name: "범용",
    color: "TAG_PINK",
  },
  detail_index: {
    name: "세부 목차",
    color: "TAG_GRASS",
  },
  beta: {
    name: "BETA",
    color: "TAG_RED",
  },
  new: {
    name: "NEW!",
    color: "STAR_YELLOW",
  },
  conclusion: {
    name: "결론",
    color: "TAG_LIGHT_ORANGE"
  }
};
