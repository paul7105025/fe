import {
  codeStatusType,
  couponConditionType,
  couponSalesType,
  couponType,
  userCouponType,
} from "../types/coupon";

export const COUPON_TYPE_RECORD: Record<couponSalesType, string> = {
  discount: "정률 할인(%)",
  experience: "체험",
};

export const COUPON_CONDITION_RECORD: Record<couponConditionType, string> = {
  "first-payment": "가입 후 최초 결제 시 사용 가능",
  "first-coupon": "가입 후 최초 1회 즉시 적용 가능",
};

export const COUPON_CONDITION_SHORT_RECORD: Record<
  couponConditionType,
  string
> = {
  "first-payment": "가입 후 최초 결제 시 사용 가능",
  "first-coupon": "가입 후 최초 1회 즉시 적용 가능",
};

export const userCouponStatusRecord: Record<codeStatusType, string> = {
  available: "사용 가능",
  expired: "기간 만료",
  used: "사용 완료",
  using: "사용 중",
};

export const couponConditionText = (coupon: couponType, isShort?: boolean) => {
  if (!isShort) {
    if (coupon?.condition === undefined) return `-`;
    if (coupon.condition === "first-payment")
      return `가입 후 최초 결제 시 사용 가능`;
    if (coupon.condition === "first-coupon")
      return `가입 후 최초 1회 즉시 적용 가능`;
  }
  if (isShort) {
    if (coupon?.condition === undefined) return `-`;
    if (coupon.condition === "first-payment") return `결제 시 사용 가능`;
    if (coupon.condition === "first-coupon") return `가입 후 최초 1회`;
  }

  return "-";
};

export const couponApplyText = (coupon: couponType) => {
  if (coupon.type === "discount") {
    return `쿠폰 사용일로부터 ${coupon.count}개월간 ${coupon.percent}% 할인 혜택 제공 후, 플러스 요금제 결제 시작`;
  }
  if (coupon.type === "experience") {
    return `쿠폰 등록일로부터 ${coupon.count}일간 플러스 요금제 혜택 무료 제공`;
  }

  return "-";
};

export const couponApplyTextShort = (coupon: couponType) => {
  if (coupon.type === "discount") {
    return `쿠폰 사용일로부터 ${coupon.count}개월간`;
  }
  if (coupon.type === "experience") {
    return `쿠폰 등록일로부터 ${coupon.count}일간`;
  }

  return "-";
};
