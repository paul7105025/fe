import { FormType, FormValue } from "../types";

export const FORM_TYPE_RECORD: Record<FormType, string> = {
  checkbox: "체크박스",
  counter: "카운터",
  input: "한 줄 입력",
  textarea: "여러 줄 입력",
  label: "라벨",
  option_chips: "라디오 옵션 선택",
  light_option_chips: "라디오 옵션 선택(경량)",
  input_keyword: "키워드 입력",
  input_expandable: "다중 줄 입력",
  select: "드롭다운",
  select_image: "이미지 선택",
  input_divided: "분할 입력",
};

export const FORM_DEFAULT_VALUE_RECORD: Record<FormType, FormValue<FormType>> = {
  checkbox: "",
  counter: 1,
  input: "",
  textarea: "",
  label: "",
  option_chips: "",
  light_option_chips: "",
  input_keyword: [],
  input_expandable: [],
  select: "",
  select_image: "",
  input_divided: [],
};
