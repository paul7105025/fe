// export type couponType =  couponDiscountType | couponExperienceType;

export type couponSalesType = "discount" | "experience"
// export type codeType = 'no-code' | 'random-code' | 'init-code'
export type couponConditionType = "first-payment" | "first-coupon"


export type couponType = {
  _id?: string;
  name: string;
  description: string;
  dueDate: string;
  type: couponSalesType;
  count: number;
  quantity: number;
  condition?: couponConditionType;
  code?: string[];
  percent?: number;
}
// export type couponExperienceType = {
//   _id: string;
//   name: string;
//   description: string;
//   dueDate: string;
//   quantity: number;
//   type: "experience"; // always experience
//   condition: couponConditionType;
//   count: number;
//   code: string | string[]; // for only dashboard
// };

// export type couponDiscountType = {
//   _id: string;
//   name: string;
//   description: string;
//   dueDate: string;
//   quantity: number;
//   type: "discount"; // always discount
  
//   count: number;
//   percent: number;
//   condition: couponConditionType;
//   code?: string | string[]; // for only dashboard
// }

export type codeStatusType = 'used' | 'using' | 'available' | 'expired'
export type userCouponType = {
  couponId: string;
  code: string;
  status: codeStatusType;
  userId: string; // for only dashboard
  count: number // for only dashboard
}

export const couponFilterList = [
  "전체",
  "사용 가능 쿠폰",
  "할인 쿠폰",
] as const;

export type couponFilterType = typeof couponFilterList[number];
