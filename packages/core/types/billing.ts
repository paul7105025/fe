export const PlanList = [
  "FREE",
  "PLUS",
] as const;

export type PlanType = typeof PlanList[number];

export const PlanDiscountList = [
  "PLUS_DISCOUNT_5_PERCENT",
  "PLUS_DISCOUNT_10_PERCENT",
  "PLUS_DISCOUNT_20_PERCENT",
  "PLUS_DISCOUNT_30_PERCENT",
  "PLUS_DISCOUNT_40_PERCENT",
  "PLUS_DISCOUNT_50_PERCENT",
  "PLUS_DISCOUNT_60_PERCENT",
  "PLUS_DISCOUNT_70_PERCENT",
  "PLUS_DISCOUNT_80_PERCENT",
  "PLUS_DISCOUNT_90_PERCENT",
  "PLUS_DISCOUNT_100_PERCENT",
] as const;

export type PlanDiscountType = typeof PlanDiscountList[number];

// export type PlanDiscountType = {
//   PLUS_DISCOUNT_5_PERCENT: number,
//   PLUS_DISCOUNT_10_PERCENT: number,
//   PLUS_DISCOUNT_20_PERCENT: number,
//   PLUS_DISCOUNT_30_PERCENT: number,
//   PLUS_DISCOUNT_40_PERCENT: number,
//   PLUS_DISCOUNT_50_PERCENT: number,
//   PLUS_DISCOUNT_60_PERCENT: number,
//   PLUS_DISCOUNT_70_PERCENT: number,
//   PLUS_DISCOUNT_80_PERCENT: number,
//   PLUS_DISCOUNT_90_PERCENT: number,
//   PLUS_DISCOUNT_100_PERCENT: number,
// }


export type Card = {
  company: string;
  issuerCode: string;
  acquirerCode: string;
  number: string;
  installmentPlanMonths: number;
  isInterestFree: boolean;
  interestPayer: null;
  approveNo: string;
  useCardPoint: boolean;
  cardType: string;
  ownerType: string;
  acquireStatus: string;
  receiptUrl: string;
  amount: number;
};

export type PaymentHistory = {
  _id: string;
  userId: string;
  kind: string;
  mId: string;
  lastTransactionKey: string;
  paymentKey: string;
  orderId: string;
  orderName: string;
  taxExemptionAmount: number;
  status: string;
  requestedAt: string;
  approvedAt: string;
  useEscrow: boolean;
  cultureExpense: boolean;
  card: Card;
  virtualAccount: null;
  transfer: null;
  mobilePhone: null;
  giftCertificate: null;
  cashReceipt: null;
  discount: null;
  cancels: null;
  secret: null;
  type: string;
  easyPay: null;
  country: string;
  failure: null;
  isPartialCancelable: boolean;
  receipt: {
    url: string;
  };
  checkout: {
    url: string;
  };
  transactionKey: string;
  currency: string;
  totalAmount: number;
  balanceAmount: number;
  suppliedAmount: number;
  vat: number;
  taxFreeAmount: number;
  method: string;
  version: string;
  createdAt: string;
  updatedAt: string;
  __v: number;
};
