export type ShareChatType = {
  _id: string;
  topic: string;
  userName: string;
  userId: string;
  originalId: string;
  shareId: string;
  views: number;
  likes?: number;
  isDeleted: boolean;
  updatedAt: string;
  createdAt: string;
}

export type ShareMessageType = {
  _id: string;
  chatId: string;
  userId: string;
  originalId: string;

  shareId: string;
  isDeleted: boolean;
  content: string;
  role: string;
  type: string;

  liked: boolean;
  disliked: boolean;
  meta: { platform: string };
  createdAt: string;
  updatedAt: string;

  image?: { _id: number; imageUrls: { url: string }[] };
  pluginName?: string;
}

export type InputMessageType = ShareMessageType;
export type OutputMessageType = ShareMessageType[];
