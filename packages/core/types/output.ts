export interface OutputMetadata {
  createdAt: string;
  updatedAt: string;
  disliked: boolean;
  liked: boolean;
  _id: string;
  output: string;
  originOutput: string;
  inputs: Array<string>;
  options: Array<string>;
}
