export interface pluginWaitlisterType {
  _id: string;
  rank: number;
  waitlisterId: string;
  inviterId?: string;
}
