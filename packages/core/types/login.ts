export type LoginProvider =
  | "google"
  | "naver"
  | "kakao"
  | "local"
  | "refresh"
  | "apple";

export type IsDuplicateLoginState = {
  provider: LoginProvider;
  email: string;
  from?: LoginProvider;
} | null;

export type IsBlockWithDrawLoginState = {
  provider: LoginProvider;
  email: string;
} | null;

export type IsBlockIPLoginState = {
  provider: LoginProvider;
} | null;

export type Login = {
  response: any;
  provider: LoginProvider;
  token?: string;
  callback?: LoginCallback;
};

export type LoginCallback = (arg0: number) => void;

export type GoogleLoginSuccessProps = { credential: string };
export type NaverLoginSuccessProps = {
  loginStatus: {
    accessToken: {
      accessToken: string;
    };
  };
};
export type KakaoLoginSuccessProps = {
  response: {
    access_token: string;
  };
};

/* local login */
export type LocalLoginState = {
  open: boolean;
  section: number;
  email: string;
  password: string;
  provider: LoginProvider;
};

export {};
