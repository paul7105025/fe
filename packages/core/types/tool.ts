import { FormMetadataType } from "./form";

//

export const ToolStatusList = ["deploy", "write", "stop", "delete"] as const;

export type ToolStatus = (typeof ToolStatusList)[number];

//

export const ToolLabelList = ["", "none", "new", "hot"] as const;

export type ToolLabel = (typeof ToolLabelList)[number];

//

export const CurrentToolCategoryList = [
  "blog",
  "marketing",
  "self_employment",
  "work",
  "youtube",
  "normal",
  "student",
  "misc",
] as const;

export const DisabledToolCategoryList = [
  "never",
  "all",
  "new",
  "favorite",
  "none",
  "startup",
  "copywriting",
  "product_introduction",
  "email",
  "creation",
  "customer_service",
  "kakao_talk",
  "bundle",
  "detail_page",
  "sns",
  "writing",
  "new_year",
  "step",
] as const;

export const ToolCategoryList = [
  ...CurrentToolCategoryList,
  ...DisabledToolCategoryList,
] as const;

export type ToolCategory = (typeof ToolCategoryList)[number];

export const ImageCategoryList = [
  "normal",
  "pop-art",
  "water",
  "oil",
  "animation",
] as const;

export type ImageCategory = (typeof ImageCategoryList)[number];

//

export const ToolTabList = ["all", "liked", "image"];

export type ToolTab = (typeof ToolTabList)[number];

//

export const ToolTagList = [
  "none",
  "title",
  "main_text",
  "description",
  "title_title",
  "intro_subject",
  "main_subject",
  "summary",
  "index",
  "general",
  "detail_index",
  "beta",
  "index",
  "conclusion",
];

export type ToolTag = (typeof ToolTagList)[number];

//

export const LinkKindList = ["outputToInput", "inputToInput"];

export type LinkKind = (typeof LinkKindList)[number];

//

export const KindList = ["tool", "step", "bundle"];

export type Kind = (typeof KindList)[number];

//

export interface LinkType {
  from: {
    toolId: string;
    inputId: string;
  };
  to: {
    toolId: string;
    inputId: string;
  };
  kind: LinkKind;
}

export interface OutputFormType {
  from: {
    toolId: string;
    inputId: string;
  };
  kind: LinkKind;
  label: string;
}

export interface StepHistoryType {
  toolId: string | null;
  inputId: string | undefined;
  outputId: string;
  inputData: string;
  outputData: string;
}

export interface CommonToolType {
  category: ToolCategory;

  description: string;
  innerDescription?: string;
  recommend: string;

  icon: string;
  name: string;
  provider: string;
  label: ToolLabel;
  tag: ToolTab;

  tutorial?: {
    link?: string;
    youtubeId?: string;
  };

  status: ToolStatus;
  __v: number;
  _id: string;

  createdAt: string;
  updatedAt: string;

  forms?: Array<FormMetadataType>;
}

export interface SingleToolType extends CommonToolType {
  maxCount: number;
  kind: "tool";
}

export interface StepToolType extends CommonToolType {
  links: Array<LinkType>;
  outputForms: Array<OutputFormType>;
  tools: Array<string>;
  kind: "step";
}

export interface BundleToolType extends CommonToolType {
  tools: Array<{
    toolId: string;
    toolName: string;
  }>;
  kind: "bundle";
}

export type ToolType = SingleToolType | StepToolType | BundleToolType;
