

export declare type ChatbotPromptForEasyType = {
  role?: string;
  personality?: string;
  requirement?: string;
};

export declare type StoreChatBotType = {
  _id: string;
  icon: string;
  profileImage?: string;
  title: string;
  description: string;
  category: string[];
  openType: string;
  priceType: string;
  firstMessage: string;
  selectTypeForExampleQuestion: string;
  exampleQuestion?: string[]
  difficulty: string;
  promptForEasy?: ChatbotPromptForEasyType;
  promptForDifficult?: string;
  isDeleted: boolean;
  isTemporarySave: boolean;
  usedNum: number;
  sharedNum: number;
  likeNum: number;
  viewNum: number;
  userId?: string;
  userName?: string;
  createdAt: string;
  updatedAt: string;

  additionalInformation?: string;
  isFree: boolean;
}