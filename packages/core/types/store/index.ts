export * from "./tool"
export * from "./chatbot";

export declare type B2b = {
  code: string,
  // createdAt: string,
  // expiredAt: string,
  // isDeleted: boolean,
  organizationName: string,
  // updatedAt: string,
  _id: string
}

export declare type B2bMember = {
  b2bId: B2b;
  userId: string;
  createdAt: string;
  isDeleted: boolean;
  updatedAt: string;
  _id: string;
}