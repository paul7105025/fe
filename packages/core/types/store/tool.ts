
export declare type StoreToolType = {
  category: string[];
  createdAt: string;
  description: string;
  forms: any;
  icon: string;
  isDeleted: boolean;
  isFree: boolean;
  isPublished: boolean;
  likes: number;
  prompt: any;
  title: string;
  updatedAt: string;
  usedNum: number;
  userId: string;
  views: number;
  _id: string;
  userName?: string;
}

export declare type StoreToolFormInputType = {
  data: {
    description: string;
    label: string;
    placeholder: string;
    value: string;
  },
  id: string;
  type: "input";
}

export declare type StoreToolFormType = StoreToolFormInputType;

type Input = {
  value: string;
  name: string;
}

export declare type GenerateStoreToolDTO =  {
  inputs: Input[];
}