export type dialogDataType = {
  type: string;
  name: string;
  path: string;
  isOpen: boolean;
  onlyOnce?: boolean; // 계정 당 한 번만 띄우기. "하루만 닫기"가 없어도 돼요.
  onlyLogin?: boolean; // 로그인 유저에게만 띄우기. 
  skipNewbie?: boolean; // 생성 1회를 한 유저만
  skipActiveFirstday?: boolean; // 신규 사용자에게는 closeDay만큼 보여주지 않기
  associatedFunnelCheck?: string; // associatedFunnel이 일치하는 경우에만 띄우기. 
  closeDay?: number; // 며칠동안 닫을건지
};

export type dialogLocalDataType = {
  name: string;
  dueDate: Date;
};
