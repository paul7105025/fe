export const FormStringTypeList = [
  "checkbox",
  "input",
  "textarea",
  "label",
  "option_chips",
  "light_option_chips",
  "select",
  "select_image",
] as const;

export type FormStringType = (typeof FormStringTypeList)[number];

export const FormNumberTypeList = ["counter"] as const;

export type FormNumberType = (typeof FormNumberTypeList)[number];

export const FormArrayTypeList = [
  "input_expandable",
  "input_keyword",
  "input_divided",
] as const;

export type FormArrayType = (typeof FormArrayTypeList)[number];

//

export const FormTypeList = [
  ...FormStringTypeList,
  ...FormNumberTypeList,
  ...FormArrayTypeList,
] as const;

export type FormType = (typeof FormTypeList)[number];

export type FormValue<T extends FormType> = T extends FormStringType
  ? string
  : T extends FormNumberType
  ? number
  : T extends FormArrayType
  ? string[]
  : never;

export type FormValueType = FormValue<FormType>;

//

interface CheckboxOptions {}

interface TextInputOptions {
  placeholder: string;
  description: string;
  maxLength: number;
  isMaxLength: boolean;
}

interface OptionChipsOptions {
  chips: Array<{
    value: string;
    label?: string;
  }>;
}

interface TextAreaOptions extends TextInputOptions {
  rows: number;
}

interface CounterOptions {
  defaultValue: number;
  maxCount: number;
  unit: string;
}

interface InputExpandableOptions {
  maxLength: number;
  placeholder: Array<{
    text: string;
    key: string;
  }>;
}

interface InputKeywordOptions {
  maxLength: number;
  placeholder: string;
  description: string;
  input: Array<string>;
}

interface ImageSelectOptions {
  cards: Array<{
    text: string;
    image: string;
  }>;
}

export type FormOptions<T extends FormType> = T extends "checkbox"
  ? CheckboxOptions
  : T extends "counter"
  ? CounterOptions
  : T extends "input" | "label"
  ? TextInputOptions
  : T extends "textarea"
  ? TextAreaOptions
  : T extends "option_chips" | "light_option_chips"
  ? OptionChipsOptions
  : T extends "input_expandable"
  ? InputExpandableOptions
  : T extends "input_keyword"
  ? InputKeywordOptions
  : T extends "select_image"
  ? ImageSelectOptions
  : never;

export type FormOptionsType = FormOptions<FormType>;

export interface FormMetadata<T extends FormType> {
  id: string;
  _id?: string;
  label: string;
  type: FormType;
  guide: {
    text: string;
    tag: string;
    isVisible: boolean;
  };
  value: FormValue<T>;
  option: FormOptions<T>;
}

export type FormMetadataType = FormMetadata<FormType>;
