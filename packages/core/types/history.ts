import { FormMetadataType } from "./form";
import { Kind, ToolCategory, ToolTag } from "./tool";

export interface HistoryMetaData {
  createdAt: string;
  updatedAt: string;
  disliked: boolean;
  liked: boolean;
  _id: string;
  tool: {
    _id: string;
    category: ToolCategory;
    icon: string;
    tag: ToolTag;
    name: string;
  };
  output: string;
  originOutput: string;
  inputs: Array<string>;
  options: Array<string>;
}

export interface HistoryDetailType {
  createdAt: string;
  updatedAt: string;
  disliked: boolean;
  liked: boolean;
  _id: string;
  tool: {
    _id: string;
    category: ToolCategory;
    icon: string;
    tag: ToolTag;
    name: string;
    kind: Kind;

    forms: Array<FormMetadataType>;
  };
  output: string;
  originOutput: string;
  inputs: Array<string>;
  options: Array<string>;
}
