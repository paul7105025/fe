import { FormValueType } from "../types";

export type SavedValueType = Array<{
  label: string;
  value: FormValueType;
}>;

export type WrtnCountType = Array<{ toolId: string; count: number }>;
