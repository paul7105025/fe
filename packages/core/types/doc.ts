export type saveStatusType = "saved" | "saving" | "offline";

export interface docType {
  _id: string;
  title: string;
  content: string | null;
  userId: string;
  description: string;
  isDeleted: boolean;
  createdAt: string;
  updatedAt: string;
  __v: number;
}

export const categoryList = [
  "선택 안 함",
  "엔터테인먼트・예술",
  "취미・여가・여행",
  "생활・노하우・쇼핑",
  "지식・동향",
] as const;

export type categoryType = typeof categoryList[number];

export const toneList = [
  "일반적인",
  "친근한",
  "전문적인",
  "유머러스한",
] as const;

export type toneType = typeof toneList[number];

export const connectList = [
  "선택 안함",
  "내용 반전",
  "예시",
  "요약ㆍ정리",
  "근거ㆍ이유",
  "추가",
  "추론",
  "가정ㆍ가설",
] as const;

export type connectType = typeof connectList[number];

export const connectItemSelector: Record<connectType, string[]> = {
  "선택 안함": [
    "그러나",
    "하지만",
    "그럼에도 불구하고",
    "그렇지만",
    "다만",
    "그런데",
    "예를 들어",
    "예컨대",
    "예를 들자면",
    "요약하자면",
    "즉",
    "다시말해",
    "그래서",
    "그러므로",
    "그렇기 때문에",
    "그 이유는",
    "왜냐하면",
    "그것은 바로",
    "그리고",
    "덧붙여서",
    "또한",
    "게다가",
    "아마도",
    "어쩌면",
    "만약",
    "만일",
  ],
  "내용 반전": [
    "그러나",
    "하지만",
    "그럼에도 불구하고",
    "그렇지만",
    "다만",
    "그런데",
  ],
  예시: ["예를 들어", "예컨대", "예를 들자면"],
  요약ㆍ정리: [
    "요약하자면",
    "즉",
    "다시말해",
    "그래서",
    "그러므로",
    "그렇기 때문에",
  ],
  근거ㆍ이유: ["그 이유는", "왜냐하면", "그것은 바로"],
  추가: ["그리고", "덧붙여서", "또한", "게다가"],
  추론: ["아마도", "어쩌면"],
  가정ㆍ가설: ["만약", "만일"],
};

export const convertConnectSelector: Record<connectType, string> = {
  "선택 안함": "선택_안함",
  "내용 반전": "내용_반전",
  예시: "예시",
  요약ㆍ정리: "요약_정리",
  근거ㆍ이유: "근거_이유",
  추가: "추가",
  추론: "추론",
  가정ㆍ가설: "가정_가설",
};

export const reverseConnectSelector: Record<
  typeof convertConnectSelector[connectType],
  connectType
> = {
  선택_안함: "선택 안함",
  내용_반전: "내용 반전",
  예시: "예시",
  요약_정리: "요약ㆍ정리",
  근거_이유: "근거ㆍ이유",
  추가: "추가",
  추론: "추론",
  가정_가설: "가정ㆍ가설",
};

export interface docMetaType {
  topic: string;
  category: categoryType;
  connect: connectType;
}
