export type imageUrlType = {
  url: string;
  disliked: boolean;
  liked: boolean;
};

export type messageImageType = {
  imageUrls: Array<imageUrlType>;
  _id: string;
};

export type messageType = {
  content: string;
  createdAt: string;
  disliked: boolean;
  liked: boolean;
  role: "user" | "assistant";
  updatedAt: string;
  _id: string;
  type: string;
  toolId?: string;
  historyId?: string;
  // image
  image?: messageImageType;

  // file
  fileId?: string;
  reference?: number[];
  uuid?: string;

  // dynamic
  dynamicChipList?: Array<string>;

  isAnalyzeMsg?: boolean;

  // plugin
  pluginName?: string;
};

export type chatType = {
  createdAt: string;
  isDeleted: boolean;
  messages: Array<messageType | Array<messageType>>;
  topic: string;
  updatedAt: string;
  version: string;
  historyId?: string;
  __v: number;
  _id: string;
  shared?: number; // 공유된 메시지의 개수
};

export type newMessageType = messageType & { uuid: string };
export type allMessageType = newMessageType | newMessageType[];

export type fileType = {
  _id: string;
  fileUUID?: string;
  createdAt: string;
  updatedAt: string;
  fileName: string;
  fileUrl?: string;
  isDeleted?: boolean;
  size?: number; // 업로드시에만 알수있음
};

// type guard
export function isMessageArray(
  message: allMessageType
): message is newMessageType[] {
  return Array.isArray(message);
}
