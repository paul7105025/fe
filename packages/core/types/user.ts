import { PlanType } from "./billing";

export const providerList = ["local", "google", "kakao", "naver"] as const;

export type ProviderType = (typeof providerList)[number];

export type dialogSurveyType = "select" | "input" | "button";

export type modalSurveyType = {
  title: string;
  subTitle: string;
  survey: Array<{
    id: number;
    type: dialogSurveyType;
    name: string;
    select?: Array<string>;
    placeholder?: string;
  }>;
};

export type userSurveyType = {
  job: Array<string>;
  company?: string;
  inflow: string;
  recommendCode: string;
  purpose: Array<string>;
  termMarketing: boolean;
};

export interface userType {
  name: string;
  email: string;
  password?: string;
  salt?: string;
  number: string;
  provider: ProviderType;
  inflow: string;
  company: string;
  job: Array<string>;
  purpose: Array<string>;
  status: "user" | "admin" | "ban";
  refreshToken: string;
  isNewbie: boolean;
  agreementDate: string | null;
  serviceTerm: string | null;
  privacyTerm: string | null;
  marketingTerm: string | null;
  isActive: boolean;
  isDeleted: boolean;
  deletedAt: string;
  cards: Array<string>;
  usages: Array<any>;

  plan: PlanType;
  nextPlan: PlanType;

  dueDate: string | null;
  paymentDate: null;

  parentUserId: string;
  recommendCode: string;
  childUserIdList: Array<string>;

  createdAt: string;
  updatedAt: string;
  _id: string;

  isRefund?: boolean;
  associatedFunnel: string;
}
