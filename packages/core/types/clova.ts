export const clovaNumberTypeList = [
  "topP",
  "topK",
  "maxTokens",
  "temperature",
  "repeatPenalty",
] as const;

export type ClovaNumberType = typeof clovaNumberTypeList[number];

interface clovaNumberOptionType {
  name: ClovaNumberType;
  label: string;
  number: number;
  min: number;
  max: number;
  step: number;
}

export const clovaBooleanTypeList = [
  "includeAiFilters",
  "includeTokens",
] as const;

export type ClovaBooleanType = typeof clovaBooleanTypeList[number];

export const clovaStringTypeList = [
  "start",
  "restart",
  "probabilities",
] as const;

export type ClovaStringType = typeof clovaStringTypeList[number];

export const clovaStringArrayTypeList = ["stopBefore"] as const;

export type ClovaStringArrayType = typeof clovaStringArrayTypeList[number];

export const clovaTypeList = [
  ...clovaNumberTypeList,
  ...clovaBooleanTypeList,
  ...clovaStringTypeList,
  ...clovaStringArrayTypeList,
] as const;

export type ClovaType = typeof clovaTypeList[number];

export interface clovaOptions {
  topP: number;
  topK: number;
  maxTokens: number;
  temperature: number;
  repeatPenalty: number;
  includeAiFilters: boolean;
  includeTokens: boolean;
  start: string;
  restart: string;
  probabilities: string;
  stopBefore: string[];
}
