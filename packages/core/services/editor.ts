import { getPlatform } from "../utils/userAgent";
import { apiInstance, lambdaInstance } from ".";

export const postDoc = async () => {
  const url = `/doc`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

export const getDocList = async ({ page, limit }) => {
  const url = `/doc?page=${page}&limit=${limit}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

export const getDocCount = async () => {
  const url = `/doc/count`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

export const deleteDoc = async ({ docIdList }) => {
  const url = `/doc/delete`;
  return await apiInstance
    .put(url, {
      docIdList,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

export const postEditorGenerate = async ({ toolId, data, user }) => {
  const platform = getPlatform();
  const url = `/editor?${platform ? `platform=${platform}` : ''}${user ? `&user=${user?.email}` : ''}`
  return await lambdaInstance
    .post(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

export const putDoc_Id = async ({ id, data }) => {
  const url = `/doc/${id}`;

  return await apiInstance
    .put(url, {
      ...data,
    })
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const getDoc_Id = async ({ id }) => {
  const url = `/doc/${id}`;

  return await apiInstance
    .get(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
};
