import { tunibInstance } from ".";

export const postToxicity = async (data) => {
  const url = `/toxicity`;
  return await tunibInstance
    .post(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postToxicityImage = async (data) => {
  const url = `/toxicity/image`;
  return await tunibInstance
    .post(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
