import { apiInstance } from ".";

interface getHistoryProps {
  page?: number;
  category?: string;
  limit?: number;
  liked?: string;
  toolId?: string;
}

export const getHistory = async ({
  page,
  category,
  limit,
  liked,
  toolId,
}: getHistoryProps) => {
  const url = `/history`;
  return await apiInstance
    .get(url, {
      params: {
        page,
        category,
        limit,
        liked,
        toolId,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getHistory_Id = async ({ historyId }) => {
  const url = `/history/${historyId}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const putHistory_Id = async ({ historyId, data }) => {
  const url = `/history/${historyId}`;
  return await apiInstance
    .put(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const deleteHistory_Id = async ({ historyId }) => {
  const url = `/history/${historyId}`;
  return await apiInstance
    .delete(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postHistory_IdReport = async ({ historyId, data }) => {
  const url = `/history/${historyId}/report`;
  return await apiInstance
    .post(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

interface getHistoryCountProps {
  page?: number;
  category?: string;
  limit?: number;
  liked?: string;
  toolId?: string;
}

export const getHistory_Count = async ({
  page = 1,
  category,
  limit = 10,
  liked = "n",
  toolId,
}: getHistoryCountProps) => {
  const url = "/history/count";

  return await apiInstance
    .get(url, {
      params: {
        page,
        category,
        liked,
        limit,
        toolId,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getEvent = async () => {
  const url = `/event`;

  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response || err;
    });
};

export const getHistoryExcel = async () => {
  const url = `/history/excel`;

  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response || err;
    });
};
