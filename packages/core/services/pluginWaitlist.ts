import { apiInstance } from ".";

export const postPluginWaitlist = async ({
  waitlisterId,
  inviterId,
}: {
  waitlisterId?: string;
  inviterId?: string;
}) => {
  const url = `/plugin-waitlist`;
  return await apiInstance
    .post(url, {
      waitlisterId,
      inviterId,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getPluginWaitlist = async ({ waitlisterId }) => {
  const url = `plugin-waitlist/${waitlisterId}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
