import { apiInstance } from ".";

export const postBillingCard = async ({
  name,
  customerIdentityNumber,
  cardNumber,
  cardExpirationYear,
  cardExpirationMonth,
  cardPassword,
}) => {
  const url = `/billing/card`;
  return await apiInstance
    .post(url, {
      name,
      customerIdentityNumber,
      cardNumber,
      cardExpirationMonth,
      cardExpirationYear,
      cardPassword,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postBillingPayment = async ({
  cardId,
  plan,
  couponId,
}: {
  cardId: string;
  plan: string;
  couponId?: string;
}) => {
  const url = `/billing/payment`;

  const data = couponId ? { cardId, plan, couponId } : { cardId, plan };

  return await apiInstance
    .post(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postBillingCancel = async () => {
  const url = `/billing/payment/cancel`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const deleteBillingCard_CardId = async ({ cardId }) => {
  const url = `/billing/card/${cardId}`;
  return await apiInstance
    .delete(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getBillingPayment = async () => {
  const url = `/billing/payment`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const putBillingCard_CardId = async ({ cardId }) => {
  const url = `/billing/card/${cardId}`;
  return await apiInstance
    .put(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
