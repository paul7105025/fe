import { apiInstance } from ".";

interface IWithdrawUser {
  mainReason: string;
  subReason?: string;
  subReasonDetail?: string;
}

export const postWithdrawUser = async ({
  mainReason,
  subReason,
  subReasonDetail,
}: IWithdrawUser) => {
  const url = `/withdraw-reason/user`;
  return await apiInstance
    .post(url, {
      mainReason: mainReason,
      subReason: subReason || "",
      subReasonDetail: subReasonDetail || "",
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

interface IWithdrawSubscription {
  mainReason: string;
  mainReasonDetail?: string;
}

export const postWithdrawSubscription = async ({
  mainReason,
  mainReasonDetail,
}: IWithdrawSubscription) => {
  const url = `/withdraw-reason/subscription`;
  return await apiInstance
    .post(url, {
      mainReason: mainReason,
      mainReasonDetail: mainReasonDetail || "",
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
