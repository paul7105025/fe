import { apiInstance, imageInstance } from ".";

export const checkAccess = async () => {
  const url = `/image-history/check-access`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postImageWait = async () => {
  const url = `/art/wait`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getImage = async ({ type, text, size }) => {
  const url = `/${type}`;

  return await imageInstance
    .post(url, {
      text,
      size,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getImageHistory = async ({ page, limit, style }) => {
  const url = `/image-history`;
  return await apiInstance
    .get(url, {
      params: {
        page,
        limit,
        style,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getImageHistory_Count = async ({
  page = 1,
  limit = 10,
  style,
}) => {
  const url = "/image-history/count";

  return await apiInstance
    .get(url, {
      params: {
        page,
        limit,
        style,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
