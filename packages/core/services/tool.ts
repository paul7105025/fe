import { apiInstance } from ".";

export const getTool = async () => {
  const url = `/tool`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getStep = async () => {
  const url = `/step`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getTool_Id = async ({ toolId }) => {
  const url = `/tool/${toolId}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getStep_Id = async ({ stepId }) => {
  const url = `/step/${stepId}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getToolFavorite = async () => {
  const url = `/tool/favorite`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getToolRecommend = async () => {
  const url = `/recommend`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postTool_IdFavorite = async ({ toolId }) => {
  const url = `/tool/${toolId}/favorite`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const deleteTool_IdFavorite = async ({ toolId }) => {
  const url = `/tool/${toolId}/favorite`;
  return await apiInstance
    .delete(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getToolStar = async ({ toolId }) => {
  const url = `/tool/score?toolId=${toolId}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postToolScore = async ({ toolId, score }) => {
  const url = `/tool/score`;
  return await apiInstance
    .post(url, { toolId, score })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
