import { apiInstance } from ".";

export const postTemplate = async ({ name, content }) => {
  const url = '/template';

  return apiInstance.post(url, {
    name, content
  })
  .then(res => res)
  .catch(err => err?.reponse || err)
}

export const getTemplate = async () => {
  const url = '/template';

  return apiInstance.get(url)
  .then(res => res)
  .catch(err => err?.reponse || err)
}

export const putTemplate_Id = async ({ id, name, content }) => {
  const url = `/template/content/${id}`;

  return apiInstance.put(url, {
    name, content
  })
  .then(res => res)
  .catch(err => err?.reponse || err)
}

export const deleteTemplate_Id = async ({ id }) => {
  const url = `/template/content/${id}`;

  return apiInstance.delete(url)
  .then(res => res)
  .catch(err => err?.reponse || err)
}