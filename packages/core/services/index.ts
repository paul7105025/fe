import axios from "axios";
import { SetterOrUpdater } from "recoil";

import { getCookie } from "../utils";

const server_url =
  process.env.NEXT_PUBLIC_API_SERVER_URL ||
  process.env.REACT_APP_API_SERVER_URL;

const store_server_url =
  process.env.NEXT_PUBLIC_STORE_API_SERVER_URL ||
  process.env.REACT_APP_STORE_API_SERVER_URL;

export const refresh = async (
  error: any,
  setToken: SetterOrUpdater<any>,
  openLoginDialog: () => void,
  loginPath?: string
) => {
  const originalRequest = error.config;
  const refreshToken = getCookie("refresh_token", {});
  if (!loginPath) loginPath = "/";
  try {
    if (error.response?.status === 401 && refreshToken) {
      const res = await axios.post(server_url + "/auth/refresh", null, {
        headers: {
          Refresh: refreshToken,
        },
      });
      if (res?.status === 201) {
        const { accessToken } = res.data.data;
        originalRequest.headers.Authorization = `Bearer ${accessToken}`;
        originalRequest.headers["Content-Type"] = "application/json";

        setToken(accessToken);

        syncHeader({
          key: "Authorization",
          value: `Bearer ${accessToken}`,
        });

        return await axios({
          ...originalRequest,
          headers: originalRequest.headers.toJSON(),
        });
      }
    } else if (error.response?.status === 401) {
      // 만약 모든 실패 응답에 대해서 로그인 창을 띄울 거면 아래 주석 제거
      // openLoginDialog();
    }
    return Promise.reject(error);
  } catch (e) {
    // window.location.href = loginPath + "?redirect=" + window.location.pathname;
  }
};

export const apiInstance = axios.create({
  baseURL: server_url,
});

//서버리스 api
export const lambdaInstance = axios.create({
  baseURL: process.env.REACT_APP_LAMBDA_URL,
});

export const lambdaNoneGenerateInstance = axios.create({
  baseURL:
    process.env.REACT_APP_LAMBDA_NONE_GENERATE_URL ||
    process.env.NEXT_PUBLIC_LAMBDA_NONE_GENERATE_URL,
});

export const tunibInstance = axios.create({
  baseURL: "https://tfilter.wrtn.ai",
});

export const imageInstance = axios.create({
  baseURL: process.env.REACT_APP_IMAGE_GENERATE_URL,
});

export const storeInstance = axios.create({
  baseURL: store_server_url,
});

export const filterInstance = axios.create({
  baseURL: "https://c9hfdl4gqc.execute-api.ap-northeast-2.amazonaws.com",
});
export const syncHeader = ({ key, value }: { key: string; value: string }) => {
  apiInstance.defaults.headers.common[key] = value;
  lambdaInstance.defaults.headers.common[key] = value;
  lambdaNoneGenerateInstance.defaults.headers.common[key] = value;
  tunibInstance.defaults.headers.common[key] = value;
  imageInstance.defaults.headers.common[key] = value;
  storeInstance.defaults.headers.common[key] = value;
  filterInstance.defaults.headers.common[key] = value;
};

export const addInterceptors = (
  setAccessToken: SetterOrUpdater<any>,
  openLoginDialog: () => void,
  openBanAbuseModal: () => void, //403
  openBlockAbuseModal: (response: any) => void, // 503
  openBlockedModal?: (response: any) => void // 500
) => {
  apiInstance.interceptors.response.use(
    (config) => config,
    (err) => {
      if (err?.response.status === 403) {
        openBanAbuseModal();
      }
      if (err?.response.status === 503) {
        openBlockAbuseModal(err?.response);
      }
      if (err?.response.status === 418) {
        
      }
      return refresh(err, setAccessToken, openLoginDialog);
    }
  );
  lambdaInstance.interceptors.response.use(
    (config) => config,
    (err) => {
      if (err?.response.status === 403) {
        openBanAbuseModal();
      }
      if (err?.response.status === 503) {
        openBlockAbuseModal(err?.response);
      }
      if (err?.response.status === 500) {
        openBlockedModal?.(err?.response);
      }
      if (err?.response.status === 418) {

      }
      return refresh(err, setAccessToken, openLoginDialog);
    }
  );
  lambdaNoneGenerateInstance.interceptors.response.use(
    (config) => config,
    (err) => {
      if (err?.response.status === 403) {
        openBanAbuseModal();
      }
      if (err?.response.status === 503) {
        openBlockAbuseModal(err?.response);
      }
      if (err?.response.status === 418) {
        
      }
      return refresh(err, setAccessToken, openLoginDialog);
    }
  );
  tunibInstance.interceptors.response.use(
    (config) => config,
    (err) => {
      if (err?.response.status === 403) {
        openBanAbuseModal();
      }
      if (err?.response.status === 503) {
        openBlockAbuseModal(err?.response);
      }
      if (err?.response.status === 418) {
        
      }
      refresh(err, setAccessToken, openLoginDialog);
    }
  );
  filterInstance.interceptors.response.use(
    (config) => config,
    (err) => refresh(err, setAccessToken, openLoginDialog)
  );
};

export * from "./auth";
export * from "./billing";
export * from "./bundle";
export * from "./editor";
export * from "./history";
export * from "./image";
export * from "./coupon";
export * from "./lambda";
export * from "./metric";
export * from "./tool";
export * from "./tunib";
export * from "./user";
export * from "./withdraw";
export * from "./chat";
export * from "./generate";
export * from "./template";
export * from "./waitlist";
export * from "./file";
export * from "./pluginWaitlist";
export * from "./store";
export * from "./share";
export * from "./filter";
