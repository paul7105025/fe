import { apiInstance } from ".";

export const getBundle = async () => {
  const url = '/bundle';
  return apiInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err);
}

export const getBundle_Id = async ({ bundleId }) => {
  const url = `/bundle/${bundleId}`;
  return apiInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err);
}