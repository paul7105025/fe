import { apiInstance } from ".";

export const getShareChats = async (category?: string, keyword?: string, page?: string) => {
  const url = `/share/search?category=${category ? `${category}` : ""}${keyword ? `&keyword=${keyword}` : ""}${page ? `&page=${page}` : ""}`;
  return await apiInstance.get(url)
    .then(res => res)
    .catch(err => err?.response || err);
}

export const getMyChats = async (page?: string, keyword?: string) => {
  const url = `/share/search/mychats?page=${page ? `${page}` : ""}${keyword ? `&keyword=${keyword}` : ""}`;

  return await apiInstance.get(url);
}

export const getMyLikeChats = async (page?: string, keyword?: string) => {
  const url = `/share/search/mylikes?page=${page ? `${page}` : ""}`;

  return await apiInstance.get(url);
}

export const getShareChat = async (shareId: string) => {
  const url = `/share/${shareId}`;
  
  return apiInstance.get(url);
}

export const putIncreaseView = async (shareChatId: string) => {
  const url = `/share/${shareChatId}/view`;

  return apiInstance.put(url);
}

export const putShareChat = async (shareChatId: string, data: { isDeleted?: boolean; originalId?: string; topic?: string }) => {
  const url = `/share/${shareChatId}`;

  return apiInstance.put(url, data);
}

export const postShareChat = async (chatId: string) => {
  const url = `/share/${chatId}`;

  return apiInstance.post(url)
}

export const getMyLikedChatIds = async () => {
  const url = `/share/like`;

  return apiInstance.get(url);
}

export const postLike = async (chatId: string) => {
  const url = `/share/${chatId}/like`;

  return apiInstance.post(url);
}

export const deleteLike = async (chatId: string) => {
  const url = `/share/${chatId}/like`;

  return apiInstance.delete(url);
}
