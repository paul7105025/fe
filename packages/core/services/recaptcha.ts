import { lambdaNoneGenerateInstance } from ".";

export const postRecaptcha = async (token: string) => {
  const url = "/recaptcha";

  return await lambdaNoneGenerateInstance
    .post(url, {
      recaptcha: token,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
