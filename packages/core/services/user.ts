import { apiInstance } from ".";

export const getUser = async () => {
  const url = `/user`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const putUser = async ({ data }) => {
  const url = `/user`;
  return await apiInstance
    .put(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const deleteUser = async () => {
  const url = `/user`;
  return await apiInstance
    .delete(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const deleteUserNaver = async ({ token }) => {
  const url = `/user?naverAuthorization=${token}`;
  return await apiInstance
    .delete(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getCode = async ({ number }) => {
  const url = `/user/code`;
  return await apiInstance
    .get(url, {
      params: { number },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getCodeVerify = async ({ number, code }) => {
  const url = `/user/code/verify`;
  return await apiInstance
    .get(url, {
      params: { number, code },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getUserWordCount = async () => {
  const url = `/user/word-count`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getUserCard = async () => {
  const url = `/user/card`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postUserRecommend = async ({
  recommendCode,
}: {
  recommendCode: string;
}) => {
  const url = `/user/recommend`;
  return await apiInstance
    .post(url, {
      recommendCode: recommendCode,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const putUserAgreement = async () => {
  const url = `/user/agreement`;
  return await apiInstance
    .put(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postUserSubscribe = async () => {
  const url = `/user/subscribe`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const deleteUserSubscribe = async () => {
  const url = `/user/subscribe`;
  return await apiInstance
    .delete(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postUserRefund = async ({ refund }) => {
  const url = `/user/refund`;
  return await apiInstance
    .post(url, {
      refund: refund,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
