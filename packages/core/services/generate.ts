import { getPlatform } from "../utils/userAgent";
import { lambdaInstance } from ".";
import { userType } from "../types/user";

export type postGenerateChat_Id_Props = {
  chatId: string;
  message: string;
  reroll?: boolean;
  type?: "mini" | "big";
  model?: "GPT3.5" | "GPT4";
  user: userType,
};

export const postGenerateBard_Id = async ({
  chatId,
  message,
  reroll=false,
  type="mini",
  user,
}) => {
  const platform = getPlatform();
  const url = `/chat/${chatId}?model=BARD&${platform ? `platform=${platform}` : ''}${user ? `&user=${user?.email}` : ''}`
  // /generate/chat/646c9e5ffbe0559c00026d4b?type=big&model=BARD&platform=web&user=bob@wrtn.io
  return await lambdaInstance
    .post(url, {
      message,
      reroll
    })
    .then(res => res)
    .catch(err => err?.response || err);
}
export const postGenerateChat_Id = async ({
  chatId,
  message,
  reroll = false,
  type = "mini",
  model = "GPT3.5",
  user,
}: postGenerateChat_Id_Props) => {
  const platform = getPlatform();
  const url = `/plugin/${chatId}?${platform ? `platform=${platform}` : ''}${user ? `&user=${user?.email}` : ''}`
  return await lambdaInstance
    .post(url, {
      message,
      reroll,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postGenerateChip_Id = async({ chatId, message, reroll = false, user }) => {
  const platform = getPlatform();
  const url = `/chip/${chatId}?${platform ? `platform=${platform}` : ''}${user ? `&user=${user?.email}` : ''}`
  return await lambdaInstance
    .post(url, {
      message,
      reroll,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getChipList = async ({ chatId, toolId }) => {
  const url = `/chip/${chatId}/tool/${toolId}`;
  return lambdaInstance
    .get(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const postGenerateChip_Command = async ({
  chatId,
  historyId,
  command,
  user
}) => {
  const platform = getPlatform();
  const url = `/chip/${chatId}/history/${historyId}?${platform ? `platform=${platform}` : ''}${user ? `&user=${user?.email}` : ''}`
  const data = {
    message: command,
    reroll: false,
  };
  return lambdaInstance
    .post(url, data)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const postChatReroll = async ({ chatId, user }) => {
  const platform = getPlatform();
  const url = `/chat/${chatId}/reroll?${platform ? `?platform=${platform}` : ''}${user ? `&user=${user?.email}` : ''}`
  return lambdaInstance
    .post(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const postGenerateImageDownload = async ({
  data,
}: {
  data: {
    imageIds: Array<string>;
  };
}) => {
  const url = `/image/download`
  return lambdaInstance
    .post(url, data, {
      responseType: "blob",
    })
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const postGenerateUpscaledImageDownload = async ({
  targetUrl,
  imageId,
}: {
  targetUrl: string;
  imageId: string;
}) => {
  const url = '/image/upscale';
  return lambdaInstance
    .post(url, {
      targetUrl,
      imageId,
    })
    .then(res => res)
    .catch(err => err?.response || err);
}