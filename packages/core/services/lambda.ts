import { getPlatform } from "../utils/userAgent";
import { lambdaInstance } from ".";
import { userType } from "../types/user";

// export const sleeper = (ms) => {
//   return function (x) {
//     return new Promise((resolve) => setTimeout(() => resolve(x), ms));
//   };
// };

type postGenerateProps = {
  toolId: string;
  data: any;
  type?: string;
  id?: string;
  user?: userType | null;
};
export const postGenerate = async ({
  toolId,
  data,
  type,
  id,
  user,
}: postGenerateProps) => {
  const platform = getPlatform();
  const query =
    (type ? `type=${type}&` : "") +
    (id ? `id=${id}&` : "") +
    (platform ? `platform=${platform}&` : "") +
    (user ? `user=${user.email}&` : "");
  const url = `/tool/${toolId}` + (query !== "" ? `?${query}` : "");

  const { input, option, ...others } = data;
  return await lambdaInstance
    .post(url, {
      ...others,
      inputs: input,
      options: option,
    })
    .then((res) => {
      console.log(res);
      return res;
    })
    .catch((err) => {
      return err;
    });
};

export const postStaticBookTitle = async ({ data }) => {
  const url = "/static/book/title";
  return await lambdaInstance
    .post(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

export const postStaticBlogTitle = async ({ data }) => {
  const url = "/static/blog/title";
  return await lambdaInstance
    .post(url, data)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};
