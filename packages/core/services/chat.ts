import { apiInstance, lambdaInstance } from ".";

export const getChat = async () => {
  const url = "/chat";
  return apiInstance
    .get(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const getChat_Id = async ({ chatId }) => {
  const url = `/chat/${chatId}`;
  return apiInstance
    .get(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const postChat = async () => {
  const url = `/chat`;
  return apiInstance
    .post(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const deleteChat_Id = async ({ chatId }) => {
  const url = `/chat/${chatId}`;
  return apiInstance
    .delete(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

export const putChat_Id = async ({ chatId, body }) => {
  const url = `/chat/${chatId}`;
  return apiInstance
    .put(url, body)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

type putChatMessage_Id_Props = {
  chatId: string;
  messageId: string;
  data: {
    liked?: boolean;
    disliked?: boolean;
  };
};

export const putChatMessage_Id = async ({
  chatId,
  messageId,
  data,
}: putChatMessage_Id_Props) => {
  const url = `/message/${messageId}`;
  return apiInstance
    .put(url, data)
    .then((res) => res)
    .catch((err) => err?.response || err);
};

interface putChatImage_Id_Props {
  imageId: string;
  data: {
    index: number;
    liked: boolean;
    disliked: boolean;
  };
}

export const putChatImage_Id = async ({
  imageId,
  data,
}: putChatImage_Id_Props) => {
  const url = `/image/${imageId}/url`;
  return apiInstance
    .put(url, data)
    .then((res) => res)
    .catch((err) => err?.response || err);
};
