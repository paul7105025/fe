import { filterInstance } from "..";

const postToxicity = async (text: string[]) => {
  const url = '/toxicity';
  return await filterInstance
  .post(url,
    {
      text: text,
      plan: "FREE"
    }
    )
  .then(res => res)
  .catch(err => err?.response || err);
}

export const filterAPI = {
  postToxicity: postToxicity
}

export default filterAPI;