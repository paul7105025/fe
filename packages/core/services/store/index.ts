import { getChatbot_Id } from "./chatbot";
import { postGenerateTool, postGenerateTool_Id } from "./generate";
import { deleteStoreFavorite_Id, getTool, getToolFavorite, getToolFavoriteOnly, getToolRecommend, getToolRelated_Id, getToolTrend, getToolUser_Id, getTool_Id, postStoreFavorite_Id } from "./tool";
import { postValidatorImportant } from "./validator";

export const storeAPI = {
  getTool: getTool,
  getToolTrend: getToolTrend,
  getTool_Id: getTool_Id,
  getToolFavorite: getToolFavorite,
  getToolFavoriteOnly: getToolFavoriteOnly,
  getToolUser_Id: getToolUser_Id,
  getToolRelated_Id: getToolRelated_Id,
  getChatbot_Id: getChatbot_Id,
  postGenerateTool_Id: postGenerateTool_Id,
  postGenerateTool: postGenerateTool,
  getToolRecommend: getToolRecommend,
  postValidatorImportant: postValidatorImportant,
  postStoreFavorite_Id: postStoreFavorite_Id,
  deleteStoreFavorite_Id: deleteStoreFavorite_Id, 
}

export default storeAPI;
