import { storeInstance } from "..";

const store_server_url =
  process.env.NEXT_PUBLIC_STORE_API_SERVER_URL ||
  process.env.REACT_APP_STORE_API_SERVER_URL;

export const getChatbot_Id = async (id: string) => {
  const url = `/store/chat-bot/${id}`;
  return await storeInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err)
}