import { storeInstance } from "..";

export const postValidatorImportant = async (text: string[]) => {
  const url = `/validator/important`;
  return await storeInstance
    .post(url, {
      inputs: text
    })
    .then(res => res)
    .catch(err => err?.response || err);
}