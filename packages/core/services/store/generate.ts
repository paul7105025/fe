import { GenerateStoreToolDTO } from "../../types/store";
import { storeInstance } from ".."


export const postGenerateTool_Id = async (toolId: string, inputs: GenerateStoreToolDTO) => {
  const url = `/generate/tool/${toolId}`
  return await storeInstance 
    .post(url, {
      inputs: inputs.inputs,
    })
    .then(res => res)
    .catch(err => err?.response || err);
}

export const postGenerateTool = async ({ prompt, model, inputs, max_tokens }) => {
  const url = '/generate/test';
  return await storeInstance
    .post(url, {
      prompt,
      model,
      inputs,
      max_tokens,
    })
    .then(res => res)
    .catch(err => err?.response || err);
}