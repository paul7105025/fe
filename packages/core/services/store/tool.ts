import { storeInstance } from "..";

type Pagination = {
  page: number;
  limit: number;
}

type getTool = {
  pagination: Pagination
  title?: string;
  category?: string;
}

export const getTool = async ({ pagination, title, category }: getTool) => {
  const url = '/tool?'
    + `${pagination?.page ? 'page=' + pagination.page + '&' : ''}`
    + `${pagination?.limit ? 'limit=' + pagination.limit + '&' : ''}`
    + `${title ? 'title=' + title + '&' : ''}`
    + `${category ? 'category=' + category + '&' : ''}`

  return await storeInstance.get(url)
  .then((res) => res)
  .catch(err => err?.response || err);
}

export const getToolTrend = async ({ pagination, title, category }: getTool) => {
  const url = '/tool/trend?'
  + `${pagination?.page ? 'page=' + pagination.page + '&' : ''}`
  + `${pagination?.limit ? 'limit=' + pagination.limit + '&' : ''}`
  + `${title ? 'title=' + title + '&' : ''}`
  + `${category ? 'category=' + category + '&' : ''}`

  return await storeInstance.get(url)
  .then((res) => res)
  .catch(err => err?.response || err);
}

export const getToolRecommend = async () => {
  const url = '/tool/recommend'
  return await storeInstance.get(url).
    then(res => res)
    .catch(err => err?.response || err);
}

export const getTool_Id = async (id: string) => {
  const url = `/tool/${id}`;
  return await storeInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err)
}

export const getToolUser_Id = async (userId: string) => {
  const url = `/tool/author/${userId}`;
  return await storeInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err)
}

export const getToolFavoriteOnly = async ({ pagination, title, category }: getTool) => {
  const url = `/tool/favorite/only?`
  + `${pagination?.page ? 'page=' + pagination.page + '&' : ''}`
  + `${pagination?.limit ? 'limit=' + pagination.limit + '&' : ''}`
  + `${title ? 'title=' + title + '&' : ''}`
  + `${category ? 'category=' + category + '&' : ''}`
  return await storeInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err);
}

export const getToolFavorite = async () => {
  const url = `/tool/favorite`
  return await storeInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err);
}

export const postStoreFavorite_Id = async ({ toolId, chatbotId }: { toolId?: string, chatbotId?: string }) => {
  const url = `/store/favorite`;
  return await storeInstance.post(url, {
    toolId: toolId,
    chatBotId: chatbotId
  })
  .then(res => res)
  .catch(err => err?.response || err);
}

export const deleteStoreFavorite_Id = async ({ toolId, chatbotId }: { toolId?: string, chatbotId?: string }) => {
  const url = `/store/favorite`;
  return await storeInstance.delete(url, {
    data: {
      toolId: toolId,
      chatBotId: chatbotId
    }
  })
  .then(res => res)
  .catch(err => err?.response || err);
}

export const getToolRelated_Id = async (toolId: string) => {
  const url  = `/tool/related/${toolId}`;
  return await storeInstance.get(url)
  .then(res => res)
  .catch(err => err?.response || err);
}