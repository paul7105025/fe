import { redirect } from "react-router-dom";
import { apiInstance } from ".";

interface authProps {
  token: string | undefined;
}

interface refreshProps {
  refreshToken: string;
}

interface userAuthProps {
  email?: string;
  password?: string;
  phone?: string;
  code?: string;
  provider?: string | null;
}


export type authDTO = {
  token: string;
}

export type postAuthSocialDTO = {
  auth: authDTO;
  deviceId?: string;
  funnel?: string;
  platform?: string | null;
}

export const postAuthGoogle = async (props: postAuthSocialDTO) => {
  const { auth, deviceId, funnel, platform } = props;
  const { token } = auth;
  const url = `/auth/google?${deviceId ? `deviceId=${deviceId}&` : ''}${funnel ? `funnel=${funnel}` : ''}${platform ? `platform=${platform}` : ''}`
  return await apiInstance
    .post(url, {
      authorizationToken: token,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthNaver = async (props: postAuthSocialDTO) => {
  const { auth, deviceId, funnel, platform } = props;
  const { token } = auth;
  const url = `/auth/naver?${deviceId ? `deviceId=${deviceId}&` : ''}${funnel ? `funnel=${funnel}` : ''}${platform ? `platform=${platform}` : ''}`;
  return await apiInstance
    .post(url, {
      authorizationToken: token,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthKakao = async (props: postAuthSocialDTO) => {
  const { auth, deviceId, funnel, platform } = props;
  const { token } = auth;
  const url = `/auth/kakao?${deviceId ? `deviceId=${deviceId}&` : ''}${funnel ? `funnel=${funnel}` : ''}${platform ? `platform=${platform}` : ''}`;
  return await apiInstance
    .post(url, {
      authorizationToken: token,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthApple = async (props) => {
  const { auth, deviceId, funnel, platform } = props;
  const { token } = auth;

  const url = `/auth/apple?${deviceId ? `deviceId=${deviceId}&` : ''}${funnel ? `funnel=${funnel}` : ''}${platform ? `platform=${platform}` : ''}`;
  return await apiInstance
    .post(url, {
      authorizationToken: token,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
}


export const postAuthRefresh = async ({ refreshToken }: refreshProps) => {
  const url = `/auth/refresh`;
  return await apiInstance
    .post(url, null, {
      headers: {
        refresh: refreshToken,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthLocalLogin = async ({
  email,
  password,
}: userAuthProps) => {
  const url = "/auth/local";
  const data = {
    email: email,
    password: password
  }

  return await apiInstance
    .post(url, {
      email: email,
      password: password,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthLocalRegister = async ({
  email,
  password,
}: userAuthProps, 
deviceId?: string | undefined, 
funnel?: string | undefined,
platform?: string | null) => {
  const url = `/auth/register?${deviceId ? `deviceId=${deviceId}&` : ''}${funnel ? `funnel=${funnel}` : ''}${platform ? `platform=${platform}` : ''}`
  return await apiInstance
    .post(url, {
      email: email,
      password: password,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthCode = async ({ email }: userAuthProps) => {
  const url = `/auth/code?email=${email}`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getAuthCode = async ({ email, code }: userAuthProps) => {
  const url = `/auth/code?email=${email}&code=${code}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getAuthCheck = async ({ email }: userAuthProps) => {
  const url = `/auth/check?email=${email}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthCodePassword = async ({ email }: userAuthProps) => {
  const url = `/auth/code/password?email=${email}`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getAuthCodePassword = async ({ email, code }: userAuthProps) => {
  const url = `/auth/code/password?email=${email}&code=${code}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postAuthCodeEmail = async ({ phone }: userAuthProps) => {
  const url = `/auth/code/email?number=${phone}`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getAuthCodeEmail = async ({ phone, code }: userAuthProps) => {
  const url = `/auth/code/email?number=${phone}&code=${code}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const putAuthLocalPassword = async ({
  email,
  password,
}: userAuthProps) => {
  const url = `/auth/local/password`;
  return await apiInstance
    .put(url, {
      email: email,
      password: password,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
