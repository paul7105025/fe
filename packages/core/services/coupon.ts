import { apiInstance } from ".";

export const getCouponList = async () => {
  const url = "/coupon";
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const getCouponCode = async ({ code = "" }) => {
  const url = "/coupon/code/" + `${code}`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const postCouponCode = async ({ id="", code = "" }) => {
  const url = "/coupon/" + `${id}${code.length > 0 ? "?code=" + code : ''}`;
  return await apiInstance
    .post(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};

export const putCouponCode = async ({ couponId, force=false }) => {
  const url = "/coupon/" + `${couponId}${force ? "?force=true":"?force=false"}`;
  return await apiInstance
    .put(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
