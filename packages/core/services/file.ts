import axios from "axios";
import { lambdaInstance } from ".";
import { fileType } from "../types/chat";

type RequestFileOptions = {
  chatId: string;
  fileUUID: string;
  fileName: string;
  fileExtension: "pdf";
}

// s3 업로드 url 요청
export const getPreSignedURL = async () => {
  const url = `/file/upload?extension=pdf`;
  return await lambdaInstance.get(url);
}

// s3 업로드
export const uploadS3 = async (fileUploadUrl: string, file: File) => {
  return await axios.put(fileUploadUrl, file, {
    headers: {
      "Content-Type": "application/pdf",
    },
  });
}

// 파일 목록 조회
export const getFileList = async () => {
  const url = "/file";
  return await lambdaInstance.get(url);
}

// 파일 분석 요청
export const postFile = async (file: RequestFileOptions) => {
  const url = "/file";
  return await lambdaInstance.post(url, file);
}

// 파일 분석 상황 조회
export const getProgress = async (fileId: string, chatId: string) => {
  const url = `/file/${fileId}/chat/${chatId}`;
  return await lambdaInstance.get(url);
}

// 파일 분석 상황 조회
export const executePollingRequest = (
  { file, chatId, loadingId }: { file: fileType; chatId: string; loadingId: string },
  progressCallback: (progress: number) => void,
  successCallback: (laodingObj: any,result: { intro: string; questions: string[] } | null, err?: any) => void,
) => {
  const intervalId = setInterval(requestOnce, 5000);
  
  return intervalId;

  async function requestOnce() {
    try {
      const res = await getProgress(file._id, chatId);
      
      const { progress, intro, qa_pairs } = res.data?.data as { progress: number; intro?: string; qa_pairs?: any };

      if (progress < 100) {
        progressCallback(progress);
      }

      if (progress >= 100 && intro && qa_pairs) {
        progressCallback(progress);
        clearInterval(intervalId);
        await wait(1000);

        return successCallback({
          loadingId, file,
        }, {
          intro,
          questions: qa_pairs.map(qa => qa.question),
        });
      }
    } catch (err) {
      clearInterval(intervalId);
      return successCallback({ loadingId, file }, null, err);
    }
  }
}

// 파일 삭제
export const deleteFile = async (fileId: string) => {
  const url = `/file/${fileId}`;
  return await lambdaInstance.delete(url)
    .then((res) => res)
    .catch((err) => err?.response || err);
}

// 파일을 토대로 채팅 생성
// 여기 스트림..
export const postGenerateFileChat = async (fileId: string, data: { message: string, reroll: boolean, chatId: string }) => {
  const { message, chatId, reroll } = data;
  // body: message, reroll model, platform
  const url = `/file/${fileId}/chat/${chatId}`;
  return await lambdaInstance
    .post(url, {
      message,
      reroll,
      platform: "web",
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
}

// 마지막 프로그래스를 위해 약간의 딜레이를 일부러 줌
const wait = (time: number) => new Promise<void>((resolve, reject) => {
  setTimeout(() => {
    resolve();
  }, time)
})
