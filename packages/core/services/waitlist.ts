import { apiInstance } from ".";

export const postWaitList = async ({
  name,
  email,
  opinion,
  marketingTerm,
  kind,
}: {
  name: string;
  email: string;
  opinion: string;
  marketingTerm: boolean;
  kind: string;
}) => {
  const url = `/wait-list`;
  return await apiInstance
    .post(url, {
      name,
      email,
      opinion,
      marketingTerm,
      kind,
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
