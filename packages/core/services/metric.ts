import { apiInstance } from ".";

export const getMetric = async () => {
  const url = `/metric`;
  return await apiInstance
    .get(url)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err?.response || err;
    });
};
