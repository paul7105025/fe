export const FORM_TYPE_CHECKBOX = "checkbox";
export const FORM_TYPE_COUNTER = "counter";
export const FORM_TYPE_TEXTAREA = "textarea";
export const FORM_TYPE_INPUT = "input";
export const FORM_TYPE_LABEL = "label";
export const FORM_TYPE_OPTION_CHIPS = "option_chips";
export const FORM_TYPE_LIGHT_OPTION_CHIPS = "light_option_chips";
export const FORM_TYPE_INPUT_EXPANDABLE = "input_expandable";
export const FORM_TYPE_INPUT_KEYWORD = "input_keyword";
export const FORM_TYPE_SELECT = "select";
export const FORM_TYPE_SELECT_IMAGE = "select_image";
export const FORM_TYPE_INPUT_DIVIDED = "input_divided";

export const OTI = "outputToInput";
export const ITI = "inputToInput";

export const DEFAULT_STRUCT = {
  id: "",
  label: "",
  type: "",
  value: [""],
  guide: {
    isVisible: true,
    text: "",
    tag: "",
  },
  option: {},
};

export const DEFAULT_CHECKBOX_OPTION = {};

export const DEFAULT_TEXT_INPUT_OPTION = {
  isMaxLength: false,
  maxLength: 100,
  placeholder: "placeholder",
  description: "description",
};

export const DEFAULT_OPTIONS_CHIPS_OPTION = {
  chips: [],
};

export const DEFAULT_TEXTAREA_OPTION = {
  isMaxLength: false,
  maxLength: 300,
  placeholder: "placeholder",

  rows: 1,
};

export const DEFAULT_COUNTER_OPTION = {
  defaultValue: 1,
  maxCount: 5,
  unit: "",
};

export const DEFAULT_INPUT_EXPANDABLE_OPTION = {
  maxLength: 100,
  placeholder: [
    {
      text: "",
      key: "01256065423234",
    },
  ],
};

export const DEFAULT_INPUT_KEYWORD_OPTION = {
  placeholder: "",
  description: "",
  input: [],
};

export const DEFAULT_SELECT_OPTION = {
  selects: [],
};

export const optionSelector = {
  option_chips: DEFAULT_OPTIONS_CHIPS_OPTION,
  light_option_chips: DEFAULT_OPTIONS_CHIPS_OPTION,
  input: DEFAULT_TEXT_INPUT_OPTION,
  counter: DEFAULT_COUNTER_OPTION,
  checkbox: DEFAULT_CHECKBOX_OPTION,
  textarea: DEFAULT_TEXTAREA_OPTION,
  input_expandable: DEFAULT_INPUT_EXPANDABLE_OPTION,
  input_keyword: DEFAULT_INPUT_KEYWORD_OPTION,
  select: DEFAULT_SELECT_OPTION,
};

export const FORMS = [
  // FORM_TYPE_CHECKBOX,
  FORM_TYPE_COUNTER,
  FORM_TYPE_INPUT,
  FORM_TYPE_TEXTAREA,
  FORM_TYPE_OPTION_CHIPS,
  FORM_TYPE_LIGHT_OPTION_CHIPS,
  // FORM_TYPE_INPUT_EXPANDABLE,
  FORM_TYPE_INPUT_KEYWORD,
];

export const FORM_TYPE_NAME = {
  [FORM_TYPE_CHECKBOX]: "체크박스",
  [FORM_TYPE_COUNTER]: "카운터",
  [FORM_TYPE_INPUT]: "한 줄 입력",
  [FORM_TYPE_INPUT_KEYWORD]: "키워드 입력",
  [FORM_TYPE_TEXTAREA]: "여러 줄 입력",
  [FORM_TYPE_INPUT_EXPANDABLE]: "다중 줄 입력",
  [FORM_TYPE_OPTION_CHIPS]: "라디오 옵션 선택",
  [FORM_TYPE_LIGHT_OPTION_CHIPS]: "라디오 옵션 선택(경량)",
  [FORM_TYPE_SELECT]: "드롭다운",
  [FORM_TYPE_SELECT_IMAGE]: "이미지 선택",
  [FORM_TYPE_INPUT_DIVIDED]: "분할 입력",
};

export const DEFAULT_FORM_VALUE = {
  [FORM_TYPE_CHECKBOX]: "",
  [FORM_TYPE_COUNTER]: 1,
  [FORM_TYPE_INPUT]: "",
  [FORM_TYPE_INPUT_KEYWORD]: [],
  [FORM_TYPE_TEXTAREA]: "",
  [FORM_TYPE_INPUT_EXPANDABLE]: [""],
  [FORM_TYPE_OPTION_CHIPS]: "",
  [FORM_TYPE_LIGHT_OPTION_CHIPS]: "",
  [FORM_TYPE_SELECT]: "",
  [FORM_TYPE_SELECT_IMAGE]: "",
  [FORM_TYPE_INPUT_DIVIDED]: [],
};
