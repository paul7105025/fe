export const TOP_P_OPTION = {
  name: "topP",
  label: "Top P",
  number: "0.8",
  min: "0",
  max: "1",
  step: "0.05"
};

export const TOP_K_OPTION = {
  name: "topK",
  label: "Top K",
  number: "0",
  min: "0",
  max: "128",
  step: "1"
};

export const TOKENS_OPTION = {
  name: "tokens",
  label: "Maximum tokens",
  number: "32",
  min: "0",
  max: "2048",
  step: "1"
};

export const TEMPERATURE_OPTION = {
  name: "temperature",
  label: "Temperature",
  number: "0.5",
  min: "0",
  max: "1",
  step: "0.05"
};

export const PENALTY_OPTION = {
  name: "penalty",
  label: "Repetition penalty",
  number: "5",
  min: "0",
  max: "10",
  step: "1"
};

export const rangeList = [
  TOP_P_OPTION,
  TOP_K_OPTION,
  TOKENS_OPTION,
  TEMPERATURE_OPTION,
  PENALTY_OPTION,
];

export const rangeSelector = {
  [TOP_P_OPTION.name]: TOP_P_OPTION,
  [TOP_K_OPTION.name]: TOP_K_OPTION,
  [TOKENS_OPTION.name]: TOKENS_OPTION,
  [TEMPERATURE_OPTION.name]: TEMPERATURE_OPTION,
  [PENALTY_OPTION.name]: PENALTY_OPTION,
};

export const SEQUENCE_OPTION = {
  name: "stopSequences",
  label: "Stop sequences",
  value: "",
};

export const START_TEXT_OPTION = {
  name: "startText",
  label: "Inject start text",
  value: "",
};

export const RESTART_TEXT_OPTION = {
  name: "restartText",
  label: "Inject restart text",
  value: "",
};

export const PROBABILITIES_OPTION = {
  name: "probabilities",
  label: "Show probabilities",
};

export const inputList = [
  SEQUENCE_OPTION,
  START_TEXT_OPTION,
  RESTART_TEXT_OPTION,
  PROBABILITIES_OPTION,
];

export const inputSelector = {
  [SEQUENCE_OPTION.name]: SEQUENCE_OPTION,
  [START_TEXT_OPTION.name]: START_TEXT_OPTION,
  [RESTART_TEXT_OPTION.name]: RESTART_TEXT_OPTION,
  [PROBABILITIES_OPTION.name]: PROBABILITIES_OPTION,
};
