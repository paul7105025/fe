import React from "react";
import styled from "styled-components";

import { colors, FlexWrapper, typo } from "../../styles";

export const LengthCounterOption = ({
  option,
  toggleIsMaxLength,
  setMaxLength,
  maxLength,
}) => {
  const [isMaxLength, setIsMaxLength] = React.useState(option || false);

  const handleIsMaxLength = (e) => {
    setIsMaxLength(e.target.checked);
    toggleIsMaxLength(e.target.checked);
  };

  return (
    <Container>
      <Checkbox
        type="checkbox"
        onChange={handleIsMaxLength}
        checked={isMaxLength}
      />
      {isMaxLength && "최대 길이 :"}
      {isMaxLength && <TextInput value={maxLength} onChange={setMaxLength} />}
    </Container>
  );
};

const Container = styled(FlexWrapper)`
  gap: 10px;

  ${typo({
    size: "12px",
    weight: "600",
    color: colors.GRAY_90,
  })}
`;

const Checkbox = styled.input``;

const TextInput = styled.input``;