import styled from "styled-components";

import { colors, typo } from "../../styles";

export const LengthCounter = ({ currentLength, maxLength }) => {
  return (
    <CounterWrapper>
      <Counter>
        {currentLength}/{maxLength}
      </Counter>
    </CounterWrapper>
  );
};

const CounterWrapper = styled.div`
  position: absolute;
  bottom: -20px;
  right: 0px;
`;

const Counter = styled.p`
  ${typo({
    size: "12px",
    weight: "600",
    color: colors.GRAY_58,
  })}
`;
