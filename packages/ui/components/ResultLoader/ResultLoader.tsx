import styled from "styled-components";

import { Spinner } from "../Spinner";

import { FlexWrapper } from "../../styles";
import React from "react";

const useTooLong = () => {
  const [tooLongLoading, setTooLongLoading] = React.useState(0);

  React.useEffect(() => {
    const timer = setTimeout(() => {
      setTooLongLoading(1);
    }, 5000);
    const timer2 = setTimeout(() => {
      setTooLongLoading(2);
    }, 10000);
    return () => {
      setTooLongLoading(0);
      clearTimeout(timer);
      clearTimeout(timer2);
    };
  }, []);

  return { tooLongLoading };
};

export const ResultLoader = () => {
  const { tooLongLoading } = useTooLong();

  return (
    <Wrapper>
      <Spinner width={53} height={53} />
      {tooLongLoading === 0 && (
        <LoaderDesc>AI가 여러가지 표현을 고민하고 있어요.</LoaderDesc>
      )}
      {tooLongLoading === 1 && <LoaderDesc>조금만 더 기다려주세요!</LoaderDesc>}
      {tooLongLoading === 2 && (
        <LoaderDesc>
          조금만 더 기다려 주세요! 긴 글은 생성에 1~2분 정도 걸릴 수 있어요.
        </LoaderDesc>
      )}
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  flex-direction: column;
  gap: 13px;
  width: 100%;
  height: 100%;
  justify-content: center;
`;

const LoaderDesc = styled.p``;
