import styled from "styled-components";
import { FlexButton } from "../../styles";

interface IconProps {
  icon: string;
  size?: number;
  color?: string;
  hoverColor?: string;
}

export const Icon = ({
  icon,
  size = 36,
  color = "#717488",
  hoverColor = color,
}: IconProps) => {
  return (
    <IconWrapper
      size={size}
      color={color}
      hoverColor={hoverColor}
      className={`uil uil-${icon}`}
    />
  );
};

interface IconButtonProps extends IconProps {
  onClick?: () => void;
}

export const IconButton = (props: IconButtonProps) => {
  return (
    <FlexButton onClick={props.onClick}>
      <Icon {...props} />
    </FlexButton>
  );
};

const IconWrapper = styled.i<{
  size: number;
  color: string;
  hoverColor: string;
}>`
  font-size: ${({ size }) => size}px;
  color: ${({ color }) => color};

  &:hover {
    color: ${({ hoverColor }) => hoverColor};
  }
`;
