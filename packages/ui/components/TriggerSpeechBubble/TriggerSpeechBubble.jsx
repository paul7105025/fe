import React from "react";
import styled from "styled-components";

import { Close } from "../../assets";
import { boxShadow, colors, typo } from "../../styles";

const TriggerSpeechBubble = ({ handleClose, toolId }) => {
  const [isHover, setIsHover] = React.useState(false);

  const currentToolId = React.useRef();

  React.useEffect(() => {
    currentToolId.current = toolId;
  }, []);

  React.useEffect(() => {
    if (currentToolId.current !== toolId) {
      handleClose();
    }
  }, [toolId]);

  return (
    <Wrapper
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
    >
      <InsideWrapper>
        <Title>결과가 마음에 드시나요?</Title>
        <Description>
          더 센스있는 표현을 원한다면 자동생성 버튼을 다시 눌러보세요!
        </Description>
        {isHover && (
          <IconWrapper onClick={handleClose}>
            <Close />
          </IconWrapper>
        )}
      </InsideWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: absolute;
  width: 230px;
  height: 90px;
  background: ${colors.WHITE};
  ${boxShadow.tooltip_message_shadow};
  border-radius: 10px;
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  z-index: 5;
  left: -220px;
  top: 96px;
`;

const InsideWrapper = styled.div`
  padding: 13px 19px 11px 25px;
  position: relative;
`;

const Title = styled.p`
  ${typo({
    weight: 700,
    size: "16px",
    height: "22px",
    color: colors.ACTION_BLUE,
  })};
`;

const Description = styled.p`
  ${typo({ weight: 600, size: "14px", height: "20px", color: colors.gray_60 })};
`;

const IconWrapper = styled.div`
  position: absolute;
  right: 10px;
  top: 10px;
`;

export default TriggerSpeechBubble;
