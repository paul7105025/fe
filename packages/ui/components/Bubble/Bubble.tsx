import React from "react";
import styled, {
  css,
  DefaultTheme,
  ThemedCssFunction,
} from "styled-components";

import { boxShadow, colors, FlexWrapper } from "../../styles";

type directionType = "left" | "right" | "top" | "bottom" | "none";

interface BubbleProps {
  direction?: directionType;
  position?: number;
  css?: ThemedCssFunction<DefaultTheme>;
  style?: React.CSSProperties;
  innerColor?: string;
  borderColor?: string;
}

export const Bubble: React.FC<React.PropsWithChildren<BubbleProps>> = ({
  children,
  direction = "left",
  position = 0,
  css,
  style,
  innerColor = colors.WHITE,
  borderColor = colors.WHITE_BOLDER_LINE,
}) => {
  return (
    <Wrapper
      direction={direction}
      position={position}
      css={css}
      style={style}
      innerColor={innerColor}
      borderColor={borderColor}
    >
      {children}
    </Wrapper>
  );
};

const Common = css`
  &::after {
    position: absolute;
    content: "";
    border-width: 8px;
    border-style: solid;
  }

  &::before {
    position: absolute;
    content: "";
    border-width: 8px;
    border-radius: 1px;
    border-style: solid;
  }
`;

const Left = (position, innerColor, borderColor) => css`
  ${Common}
  &::after {
    top: ${position}px;
    left: -15px;
    border-color: transparent ${innerColor} transparent transparent;
  }

  &::before {
    top: ${position}px;
    left: -16px;
    border-color: transparent ${borderColor} transparent
      transparent;
  }
`;

const Right = (position, innerColor, borderColor) => css`
  ${Common}
  &::after {
    top: ${position}px;
    right: -15px;
    border-color: transparent transparent transparent ${innerColor};
  }

  &::before {
    top: ${position}px;
    right: -16px;
    border-color: transparent transparent transparent
      ${borderColor};
  }
`;

const Bottom = (position, innerColor, borderColor) => css`
  ${Common}
  &::after {
    left: ${position}px;
    bottom: -15px;
    border-color: ${innerColor} transparent transparent transparent;
  }

  &::before {
    left: ${position}px;
    bottom: -16px;
    border-color: ${borderColor} transparent transparent
      transparent;
  }
`;

const Top = (position, innerColor, borderColor) => css`
  ${Common}
  &::after {
    left: ${position}px;
    top: -15px;
    border-color: transparent transparent ${innerColor} transparent;
  }

  &::before {
    left: ${position}px;
    top: -16px;
    border-color: transparent transparent ${borderColor}
      transparent;
  }
`;

const directionSelector = (direction, position, innerColor, borderColor) => {
  switch (direction) {
    case "left":
      return Left(position, innerColor, borderColor);
    case "right":
      return Right(position, innerColor, borderColor);
    case "top":
      return Top(position, innerColor, borderColor);
    case "bottom":
      return Bottom(position, innerColor, borderColor);
    case "none":
      return css``;
    default:
      return css``;
  }
};

const Wrapper = styled(FlexWrapper)<{
  direction: directionType;
  position: number;
  css?: ThemedCssFunction<DefaultTheme>;
  innerColor?: string;
  borderColor?: string;
}>`
  border-radius: 20px;
  padding: 22px 21px;
  background: ${({ innerColor }) => innerColor};
  ${boxShadow.guideline_shadow};

  border: 1px solid ${({ borderColor }) => borderColor};

  ${({ direction, position, innerColor, borderColor }) =>
    directionSelector(direction, position, innerColor, borderColor)}

  ${({ css }) => `${css}`}
`;
