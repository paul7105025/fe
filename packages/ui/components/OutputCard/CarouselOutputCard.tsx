import React from "react";
import { useRecoilValue } from "recoil";

import {
  BundleToolType,
  OutputMetadata,
  useEvent,
  useHistoryAPI,
} from "@wrtn/core";
import { nextRouteState, outputLoadingState } from "@wrtn/core/stores";

import { Dialog } from "../Dialog";
import { ModalPortal } from "../ModalPortal";
import { ReportModal } from "../ReportModal";
import { EditableCardOutput } from "../EditableCardOutput";

interface CarouselOutputCardProps {
  isTriggerEvaluate: boolean;
  handleCloseEvaluate: () => void;
  history: OutputMetadata;
  onClick: () => void;
  onDelete: () => void;
  onUpdate: (data: any) => void;
  currentIdx: number;
  maxIdx: number;
  onIncrease: () => void;
  onDecrease: () => void;
  plan: "FREE" | "PLUS";
  parent: BundleToolType;
}

export const CarouselOutputCard = ({
  isTriggerEvaluate,
  handleCloseEvaluate,
  history: data,
  onClick,
  onDelete,
  onUpdate,
  currentIdx,
  maxIdx,
  onIncrease,
  onDecrease,
  plan,
  parent,
}: CarouselOutputCardProps) => {
  const outputLoading = useRecoilValue(outputLoadingState);
  const router = useRecoilValue(nextRouteState);

  const { collectEvent } = useEvent();

  const [history, setHistory] = React.useState(data);
  const [value, setValue] = React.useState(history?.output);
  const [openReport, setOpenReport] = React.useState(false);
  const [isBlockModalOpened, setIsBlockModalOpened] = React.useState(false);

  const historyAPI = useHistoryAPI({ history, setHistory });

  return (
    <React.Fragment>
      <EditableCardOutput
        isTriggerEvaluate={isTriggerEvaluate}
        handleCloseEvaluate={handleCloseEvaluate}
        type="bundle"
        //
        currentIdx={currentIdx}
        maxIdx={maxIdx}
        onIncrease={onIncrease}
        onDecrease={onDecrease}
        //
        plan={plan}
        openBlockModal={() => {
          collectEvent("click_update_output", {
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
            current_plan: plan,
          });
          setIsBlockModalOpened(true);
        }}
        //
        value={value}
        defaultValue={history?.originOutput}
        //
        favorite={history?.liked}
        dislike={history?.disliked}
        //
        outputLoading={outputLoading}
        onChange={(value) => setValue(value)}
        onClick={() => {
          historyAPI.Click(() => onClick());
        }}
        onClickCopy={() => {
          collectEvent("click_copy_btn", {
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Copy(value);
        }}
        onClickFavorite={() => {
          collectEvent("click_like_btn", {
            onoff: !history.liked,
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Like(() => {
            onUpdate({
              ...history,
              liked: !history.liked,
            });
          });
        }}
        onClickDislike={() => {
          collectEvent("click_unlike_btn", {
            onoff: !history.disliked,
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Dislike(() => {
            onUpdate({
              ...history,
              disliked: !history.disliked,
            });
          });
        }}
        onClickDelete={() => {
          historyAPI.Remove(() => {
            onDelete();
          });
        }}
        onClickRestore={() =>
          historyAPI.Reset(() => {
            setValue(history.originOutput);
          })
        }
        onClickReport={() => {
          collectEvent("click_report_btn", {
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Report(() => {
            setOpenReport(true);
          });
        }}
      />
      {openReport && (
        <ModalPortal>
          <ReportModal
            currentHistory={history}
            onClose={() => setOpenReport(false)}
            onSuccess={() => {
              onDelete();
            }}
            type="tool"
            kind={parent.kind}
          />
        </ModalPortal>
      )}
      {isBlockModalOpened && (
        <ModalPortal>
          <Dialog
            width={395}
            iconType="surprise"
            title="플러스 요금제에서만 사용할 수 있는 기능이에요!"
            description={[
              "<결과물 수정> 기능은 플러스 요금제에서만",
              "사용할 수 있어요. 요금제를 업그레이드하고",
              "더 많은 기능을 사용해보세요!",
            ]}
            handleClose={() => setIsBlockModalOpened(false)}
            rightButtonLabel="요금제 업그레이드"
            handleRightButton={() => {
              collectEvent("view_plan_page", {
                position: "edit_result",
              });
              setIsBlockModalOpened(false)
              router.push("/app/setting/plan");
            }}
            disableCancel={true}
          />
        </ModalPortal>
      )}
    </React.Fragment>
  );
};
