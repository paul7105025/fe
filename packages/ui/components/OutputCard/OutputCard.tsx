import React from "react";
import { useRecoilValue } from "recoil";

import { Dialog } from "../Dialog";

import { ModalPortal } from "../../components/ModalPortal";
import { ReportModal } from "../../components/ReportModal";
import { EditableCardOutput } from "../../components/EditableCardOutput";

import { useDelayUpdate, useEvent, useHistoryAPI } from "@wrtn/core";
import { nextRouteState, outputLoadingState } from "@wrtn/core/stores";

interface OutputCardProps {
  isTriggerEvaluate: boolean;
  handleCloseEvaluate: () => void;
  history: any;
  onClick: () => void;
  onDelete: () => void;
  onUpdate: (data: any) => void;
  plan: "FREE" | "PLUS";
  parent: any;
  isSelectedInStep?: boolean;
}

export const OutputCard = ({
  isTriggerEvaluate,
  handleCloseEvaluate,
  history: data,
  onClick,
  onDelete,
  onUpdate,
  plan,
  parent,
  isSelectedInStep = false,
}: OutputCardProps) => {
  const outputLoading = useRecoilValue(outputLoadingState);

  const [history, setHistory] = React.useState(data);
  const [value, setValue] = React.useState(history?.output);
  const [openReport, setOpenReport] = React.useState(false);

  const [isBlockModalOpened, setIsBlockModalOpened] = React.useState(false);

  const router = useRecoilValue(nextRouteState);

  const historyAPI = useHistoryAPI({ history, setHistory });
  const { collectEvent } = useEvent();

  const changeValue = useDelayUpdate(
    (value) => setValue(value),
    (value) => onUpdate({ ...history, output: value })
  );

  return (
    <React.Fragment>
      <EditableCardOutput
        isTriggerEvaluate={isTriggerEvaluate}
        handleCloseEvaluate={handleCloseEvaluate}
        type="single"
        //
        isSelectedInStep={isSelectedInStep}
        //
        plan={plan}
        openBlockModal={() => {
          collectEvent("click_update_output", {
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
            current_plan: plan,
          });
          setIsBlockModalOpened(true);
        }}
        //
        value={value}
        defaultValue={history?.originOutput}
        //
        favorite={history?.liked}
        dislike={history?.disliked}
        //
        outputLoading={outputLoading}
        onChange={changeValue}
        onClick={() => {
          historyAPI.Click(() => onClick());
        }}
        onClickCopy={() => {
          collectEvent("click_copy_btn", {
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Copy(value);
        }}
        onClickFavorite={() => {
          collectEvent("click_like_btn", {
            onoff: !history.liked,
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Like(() => {
            onUpdate({
              ...history,
              liked: !history.liked,
            });
          });
        }}
        onClickDislike={() => {
          collectEvent("click_unlike_btn", {
            onoff: !history.disliked,
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Dislike(() => {
            onUpdate({
              ...history,
              disliked: !history.disliked,
            });
          });
        }}
        onClickDelete={() =>
          historyAPI.Remove(() => {
            onDelete();
          })
        }
        onClickRestore={() =>
          historyAPI.Reset(() => {
            setValue(history.originOutput);
          })
        }
        onClickReport={() => {
          collectEvent("click_report_btn", {
            position: "tool",
            feature_menu: "tool",
            feature_category: parent.category,
            feature_name: parent.name,
            feature_tag: parent.tag,
            feature_form: parent.kind,
          });
          historyAPI.Report(() => {
            setOpenReport(true);
          });
        }}
      />
      {openReport && (
        <ModalPortal>
          <ReportModal
            currentHistory={history}
            onClose={() => setOpenReport(false)}
            onSuccess={() => {
              onDelete();
            }}
            type="tool"
            kind={parent.kind}
          />
        </ModalPortal>
      )}
      {isBlockModalOpened && (
        <ModalPortal>
          <Dialog
            width={395}
            iconType="surprise"
            title="플러스 요금제에서만 사용할 수 있는 기능이에요!"
            description={[
              "<결과물 수정> 기능은 플러스 요금제에서만",
              "사용할 수 있어요. 요금제를 업그레이드하고",
              "더 많은 기능을 사용해보세요!",
            ]}
            handleClose={() => setIsBlockModalOpened(false)}
            rightButtonLabel="요금제 업그레이드"
            handleRightButton={() => {
              collectEvent("view_plan_page", {
                position: "edit_result",
              });
              setIsBlockModalOpened(false)
              router.push("/setting/plan");
            }}
            disableCancel={true}
          />
        </ModalPortal>
      )}
    </React.Fragment>
  );
};
