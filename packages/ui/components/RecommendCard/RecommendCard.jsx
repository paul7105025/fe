import styled from "styled-components";
import {
  MetaFull,
  LogoNaver,
  LogoBlog,
  LogoBook,
  LogoCopywriting,
  LogoYoutube,
  LogoWriting,
} from "../../assets/SVGComponent/Icon";

import purple from "../../assets/img/img_recommend_bg_purple.png";
import green from "../../assets/img/img_recommend_bg_green.png";
import blue from "../../assets/img/img_recommend_bg_blue.png";
import yellow from "../../assets/img/img_recommend_bg_yellow.png";
import red from "../../assets/img/img_recommend_bg_red.png";

import { typo, colors, FlexWrapper } from "../../styles";
import { boxShadow } from "../../styles/boxShadow";

import { TOOL_ICON_SELECTOR, TOOL_LABEL_SELECTOR } from "../../types";
import { TOOL_TAG_RECORD } from "@wrtn/core";

/**
 * @param {object} props
 * @param {string} props.color
 * @param {string} props.icon
 * @param {string} props.name
 * @param {string} props.description
 * @returns
 */
//TODO: 컴포넌트 이름 개선정도는 필요.
//TODO: Props 네이밍 개선도 필요.
const RecommendCard = ({
  color,
  icon,
  name,
  description,
  onClick,
  tag,
  label,
}) => {
  return (
    <Wrapper onClick={onClick}>
      <Top color={color}>{decideLogo[name]}</Top>
      <Bottom>
        <LogoIcon>{TOOL_ICON_SELECTOR[icon]}</LogoIcon>
        <NameWrapper>
          {tag && tag !== "none" && (
            <Tag color={TOOL_TAG_RECORD[tag]?.color}>
              {TOOL_TAG_RECORD[tag]?.name}
            </Tag>
          )}
          <Name>{name}</Name>
          {label &&
            label !== "" &&
            label !== "none" &&
            TOOL_LABEL_SELECTOR[label]}
        </NameWrapper>
        <Description>{description}</Description>
      </Bottom>
    </Wrapper>
  );
};

export default RecommendCard;

const NameWrapper = styled(FlexWrapper)`
  margin-right: auto;
  align-items: center;
  gap: 4px;
  justify-content: flex-start;
  margin-bottom: 13px;
`;

const Top = styled(FlexWrapper)`
  background-repeat: no-repeat;
  background-size: contain;
  width: 100%;
  min-height: 120px;

  font-family: Gotham Black;

  ${typo({
    size: "25px",
    height: "25px",
    weight: "900",
    color: colors.WHITE,
  })}

  ${({ color }) =>
    color === "purple"
      ? `background-image: url(${purple})`
      : color === "green"
      ? `background-image: url(${green})`
      : color === "yellow"
      ? `background-image: url(${yellow})`
      : color === "blue"
      ? `background-image: url(${blue})`
      : color === "red"
      ? `background-image: url(${red})`
      : null};
`;

const Wrapper = styled.div`
  display: block;
  border-radius: 20px;
  width: 390px;
  background: white;
  ${boxShadow.thumbnail_card_shadow};
  &:hover {
    cursor: pointer;
    ${boxShadow.thumbnail_card_hover_shadow};
  }
  @media (max-width: 480px) {
    width: calc(100vw - 75px);
  }
`;

const Bottom = styled.div`
  width: 100%;
  height: 180px;

  padding: 20px 29px;

  background: #fff;

  border-radius: 0px 0px 17px 17px;

  @media (max-width: 480px) {
    padding: 16px 25px;
  }
`;

const LogoIcon = styled.div`
  margin-bottom: 19px;
`;

const Tag = styled.div`
  display: inline-block;
  position: relative;
  padding: 3px 8px;
  background: ${(props) => colors[props.color]};
  border-radius: 4px;

  ${typo({
    weight: "500",
    size: "14px",
    height: "100%",
    color: colors.WHITE,
  })};
`;

const Name = styled.p`
  ${typo({
    weight: "600",
    size: "20px",
    height: "100%",
    color: colors.gray_90,
  })};
`;

const Description = styled.p`
  ${typo({
    weight: "500",
    size: "16px",
    height: "145%",
    color: colors.gray_70,
  })};

  display: -webkit-box;
  -webkit-box-orient: vertical;
  overflow: hidden;
  -webkit-line-clamp: 2;
`;

const decideLogo = {
  "책 초안": <LogoBook />,
  "SNS 광고문구": <MetaFull />,
  "블로그 포스팅": <LogoBlog />,
  카피라이팅: <LogoCopywriting />,
  "네이버 파워링크": <LogoNaver />,
  "유튜브 영상": <LogoYoutube />,
  이어쓰기: <LogoWriting />,
};
