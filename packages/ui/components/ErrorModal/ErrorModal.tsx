import styled from "styled-components";

import { Close } from "../../assets";
import { ReactComponent as Error } from "../../assets/icons/icon_error.svg";
import { colors, FlexButton, FlexWrapper, typo } from "../../styles";

export const ErrorModal = ({
  onClose,
  title,
  subTitle,
}: {
  onClose: () => void;
  title?: string;
  subTitle: string[];
}) => {
  return (
    <Wrapper>
      <ErrorIcon>
        <Error />
      </ErrorIcon>
      <ErrorText>
        <strong>{title}</strong>
        {title && <br />}
        {subTitle.map((text, i) => (
          <span key={i}>
            {text}
            <br />
          </span>
        ))}
      </ErrorText>
      <CloseIcon onClick={onClose}>
        <Close />
      </CloseIcon>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  padding: 11px 15px 10px 20px;

  background-color: ${colors.WHITE};
  border-radius: 12px;
  border: 1px solid ${colors.GRAY_30};
  box-shadow: 0px 2px 10px rgba(79, 68, 195, 0.1);
  gap: 13px;
  z-index: 100;

  @media (max-width: 480px) {
    flex-direction: column;
    align-items: center;
    max-width: 90vw;
  }
`;

const ErrorText = styled.p`
  ${typo({
    size: "12px",
    height: "160%",
    weight: 500,
    color: colors.GRAY_90,
    isStatic: true,
  })}
  white-space: nowrap;

  @media (max-width: 480px) {
    white-space: break-spaces;
    text-align: center;
  }
`;

const ErrorIcon = styled(FlexButton)`
  width: 20px;
  height: 20px;
`;

const CloseIcon = styled(FlexButton)`
  width: 20px;
  height: 20px;
  cursor: pointer;
`;
