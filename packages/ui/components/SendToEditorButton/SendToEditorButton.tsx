import React from "react";
import { useRecoilValue } from "recoil";
import styled from "styled-components";

import { postDoc, putDoc_Id } from "@wrtn/core/services";

import { Bubble } from "../Bubble";
import { ModalPositionPortal } from "../ModalPortal";

import textToLexical from "../../utils/textToLexical";

import { colors, FlexButton, FlexWrapper, typo } from "../../styles";
import { Next } from "../../assets";
import { nextRouteState, useEvent } from "@wrtn/core";

const sendToEditor = async ({ title, topic, category, text, push }) => {
  const res = await postDoc();

  if (res.data.result === "SUCCESS") {
    const id = res.data.data._id;

    if (id) {
      const res2 = await putDoc_Id({
        id,
        data: {
          title,
          topic,
          category,
          content: textToLexical(text),
        },
      });

      if (res2.data.result === "SUCCESS") {
        push(`/editor/${id}`);
      }
    }
  }
};

export const SendToEditorButton = ({ title, topic, category, text }) => {
  const wrapperRef = React.useRef<HTMLDivElement>(null);
  const { collectEvent } = useEvent();
  const [isHover, setIsHover] = React.useState(false);

  const router = useRecoilValue(nextRouteState);

  return (
    <Wrapper ref={wrapperRef}>
      <Button
        onClick={() => {
          collectEvent("create_editor_document");
          sendToEditor({
            title,
            topic,
            category,
            text,
            push: router.push,
          });
        }}
        onMouseEnter={() => setIsHover(true)}
        onMouseLeave={() => setIsHover(false)}
      >
        <NewText>NEW!</NewText>
        <GoText>에디터로</GoText>
        <Next />
      </Button>
      {isHover && (
        <ModalPositionPortal
          position={{
            top: wrapperRef.current
              ? wrapperRef.current.getBoundingClientRect().top + 80
              : 0,
            left: wrapperRef.current
              ? wrapperRef.current.getBoundingClientRect().right - 225
              : 0,
          }}
        >
          <BubbleWrapper>
            <Bubble
              direction="top"
              position={115}
              style={{ padding: "12px 11px", borderRadius: "10px" }}
            >
              <BubbleText>에디터에서 더 길게 작성해보세요!</BubbleText>
            </Bubble>
          </BubbleWrapper>
        </ModalPositionPortal>
      )}
    </Wrapper>
  );
};

export const NextSendToEditorButton = ({
  disabled,
  title,
  topic,
  category,
  text,
}) => {
  const buttonRef = React.useRef<HTMLButtonElement>(null);
  const [isHover, setIsHover] = React.useState(false);

  const router = useRecoilValue(nextRouteState);

  return (
    <>
      <AbsoluteButton
        disabled={disabled}
        ref={buttonRef}
        onClick={() =>
          sendToEditor({
            title,
            topic,
            category,
            text,
            push: router.push,
          })
        }
        onMouseEnter={() => setIsHover(true)}
        onMouseLeave={() => setIsHover(false)}
      >
        <NewText disabled={disabled}>NEW!</NewText>
        <GoText>에디터로</GoText>
        <Next />
      </AbsoluteButton>
      {isHover && !disabled && (
        <ModalPositionPortal
          position={{
            top: buttonRef.current
              ? buttonRef.current.getBoundingClientRect().top - 60
              : 0,
            left: buttonRef.current
              ? buttonRef.current.getBoundingClientRect().left - 50
              : 0,
          }}
        >
          <BubbleWrapper>
            <Bubble
              direction="bottom"
              position={115}
              style={{ padding: "12px 11px", borderRadius: "10px" }}
            >
              <BubbleText>에디터에서 더 길게 작성해보세요!</BubbleText>
            </Bubble>
          </BubbleWrapper>
        </ModalPositionPortal>
      )}
    </>
  );
};

const Wrapper = styled(FlexWrapper)`
  margin-top: 5px;
  padding: 15px 23px;
  position: relative;
  width: 100%;
  justify-content: flex-end;
`;

const Button = styled(FlexButton)`
  background-color: ${colors.POINT_PURPLE};
  border-radius: 5px;

  padding: 10px 18px;
  gap: 8px;

  cursor: pointer;

  &:hover {
    background-color: ${colors.POINT_PURPLE_HOVER};
  }
`;

const AbsoluteButton = styled(Button)`
  position: absolute;
  width: 173px;
  height: 42px;
  right: 30px;
  bottom: 103px;

  background-color: ${({ disabled }) =>
    disabled ? colors.BLUE_GRAY_LINE : colors.POINT_PURPLE};
  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
  z-index: 3;

  &:hover {
    background-color: ${({ disabled }) =>
      disabled ? colors.BLUE_GRAY_LINE : colors.POINT_PURPLE_HOVER};
  }
  @media (max-width: 1023px) {
    position: fixed;
    bottom: 95px;
    display: none;
  }
  @media (max-width: 767px) {
    bottom: 73px;
  }
`;

const NewText = styled.span<{ disabled?: boolean }>`
  ${({ disabled }) =>
    typo({
      size: "14px",
      weight: 800,
      color: disabled ? colors.WHITE : colors.STAR_YELLOW,
    })}
`;

const GoText = styled.span`
  ${typo({
    size: "16px",
    weight: 600,
    color: colors.WHITE,
    height: "145%",
  })}
`;

const BubbleWrapper = styled(FlexWrapper)``;

const BubbleText = styled.p`
  ${typo({
    size: "16px",
    weight: 700,
    color: colors.ACTION_BLUE,
  })}
`;
