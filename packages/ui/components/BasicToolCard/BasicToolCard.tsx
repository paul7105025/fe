import styled, { keyframes } from "styled-components";

import { colors, FlexButton, FlexWrapper, SmallTag } from "../../styles";

import { Star, FilledStar } from "../../assets/SVGComponent/Icon";

import {
  TOOL_ICON_SELECTOR,
  TOOL_LABEL_SELECTOR,
  TOOL_TAG_SELECTOR,
} from "../../types";

interface BasicToolCardProps {
  icon: string;
  isFavorite?: boolean;
  onClickFavorite?: () => void;
  name: string;
  label?: string;
  tag?: string;
  description: string;
  onClick?: () => void;
  isSlick?: boolean;
}

export const BasicToolCard = ({
  icon,
  isFavorite = false,
  onClickFavorite,
  name,
  label,
  tag = "none",
  description,
  onClick,
  isSlick,
}: BasicToolCardProps) => {
  return (
    <Wrapper column isSlick={isSlick}>
      <InnerWrapper onClick={onClick}>
        <LogoIcon>{TOOL_ICON_SELECTOR[icon]}</LogoIcon>
        <NameWrapper>
          {(!tag || tag !== "none") && (
            <SmallTag color={TOOL_TAG_SELECTOR[tag]?.color}>
              {TOOL_TAG_SELECTOR[tag]?.name}
            </SmallTag>
          )}
          <Name>{name}</Name>
          {label &&
            label !== "" &&
            label !== "none" &&
            TOOL_LABEL_SELECTOR[label]}
        </NameWrapper>
        <Description>{description}</Description>
      </InnerWrapper>
      <StarIcon favorite={isFavorite} onClick={onClickFavorite}>
        {isFavorite ? <FilledStar /> : <Star />}
      </StarIcon>
    </Wrapper>
  );
};

const gradient = keyframes`
  0% {
    background-position: 0% 5%;
  }
  50% {
    background-position: 95% 95%;
  }
  100% {
    background-position: 0% 5%;
  }
`;

const Wrapper = styled(FlexWrapper)<{ isSlick: boolean | undefined }>`
  justify-content: flex-start;

  position: relative;

  max-width: 342px;
  width: 100%;
  min-height: 212px;

  box-shadow: 4px 4px 10px rgba(79, 68, 195, 0.05);

  border: 1px solid #ecf0ff;
  border-radius: 20px;

  overflow: hidden;

  background-color: #ffffff;
  padding: 5px;

  animation: ${gradient} 3.5s ease-in-out infinite;
  animation-play-state: paused;

  &:hover {
    cursor: pointer;
    background: linear-gradient(
        115.82deg,
        rgba(158, 113, 255, 0.2) 4.9%,
        rgba(108, 237, 255, 0.2) 98.09%
      ),
      linear-gradient(112.52deg, #ecf0ff 2.63%, #f0f2f9 94.13%);
    background-size: 155% 155%;
    box-shadow: 10px 10px 24px rgba(79, 68, 196, 0.1);

    animation-play-state: running;
  }

  @media (max-width: 767px) {
    max-width: 100%;
  }

  ${({ isSlick }) =>
    isSlick &&
    `
    width : 342px;
    @media (max-width: 1023px) {
      width: 285px;
    }
    @media (max-width: 480px) {
      width: 240px;
    }
  `}
`;

const InnerWrapper = styled.div`
  background: #ffffff;
  border-radius: 15px;
  padding: 23px;

  width: 100%;
  min-height: 100%;

  flex: 1;

  /* min-height: 198px; */
`;

const NameWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 22px;
  gap: 7px;
  justify-content: flex-start;
`;

const Name = styled.p`
  /* flex: 1; */
  font-weight: 600;
  font-size: 20px;
  line-height: 100%;
  color: ${colors.GRAY_90};
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

const Description = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
  color: ${colors.GRAY_70};

  margin-top: 12px;
  overflow: hidden;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  display: -webkit-box;
`;

const StarIcon = styled(FlexButton)<{ favorite: boolean }>`
  &:hover {
    svg {
      path {
        fill: ${({ favorite }) =>
          favorite ? colors.STAR_YELLOW : colors.POINT_PURPLE};
      }
    }
  }

  svg {
    path {
      fill: ${({ favorite }) =>
        favorite ? colors.STAR_YELLOW : colors.GRAY_55};
    }
  }

  position: absolute;
  top: 22px;
  right: 18px;
  cursor: pointer;
`;

const LogoIcon = styled.div`
  svg {
    width: 40px;
    height: 40px;
    margin: 0 auto;
    text-align: center;
    g {
      transform: scale(1);
      transform-origin: center center;
    }
  }
`;
