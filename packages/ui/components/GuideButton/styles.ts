import styled, {
  css,
  DefaultTheme,
  ThemedCssFunction,
} from "styled-components";

import { FlexWrapper, colors, typo, AutoSizeTextArea } from "../../styles";
import { Help } from "../../assets/SVGComponent/Icon";

export const HelpIcon = styled(Help)`
  width: 20px;
  height: 20px;
`;

// @ts-ignore
export const GuideButtonWrapper = styled(FlexWrapper)<{ disabled?: boolean }>`
  flex-direction: row;

  width: 24px;
  height: 24px;
  position: relative;
  &:disabled {
    svg {
      path {
        fill: ${colors.gray_70};
      }
    }
  }
  &:hover {
    svg {
      path {
        fill: ${(props) => !props.disabled && colors.POINT_PURPLE};
      }
    }
  }

  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
`;

export const GuideInputWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 5px;
`;

export const GuideLabel = styled.label`
  display: flex;
  gap: 5px;
  align-items: center;
`;

export const GuideText = styled(AutoSizeTextArea)`
  flex: 1;

  padding: 4px;
  border: 1px solid rgb(196, 201, 215);
  border-radius: 4px;
`;

export const SimpleGuideButtonWrapper = styled(GuideButtonWrapper)`
  width: 20px;
  height: 20px;
`;

export const HoverContentWrapper = styled.div`
  z-index: 20;
  position: absolute;
  margin-left: 18px;
  left: 18px;
  top: -17px;
  background: transparent;

  min-width: 300px;
`;

export const hoverContent = (left, width = 500) => css`
  width: ${width}px;
  max-width: ${`calc(100vw - ${left}px - 50px)`};

  flex-direction: column;
  align-items: flex-start;
`;

export const HoverTitle = styled.h5`
  ${typo({
    size: "16px",
    weight: 700,
    color: colors.ACTION_BLUE,
    height: "16px",
  })}
  margin-bottom: 8px;

  white-space: pre-line;
`;

export const HoverText = styled.p`
  white-space: pre-line;

  ${typo({
    size: "16px",
    weight: 500,
    color: colors.GRAY_80,
    height: "28px",
  })}
  white-space: pre-line;
`;

export const HoverTag = styled.p`
  ${typo({
    size: "14px",
    weight: 500,
    color: colors.GRAY_60,
    height: "28px",
  })}
  white-space: pre-line;
`;

export const HoverContentEditor = styled.textarea`
  resize: none;
  border: 1px solid rgb(196, 201, 215);
`;
