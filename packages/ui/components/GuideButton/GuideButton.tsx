import React from "react";
import styled from "styled-components";

import { ModalPositionPortal } from "../ModalPortal";
import { Bubble } from "../Bubble";

import {
  GuideButtonWrapper,
  HelpIcon,
  HoverContentWrapper,
  HoverTitle,
  HoverText,
  HoverTag,
  hoverContent,
  SimpleGuideButtonWrapper,
} from "./styles";

interface GuideButtonProps {
  guide?: {
    isVisible: boolean;
    text: string;
    tag?: string;
  };
  name?: string;
  hoverTitle?: string;
  disabled?: boolean;
}

export const GuideButton = ({
  guide = {
    isVisible: false,
    text: "",
    tag: "",
  },
  name,

  hoverTitle = "작성 가이드",
  disabled = false,
}: GuideButtonProps) => {
  const [isHover, setIsHover] = React.useState(false);
  const ref = React.useRef<HTMLDivElement>(null);

  if (!guide.isVisible) return null;
  return (
    <GuideButtonWrapper
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      ref={ref}
      disabled={disabled}
    >
      <HelpIcon />
      {isHover && !disabled && (
        <ModalPositionPortal
          position={{
            top: ref.current && ref.current.getBoundingClientRect().top,
            left: ref.current && ref.current.getBoundingClientRect().left,
          }}
        >
          <HoverContentWrapper>
            <Bubble
              direction="left"
              position={18}
              // @ts-ignore
              css={hoverContent(
                ref.current && ref.current.getBoundingClientRect().left
              )}
            >
              {hoverTitle && <HoverTitle>{hoverTitle}</HoverTitle>}
              <HoverText>{guide?.text}</HoverText>
              {guide?.tag && <HoverTag>{guide?.tag}</HoverTag>}
            </Bubble>
          </HoverContentWrapper>
        </ModalPositionPortal>
      )}
    </GuideButtonWrapper>
  );
};

interface SimpleGuideButtonProps {
  guide?: {
    isVisible: boolean;
  };
  name?: string;
  iconColor?: string;
}

export const SimpleGuideButton = ({
  guide = {
    isVisible: false,
  },
  name,

  children,
  iconColor,
}: React.PropsWithChildren<SimpleGuideButtonProps>) => {
  const [isHover, setIsHover] = React.useState(false);
  const ref = React.useRef<HTMLDivElement>(null);

  if (!guide.isVisible) return null;
  return (
    <SimpleGuideButtonWrapper
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      ref={ref}
    >
      <IconWrapper color={iconColor}>
        <HelpIcon />
      </IconWrapper>
      {isHover && (
        <ModalPositionPortal
          position={{
            top: ref.current && ref.current.getBoundingClientRect().top + 52,
            left: ref.current && ref.current.getBoundingClientRect().left - 164,
          }}
        >
          <HoverContentWrapper>
            <Bubble
              direction="top"
              position={128}
              // @ts-ignore
              css={hoverContent(
                ref.current && ref.current.getBoundingClientRect().left - 160,
                289
              )}
            >
              {children}
            </Bubble>
          </HoverContentWrapper>
        </ModalPositionPortal>
      )}
    </SimpleGuideButtonWrapper>
  );
};

export default GuideButton;

const IconWrapper = styled.div`
  width: 100%;
  height: 100%;
  svg {
    path {
      fill: ${(props) => props.color};
    }
  }
`;
