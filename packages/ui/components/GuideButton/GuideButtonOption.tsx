import React from "react";

import { useDelayUpdate } from "@wrtn/core";

import { ModalPositionPortal } from "../ModalPortal/ModalPortal";
import { Bubble } from "../Bubble/Bubble";

import {
  GuideInputWrapper,
  GuideLabel,
  GuideText,
  HelpIcon,
  hoverContent,
  HoverContentEditor,
  HoverContentWrapper,
} from "./styles";

export const GuideButtonOption = ({
  guide = {
    isVisible: false,
    text: "",
    tag: "",
  },
  onChangeGuide,
}) => {
  const [isVisible, setIsVisible] = React.useState(guide?.isVisible || false);
  const [guideTemp, setGuideTemp] = React.useState(guide?.text || "");
  // const [tagTemp, setTagTemp] = React.useState(guide?.tag || "");

  const guideVisibleChanged = (newIsVisible) => {
    setIsVisible(newIsVisible);
    onChangeGuide(newIsVisible, "isVisible");
  };

  const guideInputChanged = useDelayUpdate(
    (e) => setGuideTemp(e.target.value),
    (e) => onChangeGuide(e.target.value, "text")
  );

  // const tagInputChanged = useDelayUpdate(
  //   (e) => setTagTemp(e.target.value),
  //   (e) => onChangeGuide(e.target.value, "tag")
  // );

  return (
    <GuideInputWrapper>
      <GuideLabel>
        가이드 :
        <input
          type="checkbox"
          checked={isVisible}
          onChange={() => guideVisibleChanged(!isVisible)}
        />
      </GuideLabel>
      {isVisible && (
        <GuideText value={guideTemp} onChange={guideInputChanged} />
        // <GuideButtonWrapper>
        //   <HelpIcon onClick={() => setIsHover(!isHover)} />
        //   {isHover && (
        //     <ModalPositionPortal
        //       position={{
        //         top: ref.current && ref.current.getBoundingClientRect().top,
        //         left:
        //           ref.current && ref.current.getBoundingClientRect().left + 5,
        //       }}
        //     >
        //       <HoverContentWrapper>
        //         <Bubble
        //           direction="left"
        //           position={18}
        //           css={hoverContent(
        //             ref.current && ref.current.getBoundingClientRect().left
        //           )}
        //         >
        //           가이드 이름
        //           <HoverContentEditor
        //             value={guideTemp}
        //             onChange={guideInputChanged}
        //           />
        //           가이드 예제
        //           <HoverContentEditor
        //             value={tagTemp}
        //             onChange={tagInputChanged}
        //           />
        //         </Bubble>
        //       </HoverContentWrapper>
        //     </ModalPositionPortal>
        //   )}
        // </GuideButtonWrapper>
      )}
    </GuideInputWrapper>
  );
};
