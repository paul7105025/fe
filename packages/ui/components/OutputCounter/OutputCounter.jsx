import React from "react";
import styled from "styled-components";

import { typo, colors, boxShadow, FlexWrapper, FlexButton } from "../../styles";
import { Down } from "../../assets/SVGComponent/Icon";

export const OutputCounter = ({
  handleDown,
  handleUp,
  count = 1,
  maxCount,
  isText=true,
}) => {
  return (
    <Wrapper>
      { isText && <Label disabled={maxCount === 1 || !maxCount}>생성 개수</Label>}
      <CounterContainer>
        <IconButton onClick={handleDown} disable={count < 2 || !maxCount}>
          <DownIcon />
        </IconButton>
        <Number disabled={maxCount === 1 || !maxCount}>{count}</Number>
        <IconButton onClick={handleUp} disable={count >= maxCount || !maxCount}>
          <UpIcon />
        </IconButton>
      </CounterContainer>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  max-width: 180px;
  width: 100%;

  justify-content: space-between;
  gap: 9px;

  ${boxShadow.counter_button_shadow};
  background: #ffffff;
  border-radius: 5px;

  padding: 10px 13px 10px 17px;

  border: 1px solid ${colors.WHITE_BOLDER_LINE};

  @media (max-width: 480px) {
    width: 100%;
  }
  user-select: none;
`;

const CounterContainer = styled(FlexWrapper)`
  width: 70px;
  justify-content: space-between;
  gap: 6px;
`;

const Label = styled.p`
  ${(props) =>
    typo({
      weight: "600",
      size: "16px",
      height: "100%",
      color: props.disabled ? colors.gray_55 : colors.gray_80,
    })}
`;

const Number = styled.p`
  ${(props) =>
    typo({
      weight: "700",
      size: "16px",
      height: "19px",
      color: props.disabled ? colors.gray_55 : colors.gray_90,
    })};
  min-width: 20px;
  text-align: center;
`;

const IconButton = styled(FlexButton)`
  min-width: 16px;
  height: 20px;
  cursor: pointer;
  ${({ disable }) => disable && `pointer-events: none;`}
  svg {
    path {
      fill: ${({ disable }) => (disable ? colors.GRAY_55 : colors.GRAY_70)};
    }
  }
`;

const DownIcon = styled(Down)``;

const UpIcon = styled(DownIcon)`
  transform: rotate(180deg);
`;
