import styled from "styled-components";

import { boxShadow, colors, FlexWrapper, typo } from "../../styles";

export const RemainCounter = ({ remain }) => {
  return (
    <Wrapper>
      <Label>남은 횟수</Label>
      <Divider />
      <Number>{remain < 0 ? 0 : remain}회</Number>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  justify-content: space-between;

  ${boxShadow.counter_button_shadow};
  background: #ffffff;
  border-radius: 5px;

  border: 1px solid ${colors.WHITE_BOLDER_LINE};

  @media (max-width: 480px) {
    width: 100%;
  }
`;

const Label = styled.p`
  padding: 12px 14px;
  white-space: nowrap;

  ${(props) =>
    typo({
      weight: "600",
      size: "16px",
      height: "100%",
      color: props.disabled ? colors.gray_55 : colors.gray_80,
    })}
`;

const Divider = styled.div`
  width: 1px;
  height: 100%;
  min-height: 45px;
  background: ${colors.WHITE_BOLDER_LINE};
`;

const Number = styled.p`
  white-space: nowrap;

  padding: 12px 14px;
  ${typo({
    weight: "700",
    size: "16px",
    height: "19px",
    color: colors.POINT_PURPLE,
  })};

  text-align: center;
`;
