import React from "react";
import styled from "styled-components";

import { typo, colors, FlexWrapper } from "../../styles";

interface AvatarProps {
  width?: string;
  size?: string;
}

export const Avatar: React.FC<React.PropsWithChildren<AvatarProps>> = ({
  children,
  width = "30px",
  size = "14px",
}) => {
  return (
    <Wrapper width={width}>
      <Label size={size}>{children}</Label>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)<{
  width: string;
}>`
  width: ${({ width }) => width};
  height: ${({ width }) => width};

  background: ${colors.LILAC};

  border-radius: 50%;

  display: flex;
  align-items: center;
  justify-content: center;
`;

const Label = styled.p<{
  size: string;
}>`
  ${({ size }) =>
    typo({
      weight: 600,
      size: size,
      height: "100%",
      color: colors.WHITE,
    })}
  user-select: none;
`;
