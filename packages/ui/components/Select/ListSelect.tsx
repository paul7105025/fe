import React from "react";
import dayjs from "dayjs";
import styled from "styled-components";

import { colors, typo, FlexWrapper, FlexButton } from "../../styles";
import { IconCarouselDown } from "../../assets";

type FormListType = {
  list: readonly string[];
  value: string;
  setValue: (val: string) => void;
  disabled?: boolean;
  reverse?: boolean;
  small?: boolean;
};

export const ListSelect: React.FC<FormListType> = ({
  list,
  value,
  setValue,
  disabled = false,
  reverse = false,
  small = false,
}: FormListType) => {
  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
    if (disabled) setOpen(false);
  }, [disabled]);

  const handleClick = () => {
    if (!disabled) setOpen((c) => !c);
  };

  const handleChangeSelection = (val: string) => {
    setValue(val);
    setOpen(false);
  };

  return (
    <Wrapper>
      <Select
        key={value}
        onClick={handleClick}
        open={open}
        disabled={disabled}
        reverse={reverse}
        small={small}
      >
        {value}
        <IconWrapper open={open} reverse={reverse} small={small}>
          <IconCarouselDown />
        </IconWrapper>
      </Select>
      {open && (
        <InsideWrapper reverse={reverse} small={small}>
          {list.map((nonSelect, i) => {
            return (
              <NonSelect
                key={nonSelect}
                onClick={() => handleChangeSelection(nonSelect)}
                small={small}
              >
                {nonSelect}
              </NonSelect>
            );
          })}
        </InsideWrapper>
      )}
    </Wrapper>
  );
};

export const CouponDropdown = ({
  list,
  value,
  setValue,
}: {
  list: Array<any>;
  value: any;
  setValue: (val: any) => void;
}) => {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => setOpen((c) => !c);

  const handleChangeSelection = (val: string) => {
    setValue(val);
    setOpen(false);
  };

  return (
    <Wrapper>
      <CouponSelect
        onClick={handleClick}
        open={open}
        disabled={false}
        reverse={false}
        small={true}
      >
        {value
          ? `${value.coupon.name} (${value.coupon.description})`
          : "쿠폰 사용 안 함"}
        <IconWrapper open={open} reverse={false} small={true}>
          <IconCarouselDown />
        </IconWrapper>
      </CouponSelect>
      {open && (
        <CouponInsideWrapper reverse={false} small={true}>
          <NonSelect onClick={() => handleChangeSelection("none")} small={true}>
            <Name>쿠폰 사용 안 함</Name>
          </NonSelect>
          {list.map((nonSelect, i) => {
            return (
              <NonSelect
                key={nonSelect.coupon._id}
                onClick={() => handleChangeSelection(nonSelect)}
                small={true}
              >
                <Name>
                  {nonSelect.coupon.name} ({nonSelect.coupon.description}) { nonSelect.status === "using" && "(현재 사용 중인 쿠폰)"}
                </Name>
                <DueDate>
                  쿠폰 만료 기간 :{" "}
                  {dayjs(nonSelect.coupon.dueDate).format("YYYY.MM.DD")}
                </DueDate>
              </NonSelect>
            );
          })}
        </CouponInsideWrapper>
      )}
    </Wrapper>
  );
};

export const EtcDropdown = ({
  list,
  value,
  setValue,
  disabled = false,
}: {
  list: Array<string>;
  value: string;
  setValue: (val: string) => void;
  disabled?: boolean;
}) => {
  const [open, setOpen] = React.useState(false);
  const [isEtc, setIsEtc] = React.useState(false);

  const handleClick = () => setOpen((c) => !c);

  const handleChangeSelection = (val: string) => {
    setIsEtc(false);
    setValue(val);
    setOpen(false);
  };

  const handleClickEtc = () => {
    setIsEtc(true);
    setOpen(false);
    setValue("");
  };

  React.useEffect(() => {
    if (!list.includes(value)) setIsEtc(true);
  }, []);

  return (
    <Wrapper>
      <CouponSelect
        onClick={disabled || isEtc ? undefined : handleClick}
        open={open}
        disabled={disabled}
        reverse={false}
        small={true}
        style={{
          cursor: disabled ? "not-allowed" : isEtc ? "normal" : "pointer",
          fontSize: "14px",
          fontWeight: 500,
        }}
      >
        {isEtc ? (
          <EtcInput value={value} onChange={(e) => setValue(e.target.value)} />
        ) : (
          <div>{value}</div>
        )}
        <IconWrapper
          onClick={isEtc ? (disabled ? undefined : handleClick) : undefined}
          open={open}
          reverse={false}
          small={true}
        >
          <IconCarouselDown />
        </IconWrapper>
      </CouponSelect>
      {open && (
        <CouponInsideWrapper reverse={false} small={true}>
          {list.map((nonSelect) => {
            return (
              <NonSelect
                key={nonSelect}
                onClick={() => handleChangeSelection(nonSelect)}
                small={true}
              >
                {nonSelect}
              </NonSelect>
            );
          })}
          <NonSelect onClick={handleClickEtc} small={true}>
            직접 입력
          </NonSelect>
        </CouponInsideWrapper>
      )}
    </Wrapper>
  );
};

export const CheckboxDropdown = ({
  list,
  value,
  setValue,
}: {
  list: Array<string>;
  value: Array<string>;
  setValue: (val: string[]) => void;
}) => {
  const [open, setOpen] = React.useState(false);

  const innerValue = React.useRef<Array<string>>(value);
  const [etcValue, setEtcValue] = React.useState("");

  const [innerChecked, setInnerChecked] = React.useState<Array<boolean>>([]);

  React.useEffect(() => {
    if (innerValue.current.length < 1) {
      innerValue.current = value;
    }
  }, [value]);

  React.useEffect(() => {
    const value = innerValue.current;

    if (Array.isArray(value) && value.length > 0) {
      setInnerChecked([
        ...list.map((item, i) => value.includes(item)),
        value.filter((i) => !list.includes(i)).length > 0 ? true : false,
      ]);
      setEtcValue(
        value.filter((i) => !list.includes(i)).length > 0
          ? value[value.length - 1]
          : ""
      );
    }
  }, [list, innerValue.current]);

  const isEtcOpen = React.useMemo(
    () =>
      innerChecked.length > 0 ? innerChecked[innerChecked.length - 1] : false,
    [innerChecked]
  );

  const handleClickOpen = () => setOpen((c) => !c);

  const handleCheck = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    if (e.target.checked && innerChecked.filter((i) => i).length >= 3) return;
    if (!e.target.checked && innerChecked.filter((i) => i).length <= 1) return;

    setInnerChecked((c) => {
      const newC = [...c];
      newC[index] = e.target.checked;
      return newC;
    });
  };

  const handleCheckEtc = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.checked && innerChecked.filter((i) => i).length >= 3) return;
    if (!e.target.checked && innerChecked.filter((i) => i).length <= 1) return;

    setInnerChecked((c) => {
      const newC = [...c];
      newC[newC.length - 1] = e.target.checked;
      return newC;
    });
  };

  //set value
  React.useEffect(() => {
    const newV = list.filter((item, index) => innerChecked[index]);
    if (isEtcOpen && etcValue.length > 0) newV.push(etcValue);


    setValue(newV);
  }, [innerChecked, isEtcOpen, etcValue]);

  return (
    <Wrapper>
      <CouponSelect
        onClick={handleClickOpen}
        open={open}
        disabled={false}
        reverse={false}
        small={true}
        style={{
          fontSize: "14px",
          fontWeight: 500,
        }}
      >
        {Array.isArray(value) ? value.join(", ") : value}
        <div style={{ flex: 1 }} />
        <IconWrapper open={open} reverse={false} small={true}>
          <IconCarouselDown />
        </IconWrapper>
      </CouponSelect>
      {open && (
        <CouponInsideWrapper
          reverse={false}
          small={true}
          style={{
            gap: 14,
            padding: "19px 22px",
          }}
        >
          {innerChecked.length > 0 &&
            list.map((item, index) => (
              <SelectLabel key={item}>
                <Checkbox
                  type="checkbox"
                  checked={innerChecked[index]}
                  onChange={(e) => handleCheck(e, index)}
                />
                {item}
              </SelectLabel>
            ))}
          <SelectLabel>
            <Checkbox
              type="checkbox"
              checked={isEtcOpen}
              onChange={handleCheckEtc}
            />
            직접 입력
          </SelectLabel>
          {isEtcOpen && (
            <EtcInput
              value={etcValue}
              onChange={(e) => setEtcValue(e.target.value)}
              placeholder="직접 입력"
            />
          )}
        </CouponInsideWrapper>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  width: 100%;
`;

interface SelectProps {
  open: boolean;
  disabled: boolean;
  reverse: boolean;
  small?: boolean;
}

const Select = styled(FlexWrapper)<SelectProps>`
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
  background: ${colors.WHITE};
  border-radius: 5px;
  width: 100%;
  padding: ${({ small }) =>
    small ? "4px 4px 4px 16px;" : "8px 4px 8px 16px;"};

  white-space: nowrap;
  user-select: none;

  justify-content: space-between;
  gap: 4px;

  ${({ small }) =>
    typo({
      weight: 500,
      size: small ? "14px" : "16px",
      height: "100%",
      color: colors.gray_100,
    })};
  cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
  border: 1px solid
    ${({ small }) => (small ? colors.GRAY_55 : colors.BLUE_GRAY_LINE)};

  ${({ open, reverse }) =>
    open
      ? reverse
        ? "border-top-left-radius: 0px; border-top-right-radius: 0px;"
        : "border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;"
      : ""}
`;

const NonSelect = styled.div<{ small?: boolean }>`
  background: ${colors.WHITE};
  // border-radius: 5px;
  width: 100%;
  padding: 12px 16px;
  ${({ small }) =>
    typo({
      weight: 500,
      size: small ? "14px" : "16px",
      height: "100%",
      color: colors.gray_100,
    })};

  cursor: pointer;

  &:hover {
    background: #f2f7ff;
  }

  z-index: 10;
`;

const InsideWrapper = styled(FlexWrapper)<{
  reverse: boolean;
  small?: boolean;
}>`
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  background: ${colors.WHITE};

  position: absolute;
  ${({ reverse, small }) =>
    small ? "top : 32px;" : reverse ? "bottom: 40px;" : "top: 40px;"}
  width: 100%;
  z-index: 1;

  border: 1px solid ${colors.BLUE_GRAY_LINE};

  ${({ reverse }) =>
    reverse
      ? "border-top-left-radius: 5px; border-top-right-radius: 5px;"
      : "border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;"}

  overflow: hidden;
`;

const IconWrapper = styled(FlexButton)<{
  open: boolean;
  reverse: boolean;
  small?: boolean;
}>`
  width: 28px;
  height: 26px;

  cursor: pointer;

  transform: ${({ open, reverse, small }) =>
    small
      ? open
        ? "rotate(180deg)"
        : "rotate(0deg)"
      : open
      ? reverse
        ? "rotate(-180deg)"
        : "rotate(0deg)"
      : "rotate(-90deg)"};

  transition: transform 0.3s cubic-bezier(0.1, -0.15, 0.5, 1.55);

  svg {
    path {
      fill: ${colors.GRAY_60};
    }
  }
`;

const CouponSelect = styled(Select)`
  ${typo({
    weight: 700,
    size: "18px",
    color: "#3F3F3F",
  })};
  padding: 8px 8px 8px 16px;
`;

const CouponInsideWrapper = styled(InsideWrapper)`
  top: 40px;
`;

const Name = styled.p`
  ${typo({
    weight: 700,
    size: "18px",
    color: "#3F3F3F",
  })};
`;

const DueDate = styled.p`
  margin-top: 6px;
  ${typo({
    weight: 500,
    size: "14px",
    color: colors.GRAY_60,
    height: "130%",
  })};
`;

const EtcInput = styled.input`
  width: 100%;

  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  border-radius: 5px;

  padding: 8px 16px;
`;

const SelectLabel = styled.label`
  display: flex;
  align-items: center;
  gap: 10px;

  ${typo({
    weight: 500,
    size: "16px",
    height: "160%",
    color: colors.GRAY_90,
  })}
`;

const Checkbox = styled.input`
  width: 20px;
  height: 20px;
`;
