import React from "react";
import styled from "styled-components";

import { TOOL_CATEGORY_RECORD, useClickOutside } from "@wrtn/core";
import { ModalPositionPortal } from "../ModalPortal/ModalPortal";

import { colors, FlexButton, FlexWrapper, Tag } from "../../styles";
import {
  TOOL_ICON_SELECTOR,
  TOOL_LABEL_SELECTOR,
  TOOL_STATUS_COLOR_SELECTOR,
  TOOL_TAG_SELECTOR,
} from "../../types";

/**
 *
 * @param {object} props >
 * @param {function} props.onClick
 */

export const MetadataSelect = ({
  type = "status",
  currentData,
  data,
  onChange,
}) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const ref = React.useRef<HTMLDivElement>(null);
  const innerRef = React.useRef<HTMLDivElement>(null);

  const onClickOption = (value) => {
    onChange(value);
    setIsOpen(!isOpen);
  };

  useClickOutside([ref, innerRef], "click", () => setIsOpen(false));

  return (
    <Wrapper ref={ref}>
      <Label onClick={() => setIsOpen(!isOpen)}>
        {type === "status" && (
          <Tag color={TOOL_STATUS_COLOR_SELECTOR[currentData] || "none"}>
            {currentData}
          </Tag>
        )}
        {type === "icon" && (
          <ToolLogo>{TOOL_ICON_SELECTOR[currentData]}</ToolLogo>
        )}
        {type === "tag" && (
          <Tag color={TOOL_TAG_SELECTOR[currentData]?.color || "none"}>
            {TOOL_TAG_SELECTOR[currentData]?.name || "없음"}
          </Tag>
        )}
        {type === "category" && TOOL_CATEGORY_RECORD[currentData]}
        {type === "label" && TOOL_LABEL_SELECTOR[currentData]}
      </Label>
      {isOpen && (
        <ModalPositionPortal
          position={{
            top: ref.current && ref.current.getBoundingClientRect().bottom,
            left: ref?.current && ref.current.getBoundingClientRect().left,
          }}
        >
          <OptionWrapper ref={innerRef}>
            {type === "status" &&
              data.map((item) => {
                return (
                  <Option key={item} onClick={() => onClickOption(item)}>
                    <Tag color={TOOL_STATUS_COLOR_SELECTOR[item] || "none"}>
                      {item}
                    </Tag>
                  </Option>
                );
              })}
            {type === "icon" &&
              data.map((item) => {
                return (
                  <Option key={item} onClick={() => onClickOption(item)}>
                    <ToolLogo>{TOOL_ICON_SELECTOR[item]}</ToolLogo>
                  </Option>
                );
              })}
            {type === "tag" &&
              data.map((item) => {
                return (
                  <Option key={item} onClick={() => onClickOption(item)}>
                    <Tag color={TOOL_TAG_SELECTOR[item]?.color || "none"}>
                      {TOOL_TAG_SELECTOR[item]?.name || "없음"}
                    </Tag>
                  </Option>
                );
              })}
            {type === "category" &&
              data.map((item) => {
                return (
                  <Option key={item} onClick={() => onClickOption(item)}>
                    {TOOL_CATEGORY_RECORD[item]}
                  </Option>
                );
              })}
            {type === "label" &&
              data.map((item) => {
                return (
                  <Option key={item} onClick={() => onClickOption(item)}>
                    {TOOL_LABEL_SELECTOR[item]}
                  </Option>
                );
              })}
          </OptionWrapper>
        </ModalPositionPortal>
      )}
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  flex-direction: column;
`;

const Label = styled(FlexButton)`
  cursor: pointer;
`;

const OptionWrapper = styled(FlexWrapper)`
  gap: 5px;
  background-color: ${colors.BACKGROUND};
  flex-direction: column;
  justify-content: flex-start;
  max-height: 300px;
  overflow-y: scroll;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
`;

const Option = styled(FlexButton)`
  width: 100%;
  padding: 8px 14px;
  cursor: pointer;
  font-size: 14px;

  &:hover {
    background-color: ${colors.GRAY_55};
  }
`;

const ToolLogo = styled.div`
  min-width: 16px;
  min-height: 16px;
  > svg {
    width: 16px;
    height: 16px;
  }
`;
