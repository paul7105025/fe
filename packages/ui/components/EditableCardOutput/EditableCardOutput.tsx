import React, { lazy } from "react";
import { useAlert } from "react-alert";
import TextareaAutosize from "react-textarea-autosize";
import { CopyToClipboard } from "react-copy-to-clipboard";
import styled, { css, keyframes } from "styled-components";

import {
  currentCountScoreState,
  inappChatDefaultValueState,
  inappChatOpenState,
  Kind,
  PlanType,
  triggerWrtnChatState,
  useDelayUpdate,
  useEvent,
  userState,
  useTriggerGenerateCount,
} from "@wrtn/core";

import { boxShadow, colors, FlexButton, FlexWrapper, typo } from "../../styles";

import {
  Copy,
  Dislike,
  FilledDislike,
  FilledLike,
  Heart,
  IconRightPolygon,
  IconWaterDropList,
  IconWaterDropListFilled,
  Refresh,
  Report,
  TrashCan,
} from "../../assets/SVGComponent/Icon";
import { ModalPositionPortal } from "../../components/ModalPortal";
import { Bubble } from "components/Bubble";
import {
  useRecoilCallback,
  useRecoilState,
  useRecoilValue,
  useSetRecoilState,
} from "recoil";

/**
 *
 * @param {object} props
 * @param {boolean} props.favorite
 * @param {boolean} props.dislike
 * @param {string} props.value
 * @param {string} props.defaultValue
 * @param {string} props.placeholder
 * @param {function()} props.onClick
 * @param {function()} props.onChange
 * @param {function()} props.onClickReport
 * @param {function()} props.onClickDislike
 * @param {function()} props.onClickDelete
 * @param {function()} props.onClickFavorite
 * @param {function()} props.onClickCopy
 * @returns
 */

interface EditableCardOutputProps {
  isTriggerEvaluate: boolean;
  handleCloseEvaluate: () => void;

  type: Kind;
  //
  currentIdx?: number;
  maxIdx?: number;
  onIncrease?: () => void;
  onDecrease?: () => void;
  //
  plan: PlanType;
  openBlockModal: () => void;
  //
  value: string;
  defaultValue: string;
  //
  isSelectedInStep?: boolean;
  placeholder?: string;
  outputLoading: boolean;
  //
  favorite: boolean;
  dislike: boolean;
  //
  onClick: () => void;
  onChange: (value: string) => void;
  onClickReport: () => void;
  onClickDelete: () => void;
  onClickFavorite: () => void;
  onClickDislike: () => void;
  onClickCopy: () => void;
  onClickRestore: () => void;
}

export const EditableCardOutput = ({
  isTriggerEvaluate,
  handleCloseEvaluate,

  type = "single",

  currentIdx,
  maxIdx,
  onIncrease,
  onDecrease,

  plan,
  openBlockModal,

  value,
  defaultValue,

  isSelectedInStep,
  placeholder,
  outputLoading,

  favorite,
  dislike,

  onClick,
  onChange,
  onClickReport,
  onClickDelete,
  onClickFavorite,
  onClickDislike,
  onClickCopy,
  onClickRestore,
}: EditableCardOutputProps) => {
  const alert = useAlert();
  const { collectEvent } = useEvent();

  const init = React.useRef(false);
  const buttonRef = React.useRef<HTMLButtonElement>(null);

  const [inputValue, setInputValue] = React.useState(value);
  const [isFocused, setIsFocused] = React.useState(false);

  const [lazyButtonPos, setLazyButtonPos] = React.useState<{
    top: number;
    left: number;
  } | null>(null);
  const [isHoverChat, setIsHoverChat] = React.useState(false);

  const setIsOpenChat = useSetRecoilState(inappChatOpenState);
  const setChatDefaultValue = useSetRecoilState(inappChatDefaultValueState);
  const triggerWrtnChat = useRecoilValue(triggerWrtnChatState);

  const [disableTrigger, setDisabledTrigger] = React.useState(false);

  const isTriggerWrtnChat = triggerWrtnChat && !disableTrigger;
  React.useEffect(() => {
    setDisabledTrigger(false);
    const time = setTimeout(() => {
      setDisabledTrigger(true);
    }, 2000);

    return () => clearTimeout(time);
  }, [triggerWrtnChat]);

  React.useEffect(() => {
    if (isTriggerEvaluate) {
      const pos = buttonRef?.current?.getBoundingClientRect();
      setLazyButtonPos({
        top: (pos?.top || 0) + 30,
        left: (pos?.left || 0) - 10,
      });
    } else {
      setLazyButtonPos(null);
    }
  }, [isTriggerEvaluate]);

  React.useEffect(() => {
    init.current = true;
  }, []);

  React.useEffect(() => {
    setInputValue(value);
  }, [value]);

  const onChangeWithPlan = (value) => {
    if (plan === "FREE") {
      openBlockModal();
      setInputValue(defaultValue);
    } else {
      onChange(value);
    }
  };

  const handleChange = useDelayUpdate(
    (e) => setInputValue(e.target.value),
    (e) => onChangeWithPlan(e.target.value)
  );

  const handleRestore = () => {
    if (defaultValue) {
      setInputValue(defaultValue);
      onChange(defaultValue);
      onClickRestore();
    }
  };

  const handleClickCopy = React.useCallback(() => {
    onClickCopy();
    alert.removeAll();
    alert.show("결과물을 복사했어요.");
  }, [alert]);

  return (
    <Wrapper
      onClick={onClick}
      disabled={outputLoading}
      isSelectedInStep={isSelectedInStep}
      blink={!init.current}
    >
      <InnerWrapper>
        <InputWrapper isFocused={isFocused}>
          <Input
            onFocus={() => setIsFocused(true)}
            onBlur={() => setIsFocused(false)}
            spellCheck={false}
            placeholder={placeholder}
            value={inputValue}
            onChange={handleChange}
            disabled={outputLoading}
          />
        </InputWrapper>
        <TextLengthWrapper>
          <TextLength>
            공백 제외{" "}
            <TextLengthValue>
              {inputValue?.replace(/\s/g, "").length}자
            </TextLengthValue>
          </TextLength>
          <TextLength>
            공백 포함{" "}
            <TextLengthValue>{inputValue?.length || 0}자</TextLengthValue>
          </TextLength>
        </TextLengthWrapper>
        <ButtonGroupWrapper>
          <IconButton
            ref={buttonRef}
            onClick={() => {
              onClickFavorite();
              handleCloseEvaluate();
            }}
            disabled={outputLoading}
          >
            {favorite ? (
              <FilledDislikeIcon style={{ transform: "rotate(180deg)" }} />
            ) : (
              <Dislike style={{ transform: "rotate(180deg)" }} />
            )}
          </IconButton>
          <IconButton
            onClick={() => {
              onClickDislike();
              handleCloseEvaluate();
            }}
            disabled={outputLoading}
          >
            {dislike ? <FilledDislikeIcon /> : <Dislike />}
          </IconButton>
          {defaultValue !== value && (
            <IconButton onClick={handleRestore} disabled={outputLoading}>
              <Refresh />
            </IconButton>
          )}
          {/* {type !== "step" && (
            <IconLabelButton
              onClick={() => {
                collectEvent("view_chat_page", {
                  position: "tool_history_card",
                });
                setIsOpenChat(true);
                setChatDefaultValue(`"${inputValue}"`);
              }}
              active={isTriggerWrtnChat}
              onMouseOver={() => {
                setIsHoverChat(true);
              }}
              onMouseLeave={() => {
                setIsHoverChat(false);
              }}
            >
              {isTriggerWrtnChat || isHoverChat ? (
                <WaterDropListFilled />
              ) : (
                <WaterDropList />
              )}{" "}
              채팅으로 수정
            </IconLabelButton>
          )} */}
          <Spacer />
          {type === "bundle" &&
            currentIdx !== undefined &&
            maxIdx !== undefined && (
              <CountGroup>
                <Carousel
                  isMore={currentIdx > 0 ? true : false}
                  style={{ transform: "rotateY(180deg)" }}
                  onClick={onDecrease}
                >
                  <IconRightPolygon />
                </Carousel>
                {currentIdx + 1 + " / " + maxIdx}
                <Carousel isMore={currentIdx < maxIdx - 1} onClick={onIncrease}>
                  <IconRightPolygon />
                </Carousel>
              </CountGroup>
            )}
          <SecretIconButton onClick={onClickDelete} disabled={outputLoading}>
            <TrashCan />
          </SecretIconButton>
          <SecretIconButton onClick={onClickReport} disabled={outputLoading}>
            <Report />
          </SecretIconButton>
          <CopyToClipboard text={value}>
            <IconButton onClick={handleClickCopy}>
              <Copy />
            </IconButton>
          </CopyToClipboard>
        </ButtonGroupWrapper>
      </InnerWrapper>
      {isTriggerEvaluate && lazyButtonPos && (
        <ModalPositionPortal
          position={{
            top: lazyButtonPos.top,
            left: lazyButtonPos.left,
          }}
        >
          <BubbleWrapper onClick={handleCloseEvaluate}>
            평가를 하면 맞춤형 결과를 제공하는데 도움이 돼요!
          </BubbleWrapper>
        </ModalPositionPortal>
      )}
    </Wrapper>
  );
};

const CountGroup = styled(FlexWrapper)`
  position: absolute;
  ${"" /* TODO: 중앙 정렬 해결하는 좋은 방법? */}
  left: 50%;
  transform: translateX(-50%);

  align-items: center;
  justify-content: center;

  ${typo({ weight: 500, size: "14px", height: "17px", color: colors.gray_60 })};

  gap: 11px;
`;

const InputWrapper = styled.div<{
  isFocused: boolean;
}>`
  padding: 7px 13px;

  margin-bottom: 20px;

  border: 1px solid ${colors.WHITE};

  background-color: ${({ isFocused }) =>
    isFocused ? colors.BACKGROUND : colors.WHITE};

  &:hover {
    background: ${colors.BACKGROUND};
    border-radius: 5px;
  }
  &:focus {
    background: ${colors.BACKGROUND};
    border: 1px solid ${colors.ACTION_BLUE};
    border-radius: 5px;
  }
`;

const Input = styled(TextareaAutosize)`
  resize: none;
  font-weight: 600;
  font-size: 16px;
  line-height: 170%;
  color: ${colors.GRAY_90};
  width: 100%;

  overflow: hidden;

  background-color: transparent;
`;

const fadeout = keyframes`
  0% {
    filter: drop-shadow(0px 4px 24px rgba(79, 68, 195, 0.1));
    background: linear-gradient(
      93.06deg,
      #b0c2ff -15.64%,
      #bfafff 99.49%,
      #bfafff 99.5%
    );
    opacity: 1;
  }

  80% {
    filter: drop-shadow(0px 4px 24px rgba(79, 68, 195, 0.1));
    background: linear-gradient(
      93.06deg,
      #b0c2ff -15.64%,
      #bfafff 99.49%,
      #bfafff 99.5%
    );
    background-color:rgba(0, 0, 0, 0.5);
  }

  100% {
    background-color:rgba(0, 0, 0, 0);
  }
`;

const Wrapper = styled.div<{
  disabled: boolean;
  isSelectedInStep?: boolean;
  blink: boolean;
}>`
  background: ${colors.WHITE};
  border-radius: 20px;
  overflow: auto;

  ${({ blink }) =>
    blink &&
    css`
      animation-duration: 2s;
      animation-name: ${fadeout};
    `}

  &:hover {
    filter: drop-shadow(0px 4px 24px rgba(79, 68, 195, 0.1));
    background: linear-gradient(
      93.06deg,
      #b0c2ff -15.64%,
      #bfafff 99.49%,
      #bfafff 99.5%
    );
    > div {
      button {
        display: block !important;
      }
    }
  }

  ${({ isSelectedInStep }) =>
    isSelectedInStep &&
    css`
      filter: drop-shadow(0px 4px 24px rgba(79, 68, 195, 0.1));
      background: ${colors.ACTION_BLUE};
      > div {
        button {
          display: block !important;
        }
      }
    `}
`;

const InnerWrapper = styled.div`
  background: white;
  margin: 2px;
  padding: 16px 15px 17px;
  border-radius: 18px;

  @media (max-width: 767px) {
    padding: 16px 15px 17px;
  }
  @media (max-width: 480px) {
    padding: 16px 15px 17px;
  }
`;

const ButtonGroupWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 14px;
  padding: 0px 15px;
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const IconButton = styled.button`
  padding: 0px;
  margin: 0px;
  width: 22px;
  height: 22px;
  background: none;
  border: none;
  cursor: pointer;
  > svg {
    width: 22px;
    height: 22px;
    path {
      fill: ${colors.GRAY_55};
    }
  }
  &:hover {
    > svg {
      path {
        fill: ${(props) =>
          props.disabled ? colors.GRAY_55 : colors.POINT_PURPLE};
      }
    }
  }
  &:disabled {
    cursor: progress;
  }
`;

const SecretIconButton = styled(IconButton)`
  display: none;
`;

const FilledHeart = styled(FilledLike)`
  path {
    fill: #5a2cda !important;
  }
`;

const FilledDislikeIcon = styled(FilledDislike)`
  path {
    fill: #5a2cda !important;
  }
`;

const Carousel = styled(FlexButton)<{
  isMore: boolean;
}>`
  width: 14px;
  height: 14px;

  cursor: pointer;
  svg {
    path {
      fill: ${({ isMore }) => (isMore ? colors.GRAY_60 : colors.GRAY_50)};
    }
  }
`;

const TextLength = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.GRAY_58,
  })};
  text-align: right;
`;

const TextLengthWrapper = styled(FlexWrapper)`
  justify-content: flex-end;
  gap: 10px;
  margin-bottom: 20px;
  margin-right: 15px;
`;

const TextLengthValue = styled.span`
  color: ${colors.gray_60};
`;

const BubbleWrapper = styled(FlexWrapper)`
  border-radius: 10px;
  padding: 12px 18px;
  background: ${colors.ACTION_BLUE};
  ${boxShadow.guideline_shadow};

  ${typo({
    weight: 700,
    size: "16px",
    color: colors.WHITE,
  })}

  &::after {
    position: absolute;
    content: "";
    border-width: 8px;
    border-style: solid;

    left: 32px;
    top: -15px;
    border-color: transparent transparent ${colors.ACTION_BLUE} transparent;
  }

  &::before {
    position: absolute;
    content: "";
    border-width: 8px;
    border-style: solid;

    left: 32px;
    top: -15px;
    border-color: transparent transparent ${colors.ACTION_BLUE} transparent;
  }
`;

const IconLabelButton = styled(FlexWrapper)<{ active: boolean }>`
  gap: 4px;
  ${typo({
    weight: 600,
    size: "12px",
    height: "170%",
    color: colors.gray_55,
  })};
  cursor: pointer;
  ${(props) =>
    props.active
      ? `
    background: linear-gradient(90deg, #AF85FF 0%, #7AAEFC 116.31%, #75B2FC 127.98%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    background-clip: text;
    text-fill-color: transparent;
  `
      : ""};
  &:hover {
    background: linear-gradient(
      90deg,
      #af85ff 0%,
      #7aaefc 116.31%,
      #75b2fc 127.98%
    );
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    background-clip: text;
    text-fill-color: transparent;
  }
`;

const WaterDropList = styled(IconWaterDropList)`
  padding: 0px;
  margin: 0px;
  width: 22px;
  height: 22px;
  background: none;
  border: none;
  cursor: pointer;
`;

const WaterDropListFilled = styled(IconWaterDropListFilled)`
  padding: 0px;
  margin: 0px;
  width: 22px;
  height: 22px;
  background: none;
  border: none;
  cursor: pointer;
`;
