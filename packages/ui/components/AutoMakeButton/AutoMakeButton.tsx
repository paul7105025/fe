import React from "react";
import styled, { keyframes } from "styled-components";

import { DefaultSpinner } from "../DefaultSpinner";

import { typo, colors, boxShadow, FlexButton } from "../../styles";
import { Carousel, MagicWand } from "../../assets/SVGComponent/Icon";

interface AutoMakeButtonProps {
  outputLoading: boolean;
  disabled: boolean;
  stopped?: boolean;
  handleGenerate: () => void;
}

export const AutoMakeButton = ({
  outputLoading = false,
  disabled,
  stopped = false,
  handleGenerate,
}: AutoMakeButtonProps) => {
  return (
    <Button
      outputLoading={outputLoading}
      disabled={!stopped && disabled}
      onClick={handleGenerate}
    >
      {!stopped ? (
        outputLoading ? (
          <DefaultSpinner width={18} height={18} color={colors.WHITE} />
        ) : (
          <MagicWand />
        )
      ) : null}
      <Label>{stopped ? "후기 남기고 더 사용하기" : "자동 생성"}</Label>
      {stopped && <WhiteCarousel />}
    </Button>
  );
};

const gradient = keyframes`
  0% {
    background-position: 10% 50%;
  }
  5%{
    background-position: 0% 50%;
  }
  45%{
    background-position: 100% 50%;
  }
  50% {
    background-position: 90% 50%;
  }
  55%{
    background-position: 100% 50%;
  }
  95%{
    background-position: 0% 50%;
  }
  100% {
    background-position: 10% 50%;
  }
`;

const Button = styled(FlexButton)<{
  outputLoading: boolean;
}>`
  flex: 1;

  height: 42px;
  border: none;

  gap: 7px;

  background: linear-gradient(
    90.34deg,
    #c57dff -21.05%,
    #669cff 70.31%,
    #99e5f4 169.62%
  );
  background-size: 125% 100%;
  background-position: 10% 50%;

  animation: ${gradient} 5s ease-in-out infinite;
  animation-play-state: paused;
  &:hover {
    animation-play-state: running;
  }

  opacity: ${({ outputLoading }) => (outputLoading ? 0.7 : 1)};

  border-radius: 4.45729px;

  ${({ disabled }) => !disabled && boxShadow.color_button_shadow};
  cursor: ${({ disabled, outputLoading }) =>
    disabled || outputLoading ? "default" : "pointer"};

  &:disabled {
    background: #e6e8f0;
  }

  @media (max-width: 1023px) {
    width: 100%;
    min-height: 42px;
  }
`;

const Label = styled.p`
  ${typo({
    weight: 600,
    size: "16px",
    height: "24px",
    color: colors.WHITE,
  })}
  user-select: none;
`;

const WhiteCarousel = styled(props => <Carousel {...props} />)`
  path {
    fill: ${colors.WHITE};
  }
`;
