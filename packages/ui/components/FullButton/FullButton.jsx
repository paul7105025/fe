import styled from "styled-components";
import { colors, FlexButton, typo } from "../../styles";

export const FullButton = ({ 
  onClick, 
  children, 
  style={}
}) => {
  return (
    <ButtonWrapper style={style} onClick={onClick}>
      {children}
    </ButtonWrapper>
  );
};

const ButtonWrapper = styled(FlexButton)`
  cursor: pointer;
  width: fit-content;

  padding: 9px 16px;

  gap: 9px;
  border-radius: 8px;

  background-color: ${colors.POINT_PURPLE};
  ${typo({ weight: 600, size: "16px", height: "100%", color: colors.WHITE })};

  &:hover {
    background-color: ${colors.POINT_PURPLE_HOVER};
  }

  @media (max-width: 767px) {
    ${typo({ weight: 600, size: "16px", height: "100%", color: colors.WHITE })};
    padding: 4px 16px;
  }
`;
