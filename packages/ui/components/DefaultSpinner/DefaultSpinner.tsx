import React from "react";
import styled, { keyframes } from "styled-components";
import { colors } from "../../styles";

interface DefaultSpinnerProps {
  width?: number;
  height?: number;
  color?: string;
  border?: number;
}

export const DefaultSpinner = ({
  width = 36,
  height = 36,
  color = colors.WHITE,
  border = 2,
}: DefaultSpinnerProps) => {
  return (
    <Wrapper border={border} color={color} width={width} height={height} />
  );
};

const spin = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const Wrapper = styled.div<{
  width: number;
  height: number;
  border: number;
}>`
  border: ${({ border }) => `${border}px solid ${colors.WHITE}`};
  border-top: ${({ border }) => `${border}px solid rgba(0, 0, 0, 0)`};
  border-radius: 50%;

  width: ${({ width }) => width}px;
  height: ${({ height }) => height}px;

  animation: ${spin} 0.7s linear infinite;

  margin-right: 3px;
`;
