import styled from "styled-components";

export const ToastMessage = ({ message }) => {
  return (
    <Wrapper>
      <Label>{message}</Label>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  background: rgba(57, 59, 68, 0.5);
  border-radius: 15px;
  padding: 12px 12px;
  margin-top: 24px;
  position: fixed !important;
  left: 50%;
  transform: translateX(-50%);
  z-index: 90;
  min-width: 461px;
  @media (max-width: 461px) {
    min-width: 0px;
    width: calc(100% - 40px);
    margin: 0px 20px;
    left: calc(50% - 20px);
    transform: translateX(calc(-50%));
    margin-top: 20px;
  }
`;

const Label = styled.p`
  font-weight: 600;
  font-size: 16px;
  line-height: 100%;
  color: #ffffff;
  text-align: center;
  white-space: pre-line;
`;
