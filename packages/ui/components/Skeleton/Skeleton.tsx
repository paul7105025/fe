import styled, { keyframes } from "styled-components";

interface SkeletonProps {
  width?: string;
  height?: string;
  borderRadius?: string;
  style?: React.CSSProperties;
}

export const Skeleton = ({
  width = "100%",
  height = "100%",
  borderRadius = "5px",
  style,
}: SkeletonProps) => {
  return (
    <SkeletonUI
      width={width}
      height={height}
      borderRadius={borderRadius}
      style={style}
    />
  );
};

const skeletonAnimation = keyframes`
    0% {
        background-color: rgba(193, 197,	204, 0.3);
    }

    50% {
        background-color: rgba(193, 197,	204, 0.4);
    }

    100% {
        background-color: rgba(193, 197,	204, 0.3);
    }
`;

const SkeletonUI = styled.div<SkeletonProps>`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  border-radius: ${(props) => props.borderRadius};

  animation: ${skeletonAnimation} 1.8s ease-in-out infinite;
`;
