import styled, { keyframes } from "styled-components";
import { IconSpinner } from "../../assets";

const offset = 187;

const animate = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(270deg); }
`;

const dash = keyframes`
  0% { stroke-dashoffset: ${offset}; }
  50% {
    stroke-dashoffset: ${offset / 4};
    transform: rotate(135deg);
  }
  100% {
    stroke-dashoffset: ${offset};
    transform: rotate(450deg);
  }
`;

const colors = (color = "#5A2CDA") => keyframes`
  0% { stroke: ${color}; }
	25% { stroke: ${color}; }
	50% { stroke: ${color}; }
	75% { stroke: ${color}; }
  100% { stroke: ${color}; }
`;

const Wrapper = styled(IconSpinner)`
  width: ${(props) => (props.width ? props.width : "36px")};
  height: ${(props) => (props.height ? props.height : "36px")};

  animation: ${animate} 1.4s linear infinite;

  .path {
    stroke-dasharray: ${offset};
    stroke-dashoffset: 0;
    transform-origin: center;
    animation: ${dash} 1.4s ease-in-out infinite,
      ${(props) => colors(props.color)} 5.6s ease-in-out infinite;
  }
`;

export type SpinnerProps = {
  width?: number | undefined;
  height?: number | undefined;
  color?: string | undefined;
};

export const Spinner = ({ width = 36, height = 36, color }: SpinnerProps) => {
  return <Wrapper width={width} height={height} color={color} />;
};
