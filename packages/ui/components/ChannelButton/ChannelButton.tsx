import styled from "styled-components";

import { showMessenger, useEvent } from "@wrtn/core";

import { ReactComponent as IconEbook } from "../../assets/icons/icon_ebook.svg";

import { IconHeadphone } from "../../assets";
import { colors, FlexButton, typo } from "../../styles";

export const ChannelButton = ({ label = "문의하기", onClick }) => {
  const { collectEvent } = useEvent();
  return (
    <Button
      onClick={() => {
        collectEvent("click_channel_talk");
        showMessenger();
        onClick();
      }}
    >
      <IconHeadphone />
      <p>{label}</p>
    </Button>
  );
};

export const ImageGuideButton = () => {
  return (
    <a
      href="https://wrtn.notion.site/f31b7c53a6a5487b95bba31d2081604d"
      target="_blank"
      rel="noopener noreferrer"
    >
      <Button>
        <IconEbook />
        <p>이미지 사용 가이드</p>
      </Button>
    </a>
  );
};

const Button = styled(FlexButton)`
  ${typo({
    size: "14px",
    weight: "600",
    height: "26px",
    color: colors.GRAY_70,
  })}

  border: none;
  background: none;
  text-decoration: underline;
  display: flex;
  align-items: center;
  cursor: pointer;
  gap: 5px;

  svg {
    width: 20px;
    height: 20px;
  }

  user-select: none;
`;
