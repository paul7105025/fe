import React from "react";
import styled, { css } from "styled-components";

import { useClickOutside } from "@wrtn/core";

import { Bubble } from "../Bubble";

import { colors, FlexWrapper, typo } from "../../styles";

export const GuideModal = ({ onClose, guideRef, type = "tool" }) => {
  const innerRef = React.useRef(null);

  useClickOutside([innerRef, guideRef], "mousedown", () => onClose());

  return (
    <Wrapper ref={innerRef}>
      <Bubble
        direction="top"
        position={window.innerWidth < 480 ? 240 : 400}
        css={hoverContent}
      >
        {type === "tool" && (
          <InnerWrapper>
            <Title>뤼튼으로 만든 결과물의 저작권은 누구의 것인가요?</Title>
            <IconDesc>
              저작권 및 사용 권한은 사용자에게 귀속되며, 개인적 용도 및 상업적
              용도로 활용할 수 있습니다. 생성된 결과를 사용하여 발생하는 문제의
              책임 또한 사용자에게 있습니다.
            </IconDesc>
            <Title>뤼튼으로 만든 결과물은 어떤 데이터로 이루어져 있나요?</Title>
            <IconDesc>
              뤼튼은 사용자가 입력한 내용을 토대로 매번 새로운 문장을 생성해
              냅니다. 학습된 데이터를 그대로 뱉거나, 검색엔진 결과에 같은 글이
              나올 확률이 희박합니다.
            </IconDesc>
            <Title>뤼튼으로 만든 결과물의 내용을 신뢰할 수 있나요?</Title>
            <IconDesc>
              결과물은 사용자가 입력한 내용을 최우선으로 반영하며, 경우에 따라
              문장을 생성하는 과정에서 사실과 다른 내용이 나올 수 있습니다.
              생성된 글 이용 시 사실 관계 여부를 확인하시는 것을 권장합니다.
            </IconDesc>
          </InnerWrapper>
        )}
        {type === "image" && (
          <InnerWrapper>
            <Title>뤼튼으로 만든 이미지의 저작권은 누구의 것인가요?</Title>
            <IconDesc>
              이미지의 저작권 및 사용 권한은 사용자에게 귀속되며, 개인적 용도 및
              상업적 용도로 활용할 수 있습니다. 따라서 생성된 이미지를
              활용하면서 발생하는 문제의 책임 또한 사용자에게 있습니다.
            </IconDesc>
            <Title>뤼튼으로 만든 이미지는 어떤 데이터로 이루어져 있나요?</Title>
            <IconDesc>
              뤼튼 이미지는 Stable Diffusion을 기반으로 제작된 서비스입니다.
              뤼튼의 의도와는 무관하게, 실제 장소와 사람에 대한 묘사가 사실과
              다를 수 있습니다.
            </IconDesc>
          </InnerWrapper>
        )}
        {type === "editor" && (
          <InnerWrapper>
            <Title>뤼튼으로 만든 결과물의 저작권은 누구의 것인가요?</Title>
            <IconDesc>
              결과물의 저작권 및 사용 권한은 사용자에게 귀속되며, 개인적 용도 및
              상업적 용도로 활용할 수 있습니다.
            </IconDesc>
            <Title>뤼튼으로 만든 결과물은 어떤 데이터로 이루어져 있나요?</Title>
            <IconDesc>
              뤼튼은 사용자가 입력한 내용을 토대로 매번 새로운 문장을 생성해
              냅니다. 학습된 데이터를 그대로 뱉거나, 검색엔진 결과에 같은 글이
              나올 확률이 희박합니다.
            </IconDesc>
            <Title>뤼튼으로 만든 결과물의 내용을 신뢰할 수 있나요?</Title>
            <IconDesc>
              결과물은 사용자가 입력한 내용을 최우선으로 반영하며, 경우에 따라
              문장을 생성하는 과정에서 사실과 다른 내용이 나올 수 있습니다.
            </IconDesc>
          </InnerWrapper>
        )}
      </Bubble>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  display: flex;
  z-index: 20;
  position: absolute;
  background: transparent;
`;

const hoverContent = css`
  min-width: 469px;
  ${"" /* max-width: 90vw; */}
  min-height: 206px;

  padding: 22px 28px 26px;

  @media (max-width: 767px) {
    min-width: 80vw;
    padding: 20px 16px;
  }
`;

const InnerWrapper = styled(FlexWrapper)`
  position: relative;
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  @media (max-width: 480px) {
    gap: 16px;
  }
`;

const Title = styled.p`
  ${typo({
    weight: "600",
    size: "16px",
    lineHeight: "24px",
    color: colors.ACTION_BLUE,
  })}
  white-space: nowrap;
  @media (max-width: 480px) {
    font-size: 12px;
    white-space: normal;
  }
`;

const IconDesc = styled(FlexWrapper)`
  gap: 4px;
  ${"" /* white-space: nowrap; */}
  white-space: pre-wrap;
  ${typo({
    weight: "500",
    size: "14px",
    height: "21px",
    color: colors.GRAY_80,
  })}
  padding-bottom: 7px;
  @media (max-width: 480px) {
    gap: 2px;
    ${typo({
      weight: "500",
      size: "12px",
      height: "21px",
      color: colors.GRAY_80,
    })}
  }
`;
