import styled from "styled-components";
import { PropsWithChildren } from "react";

import { Icon } from "../Icon";
import { FlexButton, colors, typo, FlexWrapper } from "../../styles";

interface OutlineButtonProps {
  icon?: string;
  width?: "100%" | "auto" | string;
  buttonProps?: React.HTMLAttributes<HTMLButtonElement>;
  styles?: React.CSSProperties;
}

export const OutlineButton = ({
  children,
  icon,
  width = "auto",
  buttonProps,
  styles,
}: PropsWithChildren<OutlineButtonProps>) => {
  return (
    <StyledButton {...buttonProps} style={styles} width={width}>
      <InnerWrapper justify="center">
        {icon && <Icon size={16} color={colors.POINT_PURPLE} icon={icon} />}
        <p>{children}</p>
      </InnerWrapper>
    </StyledButton>
  );
};

const StyledButton = styled(FlexButton)<{ width: string }>`
  border-radius: 5px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  background-color: ${colors.WHITE};

  width: ${({ width }) => width};

  padding: 1px;

  &:hover {
    padding: 0;
    cursor: pointer;

    border: 2px solid transparent;

    background: linear-gradient(white, white) padding-box,
      linear-gradient(
          90.34deg,
          #c57dff -21.05%,
          #669cff 70.31%,
          #99e5f4 169.62%
        )
        border-box;
    background-origin: border-box;
    background-clip: content-box, border-box;
  }
`;

const InnerWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  padding: 6px 15px;

  ${typo({
    size: "16px",
    height: "162%",
    weight: 600,
    color: colors.POINT_PURPLE,
  })}
  text-decoration: none;

  gap: 8px;
`;
