import styled from "styled-components";
import { boxShadow, colors, FlexButton, typo } from "../../styles";

export const StyledButton = styled(FlexButton)<{ selected?: boolean }>`
  flex: 1;
  border: none;

  justify-content: center;

  gap: 7px;
  background: ${({ selected }) =>
    selected
      ? "linear-gradient(90.34deg, #c57dff -21.05%, #669cff 70.31%, #99e5f4 169.62%)"
      : colors.BACKGROUND};
  ${({ disabled }) => !disabled && boxShadow.color_button_shadow};
  border-radius: 5px;
  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
  ${typo({ weight: 500, size: "16px", height: "100%" })};
  color: ${({ selected }) => (selected ? colors.WHITE : colors.ACTION_BLUE)};

  &:disabled {
    background: #e6e8f0;
  }
`;
