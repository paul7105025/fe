import styled, { keyframes } from "styled-components";
import { useRecoilValue, useSetRecoilState } from "recoil";

import { isNoCreditModalOpenState, nextRouteState, useEvent, useIsMobile } from "@wrtn/core";

import { Surprise, Close } from "../../assets";
import { FullButton } from "../FullButton";
import { typo, boxShadow, colors } from "../../styles";
import { FlexButton, FlexWrapper } from "../../styles/container";

import { ModalPositionPortal } from "../ModalPortal";

interface FreePlanBubbleProps {
  isClosing: boolean;
  isVibrate?: boolean;
  handleClose: () => void;
}

export const FreePlanBubble = ({
  isClosing,
  isVibrate = false,
  handleClose,
}: FreePlanBubbleProps) => {
  const router = useRecoilValue(nextRouteState);
  const location = window.location;

  const isMobile = useIsMobile();
  const { collectEvent } = useEvent();

  const position = isMobile
    ? {
        top: 104,
        left: 0,
        right:
          window.innerWidth > 320
            ? (window.innerWidth - 320) / 2
            : (window.innerWidth / 100) * 2.5,
      }
    : {
        top: 104,
        left: 0,
        right: 43,
      };

  return (
    <ModalPositionPortal position={position}>
      {isMobile ? (
        <MobileWrapper isClosing={isClosing} isVibrate={isVibrate}>
          <FlexWrapper column style={{ position: "relative", height: "100%" }}>
            <CloseButton
              style={{
                position: "absolute",
                top: "0px",
                right: "0px",
              }}
              onClick={handleClose}
            >
              <Close />
            </CloseButton>
            <Surprise width={30} height={30} />
            <InfoWrapper
              style={{
                marginTop: "8px",
                flexDirection: "column",
                alignItems: "center",
                gap: "4px",
              }}
            >
              <Title>더 빠르고 안정적으로 사용하고 싶으신가요?</Title>
              <Desc style={{ textAlign: "center" }}>
                플러스 요금제로 더 안정적으로
                <br />
                뤼튼을 사용해보세요!
              </Desc>
            </InfoWrapper>
            <ButtonWrapper style={{ marginTop: "11px" }}>
              <FullButton
                style={{ padding: "10px 8px" }}
                onClick={() => {
                  collectEvent("view_plan_page", {
                    position: location.pathname.includes("editor")
                      ? "lack_character_editor"
                      : "lack_character_tool",
                  });
                  handleClose();
                  router.push("/app/setting/plan");
                }}
              >
                요금제 업그레이드
              </FullButton>
            </ButtonWrapper>
          </FlexWrapper>
        </MobileWrapper>
      ) : (
        <Wrapper isClosing={isClosing} isVibrate={isVibrate}>
          <FlexWrapper style={{ gap: "12px", height: "100%" }}>
            <InfoWrapper>
              <Surprise width={30} height={30} />
              <div>
                <Title>더 빠르고 안정적으로 사용하고 싶으신가요?</Title>
                <Desc>플러스 요금제로 더 안정적으로 뤼튼을 사용해보세요!</Desc>
              </div>
            </InfoWrapper>
            <ButtonWrapper>
              <FullButton
                style={{ padding: "10px 8px" }}
                onClick={() => {
                  collectEvent("view_plan_page", {
                    position: location.pathname.includes("editor")
                      ? "lack_character_editor"
                      : "lack_character_tool",
                  });
                  handleClose();
                  router.push("/app/setting/plan");
                }}
              >
                요금제 업그레이드
              </FullButton>
              <CloseButton onClick={handleClose}>
                <Close />
              </CloseButton>
            </ButtonWrapper>
          </FlexWrapper>
        </Wrapper>
      )}
    </ModalPositionPortal>
  );
};

const visible = keyframes`
  0% { right: -600px }
  80% { right: 36px };
  100% { right: 0px };
`;

const invisible = keyframes`
  0% { right: 0px }
  20% {
    right: 36px;
  }
  100% { right: -600px };
`;

const vibrate = keyframes`
  0% { right: 0px }
  10% { right: 10px }
  20% { right: 0px }
  30% { right: -10px }
  40% { right: 0px }
  50% { right: 10px }
  60% { right: 0px }
  70% { right: -10px }
  80% { right: 0px }
  90% { right: 10px }
    100% { right: 0px }`;

const InfoWrapper = styled(FlexWrapper)`
  gap: 16px;
`;

const ButtonWrapper = styled(FlexWrapper)`
  gap: 16px;
`;

const Wrapper = styled.div<{
  isClosing: boolean;
  isVibrate: boolean;
}>`
  position: absolute;
  width: 561px;
  height: 69px;
  background: ${colors.WHITE};
  ${boxShadow.counter_button_shadow};
  border-radius: 10px;
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  z-index: 100;
  right: 0px;
  top: 0px;
  padding: 14px 18px;

  animation: ${({ isClosing }) => (isClosing ? invisible : visible)} 0.5s
      ease-in-out 1,
    ${({ isVibrate }) => isVibrate && vibrate} 0.5s ease-in-out 1;
`;

const MobileWrapper = styled.div<{ isClosing: boolean; isVibrate: boolean }>`
  position: absolute;
  top: 0px;
  right: 0px;

  width: 95vw;
  max-width: 320px;
  padding: 19px 14px;

  background: ${colors.WHITE};
  ${boxShadow.counter_button_shadow};
  border-radius: 10px;
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  z-index: 100;

  animation: ${({ isClosing }) => (isClosing ? invisible : visible)} 0.5s
      ease-in-out 1,
    ${({ isVibrate }) => isVibrate && vibrate} 0.5s ease-in-out 1;
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;

  &:hover {
    svg {
      path {
        fill: ${colors.gray_60};
      }
    }
  }
`;

const Title = styled.div`
  ${typo({
    weight: 700,
    size: "14px",
    height: "160%",
    color: colors.gray_90,
  })}
`;

const Desc = styled.div`
  ${typo({
    weight: 500,
    size: "14px",
    height: "120%",
    color: colors.gray_90,
  })}
`;
