import styled from "styled-components";

import { Spinner } from "./Spinner";

import { FlexWrapper } from "../styles";

const Loading = () => {
  return (
    <Wrapper column justify="center">
      <Spinner />
    </Wrapper>
  );
};

export default Loading;

const Wrapper = styled(FlexWrapper)`
  height: 100%;
`;
