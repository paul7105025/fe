import React, { useRef, useEffect } from "react";
import { createPortal } from "react-dom";
import styled from "styled-components";
import polyfill from "../../styles/polyfill";

const Wrapper = styled.div`
  position: fixed;
  width: 100vw;
  ${polyfill.dvh("height", 100)};
  z-index: ${(props) => (props.zIndex ? props.zIndex : 95)};
  background-color: rgba(59, 63, 78, 0.4);
  backdrop-filter: blur(4px);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ClearWrapper = styled.div`
  position: absolute;
  width: 100vw;
  ${polyfill.dvh("height", 100)};
  z-index: 94;
  background-color: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Background = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  background-color: transparent;
  top: 0;
  left: 0;
  z-index: -1;
`;

const WrapperRef = styled.div`
  position: fixed;
  z-index: 95;
  display: inline-block;
`;

const createRootElement = (id) => {
  const root = document.createElement("div");
  root.setAttribute("id", `${id}`);
  return root;
};

const addRootElement = (root) => {
  const reactRoot = document.querySelector("#__next");
  reactRoot.prepend(root);
};

const usePortal = (id) => {
  const rootRef = useRef(null);

  useEffect(() => {
    const existedParent = document.querySelector(`#__${id}`);
    const parent = existedParent || createRootElement(`__${id}`);

    if (!existedParent) addRootElement(parent);

    parent.appendChild(rootRef.current);
    return () => {
      rootRef.current.remove();
      if (!parent.childElementCount) parent.remove();
    };
  }, [id]);

  const getRoot = () => {
    if (typeof document === "undefined") return null;
    if (!rootRef.current) rootRef.current = document.createElement("div");
    return rootRef.current;
  };

  return getRoot();
};

export const ModalPortal = ({ children, onClose = () => {}, zIndex = 95 }) => {
  const [isFirst, setIsFirst] = React.useState(true);
  const target = usePortal("modal");

  React.useEffect(() => {
    setIsFirst(false);
  }, []);

  return isFirst
    ? null
    : createPortal(
        <Wrapper zIndex={zIndex}>
          {children} <Background onClick={onClose} />
        </Wrapper>,
        target
      );
};

export const ClearModalPortal = ({ children, onClose = () => {} }) => {
  const [isFirst, setIsFirst] = React.useState(true);
  const target = usePortal("modal");

  React.useEffect(() => {
    setIsFirst(false);
  }, []);

  return isFirst
    ? null
    : createPortal(
        <ClearWrapper>
          {children} <Background onClick={onClose} />
        </ClearWrapper>,
        target
      );
};

export const ModalPositionPortal = ({ position, children }) => {
  const target = usePortal("modal");
  const { top = "auto", left, width } = position;

  return typeof window === "undefined"
    ? null
    : createPortal(
        <WrapperRef
          style={{
            display: top === null && left === null ? "none" : "block",
            top: top,
            left: left,
            width,
            right: position.right ? position.right : "",
            bottom: position.bottom ? position.bottom : "",
          }}
        >
          {children}
        </WrapperRef>,
        target
      );
};
