import {
  onChangeTextValue,
  onChangeListValue,
  onPlusListValue,
  onMinusListValue,
  FormMetadataType,
  FormMetadata,
} from "@wrtn/core";

import {
  FormCheckbox,
  FormCounter,
  FormImageSelect,
  FormInputDivided,
  FormInputExpandable,
  FormInputKeyword,
  FormRadio,
  FormSelect,
  FormTextArea,
  FormTextInput,
} from "../FormElements";

interface FormSelectorProps {
  form: FormMetadataType;
  updateForm: (id: string, targetForm: FormMetadataType) => void;
}

const FormSelector = ({ form, updateForm }: FormSelectorProps) => {
  const { id, type } = form;

  const onChange = (targetForm: FormMetadataType) => updateForm(id, targetForm);

  switch (type) {
    case "checkbox":
      return (
        <FormCheckbox
          {...(form as FormMetadata<"checkbox">)}
          onChange={(value) => onChangeTextValue({ value, form, onChange })}
        />
      );
    case "textarea":
      return (
        <FormTextArea
          {...(form as FormMetadata<"textarea">)}
          onChange={(value) => onChangeTextValue({ value, form, onChange })}
        />
      );
    case "input":
      return (
        <FormTextInput
          {...(form as FormMetadata<"input">)}
          onChange={(value) => onChangeTextValue({ value, form, onChange })}
        />
      );
    case "option_chips":
      return (
        <FormRadio
          {...(form as FormMetadata<"option_chips">)}
          onChange={(value) => onChangeTextValue({ value, form, onChange })}
        />
      );
    case "light_option_chips":
      return (
        <FormRadio
          {...(form as FormMetadata<"light_option_chips">)}
          onChange={(value) => onChangeTextValue({ value, form, onChange })}
          light
        />
      );
    case "input_expandable":
      return (
        <FormInputExpandable
          {...(form as FormMetadata<"input_expandable">)}
          onChange={(value, index) =>
            onChangeListValue({ value, index, form, onChange })
          }
          onPlus={(value) => onPlusListValue({ value: "", form, onChange })}
          onMinus={(index) =>
            onMinusListValue({ value: [], index, form, onChange })
          }
        />
      );
    case "input_keyword":
      return (
        <FormInputKeyword
          {...(form as FormMetadata<"input_keyword">)}
          onChange={(value, index) => {}}
          onPlus={(value) => onPlusListValue({ value, form, onChange })}
          onMinus={(index) =>
            onMinusListValue({ value: [], index, form, onChange })
          }
        />
      );
    // case "counter":
    //   return (
    //     <FormCounter
    //       {...(form as FormMetadata<"counter">)}
    //       onChange={(value) => onChangeTextValue({ value, form, onChange })}
    //     />
    //   );
    case "select":
      return (
        <FormSelect
          {...(form as FormMetadata<"select">)}
          onChange={(value) => onChangeTextValue({ value, form, onChange })}
        />
      );
    case "select_image":
      return (
        <FormImageSelect
          {...(form as FormMetadata<"select_image">)}
          onChange={(value) => onChangeTextValue({ value, form, onChange })}
        />
      );
    case "input_divided":
      return (
        <FormInputDivided
          {...(form as FormMetadata<"input_divided">)}
          onChange={(value, index) =>
            onChangeListValue({ value, form, onChange, index })
          }
        />
      );
    default:
      return null;
  }
};

export default FormSelector;
