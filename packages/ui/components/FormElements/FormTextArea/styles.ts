import styled from "styled-components";
import TextareaAutosize from "react-textarea-autosize";
import {
  FlexWrapper,
  colors,
  InputStyle,
  FormLabel,
} from "../../../styles";

export const Wrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: flex-start;
  flex-direction: column;
`;

export const ColumnWrapper = styled(Wrapper)`
  flex-direction: column;
  align-items: flex-start;
  gap: 16px;
  position: relative;
`;

export const TextAreaWrapper = styled(Wrapper)`
  width: 100%;
  position: relative;
`;

export const TextArea = styled(TextareaAutosize)`
  ${InputStyle}
`;

export const LabelInput = styled.input`
  ${FormLabel}
`;

export const Label = styled.p`
  ${FormLabel}
`;

export const GuideWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: flex-start;
  gap: 6px;
`;

export const RowButton = styled.button`
  margin-left: 12px;
  padding: 5px 7px;
  background-color: ${colors.gray_40};
`;
