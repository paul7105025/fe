import React from "react";

import { useDelayUpdate } from "@wrtn/core";

import { GuideButtonOption } from "../../GuideButton";
import { LengthCounterOption } from "../../LengthCounter";

import {
  ColumnWrapper,
  GuideWrapper,
  LabelInput,
  RowButton,
  TextArea,
} from "./styles";

export const FormTextAreaOption = ({
  guide,
  label = "",
  option,
  onChangeLabel,
  onChangeOption,
  onChangeGuide,
}) => {
  const [labelInput, setLabelInput] = React.useState(label);
  const [placeholder, setPlaceholder] = React.useState(
    option?.placeholder || ""
  );
  const [rows, setRows] = React.useState(option?.rows || 3);

  const [maxLength, setMaxLength] = React.useState(option?.maxLength || 0);

  const handleLabel = useDelayUpdate(
    (e) => setLabelInput(e.target.value),
    (e) => onChangeLabel(e.target.value)
  );

  const handlePlaceholder = useDelayUpdate(
    (e) => setPlaceholder(e.target.value),
    (e) => onChangeOption(e.target.value, "placeholder")
  );

  const handleRows = useDelayUpdate(
    (rowsData) => setRows(rowsData),
    (rowsData) => onChangeOption(rowsData, "rows")
  );

  const toggleIsMaxLength = (e) => {
    onChangeOption(e, "isMaxLength");
  };

  const handleMaxLength = useDelayUpdate(
    (e) => setMaxLength(e.target.value),
    (e) => onChangeOption(e.target.value, "maxLength")
  );

  return (
    <ColumnWrapper>
      <GuideWrapper>
        <LabelInput
          placeholder="라벨을 입력해주세요."
          onChange={handleLabel}
          value={labelInput}
        />
        <RowButton onClick={() => handleRows(rows - 1)}>▲</RowButton>
        <RowButton onClick={() => handleRows(rows + 1)}>▼</RowButton>
      </GuideWrapper>
      <GuideButtonOption guide={guide} onChangeGuide={onChangeGuide} />
      <TextArea
        minRows={rows}
        placeholder="placeholder를 적어주세요"
        onChange={handlePlaceholder}
        value={placeholder}
      />
      <LengthCounterOption
        option={option?.isMaxLength || false}
        toggleIsMaxLength={toggleIsMaxLength}
        maxLength={maxLength}
        setMaxLength={handleMaxLength}
      />
    </ColumnWrapper>
  );
};
