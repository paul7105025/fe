import React from "react";

import { LengthCounter } from "../../LengthCounter";

import {
  useForceUpdate,
  useReset,
  useDelayUpdate,
  FormMetadata,
} from "@wrtn/core";

import { GuideWrapper, Label, ColumnWrapper, TextArea } from "./styles";
import { InputDescription } from "../../../styles";
import { FormProps } from "../types";

export const FormTextArea = ({
  label = "",
  value,
  guide,
  option,
  onChange,

  style = {},
}: FormProps<"textarea">) => {
  const [textValue, setTextValue] = React.useState(value);

  const handleChange = useDelayUpdate(
    (e) => setTextValue(e.target.value),
    (e) => onChange(e.target.value)
  );

  useReset("", value, setTextValue);
  useForceUpdate(value, textValue, setTextValue);

  React.useEffect(() => {
    if (option?.isMaxLength)
      setTextValue((c) =>
        c.length > option?.maxLength ? c.slice(0, option?.maxLength) : c
      );
  }, []);

  return (
    <ColumnWrapper>
      <GuideWrapper>
        <Label>{label}</Label>
      </GuideWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <TextArea
        minRows={option?.rows}
        placeholder={option?.placeholder}
        value={textValue}
        onChange={handleChange}
        maxLength={option?.isMaxLength ? option?.maxLength : 30000}
      />
      {option?.isMaxLength && (
        <LengthCounter
          currentLength={textValue.length}
          maxLength={option?.maxLength}
        />
      )}
    </ColumnWrapper>
  );
};
