import React from "react";

import { useForceUpdate, useReset, useDelayUpdate } from "@wrtn/core";

import { ModalPositionPortal } from "../../ModalPortal/ModalPortal";

import {
  GuideWrapper,
  InputLabel,
  SelectIcon,
  SelectInputWrapper,
  SelectOption,
  SelectOptionWrapper,
  SelectValue,
  SelectWrapper,
} from "./styles";
import { InputDescription } from "../../../styles";

export const FormSelect = ({
  label = "",
  value,
  guide,
  option,
  onChange,
  
}) => {
  const wrapperRef = React.useRef<HTMLDivElement>(null);

  const [isOpen, setIsOpen] = React.useState(false);
  const [selectValue, setSelectValue] = React.useState(value);

  const handleChange = useDelayUpdate(
    (e) => setSelectValue(e),
    (e) => onChange(e)
  );

  useReset("", value, setSelectValue);
  useForceUpdate(value, selectValue, setSelectValue);

  const onClickSelect = (select) => {
    handleChange(select);
    setIsOpen(!isOpen);
  };

  return (
    <SelectWrapper>
      <GuideWrapper>
        <InputLabel>{label}</InputLabel>
      </GuideWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <SelectInputWrapper onClick={() => setIsOpen((o) => !o)} ref={wrapperRef}>
        <SelectValue
          placeholder="스타일을 선택해 주세요."
          value={selectValue}
          disabled
        />
        <SelectIcon open={isOpen} />
      </SelectInputWrapper>
      {isOpen && (
        <ModalPositionPortal
          position={{
            top:
              wrapperRef.current &&
              wrapperRef.current.getBoundingClientRect().bottom - 5,
            left:
              wrapperRef.current &&
              wrapperRef.current.getBoundingClientRect().left,
          }}
        >
          <SelectOptionWrapper
            style={{
              minWidth: wrapperRef.current
                ? wrapperRef.current.getBoundingClientRect().width
                : 0,
            }}
          >
            {option?.selects?.map((select) => (
              <SelectOption key={select} onClick={() => onClickSelect(select)}>
                {select}
              </SelectOption>
            ))}
          </SelectOptionWrapper>
        </ModalPositionPortal>
      )}
    </SelectWrapper>
  );
};

export default FormSelect;
