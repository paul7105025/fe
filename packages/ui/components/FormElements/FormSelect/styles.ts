import styled from "styled-components";

import { colors, FlexButton, FlexWrapper, FormLabel, typo } from "../../../styles";

import { Down } from "../../../assets";

export const SelectWrapper = styled(FlexWrapper)`
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  width: 100%;

  position: relative;
`;

export const GuideWrapper = styled(FlexWrapper)`
  gap: 5px;
`;

export const InputLabel = styled.p`
  ${FormLabel}
`;

export const SelectInputWrapper = styled(FlexWrapper)`
  width: 100%;

  padding: 8px 16px;

  justify-content: space-between;

  border: 1px solid ${colors.GRAY_55};
  border-radius: 8px;

  cursor: pointer;
`;

export const SelectValue = styled.input`
  background-color: transparent;

  ${typo({
    size: "16px",
    height: "162%",
    color: colors.GRAY_80,
  })}

  ::placeholder {
    ${typo({
      size: "16px",
      height: "162%",
      color: colors.GRAY_60,
    })}
  }
`;

export const SelectIcon = styled(Down)<{ open: boolean }>`
  ${({ open }) =>
    open
      ? `
        transform: rotate(180deg);
    `
      : ``}
`;

export const SelectOptionWrapper = styled(FlexWrapper)`
  border: 1px solid ${colors.GRAY_55};
  border-top: 0px;
  border-radius: 0px 0px 8px 8px;

  flex-direction: column;

  background-color: ${colors.WHITE};
`;

export const SelectOption = styled(FlexButton)`
  padding: 8px 16px;
  width: 100%;

  border-radius: 8px;

  cursor: pointer;

  ${typo({
    size: "16px",
    height: "162%",
    color: colors.GRAY_90,
  })}

  :hover {
    background-color: ${colors.GRAY_10};
  }
`;
