import styled, { css } from "styled-components";

import {
  FlexWrapper,
  typo,
  colors,
  FormLabel,
  FlexButton,
  InputStyle,
} from "../../../styles";

export const Wrapper = styled(FlexWrapper)`
  gap: 20px;
`;

export const ColumnWrapper = styled(Wrapper)`
  flex-direction: column;
  gap: 16px;
  width: 100%;
  align-items: flex-start;
`;

export const GuideWrapper = styled(FlexWrapper)`
  gap: 5px;
`;

export const Label = styled.p`
  ${FormLabel}
`;

export const OptionChipsLabelInput = styled.input`
  ${FormLabel}
`;

export const ChipWrapper = styled(FlexWrapper)`
  gap: 10px;
  flex-wrap: wrap;
  justify-content: flex-start;
`;

export const ChipInputLabel = styled(FlexWrapper)`
  gap: 10px;
`;

export const RadioName = styled.p``;

export const ChipNameInput = styled.input`
  width: 50px;
  background-color: ${colors.gray_40};
`;

export const OptionButton = styled.button`
  width: 100%;
  background-color: #c4c9d7;
  :hover {
    background-color: #b3b9c9;
  }
`;

export const RadioChip = styled(FlexButton)<{
  light?: boolean;
  selected?: boolean;
}>`
  ${typo({
    size: "16px",
    weight: 500,
    color: colors.GRAY_90,
    height: "26px",
  })};

  cursor: pointer;
  border-radius: 30px;
  padding: 4px 22px;

  background-color: ${({ selected }) =>
    selected ? colors.LIGHT_BLUE : colors.WHITE};
  border: 1px solid ${colors.BLUE_GRAY_LINE};

  &:hover {
    background-color: ${({ selected }) =>
      selected ? colors.LIGHT_BLUE : colors.LIGHT_BLUE};
  }

  ${({ light }) =>
    light &&
    css`
      padding: 10px 26px;
      gap: 5px;
      flex-direction: column;
    `}
`;
