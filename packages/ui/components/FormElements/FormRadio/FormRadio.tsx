import React from "react";
import { GuideButton } from "../../GuideButton";

import { useForceUpdate, useReset } from "@wrtn/core";

import {
  ChipWrapper,
  ColumnWrapper,
  Label,
  RadioChip,
  RadioName,
  GuideWrapper,
} from "./styles";
import { InputDescription } from "../../../styles";
import { FormProps } from "../types";

export const FormRadio = ({
  label = "",
  value = "",
  guide,
  option,
  onChange,

  light = false,
}: FormProps<"option_chips">) => {
  const [selectedChip, setSelectedChip] = React.useState(value);

  const handleClick = (val) => {
    setSelectedChip(val);
    onChange(val);
  };

  useReset("", value, setSelectedChip);
  useForceUpdate(value, selectedChip, setSelectedChip);

  return (
    <ColumnWrapper>
      <GuideWrapper>
        <Label>{label}</Label>
      </GuideWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <ChipWrapper>
        {option?.chips?.map((chip) => (
          <RadioChip
            key={chip.value}
            selected={selectedChip === chip.value}
            onClick={() => handleClick(chip.value)}
          >
            <RadioName>{light ? chip?.label : chip.value}</RadioName>
          </RadioChip>
        ))}
      </ChipWrapper>
    </ColumnWrapper>
  );
};
