import React from "react";
import { nanoid } from "nanoid";

import { useDelayUpdate } from "@wrtn/core";

import { GuideButtonOption } from "../../GuideButton";
import {
  ChipNameInput,
  ChipWrapper,
  ColumnWrapper,
  GuideWrapper,
  Label,
  OptionButton,
  OptionChipsLabelInput,
  RadioChip,
} from "./styles";

export const FormRadioOption = ({
  guide,
  label = "",
  option,
  onChangeLabel,
  onChangeOption,
  onChangeGuide,
  light = false,
}) => {
  const [labelInput, setLabelInput] = React.useState(label);
  const [chipOption, setChipOption] = React.useState(option?.chips || []);

  const labelInputChanged = useDelayUpdate(
    (e) => setLabelInput(e.target.value),
    (e) => onChangeLabel(e.target.value)
  );

  const chipChanges = useDelayUpdate(setChipOption, (newChips) =>
    onChangeOption(newChips, "chips")
  );

  const increaseOption = () => {
    const newChips = [
      ...chipOption,
      {
        key: nanoid(),
        value: `option ${chipOption.length + 1}`,
      },
    ];
    chipChanges(newChips);
  };

  const decreaseOption = () => {
    const newOption = chipOption.slice(0, -1);
    chipChanges(newOption);
  };

  const changeOptionLabel = (e, index) => {
    const newOption = [...chipOption];
    newOption[index] = {
      ...newOption[index],
      label: e.target.value,
    };
    chipChanges(newOption);
  };

  const changeOptionValue = (e, index) => {
    const newOption = [...chipOption];
    newOption[index] = {
      ...newOption[index],
      value: e.target.value,
    };
    chipChanges(newOption);
  };

  return (
    <ColumnWrapper>
      <GuideWrapper>
        <Label>
          <OptionChipsLabelInput
            placeholder="라벨을 입력해주세요."
            value={labelInput}
            onChange={labelInputChanged}
          />
        </Label>
      </GuideWrapper>
      <GuideButtonOption guide={guide} onChangeGuide={onChangeGuide} />
      <OptionButton onClick={decreaseOption}>-</OptionButton>
      <ChipWrapper>
        {chipOption.map((option, index) => (
          <RadioChip light={light} key={option.key}>
            {light && (
              <ChipNameInput
                placeholder="라벨"
                value={option.label}
                onChange={(e) => changeOptionLabel(e, index)}
              />
            )}
            <ChipNameInput
              placeholder="값"
              value={option.value}
              onChange={(e) => changeOptionValue(e, index)}
            />
          </RadioChip>
        ))}
      </ChipWrapper>
      <OptionButton onClick={increaseOption}>+</OptionButton>
    </ColumnWrapper>
  );
};
