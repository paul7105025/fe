import {
  FormArrayType,
  FormMetadata,
  FormNumberType,
  FormStringType,
  FormType,
} from "@wrtn/core";

type onChangeType<T extends FormType> = T extends FormStringType
  ? (value: string) => void
  : T extends FormNumberType
  ? (value: number) => void
  : T extends FormArrayType
  ? (value: string[], index: number) => void
  : never;

type onPlusType = (value: string) => void;

export interface FormProps<T extends FormType> extends FormMetadata<T> {
  onChange: onChangeType<T>;
  style?: React.CSSProperties;
  disabled?: boolean;
  light?: boolean;

  onPlus?: onPlusType;
  onMinus?: (index: number) => void;
}
