import styled from "styled-components";

import { BlockWrapper, FlexWrapper, colors } from "../../../styles";

import { ReactComponent as PlusIcon } from "../../../assets/icons/icon_plus.svg";
import { ReactComponent as MinusIcon } from "../../../assets/icons/icon_minus.svg";

export const Wrapper = styled(BlockWrapper)``;

export const IconPlus = styled(PlusIcon)`
  width: 15px;
  height: 15px;
  cursor: pointer;
`;

export const IconMinus = styled(MinusIcon)`
  width: 15px;
  height: 15px;
  position: absolute;
  right: 10px;
  top: 14px;
  cursor: pointer;
`;

export const Input = styled.input`
  background: #fff;
  width: 100%;
  padding: 4px 9px;
  border: 1px solid #c4c9d7;
  border-radius: 5px;

  margin-bottom: 10px;
  height: 42px;
`;

export const LabelWrapper = styled(FlexWrapper)`
  justify-content: flex-start;

  margin-bottom: 18px;
  gap: 10px;
  padding-right: 10px;
`;

export const LabelInput = styled.input`
  color: ${colors.gray_80};
`;

export const InputWrapper = styled(FlexWrapper)`
  position: relative;
`;

export const FormInputExpandableWrapper = styled(BlockWrapper)`
  width: 100%;
`;

export const GuideWrapper = styled(FlexWrapper)`
  flex-direction: row;
  gap: 5px;
`;
