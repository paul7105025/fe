import React from "react";

import { useDelayUpdate } from "@wrtn/core";

import {
  GuideWrapper,
  IconMinus,
  IconPlus,
  Input,
  FormInputExpandableWrapper,
  InputWrapper,
  LabelInput,
  LabelWrapper,
} from "./styles";
import { InputDescription } from "../../../styles";
import { FormProps } from "../types";

export const FormInputExpandable = ({
  label = "",
  value = [""],
  guide,
  option,
  onChange,
  onPlus = () => {},
  onMinus = () => {},
}: FormProps<"input_expandable">) => {
  const [valueArray, setValueArray] = React.useState(value);

  const handleChange = useDelayUpdate(
    (e, index) =>
      setValueArray([
        ...valueArray.slice(0, index),
        e.target.value,
        ...valueArray.slice(index + 1),
      ]),
    (e, index) => onChange(e.target.value, index)
  );

  const handlePlus = useDelayUpdate(
    () => setValueArray([...valueArray, ""]),
    () => onPlus(""),
    200
  );

  const handleMinus = useDelayUpdate(
    (index) =>
      setValueArray([
        ...valueArray.slice(0, index),
        ...valueArray.slice(index + 1),
      ]),
    (index) => onMinus(index),
    200
  );

  return (
    <FormInputExpandableWrapper>
      <LabelWrapper>
        <GuideWrapper>
          <LabelInput value={label} readOnly />
        </GuideWrapper>
        <div style={{ flex: 1 }} />
        <IconPlus onClick={handlePlus} />
      </LabelWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      {valueArray.map((item, index) => (
        <InputWrapper key={index}>
          <Input
            placeholder={option?.placeholder[index]?.text || ""}
            value={item}
            onChange={(e) => handleChange(e, index)}
          />
          {index !== 0 && <IconMinus onClick={() => handleMinus(index)} />}
        </InputWrapper>
      ))}
    </FormInputExpandableWrapper>
  );
};
