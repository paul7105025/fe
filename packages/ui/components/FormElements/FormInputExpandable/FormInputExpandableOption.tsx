import React from "react";
import { nanoid } from "nanoid";

import { GuideButtonOption } from "../../GuideButton";

import { useDelayUpdate } from "@wrtn/core";

import {
  Wrapper,
  GuideWrapper,
  IconMinus,
  IconPlus,
  Input,
  InputWrapper,
  LabelInput,
  LabelWrapper,
} from "./styles";

export const FormInputExpandableOption = ({
  guide,
  label = "",
  option,
  onChangeLabel,
  onChangeOption,
  onPlusValueAndOption,
  onMinusValueAndOption,
  onChangeGuide,
}) => {
  const [labelInput, setLabelInput] = React.useState(label);
  const [placeholder, setPlaceholder] = React.useState(
    option?.placeholder || ""
  );

  const handleLabel = useDelayUpdate(
    (e) => setLabelInput(e.target.value),
    (e) => onChangeLabel(e.target.value)
  );

  const handlePlaceholder = useDelayUpdate(
    (e, index) =>
      setPlaceholder([
        ...placeholder.slice(0, index),
        {
          ...placeholder[index],
          text: e.target.value,
        },
        ...placeholder.slice(index + 1),
      ]),
    (e, index) =>
      onChangeOption(
        [
          ...option.placeholder.slice(0, index),
          {
            ...option.placeholder[index],
            text: e.target.value,
          },
          ...option.placeholder.slice(index + 1),
        ],
        "placeholder"
      )
  );

  const handlePlus = useDelayUpdate(
    () => setPlaceholder([...placeholder, { text: "", key: nanoid() }]),
    () =>
      onPlusValueAndOption(
        [...option.placeholder, { text: "", key: nanoid() }],
        "placeholder"
      ),
    200
  );

  const handleMinus = useDelayUpdate(
    (index) =>
      setPlaceholder([
        ...placeholder.slice(0, index),
        ...placeholder.slice(index + 1),
      ]),
    (index) => onMinusValueAndOption(index, "placeholder"),
    200
  );

  return (
    <Wrapper>
      <LabelWrapper>
        <GuideWrapper>
          <LabelInput
            placeholder="제목을 입력해주세요."
            onChange={handleLabel}
            value={labelInput}
          />
        </GuideWrapper>
        <IconPlus onClick={handlePlus} />
      </LabelWrapper>
      <GuideButtonOption guide={guide} onChangeGuide={onChangeGuide} />
      {placeholder.map((item, index) => (
        <InputWrapper key={item.key}>
          <Input
            placeholder="placeholder를 입력해주세요."
            value={item.text}
            onChange={(e) => handlePlaceholder(e, index)}
          />
          {index !== 0 && <IconMinus onClick={() => handleMinus(index)} />}
        </InputWrapper>
      ))}
    </Wrapper>
  );
};
