import styled from "styled-components";

import { colors, FlexButton, FlexWrapper, FormLabel, typo } from "../../../styles";

export const SelectWrapper = styled(FlexWrapper)`
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  width: 100%;

  position: relative;
`;

export const GuideWrapper = styled(FlexWrapper)`
  gap: 5px;
`;

export const InputLabel = styled.p`
  ${FormLabel}
`;

export const SelectCardsWrapper = styled(FlexWrapper)`
  justify-content: flex-start;
  flex-wrap: wrap;
  gap: 8px;
`;

export const SelectCard = styled(FlexButton)<{ selected: boolean }>`
  flex-direction: column;

  cursor: pointer;

  border: 1px solid ${colors.GRAY_55};
  border-radius: 8px;

  width: 120px;

  overflow: hidden;

  ${({ selected }) =>
    selected &&
    `
    border: 1px solid ${colors.ACTION_BLUE};
    background-color: ${colors.BACKGROUND};
  `}

  @media (max-width: 767px) {
    max-width: 27vw;
    min-width: 80px;
  }

  &:hover {
    border: 1px solid ${colors.ACTION_BLUE};
    background-color: ${colors.BACKGROUND};
  }
`;

export const SelectImage = styled.img`
  width: 100%;
  /* height: 100px; */
`;

export const SelectTextWrapper = styled.div`
  width: 100%;
  padding: 8px 12px;

  text-align: left;

  ${typo({
    size: "14px",
    color: colors.GRAY_80,
  })}
`;
