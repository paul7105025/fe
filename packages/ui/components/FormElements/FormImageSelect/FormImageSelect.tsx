import React from "react";

import { useForceUpdate, useReset, useDelayUpdate } from "@wrtn/core";

import {
  GuideWrapper,
  InputLabel,
  SelectCard,
  SelectCardsWrapper,
  SelectImage,
  SelectTextWrapper,
  SelectWrapper,
} from "./styles";
import { InputDescription } from "../../../styles";
import { FormProps } from "../types";

export const FormImageSelect = ({
  label = "",
  value,
  guide,
  option,
  onChange,
}: FormProps<"select_image">) => {
  const [selectValue, setSelectValue] = React.useState(value);

  const handleChange = useDelayUpdate(
    (e) => setSelectValue(e),
    (e) => onChange(e)
  );

  useReset("", value, setSelectValue);
  useForceUpdate(value, selectValue, setSelectValue);

  const onClickSelect = (select) => {
    handleChange(select);
  };

  return (
    <SelectWrapper>
      <GuideWrapper>
        <InputLabel>{label}</InputLabel>
      </GuideWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <SelectCardsWrapper>
        {option.cards.map((card, index) => (
          <SelectCard
            key={card.text}
            onClick={() => onClickSelect(card.text)}
            selected={selectValue === card.text}
          >
            <SelectImage src={card.image} />
            <SelectTextWrapper>{card.text}</SelectTextWrapper>
          </SelectCard>
        ))}
      </SelectCardsWrapper>
    </SelectWrapper>
  );
};
