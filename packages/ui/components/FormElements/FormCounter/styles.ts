import styled from "styled-components";

import { typo, colors, FlexWrapper, FlexButton, FormLabel } from "../../../styles";
import { Down } from "../../../assets/SVGComponent/Icon";

export const InputWrapper = styled(FlexWrapper)`
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  width: 100%;
`;

export const InputLabel = styled.p`
  ${FormLabel}
`;

export const InputLabelInput = styled.input`
  ${FormLabel}
`;

export const GuideWrapper = styled(FlexWrapper)`
  gap: 5px;
`;

export const CounterWrapper = styled(FlexWrapper)`
  min-width: 118px;

  justify-content: space-between;
  gap: 9px;

  background: #ffffff;
  border-radius: 5px;

  padding: 12px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
`;

export const CounterContainer = styled(FlexWrapper)`
  width: 100%;
  justify-content: space-between;
  gap: 6px;
`;

export const Label = styled.p`
  ${FormLabel}
`;

export const Number = styled.p`
  ${typo({
    weight: "700",
    size: "16px",
    height: "19px",
    color: colors.gray_90,
  })};
  min-width: 20px;
  text-align: center;
`;

export const IconButton = styled(FlexButton)<{
  disable?: boolean;
}>`
  min-width: 16px;
  height: 20px;
  cursor: pointer;
  ${({ disable }) => disable && `pointer-events: none;`}
  svg {
    path {
      fill: ${({ disable }) => (disable ? colors.GRAY_55 : colors.GRAY_70)};
    }
  }
`;

export const DownIcon = styled(Down)``;

export const UpIcon = styled(DownIcon)`
  transform: rotate(180deg);
`;
