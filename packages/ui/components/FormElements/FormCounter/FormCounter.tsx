import React from "react";

import { useForceUpdate, useReset } from "@wrtn/core";

import {
  GuideWrapper,
  InputLabel,
  InputWrapper,
  CounterContainer,
  IconButton,
  DownIcon,
  UpIcon,
  Number,
  CounterWrapper,
} from "./styles";
import { InputDescription } from "../../../styles";
import { FormProps } from "../types";

export const FormCounter = ({
  label = "",
  value,
  guide,
  option,
  onChange,
}: FormProps<"counter">) => {
  const [countValue, setCountValue] = React.useState(
    value || option.defaultValue
  );

  const handleIncrease = () => {
    if (option.maxCount && countValue < option.maxCount) {
      setCountValue(countValue + 1);
      onChange(countValue + 1);
    }
  };

  const handleDecrease = () => {
    if (countValue > 1) {
      setCountValue(countValue - 1);
      onChange(countValue - 1);
    }
  };

  useReset(option.defaultValue, value, setCountValue);
  useForceUpdate(value, countValue, setCountValue);

  return (
    <InputWrapper>
      <GuideWrapper>
        <InputLabel>{label}</InputLabel>
      </GuideWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <CounterWrapper>
        <CounterContainer>
          <IconButton onClick={handleDecrease} disable={countValue < 2}>
            <DownIcon />
          </IconButton>
          <Number>
            {countValue} {option.unit}
          </Number>
          <IconButton
            onClick={handleIncrease}
            disable={option.maxCount && countValue >= option.maxCount}
          >
            <UpIcon />
          </IconButton>
        </CounterContainer>
      </CounterWrapper>
    </InputWrapper>
  );
};
