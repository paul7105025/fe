import React from "react";

import { useDelayUpdate } from "@wrtn/core";

import { GuideButtonOption } from "../../GuideButton";
import {
  GuideWrapper,
  InputLabel,
  InputLabelInput,
  InputWrapper,
  Number,
  CounterWrapper,
  CounterContainer,
  IconButton,
  DownIcon,
  UpIcon,
} from "./styles";

export const FormCounterOption = ({
  guide,
  label = "",
  option,
  onChangeLabel,
  onChangeOption,
  onChangeGuide,
}) => {
  const [labelInput, setLabelInput] = React.useState(label);

  const [unitInput, setUnitInput] = React.useState(option?.unit || "");
  const [defaultValueInput, setDefaultValueInput] = React.useState(
    option?.defaultValue || 1
  );
  const [maxCountInput, setMaxCountInput] = React.useState(
    option?.maxCount || 5
  );

  const labelInputChanged = useDelayUpdate(
    (e) => setLabelInput(e.target.value),
    (e) => onChangeLabel(e.target.value)
  );

  const unitInputChanged = useDelayUpdate(
    (e) => setUnitInput(e.target.value),
    (e) => onChangeOption(e.target.value, "unit")
  );

  const defaultValueInputChanged = (e) => {
    setDefaultValueInput(e);
    onChangeOption(e, "defaultValue");
  };

  const maxCountInputChanged = (e) => {
    setMaxCountInput(e.target.value);
    onChangeOption(e.target.value, "maxCount");
  };

  const handleIncrease = () => {
    if (defaultValueInput < maxCountInput) {
      defaultValueInputChanged(defaultValueInput + 1);
    }
  };

  const handleDecrease = () => {
    if (defaultValueInput > 1) {
      defaultValueInputChanged(defaultValueInput - 1);
    }
  };

  React.useEffect(() => {
    if (maxCountInput < 1) {
      setMaxCountInput(1);
    }

    if (defaultValueInput > maxCountInput) {
      setDefaultValueInput(maxCountInput);
    }

    if (defaultValueInput < 1) {
      setDefaultValueInput(1);
    }
  }, [defaultValueInput, maxCountInput]);

  return (
    <InputWrapper>
      <GuideWrapper>
        <InputLabel>
          <InputLabelInput
            placeholder="라벨을 입력해주세요."
            value={labelInput}
            onChange={labelInputChanged}
          />
        </InputLabel>
      </GuideWrapper>
      <GuideButtonOption guide={guide} onChangeGuide={onChangeGuide} />
      <GuideWrapper>
        <InputLabel>
          <InputLabelInput
            placeholder="최대 수량을 입력해주세요."
            value={maxCountInput}
            onChange={maxCountInputChanged}
            type="number"
            min={1}
          />
        </InputLabel>
      </GuideWrapper>
      <CounterWrapper>
        <CounterContainer>
          <IconButton onClick={handleDecrease} disable={defaultValueInput < 2}>
            <DownIcon />
          </IconButton>
          <Number>{defaultValueInput} </Number>
          <InputLabelInput
            placeholder="단위"
            value={unitInput}
            onChange={unitInputChanged}
          />
          <IconButton
            onClick={handleIncrease}
            disable={defaultValueInput >= maxCountInput}
          >
            <UpIcon />
          </IconButton>
        </CounterContainer>
      </CounterWrapper>
    </InputWrapper>
  );
};
