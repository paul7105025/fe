import React from "react";
import styled from "styled-components";

import { useDelayUpdate } from "@wrtn/core";

import { GuideButtonOption } from "../../GuideButton";
import { FlexWrapper } from "../../../styles";

export const FormCheckboxOption = ({
  guide,
  label = "",
  onChangeLabel,
  onChangeGuide,
}) => {
  const [labelInput, setLabelInput] = React.useState(label);

  const onChangeLabelInput = useDelayUpdate(
    (e) => setLabelInput(e.target.value),
    (e) => onChangeLabel(e.target.value)
  );

  return (
    <FormCheckboxWrapper>
      <input type="checkbox" />
      <input
        value={labelInput}
        onChange={onChangeLabelInput}
        placeholder="체크박스 이름"
      />
      <GuideButtonOption guide={guide} onChangeGuide={onChangeGuide} />
    </FormCheckboxWrapper>
  );
};

const FormCheckboxWrapper = styled(FlexWrapper)`
  gap: 9px;
  justify-content: flex-start;
`;
