import React from "react";
import styled from "styled-components";

import { FlexWrapper, typo } from "../../../styles";
import { FormProps } from "../types";

export const FormCheckbox = ({
  label = "",
  value = "",
  guide,
  onChange,
  disabled,
}: FormProps<"checkbox">) => {
  const [checked, setChecked] = React.useState(value.length > 0);

  const onChangeChecked = (e) => {
    if (e.target.checked) {
      setChecked(true);
      onChange(label);
    } else {
      setChecked(false);
      onChange("");
    }
  };

  return (
    <FormCheckboxWrapper>
      <input
        checked={checked}
        onChange={onChangeChecked}
        disabled={disabled}
        type="checkbox"
      />
      <InputLabelText>{label}</InputLabelText>
    </FormCheckboxWrapper>
  );
};

const FormCheckboxWrapper = styled(FlexWrapper)`
  gap: 9px;
`;

const InputLabelText = styled.span`
  ${typo({ weight: "medium", size: "small", color: "black" })};
`;
