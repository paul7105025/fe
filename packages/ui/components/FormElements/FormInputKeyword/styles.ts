import styled from "styled-components";
import {
  FormWrapper,
  FormLabel,
  InputStyle,
  LabelWrapper,
  FlexWrapper,
  colors,
  TipText,
} from "../../../styles";

export const InputWrapper = styled(FormWrapper)``;

export const SomeWrapper = styled(FlexWrapper)`
  gap: 6px;
  justify-content: flex-start;
`;

export const InputLabel = styled.p`
  ${FormLabel}
  white-space: nowrap;
`;

export { TipText, LabelWrapper };

export const InputLabelInput = styled.input`
  width: 70%;
  padding: 4px 9px;
  border: 1px solid #c4c9d7;
  border-radius: 5px;
`;

export const Input = styled.input`
  ${InputStyle}
`;

export const ChipWrapper = styled(FlexWrapper)`
  width: 100%;
  flex-wrap: wrap;
  justify-content: flex-start;
  gap: 12px 4px;
  margin-top: 4px;
`;

export const Chip = styled.button`
  display: flex;
  align-items: center;
  cursor: pointer;

  background-color: ${colors.WHITE};

  padding: 4px 12px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 30px;
  gap: 4px;

  word-wrap: break-word;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;

  svg {
    width: 18px;
    height: 18px;
    flex: 0 0 18px;
  }

  p {
    font-weight: 500;
    font-size: 16px;
    line-height: 26px;
  }

  &:hover {
    background-color: ${colors.LIGHT_BLUE};
  }
`;
