import React from "react";
import { Close } from "../../../assets";

import { useForceUpdate, useReset, useDelayUpdate } from "@wrtn/core";

import {
  LabelWrapper,
  Input,
  InputLabel,
  InputWrapper,
  ChipWrapper,
  Chip,
  TipText,
  SomeWrapper,
} from "./styles";
import { InputDescription } from "../../../styles";
import { FormProps } from "../types";

export const FormInputKeyword = ({
  label = "",
  value,
  guide,
  option,
  onPlus = () => {},
  onMinus = () => {},
}: FormProps<"input_keyword">) => {
  const [inputValue, setInputValue] = React.useState("");
  const [values, setValues] = React.useState(value || []);

  const onChangeInputValue = React.useCallback((e) => {
    setInputValue(e.target.value);
  }, []);

  const handlePlus = useDelayUpdate(
    (chip) => setValues([...values, chip]),
    (chip) => onPlus(chip),
    100
  );

  const handleDelete = useDelayUpdate(
    (index) =>
      setValues([...values.slice(0, index), ...values.slice(index + 1)]),
    (index) => onMinus(index),
    100
  );

  const onEnterKeyDown = React.useCallback(
    (e) => {
      if (e.key === "Enter") {
        if (e.nativeEvent.isComposing === false && inputValue.length > 0) {
          e.preventDefault();
          if (value.includes(inputValue)) {
            setInputValue("");
            return;
          }
          handlePlus(inputValue);
          setInputValue("");
        }
      }
    },
    [inputValue, values]
  );

  const onResetInnerValues = React.useCallback((resetValue) => {
    setInputValue("");
    setValues(resetValue);
  }, []);

  useReset([], value, onResetInnerValues);
  useForceUpdate(value, values, setValues);

  return (
    <InputWrapper>
      <LabelWrapper>
        <SomeWrapper>
          <InputLabel>{label}</InputLabel>
        </SomeWrapper>
        <TipText>※ 키워드를 한 개씩 작성 한 후 Enter를 눌러주세요.</TipText>
      </LabelWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <Input
        maxLength={option?.maxLength}
        placeholder={option?.placeholder || ""}
        value={inputValue}
        onChange={onChangeInputValue}
        onKeyDownCapture={onEnterKeyDown}
      />
      <ChipWrapper>
        {Array.isArray(values) &&
          values.map((v, i) => (
            <Chip key={v} onClick={() => handleDelete(i)}>
              <p>{v}</p>
              <Close />
            </Chip>
          ))}
      </ChipWrapper>
    </InputWrapper>
  );
};
