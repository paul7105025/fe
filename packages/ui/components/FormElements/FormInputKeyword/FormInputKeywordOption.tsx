import React from "react";

import { GuideButtonOption } from "../../GuideButton";

import { useDelayUpdate } from "@wrtn/core";

import {
  LabelWrapper,
  Input,
  InputLabel,
  InputLabelInput,
  InputWrapper,
} from "./styles";

export const FormInputKeywordOption = ({
  guide,
  label = "",
  option,
  onChangeLabel,
  onChangeOption,
  onChangeGuide,
}) => {
  const [labelInput, setLabelInput] = React.useState(label);
  const [placeholderInput, setPlaceholderInput] = React.useState(
    option?.placeholder || ""
  );

  const labelInputChanged = useDelayUpdate(
    (e) => setLabelInput(e.target.value),
    (e) => onChangeLabel(e.target.value)
  );

  const placeholderInputChanged = useDelayUpdate(
    (e) => setPlaceholderInput(e.target.value),
    (e) => onChangeOption(e.target.value, "placeholder")
  );

  return (
    <InputWrapper>
      <LabelWrapper>
        <InputLabel>
          라벨 :{" "}
          <InputLabelInput value={labelInput} onChange={labelInputChanged} />
        </InputLabel>
      </LabelWrapper>
      <GuideButtonOption guide={guide} onChangeGuide={onChangeGuide} />
      <Input
        value={placeholderInput}
        onChange={placeholderInputChanged}
        placeholder="placeholder를 입력해주세요."
      />
    </InputWrapper>
  );
};
