import React from "react";

import { useForceUpdate, useReset, useDelayUpdate } from "@wrtn/core";

import {
  GuideWrapper,
  InnerWrapper,
  Input,
  InputBox,
  InputLabel,
  InputWrapper,
} from "./styles";
import { GuideButton } from "../../GuideButton";
import { LengthCounter } from "../../LengthCounter";
import { InputDescription } from "../../../styles";

export const FormInputDivided = ({
  label = "",
  value,
  guide,
  option,
  onChange,
  
}) => {
  const [valueArray, setValueArray] = React.useState(value);

  const handleChange = useDelayUpdate(
    (e, index) =>
      setValueArray((res) => [
        ...res.slice(0, index),
        e.target.value,
        ...res.slice(index + 1),
      ]),
    (e, index) => onChange(e.target.value, index)
  );

  useReset([], value, setValueArray);
  useForceUpdate(value, valueArray, setValueArray);

  return (
    <InputWrapper>
      <GuideWrapper>
        <InputLabel>{label}</InputLabel>
      </GuideWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <InnerWrapper>
        {option?.inputs?.map((v, i) => (
          <React.Fragment key={v?.placeholder}>
            <InputBox>
              <Input
                placeholder={v?.placeholder}
                maxLength={v?.isMaxLength ? v?.maxLength : 10}
                value={valueArray[i]}
                onChange={(e) => handleChange(e, i)}
              />
              {v?.isMaxLength && (
                <LengthCounter
                  currentLength={valueArray[i]?.length}
                  maxLength={v?.maxLength}
                />
              )}
            </InputBox>
            {i !== option?.inputs?.length - 1 && (
              <div style={{ marginRight: 16 }}>는</div>
            )}
          </React.Fragment>
        ))}
      </InnerWrapper>
    </InputWrapper>
  );
};
