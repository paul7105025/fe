import styled from "styled-components";

import { FlexWrapper, FormLabel, InputStyle } from "../../../styles";

export const InputWrapper = styled(FlexWrapper)`
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  width: 100%;

  position: relative;
`;

export const InputLabel = styled.p`
  ${FormLabel}
`;

export const InputLabelInput = styled.input`
  ${FormLabel}
`;

export const Input = styled.input`
  ${InputStyle}
`;

export const InnerWrapper = styled(FlexWrapper)`
  justify-content: flex-start;
  width: 100%;

  gap: 8px;
`;

export const InputBox = styled(FlexWrapper)`
  flex: 1;
  position: relative;
`;

export const GuideWrapper = styled(FlexWrapper)`
  gap: 5px;
`;
