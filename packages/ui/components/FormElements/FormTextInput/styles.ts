import styled from "styled-components";

import { FlexWrapper, FormLabel, InputStyle } from "../../../styles";

export const InputWrapper = styled(FlexWrapper)`
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  width: 100%;

  position: relative;
`;

export const InputLabel = styled.p`
  ${FormLabel}
`;

export const InputLabelInput = styled.input`
  ${FormLabel}
`;

export const Input = styled.input`
  ${InputStyle}
`;

export const GuideWrapper = styled(FlexWrapper)`
  gap: 5px;
`;
