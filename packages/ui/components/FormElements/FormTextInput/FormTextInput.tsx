import React from "react";

import { useForceUpdate, useReset, useDelayUpdate } from "@wrtn/core";

import { LengthCounter } from "../../LengthCounter";

import { GuideWrapper, InputLabel, InputWrapper, Input } from "./styles";
import { InputDescription } from "../../../styles";
import { FormProps } from "../types";

export const FormTextInput = ({
  label = "",
  value,
  guide,
  option,
  onChange,
}: FormProps<"input">) => {
  const [textValue, setTextValue] = React.useState(value);

  const handleChange = useDelayUpdate(
    (e) => setTextValue(e.target.value),
    (e) => onChange(e.target.value)
  );

  useReset("", value, setTextValue);
  useForceUpdate(value, textValue, setTextValue);

  React.useEffect(() => {
    if (option?.isMaxLength)
      setTextValue((c) =>
        c?.length > option?.maxLength ? c.slice(0, option?.maxLength) : c
      );
  }, []);

  return (
    <InputWrapper>
      <GuideWrapper>
        <InputLabel>{label}</InputLabel>
      </GuideWrapper>
      {guide.isVisible && <InputDescription>{guide.text}</InputDescription>}
      <Input
        placeholder={option?.placeholder}
        maxLength={option?.isMaxLength ? option?.maxLength : 30000}
        value={textValue}
        onChange={handleChange}
      />
      {option?.isMaxLength && (
        <LengthCounter
          currentLength={textValue?.length}
          maxLength={option?.maxLength}
        />
      )}
    </InputWrapper>
  );
};
