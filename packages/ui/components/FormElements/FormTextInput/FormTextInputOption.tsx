import React from "react";
import { useDelayUpdate } from "@wrtn/core";

import { GuideButtonOption } from "../../GuideButton";
import { LengthCounterOption } from "../../LengthCounter";

import {
  GuideWrapper,
  InputLabel,
  InputLabelInput,
  InputWrapper,
  Input,
} from "./styles";

export const FormTextInputOption = ({
  guide,
  label = "",
  option,
  onChangeLabel,
  onChangeOption,
  onChangeGuide,
}) => {
  const [labelInput, setLabelInput] = React.useState(label);
  const [placeholderInput, setPlaceholderInput] = React.useState(
    option?.placeholder || ""
  );

  const [maxLength, setMaxLength] = React.useState(option?.maxLength || 0);

  const labelInputChanged = useDelayUpdate(
    (e) => setLabelInput(e.target.value),
    (e) => onChangeLabel(e.target.value)
  );

  const placeholderInputChanged = useDelayUpdate(
    (e) => setPlaceholderInput(e.target.value),
    (e) => onChangeOption(e.target.value, "placeholder")
  );

  const toggleIsMaxLength = (e) => {
    onChangeOption(e, "isMaxLength");
  };

  const handleMaxLength = useDelayUpdate(
    (e) => setMaxLength(e.target.value),
    (e) => onChangeOption(e.target.value, "maxLength")
  );

  return (
    <InputWrapper>
      <GuideWrapper>
        <InputLabel>
          <InputLabelInput
            placeholder="라벨을 입력해주세요."
            value={labelInput}
            onChange={labelInputChanged}
          />
        </InputLabel>
      </GuideWrapper>
      <GuideButtonOption guide={guide} onChangeGuide={onChangeGuide} />
      <Input
        value={placeholderInput}
        onChange={placeholderInputChanged}
        placeholder={"placeholder를 입력해주세요."}
      />
      <LengthCounterOption
        option={option?.isMaxLength || false}
        toggleIsMaxLength={toggleIsMaxLength}
        maxLength={maxLength}
        setMaxLength={handleMaxLength}
      />
    </InputWrapper>
  );
};
