import React from "react";
import styled from "styled-components";

import { colors, FlexButton, typo } from "../../styles";
import { ReactComponent as Search } from "../../assets/icons/icon_search.svg";

interface SearchInputProps {
  placeholder: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  long?: boolean;
  style?: React.CSSProperties;
}

export const SearchInput = ({
  placeholder,
  value,
  onChange,
  onKeyDown,
  long = false,
  style,
}: SearchInputProps) => {
  const inputRef = React.useRef<HTMLInputElement>(null);

  return (
    <Wrapper
      long={long}
      htmlFor="search-input"
      onClick={() => inputRef.current?.focus()}
      style={style}
    >
      <SearchIcon long={long}>
        <Search />
      </SearchIcon>
      <Input
        id="search-input"
        ref={inputRef}
        placeholder={placeholder}
        spellCheck={false}
        value={value}
        onChange={onChange}
        onKeyDown={onKeyDown}
        long={long}
      />
    </Wrapper>
  );
};

const Wrapper = styled.label<{ long: boolean }>`
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 8px;
  display: flex;
  padding: 9px 12px 8px 12px;
  align-items: center;
  min-width: ${({ long }) => (long ? "444px" : "200px")};
  max-width: 100%;

  cursor: text;

  gap: 5px;
  background: white;

  @media (max-width: 480px) {
    min-width: ${({ long }) => (long ? "335px" : "160px")};
    max-width: 95vw;
  }
`;

const SearchIcon = styled(FlexButton)<{ long: boolean }>`
  width: ${({ long }) => (long ? "18px" : "14px")};
  height: ${({ long }) => (long ? "18px" : "14px")};

  svg {
    path {
      fill: ${colors.GRAY_55};
    }
  }
`;

const Input = styled.input<{
  long: boolean;
}>`
  border: none;
  background: white;
  width: 100%;

  ${(long) =>
    typo({
      size: long ? "14px" : "12px",
      height: long ? "14px" : "12px",
      color: colors.GRAY_90,
    })}

  &::placeholder {
    user-select: none;
    ${({ long }) =>
      typo({
        size: long ? "14px" : "12px",
        height: long ? "14px" : "12px",
        color: colors.GRAY_55,
      })}
  }
  &:focus {
    outline: none;
  }
`;
