import styled from "styled-components";

import {
  Close,
  ColoredSiren,
  Crying,
  Timer,
  PartyFace,
  IconExclamation,
  Surprise,
  CreditCard,
  Desktop,
  Laugh,
  Link,
  WasteBasket,
} from "../../assets/SVGComponent/Icon";

import { ImageTool } from "../../assets";

import { typo, colors, FlexButton, boxShadow } from "../../styles";
import { useKeyPress } from "@wrtn/core";

export const decideIcon = (iconType: string) => {
  switch (iconType) {
    case "report":
      return <ColoredSiren />;
    case "crying":
      return <Crying />;
    case "timer":
      return <Timer />;
    case "partyFace":
      return <PartyFace />;
    case "image":
      return <ImageTool />;
    case "!":
      return <IconExclamation />;
    case "surprise":
      return <Surprise />;
    case "creditCard":
      return <CreditCard />;
    case "desktop":
      return <Desktop />;
    case "laugh":
      return <Laugh />;
    case "link":
      return <Link />;
    case "waste":
      return <WasteBasket />;
    default:
      return <></>;
  }
};

interface DialogProps {
  iconType?: string;
  title?: string;
  description?: Array<string>;
  rightButtonLabel: string;

  handleClose?: () => void;
  handleRightButton: (() => void) | (() => Promise<void>);

  disableCancel?: boolean;
  width?: number;
  isList?: boolean;
  isSimpleButton?: boolean;
  isDisabled?: boolean;

  children?: React.ReactNode;
}

export const Dialog = ({
  iconType,
  title,
  description = [],
  rightButtonLabel,

  handleClose = () => {},
  handleRightButton,

  disableCancel = false,
  width = 395,
  isList,
  isSimpleButton,
  isDisabled,

  children,
}: DialogProps) => {
  useKeyPress("Escape", handleClose);

  return (
    <Wrapper width={width}>
      <CloseIcon onClick={handleClose} />
      {iconType && <IconWrapper>{decideIcon(iconType)}</IconWrapper>}
      <TopWrapper></TopWrapper>
      <BottomWrapper>
        <Title>{title}</Title>
        {isList ? (
          <List>
            {description.map((item, index) => (
              <ListItem key={index}>{item}</ListItem>
            ))}
          </List>
        ) : (
          description.map((item, index) => (
            <Description key={index}>{item}</Description>
          ))
        )}
        {children}
        <ButtonWrapper>
          {!disableCancel && (
            <CancelButton onClick={handleClose}>취소하기</CancelButton>
          )}
          {isSimpleButton ? (
            <SimpleRightButton
              disabled={isDisabled}
              onClick={handleRightButton}
            >
              {rightButtonLabel}
            </SimpleRightButton>
          ) : (
            <RightButton disabled={isDisabled} onClick={handleRightButton}>
              {rightButtonLabel}
            </RightButton>
          )}
        </ButtonWrapper>
      </BottomWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div<{ width: number }>`
  position: relative;
  width: ${({ width }) => width}px;
  border-radius: 20px;
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  background: ${colors.WHITE};

  ${boxShadow.dialog_shadow};
`;

const IconWrapper = styled(FlexButton)`
  position: absolute;
  top: 28px;
  left: 32px;
  background: ${colors.WHITE};

  width: 63px;
  height: 63px;

  border-radius: 50%;

  svg {
    width: 36px;
    height: 36px;
  }
`;

const CloseIcon = styled(Close)`
  position: absolute;
  top: 18px;
  right: 18px;

  width: 20px;
  height: 20px;

  cursor: pointer;
`;

const TopWrapper = styled.div`
  width: 100%;
  height: 57px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 20px 20px 0px 0px;
`;

const BottomWrapper = styled.div`
  padding: 32px 22px 18px;
`;

const Title = styled.p`
  ${typo({
    weight: 700,
    size: "18px",
    height: "140%",
    color: colors.gray_90,
  })}

  text-align: center;
  margin-bottom: 22px;
  white-space: pre-line;
`;

const List = styled.ul`
  white-space: pre-line;

  list-style-position: inside;
`;

const ListItem = styled.li`
  ${typo({
    weight: 500,
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};

  text-align: center;
  margin-bottom: 24px;
`;

const Description = styled.p`
  ${typo({
    weight: 500,
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};

  text-align: center;
`;

const ButtonWrapper = styled.div`
  display: flex;

  margin-top: 33px;
`;

const CancelButton = styled(FlexButton)`
  background: ${colors.WHITE};
  padding: 12px;

  flex: 1;
  border-radius: 5px;

  cursor: pointer;

  &:hover {
    background: ${colors.gray_30};
  }

  ${typo({
    weight: 500,
    size: "16px",
    height: "100%",
    color: colors.gray_80,
    isStatic: true,
  })};
`;

const RightButton = styled(FlexButton)`
  background: ${colors.LIGHT_BLUE};
  padding: 12px;

  flex: 1;
  border-radius: 5px;

  cursor: pointer;

  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: colors.ACTION_BLUE,
    isStatic: true,
  })};

  &:hover {
    background-color: ${colors.LIGHT_BLUE_10};
  }

  &:disabled {
    background-color: ${colors.WHITE};
    cursor: not-allowed;
    color: ${colors.GRAY_55};
  }
`;

const SimpleRightButton = styled(FlexButton)`
  /* background: ${colors.LIGHT_BLUE}; */
  padding: 12px;

  flex: 1;
  border-radius: 5px;

  cursor: pointer;

  ${typo({
    weight: 500,
    size: "16px",
    color: colors.GRAY_80,
    isStatic: true,
  })};

  &:hover {
    background-color: ${colors.gray_30};
  }

  &:disabled {
    background-color: ${colors.WHITE};
    cursor: not-allowed;
    color: ${colors.GRAY_55};
  }
`;
