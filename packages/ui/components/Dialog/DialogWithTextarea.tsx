import React from "react";
import styled from "styled-components";

import { useDelayUpdate } from "@wrtn/core";

import { typo, colors } from "../../styles";
import { Close } from "../../assets/SVGComponent/Icon";

interface DialogWithTextareaProps {
  handleClose: () => void;
  handleRightButton: () => void;
  disableRightButton: boolean;
  value?: string;
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
}

export const DialogWithTextarea: React.FC<DialogWithTextareaProps> = ({
  handleClose,
  handleRightButton,
  disableRightButton,
  value,
  onChange,
}) => {
  const [text, setText] = React.useState(value || "");

  const onChangeText = useDelayUpdate(
    (e) => setText(e.target.value),
    (e) => onChange(e)
  );

  return (
    <Wrapper>
      <CloseIcon onClick={handleClose} />
      <TopWrapper>
        <Title>결과물 신고하기</Title>
        <Description>
          이 결과물을 신고하려는 이유가 무엇인지 알려주세요.
        </Description>
      </TopWrapper>
      <BottomWrapper>
        <Textarea
          value={text}
          onChange={onChangeText}
          placeholder="신고하시는 이유를 작성해주세요."
        />
        <ButtonWrapper>
          <CancelButton onClick={handleClose}>
            <CancelButtonLabel>취소하기</CancelButtonLabel>
          </CancelButton>
          <ReportButton
            onClick={handleRightButton}
            disabled={disableRightButton}
          >
            <ReportButtonLabel disabled={disableRightButton}>
              신고하기
            </ReportButtonLabel>
          </ReportButton>
        </ButtonWrapper>
      </BottomWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;

  width: 560px;
  height: 303px;
  border-radius: 20px;

  background: ${colors.WHITE};
`;

const TopWrapper = styled.div`
  width: 100%;
  height: 107px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 20px 20px 0px 0px;
  padding: 33px 0px 0px 0px;
`;

const BottomWrapper = styled.div`
  padding: 23px 23px 18px 28px;
`;

const Textarea = styled.textarea`
  border: 1px solid #d8dde9;
  border-radius: 5px;

  width: 509px;
  height: 91px;

  padding: 19px 20px;

  resize: none;

  ${typo({
    weight: 500,
    size: "16px",
    height: "100%",
    color: colors.gray_100,
  })}

  ::placeholder {
    ${typo({
      weight: 500,
      size: "16px",
      height: "100%",
      color: colors.gray_55,
    })}
  }
`;

const ButtonWrapper = styled.div`
  display: flex;
  margin-top: 24px;
`;

const CloseIcon = styled(Close)`
  position: absolute;
  top: 18px;
  right: 18px;

  width: 20px;
  height: 20px;

  cursor: pointer;
`;

const Title = styled.p`
  ${typo({
    weight: 700,
    size: "18px",
    height: "100%",
    color: colors.gray_90,
  })}

  text-align: center;
  margin-bottom: 15px;
`;

const Description = styled.p`
  ${typo({
    weight: 500,
    size: "16px",
    height: "100%",
    color: colors.gray_70,
  })};

  text-align: center;
`;

const CancelButton = styled.button`
  background: ${colors.WHITE};
  height: 40px;
  flex: 1;
  border-radius: 5px;

  cursor: pointer;

  &:hover {
    background: ${colors.gray_10};
  }
`;

const CancelButtonLabel = styled.p`
  ${typo({
    weight: 500,
    size: "16px",
    height: "100%",
    color: colors.gray_80,
  })};
`;

const ReportButton = styled.button`
  background: ${colors.LIGHT_BLUE};
  height: 40px;
  flex: 1;
  border-radius: 5px;

  cursor: pointer;

  &:hover {
    background: ${colors.BLUE_GRAY_LINE};
  }

  ${({ disabled }) =>
    disabled &&
    `
    background: ${colors.WHITE};
    pointer-events: none;
    &:hover {
    background: ${colors.WHITE};
  }
  `}
`;

// TODO 수정 필요
const ReportButtonLabel = styled.p<{
  disabled: boolean;
}>`
  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: colors.ACTION_BLUE,
  })};

  ${({ disabled }) =>
    disabled &&
    `
    font-weight: 500;
    font-size: 16px;
    line-height: 100%;
    color: ${colors.gray_55};
  `}
`;
