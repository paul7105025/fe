import React from "react";
import styled from "styled-components";

import {
  AutoSizeTextArea,
  colors,
  FlexButton,
  FlexWrapper,
  typo,
} from "../../styles";
import { Close } from "../../assets";
import { modalSurveyType } from "@wrtn/core";

interface DialogWithSelectProps {
  data: modalSurveyType;
  width: number;
  rightButtonLabel: string;
  selected: {
    main: string;
    sub: string;
  };
  setSelected: React.Dispatch<
    React.SetStateAction<{
      main: string;
      sub: string;
    }>
  >;
  handleClose: () => void;
  handleRightButton: () => void;
}

export const DialogWithSelect = ({
  data,
  width,
  rightButtonLabel,
  selected,
  setSelected,
  handleClose,
  handleRightButton,
}: DialogWithSelectProps) => {
  //   const [selected, setSelected] = React.useState({
  //     main: "",
  //     sub: "",
  //   });
  const [isETC, setIsETC] = React.useState(false);
  const [isDisabled, setIsDisabled] = React.useState(true);

  const isMainSelected = (name: string) => {
    return selected.main === name;
  };

  const isSubSelected = (name: string) => {
    return selected.sub === name;
  };

  const selectMain = (name: string) => {
    setIsETC(false);
    setSelected({
      main: name,
      sub: "",
    });
  };

  const selectSub = (name: string) => {
    if (name === "기타") {
      setIsETC(true);
      setSelected((res) => ({
        ...res,
        sub: "",
      }));
    } else {
      setIsETC(false);
      setSelected((res) => ({
        ...res,
        sub: name,
      }));
    }
  };

  const changeSub = (value: string) => {
    setSelected((res) => ({
      ...res,
      sub: value,
    }));
  };

  React.useEffect(() => {
    const currentSurvey = data.survey.find(
      (survey) => survey.name === selected.main
    );

    if (currentSurvey) {
      if (currentSurvey.type === "select") {
        if (selected.sub && selected.sub.length > 0) {
          setIsDisabled(false);
        } else {
          setIsDisabled(true);
        }
      } else {
        setIsDisabled(false);
      }
    } else {
      setIsDisabled(true);
    }
  }, [selected]);

  return (
    <Wrapper width={width}>
      <CloseIcon onClick={handleClose}>
        <Close />
      </CloseIcon>
      <TopWrapper>
        <Title>{data.title}</Title>
        <SubTitle>{data.subTitle}</SubTitle>
      </TopWrapper>
      <BottomWrapper>
        <SelectWrapper>
          {data.survey.map((singleSurvey) => (
            <React.Fragment key={singleSurvey.id}>
              <SelectButton
                onClick={() => selectMain(singleSurvey.name)}
                isSelected={isMainSelected(singleSurvey.name)}
              >
                {singleSurvey.name}
              </SelectButton>
              {isMainSelected(singleSurvey.name) &&
                singleSurvey.type === "select" &&
                singleSurvey.select && (
                  <RadioWrapper>
                    {singleSurvey.select.map((select) => (
                      <RadioLabel key={select}>
                        <RadioInput
                          autoFocus
                          type="radio"
                          name={singleSurvey.name}
                          value={select}
                          checked={
                            select === "기타" ? isETC : isSubSelected(select)
                          }
                          onChange={(e) => selectSub(e.target.value)}
                        />
                        <RadioText>{select}</RadioText>
                      </RadioLabel>
                    ))}
                  </RadioWrapper>
                )}
              {isMainSelected(singleSurvey.name) &&
                (singleSurvey.type === "input" || isETC) && (
                  <InputWrapper>
                    <Input
                      placeholder={singleSurvey.placeholder}
                      value={selected.sub}
                      onChange={(e) => changeSub(e.target.value)}
                    />
                  </InputWrapper>
                )}
            </React.Fragment>
          ))}
        </SelectWrapper>
        <ButtonWrapper>
          <CancelButton onClick={handleClose}>취소하기</CancelButton>
          <RightButton disabled={isDisabled} onClick={handleRightButton}>
            {rightButtonLabel}
          </RightButton>
        </ButtonWrapper>
      </BottomWrapper>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)<{ width: number }>`
  position: relative;
  width: ${({ width }) => width}px;
  max-height: 95vh;

  border-radius: 20px;

  background: ${colors.WHITE};
  flex-direction: column;
`;
const CloseIcon = styled(FlexButton)`
  position: absolute;
  top: 18px;
  right: 18px;

  width: 20px;
  height: 20px;

  cursor: pointer;
`;

const TopWrapper = styled.div`
  width: 100%;
  padding: 33px 30px 25px;
  background: ${colors.LIGHT_BLUE};
  border-radius: 20px 20px 0px 0px;
`;

const BottomWrapper = styled.div`
  width: 100%;
  padding: 25px 28px 12px;
`;

const Title = styled.p`
  ${typo({
    weight: 700,
    size: "18px",
    color: colors.GRAY_90,
    isStatic: true,
  })}

  text-align: center;
`;

const SubTitle = styled.p`
  ${typo({
    weight: 500,
    size: "16px",
    height: "130%",
    color: colors.GRAY_70,
    isStatic: true,
  })}

  text-align: center;
  margin-top: 15px;
  white-space: pre-line;
`;

const SelectWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 20px;
  flex-direction: column;
`;

const SelectButton = styled(FlexButton)<{
  isSelected: boolean;
}>`
  width: 100%;
  padding: 12px;

  cursor: pointer;

  background-color: ${({ isSelected }) =>
    isSelected ? colors.ACTION_BLUE : colors.BACKGROUND};
  border-radius: 5px;

  ${({ isSelected }) =>
    typo({
      weight: 600,
      size: "16px",
      color: isSelected ? colors.WHITE : colors.ACTION_BLUE,
      isStatic: true,
    })}

  &:hover {
    background-color: ${({ isSelected }) =>
      isSelected ? colors.ACTION_BLUE : colors.LIGHT_BLUE_10};
  }
`;

const RadioWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 17px;

  padding: 4px 20px;

  flex-direction: column;
  align-items: flex-start;
`;

const RadioLabel = styled.label`
  display: flex;
  align-items: center;
  gap: 10px;
  width: 100%;
`;

const RadioInput = styled.input`
  /* vertical-align: middle;
  appearance: none;
  border: max(2px, 0.1em) solid gray; */
  border-radius: 50%;
  width: 18px;
  height: 18px;
  transition: border 0.5s ease-in-out;
  /* &:checked {
    border: max(2px, 0.1em) solid ${colors.MAIN_BLUE};
  }
  &:focus-visible {
    outline-offset: max(2px, 0.1em);
    outline: max(2px, 0.1em) dotted tomato;
  }
  &:hover {
    border: max(2px, 0.1em) solid ${colors.MAIN_BLUE};
    cursor: pointer;
  } */
`;

const RadioText = styled.span`
  ${typo({
    weight: 600,
    size: "16px",
    height: "150%",
    color: colors.GRAY_90,
    isStatic: true,
  })}
`;

const ButtonWrapper = styled.div`
  display: flex;

  margin-top: 33px;
`;

const InputWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 15px 20px;
  border: 1px solid ${colors.GRAY_50};
  border-radius: 5px;
`;

const Input = styled(AutoSizeTextArea)`
  width: 100%;
  resize: none;

  ${typo({
    weight: 500,
    size: "16px",
    height: "120%",
    color: colors.GRAY_90,
  })}

  &::placeholder {
    color: ${colors.GRAY_55};
    user-select: none;
  }
`;

const CancelButton = styled(FlexButton)`
  background: ${colors.WHITE};
  padding: 12px;

  flex: 1;
  border-radius: 5px;

  cursor: pointer;

  &:hover {
    background: ${colors.gray_30};
  }

  ${typo({
    weight: 500,
    size: "16px",
    height: "100%",
    color: colors.gray_80,
    isStatic: true,
  })};
`;

const RightButton = styled(FlexButton)`
  /* background: ${colors.LIGHT_BLUE}; */
  padding: 12px;

  flex: 1;
  border-radius: 5px;

  cursor: pointer;

  ${typo({
    weight: 500,
    size: "16px",
    color: colors.GRAY_80,
    isStatic: true,
  })};

  &:hover {
    background-color: ${colors.gray_30};
  }

  &:disabled {
    background-color: ${colors.WHITE};
    cursor: not-allowed;
    color: ${colors.GRAY_55};
  }
`;
