import styled from "styled-components";

import { IconRightPolygon } from "../../assets";
import { colors, FlexButton, FlexWrapper, typo } from "../../styles";

interface PaginationProps {
  currentPage: number;
  setPage: (page: number) => void;
  maxPage: number;
}

export const Pagination = ({
  currentPage, // 0 ~ 9
  setPage, // - ~ 9
  maxPage,
}: PaginationProps) => {
  if (maxPage === 0) {
    return <></>;
  }

  const min = (a: number, b: number) => {
    return a < b ? a : b;
  };

  const List = () => {
    const list: Array<JSX.Element> = [];
    let current = Number(currentPage / 10) * 10;
    for (
      let i = current + 6 > maxPage ? maxPage - 10 : current - 5;
      current < 7 ? i < min(10, maxPage) : i < min(current + 6, maxPage);
      i++
    ) {
      if (i < 0) continue;
      if (i === currentPage)
        list.push(<CurrentPage key={i}>{i + 1}</CurrentPage>);
      else
        list.push(
          <Index key={i} onClick={() => setPage(i)}>
            {i + 1}
          </Index>
        );
    }
    return list;
  };

  const onHandleDecrease = () => {
    if (currentPage > 0) setPage(currentPage - 1);
  };

  const onHandleIncrease = () => {
    if (currentPage < maxPage - 1) setPage(currentPage + 1);
  };

  return (
    <Wrapper justify="space-between">
      <ChevronContainer
        isVisible={currentPage > 0 ? true : false}
        onClick={onHandleDecrease}
      >
        <LeftChevron />
      </ChevronContainer>
      {List()}
      <ChevronContainer
        isVisible={currentPage < maxPage - 1 ? true : false}
        onClick={onHandleIncrease}
      >
        <RightChevron />
      </ChevronContainer>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  gap: 12px;
  // min-width: 400px;
`;

const ChevronContainer = styled(FlexButton)<{ isVisible: boolean }>`
  visibility: ${({ isVisible }) => (!isVisible ? "hidden" : "visible")};
  cursor: pointer;
`;

const RightChevron = styled(IconRightPolygon)<{ isLastPage?: boolean }>`
  path {
    fill: ${(props) => (props.isLastPage ? colors.gray_55 : colors.gray_80)};
  }
`;

const LeftChevron = styled(RightChevron)`
  transform: rotate(180deg);
`;

const Index = styled.div`
  display: inline;
  ${typo({ weight: 600, size: "16px", height: "16px", color: colors.gray_70 })};
  cursor: pointer;
  user-select: none;
`;

const CurrentPage = styled.div`
  display: inline;
  ${typo({ weight: 600, size: "16px", height: "16px", color: colors.gray_90 })};
  cursor: pointer;
  user-select: none;
`;
