import React from "react";
import styled from "styled-components";

import { colors, FlexButton, FlexWrapper, typo } from "../../styles";

import { Close, ScoreStar, LoveYou } from "../../assets";

const scoreDesc = {
  0: "이 툴의 별점은?",
  1: "매우 유용하지 않아요.",
  2: "유용하지 않아요.",
  3: "보통이에요.",
  4: "유용해요!",
  5: "매우 유용해요!",
};

interface CommonScoreContainerProps {
  isCompleted: boolean;
  close: () => void;
  toolName: string;
  score: number;
  selectedScore: number;
  setScore: (score: number) => void;
  setSelectedScore: (score: number) => void;
  submitScore: () => void;
}

export const CommonScoreContainer = ({
  isCompleted,
  close,
  toolName,
  score,
  selectedScore,
  setScore,
  setSelectedScore,
  submitScore,
}: CommonScoreContainerProps) => {
  if (isCompleted)
    return (
      <InnerWrapper>
        <CloseButton onClick={close}>
          <Close />
        </CloseButton>
        <LoveYouIcon />
        <ScoreText
          style={{
            marginTop: 16,
          }}
        >
          접수가 완료되었어요!
        </ScoreText>
        <ThanksText>
          여러분의 소중한 피드백은
          <br />
          서비스 개선에 큰 도움이 됩니다.
        </ThanksText>
      </InnerWrapper>
    );

  return (
    <InnerWrapper column>
      <CloseButton onClick={close}>
        <Close />
      </CloseButton>
      <Title>
        <Bold>{toolName}</Bold> 툴에 대한 평가를 남겨주세요!
      </Title>
      <ScoreText>{scoreDesc[score]}</ScoreText>
      <StarWrapper onMouseLeave={() => setScore(selectedScore)}>
        {[1, 2, 3, 4, 5].map((v) => (
          <Star
            key={v}
            onMouseOver={() => setScore(v)}
            onClick={() => setSelectedScore(v)}
            color={score >= v ? colors.POINT_PURPLE : colors.GRAY_30}
            selected={selectedScore >= v}
          />
        ))}
      </StarWrapper>
      <CompleteButton onClick={submitScore} disabled={selectedScore < 1}>
        완료하기
      </CompleteButton>
    </InnerWrapper>
  );
};

const InnerWrapper = styled(FlexWrapper)`
  position: relative;
  width: 100%;
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
  position: absolute;
  
  top: 5px;
  right: 5px;

  width: 16px;
  height: 16px;

  svg {
    path {
      fill: ${colors.GRAY_70};
    }
  }
`;

const Title = styled.span`
  margin-top: 6px;
  text-align: center;
  ${typo({
    size: "14px",
    height: "14px",
    weight: "600",
    color: colors.GRAY_70,
  })}
`;

const Bold = styled(Title)`
  font-weight: 700;
`;

const ScoreText = styled.p`
  margin-top: 14px;
  ${typo({
    size: "16px",
    height: "16px",
    weight: "700",
    color: colors.POINT_PURPLE,
  })}
`;

const StarWrapper = styled(FlexWrapper)`
  margin-top: 9px;
  gap: 2px;
`;

const Star = styled(ScoreStar)<{ selected: boolean }>`
  cursor: pointer;
  width: 28px;
  height: 28px;

  path {
    fill: ${({ color }) => color};
  }
  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const CompleteButton = styled(FlexButton)<{ disabled: boolean }>`
  cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
  padding: 8px 16px;
  margin-top: 15px;

  background-color: ${colors.BACKGROUND};
  &:hover {
    background-color: ${({ disabled }) =>
      disabled ? "transparent" : colors.WHITE_BOLDER_LINE};
  }
  border-radius: 8px;

  ${({ disabled }) =>
    typo({
      size: "14px",
      height: "14px",
      weight: "600",
      color: disabled ? colors.GRAY_50 : colors.ACTION_BLUE,
    })}
`;

const LoveYouIcon = styled(LoveYou)`
  margin-top: 10px;
  width: 36px;
  height: 36px;
`;

const ThanksText = styled.p`
  margin-top: 12px;
  text-align: center;
  ${typo({
    size: "14px",
    height: "20px",
    weight: "500",
    color: colors.GRAY_70,
  })}
`;
