import React from "react";
import { css } from "styled-components";

import { Wrapper } from "./styles";
import { boxShadow } from "../../styles";

import { useTimeout } from "@wrtn/core";
import { getToolStar, postToolScore } from "@wrtn/core/services";

import { Bubble } from "../../components/Bubble/Bubble";
import { ModalPositionPortal } from "../../components/ModalPortal";

import { CommonScoreContainer } from "./CommonScoreContainer";

interface ToolScoreModalProps {
  toolId: string;
  toolName: string;
  currentCount: { [key: string]: number };
  resultClientRect: DOMRect;
}

export const ToolScoreModal = ({
  toolId,
  toolName,
  currentCount,
  resultClientRect,
}: ToolScoreModalProps) => {
  const [isScoreModalOpen, setIsScoreModalOpen] = React.useState(false);
  const [done, setDone] = React.useState({});

  const getToolStarAPI = async (toolId: string) => {
    if (toolId) {
      const res = await getToolStar({ toolId });
      if (res?.status === 200) {
        const { data } = res.data;
        if (data !== null) {
          setDone({
            ...done,
            [toolId]: true,
          });
        }
      }
    }
  };

  React.useEffect(() => {
    setIsScoreModalOpen(false);
    if (toolId) {
      getToolStarAPI(toolId);
    }
  }, [toolId]);

  React.useEffect(() => {
    if (currentCount[toolId] >= 10 && done[toolId] !== true) {
      setIsScoreModalOpen(true);
    } else {
      setIsScoreModalOpen(false);
    }
  }, [currentCount]);

  const [isCompleted, setIsCompleted] = React.useState(false);
  const [score, setScore] = React.useState(0);
  const [selectedScore, setSelectedScore] = React.useState(0);

  const submitScore = async () => {
    const res = await postToolScore({
      toolId: toolId,
      score: selectedScore,
    });
    if (res?.data.result === "SUCCESS") {
      setIsCompleted(true);
    } else if (res?.statusCode === 400) {
      setIsCompleted(true);
    }
  };

  useTimeout(
    () => {
      if (isCompleted) close();
    },
    5000,
    [isCompleted]
  );

  const close = () => {
    setDone({
      ...done,
      [toolId]: true,
    });
    setIsScoreModalOpen(false);
  };

  if (isScoreModalOpen)
    return (
      <ModalPositionPortal
        position={{
          top: resultClientRect?.top - 200,
          left: resultClientRect?.left + resultClientRect?.width / 2 - 187,
        }}
      >
        <Wrapper>
          <Bubble direction="bottom" css={hoverContent}>
            <CommonScoreContainer
              isCompleted={isCompleted}
              score={score}
              toolName={toolName}
              setScore={setScore}
              selectedScore={selectedScore}
              setSelectedScore={setSelectedScore}
              submitScore={submitScore}
              close={close}
            />
          </Bubble>
        </Wrapper>
      </ModalPositionPortal>
    );
  return null;
};

const hoverContent = css`
  min-width: 374px;
  min-height: 166px;
  padding: 13px 11px;
  ${boxShadow.dialog_shadow};

  &::after {
    bottom: -40px;
    transform: scale(0.8, 1.5);
    left: calc(50% - 18.5px);
    border-width: 16.5px;
  }

  &::before {
    bottom: -41.5px;
    transform: scale(0.8, 1.5);
    left: calc(50% - 18.5px);
    border-width: 16.5px;
  }
`;
