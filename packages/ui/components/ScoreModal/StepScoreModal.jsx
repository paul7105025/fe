import React from "react";
import styled from "styled-components";

import { boxShadow, colors } from "../../styles";

import { ModalPositionPortal } from "../../components/ModalPortal";
import { getToolStar, postToolScore, useIs960, useTimeout } from "@wrtn/core";
import { CommonScoreContainer } from "./CommonScoreContainer";
import { Wrapper } from "./styles";

export const StepScoreModal = ({ stepId, currentCount }) => {
  const [isScoreModalOpen, setIsScoreModalOpen] = React.useState(false);
  const [done, setDone] = React.useState({});

  const getToolStarAPI = async (stepId) => {
    if (stepId) {
      const res = await getToolStar({ toolId: stepId });
      if (res?.status === 200) {
        const { data } = res.data;
        if (data !== null) {
          setDone({
            ...done,
            [stepId]: true,
          });
        } else {
          setDone({
            ...done,
            [stepId]: false,
          });
        }
      }
    }
  };

  React.useEffect(() => {
    setIsScoreModalOpen(false);
    getToolStarAPI(stepId);
  }, []);

  React.useEffect(() => {
    if (
      currentCount[stepId] >= 1 &&
      done[stepId] !== undefined &&
      done[stepId] !== true
    ) {
      setIsScoreModalOpen(true);
    } else {
      setIsScoreModalOpen(false);
    }
  }, [currentCount, stepId]);

  const [isCompleted, setIsCompleted] = React.useState(false);
  const [score, setScore] = React.useState(0);
  const [selectedScore, setSelectedScore] = React.useState(0);

  const submitScore = async () => {
    const res = await postToolScore({
      toolId: stepId,
      score: selectedScore,
    });
    if (res?.data.result === "SUCCESS") {
      setIsCompleted(true);
    } else if (res?.statusCode === 400) {
      setIsCompleted(true);
    }
  };

  useTimeout(
    () => {
      if (isCompleted) close();
    },
    5000,
    [isCompleted]
  );

  const close = () => {
    setDone({
      ...done,
      [stepId]: true,
    });
    setIsScoreModalOpen(false);
  };

  const is960 = useIs960();

  if (
    isScoreModalOpen &&
    (done[stepId] === undefined || done[stepId] === false)
  )
    return (
      <ModalPositionPortal
        position={{
          top: window.innerHeight - 250,
          left: window.innerWidth / 2 + (!is960 ? 116 : 0) - 187,
        }}
      >
        <Wrapper>
          <HoverContent>
            <CommonScoreContainer
              isCompleted={isCompleted}
              score={score}
              setScore={setScore}
              selectedScore={selectedScore}
              setSelectedScore={setSelectedScore}
              submitScore={submitScore}
              close={close}
            />
          </HoverContent>
        </Wrapper>
      </ModalPositionPortal>
    );
  return null;
};

const HoverContent = styled.div`
  min-width: 374px;
  min-height: 166px;
  border-radius: 20px;
  padding: 13px 11px;
  background: ${colors.WHITE};
  ${boxShadow.dialog_shadow};

  display: flex;
  flex-direction: column;

  border: 1px solid ${colors.WHITE_BOLDER_LINE};
`;
