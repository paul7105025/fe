import styled from "styled-components";

import { FlexWrapper } from "../../styles";

export const Wrapper = styled(FlexWrapper)`
  display: flex;
  z-index: 20;
  position: absolute;
  background: transparent;
`;
