import { PlanType, PLAN_COLOR, PLAN_INFO } from "@wrtn/core";
import styled from "styled-components";

import { colors, typo } from "../../styles";

export const PlanLabel = ({ type = "FREE" }: { type: PlanType }) => {
  return <Wrapper planType={type}>{type.toUpperCase()}</Wrapper>;
};

export const PlanPriceLabel = ({
  type = "FREE",
}: {
  type: PlanType;
}): JSX.Element => {
  return (
    <Wrapper
      planType={type}
      style={{
        backgroundColor: type === "FREE" ? colors.GRAY_90 : PLAN_COLOR[type],
      }}
    >
      무제한
    </Wrapper>
  );
};

const Wrapper = styled.div<{
  planType: PlanType;
}>`
  background: ${({ planType }) => PLAN_COLOR[planType]};
  border-radius: 8px;
  padding: 5px 8px;
  display: inline;
  ${typo({ weight: 700, size: "12px", height: "100%", color: colors.WHITE })};
`;
