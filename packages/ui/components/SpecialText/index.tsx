import styled from "styled-components";

import { FilledStar } from "../../assets";
import { colors, typo } from "../../styles";

export const Star = styled(FilledStar)`
  margin-left: 6px;
  width: 14px;
  height: 14px;
`;

export const StarText = () => {
  return <StarTextWrapper>NEW!✨</StarTextWrapper>;
};

export const HotText = () => {
  return <HotTextWrapper>HOT! 🔥</HotTextWrapper>;
};

const StarTextWrapper = styled.p`
  white-space: nowrap;
  margin-left: 5px;
  ${typo({
    size: "12px",
    weight: "700",
    height: "12px",
    color: colors.STAR_YELLOW,
  })}
`;

const HotTextWrapper = styled.p`
  white-space: nowrap;
  margin-left: 5px;
  ${typo({
    size: "12px",
    weight: "700",
    height: "12px",
    color: colors.HOT_RED,
  })}
`;
