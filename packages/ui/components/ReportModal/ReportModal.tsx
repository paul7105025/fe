import React from "react";
import { useAlert } from "react-alert";

import { Dialog, DialogWithTextarea } from "../../components/Dialog";

import { Kind, useEvent, useTagManager } from "@wrtn/core";
import { getHistory_Id, postHistory_IdReport } from "@wrtn/core/services";

interface ReportModalProps {
  onClose: () => void;
  currentHistory: any;
  onSuccess: (history: any) => void;
  type: "tool" | "history";
  kind: Kind;
}

export const ReportModal = ({
  onClose,
  currentHistory,
  onSuccess,
  type,
  kind,
}: ReportModalProps) => {
  const [state, setState] = React.useState("ready");
  const [text, setText] = React.useState("");

  const { collectEvent } = useEvent();
  const tagManager = useTagManager();
  const alert = useAlert();

  const handleReport = async () => {
    const resHistory = await getHistory_Id({ historyId: currentHistory._id });
    const res = await postHistory_IdReport({
      historyId: currentHistory._id,
      data: {
        reportReason: text,
      },
    });
    if (res?.status === 201) {
      if (currentHistory.tool) {
        tagManager({
          event: "click_report_submit_btn",
          data: {
            tool_name: currentHistory.tool.name,
            tool_id: currentHistory.tool._id,
            tool_tag: currentHistory.tool.tag,
            output_date: currentHistory.createdAt,
            report_contents: text,
          },
        });
        collectEvent("click_report_submit_btn", {
          position: type,
          feature_menu: "tool",
          feature_category: currentHistory.tool.category,
          feature_name: currentHistory.tool.name,
          feature_tag: currentHistory.tool.tag,
          feature_form: kind,
          report_contents: text,
        });
      } else {
        if (resHistory?.status === 200) {
          const { data } = resHistory.data;
          tagManager({
            event: "click_report_submit_btn",
            data: {
              tool_name: data.tool.name,
              tool_id: data.tool._id,
              tool_tag: data.tool.tag,
              output_date: data.createdAt,
              report_contents: text,
            },
          });
          collectEvent("click_report_submit_btn", {
            position: type,
            feature_menu: "tool",
            feature_category: data.tool.category,
            feature_name: data.tool.name,
            feature_tag: data.tool.tag,
            feature_form: kind,
            report_contents: text,
          });
        }
      }
      alert.removeAll();
      alert.show("결과물을 신고했어요.");
      onSuccess(currentHistory);
      onClose();
    }
  };

  if (state === "ready") {
    return (
      <Dialog
        iconType="report"
        title="이 결과물을 신고하시겠어요?"
        description={["결과물을 신고하면 결과 창에서 삭제됩니다."]}
        handleClose={onClose}
        rightButtonLabel="신고하기"
        handleRightButton={() => {
          setState("write");
        }}
      />
    );
  } else if (state === "write") {
    return (
      <DialogWithTextarea
        handleClose={onClose}
        value={text}
        onChange={(e) => setText(e.target.value)}
        disableRightButton={text.length === 0}
        handleRightButton={() => {
          handleReport();
        }}
      />
    );
  } else {
    return <></>;
  }
};
