import React from "react";
import styled from "styled-components";

import { colors, FlexWrapper, typo } from "../styles";

export const StepGuide = () => {
  return (
    <StepGuideWrapper>
      <StepGuideBox>
        <b>단계별 생성 기능 사용법</b>
        <div style={{ height: 8 }} />
        1. 마음에 드는 결과물을 클릭해 선택한 후, <b>
          &lt;다음 단계로&gt;
        </b>{" "}
        버튼을 눌러주세요.
        <br />
        2. 선택한 결과물이 다음 단계의 입력값으로 들어갑니다.
      </StepGuideBox>
    </StepGuideWrapper>
  );
};

export const BundleGuide = () => {
  return (
    <StepGuideWrapper>
      <StepGuideBox>
        <b>상세페이지 작성법</b>
        <div style={{ height: 8 }} />
        1. 입력칸을 채운 후 자동 생성 버튼을 눌러주세요. 용도에 맞는 결과물이
        한번에 생성됩니다.
        <br />
        2. <b>전체 결과물</b>을 다시 생성하고 싶을 경우 <b>자동 생성</b> 버튼을,{" "}
        <b>개별 </b>
        결과물만 다시 생성하고 싶다면 해당 결과물 옆의 <b>개별 생성</b> 버튼을
        눌러주세요.
      </StepGuideBox>
    </StepGuideWrapper>
  );
};

const StepGuideWrapper = styled(FlexWrapper)`
  bottom: -20px;
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`;

const StepGuideBox = styled.div`
  ${typo({
    size: "14px",
    height: "24px",
    weight: "500",
    color: colors.GRAY_60,
  })}
`;
