
import styled, { CSSProperties } from 'styled-components'
import { colors } from '.';

type DividerProps = {
  direction: "vertical" | "horizontal",
  height?: CSSProperties["height"],
  width?: CSSProperties["width"],
  color?: CSSProperties["color"],
};

export const Divider = styled.div<DividerProps>`
  ${props => props.direction === "vertical" ? ` 
      width: ${props.width || "0px"};
      height: ${props.height || "100%"};
    ` : `
      width: ${props.width || "100%"};
      height: ${props.height || "0px"};
    ` 
  };
  flex-wrap: wrap;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
`;
