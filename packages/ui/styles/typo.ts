import { css } from "styled-components";
import { colors } from "./colors";

type typoType = {
  weight?: number | string;
  size?: string;
  height?: string;
  color?: string;
  isStatic?: boolean;
};

export const typo = ({
  weight = 500,
  size = "16px",
  height = "100%",
  color = "#3B3F4E",
  isStatic = false,
}: typoType) => {
  const newSize = parseInt(size.replace("px", "")) / 16 + "rem";
  //
  return `
  font-weight: ${weight};
  font-size: ${isStatic ? size : newSize || size};
  line-height: ${height};
  color: ${color};
`;
};

export const FormLabel = css`
  ${typo({
    size: "16px",
    weight: 600,
    color: colors.GRAY_80,
  })};
`;

export const lineHeightSelector: Record<string, number> = {
  body: 160,
  body_chat: 180,
  body_compact: 100,
  heading: 100,
  caption: 100,
  content: 100,
};

export const weightSelector: Record<string, number> = {
  reg: 400,
  med: 500,
  semi: 600,
  bold: 700,
};

export const typoSelector = (typo: string) => {
  const typoList = typo.split("-");

  if (typoList.length < 3) {
    return {
      fontSize: 16,
      lineHeight: 100,
      fontWeight: 400,
    };
  }

  const [type, size, weight] = typoList;

  return {
    fontSize: Number(size),
    lineHeight: (Number(size) * lineHeightSelector[type]) / 100,
    fontWeight: weightSelector[weight],
  };
};

export const newTypo = (font: string) => {
  const currentTypo = typoSelector(font);
  return css`
    font-size: ${currentTypo.fontSize}px;
    line-height: ${currentTypo.lineHeight}px;
    font-weight: ${currentTypo.fontWeight};
  `;
};
