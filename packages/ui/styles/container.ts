import styled, { css, CSSProperties } from "styled-components";
import TextareaAutosize from "react-textarea-autosize";

import { newTypo, typo } from "./typo";
import { colors, newColors } from "./colors";

export const BlockWrapper = styled.div`
  display: block;
`;

export type FlexProps = {
  column?: boolean;
  row?: boolean;

  align?: CSSProperties["alignItems"];
  justify?: CSSProperties["justifyContent"];
};

const directionSelector = ({ column, row }: FlexProps) => {
  if (column) {
    return "column";
  } else if (row) {
    return "row";
  }
  return "row";
};

export const FlexWrapper = styled.div<FlexProps>`
  display: flex;
  flex-direction: ${(props) =>
    directionSelector({
      column: props.column,
      row: props.row,
    })};

  align-items: ${(props) => (props.align ? props.align : "center")};
  justify-content: ${(props) => (props.justify ? props.justify : "flex-start")};

  position: relative;
  @media (max-width: 767px) {
    font-size: 14px;
    line-height: 26px;
    > svg {
    }
  }
`;

export const FlexButton = styled.button<FlexProps>`
  display: flex;

  flex-direction: ${(props) =>
    directionSelector({
      column: props.column,
      row: props.row,
    })};

  align-items: ${(props) => (props.align ? props.align : "center")};
  justify-content: ${(props) => (props.justify ? props.justify : "center")};

  position: relative;
  background-color: transparent;
`;

export const FlexLabel = styled.label<FlexProps>`
  display: flex;

  flex-direction: ${(props) =>
    directionSelector({
      column: props.column,
      row: props.row,
    })};

  align-items: ${(props) => (props.align ? props.align : "center")};
  justify-content: ${(props) => (props.justify ? props.justify : "flex-start")};

  position: relative;
`;

export const FormWrapper = styled.div`
  display: flex;
  justify-content: center;

  flex-direction: column;
  align-items: flex-start;

  position: relative;
  gap: 16px;
  width: 100%;
`;

export const TipText = styled.p`
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.GRAY_60,
  })}
`;

export const LabelWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: flex-start;
  gap: 6px;

  @media (max-width: 480px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

export const Input = styled.input``;

export const InputStyle = css`
  background: ${colors.BACKGROUND};

  border-radius: 8px;
  width: 100%;
  resize: none;
  padding: 8px 16px;

  ${typo({
    size: "16px",
    height: "26px",
    color: colors.GRAY_90,
  })}

  &::placeholder {
    color: ${colors.GRAY_55};
    user-select: none;
  }

  &:focus {
    padding: 7px 15px;
    border: 1px solid ${colors.ACTION_BLUE};
  }
`;

export const InputDescription = styled.p`
  ${typo({
    size: "14px",
    weight: 500,
    color: colors.GRAY_60,
  })}
`;

export const Textarea = styled.textarea.attrs((props) => ({}))`
  width: 100%;
`;

export const AutoSizeTextArea = styled(TextareaAutosize)`
  background: ${colors.WHITE};
  resize: none;
`;

export const Button = styled.button`
  background-color: #4caf50;
`;

export const Label = styled.label``;

export const Tag = styled.span<{ color: string }>`
  display: ${({ color }) => (color === "BLACK" ? "none" : "flex")};
  justify-content: center;
  align-items: center;
  position: relative;
  padding: 3px 8px;
  background: ${(props) => colors[props.color]};
  border-radius: 4px;
  margin-left: 11px;
  margin-right: 8px;
  white-space: nowrap;

  ${typo({
    weight: 500,
    size: "14px",
    height: "100%",
    color: colors.WHITE,
  })};
`;

export const SmallTag = styled(Tag)`
  margin-left: 0px;
  margin-right: 0px;
  max-height: 20px;
  ${typo({
    size: "12px",
    height: "12px",
    color: colors.WHITE,
  })}
`;

export const ChipStyleSelector = {
  sm: css`
    padding: 3px 12px;
    border-radius: 30px;
    background-color: ${colors.POINT_PURPLE};
    ${typo({
      size: "16px",
      weight: 600,
      height: "162%",
      color: colors.WHITE,
    })}

    @media (max-width: 767px) {
      padding: 1.5px 10px;
      font-size: 12px;
    }
  `,
  md: css`
    padding: 9px 20px;
    background: linear-gradient(
        92.98deg,
        rgba(94, 32, 226, 0.0561) 26.65%,
        rgba(68, 75, 231, 0.0352) 79.26%
      ),
      rgba(90, 44, 218, 0.05);
    backdrop-filter: blur(1.79312px);

    border-radius: 45.3778px;

    ${typo({
      size: "20px",
      weight: 700,
      height: "120%",
      color: colors.POINT_PURPLE,
    })}

    @media (max-width: 767px) {
      padding: 6px 15px;
      font-size: 14px;
    }
  `,
  lg: css`
    padding: 10px 70px;
    border-radius: 70px;
    background-color: ${colors.WHITE};
    ${typo({
      size: "20px",
      weight: 700,
      height: "160%",
      color: colors.POINT_PURPLE,
    })}

    &:hover {
      background-color: ${colors.HOVER_PURPLE};
      color: ${colors.WHITE};
    }
  `,
  chat: css`
    padding: 8px 16px;
    border-radius: 21px;
    background-color: ${newColors.WHITE};
    border: 1px solid ${newColors.GRAY_100};
    box-shadow: 0px 2px 10px rgba(79, 68, 195, 0.1);
    max-width: 600px;

    ${newTypo("body_compact-16-med")}
    line-height: 160%;
    color: ${newColors.PURPLE_500_PRIMARY};
    word-break: keep-all;
  `,
};
