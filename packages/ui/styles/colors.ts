export const colors = {
  LIGHT_BLUE: "#F2F7FF",
  ACTION_BLUE: "#3784F6",
  SKY_BLUE: "#EAF2FF",
  ABOUT_SKY_BLUE: "#6FD4FF",
  HOVER_PURPLE: "#9E96FF",
  SKY_PURPLE: "#EAE3FF",
  POINT_PURPLE: "#5A2CDA",
  POINT_PURPLE_2: "#341A80",
  POINT_PURPLE_3: "#1F104D",
  POINT_PURPLE_4: "#180C39",
  POINT_GREEN: "#21EC8B",
  LILAC: "#C8CAFF",
  BACKGROUND: "#F2F7FF",
  BLUE_GRAY_LINE: "#D1D7ED",
  WHITE_BOLDER_LINE: "#DEE4F3",
  YELLOW: "#FFAC33",
  ABOUT_YELLOW: "#FFCF24",
  POINT_YELLOW: "#FFCF55",
  ORANGE: "#FFB800",
  STAR_YELLOW: "#FFB800",
  HOT_RED: "#FF2929",
  CREDIT_ORANGE: "#F4900C",

  TAG_BLUE: "#6FB4FF",
  TAG_PURPLE: "#BF98FF",
  TAG_YELLOW: "#DCBA64",
  TAG_RED: "#DC6481",
  TAG_GREEN: "#8DC98C",
  TAG_ORANGE: "#D67C4A",
  TAG_INDIGO: "#6B7CD6",
  TAG_LIGHT_GRASS: "#73C6C1",
  TAG_GRASS: "#4BAB8E",
  TAG_PINK: "#DE7CE0",
  TAG_LIGHT_ORANGE: "#FFA31A",

  white: "#FFFFFF",
  gray_10: "#FAFAFF",
  gray_20: "#F0F2F9",
  gray_30: "#ECEEF7",
  gray_40: "#E6E8F0",
  gray_50: "#D8DDE9",
  gray_55: "#C4C9D7",
  gray_60: "#83879D",
  gray_70: "#717488",
  gray_80: "#505467",
  gray_90: "#3B3F4E",
  gray_100: "#393B44",

  BLACK: "#000000",
  WHITE: "#FFFFFF",
  GRAY_10: "#FAFAFF",
  GRAY_20: "#F0F2F9",
  GRAY_30: "#ECEEF7",
  GRAY_40: "#E6E8F0",
  GRAY_50: "#D8DDE9",
  GRAY_55: "#C4C9D7",
  GRAY_58: "#A1A5B9",
  GRAY_60: "#83879D",
  GRAY_70: "#717488",
  GRAY_80: "#505467",
  GRAY_90: "#3B3F4E",
  GRAY_100: "#393B44",

  GRAY_02: "#CCCCCC",

  SUB_BLUE: "#475EB8",
  MAIN_BLUE: "#354FB5",
  THIRD_BLUE: "#8695D3",
  BLUE_10: "#364CA4",
  BLUE_20: "#374A93",
  BLUE_30: "#384782",
  BLUE_40: "#394470",
  BLUE_50: "#3A425F",

  LIGHT_BLUE_10: "#d9e7ff",

  ACTION_BLUE_HOVER: "#4B90F6",
  YELLOW_HOVER: "#FFB800",
  POINT_PURPLE_HOVER: "#4f23c9",

  TOOL_BLUE_GREEN: "#74B8C7",
  TOOL_GREEN: "#74C7B3",
  TOOL_LIGHT_GREEN: "#7EC781",
  TOOL_PURPLE: "#BB9AE5",
  TOOL_PINK: "#FB81D9",
  TOOL_CORAL: "#FF8383",
  TOOL_SALMON: "#FF906C",
  TOOL_ORANGe: "#FFA34F",

  FEEDBACK_GREEN: "#08B539",
  FEEDBACK_RED: "#FF2929",

  BRAND_NAVER_MAIN: "#03CF5D",
  BRAND_KAKAO_MAIN: "#FEE500",
  BRAND_GOOGLE_MAIN: "#4285F4",
  BRAND_EMAIL_MAIN: "#FF6C00",
};

export const newColors = {
  WHITE: "#FFFFFF",
  BLACK: "#000000",

  GRAY_000: "#F1F5FF",
  GRAY_100: "#E1E7F5",
  GRAY_200: "#D3D9EB",
  GRAY_300: "#C3C8D9",
  GRAY_400: "#ADB2C5",
  GRAY_500: "#8F95AB",
  GRAY_600: "#717688",
  GRAY_700: "#505567",
  GRAY_800: "#3B3F4E",

  GRAY_100_NEUTRAL: "#E7E7E7",
  GRAY_200_NEUTRAL: "#D9D9D9",

  PURPLE_200: "#F7F6FF",
  PURPLE_300: "#F3F0FF",

  PURPLE_500_PRIMARY: "#6446FF",
  PURPLE_600_BRAND: "#5A2CDA",

  BLUE_200_BG: "#FAFCFF",
  BLUE_400_BG: "#F2F7FF",
  BLUE_500_NOTICE: "#3784F6",
};
