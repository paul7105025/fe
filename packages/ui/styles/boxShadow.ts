import { css } from "styled-components";

const output_card_hover_shadow = css`
  filter: drop-shadow(0px 4px 24px rgba(79, 68, 195, 0.1));
`;

const history_card_shadow = css`
  filter: drop-shadow(4px 4px 10px rgba(79, 68, 195, 0.05));
`;

const history_card_hover_shadow = css`
  filter: drop-shadow(10px 10px 24px rgba(79, 68, 196, 0.1));
`;

const thumbnail_card_shadow = css`
  box-shadow: 4px 4px 10px rgba(79, 68, 195, 0.05);
`;

const thumbnail_card_hover_shadow = css`
  box-shadow: 10px 10px 24px rgba(79, 68, 196, 0.1);
`;

const basic_card_shadow = css`
  box-shadow: 4px 4px 10px rgba(79, 68, 195, 0.05);
`;

const basic_card_hover_shadow = css`
  box-shadow: 10px 10px 24px rgba(79, 68, 196, 0.1);
`;

const color_button_shadow = css`
  box-shadow: 4px 4px 15px rgba(79, 68, 195, 0.15),
    inset 4px 4px 5px rgba(255, 255, 255, 0.15);
`;

const counter_button_shadow = css`
  filter: drop-shadow(0px 2px 10px rgba(79, 68, 195, 0.1));
`;

const guideline_shadow = css`
  box-shadow: 2px 2px 10px rgba(39, 44, 70, 0.1);
`;

const trigger_tool_message_hover_shadow = css`
  box-shadow: 0px 0px 20px rgba(79, 68, 195, 0.1);
`;

const history_modal_shadow = css`
  box-shadow: 0px 4px 24px rgba(30, 23, 98, 0.25);
`;

const dialog_shadow = css`
  box-shadow: 0px 4px 20px rgba(57, 68, 112, 0.1);
`;

const tooltip_message_shadow = css`
  box-shadow: 2px 2px 10px rgba(57, 68, 112, 0.1);
`;

const NDA_shadow = css`
  box-shadow: 0px 4px 30px rgba(0, 0, 0, 0.15);
`;

const login_box_shadow = css`
  filter: drop-shadow(0px 0px 20px rgba(56, 71, 130, 0.05));
`

export const boxShadow = {
  output_card_hover_shadow,
  history_card_shadow,
  history_card_hover_shadow,
  thumbnail_card_shadow,
  thumbnail_card_hover_shadow,
  basic_card_shadow,
  basic_card_hover_shadow,
  color_button_shadow,
  counter_button_shadow,
  guideline_shadow,
  history_modal_shadow,
  dialog_shadow,
  tooltip_message_shadow,
  NDA_shadow,
  trigger_tool_message_hover_shadow,
  login_box_shadow,
};
