import { css } from "styled-components";

const dvh = (tag: string, value: number, convertUnit?: string) => {
  const unit = convertUnit ? convertUnit : 'vh';
  return css`
    ${tag}: ${value.toString()}${unit};
    ${tag}: ${value.toString()}dvh;
  `
}

const polyfill = {
  dvh,
};

export default polyfill;