export const stepCategorySelector = {
  "엔터테인먼트, 예술": "엔터테인먼트 ・ 예술",
  "취미, 여가, 여행": "취미 ・ 여가 ・ 여행",
  "생활, 노하우, 쇼핑": "생활 ・ 노하우 ・ 쇼핑",
  "지식, 동향": "지식 ・ 동향",
};

const textToLexical = (text) => {
  const root = {
    children: [],
    direction: "ltr",
    format: "",
    indent: 0,
    type: "root",
    version: 1,
  };

  text.split(/\r?\n/).forEach((v) => {
    root.children.push({
      children:
        v.length > 0
          ? [
              {
                detail: 0,
                format: 0,
                mode: "normal",
                style: "",
                text: v,
                type: "text",
                version: 1,
              },
            ]
          : [],
      direction: "ltr",
      format: "",
      indent: 0,
      type: "paragraph",
      version: 1,
    });
  });

  return { root: root };
};

export default textToLexical;
