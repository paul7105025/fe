import styled from "styled-components";

import { FormMetadataType } from "@wrtn/core";

import FormSelector from "../../components/FormSelector";

import { colors, FlexButton, FlexWrapper, typo } from "../../styles";

interface FormContainerProps {
  forms: Array<FormMetadataType>;
  exampleForms: () => void;
  resetForms: () => void;
  updateForms: (id: string, targetForm: FormMetadataType) => void;
}

export const FormContainer = ({
  forms,
  exampleForms,
  resetForms,
  updateForms,
}: FormContainerProps) => {
  return (
    <FormScrollWrapper column>
      <ExampleButton onClick={exampleForms}>예시 텍스트</ExampleButton>
      <ResetButton onClick={resetForms}>전체 지우기</ResetButton>
      {forms.map((form) => {
        return (
          <FormSelector key={form.id} form={form} updateForm={updateForms} />
        );
      })}
    </FormScrollWrapper>
  );
};

//style

const FormScrollWrapper = styled(FlexWrapper)`
  flex: 1;

  gap: 30px;

  width: 100%;
  padding-top: 28px;
`;

const FormControlButton = styled(FlexButton)`
  z-index: 3;
  position: absolute;
  top: 20px;
  padding: 5px 9px;
  user-select: none;

  ${typo({
    weight: "600",
    size: "14px",
    height: "14px",
    color: colors.ACTION_BLUE,
  })}

  cursor: pointer;
  background-color: ${colors.BACKGROUND};
  border-radius: 5px;
  &:hover {
    opacity: 0.7;
  }
`;

const ResetButton = styled(FormControlButton)`
  right: 0px;
`;

const ExampleButton = styled(FormControlButton)`
  right: 90px;
`;
