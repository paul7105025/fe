import { Suspense } from "react";

import { ErrorBoundary, MFELoadingPage } from ".";

export const MFEWrapper = ({ children }) => {
  return (
    <Suspense
      fallback={
        // skeleton ? skeleton :
        // eslint-disable-next-line react/jsx-no-undef
        <MFELoadingPage />
      }
    >
      <ErrorBoundary>{children}</ErrorBoundary>
    </Suspense>
  );
};
