import styled from "styled-components";
import { FlexWrapper, typo, colors } from "../../styles";

export const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  gap: 20px;
`;

export const Waiting = styled.p`
  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: colors.gray_80,
  })}
`;
