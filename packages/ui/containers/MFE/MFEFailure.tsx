import { Spinner } from "../../components/Spinner";
import { FullButton } from "../../components/FullButton";
import { Wrapper, Waiting } from "./styles";

export const MFEFailure = () => {
  const handleReload = () => {
    window.location.reload();
  };

  return (
    <Wrapper column>
      <Spinner width={55} height={55} />
      <Waiting>페이지를 불러오지 못했어요. 다시 시도해주세요.</Waiting>
      <FullButton onClick={handleReload}>다시 시도하기</FullButton>
    </Wrapper>
  );
};
