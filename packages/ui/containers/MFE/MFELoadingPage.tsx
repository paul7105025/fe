import { Spinner } from "../../components/Spinner";

import { Wrapper, Waiting } from "./styles";

export const MFELoadingPage = () => {
  return (
    <Wrapper column justify="center">
      <Spinner width={55} height={55} />
      <Waiting>잠시만 기다려 주세요!</Waiting>
    </Wrapper>
  );
};
