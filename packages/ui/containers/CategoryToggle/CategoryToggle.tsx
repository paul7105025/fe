import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import { ReactComponent as IconArrow } from "../../assets/icons/icon_arrow.svg";
import { colors, FlexButton, FlexWrapper } from "../../styles";

import { ToolListItem } from "../../components/ToolListItem";
import { StarText, Star } from "../../components/SpecialText";

import { favoriteIdListState } from "@wrtn/core/stores";

import { ToolType, useEvent, useTagManager } from "@wrtn/core";
import polyfill from "../../styles/polyfill";

/**
 * @param {object} props
 * @param {function} props.currentToolId 해당 카테고리 툴 Id
 * @returns
 */

const dividerCategory = (category: string) => {
  if (category === "favorite") return true;
  if (category === "new") return true;

  return false;
};

interface ToggleProps {
  category: string;
  title: string;
  data: ToolType[];
  initialCollapsed: boolean;
  saveCollapse: () => void;
  outputLoading: boolean;
  searchInput: string;
  selectedToolId: string,
  onClick: (tool: ToolType) => void;
}

const Toggle = ({
  category,
  title,
  data = [],
  initialCollapsed = false,
  saveCollapse,
  outputLoading,
  searchInput,
  selectedToolId,
  onClick
}: ToggleProps) => {
// }: ToggleProps) => {
  // const pathname = window.location.pathname;
  // const toolId = pathname.split("/")[2];

  const favoriteIdList = useRecoilValue(favoriteIdListState);

  const triggerRef = React.useRef(null);

  const tagManager = useTagManager();

  const { collectEvent } = useEvent();

  const [isCollapsed, setIsCollapsed] = React.useState(initialCollapsed);

  const toggleCollapse = () => {
    setIsCollapsed((prev) => !prev);
    saveCollapse();
  };

  return (
    <ToggleWrapper column>
      <ToggleHeader onClick={toggleCollapse}>
        <ToggleLabel>{title}</ToggleLabel>
        {category === "new" && <StarText />}
        {category === "favorite" && <Star />}
        <div style={{ flex: 1 }} />
        <ToggleButton isCollapsed={isCollapsed}>
          <IconArrow />
        </ToggleButton>
      </ToggleHeader>
      <ToggleChildWrapper isCollapsed={isCollapsed} ref={triggerRef}>
        {data.map((item) => {
          return (
            <PositionWrapper key={item._id}>
              <ToolListItem
                name={item.name}
                tag={item.tag}
                icon={item.icon}
                selected={
                  item._id === selectedToolId
                }
                onClick={() => {
                  tagManager({
                    event: "click_tool_nav_item",
                    data: {
                      tool_name: item.name,
                      tool_id: item._id,
                      tool_tag: item.tag,
                      category: item.category,
                      current_category: title,
                      current_keyword: searchInput,
                      is_favorite: favoriteIdList.includes(item._id)
                        ? true
                        : false,
                    },
                  });
                  // collectEvent("click_tool_card", {
                  //   position: "left_nav",
                  //   feature_category: item.category,
                  //   feature_name: item.name,
                  //   feature_tag: item.tag,
                  //   feature_form: item.kind,
                  //   is_recomm_feature: false,
                  // });
                  // if (item._id !== id) {
                    // TODO: bundle 추가해야 함.
                    // TODO: 스텝, 번들, 툴 간 구별하는 부분 필요함.
                    onClick(item);
                    // if (item.kind === "step") {
                    //   // navigate(`/app/step/${item._id}`);
                    //   window.location.assign(`/app/step/${item._id}`);
                    // } else if (item.kind === "bundle") {
                    //   // navigate(`/app/bundle/${item._id}`);
                    //   window.location.assign(`/app/bundle/${item._id}`);
                    // } else {
                    //   // navigate(`/app/tool/${item._id}`);
                    //   window.location.assign(`/app/tool/${item._id}`);
                    // }
                  // }
                }}
                outputLoading={outputLoading}
              />
            </PositionWrapper>
          );
        })}
      </ToggleChildWrapper>
      {dividerCategory(category) && <Divider />}
    </ToggleWrapper>
  );
};

const CategoryToggle = React.memo(Toggle);

export default CategoryToggle;

const Divider = styled.div`
  width: 100%;
  height: 1px;
  margin-top: 8px;
  background-color: ${colors.WHITE_BOLDER_LINE};
`;

const ToggleWrapper = styled(FlexWrapper)`
  width: 100%;

  padding-bottom: 8px;
`;

const ToggleChildWrapper = styled.div<{ isCollapsed: boolean }>`
  width: 100%;
  gap: 8px;
  ${({ isCollapsed }) =>
    isCollapsed ? "max-height: 0" : polyfill.dvh("max-height", 100)};
  transition: max-height 0.3s cubic-bezier(0.1, -0.15, 0.6, 1.45);
  overflow: hidden;
`;

const ToggleHeader = styled(FlexWrapper)`
  width: 100%;

  cursor: pointer;
  padding: 0px 12px 0px 16px;
  user-select: none;
`;

const ToggleLabel = styled.p`
  font-weight: 600;
  font-size: 12px;
  line-height: 100%;
  color: #83879d;
`;

const ToggleButton = styled(FlexButton)<{ isCollapsed: boolean }>`
  width: 32px;
  height: 30px;

  cursor: pointer;
  > svg {
    path {
      fill: #83879d;
    }
  }
  transform: ${({ isCollapsed }) => isCollapsed && "rotate(-90deg)"};
  transition: transform 0.3s cubic-bezier(0.1, -0.15, 0.6, 1.75);
`;

const PositionWrapper = styled.div`
  position: relative;
`;
