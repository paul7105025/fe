# Wrtn Frontend

뤼튼 프론트엔드 

## Structure

- `/platforms`: 뤼튼의 플랫폼들을 모아놓은 곳.
- `/services` : 하위 mfe(micro frontend service)인 `chat`, `tool`, `editor`가 존재.
- `/packages`: core 라이브러리인 `core`, `ui`가 존재.

# Turborepo starter

레포 설치는 아래를 참고해주세요.

This is an official pnpm starter turborepo.

## What's inside?

This turborepo uses [pnpm](https://pnpm.io) as a package manager. It includes the following packages/apps:

### Utilities

This turborepo has some additional tools already setup for you:

- [TypeScript](https://www.typescriptlang.org/) for static type checking
- [ESLint](https://eslint.org/) for code linting
- [Prettier](https://prettier.io) for code formatting

### Build

To build all apps and packages, run the following command:

```
cd my-turborepo
pnpm run build
```

### Develop

To develop all apps and packages, run the following command:

```
cd my-turborepo
pnpm run dev
```

이거만으로 되면 좋겠지만 저희 설계 미숙으로 안됩니다...
메인 app 서비스 디렉토리로 들어가서 pnpm run dev로 여시고, 다른 mfe 서비스들도 각각의 디렉토리로 들어가서 pnpm run start로 켜줘야 합니다.
그리고 localhost:3000으로 접속해서 확인하시면 됩니다.

### Remote Caching

Turborepo can use a technique known as [Remote Caching](https://turbo.build/repo/docs/core-concepts/remote-caching) to share cache artifacts across machines, enabling you to share build caches with your team and CI/CD pipelines.

By default, Turborepo will cache locally. To enable Remote Caching you will need an account with Vercel. If you don't have an account you can [create one](https://vercel.com/signup), then enter the following commands:

```
cd my-turborepo
pnpm dlx turbo login
```

This will authenticate the Turborepo CLI with your [Vercel account](https://vercel.com/docs/concepts/personal-accounts/overview).

Next, you can link your Turborepo to your Remote Cache by running the following command from the root of your turborepo:

```
pnpm dlx turbo link
```

## Useful Links

Learn more about the power of Turborepo:

- [Tasks](https://turbo.build/repo/docs/core-concepts/monorepos/running-tasks)
- [Caching](https://turbo.build/repo/docs/core-concepts/caching)
- [Remote Caching](https://turbo.build/repo/docs/core-concepts/remote-caching)
- [Filtering](https://turbo.build/repo/docs/core-concepts/monorepos/filtering)
- [Configuration Options](https://turbo.build/repo/docs/reference/configuration)
- [CLI Usage](https://turbo.build/repo/docs/reference/command-line-reference)


## 깃헙 관리

develop 브랜치로 pr 날려주시면 됩니다.

