
const path = require("path");
const { getLoader, loaderByName } = require("@craco/craco");
const cracoModuleFederation = require('craco-module-federation');
const { dependencies, devDependencies } = require("./package.json");

const entryPath = './src/index';

/* You only need to edit the above variables. */
/* ########################################## */

const packages = [];

const localDependencies = Object.keys(dependencies).filter(v => v.startsWith("@wrtn/"))
// const localDevDependencies = Object.keys(devDependencies).filter(v => v.startsWith("@wrtn/"))

localDependencies.forEach(v => packages.push(path.dirname(require.resolve(v))))
// localDevDependencies.forEach(v => packages.push(path.dirname(require.resolve(v))))

module.exports = {
  webpack: {
    entry: entryPath,
    optimization: {
      splitChunks: {
        cacheGroups: {
          defaultVendors: {
            test: /[\\/]node_modules[\\/]/,
            chunks: "all",
            name: "libs",
            enforce: true,
            minChunks: 2,
          },
        },
      },
      runtimeChunk: { name: "libs" },
    },
    configure: (webpackConfig) => {
      const { isFound, match } = getLoader(
        webpackConfig,
        loaderByName("babel-loader")
      );
      webpackConfig.output.publicPath = 'auto';

      webpackConfig.experiments = {
        topLevelAwait : true
      }

      webpackConfig.optimization = {
        ...webpackConfig.optimization,
        runtimeChunk : false,
        splitChunks : false,

      }
      
      if (isFound) {
        const include = Array.isArray(match.loader.include)
          ? match.loader.include
          : [match.loader.include];

        match.loader.include = include.concat(packages);

        return webpackConfig;
      }
    },
  },
  plugins: [
    {
      plugin: cracoModuleFederation,
//    options: { useNamedChunkIds: true }
    }
  ]
};