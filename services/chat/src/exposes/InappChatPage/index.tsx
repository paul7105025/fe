import React from "react";
import axios from "axios";
import { useAlert } from "react-alert";
import styled, { css } from "styled-components";
import {
  RecoilRoot,
  useRecoilState,
  useRecoilValue,
  useSetRecoilState,
} from "recoil";
import TextareaAutosize from "react-textarea-autosize";
import { ReactComponent as ChatWrtnLogo } from "@wrtn/ui/assets/wrtn/logo_chat_wrtn.svg";
import {
  colors,
  FlexButton,
  FlexWrapper,
  typo,
  AutoSizeTextArea,
} from "@wrtn/ui/styles";
import { transitions, positions, Provider as AlertProvider } from "react-alert";

import {
  blockChatLimitState,
  blockChatUsageState,
  getChat_Id,
  postChat,
  postGenerateChat_Id,
  putChatMessage_Id,
  putUser,
  toolIdState,
  useEvent,
  useIntersectAll,
  useRefresh,
  userState,
  useTagManager,
} from "@wrtn/core";

import { IconPaperPlane, Refresh, IconCarouselDown } from "@wrtn/ui/assets";

import { Spinner } from "@wrtn/ui/components/Spinner";

import { ChatEmptyWrapper } from "../../components";

import { chatInappGuide } from "src/constants";

import ChatOutput from "src/components/ChatOutput";
import { ChatInappHeader } from "src/components/ChatInappHeader";
import { chatIdState } from "src/stores";
import { ToastMessage } from "@wrtn/ui/components/ToastMessage";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

export type ChatControllerProps = {
  toggleSideNavigation: () => {};
  defaultValue?: null | string;
};

const exampleList = [
  "좀 더 ...하게 바꿔줘",
  "분량 늘리기/줄이기",
  "존댓말/반말로 바꿔줘",
  "좀 더 감성적인 표현으로 바꿔줘",
  "요약해줘",
  "말투를 ...하게 바꿔줘",
];
// chatPage와 달리 router로 구분하지 않습니다.
export const InappChatPage = ({
  toggleSideNavigation,
  defaultValue,
}: ChatControllerProps) => {
  const [chatId, handleChangeChatId] = useRecoilState(chatIdState);
  const toolId = useRecoilValue(toolIdState);
  const user = useRecoilValue(userState);
  const [blockChatUsage, setBlockChatUsage] =
    useRecoilState(blockChatUsageState);

  const alert = useAlert();

  const tagManager = useTagManager();
  const { fetchChat } = useRefresh();
  const {
    collectEvent,
    collectUserProperties,
    collectUserProperties_increment,
    collectUserProperties_once,
  } = useEvent();

  const scrollRef = React.useRef<any>();
  const requestRef = React.useRef<any>();
  const inputRef = React.useRef<any>();

  const [input, setInput] = React.useState<string>("");
  const [focus, setFocus] = React.useState<any>(false);

  const [chat, setChat] = React.useState<any>(null);
  const [loading, setLoading] = React.useState(false);
  const [isReachEnd, setIsReachEnd] = React.useState(false);
  const [expand, setExpand] = React.useState<boolean>(true);

  const [defaultMessage, setDefaultMessage] = React.useState([]);

  React.useEffect(() => {
    if (!defaultValue) {
      setInput("");
      setDefaultMessage([]);
    }
    if (defaultValue) {
      setDefaultMessage([
        [
          {
            role: "assistant",
            type: "ToolOutput",
            toolId: window.location.pathname.split("/")[2],
            content: defaultValue,
          },
        ],
        {
          role: "assistant",
          type: "message",
          content: "이 문구에 대해 어떻게 수정해드릴까요?",
        },
      ]);
    }
  }, [defaultValue]);

  React.useEffect(() => {
    setDefaultMessage([]);
  }, [chatId]);

  const init = () => {
    setInput("");
    setDefaultMessage([]);
  };

  const setBlockChatLimit = useSetRecoilState(blockChatLimitState);

  const handleCreateChat = async () => {
    const res = await postChat();
    if (res?.status === 201) {
      const { data } = res.data;
      handleChangeChatId(data?._id);
      fetchChat();
      init();
      toggleSideNavigation();
    } else if (
      res.status === 403 &&
      res.data.message === "무료유저는 채팅방 1개만 사용할 수 있습니다."
    ) {
      setBlockChatLimit(true);
    }
  };

  const fetchChatById = async () => {
    if (chatId) {
      const res = await getChat_Id({ chatId });
      if (res.status === 200) {
        const { data } = res.data;
        setChat(data);
        setDefaultMessage([]);
      } else if (res.status === 404) {
        // navigate("/app/chat");
        handleChangeChatId(null);
        init();
      }
    }
  };

  const bottomRef = useIntersectAll(
    (entry, observer) => {
      if (!entry.isIntersecting && !isReachEnd && chat?.messages?.length > 0) {
        scrollToBottom(0, true);
      }
    },
    {
      threshold: 0,
    }
  );

  const scrollToBottom = (trial = 0, isSmooth = true) => {
    if (!scrollRef.current) {
      if (trial < 5) {
        setTimeout(() => {
          scrollToBottom(trial + 1);
        }, 100);
      }
      return;
    }

    scrollRef.current.scrollTo({
      top: scrollRef.current.scrollHeight,
      behavior: isSmooth ? "smooth" : "auto",
    });
    setIsReachEnd(true);
  };

  const generate = async (id, isMove = false) => {
    requestRef.current = axios.CancelToken?.source();
    const res = await postGenerateChat_Id({
      chatId: id,
      message: input,
      type: "mini",
      user: user
    });
    if (res?.status === 201) {
      setDefaultMessage([]);
      await fetchChatById();
      setInput("");
      setIsReachEnd(false);
      collectUserProperties_once("first_generate_date", new Date());
      collectUserProperties_increment("count_generate_accum", 1);
      collectUserProperties("last_generate_date", new Date());
      collectUserProperties_increment(
        "amount_generate_accum",
        res?.data?.content?.replace(/ /g, "")?.length
      );
      tagManager({
        event: "generate_done", 
        data: {
          feature_menu: "chat",
          feature_category: "",
          feature_name: "",
          feature_tag: "",
          feature_form: "",
          prompt_id: [],
          generate_character: res?.data?.content?.replace(/ /g, "")?.length,
          generate_token: "", // TODO: generate_token
          generate_model: "", // TODO: generate model
          input_tone_and_manner: "",
          input_category: "",
          input_continue_option: "",
          input_style: "",
          input_ratio: "",
        }
      })
      collectEvent("generate_done", {
        feature_menu: "chat",
        feature_category: "",
        feature_name: "",
        feature_tag: "",
        feature_form: "",
        prompt_id: [],
        generate_character: res?.data?.content?.replace(/ /g, "")?.length,
        generate_token: "", // TODO: generate_token
        generate_model: "", // TODO: generate model
        input_tone_and_manner: "",
        input_category: "",
        input_continue_option: "",
        input_style: "",
        input_ratio: "",
      });
      await putUser({
        data: {
          isNewbie: false
        }
      });
      if (isMove) {
        collectEvent("create_chat", {
          chat_type: "mini",
        });
        handleChangeChatId(id);
        init();
        fetchChat();
      }
      if (chat?.messages?.length < 3) {
        fetchChat();
      }
    } else if (res.status === 403) {
      setBlockChatUsage(true);
    }
  };

  const initialGenerate = async () => {
    const res = await postChat();
    if (res?.status === 201) {
      const { data } = res.data;
      if (data._id) {
        await generate(data._id, true);
      }
    } else if (res.status === 403) {
      setBlockChatLimit(true);
    }
  };

  const generateReset = async () => {
    requestRef.current = axios.CancelToken?.source();
    const res = await postGenerateChat_Id({
      chatId: chatId || "",
      message: input,
      reroll: true,
      type: "mini",
      user: user,
    });
    if (res?.status === 201) {
      collectUserProperties_once("first_generate_date", new Date());
      collectUserProperties_increment("count_generate_accum", 1);
      collectUserProperties("last_generate_date", new Date());
      collectUserProperties_increment(
        "amount_generate_accum",
        res?.data?.content?.replace(/ /g, "")?.length
      );
      tagManager({
        event: "generate_done", 
        data: {
          feature_menu: "chat",
          feature_category: "",
          feature_name: "",
          feature_tag: "",
          feature_form: "",
          prompt_id: [],
          generate_character: res?.data?.content?.replace(/ /g, "")?.length,
          generate_token: "", // TODO: generate_token
          generate_model: "", // TODO: generate model
          input_tone_and_manner: "",
          input_category: "",
          input_continue_option: "",
          input_style: "",
          input_ratio: "",
        }
      })
      collectEvent("generate_done", {
        feature_menu: "chat",
        feature_category: "",
        feature_name: "",
        feature_tag: "",
        feature_form: "",
        prompt_id: [],
        generate_character: res?.data?.content?.replace(/ /g, "")?.length,
        generate_token: "", // TODO: generate_token
        generate_model: "", // TODO: generate model
        input_tone_and_manner: "",
        input_category: "",
        input_continue_option: "",
        input_style: "",
        input_ratio: "",
      });
      await putUser({
        data: {
          isNewbie: false
        }
      });
      await fetchChatById();
    } else if (
      res.status === 403 &&
      res.data.message === "미니 챗에서 더 이상 사용할 수 없습니다."
    ) {
      setBlockChatUsage(true);
    }
  };

  const copy = async () => {
    alert.removeAll();
    alert.show("결과물을 복사했어요.");
  };

  const reset = async () => {
    if (loading) {
      requestRef.current?.cancel();
      setLoading(false);
    } else {
      setLoading(true);
      await generateReset();
      setLoading(false);
    }
  };

  const submit = async () => {
    if (!loading) {
      setLoading(true);
      if (chatId) {
        handleChangeChatId(chatId);
        await generate(chatId);
      } else {
        await initialGenerate();
      }
      setLoading(false);
    }
  };

  const isPaidUser = React.useMemo(() => {
    return user?.plan !== "FREE" || user?.email.includes("@wrtn.io");
  }, [user]);

  React.useEffect(() => {
    if (chatId) {
      fetchChatById();
      setIsReachEnd(false);
    }
  }, [chatId]);

  const [chatCommandIdx, setChatCommandIdx] = React.useState<null | number>(
    null
  );

  const handleChangeCommand = (index: number) => {
    if (chatCommandIdx === index) setChatCommandIdx(null);
    else {
      setInput((c) => c + " " + exampleList[index]);
      setChatCommandIdx(index);
    }
  };

  const handleClickFavorite = async (id: string) => {
    const res = await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: true,
        disliked: false,
      },
    });
  };

  const handleClickDislike = async (id: string) => {
    const res = await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: false,
        disliked: true,
      },
    });
  };

  return (
    <OuterWrapper>
      <InnerWrapper>
        <ChatInappHeader
          chatId={chatId}
          toggleSideNavigation={toggleSideNavigation}
          onCreateChat={handleCreateChat}
        />
        <Wrapper>
          <BoxWrapper ref={scrollRef}>
            <BoxScrollWrapper>
              {/* { defaultValue && 
                defaultMessage.map((item, idx) => {
                    
                })
              } */}
              {(chat?.messages || [])
                .concat(defaultMessage)
                ?.map((item, idx) => {
                  if (Array.isArray(item)) {
                    if (
                      item?.length &&
                      item[0].role === "assistant" &&
                      item[0].type === "ToolOutput"
                    ) {
                      const toolId = item[0].toolId;
                      return (
                        <ChatOutput.ToolOutput
                          contents={item}
                          toolId={toolId}
                          onCopy={copy}
                          chatId={chatId}
                          // withLeftBottom={() => lastChatRegen(idx === chat?.messages?.length - 1 && !manageLoading.loading)}
                          // withLeftTop={({ idx, add, subtract, canAdd, canSubtract}) => {
                          //   return (
                          //     <ToolListPaginationButtonWrapper>
                          //       <IconRoundCarouselLeft isDisabled={canSubtract} onClick={subtract}/>
                          //         { (idx + 1).toString() } / { item.length}
                          //       <IconRoundCarousel isDisabled={canAdd} onClick={add}/>
                          //     </ToolListPaginationButtonWrapper>
                          //   )
                          // }}
                        />
                      );
                    } else if (
                      item[0].role === "assistant" &&
                      item[0].type === "message"
                    ) {
                      return (
                        <ChatOutput.Assistant
                          contents={item}
                          onCopy={copy}
                          chatId={chatId}
                          // withLeftBottom={() =>
                          //   lastChatRegen(
                          //     idx === chat?.messages?.length - 1 &&
                          //       !manageLoading.loading
                          //   )
                          // }
                          // withLeftTop={({
                          //   idx,
                          //   add,
                          //   subtract,
                          //   canAdd,
                          //   canSubtract,
                          // }) => {
                          //   return (
                          //     <ToolListPaginationButtonWrapper>
                          //       <IconRoundCarouselLeft
                          //         isDisabled={canSubtract}
                          //         onClick={subtract}
                          //       />
                          //       {(idx + 1).toString()} / {item.length || 0}
                          //       <IconRoundCarousel
                          //         isDisabled={canAdd}
                          //         onClick={add}
                          //       />
                          //     </ToolListPaginationButtonWrapper>
                          //   );
                          // }}
                        />
                      );
                    }
                  } else if (
                    item.role === "assistant" &&
                    item.type === "message"
                  ) {
                    return (
                      <ChatOutput.Assistant
                        contents={item}
                        onCopy={copy}
                        chatId={chatId}
                        // withLeftBottom={() => lastChatRegen(idx === chat?.messages?.length - 1 && !manageLoading.loading)}
                        // withLeftTop={({ idx, add, subtract, canAdd, canSubtract}) => {
                        //   return (
                        //     <ToolListPaginationButtonWrapper>
                        //       <IconRoundCarouselLeft isDisabled={canSubtract} onClick={subtract}/>
                        //         { (idx + 1).toString() } / { item.length || 0 }
                        //       <IconRoundCarousel isDisabled={canAdd} onClick={add}/>
                        //     </ToolListPaginationButtonWrapper>
                        //   )
                        // }}
                      />
                    );
                  } else if (item.role === "user" && item.type === "message") {
                    return <ChatOutput.User message={item} />;
                  } else if (
                    item.role === "user" &&
                    item.type === "ToolInput"
                  ) {
                    return <ChatOutput.ToolInput message={item} />;
                  } else if (item.role === "user" && item.type === "Chip") {
                    return <ChatOutput.User message={item} />;
                  }
                  // if (item.role === "assistant") {
                  //   return (
                  //     <ChatOutput.Assistant
                  //       message={item}
                  //       onCopy={copy}
                  //       chatId={chatId}
                  //     />
                  //   );
                  // } else {
                  //   return <ChatOutput.User message={item} />;
                  // }
                })}
              <div
                ref={bottomRef}
                style={{
                  marginTop: 195,
                  width: "100%",
                  height: 140,
                }}
              />
            </BoxScrollWrapper>
            <ShadowWrapper />
          </BoxWrapper>
          {(!chatId || chat?.messages?.length < 1) && !defaultValue && (
            <ChatEmptyWrapper
              isFree={!isPaidUser}
              guide={chatInappGuide}
              isMini={true}
            />
          )}
        </Wrapper>
        {/* {isPaidUser && ( */}
        <InterfaceWrapper>
          {defaultValue && (
            <ChatCommandWrapper column>
              <ChatCommandTitle onClick={() => setExpand((c) => !c)}>
                예시 <CarouselDown isupper={expand} />
              </ChatCommandTitle>
              {expand && (
                <>
                  <ChatCommandDesc>
                    결과물을 원하는대로 가공해보세요. 챗 뤼튼의 답변에
                    구체적으로 피드백을 주면 더 잘 알아들어요.
                  </ChatCommandDesc>
                  <ChatCommandButtons>
                    {exampleList.map((v, i) => (
                      <ChatCommandButton
                        onClick={() => handleChangeCommand(i)}
                        selected={chatCommandIdx === i}
                        key={v}
                      >
                        {v}
                      </ChatCommandButton>
                    ))}
                  </ChatCommandButtons>
                </>
              )}
            </ChatCommandWrapper>
          )}
          {!defaultValue &&
            chatId &&
            chat?.messages &&
            chat?.messages?.length > 0 && (
              <InteractButton onClick={reset}>
                {loading ? (
                  <React.Fragment>생성 멈추기</React.Fragment>
                ) : (
                  <React.Fragment>
                    <Refresh />
                    다시 생성하기
                  </React.Fragment>
                )}
              </InteractButton>
            )}
          <InputWrapper focus={focus}>
            <Input
              ref={inputRef}
              value={input}
              disabled={!user}
              onChange={(e) => {
                if (!user) return;
                setInput(e.target.value);
              }}
              onFocus={() => {
                if (!user) return;
                setFocus(true);
              }}
              onBlur={() => {
                if (!user) return;
                setFocus(false);
              }}
              maxRows={5}
              onKeyDown={(e) => {
                if (!user) return;
                if (
                  !e.metaKey &&
                  e.shiftKey &&
                  e.key === "Enter" &&
                  input.trim().length > 0
                ) {
                  e.preventDefault();
                  submit();
                }
              }}
            />
            <InputButton
              onClick={submit}
              disabled={loading || input.trim().length === 0}
            >
              {loading ? (
                <Spinner width={16} height={16} color={colors.WHITE} />
              ) : (
                <IconPaperPlane />
              )}
            </InputButton>
          </InputWrapper>
        </InterfaceWrapper>
        {/* )} */}
      </InnerWrapper>
    </OuterWrapper>
  );
};

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

const queryClient = new QueryClient();

const InappChat = ({ toggleSideNavigation, defaultValue }) => {
  return (
    <AlertProvider template={ToastMessage} {...alertOptions}>
      <QueryClientProvider client={queryClient}>
        <RecoilRoot override={false}>
          <InappChatPage
            toggleSideNavigation={toggleSideNavigation}
            defaultValue={defaultValue}
          />
        </RecoilRoot>
      </QueryClientProvider>
    </AlertProvider>
  );
};
export default InappChat;

const OuterWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const InnerWrapper = styled(FlexWrapper)`
  flex: 1;
  flex-direction: column;
  justify-content: flex-start;
`;

const Logo = styled(ChatWrtnLogo)`
  width: 46px;
  height: 46px;
`;

const Wrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  height: 100%;
  width: 100%;
  flex: 1;
  text-align: center;
`;

const InputWrapper = styled(FlexWrapper)<{ focus: boolean }>`
  padding: 6px 6px 6px 20px;
  background: #f2f7ff;
  border-radius: 8px;
  max-width: 712px;
  gap: 8px;
  width: 100%;

  ${({ focus }) =>
    focus &&
    css`
      border: 1px solid ${colors.ACTION_BLUE};
      padding: 5px 5px 5px 19px;
    `}
`;

const Input = styled(AutoSizeTextArea)`
  flex: 1;
  background: none;
  width: 100%;
  ${typo({ weight: 600, size: "16px", height: "160%", color: colors.gray_90 })};

  resize: none;
`;

const InputButton = styled.button`
  border: none;
  height: 36px;
  width: 70px;
  display: flex;
  align-items: center;
  justify-content: center;

  border-radius: 6px;
  background: linear-gradient(
    90.34deg,
    #c57dff -21.05%,
    #669cff 70.31%,
    #99e5f4 169.62%
  );

  &:hover {
    cursor: pointer;
  }

  &:disabled {
    background: ${colors.gray_55};
    cursor: not-allowed;
  }
`;

const InterfaceWrapper = styled(FlexWrapper)`
  background: linear-gradient(0deg, #fff 90%, rgba(255, 255, 255, 0) 100%);
  width: 100%;

  position: absolute;
  bottom: 24px;
  left: 0;
  padding: 10px 20px 0px 20px;

  gap: 16px;
  flex-direction: column;
  @media (max-width: 767px) {
    bottom: 22px;
  }
`;

const BoxWrapper = styled.div`
  display: flex;
  flex-direction: column;

  padding: 0px 20px;
  width: 100%;
  flex: 1;

  max-height: calc(100vh - 120px);
  max-height: calc(100dvh - 120px);

  overflow: scroll;
`;

const BoxScrollWrapper = styled.div`
  max-width: 712px;
  width: 100%;

  margin: auto auto 0px;

  padding: 40px 0px 0px;
`;

const ChatBoxWrapper = styled.div`
  position: relative;
  background: white;
  display: flex;
  border: 1px solid #d1d7ed;
  border-radius: 8px;
  padding: 32px 38px 34px 31px;
  margin-bottom: 20px;
  gap: 30px;
  text-align: justify;
`;

const InteractButton = styled(FlexButton)`
  background: #ffffff;
  border: 1.5px solid ${colors.POINT_PURPLE};
  border-radius: 30px;
  ${typo({
    weight: 600,
    size: "16px",
    height: "160%",
    color: colors.POINT_PURPLE,
  })};

  position: absolute;
  bottom: 100%;
  margin-bottom: 16px;

  width: 100%;
  max-width: 200px;

  left: 50%;
  padding: 7px;
  cursor: pointer;
  transform: translateX(-50%);

  gap: 10px;

  svg {
    width: 16px;
    height: 16px;
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const ShadowWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 215px;

  position: absolute;
  bottom: 0;
  left: 0;

  background: linear-gradient(0deg, #ffffff 100%, rgba(255, 255, 255, 0) 100%);
`;

const ChatCommandButton = styled(FlexButton)<{ selected: boolean }>`
  border-radius: 30px;
  padding: 10px 12px;
  cursor: pointer;

  background-color: ${(props) =>
    props.selected ? colors.POINT_PURPLE : "#DED5F8"};
  ${(props) =>
    typo({
      weight: 600,
      size: "14px",
      height: "100%",
      color: props.selected ? colors.WHITE : colors.POINT_PURPLE,
    })}
`;
const ChatCommandButtons = styled(FlexWrapper)`
  flex-wrap: wrap;
  gap: 9px;
`;

const ChatCommandWrapper = styled(FlexWrapper)`
  align-content: center;
  gap: 9px;
`;

const ChatCommandTitle = styled(FlexWrapper)`
  ${typo({
    weight: 600,
    size: "14px",
    height: "26px",
    color: colors.POINT_PURPLE,
  })};
  cursor: pointer;
`;

const ChatCommandDesc = styled(FlexWrapper)`
  ${typo({
    weight: 600,
    size: "12px",
    height: "160%",
    color: colors.gray_60,
  })};
  text-align: center;
`;

const CarouselDown = styled(IconCarouselDown)<{ isupper: boolean }>`
  transform: ${(props) => (props.isupper ? "rotate(180deg)" : "rotate(0deg)")};
  transition: 0.5s;
`;
