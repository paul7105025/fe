import { Icon, colors, FlexWrapper, newColors } from "@wrtn/ui"
import React from "react"
import styled from "styled-components"

const ImageNavigation = () => {
  return (
    <NavWrapper>
      <NavHeader>
        <TitleIcon>
          <Icon icon="image-v" size={16} color={newColors.GRAY_300} />
          <p style={{ fontSize: "16px", fontWeight: "600", color: newColors.GRAY_300 }}>이미지</p>
        </TitleIcon>
      </NavHeader>
    </NavWrapper>
  )
}

export default ImageNavigation

const NavWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 16px 0;
  flex-direction: column;
  align-items: inherit;
`

const NavHeader = styled(FlexWrapper)`
  flex-direction: row;
  justify-content: space-between;
  padding: 0 16px 6px;
`

const TitleIcon = styled(FlexWrapper)`
  flex-direction: row;
  gap: 10px;
`