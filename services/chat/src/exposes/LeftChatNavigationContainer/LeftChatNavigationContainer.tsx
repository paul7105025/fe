import React from "react";
import dayjs from "dayjs";
import styled from "styled-components";
import { useAlert } from 'react-alert';
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";

import {
  accessTokenState,
  addInterceptors,
  blockChatLimitState,
  chatListState,
  chatLoadingState,
  chatType,
  nextRouteState,
  postChat,
  syncHeader,
  useEvent,
  useIsMobile,
  useLoginDialog,
  useModalOpen,
  useRefresh,
  useSync,
  userState,
} from "@wrtn/core";

import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import { useFetchChat, useFetchFile } from "src/hooks";

import { Dialog } from "@wrtn/ui/components/Dialog";
import { colors, typo, FlexWrapper } from "@wrtn/ui/styles";

import { chatIdState } from "src/stores";
import { NavItem } from "src/components/NavItem";
import { IconAddChat } from "@wrtn/ui";
import { ReactComponent as ChatWrtnLogo } from "@wrtn/ui/assets/wrtn/logo_chat_wrtn.svg";
import FileNaviation from "./FileNavigation";
import ImageNavigation from "./ImageNavigation";
import Loading from "@wrtn/ui/components/Loading";

interface ChatNavbarProps {
  isMobile?: boolean;
  toggleSideNavigation?: () => void;
  isSideNavigationOpened?: boolean;
  handleChangeChatId: (id: string | null) => {};
  chatId?: string | null;
  onClose?: () => void;
}

type GroupedChatType = {
  date: string;
  child: Array<chatType>;
};

const LeftChatNavigationContainer = ({
  isMobile = false,
  onClose = () => {},
}: ChatNavbarProps) => {
  useSync();

  const [accessToken, setAccessToken] = useRecoilState(accessTokenState);
  const [blockChatLimit, setBlockChatLimit] =
    useRecoilState(blockChatLimitState);
  const user = useRecoilValue(userState);

    
  const router = useRecoilValue(nextRouteState);
  const chatList = useRecoilValue(chatListState);
  const chatLoading = useRecoilValue(chatLoadingState);
  
  const [chatId, setChatId] = useRecoilState(chatIdState);

  const [chatListInitLoading, setChatListInitLoading] = React.useState(true);

  const { collectEvent } = useEvent();
  const alert = useAlert();
  const isNavMobile = useIsMobile() || isMobile;

  const { fetchChat } = useRefresh();
  const { fetchChatById } = useFetchChat();
  const { handleOpen: handleLoginDialogOpen } = useLoginDialog();
  const { fetchFilesByUserId } = useFetchFile();
  const banAbuseModal = useModalOpen({ modalId: "banAbuseModal" });

  React.useEffect(() => {
    syncHeader({
      key: "Authorization",
      value: `Bearer ${accessToken}`,
    });
  }, []);

  React.useEffect(() => {
    fetchFilesByUserId();
  }, []); 

  const move = async (id: string) => {
    setChatId(id);
    if (id !== chatId) fetchChatById(id);
    // if (isNavMobile) {
      onClose();
    // }
  };

  const createChatOnly = async () => {
    const res = await postChat();
    if (res?.status === 201) {
      const { data } = res.data;
      move(data?._id);
    } else if (res.status === 401) {
      handleLoginDialogOpen();
    } else if (res.status === 403) {
      setBlockChatLimit(true);
    }
  };

  const start = async () => {
    if (chatLoading) return;
    collectEvent("create_chat", {
      chat_type: "main",
    });
    await createChatOnly();
    await fetchChat();
  };

  React.useEffect(() => {
    fetchChat().then(() => {
      setChatListInitLoading(false);
    })
  }, []);

  const groupedList: chatType[] = React.useMemo(() => {
    return [...chatList]?.sort((a, b) =>
      dayjs(a.updatedAt).isBefore(b.updatedAt) ? 1 : -1
    );
  }, [chatList]);

  if (chatListInitLoading) return (
    <Loading />
  )

  return (
    <NavWrapper column>
      <NavHeader isMobile={isMobile}>
        <ChatLogo/>
        <UserText>{user?.name ? `${user.name}의 어시스턴트` : "AI 어시스턴트 뤼튼"}</UserText>
      </NavHeader>
      <NavBody>
        <FileNaviation isMobile={isMobile}/>
        <ImageNavigation />
        <DivideWrapper>
          <TitleText>채팅 목록</TitleText>
          <IconWrapper onClick={start}>
            <IconAddChat />
          </IconWrapper>
        </DivideWrapper>
        {groupedList.map((chat, idx) => (
          <NavItem item={chat} move={move} />
        ))}
        {/* {Object.values(groupedList).map(
          (parent: GroupedChatType, idx: number) => {
            return (
              <div key={idx.toString()}>
                <DateWrapper>
                  <DateText>{parent.date}</DateText>
                </DateWrapper>
                {parent.child.map((item: chatType) => {
                  return <NavItem key={item._id} item={item} move={move} />;
                })}
              </div>
            );
          }
        )} */}
      </NavBody>
      {blockChatLimit && (
        <ModalPortal onClose={() => setBlockChatLimit(false)}>
          <Dialog
            width={440}
            iconType="surprise"
            title={"무료 요금제에서는 채팅방을 3개까지 생성할 수 있어요"}
            description={[
              "기존 채팅방을 지우면 새로운 채팅방을 생성할 수 있어요.",
              "요금제를 업그레이드하면 제한 없이 사용할 수 있어요.",
            ]}
            handleClose={() => setBlockChatLimit(false)}
            rightButtonLabel="요금제 업그레이드"
            handleRightButton={() => {
              collectEvent("view_plan_page", {
                position: "download",
              });
              setBlockChatLimit(false);
              router.push("/setting/plan");
            }}
            disableCancel={true}
          />
        </ModalPortal>
      )}
    </NavWrapper>
  );
};

export default LeftChatNavigationContainer;

const NavWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  background: #f2f7ff;
  overflow: hidden;

  justify-content: flex-start;
  align-items: flex-start;
  align-self: stretch;
  @media (max-width: 767px) {
    background: white;
    width: 100%;
  }
`;

const NavHeader = styled.div<{ isMobile: boolean }>`
  width: 100%;
  padding: ${({ isMobile }) => (isMobile ? 16 : 20)}px 16px;
  padding-right: 27px;
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px;
`;

const NavBody = styled.div`
  width: 100%;
  overflow: scroll;
`;

const DivideWrapper = styled(FlexWrapper)`
  padding: 25px 16px 8px;
  width: 100%;
  border-top: 1px solid ${colors.WHITE_BOLDER_LINE};
  justify-content: space-between;
`;

const UserText = styled.p`
  ${typo({
    weight: 700,
    size: "16px",
    height: "100%",
    color: colors.gray_80,
  })}
`

const TitleText = styled.p`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.gray_60,
  })};
`;

const ChatLogo = styled(ChatWrtnLogo)`
  width: 32px;
  height: 32px;
`;

const IconWrapper = styled(FlexWrapper)`
  background: white;
  border-radius: 32px;
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  align-items: center;
  width: 32px;
  height: 32px;
  justify-content: center;
  cursor: pointer;
`