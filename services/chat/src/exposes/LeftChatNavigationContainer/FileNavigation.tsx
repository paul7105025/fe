import React, { useMemo, useState } from "react"
import { FlexWrapper, Icon, IconChevron, colors } from "@wrtn/ui"
import styled from "styled-components"
import FileNavItem from "src/components/FileNavItem";
import { animated, useTransition } from "@react-spring/web";
import { useRecoilValue, useRecoilState } from "recoil";
import { allFilesState, currentFileState } from "@wrtn/core";

const FileNaviation = ({ isMobile }) => {
  const [showItems, setShowItems] = useState(false);
  const files = useRecoilValue(allFilesState)
  const [currentFile, setCurrentFile] = useRecoilState(currentFileState);

  const showTransition = useTransition(showItems, {
    from: { maxHeight: 0 },
    enter: { maxHeight: 20000 },
    leave: { maxHeight: 0 },
    config: { duration: 200 },
    unique: true,
  });

  const filteredFiles = useMemo(() => files.filter(file => !file.isDeleted), [files]);

  return (
    <NavWrapper>
      <NavHeader>
        <TitleIcon>
          <Icon icon="paperclip" size={16}/>
          <p style={{ fontSize: "16px", fontWeight: "600", color: colors.GRAY_80 }}>파일</p>
        </TitleIcon>
        <IconArrow isCollapsed={showItems} onClick={() => setShowItems(!showItems)}/>
      </NavHeader>
      {showTransition(
        (style, item) =>
        item && (
          <animated.div style={style}>
            {filteredFiles.length === 0 && <FileNavItem isSelected={false} title="파일이 없습니다." disabled={true}/>}
            {filteredFiles.map((file, idx) => {
              return (
                <FileNavItem
                  key={file._id}
                  fileId={file._id}
                  isSelected={currentFile ? currentFile._id === file._id : false}
                  title={file.fileName ?? "empty.pdf"}
                  onClick={() => setCurrentFile(file)}
                />
              )
            })}
          </animated.div>
        )
      )}
    </NavWrapper>
  )
}

export default FileNaviation

const NavWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 10px 0 0;
  flex-direction: column;
  align-items: inherit;
  transition: opacity 0.3s ease-in-out;
`

const NavHeader = styled(FlexWrapper)`
  flex-direction: row;
  justify-content: space-between;
  padding: 0 16px 6px;
`

const TitleIcon = styled(FlexWrapper)`
  flex-direction: row;
  gap: 10px;
`

const IconArrow = styled(IconChevron)<{ isCollapsed: boolean }>`
  cursor: pointer;
  transform: ${({ isCollapsed }) =>
    isCollapsed ? "rotate(180deg)" : "rotate(90deg)"};
  transition: transform 0.3s ease-in-out;
  user-select: none;
  path {
    fill: ${colors.GRAY_90};
  }
`;
