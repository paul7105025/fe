import React from "react";
import styled from "styled-components";
import { RecoilRoot, useRecoilState, useRecoilValue } from "recoil";

import { Provider as AlertProvider } from "react-alert";
import {
  useQuery,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";

import { FlexWrapper } from "@wrtn/ui/styles";

import { ToastMessage } from "@wrtn/ui/components/ToastMessage";

import {
  ToolType,
  alertOptions,
  currentToolChatIdState,
  streamMessageState,
  getTool_Id,
  useRefresh,
  useSync,
  chatListState,
} from "@wrtn/core";

import { chatIdState, chipTutorialState } from "src/stores";

import { ChatEmpty } from "src/components";
import {
  useChatLoading,
  useScrollRef,
  useGenerate,
  useHandleAllMessage,
  useFetchChat,
  useChipList,
} from "src/hooks";

import { LastChatRegen } from "src/components/LastChatRegen";
import { ChatInputContainer } from "src/containers/ChatInputContainer";
import { ChatScrollContainer } from "src/containers/ChatScrollContainer";
import { useFileChatGenerate } from "src/hooks/file/useFileChatGenerate";

const ChatController = ({ renderAuth }) => {
  useSync();
  useFetchChat();
  useChipList();

  const chatId = useRecoilValue(chatIdState);
  const chatList = useRecoilValue(chatListState);
  const chipTutorial = useRecoilValue(chipTutorialState);
  const streamMessage = useRecoilValue(streamMessageState);

  const [toolId, setToolId] = useRecoilState(currentToolChatIdState);

  const [input, setInput] = React.useState("");

  const { scrollRef } = useScrollRef();
  const { allMessages } = useHandleAllMessage();

  const { data: tool } = useQuery<ToolType>({
    queryKey: [toolId],
    queryFn: () =>
      toolId && getTool_Id({ toolId }).then((res) => res.data.data),
    keepPreviousData: true,
  });

  const { fetchFavorite } = useRefresh();

  const manageLoading = useChatLoading();

  const {
    handleClickGenerate,
    handleClickRegenerate,
    handleStopGenerate,
    formData,
  } = useGenerate({ tool, input, setInput });
  const { generateFileChat, reGenerateFileChat } = useFileChatGenerate({
    setInput,
  });

  React.useEffect(() => {
    fetchFavorite();
  }, []);

  React.useEffect(() => {
    setToolId(null);
  }, [chatId]);

  const hasMessage = allMessages.length >= 1;

  return (
    <OuterWrapper>
      <InnerWrapper>
        <Wrapper>
          <BoxWrapper ref={scrollRef} id="ScrollContainer">
            <ChatScrollContainer />
            <ShadowWrapper />
          </BoxWrapper>
          <ChatEmpty
            renderAuth={renderAuth}
            isShow={
              !manageLoading.loading &&
              (!chatId || allMessages.length < 1) &&
              !chipTutorial &&
              chatList.length > 0
            }
          />
          <ButtonWrapper>
            <AbsoluteWrapper>
              {hasMessage && chatId && (
                <LastChatRegen
                  isStream={streamMessage.length > 0}
                  showToolInput={!!tool}
                  handleStopGenerate={handleStopGenerate}
                  handleClickRegenerate={handleClickRegenerate}
                  handleClickRegenerateFile={reGenerateFileChat}
                />
              )}
            </AbsoluteWrapper>
          </ButtonWrapper>
          <InterfaceWrapper>
            <ChatInputContainer
              input={input}
              setInput={setInput}
              tool={tool}
              formData={formData}
              loading={manageLoading.loading}
              hasEnoughForShare={allMessages.length > 1}
              handleClickGenerate={handleClickGenerate}
              handleClickGenerateFile={generateFileChat}
            />
          </InterfaceWrapper>
        </Wrapper>
      </InnerWrapper>
    </OuterWrapper>
  );
};

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

const ChatPage = ({ renderAuth }) => {
  return (
    <AlertProvider template={ToastMessage} {...alertOptions}>
      <QueryClientProvider client={queryClient}>
        <RecoilRoot override={false}>
          <ChatController renderAuth={renderAuth} />
        </RecoilRoot>
      </QueryClientProvider>
    </AlertProvider>
  );
};

export default ChatPage;

const OuterWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const InnerWrapper = styled(FlexWrapper)`
  height: 100%;
  width: 100%;
  flex-direction: column;
`;

const Wrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;

  width: 100%;
  height: 100%;
  text-align: center;

  @media (max-width: 767px) {
    min-height: calc(100% + 20px);
  }
`;

const BoxWrapper = styled.div`
  display: flex;
  flex-direction: column;

  padding: 0px 20px;
  width: 100%;
  flex: 1;

  max-height: calc(100vh - 120px);

  overflow: scroll;

  @media (max-width: 767px) {
    max-height: calc(100vh - 160px);
  }
`;

const InterfaceWrapper = styled(FlexWrapper)`
  background: linear-gradient(0deg, #ffffff 85%, rgba(255, 255, 255, 0) 100%);
  width: 100%;
  bottom: 24px;
  left: 0;
  padding: 20px 20px 0px;
  // padd-top: 40px;

  flex-direction: column;
  @media (max-width: 767px) {
    bottom: 20px;
    padding: 20px 0px 0px;

    /* position: absolute; */
  }
`;

const ShadowWrapper = styled(FlexWrapper)`
  width: 100%;
  // height: 215px;

  position: absolute;
  bottom: 0;
  left: 0;
`;

const ButtonWrapper = styled(FlexWrapper)`
  display: flex;
  justify-content: center;
`;

const AbsoluteWrapper = styled.div`
  position: absolute;
  bottom: 10px;
`;