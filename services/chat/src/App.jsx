import React from 'react';
import { RecoilRoot } from "recoil";
import { Routes, BrowserRouter, Route } from "react-router-dom";
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import { ToastMessage } from "@wrtn/ui/components/ToastMessage";
import ChatPage from './exposes/ChatPage';

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

function App() {
  return (
    <AlertProvider template={ToastMessage} {...alertOptions}>
      <RecoilRoot>
        <BrowserRouter>
          <Routes>
            <Route path="app">
              <Route path="" element={<ChatPage /> } />
              <Route path=":chatId" element={<ChatPage /> } />
            </Route>
          </Routes>
        </BrowserRouter>
      </RecoilRoot>
    </AlertProvider>
  );
}

export default App;