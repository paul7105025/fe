import {
  FormMetadataType,
  chatListState,
  isDialogOpenState,
  messageType,
  userState,
} from "@wrtn/core";
import { atom, selector } from "recoil";
export type chatChipType = "tool" | "commend";

export const chatIdState = atom<string | null>({
  key: "@wrtn/chat/chatIdState",
  default: null,
});

export const chatLoadingMessageState = atom<messageType[]>({
  key: "@wrtn/chat/chatLoadingMessageState",
  default: [],
});

export const chatChipTypeState = atom<chatChipType>({
  key: "@wrtn/chat/chatChipTypeState",
  default: "tool",
});

type generatedToolChatType = {
  toolId: string | null;
  forms: FormMetadataType[] | null;
};

export const previousGeneratedToolChatState = atom<generatedToolChatType>({
  key: "@wrtn/chat/previousGeneratedToolChatState",
  default: {
    toolId: null,
    forms: null,
  },
});

const exampleList = [
  {
    name: "원어민이 많이 쓰는 영어 표현 10개는?",
    id: "63b2b5587b66829fa8483b89",
    dynamic: false,
  },
  {
    name: "다이어트 식단을 표로 짜줘",
    id: "6302e4fba4a7d28a2dc83153",
    dynamic: false,
  },
  {
    name: "ENFP의 특징은?",
    id: "6412b907270278c8665625cf",
    dynamic: false,
  },
];

export const chatChipListState = atom({
  key: "@wrtn/chat/chatChipListState",
  default: exampleList,
});

export const chatTooltipTutorialState = atom({
  key: "@wrtn/chat/chatTooltipTutorialState",
  default: [false, false, false],
});

export const chatTrailState = atom({
  key: "@wrtn/chat/chatTrailState",
  default: false,
});

export const chatScrollToBottomState = atom<
  (trial?: number, isSmooth?: boolean) => void
>({
  key: "@wrtn/chat/chatScrollToBottomState",
  default: (trial?: number, isSmooth?: boolean) =>
    console.log("chatScrollToBottomState", trial, isSmooth),
});

export const chipTutorialState = selector<boolean>({
  key: "@wrtn/chat/chipTutorialState",
  get: ({ get }) => {
    const user = get(userState);
    const chatId = get(chatIdState);
    const chatList = get(chatListState);
    const isDialogOpen = get(isDialogOpenState);

    if (!user) {
      return true;
    }
    if (!chatId && chatList?.length === 0 && !isDialogOpen) {
      return true;
    }
    return false;
  },
});
