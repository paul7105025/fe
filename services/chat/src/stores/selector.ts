import { allMessageType, allMessagesState, isMessageArray, newMessageType } from '@wrtn/core'
import { selector } from 'recoil'

export const lastOneMessage = selector({
  key: "@wrtn/chat/lastMessageSelector",
  get: ({ get }) => {
    const allMessages = get(allMessagesState)
    const lastGroup = allMessages[allMessages.length - 1];
    const lastMessage = isMessageArray(lastGroup) ? lastGroup[0] : lastGroup;

    return lastMessage;
  },
})

export const lastUserMessageSelector = selector({
  key: "@wrtn/chat/lastUserMessageSelector",
  get: ({ get }) => {
    const allMessages = get(allMessagesState)
    const userMessages: newMessageType[] = [];
    allMessages.forEach((message) => {
      if (!isMessageArray(message) && message.role === "user") userMessages.push(message);
    })

    return userMessages[userMessages.length - 1];
  }
})
