import { allMessageType } from "@wrtn/core";

export const chatSwitch = (
  item: allMessageType,
  role: "user" | "assistant",
  type?: string,
  _id?: string
) => {
  if (Array.isArray(item)) {
    if (item.length > 0) {
      if (role === "assistant" && type === "ToolOutput") {
        return item[0].role === "assistant" && item[0].type === "ToolOutput";
      }

      if (role === "assistant" && type === "ToolChipOutput") {
        return (
          item[0].role === "assistant" && item[0].type === "ToolChipOutput"
        );
      }

      if (role === "assistant" && type === "Message") {
        return item[0].role === "assistant" && item[0].type === "Message";
      }

      if (role === "user" && type === "CommandChipOutput") {
        return item[0].role === "user" && item[0].type === "CommandChipOutput";
      }

      if (role === "assistant" && type === "CommandChipOutput") {
        return (
          item[0].role === "assistant" && item[0].type === "CommandChipOutput"
        );
      }
    } else return false;
  } else {
    if (role === "user" && type === "CommandChipOutput") {
      return item.role === "user" && item.type === "CommandChipOutput";
    }

    if (role === "assistant" && type === "CommandChipOutput") {
      return item.role === "assistant" && item.type === "CommandChipOutput";
    }

    if (role === "user" && type === "ToolChipInput") {
      return item.role === "user" && item.type === "ToolChipInput";
    }

    if (role === "user" && type === "CommandChipInput") {
      return item.role === "user" && item.type === "CommandChipInput";
    }

    if (role === "user" && type === "Message") {
      return item.role === "user" && item.type === "Message";
    }

    if (role === "user" && type === "ToolInput") {
      return item.role === "user" && item.type === "ToolInput";
    }

    if (role === "user" && type === "Chip") {
      return item.role === "user" && item.type === "Chip";
    }

    return false;
  }
};
