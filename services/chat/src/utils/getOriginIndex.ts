import { imageUrlType, newMessageType } from "@wrtn/core";

export const getOriginIndex = (image: imageUrlType, contents: newMessageType[]) => {
  const imageURL = image.url;

  let contentIndex = -1;
  let urlIndex = -1;

  contents.forEach((content, index) => {
    if (content.hasOwnProperty("image")) {
      const imageUrls = content.image.imageUrls;

      imageUrls.forEach((url, index2) => {
        if (url.url === imageURL) {
          contentIndex = index;
          urlIndex = index2;
        }
      });
    }
  });

  return { contentIndex, urlIndex };
};
