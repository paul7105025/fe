import { nanoid } from "nanoid";

import { allMessageType, messageType } from "@wrtn/core";

export const changeUUID = (
  messages: Array<messageType | messageType[]>
): allMessageType[] => {
  return messages.map((message) => {
    if (Array.isArray(message)) {
      return message.map((m) => ({ ...m, uuid: m._id }));
    }
    if (message._id.includes("loading")) {
      return { ...message, uuid: nanoid(10) };
    }
    return { ...message, uuid: message._id };
  });
};

export const addUUID = (
  message: messageType | messageType[],
  newNanoId: string
): allMessageType => {
  if (Array.isArray(message)) {
    return message.map((m) => ({ ...m, uuid: newNanoId }));
  }

  return { ...message, uuid: newNanoId };
};
