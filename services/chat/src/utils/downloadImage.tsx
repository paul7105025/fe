export const downloadImage = async (
  url: string,
  name = "download",
  type = "png"
) => {
  const blob = await fetch(url, {
    mode: "no-cors",
  })
    .then((res) => {
      return res.blob();
    })
    .catch((err) => console.error("err : ", err));

  if (blob) {
    const newUrl = window.URL.createObjectURL(blob);
    const anchor = document.createElement("a");
    anchor.href = newUrl;

    anchor.download = `${name}.${type}`;
    anchor.click();
    window.URL.revokeObjectURL(newUrl);
  }
};
