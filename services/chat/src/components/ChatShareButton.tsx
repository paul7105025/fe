import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import { currentChatState, useHoverPopup } from "@wrtn/core";

import { Dialog } from "@wrtn/ui/components/Dialog";
import {
  ModalPortal,
  ModalPositionPortal,
} from "@wrtn/ui/components/ModalPortal";

import { useChatLoading, useShare } from "src/hooks";

import { Icon, newColors, typo } from "@wrtn/ui";

export const ChatShareButton = () => {
  const chat = useRecoilValue(currentChatState);

  const [isShared, setIsShared] = React.useState(!!chat?.shared);
  const manageLoading = useChatLoading();

  const hoverPopup = useHoverPopup();

  const { showModal, setShowModal, handleShare } = useShare();

  React.useEffect(() => {
    setIsShared(!!chat?.shared);
  }, [chat]);

  const onClickShare = () => {
    if (manageLoading.loading) return;
    setShowModal(false);
    setIsShared(true);
    handleShare();
  };

  return (
    <>
      <ShareWrapper
        isDisabled={isShared}
        ref={hoverPopup.hoverRef}
        onMouseEnter={hoverPopup.onMouseEnter}
        onMouseLeave={hoverPopup.onMouseLeave}
      >
        <ShareButton
          onClick={isShared ? undefined : () => setShowModal(true)}
          isDisabled={isShared}
        >
          <Icon icon="upload" color="white" size={20} />
        </ShareButton>
        <div>{isShared ? "공유된 채팅방" : "공유하기"}</div>
        {isShared &&
          hoverPopup.open(() => {
            return (
              <ModalPositionPortal
                position={{
                  top:
                    hoverPopup.hoverRef?.current &&
                    hoverPopup.hoverRef?.current.getBoundingClientRect().top +
                      17,
                  left:
                    hoverPopup.hoverRef?.current &&
                    hoverPopup.hoverRef?.current.getBoundingClientRect().left -
                      108,
                }}
              >
                <Popup>다시 공유하려면 기존 게시물을 삭제해주세요.</Popup>
              </ModalPositionPortal>
            );
          })}
      </ShareWrapper>
      {showModal && (
        <ModalPortal>
          <Dialog
            title="공유하시겠습니까?"
            description={[
              "채팅방이 공유되면 대화 내용이 타 유저에게",
              "공개됩니다. 공개를 원하지 않으시면",
              "취소 버튼을 눌러주세요.",
            ]}
            rightButtonLabel="공유하기"
            iconType="link"
            handleClose={() => setShowModal(false)}
            handleRightButton={onClickShare}
          />
        </ModalPortal>
      )}
    </>
  );
};

const ShareWrapper = styled.div<{ isDisabled: boolean }>`
  position: absolute;
  z-index: 5;
  right: 8%;
  top: 40px;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 8px;

  ${({ isDisabled }) =>
    typo({
      weight: 600,
      size: "14px",
      color: isDisabled ? newColors.GRAY_500 : newColors.PURPLE_500_PRIMARY,
    })}
`;

const ShareButton = styled.div<{ isDisabled: boolean }>`
  width: 50px;
  height: 50px;
  background: ${({ isDisabled }) =>
    isDisabled ? newColors.GRAY_500 : newColors.PURPLE_500_PRIMARY};
  border-radius: 25px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  cursor: ${({ isDisabled }) => (isDisabled ? "default" : "pointer")};
  box-shadow: 0px 4px 20px rgba(57, 68, 112, 0.1);
`;

const Popup = styled.div`
  background-color: ${newColors.GRAY_700};
  padding: 4px 9px;
  ${typo({
    weight: 600,
    size: "12px",
    color: newColors.WHITE,
  })};
  transform: translateX(-50%);
  border-radius: 5px;
`;
