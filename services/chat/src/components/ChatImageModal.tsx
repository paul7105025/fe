import styled from "styled-components";

import { ModalPortal, ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { imageUrlType, postGenerateUpscaledImageDownload, useEvent, useIsMobile } from "@wrtn/core";

import { FlexButton, FlexWrapper, Icon, boxShadow, colors, newColors, typo } from "@wrtn/ui";
import { FilledDislike, Dislike } from "@wrtn/ui/assets";
import useHoverMenu from "@wrtn/core/hooks/useHoverMenu";
import React from "react";

interface ChatImageModalProps {
  isModalOpen: boolean;
  currentModalImage: imageUrlType;
  currentModalImageId: string
  currentLike: {
    liked: boolean;
    disliked: boolean;
  };
  closeChatImageModal: () => void;
  pagination: () => JSX.Element;
  handleClickFavoriteImage: (type: "liked" | "disliked") => Promise<void>;
}

export const ChatImageModal = ({
  isModalOpen,
  currentModalImage,
  currentModalImageId,
  currentLike,
  closeChatImageModal,
  pagination,
  handleClickFavoriteImage,
}: ChatImageModalProps) => {
  const { collectEvent } = useEvent();

  const isMobile = useIsMobile();
  const imageDownloadHover = useHoverMenu({
    position: {
      top: 25,
      left: -130,
    }
  })

  const downloadUpscaledImage = React.useCallback(async (targetUrl: string, imageId: string) => {
    const res = await postGenerateUpscaledImageDownload({
      targetUrl,
      imageId,
    });

    const link = document.createElement("a");
    link.href = res.data.data;
    link.setAttribute('download', 'true');
    document.body.appendChild(link);
    link.click();

    document.body.removeChild(link);

    collectEvent("click_img_download_btn", {
      position: "modal",
      is_upscale: true,
    });
  }, [collectEvent])

  if (!isModalOpen) return null;

  return (
    <ModalPortal onClose={closeChatImageModal}>
      <ModalWrapper column>
        <CloseButton onClick={closeChatImageModal}>
          <Icon
            icon="multiply"
            size={isMobile ? 15 : 20}
            color={colors.GRAY_80}
          />
        </CloseButton>
        <ChatImage src={currentModalImage.url} alt="generated_image" />
        {pagination()}
        <ImageButtonWrapper>
          <ChatImageButton onClick={() => handleClickFavoriteImage("liked")}>
            {currentLike.liked ? <FilledLikeIcon /> : <LikeIcon />}
          </ChatImageButton>
          <ChatImageButton onClick={() => handleClickFavoriteImage("disliked")}>
            {currentLike.disliked ? <FilledDislikeIcon /> : <DislikeIcon />}
          </ChatImageButton>
          <div
            onClick={imageDownloadHover.handleClick}
            ref={imageDownloadHover.parentRef}
          >
            <ChatImageButton>
              <Icon
                icon="import"
                size={isMobile ? 15 : 20}
                color={colors.GRAY_60}
                hoverColor={colors.POINT_PURPLE}
              />
            </ChatImageButton>
          </div>
          {imageDownloadHover.open(({ position, childRef }) => {
            return (
              <ModalPositionPortal position={position}>
                <HoverWrapper ref={childRef}>
                  <GuideOuterItemWrapper>
                    <GuideOuterItem
                      href={currentModalImage.url}
                      download
                      rel="noopener noreferrer"
                      onClick={() =>
                        collectEvent("click_img_download_btn", {
                          position: "modal",
                          is_upscale: false,
                        })
                      }
                    >
                      <Icon
                        icon="import"
                        size={20}
                        color={newColors.GRAY_400}
                        hoverColor={colors.HOVER_PURPLE}
                      />
                      다운로드</GuideOuterItem>
                  </GuideOuterItemWrapper>
                  <GuideOuterItemWrapper>
                    <GuideOuterItem onClick={() => downloadUpscaledImage(currentModalImage.url, currentModalImageId)}>
                      <Icon
                        icon="expand-alt"
                        size={20}
                        color={newColors.GRAY_400}
                        hoverColor={colors.HOVER_PURPLE}
                      />
                      고화질 다운로드</GuideOuterItem>
                  </GuideOuterItemWrapper>
                </HoverWrapper>
              </ModalPositionPortal>
            )
          })}
        </ImageButtonWrapper>
      </ModalWrapper>
    </ModalPortal>
  );
};

const ModalWrapper = styled(FlexWrapper)`
  overflow: hidden;
  padding: 10px 10px 20px;

  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 20px;
  background-color: ${colors.BACKGROUND};

  ${boxShadow.tooltip_message_shadow}

  min-width: 766px;

  @media (max-width: 767px) {
    min-width: 0px;
    width: 90vw;
    max-width: 90vw;
  }
  @media (max-width: 480px) {
    min-width: 0px;
    width: 95vw;
    max-width: 95vw;
  }
`;

const CloseButton = styled(FlexButton)`
  position: absolute;
  top: 24px;
  right: 24px;

  width: 40px;
  height: 40px;

  background-color: ${colors.WHITE};
  border-radius: 50%;

  z-index: 1;
  cursor: pointer;

  @media (max-width: 480px) {
    width: 20px;
    height: 20px;
  }
`;

const ChatImage = styled.img`
  width: 100%;
  border-radius: 20px 20px 0px 0px;
  margin-bottom: 16px;
`;

const ImageButtonWrapper = styled(FlexWrapper)`
  gap: 12px;

  position: absolute;
  bottom: 24px;
  right: 24px;
`;

const ChatImageButton = styled(FlexButton)`
  cursor: pointer;

  svg {
    width: 22px;
    height: 22px;

    @media (max-width: 480px) {
      width: 18px;
      height: 18px;
    }

    path {
      fill: ${colors.GRAY_60};
    }
  }
  :hover {
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  }
`;

const DislikeIcon = styled(Dislike)``;

const LikeIcon = styled(DislikeIcon)`
  transform: rotate(180deg);
`;

const FilledDislikeIcon = styled(FilledDislike)``;

const FilledLikeIcon = styled(FilledDislikeIcon)`
  transform: rotate(180deg);
`;

const HoverWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  flex-direction: column;
  justify-content: flex-start;
  background-color: ${newColors.WHITE};
  padding: 8px 0px;
  border-radius: 8px;
  border: 1px solid ${newColors.GRAY_200};
  overflow: hidden;
`;

const NavOuterLink = styled.a`
  gap: 10px;

  display: flex;
  align-items: center;

  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: newColors.GRAY_700,
  })};

  cursor: pointer;
  padding: 6px 8px;
  &:hover {
    background: ${newColors.GRAY_000};
    border-radius: 5px;
  }
`;

const GuideOuterItemWrapper = styled.div`
  width: 100%;
`;

const GuideOuterItem = styled(NavOuterLink)`
  text-align: left;
  width: 100%;
  min-width: 105px;
  padding: 12px 16px;
  border-radius: 0px;
  &:hover {
    border-radius: 0px;
  }
`;
