import React from "react";
import styled, { css } from "styled-components";
import { useRecoilState, useRecoilValue } from "recoil";

import { Spinner } from "@wrtn/ui/components/Spinner";
import {
  colors,
  IconPaperPlane,
  FlexButton,
  typo,
  Icon,
  FlexWrapper,
} from "@wrtn/ui";
import { chatInputType, chatInputTypeState, chatToggleState, currentFileState, useHoverPopup } from "@wrtn/core";

interface SendButtonProps {
  type: "default" | "tool";
  onClick: () => void;
  disabled: boolean;
  loading: boolean;
}

export const SendButton = ({
  onClick,
  disabled,
  loading,
}: SendButtonProps) => {
  const [chatToggle, setChatToggle] = useRecoilState(chatToggleState);
  const chatInputType = useRecoilValue(chatInputTypeState);
  const currentFile = useRecoilValue(currentFileState);

  const [isOpen, setIsOpen] = React.useState(false);

  React.useEffect(() => {
    if (chatInputType === "tool")
      setIsOpen(false);
  }, [chatInputType])

  const toggleOpen = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    setIsOpen((o) => !o);
  };

  const selectToggle = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    toggle: number
  ) => {
    e.stopPropagation();
    setChatToggle(toggle);
    setIsOpen(false);
  };

  const disabledChange = !!currentFile

  return (
    <InputButton
      buttonType={disabledChange ? "tool" : chatInputType}
      onClick={disabled ? undefined : onClick}
      disabled={disabled}
      isOpen={isOpen}
      justify="center"
    >
      {/* {chatInputType === "default" && !disabledChange && (
        <>
          <SelectButton onClick={toggleOpen}>
            <span>{chatToggle === TOGGLE_SPEED ? "속도" : "지능"}</span>
            <Icon icon="arrow-up" size={20} color={colors.WHITE} />
            <Icon icon="angle-down" size={20} color={colors.WHITE} />
          </SelectButton>
          <Divider />
        </>
      )} */}
      {loading ? (
        <Spinner width={16} height={16} color={colors.WHITE} />
      ) : (
        <IconPaperPlane />
      )}
      {/* {!disabled && isOpen && (
        <SelectWrapper column>
          <Select
            select={chatToggle === TOGGLE_PERFORMANCE}
            onClick={(e) => selectToggle(e, TOGGLE_PERFORMANCE)}
          >
            GPT-4 <Icon icon="arrow-up" size={20} color={colors.BLACK} />
            <span>GPT-4</span>
          </Select>
          <Select
            select={chatToggle === TOGGLE_SPEED}
            onClick={(e) => selectToggle(e, TOGGLE_SPEED)}
          >
            GPT-3.5 <Icon icon="arrow-up" size={20} color={colors.BLACK} />
            <span>GPT-3.5</span>
          </Select>
        </SelectWrapper>
      )} */}
    </InputButton>
  );
};


const InputButton = styled(FlexWrapper)<{
  disabled: boolean;
  isOpen: boolean;
  buttonType: chatInputType;
}>`
  border: none;
  height: 34px;
  // width: ${({ buttonType }) => (buttonType === "default" ? "140px" : "70px")};
  width: 70px;

  position: relative;

  gap: 3px;

  padding: 0px 16px;

  border-radius: ${({ isOpen }) => (isOpen ? "0 0 5px 5px" : "5px")};

  background: linear-gradient(
    90.34deg,
    #c57dff -21.05%,
    #669cff 70.31%,
    #99e5f4 169.62%
  );

  cursor: pointer;

  ${({ disabled }) =>
    disabled &&
    css`
      background: ${colors.gray_55};
      cursor: not-allowed;
    `};
    @media(max-width: 767px){
      width: 70px;
    }
`;

const SelectButton = styled(FlexButton)`
  gap: 1px;
  height: 100%;

  ${typo({
    size: "15px",
    weight: 600,
    height: "162%",
    color: colors.WHITE,
  })};

white-space: nowrap;
  @media(max-width: 767px){
    display: none;
  }
`;

const Divider = styled.div`
  width: 1px;
  height: calc(100% - 12px);
  background: ${colors.WHITE};
  margin-right: 10px;
  @media(max-width: 767px){
    display: none;
  }
`;

const SelectWrapper = styled(FlexWrapper)`
  width: 100%;

  position: absolute;
  display: flex;
  bottom: 36px;
  left: 0;

  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;

  overflow: hidden;

  background-color: ${colors.WHITE};
`;

const Select = styled(FlexButton)<{ select: boolean }>`
  width: 100%;
  padding: 6px 12px;

  justify-content: flex-start;

  background-color: ${({ select }) =>
    select ? colors.BACKGROUND : colors.WHITE};

  ${typo({
    size: "15px",
    weight: 500,
    height: "162%",
    color: colors.GRAY_90,
  })};

  white-space: nowrap;

  span {
    padding-left: 9px;
    color: ${colors.GRAY_60};
  }

  &:hover {
    cursor: pointer;
    background-color: ${colors.BACKGROUND};
  }
`;
