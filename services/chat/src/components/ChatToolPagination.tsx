import styled from "styled-components";
import {
  FlexWrapper,
  typo,
  colors,
  IconRoundCarouselRight,
  FlexButton,
} from "@wrtn/ui";

interface ChatToolPaginationProps {
  idx: number;
  max: number;

  add: () => void;
  subtract: () => void;
  isHorizontal?: boolean;
}

export const ChatToolPagination = ({
  idx,
  max,

  add,
  subtract,
  isHorizontal,
}: ChatToolPaginationProps) => {


  return (
    <ToolListPaginationButtonWrapper column={isHorizontal ? false : true}>
      <IconButton
        onClick={idx === 0 ? undefined : () => subtract()}
        disabled={idx === 0}
      >
        <IconRoundCarouselLeft />
      </IconButton>
      <span>
        {(idx + 1).toString()} / {max}
      </span>
      <IconButton
        onClick={idx === max - 1 ? undefined : () => add()}
        disabled={idx === max - 1}
      >
        <IconRoundCarousel />
      </IconButton>
    </ToolListPaginationButtonWrapper>
  );
};

const ToolListPaginationButtonWrapper = styled(FlexWrapper)`
  position: sticky;
  ${typo({
    weight: 600,
    size: "16px",
    height: "160%",
    color: colors.GRAY_80,
  })};

  justify-content: space-between;

  white-space: nowrap;

  gap: 10px;
`;

const IconButton = styled(FlexButton)`
  cursor: pointer;

  svg {
    circle {
      stroke: ${({ disabled }) =>
        disabled ? colors.BLUE_GRAY_LINE : colors.gray_60};
    }
    path {
      fill: ${({ disabled }) =>
        disabled ? colors.BLUE_GRAY_LINE : colors.gray_60};
    }
  }
`;

const IconRoundCarousel = styled(IconRoundCarouselRight)``;

const IconRoundCarouselLeft = styled(IconRoundCarousel)`
  transform: rotate(180deg);
`;
