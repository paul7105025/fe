import React from "react";
import styled from "styled-components";
import { useRecoilState, useRecoilValue } from "recoil";

import {
  inappChatOpenState,
  useEvent,
  hoverChatGuideState,
  nextRouteState,
} from "@wrtn/core";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import {
  Tag,
  FlexWrapper,
  typo,
  colors,
  FlexButton,
  boxShadow,
} from "@wrtn/ui/styles";
import {
  Help,
  IconCarouselDown,
  IconMenu,
  IconUpperRight,
  IconWaterDropPlus,
} from "@wrtn/ui/assets";

export const ChatInappHeader = ({
  toggleSideNavigation,
  onCreateChat,
  chatId,
}) => {
  const router = useRecoilValue(nextRouteState);

  const [isOpenChat, setOpenChat] = useRecoilState(inappChatOpenState);

  const { collectEvent } = useEvent();

  const helpRef = React.useRef<HTMLDivElement | null>(null);
  const [isHover, setIsHover] = useRecoilState(hoverChatGuideState);

  const handleClickNewChat = () => {
    onCreateChat();
  };

  const handleOpenWindow = () => {
    collectEvent("view_chat_page", {
      position: "mini_extension",
    });
    router.push(`/app/chat/${chatId}`);
  };

  return (
    <HeaderWrapper>
      <Header row>
        <HeaderSection>
          <PrevButton onClick={toggleSideNavigation}>
            <IconMenu height={24} width={24} />
          </PrevButton>
          <HeaderTitle>챗 뤼튼</HeaderTitle>
          <IconUpperRight
            style={{ cursor: "pointer" }}
            onClick={handleOpenWindow}
          />
          <Tag color="ORANGE">NEW!</Tag>
        </HeaderSection>
        <HeaderSection style={{ gap: "6px" }}>
          <IconWaterDropPlus
            style={{ cursor: "pointer", marginRight: "6px" }}
            onClick={handleClickNewChat}
          />
          <HelpWrapper
            ref={helpRef}
            onMouseEnter={() => setIsHover(true)}
            onMouseLeave={() => setIsHover(false)}
          >
            <HelpIcon height={24} width={24} fill={colors.gray_60} />
            {isOpenChat && isHover && (
              <ModalPositionPortal
                position={{
                  top:
                    helpRef.current &&
                    helpRef.current.getBoundingClientRect().top + 25,
                  left:
                    helpRef.current &&
                    helpRef.current.getBoundingClientRect().right - 285,
                  // right: helpRef.current && helpRef.current.getBoundingClientRect().
                }}
              >
                <HelpGap />
                <HelpContentWrapper column>
                  <HelpContentSection column>
                    <HelpContentTitle>이렇게 써보세요</HelpContentTitle>
                    <HelpContentSub>툴에서 결과물을 생성한 이후</HelpContentSub>
                    <HelpContentSub>
                      챗 뤼튼을 통해 가공해보세요.
                    </HelpContentSub>
                    <HelpContentExample>
                      “~~” 문장을 존댓말로 바꿔줘
                    </HelpContentExample>
                    <HelpContentExample>
                      “~~” 조금 더 감성적으로 써줘
                    </HelpContentExample>
                  </HelpContentSection>
                  <HelpContentDivider />
                  <HelpContentSection column>
                    <HelpContentTitle>이런 장점이 있어요</HelpContentTitle>
                    <HelpContentSub>
                      구체적으로 쓸수록 잘 알아들어요.{" "}
                    </HelpContentSub>
                    <HelpContentSub>
                      피드백을 주면 더 잘 알아들어요.
                    </HelpContentSub>
                    <HelpContentExample>“맞아, 정확해!“</HelpContentExample>
                    <HelpContentExample>
                      “이건 아니야. ~를 의미했어"
                    </HelpContentExample>
                  </HelpContentSection>
                </HelpContentWrapper>
              </ModalPositionPortal>
            )}
          </HelpWrapper>
          <CarouselDown onClick={() => setOpenChat(false)} />
        </HeaderSection>
      </Header>
    </HeaderWrapper>
  );
};

const HeaderWrapper = styled(FlexWrapper)`
  padding: 26px 40px 21px 40px;
  width: 100%;
  border-bottom: 1px solid #dee4f3;
  @media (max-width: 1023px) {
    /* width: 100%; */
  }
  @media (max-width: 767px) {
    border: none;
    padding: 32px 20px;
  }
`;

const HeaderSection = styled(FlexWrapper)``;

const CarouselDown = styled(IconCarouselDown)`
  & path {
    fill: ${colors.gray_60};
  }
  cursor: pointer;
`;

const Header = styled(FlexWrapper)`
  position: relative;
  justify-content: space-between;
  flex: 1;
`;

const HeaderTitle = styled.p`
  margin-left: 10px;
  ${typo({
    weight: "600",
    size: "20px",
    color: colors.GRAY_90,
  })};
  margin-right: 8px;

  display: flex;
  align-items: center;
  white-space: nowrap;

  @media (max-width: 767px) {
    font-size: 20px;
  }
`;

const PrevButton = styled(FlexButton)<{ isSideNavigationOpened?: boolean }>`
  z-index: 80;

  cursor: pointer;

  svg {
    fill: ${colors.gray_80};
  }

  transform: ${({ isSideNavigationOpened }) =>
    isSideNavigationOpened ? "rotate(0deg)" : "rotate(-180deg)"};
  transition: transform 0.3s cubic-bezier(0.1, -0.15, 0.6, 1.45);
`;

const HelpContentWrapper = styled(FlexWrapper)`
  padding: 24px 22px;
  min-width: 285px;
  align-items: flex-start;
  background-color: ${colors.BACKGROUND};
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  border-radius: 10px;
  ${boxShadow.tooltip_message_shadow};
`;

const HelpContentSection = styled(FlexWrapper)`
  align-items: flex-start;
`;

const HelpContentDivider = styled.div`
  border: 0.5px solid ${colors.WHITE_BOLDER_LINE};
  width: 100%;
  height: 0px;
  margin-bottom: 14px;
`;

const HelpContentTitle = styled.div`
  ${typo({
    weight: 700,
    size: "16px",
    height: "100%",
    color: colors.ACTION_BLUE,
  })};
  padding-bottom: 14px;
`;

const HelpContentSub = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.gray_70,
  })};
  padding-bottom: 10px;
`;

const HelpContentExample = styled.div`
  ${typo({
    weight: 600,
    size: "12px",
    height: "100%",
    color: colors.gray_60,
  })};
  padding-bottom: 10px;
`;

const HelpWrapper = styled.div``;

const HelpGap = styled.div`
  height: 30px;
  width: 100%;
`;

const HelpIcon = styled(Help)`
  cursor: pointer;
  & path {
    fill: ${colors.gray_60};
  }
`;
