import styled from "styled-components";
import { useRecoilValue } from "recoil";
import { useTransition, animated } from "@react-spring/web";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

import { ReactComponent as ChatMainLogo } from "../assets/logos/logo_chat_main.svg";

import { userState } from "@wrtn/core";
import SuggestChipContainer from "src/containers/SuggestChipContainer";

interface ChatEmptyProps {
  renderAuth: any;
  isShow: boolean;
}

export const ChatEmpty = ({ renderAuth, isShow }: ChatEmptyProps) => {
  const emptyTransition = useTransition(isShow, {
    keys: null,
    from: { opacity: 0, transform: "translateY(50%)" },
    enter: { opacity: 1, transform: "translateY(0%)" },
    // leave:  { opacity: 0, transform: 'translateY(-50%)' },
    // config: { duration: 400 },
  });

  const user = useRecoilValue(userState);

  if (!isShow) return null;

  return emptyTransition(
    (style, item) =>
      item && (
        <Wrapper column justify="center">
          <animated.div style={style}>
            <FlexWrapper column justify="center">
              <Subtitle>모두를 위한 AI 포털</Subtitle>
              <LogoWrapper>
                {/* <ChatWrtnLogo /> */}
                <ChatMainLogo />
              </LogoWrapper>
            </FlexWrapper>
            {!user ? <>{renderAuth()}</> : <SuggestChipContainer.Default />}
          </animated.div>
        </Wrapper>
      )
  );
};

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  position: absolute;
  top: 0;
  left: 0;

  padding: 60px 20px 224px;

  overflow: scroll;
  justify-content: center;

  @media (max-width: 767px) {
    justify-content: flex-start;
  }
`;

const LogoWrapper = styled(FlexWrapper)`
  margin-bottom: 68px;
`;

const Subtitle = styled.p`
  ${typo({ weight: 600, size: "18px", color: colors.GRAY_80 })};
  margin-bottom: 30px;
`;
