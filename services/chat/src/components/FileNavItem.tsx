import { useState } from "react";
import styled from "styled-components";
import { FlexWrapper, typo, newColors, Icon } from "@wrtn/ui";
import { useChatLoading, useFileManage } from "src/hooks";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { Dialog } from "@wrtn/ui/components/Dialog";

interface FileNavItemProps {
  fileId?: string;
  isSelected: boolean;
  title: string;
  disabled?: boolean;
  onClick?(): void
}


const FileNavItem = ({ fileId, isSelected, title, disabled, onClick }: FileNavItemProps) => {
  const [showArrow, setShowArrow] = useState(false)
  const [textLength, setTextLength] = useState(21);
  const { loading } = useChatLoading();
  const [showModal, setShowModal] = useState(false);

  const { removeFile } = useFileManage();

  const onHoverWrapper = () => {
    setShowArrow(true)
    setTextLength(18)
  }
  const onLeaveWrapper = () => {
    setShowArrow(false)
    setTextLength(21)
  }

  const onClickTrash = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setShowModal(true);
  }

  const handleDelete = async () => {
    if (fileId) await removeFile(fileId);
    setShowModal(false)
  }

  const onClickItem = () => {
    if (loading || showModal) return

    onClick();
  }

  return (
    <ItemWrapper isSelected={isSelected} disabled={disabled} onClick={onClickItem} onMouseEnter={onHoverWrapper} onMouseLeave={onLeaveWrapper}>
      <ItemText isSelected={isSelected}>
        {title.length >= textLength ? `${title.slice(0, textLength)}...` : title}
      </ItemText>
      {!disabled && showArrow && (
        <div style={{ display: "flex", gap: "3px", paddingRight: "2px" }}>
          <IconWrapper>
            <Icon
              icon="arrow-right"
              size={20}
              color="inherit"
            />
          </IconWrapper>
          <IconWrapper onClick={onClickTrash}>
            <Icon
              icon="trash-alt"
              size={20}
              color="inherit"
            />
          </IconWrapper>
        </div>
      )}
      {showModal && (
        <ModalPortal>
          <Dialog
            iconType="!"
            title="정말로 파일을 삭제하시겠어요?"
            description={[
              "업로드한 파일을 삭제합니다.",
              "채팅창에서 다시 업로드할 수 있어요!"
            ]}
            rightButtonLabel="삭제하기"
            handleRightButton={handleDelete}
            handleClose={() => setShowModal(false)}
          />
        </ModalPortal>
      )}
    </ItemWrapper>
  )
}

const ItemWrapper = styled(FlexWrapper)<{ isSelected: boolean, disabled: boolean }>`
  height: 40px;
  padding: 0 16px;
  cursor: ${({ disabled }) => (!disabled && "pointer")};
  justify-content: space-between;
  background: ${({ isSelected }) => (isSelected && "white")}
`

const ItemText = styled.p<{ isSelected: boolean }>`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
  })};

  color: ${({ isSelected }) => (isSelected ? newColors.GRAY_700 : newColors.GRAY_600)}
`;

const IconWrapper = styled.span`
  color: ${newColors.GRAY_300};

  &:hover {
    color: ${newColors.PURPLE_500_PRIMARY};
  }
`;

export default FileNavItem;
