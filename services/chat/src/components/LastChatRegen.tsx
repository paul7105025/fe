import styled from "styled-components";

import {
  handleClickRegenerateProps,
  useChatLoading,
  useFileUpload,
} from "src/hooks";
import { Icon, colors, FlexButton, typo, newColors } from "@wrtn/ui";
import useHoverMenu from "@wrtn/core/hooks/useHoverMenu";
import React from "react";
import { useChatTutorial } from "src/hooks/useChatTutorial";
import { ChatTutorialBubble } from "./ChatTutorialBubble";
import { Spinner } from "@wrtn/ui/components/Spinner";
import { useRecoilValue } from "recoil";
import { lastOneMessage } from "src/stores";
import { isMessageArray } from "@wrtn/core";

interface LastChatRegenProps {
  handleClickRegenerate: ({
    type,
    toolId,
  }: handleClickRegenerateProps) => Promise<void>;
  handleClickRegenerateFile: (fileId: string) => Promise<void>;
  handleStopGenerate: () => void;
  isStream?: boolean;
  showToolInput?: boolean;
}

export const LastChatRegen = ({
  handleClickRegenerate,
  handleClickRegenerateFile,
  handleStopGenerate,
  isStream,
  showToolInput,
}: LastChatRegenProps) => {
  const { currentActive, handleActive, handleRemove } = useChatTutorial(2);
  const manageLoading = useChatLoading();
  const lastMessage = useRecoilValue(lastOneMessage);
  const { isFileMode, stopAnalyzeFile } = useFileUpload();

  const hoverTutorial = useHoverMenu({
    position: {
      top: 47,
      left: -127,
    },
  });

  React.useEffect(() => {
    if (currentActive) {
      setTimeout(() => hoverTutorial.handleMouseOver(), 1400);
      handleRemove();
    }
  }, [currentActive]);

  const isAvailableStop = ["Message", "FileInput", "FileOutput", "DynamicChipInput"].includes(
    lastMessage.type
  );
  const isAvailableRegen =
    ["Message", "ToolOutput", "ImageOutput", "DynamicChipOutput"].includes(lastMessage.type) ||
    (lastMessage.type === "FileOutput" &&
      !lastMessage.isAnalyzeMsg &&
      isFileMode);

  const isAnalyzING = lastMessage.type === "FileInput";

  let toolId = isMessageArray(lastMessage)
    ? lastMessage[0].toolId
    : lastMessage.toolId;

  if (!isAvailableRegen && !manageLoading.loading) return null;

  return (
    <>
      <InteractButton
        showToolInput={showToolInput}
        showFileInput={!!isFileMode}
        ref={hoverTutorial.parentRef}
        isClickAvailable={!manageLoading.loading || isAvailableStop}
        onClick={() => {
          if (manageLoading.loading && !isStream) return;
          if (manageLoading.loading && !isAvailableStop) return;
          if (manageLoading.loading && isAvailableStop) {
            if (lastMessage.type === "FileInput") {
              if (!isFileMode) return;
              return stopAnalyzeFile();
            }

            return handleStopGenerate();
          }

          if (lastMessage.type === "FileOutput" && lastMessage.fileId) {
            return handleClickRegenerateFile(lastMessage.fileId);
          }

          handleClickRegenerate(
            toolId ? { type: "tool", toolId } : { type: "default" }
          );
        }}
      >
        {!manageLoading.loading && isAvailableRegen && (
          <>
            <Icon icon="sync" size={16} color="inherit" />
            다시 생성
          </>
        )}
        {manageLoading.loading && (
          <>
            {isAvailableStop &&
            (isStream || lastMessage.type.startsWith("File")) ? (
              <>
                <Icon
                  icon="stop-circle"
                  size={16}
                  color="inherit"
                />
                {isAnalyzING ? "분석 멈추기" : "생성 멈추기"}
              </>
            ) : (
              <>
                <Spinner width={16} height={16} color={newColors.PURPLE_500_PRIMARY} />
                생성 중
              </>
            )}
          </>
        )}
      </InteractButton>
      {hoverTutorial.open(({ position }) => (
        <ChatTutorialBubble
          position={position}
          arrow={{
            direction: "top",
            position: 185,
          }}
          close={() => hoverTutorial.handleMouseOut(() => handleActive())}
          title="다시 생성 해보세요!"
          guideText={`같은 입력에 대한 여러가지 결과물\n을 생성하고 비교해보세요.`}
          childRef={hoverTutorial.childRef}
        />
      ))}
    </>
  );
};

export const InteractButton = styled(FlexButton)<{
  showToolInput?: boolean;
  showFileInput?: boolean;
  isClickAvailable: boolean;
}>`
  background: ${colors.WHITE};
  border: 1px solid ${newColors.GRAY_100};
  border-radius: 30px;
  z-index: 1;
  box-shadow: 0px 2px 10px rgba(79, 68, 195, 0.1);

  ${typo({
    weight: 600,
    size: "14px",
    color: newColors.GRAY_500,
  })};

  white-space: nowrap;

  width: 108px;
  height: 36px;

  padding: 2px 18px;

  gap: 6px;
  cursor: ${({ isClickAvailable }) =>
    isClickAvailable ? "pointer" : "default"};

  &:hover {
    color: ${({ isClickAvailable }) =>
      isClickAvailable ? newColors.PURPLE_500_PRIMARY : newColors.GRAY_500};
    background-color: ${({ isClickAvailable }) => 
      isClickAvailable ? "#F7F6FF" : colors.WHITE};
  }

  position: relative;
  bottom: ${({ showToolInput, showFileInput }) => (showToolInput ? "50px" : (showFileInput ? "40px" : "0px"))};

  @media (max-width: 767px) {
    bottom: ${({ showToolInput, showFileInput }) => (showToolInput ? "42px" : (showFileInput ? "20px" : "0px"))};
  }
`;
