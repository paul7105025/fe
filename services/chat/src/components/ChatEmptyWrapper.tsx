import styled from "styled-components";

import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";

import { ReactComponent as ChatWrtnLogo } from "@wrtn/ui/assets/wrtn/logo_chat_wrtn.svg";

import { chatEmptyGuide } from "../constants";

interface ChatEmptyWrapperProps {
  isFree: boolean;
  guide?: any;
  isMini?: boolean;
}

export const ChatEmptyWrapper = ({
  isFree,
  guide = chatEmptyGuide,
  isMini = false,
}: ChatEmptyWrapperProps) => {
  return (
    <Wrapper column>
      <div style={{ flex: 1 }} />
      <LogoWrapper>
        <ChatWrtnLogo />
      </LogoWrapper>
      <Title isMini={isMini}>챗 뤼튼</Title>
      <Subtitle>챗 뤼튼에게 무엇이든 물어보세요!</Subtitle>
      {/* {isFree || false&& (
        <Subtitle>
          현재 플러스 요금제 회원에게만 우선적으로 제공되는 기능이에요.
        </Subtitle>
      )} */}
      {/* {isFree ? (
        <CheckPlanButton onClick={() => navigate("/app/setting/plan")}>
          요금제 확인하기
        </CheckPlanButton>
      ) : ( */}
      <GuideWrapper>
        {guide
          .filter((v) => v.type === "block")
          .map((guide) => (
            <GuideBox key={guide.title}>
              <GuideTitle>{guide.title}</GuideTitle>
              <div style={{ flex: 1 }} />
              <FlexWrapper style={{ gap: "14px" }} column>
                {guide.description.map((desc) => (
                  <GuideDesc key={desc}>{desc}</GuideDesc>
                ))}
              </FlexWrapper>
              <FlexWrapper column>
                {guide?.sub_description?.map((desc) => (
                  <GuideSubDesc key={desc}>{desc}</GuideSubDesc>
                ))}
              </FlexWrapper>
            </GuideBox>
          ))}
      </GuideWrapper>
      {/* )} */}
      {guide.find((v) => v.type === "warning") && (
        <FlexWrapper
          row
          style={{
            alignItems: "flex-start",
            justifyContent: "flex-start",
            gap: "5px",
          }}
        >
          <Warning>주의</Warning>
          <FlexWrapper
            column
            style={{ maxWidth: "400px", justifyContent: "flex-start" }}
          >
            {guide
              .filter(
                (v) =>
                  v.type === "warning" &&
                  ((isFree && v.for === "free") ||
                    (!isFree && v.for === "paid"))
              )[0]
              .description?.map((v) => (
                <GuideSubDesc>{v}</GuideSubDesc>
              ))}
          </FlexWrapper>
        </FlexWrapper>
      )}
      <div style={{ flex: 1 }} />
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  position: absolute;
  top: 0;
  left: 0;

  padding: 20px;
  padding-bottom: 112px;
  overflow: scroll;

  justify-content: flex-start;
`;

const LogoWrapper = styled(FlexWrapper)`
  margin-bottom: 21px;
`;

const Title = styled.h1<{ isMini: boolean }>`
  ${(props) =>
    typo({
      weight: 700,
      size: props.isMini ? "20px" : "30px",
      color: colors.GRAY_90,
    })};

  margin-bottom: ${(props) => (props.isMini ? "14px" : "21px")};
`;

const Subtitle = styled.p`
  ${typo({ weight: 600, size: "18px", color: colors.GRAY_70 })};
  margin-bottom: 16px;
`;

const CheckPlanButton = styled(FlexButton)`
  background: ${colors.POINT_PURPLE};
  border-radius: 5px;

  padding: 8px 43px;
  ${typo({ weight: 600, size: "18px", height: "144%", color: colors.WHITE })};
  cursor: pointer;
`;

const GuideWrapper = styled(FlexWrapper)`
  margin-top: 20px;
  gap: 24px;
  flex-wrap: wrap;
  padding-bottom: 27px;
`;

const GuideBox = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  gap: 14px;
  width: 212px;

  background-color: ${colors.BACKGROUND};
  border-radius: 18px;

  padding: 25px 10px 35px;

  min-width: 212px;
  height: 225px;
`;

const GuideTitle = styled.p`
  ${typo({
    weight: 700,
    size: "16px",
    height: "160%",
    color: colors.ACTION_BLUE,
  })};
`;

const GuideDesc = styled.p`
  white-space: pre-line;
  ${typo({ weight: 600, size: "14px", height: "100%", color: colors.GRAY_70 })};
`;

const GuideSubDesc = styled.div`
  white-space: pre-line;
  ${typo({ weight: 600, size: "12px", height: "180%", color: colors.GRAY_60 })};
  text-align: left;
  width: 100%;
`;

const Warning = styled.p`
  ${typo({
    weight: 700,
    size: "12px",
    height: "180%",
    color: colors.ACTION_BLUE,
  })};
`;
