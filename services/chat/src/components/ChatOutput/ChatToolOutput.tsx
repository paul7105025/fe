import React from "react";
import styled from "styled-components";
import { CopyToClipboard } from "react-copy-to-clipboard";

import {
  useEvent,
  TOOL_TAG_RECORD,
  useFetchToolList,
  putChatMessage_Id,
} from "@wrtn/core";

import { ChatAssistantOutputProps } from ".";
import { ToolProvider } from "../ToolProvider";

import { Tag, Copy, FlexWrapper, TOOL_ICON_SELECTOR } from "@wrtn/ui";

import {
  AbsoluteWrapper,
  ChatBottomWrapper,
  ChatAIBoxWrapper,
  ChatIcon,
  ChatText,
  CopyButton,
  DislikeIcon,
  FilledDislikeIcon,
  FilledLikeIcon,
  LikeIcon,
  ChatLogo,
  ChatLogoIcon,
  StickyWrapper,
  ChatTitle,
  ChatTopWrapper,
  ChatIconButton,
  ChatDivider,
} from "./styles";
import { ChatMarkdownComponent } from "./MarkdownComponents";

interface ChatToolOutputProps extends ChatAssistantOutputProps {
  toolId: string;
}

export const ChatToolOutput = ({
  contents,
  toolId,
  withLeftTop,
  withLeftBottom,
  withBottomCenter,
  chatId,
  onCopy,
}: ChatToolOutputProps) => {
  const { allToolList } = useFetchToolList();
  const tool = React.useMemo(
    () => allToolList.find((v) => v._id === toolId),
    [allToolList, toolId]
  );

  const { collectEvent } = useEvent();
  const [idx, setIdx] = React.useState(0);

  const addIdx = () => {
    setIdx((c) => c + 1);
  };

  const subtractIdx = () => {
    setIdx((c) => c - 1);
  };

  const [likedList, setLikedList] = React.useState<boolean[]>(
    contents.map((v) => v.liked)
  );
  const [dislikedList, setDislikedList] = React.useState<boolean[]>(
    contents.map((v) => v.disliked)
  );

  React.useEffect(() => {
    setLikedList(contents.map((v) => v.liked));
    setDislikedList(contents.map((v) => v.disliked));
  }, [contents]);

  const liked = likedList[idx];
  const disliked = dislikedList[idx];

  const setLiked = (val: boolean) =>
    setLikedList((c) => [...c.map((v, i) => (i === idx ? val : v))]);
  const setDisliked = (val: boolean) =>
    setDislikedList((c) => [...c.map((v, i) => (i === idx ? val : v))]);

  const handleClickFavorite = async (id: string) => {
    await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: !liked,
        disliked: false,
      },
    });
    collectEvent("click_like_btn", {
      onoff: !liked,
      position: "chat",
      feature_menu: "tool",
      feature_category: tool.category,
      feature_name: tool.name,
      feature_tag: tool.tag,
      feature_form: tool.kind,
    });
    setLiked(!liked);
    setDisliked(false);
  };

  const handleClickDislike = async (id: string) => {
    await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: false,
        disliked: !disliked,
      },
    });
    collectEvent("click_unlike_btn", {
      onoff: !disliked,
      position: "chat",
      feature_menu: "tool",
      feature_category: tool.category,
      feature_name: tool.name,
      feature_tag: tool.tag,
      feature_form: tool.kind,
    });
    setLiked(false);
    setDisliked(!disliked);
  };

  return (
    <FlexWrapper column align="center" style={{width: '100%'}}>
    <ChatAIBoxWrapper
      column
      // key={contents[idx]?._id}s
      style={{ padding: "0px", gap: "0px" }}
    >
      <ChatTopWrapper row align="flex-start" justify="center">
        <ChatIcon>
          <ChatLogo />
        </ChatIcon>
        <ChatText>
          <ChatMarkdownComponent.Wrapper text={contents[idx].content} />
        </ChatText>
      </ChatTopWrapper>
      <ChatDivider />
      <ChatBottomWrapper
        row
        align="flex-start"
        justify="center"
        style={{ width: "100%" }}
      >
        <FlexWrapper
          row
          align="center"
          justify="flex-start"
          style={{ width: "100%", gap: "0px" }}
        >
          <ChatLogoIcon>{TOOL_ICON_SELECTOR[tool.icon]}</ChatLogoIcon>
          <Tag color={TOOL_TAG_RECORD[tool.tag].color}>
            {TOOL_TAG_RECORD[tool.tag].name}
          </Tag>
          <ChatTitle>{tool.name}</ChatTitle>
          <ToolProvider provider={tool.provider} />
        </FlexWrapper>
        <ToolButtonWrapper row align="center" justify="flex-end">
          <ChatIconButton onClick={() => handleClickFavorite(contents[idx]._id)}>
            {liked ? <FilledLikeIcon /> : <LikeIcon />}
          </ChatIconButton>
          <ChatIconButton onClick={() => handleClickDislike(contents[idx]._id)}>
            {disliked ? <FilledDislikeIcon /> : <DislikeIcon />}
          </ChatIconButton>
          <CopyToClipboard text={contents[idx].content}>
            <CopyButton
              onClick={() => {
                onCopy();
                collectEvent("click_copy_btn", {
                  position: "chat",
                  feature_menu: "tool",
                  feature_category: tool.category,
                  feature_name: tool.name,
                  feature_tag: tool.tag,
                  feature_form: tool.kind,
                });
              }}
            >
              <Copy />
            </CopyButton>
          </CopyToClipboard>
        </ToolButtonWrapper>
      </ChatBottomWrapper>
      <AbsoluteWrapper>
        <StickyWrapper>
          <>
            {withLeftTop &&
              withLeftTop({
                idx,
                add: addIdx,
                subtract: subtractIdx,
                max: contents.length,
              })}
            {withLeftBottom && withLeftBottom()}
          </>
        </StickyWrapper>
      </AbsoluteWrapper>
    </ChatAIBoxWrapper>
    {withBottomCenter && 
      withBottomCenter({
        idx,
        add: addIdx,
        subtract: subtractIdx,
        max: contents.length,
      })}           
    </FlexWrapper>
  );
};

const ToolButtonWrapper = styled(FlexWrapper)`
  width: 100%;
  gap: 14px;

  @media (max-width: 767px) {
    position: absolute;
    bottom: 77px;
    right: 33px;
  }
`;
