import React from "react";
import remarkGfm from "remark-gfm";
import { useRecoilState } from "recoil";
import { DotPulse } from "@uiball/loaders";
import styled, { keyframes } from "styled-components";

import { colors } from "@wrtn/ui";

import {
  ChatAIBoxWrapper,
  ChatIcon,
  ChatText,
  ChatLogo,
  AbsoluteWrapper,
  StickyWrapper,
} from "./styles";
import { streamMessageState } from "@wrtn/core";
import { useNaturalDisplayText } from "src/hooks/useNaturalDisplayText";
import { ChatMarkdownComponent } from "./MarkdownComponents";

interface StreamLoadingOutputType {
  withLeftBottom?: () => JSX.Element;
}

export const StreamLoadingOutput = ({
  withLeftBottom,
}: StreamLoadingOutputType) => {
  const [streamMessage, setStreamMessage] = useRecoilState(streamMessageState);

  React.useEffect(() => {
    setStreamMessage("");

    return () => {
      setStreamMessage("");
    }
  }, []);

  const { displayText } = useNaturalDisplayText()

  return (
    <ChatAIBoxWrapper key={"loading-output"} style={{ alignItems: "top" }}>
      <ChatIcon>
        <ChatLogo />
      </ChatIcon>
      <ChatText style={{ paddingLeft: 5, minHeight: 8, height: "100%" }}>
        {streamMessage === null && (
          <DotPulse size={20} speed={1.3} color={colors.GRAY_60} />
        )}
        {streamMessage === "" ? (
          <CursorWrapper>
            <Cursor />
          </CursorWrapper>
        ) : (
          <ChatMarkdownComponent.Wrapper text={displayText} />
        )}
      </ChatText>
      <AbsoluteWrapper>
        <StickyWrapper>
          {streamMessage !== "" && withLeftBottom && withLeftBottom()}
        </StickyWrapper>
      </AbsoluteWrapper>
    </ChatAIBoxWrapper>
  );
};

const Blink = keyframes`
  0% { background-color: gray; }
  80% { background-color: white; }
  100% { background-color: gray; }
`;

export const CursorWrapper = styled.div`
  display: inline-block;
  width: 0.4rem;
  height: 1.1rem;
`;

export const Cursor = styled.div`
  position: relative;
  top: 5px;
  height: 100%;
  background-color: black;
  margin-left: 5px;

  animation: ${Blink} 1s ease infinite;
`;
