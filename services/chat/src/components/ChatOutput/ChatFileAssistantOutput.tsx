import React from 'react'
import { CopyToClipboard } from "react-copy-to-clipboard";
import styled from 'styled-components';

import {
  AbsoluteWrapper,
  ChatAIBoxWrapper,
  ChatBottomWrapper,
  ChatIcon,
  ChatIconButton,
  ChatLogo,
  ChatText,
  CopyButton,
  DislikeIcon,
  FilledDislikeIcon,
  FilledLikeIcon,
  LikeIcon,
  StickyWrapper,
} from './styles'
import { Copy, FlexWrapper, Icon, colors, newColors, typo } from '@wrtn/ui'
import { ChatAssistantOutputProps } from '.';
import { ChatMarkdownComponent } from './MarkdownComponents';
import { useNaturalDisplayText } from 'src/hooks/useNaturalDisplayText';
import { useEvent, putChatMessage_Id, streamMessageState, useHoverPopup } from "@wrtn/core";
import { useRecoilValue } from 'recoil';
import { CursorWrapper, Cursor } from './StreamLoadingOutput';
import { ModalPositionPortal } from '@wrtn/ui/components/ModalPortal';

type ChatFileAssistantOutputProps = ChatAssistantOutputProps & {}

export const ChatFileAssistantOutput = ({
  contents: messages,
  chatId,
  withLeftTop,
  withBottomCenter,
  onCopy,
}: ChatFileAssistantOutputProps) => {
  const [idx, setIdx] = React.useState(0);
  const [liked, setLiked] = React.useState(false);
  const [disliked, setDisliked] = React.useState(false);
  const [msg, setMsg] = React.useState(messages[idx]);
  const { collectEvent } = useEvent();
  const streamMessage = useRecoilValue(streamMessageState);
  const hoverPopup = useHoverPopup();

  React.useEffect(() => {
    setMsg(messages[idx]);
  }, [messages[idx].content])

  const addIdx = () => {
    setIdx((c) => c + 1);
  };
  const subtractIdx = () => {
    setIdx((c) => c - 1);
  };

  const handleClickLikeDislike = async (messageId: string, liked?: boolean) => {
    await putChatMessage_Id({
      chatId: chatId || "",
      messageId,
      data: {
        liked: liked === undefined ? false : liked,
        disliked: liked === undefined ? false : !liked,
      }
    });

    collectEvent(liked ? "click_like_btn" : "click_unlike_btn", {
      onoff: liked === undefined ? false : true,
      position: "chat",
      feature_menu: "chat",
      feature_category: "",
      feature_name: "",
      feature_tag: "",
      feature_form: "",
    });

    setLiked(liked === undefined ? false : liked);
    setDisliked(liked === undefined ? false : !liked);
  }

  const isStreamING = messages[idx]._id.startsWith("loading-invisible-");
  const { displayText } = useNaturalDisplayText();

  const showRef = msg.reference && msg.reference.length > 0;
  const refText = msg.reference?.map(f => `p${f}`).join(" ");

  return (
    <FlexWrapper column align="center" style={{width: '100%'}}>
    <ChatAIBoxWrapper
      column
      style={{ padding: "0px", gap: "0px", alignItems: "center" }}
    >
      <ChatTopWrapper row align="flex-start" justify="center" invisible={!showRef}>
        <ChatIcon>
          <ChatLogo />
        </ChatIcon>
        <ChatText>
          {isStreamING && streamMessage === "" && (
            <CursorWrapper>
              <Cursor />
            </CursorWrapper>
          )}
          {isStreamING && streamMessage !== "" && (
            <ChatMarkdownComponent.Wrapper text={displayText} type={messages[idx]?.type}/>
          )}
          {!isStreamING && (
            <ChatMarkdownComponent.Wrapper text={messages[idx].content} type={messages[idx]?.type}/>
          )}
        </ChatText>
      </ChatTopWrapper>
      {showRef && <ChatDivider />}
      <BottomWrapper>
        <ResourceWrapper
          invisible={!showRef}>
          <label>출처 및 참고</label>
          <div
            onMouseEnter={hoverPopup.onMouseEnter}
            onMouseLeave={hoverPopup.onMouseLeave}
            ref={hoverPopup.hoverRef}>
            <Icon
              size={20}
              icon="question-circle"
              color={newColors.GRAY_500}
            />
          </div>
          {hoverPopup.open(() => {
            return (
              <ModalPositionPortal
                position={{
                  top: hoverPopup.hoverRef?.current && hoverPopup.hoverRef?.current.getBoundingClientRect().top + 25,
                  left: hoverPopup.hoverRef?.current && hoverPopup.hoverRef?.current.getBoundingClientRect().left + 30,
                }}
              >
                <Popup>페이지가 잘못 표기될 경우 오른쪽의 엄지 아이콘으로 알려주세요.</Popup>
              </ModalPositionPortal>
            )
          })}
          <div>{refText}</div>
        </ResourceWrapper>
        <ToolButtonWrapper row align="center" justify="flex-end">
          {!isStreamING && (
            <>
              <ChatIconButton onClick={() => handleClickLikeDislike(messages[idx]._id, liked ? undefined : true)}>
                {liked ? <FilledLikeIcon /> : <LikeIcon />}
              </ChatIconButton>
              <ChatIconButton onClick={() => handleClickLikeDislike(messages[idx]._id, disliked ? undefined : false)}>
                {disliked ? <FilledDislikeIcon /> : <DislikeIcon />}
              </ChatIconButton>
              <CopyToClipboard text={messages[idx].content}>
                <CopyButton
                  onClick={() => {
                    onCopy();
                    collectEvent("click_copy_btn", {
                      position: "chat",
                      feature_menu: "chat",
                    });
                  }}
                >
                  <Copy />
                </CopyButton>
              </CopyToClipboard>
            </>)}
        </ToolButtonWrapper>
      </BottomWrapper>
      <AbsoluteWrapper>
        <StickyWrapper>
          {withLeftTop &&
            withLeftTop({
              idx,
              add: addIdx,
              subtract: subtractIdx,
              max: messages.length,
            })}
        </StickyWrapper>
      </AbsoluteWrapper>
    </ChatAIBoxWrapper>
    {withBottomCenter && 
      withBottomCenter({
        idx,
        add: addIdx,
        subtract: subtractIdx,
        max: messages.length,
      })}     
    </FlexWrapper>
  )
}

export const ChatTopWrapper = styled(FlexWrapper)<{ invisible: boolean }>`
  gap: 30px;
  width: 100%;
  padding: ${props => props.invisible ? "32px 38px 0px 31px" : "32px 38px 36px 31px"};

  @media (max-width: 767px) {
    padding: 36px 30px 10px;
  }
`;

const ChatDivider = styled.div`
  width: 95%;
  height: 1px;
  background: ${colors.BLUE_GRAY_LINE};
`

const BottomWrapper = styled(ChatBottomWrapper)`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  padding: 16px 30px;
`

const ResourceWrapper = styled(FlexWrapper)<{ invisible: boolean }>`
  flex-direction: row;
  gap: 10px;
  align-items: center;
  visibility: ${props => props.invisible ? "hidden" : "inherit"};

  ${typo({
    size: "14px",
    weight: 500,
    color: newColors.GRAY_500,
  })}
`

const ToolButtonWrapper = styled(FlexWrapper)`
  gap: 14px;
`;

const Popup = styled.div`
  background-color: ${colors.gray_80};
  padding: 4px 9px;
  ${typo({
    weight: 600,
    size: "12px",
    color: colors.white,
  })};
  transform: translateX(-50%);
  border-radius: 5px;
`;
