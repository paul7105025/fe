import React from "react";

import {
  useEvent,
  imageUrlType,
  putChatImage_Id,
  newMessageType,
  postGenerateUpscaledImageDownload,
} from "@wrtn/core";

import { getOriginIndex } from "src/utils/getOriginIndex";

import { withLeftTopProps } from ".";

import { ChatImageModal } from "../ChatImageModal";
import { ChatToolPagination } from "../ChatToolPagination";

import { Icon, colors, FlexWrapper, FlexButton, newColors, typo } from "@wrtn/ui";
import {
  AbsoluteWrapper,
  ChatAIBoxWrapper,
  ChatIcon,
  ChatText,
  DislikeIcon,
  FilledDislikeIcon,
  FilledLikeIcon,
  ChatIconWrapper,
  LikeIcon,
  ChatLogo,
  StickyWrapper,
  ChatIconButton,
} from "./styles";
import styled from "styled-components";
import { ChatMarkdownComponent } from "./MarkdownComponents";
import { DotPulse } from "@uiball/loaders";
import useHoverMenu from "@wrtn/core/hooks/useHoverMenu";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

interface ChatImagesOutputProps {
  contents: newMessageType[];

  withLeftBottom?: () => JSX.Element;
  withLeftTop?: ({ idx, add, subtract, max }: withLeftTopProps) => JSX.Element;
  withBottomCenter?: ({ idx, add, subtract, max }: withLeftTopProps) => JSX.Element;
}

export const ChatImagesOutput = ({
  contents,
  withLeftBottom,
  withLeftTop,
  withBottomCenter,
}: ChatImagesOutputProps) => {
  const imageContainerRefs = React.useRef<HTMLDivElement[]>([]);

  const { collectEvent } = useEvent();

  const [idx, setIdx] = React.useState(0);
  const [modalIdx, setModalIdx] = React.useState(0);
  const [imageIdx, setImageIdx] = React.useState(0);

  const [isModalOpen, setIsModalOpen] = React.useState(false);

  const [likedList, setLikedList] = React.useState<
    Array<
      Array<{
        liked: boolean;
        disliked: boolean;
      }>
    >
  >(
    contents.map((v) =>
      v.image.imageUrls.map((img) => ({
        liked: img.liked,
        disliked: img.disliked,
      }))
    )
  );

  const imageDownloadHover = useHoverMenu({
    position: {
      top: 25,
      left: -130,
    }
  })

  React.useEffect(() => {
    setLikedList(
      contents.map((v) =>
        v.image.imageUrls.map((img) => ({
          liked: img.liked,
          disliked: img.disliked,
        }))
      )
    );
  }, [contents]);

  React.useEffect(() => {
    const container = document.getElementById('ScrollContainer')
    const handleScroll = () => {
      imageDownloadHover.handleMouseOut();
    }

    if (imageDownloadHover.hover) {
      container.addEventListener('scroll', handleScroll);
    } else {
      container.removeEventListener('scroll', handleScroll);
    }
  }, [imageDownloadHover.hover])

  const currentLikedList = likedList[idx];

  const setLiked = (
    chatIndex: number,
    imageIndex: number,
    type: "liked" | "disliked"
  ) =>
    setLikedList((c) =>
      c.map((v, i) =>
        i === chatIndex
          ? v.map((v, i) =>
              i === imageIndex
                ? {
                    liked: type === "liked" ? !v.liked : false,
                    disliked: type === "disliked" ? !v.disliked : false,
                  }
                : v
            )
          : v
      )
    );

  const openChatImageModal = () => setIsModalOpen(true);
  const closeChatImageModal = () => setIsModalOpen(false);

  const allImages = React.useMemo(
    () =>
      contents.reduce((acc, cur) => {
        if (cur.image.imageUrls.length > 0) {
          return [...acc, ...cur.image.imageUrls];
        }
        return acc;
      }, [] as imageUrlType[]),
    [contents]
  );

  const currentModalImage = React.useMemo(
    () => allImages[modalIdx],
    [allImages, modalIdx]
  );

  const currentModalImageId = React.useMemo(
    () => contents[idx].image._id,
    [contents, idx]
  );

  const { contentIndex, urlIndex } = getOriginIndex(
    currentModalImage,
    contents
  );

  const addIdx = () => setIdx((c) => c + 1);
  const subtractIdx = () => setIdx((c) => c - 1);

  const handleClickFavoriteImage = async (
    chatIndex: number,
    imageIndex: number,
    type: "liked" | "disliked"
  ) => {
    const res = await putChatImage_Id({
      imageId: contents[chatIndex].image._id,
      data: {
        index: imageIndex,
        liked:
          type === "liked" ? !likedList[chatIndex][imageIndex].liked : false,
        disliked:
          type === "disliked"
            ? !likedList[chatIndex][imageIndex].disliked
            : false,
      },
    });
    if (res?.status <= 201) {
      setLiked(chatIndex, imageIndex, type);
      collectEvent(type === "liked" ? "click_like_btn" : "click_unlike_btn", {
        onoff:
          type === "liked"
            ? !likedList[chatIndex][imageIndex].liked
            : !likedList[chatIndex][imageIndex].disliked,
        position: "chat",
        feature_menu: "image",
        feature_category: "",
        feature_name: "",
        feature_tag: "",
        feature_form: "",
      });
    }
  };

  const handleShowDonwloadModal = (index: number) => {
    imageDownloadHover.parentRef.current = imageContainerRefs[index];
    imageDownloadHover.handleClick();
  }

  const downloadUpscaledImage = React.useCallback(async (targetUrl: string, imageId: string) => {
    const res = await postGenerateUpscaledImageDownload({
      targetUrl,
      imageId,
    });

    const link = document.createElement("a");
    link.href = res.data.data;
    link.setAttribute('download', 'true');
    document.body.appendChild(link);
    link.click();

    document.body.removeChild(link);

    collectEvent("click_img_download_btn", {
      position: "chat",
      is_upscale: true,
    });
  }, [collectEvent]);

  const openModalImage = (url: string) => {
    const modalIndex = allImages.findIndex((v) => v.url === url);

    setModalIdx(modalIndex);
    openChatImageModal();
  };

  const addModalIdx = () => setModalIdx((c) => c + 1);
  const subtractModalIdx = () => setModalIdx((c) => c - 1);

  const isStreamING = contents[idx].content === "";

  return (
    <FlexWrapper column align={"center"} style={{width: '100%'}}>
      <ChatAIBoxWrapper key={contents[idx]._id} style={isStreamING ? { alignItems: "center" } : {}}>
        <ChatIcon>
          <ChatLogo />
        </ChatIcon>
        <ChatText>
          {isStreamING ? (
            <div style={{ display: "flex", alignItems: "center", paddingLeft: "1px", height: "27px"}}>
              <DotPulse size={20} speed={1.3} color={colors.GRAY_60}/>
            </div>
          ) : (
            <ChatMarkdownComponent.Wrapper text={contents[idx].content} type={contents[idx]?.type}/>
          )}
          {isStreamING || <ChatImageContainer>
            {contents[idx]?.image.imageUrls.map((v, i) => {
              return (
                <ChatImageWrapper key={v.url}>
                  <ChatImage src={v.url} alt={"ai generated"} />
                  <ChatImageBackground column align="flex-start">
                    <ChatImageButton onClick={() => openModalImage(v.url)}>
                      <Icon
                        icon="arrows-maximize"
                        size={20}
                        color={colors.WHITE}
                        hoverColor={colors.HOVER_PURPLE}
                      />
                    </ChatImageButton>
                    <div style={{ flex: 1 }} />
                    <FlexWrapper
                      style={{
                        width: "100%",
                        gap: 12,
                      }}
                    >
                      <div style={{ flex: 1 }} />
                      <ChatImageButton
                        onClick={() => handleClickFavoriteImage(idx, i, "liked")}
                      >
                        {currentLikedList[i]?.liked ? (
                          <FilledLikeIcon />
                        ) : (
                          <LikeIcon />
                        )}
                      </ChatImageButton>
                      <ChatImageButton
                        onClick={() =>
                          handleClickFavoriteImage(idx, i, "disliked")
                        }
                      >
                        {currentLikedList[i]?.disliked ? (
                          <FilledDislikeIcon />
                        ) : (
                          <DislikeIcon />
                        )}
                      </ChatImageButton>
                      <div
                        onClick={() => {
                          setImageIdx(i);
                          handleShowDonwloadModal(i)
                        }}
                        ref={(ref) => (imageContainerRefs[i] = ref)}
                      >
                        <ChatImageButton>
                          <Icon
                            icon="import"
                            size={20}
                            color={colors.WHITE}
                            hoverColor={colors.HOVER_PURPLE}
                          />
                        </ChatImageButton>
                      </div>
                      {imageDownloadHover.open(({ position, childRef }) => {
                        return (
                          <ModalPositionPortal position={position}>
                            <HoverWrapper ref={childRef}>
                              <GuideOuterItemWrapper>
                                <GuideOuterItem
                                  href={contents[idx]?.image.imageUrls[imageIdx].url}
                                  download
                                  rel="noopener noreferrer"
                                  onClick={() => 
                                    collectEvent("click_img_download_btn", {
                                      position: "chat",
                                      is_upscale: false,
                                    })
                                  }
                                >
                                  <Icon
                                    icon="import"
                                    size={20}
                                    color={newColors.GRAY_400}
                                    hoverColor={colors.HOVER_PURPLE}
                                  />
                                  다운로드</GuideOuterItem>
                              </GuideOuterItemWrapper>
                              <GuideOuterItemWrapper>
                                <GuideOuterItem onClick={() => downloadUpscaledImage(contents[idx]?.image.imageUrls[imageIdx].url, contents[idx].image._id)}>
                                  <Icon
                                    icon="expand-alt"
                                    size={20}
                                    color={newColors.GRAY_400}
                                    hoverColor={colors.HOVER_PURPLE}
                                  />
                                  고화질 다운로드</GuideOuterItem>
                              </GuideOuterItemWrapper>
                            </HoverWrapper>
                          </ModalPositionPortal>
                        )
                      })}
                    </FlexWrapper>
                  </ChatImageBackground>
                </ChatImageWrapper>
              );
            })}
          </ChatImageContainer>}
        </ChatText>
        <ChatIconWrapper>
          <ChatIconButton
            onClick={() => {
              // download
            }}
          >
            {/* <Icon icon="plus-circle" size={22} color={"#A1A5B9"} /> */}
          </ChatIconButton>
        </ChatIconWrapper>
        <AbsoluteWrapper>
          <StickyWrapper>
            {withLeftTop &&
              withLeftTop({
                idx,
                add: addIdx,
                subtract: subtractIdx,
                max: contents.length,
              })}
            {withLeftBottom && withLeftBottom()}
          </StickyWrapper>
        </AbsoluteWrapper>
        {isStreamING || <ChatImageModal
          isModalOpen={isModalOpen}
          currentModalImage={currentModalImage}
          currentModalImageId={currentModalImageId}
          currentLike={{
            liked: likedList[contentIndex][urlIndex].liked,
            disliked: likedList[contentIndex][urlIndex].disliked,
          }}
          closeChatImageModal={closeChatImageModal}
          handleClickFavoriteImage={(type) =>
            handleClickFavoriteImage(contentIndex, urlIndex, type)
          }
          pagination={() => (
            <ChatToolPagination
              idx={modalIdx}
              max={allImages.length}
              add={addModalIdx}
              subtract={subtractModalIdx}
              isHorizontal={true}
            />
          )}
        />}
      </ChatAIBoxWrapper>
      {withBottomCenter && 
        withBottomCenter({
          idx,
          add: addIdx,
          subtract: subtractIdx,
          max: contents.length,
        })}    
    </FlexWrapper>
  );
};

const ChatImageContainer = styled.div`
  margin-top: 33px;
  display: grid;
  grid-row: auto auto;
  grid-column: 164px;
  grid-gap: 13.5px 14px;
  grid-template-columns: 1fr 1fr;

  @media (max-width: 767px) {
    grid-template-columns: 1fr;
  }
`;

const ChatImageWrapper = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 5px;
  overflow: hidden;

  position: relative;

  max-height: 185.97px;

  @media (max-width: 767px) {
    max-height: 120px;
  }
  @media (max-width: 480px) {
    max-height: 100px;
  }
`;

const ChatImage = styled.img`
  width: 100%;
  height: 100%;

  object-fit: cover;

  background-color: transparent;
`;

const ChatImageBackground = styled(FlexWrapper)`
  width: 100%;
  height: 100%;

  padding: 10px 13px 12px;
  opacity: 0;

  background: linear-gradient(
    180deg,
    rgba(0, 0, 0, 0.8) 0%,
    rgba(0, 0, 0, 0) 29.37%,
    rgba(0, 0, 0, 0) 72.37%,
    rgba(0, 0, 0, 0.8) 100%
  );

  position: absolute;

  top: 0px;
  left: 0px;

  &:hover {
    opacity: 1;
  }
`;

const ChatImageButton = styled(FlexButton)`
  cursor: pointer;

  svg {
    width: 22px;
    height: 22px;
    path {
      fill: ${colors.WHITE};
    }
  }
  :hover {
    svg {
      path {
        fill: ${colors.HOVER_PURPLE};
      }
    }
  }
`;

const HoverWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  flex-direction: column;
  justify-content: flex-start;
  background-color: ${newColors.WHITE};
  padding: 8px 0px;
  border-radius: 8px;
  border: 1px solid ${newColors.GRAY_200};
  overflow: hidden;
`;

const NavOuterLink = styled.a`
  gap: 10px;

  display: flex;
  align-items: center;

  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: newColors.GRAY_700,
  })};

  cursor: pointer;
  padding: 6px 8px;
  &:hover {
    background: ${newColors.GRAY_000};
    border-radius: 5px;
  }
`;

const GuideOuterItemWrapper = styled.div`
  width: 100%;
`;

const GuideOuterItem = styled(NavOuterLink)`
  text-align: left;
  width: 100%;
  min-width: 105px;
  padding: 12px 16px;
  border-radius: 0px;
  &:hover {
    border-radius: 0px;
  }
`;

