import React from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";

import { ChatAssistantOutputProps } from ".";

import { useEvent, putChatMessage_Id, streamMessageState } from "@wrtn/core";

import { Copy } from "@wrtn/ui";
import {
  AbsoluteWrapper,
  ChatAIBoxWrapper,
  ChatIcon,
  ChatText,
  CopyButton,
  DislikeIcon,
  FilledDislikeIcon,
  FilledLikeIcon,
  ChatIconWrapper,
  LikeIcon,
  ChatLogo,
  StickyWrapper,
  ChatIconButton,
} from "./styles";
import { ChatMarkdownComponent } from "./MarkdownComponents";
import { MetadataContainer } from "src/containers/MetadataContainer";

export const ChatPluginOutput = ({
  contents,
  chatId,
  onCopy,
}: ChatAssistantOutputProps) => {
  const [idx, setIdx] = React.useState(0);
  const [dislikedList, setDislikedList] = React.useState<boolean[]>(
    contents.map((v) => v.disliked)
  );
  const [likedList, setLikedList] = React.useState<boolean[]>(
    contents.map((v) => v.liked)
  );

  const { collectEvent } = useEvent();

  React.useEffect(() => {
    setIdx(0);
  }, [contents.length]);

  React.useEffect(() => {
    setLikedList(contents.map((v) => v.liked));
    setDislikedList(contents.map((v) => v.disliked));
  }, [contents]);

  const liked = likedList[idx];
  const disliked = dislikedList[idx];

  const setLiked = (val: boolean) =>
    setLikedList((c) => [...c.map((v, i) => (i === idx ? val : v))]);
  const setDisliked = (val: boolean) =>
    setDislikedList((c) => [...c.map((v, i) => (i === idx ? val : v))]);

  const handleClickFavorite = async (id: string) => {
    await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: !liked,
        disliked: false,
      },
    });

    collectEvent("click_like_btn", {
      onoff: !liked,
      position: "chat",
      feature_menu: "search",
      feature_category: "",
      feature_name: "",
      feature_tag: "",
      feature_form: "",
    });
    setLiked(!liked);

    setDisliked(false);
  };

  const handleClickDislike = async (id: string) => {
    await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: false,
        disliked: !disliked,
      },
    });

    collectEvent("click_unlike_btn", {
      onoff: !disliked,
      position: "chat",
      feature_menu: "search",
      feature_category: "",
      feature_name: "",
      feature_tag: "",
      feature_form: "",
    });
    setLiked(false);
    setDisliked(!disliked);
  };

  const links = React.useMemo(() => {
    const linkMarkdowns = contents[idx].content.match(/\[([^\[\]]+)\](\(([^\(\)]+)\)|\[([^\[\]]+)\])/g);
    return linkMarkdowns?.map(md => md.split("(")[1].replace(")", ""));
  }, [contents[idx]]);

  return (
    <ChatAIBoxWrapper key={contents[idx]._id}>
      <ChatIcon>
        <ChatLogo />
      </ChatIcon>
      <ChatText>
        <ChatMarkdownComponent.Wrapper text={contents[idx].content} type={contents[idx]?.type}/>
        {links && <MetadataContainer links={links}/>}
      </ChatText>
      <ChatIconWrapper>
        <>
          <ChatIconButton onClick={() => handleClickFavorite(contents[idx]._id)}>
            {liked ? <FilledLikeIcon /> : <LikeIcon />}
          </ChatIconButton>
          <ChatIconButton onClick={() => handleClickDislike(contents[idx]._id)}>
            {disliked ? <FilledDislikeIcon /> : <DislikeIcon />}
          </ChatIconButton>
        </>
        <CopyToClipboard text={contents[idx].content}>
          <CopyButton
            onClick={() => {
              onCopy();
              collectEvent("click_copy_btn", {
                position: "chat",
                feature_menu: "search",
                feature_category: "",
                feature_name: "",
                feature_tag: "",
                feature_form: "",
              });
            }}
          >
            <Copy />
          </CopyButton>
        </CopyToClipboard>
      </ChatIconWrapper>
      
    </ChatAIBoxWrapper>
  );
};
