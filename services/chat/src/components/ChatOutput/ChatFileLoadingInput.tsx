import { useEffect, useState } from "react";
import styled, { keyframes } from "styled-components";
import { useRecoilValue } from "recoil";

import { fileLoadingProgressState, messageType, userState } from "@wrtn/core";
import { Avatar } from "@wrtn/ui/components/Avatar";
import { DefaultSpinner, FlexWrapper, Icon, colors } from "@wrtn/ui";
import { ChatIcon, ChatText } from "./styles";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { Spinner } from "@wrtn/ui/components/Spinner";


export const ChatFileLoadingInput = ({ message, showProgress }: { message: messageType, showProgress: boolean }) => {
  const user = useRecoilValue(userState);
  const percent = useRecoilValue(fileLoadingProgressState);
  const [dealt, setDealt] = useState(0);

  useEffect(() => {
    if (dealt < 100 && dealt < percent) {
      const timer = setInterval(() => {
        setDealt(dealt => dealt+1)
      }, 10)

      return () => clearInterval(timer);
    }
  }, [dealt, percent]);

  return (
    <LoadingBox key={message._id}>
      {showProgress && (
        <Progress>
          <Dealt dealt={dealt} />
        </Progress>
      )}
      <ChatBoxWrapper>
        <ChatIcon>
          <Avatar width="27px">{user?.name[0]}</Avatar>
        </ChatIcon>
        <ChatText>
          <Icon
            icon="paperclip"
            size={20}
            color={colors.GRAY_70}
          />
          <span style={{ marginLeft: '10px' }}>{message.content}</span>
        </ChatText>
        {showProgress && (
          <Spinner width={22} height={22} />
        )}
      </ChatBoxWrapper>
    </LoadingBox>
  );
};

export const LoadingBox = styled(FlexWrapper)`
  flex-direction: column;
  background: ${colors.WHITE};
  border: 1px solid #d1d7ed;
  border-radius: 8px;
  margin-bottom: 20px;
`

export const ChatBoxWrapper = styled(FlexWrapper)`
  flex-direction: row;
  gap: 30px;
  padding: 32px 38px 34px 31px;
  text-align: justify;
  align-items: flex-start;
  width: 100%;
`

const LoadingBar = keyframes`
  0% { width: 0 }
  20% { width: 50% }
  100% { width: 80% }
`

const Progress = styled.div`
  width: 100%;
  height: 8px;
  background: ${colors.WHITE};
  border-radius: 7px 7px 0 0;
`;

const Dealt = styled.div<{ dealt: number }>`
  border-radius: ${(props) => props.dealt >= 98 ? '6.5px 6.5px 0 0' : '6.5px 0 0 0'};
  background-color: #6446FF;
  width: ${(props) => props.dealt + "%"};
  height: 100%;
  animation: linear 3s ease-out 1;
`;
