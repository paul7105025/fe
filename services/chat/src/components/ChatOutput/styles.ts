import styled from "styled-components";

import { FlexWrapper, colors, typo, newColors, newTypo } from "@wrtn/ui";

import { FilledDislike, Dislike, Copy } from "@wrtn/ui/assets";
import { ReactComponent as ChatWrtnLogo } from "@wrtn/ui/assets/wrtn/logo_chat_wrtn.svg";

export const ChatBoxWrapper = styled(FlexWrapper)`
  background: ${colors.WHITE};
  border: 1px solid #d1d7ed;
  border-radius: 8px;
  padding: 32px 38px 34px 31px;
  margin-bottom: 20px;
  gap: 30px;
  text-align: justify;
  align-items: flex-start;
`;

export const ChatAIBoxWrapper = styled(ChatBoxWrapper)`
  width: 100%;
  background: #f2f7ff;
  padding: 32px 38px 45px 31px;
  position: relative;
`;

export const ChatIcon = styled.div`
  width: 27px;
  height: 27px;
  svg {
    width: 100%;
    height: 100%;
  }
`;

export const ChatText = styled.div`
  max-width: 100%;
  overflow-x: hidden;
  flex: 1;
  ${typo({ weight: 600, size: "16px", height: "180%", color: newColors.GRAY_700 })};

  p {
    margin-bottom: 10px;
  }

  table {
    border: 1px solid ${colors.BLUE_GRAY_LINE};
    background: white;
    border-collapse: collapse;
  }

  tbody {
  }

  thead {
  }

  tr {
    border-bottom: 1px solid ${colors.BLUE_GRAY_LINE};
  }

  th {
    padding: 8px;
    ${typo({
      weight: 600,
      size: "14px",
      height: "160%",
      color: colors.gray_80,
    })};
  }

  td {
    padding: 8px;
    ${typo({
      weight: 500,
      size: "14px",
      height: "160%",
      color: colors.gray_80,
    })};
  }

  pre {
    padding: 0px;
    background: ${colors.BLACK};
    color: white;
    border-radius: 8px;
  }
`;

export const ChatIconWrapper = styled(FlexWrapper)`
  position: absolute;
  bottom: 15px;
  right: 18px;
  gap: 14px;
`;

export const CopyButton = styled.button`
  border: none;
  background: none;
  width: 16px;
  height: 16px;
  svg {
    width: 100%;
    height: 100%;
    path {
      fill: ${colors.gray_55};
    }
  }

  &:hover {
    cursor: pointer;
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  }
`;

export const ChatLogo = styled(ChatWrtnLogo)`
  width: 46px;
  height: 46px;
`;

export const ChatIconButton = styled.button`
  padding: 0px;
  margin: 0px;
  width: 22px;
  height: 22px;
  background: none;
  border: none;
  cursor: pointer;
  > svg {
    width: 22px;
    height: 22px;
    path {
      fill: ${colors.GRAY_55};
    }
  }
  &:hover {
    > svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  }
`;

export const DislikeIcon = styled(Dislike)``;

export const LikeIcon = styled(DislikeIcon)`
  transform: rotate(180deg);
`;

export const FilledDislikeIcon = styled(FilledDislike)``;

export const FilledLikeIcon = styled(FilledDislikeIcon)`
  transform: rotate(180deg);
`;

export const ChatBottomWrapper = styled(FlexWrapper)`
  padding: 18px 18px 18px 26px;
`;

export const ChatTopWrapper = styled(FlexWrapper)`
  gap: 30px;
  width: 100%;
  padding: 32px 38px 36px 31px;

  @media (max-width: 767px) {
    padding: 36px 30px;
  }
`;

export const ChatLogoIcon = styled(FlexWrapper)`
  max-width: 36px;
  height: 26px;
  svg {
    height: 26px;
    display: block;
    margin: 0 auto;
    text-align: center;
    g {
      transform-origin: center center;
    }
  }
`;

export const ChatTitle = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    color: colors.gray_60,
  })};

  margin-right: 16px;
  white-space: nowrap;
`;

export const ChatDivider = styled.div`
  width: 100%;
  height: 1px;
  background: ${colors.BLUE_GRAY_LINE};
`;

export const AbsoluteWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-end;
  position: absolute;

  left: 0;
  bottom: 0px;
  height: calc(100% - 16px);

  transform: translateX(calc(-100% - 16px));

  @media (max-width: 767px) {
    bottom: -53px;
    left: 0;
    transform: none;
    height: auto;
    width: 100%;
  }
`;

export const StickyWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-end;

  padding-bottom: 10px;

  position: sticky;

  left: -20px;
  bottom: 40px;

  gap: 16px;
`;


export const ChatPreHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background: ${newColors.GRAY_800};
  padding: 12px 20px;
  border-radius: 8px 8px 0px 0px;
`;


export const CodeCopy = styled(Copy)`
  cursor: pointer;
  path {
    fill: ${newColors.WHITE};
  }
`;

export const ChatCode = styled.code`
  width: 100%;
  ${newTypo("body_compact-16-semi")}
`