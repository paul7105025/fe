import React from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";

import { ChatAssistantOutputProps } from ".";

import {
  useEvent,
  putChatMessage_Id,
  streamMessageState,
  userState,
  useLoginDialog,
} from "@wrtn/core";

import { Copy, FlexWrapper } from "@wrtn/ui";
import {
  AbsoluteWrapper,
  ChatAIBoxWrapper,
  ChatIcon,
  ChatText,
  CopyButton,
  DislikeIcon,
  FilledDislikeIcon,
  FilledLikeIcon,
  ChatIconWrapper,
  LikeIcon,
  ChatLogo,
  StickyWrapper,
  ChatIconButton,
} from "./styles";
import { useNaturalDisplayText } from "src/hooks/useNaturalDisplayText";
import { ChatMarkdownComponent } from "./MarkdownComponents";
import { useRecoilValue } from "recoil";
import { Cursor, CursorWrapper } from "./StreamLoadingOutput";

export const ChatAssistantOutput = ({
  contents,
  chatId,
  onCopy,
  withLeftTop,
  withBottomCenter,
}: ChatAssistantOutputProps) => {
  const [idx, setIdx] = React.useState(0);
  const [dislikedList, setDislikedList] = React.useState<boolean[]>(
    contents.map((v) => v.disliked)
  );
  const [likedList, setLikedList] = React.useState<boolean[]>(
    contents.map((v) => v.liked)
  );

  const { collectEvent } = useEvent();
  const { handleOpen } = useLoginDialog();

  const user = useRecoilValue(userState);
  const streamMessage = useRecoilValue(streamMessageState);

  React.useEffect(() => {
    setIdx(0);
  }, [contents.length]);

  React.useEffect(() => {
    setLikedList(contents.map((v) => v.liked));
    setDislikedList(contents.map((v) => v.disliked));
  }, [contents]);

  const liked = likedList[idx];
  const disliked = dislikedList[idx];

  const setLiked = (val: boolean) =>
    setLikedList((c) => [...c.map((v, i) => (i === idx ? val : v))]);
  const setDisliked = (val: boolean) =>
    setDislikedList((c) => [...c.map((v, i) => (i === idx ? val : v))]);

  const addIdx = () => {
    setIdx((c) => c + 1);
  };
  const subtractIdx = () => {
    setIdx((c) => c - 1);
  };

  const handleClickFavorite = async (id: string) => {
    if (!user) {
      handleOpen();
      return;
    }

    await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: !liked,
        disliked: false,
      },
    });

    collectEvent("click_like_btn", {
      onoff: !liked,
      position: "chat",
      feature_menu: "chat",
      feature_category: "",
      feature_name: "",
      feature_tag: "",
      feature_form: "",
    });
    setLiked(!liked);

    setDisliked(false);
  };

  const handleClickDislike = async (id: string) => {
    if (!user) {
      handleOpen();
      return;
    }
    
    await putChatMessage_Id({
      chatId: chatId || "",
      messageId: id,
      data: {
        liked: false,
        disliked: !disliked,
      },
    });

    collectEvent("click_unlike_btn", {
      onoff: !disliked,
      position: "chat",
      feature_menu: "chat",
      feature_category: "",
      feature_name: "",
      feature_tag: "",
      feature_form: "",
    });
    setLiked(false);
    setDisliked(!disliked);
  };

  const isStreamING = contents[idx]._id.startsWith("loading-invisible-");
  const { displayText } = useNaturalDisplayText();

  return (
    <FlexWrapper column align="center" style={{width: '100%'}}>
    <ChatAIBoxWrapper key={contents[idx]._id}>
      <ChatIcon>
        <ChatLogo />
      </ChatIcon>
      <ChatText>
        {isStreamING && streamMessage === "" && (
          <CursorWrapper>
            <Cursor />
          </CursorWrapper>
        )}
        {isStreamING && streamMessage !== "" && (
          <ChatMarkdownComponent.Wrapper text={displayText} type={contents[idx]?.type}/>
        )}
        {!isStreamING && (
          <ChatMarkdownComponent.Wrapper text={contents[idx].content} type={contents[idx]?.type}/>
        )}
      </ChatText>
      <ChatIconWrapper>
        {contents[0].type !== "ToolChipOutput" && !isStreamING && (
          <>
            <ChatIconButton
              onClick={() => handleClickFavorite(contents[idx]._id)}
            >
              {liked ? <FilledLikeIcon /> : <LikeIcon />}
            </ChatIconButton>
            <ChatIconButton
              onClick={() => handleClickDislike(contents[idx]._id)}
            >
              {disliked ? <FilledDislikeIcon /> : <DislikeIcon />}
            </ChatIconButton>
          </>
        )}
        {!isStreamING && (
          <CopyToClipboard text={contents[idx].content}>
            <CopyButton
              onClick={() => {
                onCopy();
                collectEvent("click_copy_btn", {
                  position: "chat",
                  feature_menu: "chat",
                  feature_category: "",
                  feature_name: "",
                  feature_tag: "",
                  feature_form: "",
                });
              }}
            >
              <Copy />
            </CopyButton>
          </CopyToClipboard>
        )}
      </ChatIconWrapper>
      <AbsoluteWrapper>
        <StickyWrapper>
          {withLeftTop &&
            withLeftTop({
              idx,
              add: addIdx,
              subtract: subtractIdx,
              max: contents.length,
            })}
        </StickyWrapper>
      </AbsoluteWrapper>
    </ChatAIBoxWrapper>
      {withBottomCenter && 
        withBottomCenter({
          idx,
          add: addIdx,
          subtract: subtractIdx,
          max: contents.length,
        })}    
    </FlexWrapper>
  );
};
