import styled from 'styled-components';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { ChatBottomWrapper, ChatBoxWrapper, ChatIcon, ChatIconButton, ChatText } from './styles';
import { allFilesState, selectedTemplateState, useModalOpen, userState } from '@wrtn/core';
import { Avatar } from '@wrtn/ui/components/Avatar';
import { FlexWrapper, Icon, colors, newColors, typo } from '@wrtn/ui';
import { ChatTemplate } from './ChatUserOutput';
import { useFileManage } from 'src/hooks';
import { useMemo } from 'react';

interface ChatFileUserInputProps {
  message: {
    role: string;
    _id: string;
    content: string;
    fileId?: string;
  };
}

export const ChatFileUserInput = ({
  message,
}: ChatFileUserInputProps) => {
  const user = useRecoilValue(userState);
  const setTemplate = useSetRecoilState(selectedTemplateState);
  const files = useRecoilValue(allFilesState)

  const { handleDownloadFile } = useFileManage();

  const file = useMemo(() => files.find(file => file._id === message?.fileId), [message, files]);

  const modalOpen = useModalOpen({ modalId: "templateAddModal" });
  return (
    <>
    <ChatBoxWrapper key={message._id} style={{ flexDirection: "column", gap: 0, paddingBottom: 0, paddingRight: "30px" }}>
      <TopWrapper style={{ alignItems: "flex-start", width: "100%" }}>
        <ChatIcon>
          <Avatar width="27px">{user?.name[0]}</Avatar>
        </ChatIcon>
        <ChatText style={{ flex: 1 }}>
          {message.content}
        </ChatText>
        <FlexWrapper style={{ alignSelf: "flex-end" }}>
          <ChatIconButton
            onClick={() => {
              setTemplate({
                id: "",
                name: "",
                content: message.content,
              });
              modalOpen.open();
            }}
          >
            <ChatTemplate />
          </ChatIconButton>
        </FlexWrapper>
      </TopWrapper>
      {file && (
        <>
          <ChatDivider />
          <BottomWrapper>
            <Icon icon="paperclip" size={20} color={colors.GRAY_70} />
            <FileName
              isDeleted={file.isDeleted}
              onClick={file.isDeleted ? undefined : () => handleDownloadFile(file._id)}>{file.fileName}</FileName>
          </BottomWrapper>
        </>
      )}
    </ChatBoxWrapper>
    </>
  )
}

const ChatDivider = styled.div`
  width: 100%;
  height: 1px;
  background: ${colors.BLUE_GRAY_LINE};
`

const TopWrapper = styled(FlexWrapper)`
  flex-direction: row;
  gap: 30px;
  margin-bottom: 30px;
`

const BottomWrapper = styled(ChatBottomWrapper)`
  flex-direction: row;
  padding: 20px 0;
  gap: 16px;

  ${typo({
    size: "14px",
    weight: 500,
    color: newColors.GRAY_500,
  })}
`
const FileName = styled.span<{ isDeleted: boolean }>`
  &:hover {
    cursor: ${({ isDeleted }) => isDeleted ? "default" : "pointer"};
    text-decoration: ${({ isDeleted }) => isDeleted ? "default" : "underline"};
  }
`