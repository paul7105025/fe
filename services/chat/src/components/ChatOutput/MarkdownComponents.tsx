import { ChatCode, ChatPreHeader, CodeCopy } from "./styles";
import { useAlert } from "react-alert";
import React from "react";
import ReactMarkdown from "react-markdown";
import remarkGfm from "remark-gfm";
import SyntaxHighligher from "react-syntax-highlighter";
import { vs2015 } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { chatInputTypeState, currentChatState, useEvent } from "@wrtn/core";
import { useRecoilValue } from "recoil";
import { newColors } from "@wrtn/ui";

const Code = ({ node, inline, className, ...props }) => {
  const alert = useAlert();
  const handleCopy = (e: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
    const text = e.currentTarget.parentElement.nextElementSibling.textContent;
    window.navigator.clipboard.writeText(text);
    alert.removeAll();
    alert.show("복사되었습니다.")
  }

  const language = /language-(\w+)/.exec(className || '');
  if (inline) {
    return <code style={{ whiteSpace: "break-spaces" }} {...props} />
  }
  return (
    <>
      <ChatPreHeader>
        <div>{language?.at(1) || ""}</div>
        <CodeCopy
          onClick={handleCopy}/>
      </ChatPreHeader>
      <div style={{width: '100%', padding: '10px 16px'}}>
        <SyntaxHighligher style={vs2015} language={language?.at(1) || 'javascript'} highlighter='hljs' customStyle={{'whiteSpace': 'pre-wrap'}}>
          {/* <>
            <ChatCode style={{ whiteSpace: "break-spaces" }} {...props} />
          </> */}
          { props.children }
        </SyntaxHighligher>
      </div>
    </>
  )
}

const Wrapper = ({ text, type }: { text: string, type?: string }) => {
  const { collectEvent } = useEvent();
  const chatInputType = useRecoilValue(chatInputTypeState);
  const chat = useRecoilValue(currentChatState);
  return (
    <ReactMarkdown
      remarkPlugins={[remarkGfm]}
      components={{
        // @ts-ignore
        code: ChatMarkdownComponent.code,
        p: ({ ...props}) => (<p style={{display: 'inline', whiteSpace: "pre-wrap", lineHeight: '180%' }} {...props} />),
        ul: ({ ...props}) => (
          <ul style={{ listStylePosition: "outside", listStyleType: 'disc', paddingLeft: "20px", marginBottom: "10px" }} {...props} />),
        ol: ({ ordered, ...props }) => (
          <ol style={{ listStylePosition: "outside", paddingLeft: "20px", marginBottom: "10px" }} {...props} />),
        a: ({ ...props }) => (
          <a
            {...props} 
            style={{ lineHeight: '180%'}}
            target="_blank" 
            onClick={() => {
              collectEvent("track_chat_url", {
                mode_name: type === "PluginOutput" ? "검색" : "일반",
                url: props?.href || "",
                chat_id: chat._id,
              });
          }} />
          ),
          th: ({ ...props }) => (
            <th
              {...props}
              style={{ backgroundColor: 'rgba(55, 132, 246, 0.3)' }}
            />
          ),
          h1: ({ ...props }) => (
            <h1
              {...props}
              style={{
                marginTop: "2rem",
                marginBottom: "4px",
                fontWeight: "600",
                lineHeight: "180%",
                fontSize: "30px",
              }}
            />
          ),
          h2: ({ ...props }) => (
            <h2
              {...props}
              style={{
                marginTop: "1.4rem",
                marginBottom: "1px",
                fontWeight: "600",
                fontSize: "24px",
                lineHeight: "180%",
              }}
            />
          ),
          h3: ({ ...props }) => (
            <h3
              {...props}
              style={{
                marginTop: "1rem",
                marginBottom: "1px",
                fontWeight: "600",
                fontSize: "20px",
                lineHeight: "180%",
              }}
            />
          ),
          h4: ({ ...props }) => (
            <h4 style={{ lineHeight: '180%'}} {...props} />
          ),
          h5: ({ ...props }) => (
            <h5 style={{ lineHeight: '180%'}} {...props} />
          ),
          h6: ({ ...props }) => (
            <h6 style={{ lineHeight: '180%'}} {...props} />
          ),
          blockquote: ({ ...props }) => (
            <blockquote
              {...props}
              style={{
                ...props.style,
                paddingLeft: '16px',
                borderLeft: `2px solid ${newColors.GRAY_700}`,
                color: newColors.GRAY_600
              }}
            />
          )
      }}
    >
      {text}
    </ReactMarkdown>
  )
}

export const ChatMarkdownComponent = {
  Wrapper: React.memo(Wrapper),
  code: Code
};