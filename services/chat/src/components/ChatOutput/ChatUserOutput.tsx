import { useRecoilValue, useSetRecoilState } from "recoil";

import { Avatar } from "@wrtn/ui/components/Avatar";

import { userState, useModalOpen, selectedTemplateState } from "@wrtn/core";

import {
  ChatBoxWrapper,
  ChatIcon,
  ChatText,
  ChatIconWrapper,
  ChatIconButton,
} from "./styles";
import { colors, IconChatTemplate } from "@wrtn/ui";
import styled from "styled-components";
import { ChatMarkdownComponent } from "./MarkdownComponents";

interface ChatUserOutputProps {
  message: {
    role: string;
    _id: string;
    content: string;
    type?: string;
    pluginName?: string;
  };
}

export const ChatUserOutput = ({ message }: ChatUserOutputProps) => {
  const user = useRecoilValue(userState);
  const setTemplate = useSetRecoilState(selectedTemplateState);
  // const templateToolList = useRecoilValue(templateToolListState);
  const isSearchPlugin =
    message.type === "PluginInput" && message.pluginName === "search";

  const modalOpen = useModalOpen({ modalId: "templateAddModal" });
  return (
    <ChatBoxWrapper key={message._id}>
      <ChatIcon>
        <Avatar width="27px">{user?.name[0]}</Avatar>
      </ChatIcon>
      <ChatText>
        <ChatMarkdownComponent.Wrapper
          text={`${isSearchPlugin ? "! " : ""}${message.content}`}
          type={message?.type}
        />
      </ChatText>
      <ChatIconWrapper>
        <ChatIconButton
          onClick={() => {
            setTemplate({
              id: "",
              name: "",
              content: message.content,
            });
            modalOpen.open();
          }}
        >
          <ChatTemplate />
        </ChatIconButton>
      </ChatIconWrapper>
    </ChatBoxWrapper>
  );
};

export const ChatTemplate = styled(IconChatTemplate)`
  width: 22px;
  height: 22px;
  path {
    fill: ${colors.GRAY_55};
    stroke: ${colors.GRAY_55};
  }
  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
      stroke: ${colors.POINT_PURPLE};
    }
  }
`;
