import React from "react";
import { useRecoilValue } from "recoil";

import {
  newMessageType,
  userState,
  useFetchToolList,
  TOOL_TAG_RECORD,
} from "@wrtn/core";

import { ToolProvider } from "../ToolProvider";
import { Avatar } from "@wrtn/ui/components/Avatar";

import {
  ChatDivider,
  ChatBottomWrapper,
  ChatBoxWrapper,
  ChatIcon,
  ChatText,
  ChatLogoIcon,
  ChatTitle,
  ChatTopWrapper,
} from "./styles";
import { FlexWrapper, TOOL_ICON_SELECTOR, Tag } from "@wrtn/ui";
import { ChatMarkdownComponent } from "./MarkdownComponents";

export const ChatToolInput = ({ message }: { message: newMessageType }) => {
  const user = useRecoilValue(userState);
  const { allToolList } = useFetchToolList();

  const tool = React.useMemo(
    () => allToolList.find((v) => v._id === message.toolId),
    [allToolList, message]
  );
  return (
    <ChatBoxWrapper
      column
      key={message?._id}
      style={{ padding: "0px", gap: "0px" }}
    >
      <ChatTopWrapper align="flex-start">
        <ChatIcon>
          <Avatar width="27px">{user?.name[0]}</Avatar>
        </ChatIcon>
        <ChatText>
          <ChatMarkdownComponent.Wrapper text={message.content} type={message.type}/>
        </ChatText>
      </ChatTopWrapper>
      <ChatDivider />
      <ChatBottomWrapper
        row
        align="flex-start"
        justify="center"
        style={{ width: "100%" }}
      >
        <FlexWrapper
          row
          align="center"
          justify="flex-start"
          style={{ width: "100%" }}
        >
          <ChatLogoIcon>{TOOL_ICON_SELECTOR[tool?.icon]}</ChatLogoIcon>
          <Tag color={TOOL_TAG_RECORD[tool.tag].color}>
            {TOOL_TAG_RECORD[tool.tag].name}
          </Tag>
          <ChatTitle>{tool?.name}</ChatTitle>
          <ToolProvider provider={tool.provider} />
        </FlexWrapper>
        <FlexWrapper
          row
          align="center"
          justify="flex-end"
          style={{ width: "100%", gap: "9px" }}
        />
      </ChatBottomWrapper>
    </ChatBoxWrapper>
  );
};
