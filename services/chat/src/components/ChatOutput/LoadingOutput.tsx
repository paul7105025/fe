import { DotPulse } from "@uiball/loaders";

import { colors } from "@wrtn/ui";
import { ChatAIBoxWrapper, ChatIcon, ChatText, ChatLogo, AbsoluteWrapper, StickyWrapper } from "./styles";

interface LoadingOutput {
  withLeftBottom?: () => JSX.Element;
}

export const LoadingOutput = ({
  withLeftBottom,
}: LoadingOutput) => {
  return (
    <ChatAIBoxWrapper key={"loading-output"} style={{ alignItems: "center" }}>
      <ChatIcon>
        <ChatLogo />
      </ChatIcon>
      <ChatText style={{ paddingLeft: 5, minHeight: 8, height: "100%" }}>
        <DotPulse size={20} speed={1.3} color={colors.GRAY_60} />
      </ChatText>
      <AbsoluteWrapper>
        <StickyWrapper>
          {withLeftBottom && withLeftBottom()}
        </StickyWrapper>
      </AbsoluteWrapper>
    </ChatAIBoxWrapper>
  );
};
