import { newMessageType } from "@wrtn/core";
import { ChatAssistantOutput } from "./ChatAssistantOutput";
import { ChatImagesOutput } from "./ChatImagesOutput";
import { StreamLoadingOutput } from "./StreamLoadingOutput";
import { LoadingOutput } from "./LoadingOutput";
import { ChatToolInput } from "./ChatToolInput";
import { ChatToolOutput } from "./ChatToolOutput";
import { ChatUserOutput } from "./ChatUserOutput";
import { ChatFileLoadingInput } from "./ChatFileLoadingInput";
import { ChatFileUserInput } from "./ChatFileUserInput";
import { ChatFileAssistantOutput } from "./ChatFileAssistantOutput";
import { ChatPluginOutput } from "./ChatPluginOutput";

export type withLeftTopProps = {
  idx: number;
  add: () => void;
  subtract: () => void;
  max: number;
};

export interface ChatAssistantOutputProps {
  contents: newMessageType[];
  onCopy: () => void;
  chatId: string;

  withLeftBottom?: () => JSX.Element;
  withLeftTop?: ({ idx, add, subtract, max }: withLeftTopProps) => JSX.Element;
  withBottomCenter?: ({ idx, add, subtract, max }: withLeftTopProps) => JSX.Element;
}

const ChatOutput = {
  Assistant: ChatAssistantOutput,
  User: ChatUserOutput,
  StreamLoading: StreamLoadingOutput,
  Loading: LoadingOutput,
  ToolInput: ChatToolInput,
  ToolOutput: ChatToolOutput,
  ImageOutput: ChatImagesOutput,
  FileLoading: ChatFileLoadingInput,
  FileOutput: ChatFileAssistantOutput,
  FileInput: ChatFileUserInput,
  PluginOutput: ChatPluginOutput,
};

export default ChatOutput;
