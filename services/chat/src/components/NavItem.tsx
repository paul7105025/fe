import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import { chatType } from "@wrtn/core/types";
import { useModalOpen, useRefresh } from "@wrtn/core/hooks";
import { deleteChat_Id, putChat_Id } from "@wrtn/core/services";
import { chatListState, chatLoadingState } from "@wrtn/core/stores";
import { useEvent } from "@wrtn/core"

import { chatIdState } from "src/stores";

import { Icon, colors, FlexWrapper, typo, FlexButton } from "@wrtn/ui";

interface NavItemProps {
  item: chatType;
  move: (id: string) => Promise<void>;
}

export const NavItem = ({ item, move }: NavItemProps) => {
  const [isEdit, setIsEdit] = React.useState(false);
  const [deletedChatId, setDeletedChatId] = React.useState("");

  const inputRef = React.useRef<HTMLInputElement>(null);

  const chatId = useRecoilValue(chatIdState);
  const chatList = useRecoilValue(chatListState);
  const chatLoading = useRecoilValue(chatLoadingState);

  const { fetchChat } = useRefresh();
  const { collectEvent } = useEvent();
  const { open, close, resolved, setResolved } = useModalOpen({
    modalId: "chatDeleteCheckModal",
  });

  React.useEffect(() => {
    if (!resolved) return;

    (async () => {
      const res = await deleteChat_Id({ chatId: deletedChatId });
      if (res?.status === 200) {
        const list = chatList.filter((e) => e._id !== deletedChatId);
        await fetchChat();
        if (list.length > 0 && chatId === deletedChatId) move(list[0]._id);
        else move("");
      }
      close();
      setResolved(false);
    })();
  }, [resolved]);

  const deleteChat = async (_chatId: string) => {
    setDeletedChatId(_chatId);
    open();
  };

  const toggleEditTopic = (topic: string) => {
    setIsEdit(true);
    inputRef.current.value = topic;
    inputRef.current.focus();

    setTimeout(() => {
      inputRef.current.focus();
    }, 10);
  };

  const editChatTopic = async (chatId: string) => {
    const newTopic = inputRef.current.value;

    if (newTopic === item.topic) {
      setIsEdit(false);
      return;
    }

    const res = await putChat_Id({
      chatId: chatId || "",
      body: {
        topic: newTopic,
      },
    });

    if (res?.status === 200) {
      await fetchChat();
    }

    setIsEdit(false);
  };

  const onKeyPressEdit = async (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      inputRef.current.blur();
    }
  };

  return (
    <NavItemWrapper
      onClick={() => {
        if (chatLoading || isEdit) return;
        move(item._id);
      }}
      selected={item._id === chatId}
      disabled={chatLoading}
      key={item._id}
    >
      <NavInput
        isEdit={isEdit}
        ref={inputRef}
        onBlur={() => editChatTopic(item._id)}
        onKeyDown={(e) => onKeyPressEdit(e)}
      />
      {!isEdit && (
        <>
          <NavTopic>{item.topic || "새 채팅"}</NavTopic>
          <EditButton
            onClick={(e) => {
              e.stopPropagation();
              collectEvent('edit_chat_name')
              toggleEditTopic(item.topic || "새 채팅");
            }}
          >
            <Icon icon="edit-alt" size={20} color={colors.GRAY_55} />
          </EditButton>
          <DeleteButton
            onClick={(e) => {
              e.stopPropagation();
              collectEvent('delete_chat')
              deleteChat(item._id);
            }}
          >
            <Icon icon="trash-alt" size={20} color={colors.GRAY_55} />
          </DeleteButton>
        </>
      )}
    </NavItemWrapper>
  );
};

const NavItemWrapper = styled(FlexWrapper)<{
  selected: boolean;
  disabled: boolean;
}>`
  position: relative;
  padding: 14px 16px;
  width: 100%;

  background-color: ${({ selected }) =>
    selected ? colors.WHITE : "transparent"};

  > button {
    display: none;
  }

  &:hover {
    background: white;
    cursor: ${(props) => (props.disabled ? "wait" : "pointer")};
    > button {
      display: block;
    }
  }

  @media (max-width: 767px) {
    > button {
      display: block;
    }

    &:hover {
      background: ${colors.LIGHT_BLUE};
    }

    background-color: ${({ selected }) =>
      selected ? colors.LIGHT_BLUE : "transparent"};
  }
`;

const NavTopic = styled.div`
  position: relative;
  ${typo({ weight: 600, size: "16px", height: "140%", color: colors.gray_90 })};

  max-width: calc(100% - 46px);
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

  &:hover {
    cursor: pointer;
  }
`;

const NavInput = styled.input<{ isEdit: boolean }>`
  ${typo({ weight: 600, size: "16px", height: "140%", color: colors.gray_90 })};
  width: 100%;
  max-width: 100%;
  background-color: transparent;

  display: ${({ isEdit }) => (isEdit ? "block" : "none")};
`;

const LabelButton = styled(FlexButton)`
  position: absolute;
  top: 50%;
  right: 20px;
  transform: translateY(-50%);

  cursor: pointer;
  z-index: 1;

  &:hover {
    i {
      color: ${colors.POINT_PURPLE};
    }
  }
`;

const EditButton = styled(LabelButton)`
  right: 46px;
`;

const DeleteButton = styled(LabelButton)`
  right: 20px;
`;
