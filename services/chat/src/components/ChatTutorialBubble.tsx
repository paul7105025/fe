import styled from "styled-components";

import { colors, Icon, typo, FlexButton, newColors } from "@wrtn/ui";

import { Bubble } from "@wrtn/ui/components/Bubble";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

interface ITutorialBubbleProps {
  position: {
    top: number;
    left: number;
  };
  arrow: {
    direction: "left" | "right" | "top" | "bottom";
    position: number;
  };
  close: () => void;
  title: string;
  guideText: string;
  childRef: React.RefObject<any>;
}

export const ChatTutorialBubble = ({
  position,
  arrow,
  close,
  title,
  guideText,
  childRef,
}: ITutorialBubbleProps) => {
  return (
    <ModalPositionPortal position={position}>
      <div ref={childRef} style={{ zIndex: 55 }}>
        <Bubble
          direction={arrow.direction}
          position={arrow.position}
          style={{
            flexDirection: "column",
            alignItems: "flex-start",
            padding: 18,
            borderRadius: 8,
            minWidth: 237,
          }}
          innerColor={newColors.PURPLE_200}
          borderColor={colors.WHITE_BOLDER_LINE}
        >
          <CloseButton onClick={close}>
            <Icon icon="times" size={20} color={colors.GRAY_70} />
          </CloseButton>
          <GuideTitle>{title}</GuideTitle>
          <GuideText>{guideText}</GuideText>
        </Bubble>
      </div>
    </ModalPositionPortal>
  );
};

const GuideTitle = styled.p`
  ${typo({
    weight: 900,
    size: "16px",
    height: "138%",
    color: colors.POINT_PURPLE,
  })}
  margin-bottom: 6px;
`;

const GuideText = styled.p`
  ${typo({
    weight: 600,
    size: "14px",
    height: "150%",
    color: colors.POINT_PURPLE,
  })}
  white-space: pre-line;
`;

const CloseButton = styled(FlexButton)`
  position: absolute;
  top: 16px;
  right: 14px;
  cursor: pointer;
`;
