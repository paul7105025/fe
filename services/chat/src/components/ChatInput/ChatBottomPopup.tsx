import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import {
  TOOL_TAG_RECORD,
  useEvent,
  useHoverPopup,
  useIsMobile,
  useLoginDialog,
  useModalOpen,
  userState,
} from "@wrtn/core";
import useHoverMenu from "@wrtn/core/hooks/useHoverMenu";

import { useManageChatHoverMenu } from "src/hooks/useManageChatHoverMenu";
import { ToolProvider } from "../ToolProvider";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import {
  FlexButton,
  FlexWrapper,
  TOOL_ICON_SELECTOR,
  Tag,
  boxShadow,
  colors,
  typo,
} from "@wrtn/ui";

import { IconChatTool, IconChatTemplate } from "@wrtn/ui/assets";
import { ChatBottomButton } from "./ChatBottomButton";

interface ChatBottomPopupProps {
  hideTool?: boolean;
  setInput: React.Dispatch<React.SetStateAction<string>>;
}

export const ChatBottomPopup = ({ hideTool, setInput }: ChatBottomPopupProps) => {
  const user = useRecoilValue(userState);

  const [initPosition, setInitPosition] = React.useState(0);

  const isMobile = useIsMobile();
  const { collectEvent } = useEvent();

  const { list, currentMenu, handleChange, onClickToolMenu, onClickTemplate } =
    useManageChatHoverMenu();

  const loginDialog = useLoginDialog();

  const favoriteToolModal = useModalOpen({ modalId: "favoriteModal" });
  const templateListModal = useModalOpen({ modalId: "templateListModal" });

  const toolPopup = useHoverPopup();

  const hoverMenu = useHoverMenu({
    position: {
      top: -13 - 300,
      left: -1 + initPosition,
    },
  });

  const openTool = () => {
    hoverMenu.handleMouseOver();
    handleChange("tool");
    setInitPosition(0);
  };

  const openTemplate = () => {
    hoverMenu.handleMouseOver();
    handleChange("template");
    setInitPosition(34);
  };

  return (
    <Wrapper ref={hoverMenu.parentRef}>
      {!hideTool && <ChatBottomButton type="tool" onClick={openTool} />}
      <ChatBottomButton type="template" onClick={openTemplate} />
      {hoverMenu.open(({ position, childRef }) => {
        return (
          <ModalPositionPortal
            position={isMobile ? { bottom: 0, left: 0 } : position}
          >
            <ResponsiveWrapper>
              <HoverWrapper column ref={childRef}>
                <HoverHeader>
                  {!hideTool && (
                    <HoverMenu
                      justify="center"
                      align="flex-end"
                      isSelected={currentMenu === "tool"}
                      onClick={() => handleChange("tool")}
                    >
                      툴
                    </HoverMenu>
                  )}
                  <HoverMenu
                    justify="center"
                    align="flex-end"
                    isSelected={currentMenu === "template"}
                    onClick={() => handleChange("template")}
                  >
                    자주 쓰는 문구
                  </HoverMenu>
                  <div style={{ flex: 1 }} />
                  <EditButton
                    onClick={() => {
                      if (!user) return loginDialog.handleOpen();
                      currentMenu === "tool"
                        ? favoriteToolModal.open()
                        : templateListModal.open();
                    }}
                  >
                    편집하기
                  </EditButton>
                </HoverHeader>
                <ChatHoverMenuWrapper column>
                  <ScrollWrapper column>
                    {currentMenu === "tool" &&
                      list.map((v) => (
                        <ToolMenu
                          onClick={() => {
                            if (!user) return loginDialog.handleOpen();
                            collectEvent("open_tool_in_chat", {
                              open_method: "plus_button",
                            });
                            onClickToolMenu(
                              v._id,
                              () => hoverMenu.handleClick(),
                              "chat_field"
                            );
                          }}
                          key={v._id}
                        >
                          <MenuIcon>{TOOL_ICON_SELECTOR[v.icon]}</MenuIcon>
                          {v.tag !== "none" && (
                            <Tag color={TOOL_TAG_RECORD[v.tag].color}>
                              {TOOL_TAG_RECORD[v.tag].name}
                            </Tag>
                          )}
                          <ToolTitle>{v.name}</ToolTitle>
                          <div style={{ flex: 1 }} />
                          <ToolProvider provider={v.provider} />
                        </ToolMenu>
                      ))}

                    {currentMenu === "template" &&
                      list.map((v) => (
                        <ToolMenu
                          onClick={() => {
                            if (!user) return loginDialog.handleOpen();
                            onClickTemplate(v.content, setInput, () =>
                              hoverMenu.handleClick()
                            );
                          }}
                          key={v.name}
                        >
                          <TemplateTitle>/{v.name}</TemplateTitle>
                          <TemplateDesc>{v.content}</TemplateDesc>
                        </ToolMenu>
                      ))}
                  </ScrollWrapper>
                </ChatHoverMenuWrapper>
              </HoverWrapper>
            </ResponsiveWrapper>
          </ModalPositionPortal>
        );
      })}
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  gap: 14px;
`;

const BottomButton = styled(FlexButton)`
  cursor: pointer;
  gap: 4px;
`;

const HoverWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  justify-content: flex-start;
  background-color: ${colors.WHITE};

  border-radius: 8px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};

  width: 445px;
  height: 300px;
  overflow: hidden;

  ${boxShadow.basic_card_shadow};
  @media (max-width: 767px) {
    width: 100vw;
  }
`;
const HoverHeader = styled(FlexWrapper)`
  width: 100%;
  border-bottom: 1px solid ${colors.BLUE_GRAY_LINE};
  gap: 23px;
  padding: 18px 29px 0px 36px;
  align-items: flex-start;
`;

const HoverMenu = styled(FlexWrapper)<{ isSelected: boolean }>`
  ${({ isSelected }) =>
    typo({
      weight: isSelected ? 700 : 600,
      size: "14px",
      height: "100%",
      color: isSelected ? colors.POINT_PURPLE : colors.GRAY_90,
    })};
  cursor: pointer;

  padding-bottom: 15px;
  min-width: 30px;
  box-sizing: border-box;
  border-bottom: ${(props) =>
    props.isSelected ? `2px solid ${colors.POINT_PURPLE}` : ""};
`;

const EditButton = styled.div`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.ACTION_BLUE,
  })};
  text-decoration-line: underline;
  cursor: pointer;
`;

const ChatHoverMenuWrapper = styled(FlexWrapper)`
  width: 100%;

  flex: 1;
  overflow: scroll;
`;

const ScrollWrapper = styled(FlexWrapper)`
  width: 100%;
  padding-bottom: 20px;
`;

const ToolMenu = styled(FlexWrapper)`
  width: 100%;
  height: 46px;
  gap: 10px;

  background-color: ${colors.WHITE};

  padding: 0px 30px;

  &:hover {
    background-color: ${colors.BACKGROUND};
  }
`;

const ToolTitle = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.gray_80,
  })}
`;

const MenuIcon = styled(FlexWrapper)`
  max-width: 20px;
  height: 20px;
  svg {
    height: 20px;
    display: block;
    margin: 0 auto;
    text-align: center;
    g {
      transform-origin: center center;
    }
  }
`;

const TemplateTitle = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.gray_90,
  })};
  width: 145px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const TemplateDesc = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.gray_60,
  })};
  flex: 1;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const ResponsiveWrapper = styled.div`
  display: block;
  @media (max-width: 767px) {
    display: flex;
    height: 100vh;
    align-items: flex-end;
  }
`;
