import styled from "styled-components";
import { Icon, colors, FlexButton, typo } from "@wrtn/ui";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import { useHoverPopup, useIsMobile } from "@wrtn/core";

import { IconChatTool, IconChatTemplate } from "@wrtn/ui/assets";
interface ChatBottomButtonProps {
  type: "code" | "image" | "file" | "tool" | "template" | "question";
  tooltip?: string;
  onClick?(): void;
}

export const ChatBottomButton = ({ type, tooltip, onClick }: ChatBottomButtonProps) => {
  const isMobile = useIsMobile();
  const hoverPopup = useHoverPopup();

  return (
    <>
      <BottomButton
        ref={hoverPopup.hoverRef}
        onMouseEnter={hoverPopup.onMouseEnter}
        onMouseLeave={hoverPopup.onMouseLeave}
        onClick={onClick}
        noBackground={type === "question"}
      >
        {type === "code" && (
          <Icon icon="brackets-curly" size={20} color={colors.GRAY_70} />
        )}
        {type === "image" && (
          <Icon icon="image-v" size={20} color={colors.GRAY_70} />
        )}
        {type === "file" && (
          <Icon icon="paperclip" size={20} color={colors.GRAY_70} />
        )}
        {type === "tool" && <IconChatTool />}
        {type === "template" && (
          <>
            <IconChatTemplate />
            <ButtonText>자주 쓰는 문구</ButtonText>
          </>
        )}
        {type === "question" && (
          <Icon
            icon="question-circle"
            size={28}
            color={colors.GRAY_60}
            hoverColor={colors.POINT_PURPLE}
          />
        )}
      </BottomButton>
      {hoverPopup.open(() => {
        return (
          <ModalPositionPortal
            position={{
              top:
                hoverPopup.hoverRef?.current &&
                hoverPopup.hoverRef.current.getBoundingClientRect().top +
                  (isMobile ? -30 : type === "question" ? 36 : 30),
              left:
                hoverPopup.hoverRef?.current &&
                hoverPopup.hoverRef.current.getBoundingClientRect().left +
                  (type === "question" ? -20 : 0),
            }}
          >
            <Popup>
              {type === "code" && "코드를 작성할 수 있어요!"}
              {type === "image" && "이미지 첨부 ) 이 기능은 준비 중이에요!"}
              {type === "tool" && "툴 리스트"}
              {type === "template" && "자주 쓰는 문구"}
              {type === "question" && "안내/주의사항"}
              {tooltip}
            </Popup>
          </ModalPositionPortal>
        );
      })}
    </>
  );
};

const BottomButton = styled(FlexButton)<{ noBackground: boolean }>`
  cursor: pointer;
  padding: 3px;
  border-radius: 5px;
  &:hover {
    background-color: ${({ noBackground }) =>
      noBackground ? "transparent" : colors.GRAY_30};
  }
`;

const Popup = styled.div`
  background-color: ${colors.gray_80};
  padding: 4px 9px;
  ${typo({
    weight: 600,
    size: "12px",
    color: colors.white,
  })};
  /* transform: translateX(-50%); */
  border-radius: 5px;
`;

const ButtonText = styled.span`
  ${typo({
    size: "14px",
    weight: 500,
    color: colors.GRAY_70,
  })};
  @media (max-width: 767px) {
    display: none;
  }
`;
