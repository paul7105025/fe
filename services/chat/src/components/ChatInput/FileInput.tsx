import React from "react";
import styled from "styled-components";
import {
  AutoSizeTextArea,
  FlexButton,
  FlexWrapper,
  Icon,
  colors,
  typo,
} from "@wrtn/ui";
import { chatInputTypeState, currentFileState, useEvent, useHoverPopup, userState } from "@wrtn/core";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import { useChatLoading, useFileManage } from "src/hooks";
import { useRecoilValue, useSetRecoilState } from "recoil";

interface IFileInputProps {
  input: string;
  inputRef: React.Ref<HTMLTextAreaElement>;

  setInput: React.Dispatch<React.SetStateAction<string>>;
  setFocus: React.Dispatch<React.SetStateAction<boolean>>;

  handleClickGenerate: (input: string) => Promise<void>;
  closeFileInput(): void;
}

export const FileInput = ({
  input,
  inputRef,
  setInput,
  setFocus,

  handleClickGenerate,
  closeFileInput,
}: IFileInputProps) => {
  const user = useRecoilValue(userState);
  const hoverPopup = useHoverPopup();
  const { loading } = useChatLoading();
  const currentFile = useRecoilValue(currentFileState);
  const setType = useSetRecoilState(chatInputTypeState);

  const { handleDownloadFile } = useFileManage();
  const { collectEvent } = useEvent();

  React.useEffect(() => {
    setType("default");
  }, [])

  return (
    <Wrapper>
      <FileWrapper>
        <Icon icon="paperclip" size={20} color={colors.GRAY_70} />
        <FileName onClick={(e) => handleDownloadFile()}>
          {currentFile.fileName}
        </FileName>
        <CloseButton
          disabled={loading}
          onClick={closeFileInput}
          onMouseEnter={hoverPopup.onMouseEnter}
          onMouseLeave={hoverPopup.onMouseLeave}
          ref={hoverPopup.hoverRef}
        >
          <Icon icon="multiply" size={20} color={colors.GRAY_80} />
        </CloseButton>
        {hoverPopup.open(() => {
          return (
            <ModalPositionPortal
              position={{
                top:
                  hoverPopup.hoverRef?.current &&
                  hoverPopup.hoverRef?.current.getBoundingClientRect().top + 30,
                left:
                  hoverPopup.hoverRef?.current &&
                  hoverPopup.hoverRef?.current.getBoundingClientRect().left -
                    90,
                width: "260px",
              }}
            >
              <Popup>입력창을 닫으면 파일 모드 및 문서 분석이 중단돼요.</Popup>
            </ModalPositionPortal>
          );
        })}
      </FileWrapper>
      <TextAreaWrapper>
        <div style={{ alignSelf: "baseline" }}>
          <Icon icon="corner-down-right-alt" size={20} color={colors.GRAY_80} />
        </div>
        <TextArea
          ref={inputRef}
          value={input}
          placeholder="문서에 대한 내용을 물어보세요."
          disabled={!user}
          onChange={(e) => {
            if (!user) return;
            setInput(e.target.value);
          }}
          onFocus={() => {
            if (!user) return;
            setFocus(true);
          }}
          onBlur={() => {
            if (!user) return;
            setFocus(false);
          }}
          onKeyPress={(e) => {
            if (!user) return;
            if (loading) return;
            if (e.key === "Enter" && !e.shiftKey) {
              e.preventDefault();
            }

            if (
              !e.metaKey &&
              !e.shiftKey &&
              e.key === "Enter" &&
              input.trim().length > 0
            ) {
              // console.log(e)
              e.preventDefault();
              e.stopPropagation();
              // submit();

              collectEvent("click_generate_btn", {
                feature_menu: "file",
                generate_position: "chat",
                method: "enter",
              });

              handleClickGenerate(input);
              setInput("");
            }
          }}
        />
      </TextAreaWrapper>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  flex-direction: column;
  width: 100%;

  padding: 20px;
  background: transparent;
`;

const FileWrapper = styled(FlexWrapper)`
  justify-content: space-between;
  position: relative;
  width: 100%;
  height: 100%;
  padding-bottom: 20px;

  border-radius: 8px 8px 0px 0px;
  border-bottom: 1px solid ${colors.BLUE_GRAY_LINE};

  @media (max-width: 1023px) {
    width: 100%;
  }
  @media (max-width: 767px) {
    height: auto;
  }
`;

const TextAreaWrapper = styled(FlexWrapper)`
  height: 100%;
  width: 100%;
  padding-top: 15px;
`;

const TextArea = styled(AutoSizeTextArea)`
  background: none;
  width: 100%;
  padding-left: 15px;

  ${typo({ weight: 600, size: "16px", height: "160%", color: colors.gray_90 })};

  resize: none;

  &::placeholder {
    color: ${colors.GRAY_55};
  }
`;

const FileName = styled.div`
  padding-left: 15px;
  font-family: "Pretendard";
  font-weight: 600;

  flex: 1;
  text-align: left;

  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;

const CloseButton = styled(FlexButton)`
  cursor: pointer;
`;
const Popup = styled.div`
  background-color: ${colors.gray_80};
  padding: 4px 9px;
  ${typo({
    weight: 600,
    size: "12px",
    color: colors.white,
  })};
  transform: translateX(-50%);
  border-radius: 5px;
`;
