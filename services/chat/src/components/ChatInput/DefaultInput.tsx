import React from "react";
import styled from "styled-components";

import { animated, useTransition } from "@react-spring/web";

import { AutoSizeTextArea, FlexWrapper, colors, newColors, typo } from "@wrtn/ui";

import { useChatScroll } from "src/hooks";

interface DefaultInputProps {
  input: string;
  isDefault: boolean;
  loading: boolean;
  inputRef: React.Ref<HTMLTextAreaElement>;

  setInput: React.Dispatch<React.SetStateAction<string>>;
  setFocus: React.Dispatch<React.SetStateAction<boolean>>;

  handleClickGenerate: (input: string) => Promise<void>;
}

export const DefaultInput = ({
  input,
  isDefault,
  loading,
  inputRef,
  setInput,
  setFocus,

  handleClickGenerate,
}: DefaultInputProps) => {
  const heightRef = React.useRef<HTMLDivElement>(null);

  const { handleScrollToBottom } = useChatScroll();

  const DefaultInputTransition = useTransition(isDefault, {
    from: { opacity: 0, maxHeight: 0 },
    enter: {
      opacity: 1,
      maxHeight: 1000,
    },
    config: { duration: 400 },
    onDestroyed: () => handleScrollToBottom(),
  });

  return DefaultInputTransition(
    (style, item) =>
      item && (
        <animated.div
          style={{
            width: "100%",
            flex: 1,
            backgroundColor: colors.WHITE,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
            ...style,
          }}
        >
          <TextAreaWrapper column ref={heightRef}>
            <TextArea
              ref={inputRef}
              value={input}
              rows={2}
              maxRows={5}
              minRows={2}
              placeholder="뤼튼에게 무엇이든 요청하세요.  ‘-그려줘’ 라고 입력하면 뤼튼이 이미지를 그려드려요."
              onChange={(e) => {
                setInput(e.target.value);
              }}
              onFocus={() => {
                setFocus(true);
              }}
              onBlur={() => {
                setFocus(false);
              }}
              onKeyPress={(e) => {
                if (loading) return;
                if (e.key === "Enter" && !e.shiftKey) {
                  e.preventDefault();
                }

                if (
                  !e.metaKey &&
                  !e.shiftKey &&
                  e.key === "Enter" &&
                  input.trim().length > 0
                ) {
                  e.preventDefault();
                  e.stopPropagation();
                  // submit();
                  handleClickGenerate(input);
                  setInput("");
                }
              }}
            />
          </TextAreaWrapper>
        </animated.div>
      )
  );
};

const TextAreaWrapper = styled(FlexWrapper)`
  padding: 0px 6px 0px 20px;
  background: transparent;
  border-radius: 8px;
  gap: 8px;
  width: 100%;
  height: 100%;
`;

const TextArea = styled(AutoSizeTextArea)`
  padding-top: 20px;
  background: none;
  width: 100%;
  ${typo({ weight: 600, size: "16px", height: "160%", color: colors.gray_90 })};

  resize: none;

  &::placeholder {
    color: ${newColors.GRAY_300};
  }
`;
