import React from "react";
import styled from "styled-components";

import { animated, useTransition } from "@react-spring/web";

import {
  AutoSizeTextArea,
  FlexWrapper,
  Icon,
  colors,
  newColors,
  typo,
} from "@wrtn/ui";

import { useChatScroll } from "src/hooks";

interface ISearchInputProps {
  input: string;
  loading: boolean;
  inputRef: React.Ref<HTMLTextAreaElement>;

  setInput: React.Dispatch<React.SetStateAction<string>>;
  setFocus: React.Dispatch<React.SetStateAction<boolean>>;

  handleClickGenerate: (input: string) => Promise<void>;
  onChangeMode: (mode: "default" | "search") => void;
}

export const SearchInput = ({
  input,
  loading,
  inputRef,
  setInput,
  setFocus,

  handleClickGenerate,
  onChangeMode,
}: ISearchInputProps) => {
  const heightRef = React.useRef<HTMLDivElement>(null);

  const { handleScrollToBottom } = useChatScroll();

  const DefaultInputTransition = useTransition(true, {
    from: { opacity: 0, maxHeight: 0 },
    enter: {
      opacity: 1,
      maxHeight: 1000,
    },
    config: { duration: 400 },
    onDestroyed: () => handleScrollToBottom(),
  });

  return DefaultInputTransition(
    (style, item) =>
      item && (
        <animated.div
          style={{
            width: "100%",
            flex: 1,
            backgroundColor: newColors.PURPLE_200,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
            ...style,
          }}
        >
          <TextAreaWrapper column ref={heightRef}>
            <div style={{ marginTop: "-3px" }}>
              <Icon icon="exclamation" size={26} color={newColors.GRAY_700} />
            </div>
            <div style={{ width: "100%" }}>
              <TextArea
                ref={inputRef}
                value={input}
                rows={2}
                maxRows={5}
                minRows={2}
                placeholder="검색하고 싶은 내용을 입력하세요. 검색모드는 실시간 정보를 반영해 드려요."
                onChange={(e) => {
                  setInput(e.target.value);
                }}
                onFocus={() => {
                  setFocus(true);
                }}
                onBlur={() => {
                  setFocus(false);
                }}
                onKeyPress={(e) => {
                  if (loading) return;
                  if (e.key === "Enter" && !e.shiftKey) {
                    e.preventDefault();
                  }

                  if (
                    !e.metaKey &&
                    !e.shiftKey &&
                    e.key === "Enter" &&
                    input.trim().length > 0
                  ) {
                    e.preventDefault();
                    e.stopPropagation();

                    handleClickGenerate(input);
                    setInput("");
                  }
                }}
                onKeyDown={(e) => {
                  if (e.key === "Backspace" && input === "") {
                    onChangeMode("default");
                  }
                }}
              />
            </div>
          </TextAreaWrapper>
        </animated.div>
      )
  );
};

const TextAreaWrapper = styled(FlexWrapper)`
  padding: 0px 16px 0px 10px;
  background: transparent;
  border-radius: 8px;
  width: 100%;
  height: 100%;
  flex-direction: row;
  align-items: flex-start;
  padding-top: 20px;
`;

const TextArea = styled(AutoSizeTextArea)`
  background: none;
  width: 100%;
  ${typo({ weight: 600, size: "16px", height: "160%", color: colors.gray_90 })};

  resize: none;

  &::placeholder {
    color: ${newColors.GRAY_300};
  }
`;
