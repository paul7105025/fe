import React from "react";
import styled from "styled-components";

import { useIsMobile } from "@wrtn/core";
import useHoverMenu from "@wrtn/core/hooks/useHoverMenu";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { ChatBottomButton } from "./ChatBottomButton";
import { colors, typo, FlexWrapper, boxShadow } from "@wrtn/ui";

export const ChatGuideButton = () => {
  const isMobile = useIsMobile();

  const hoverGuide = useHoverMenu({
    position: {
      top: isMobile ? -295 - 10 : -330 - 10,
      left: isMobile ? -255 + 20 : -285 + 20,
    },
  });

  return (
    <>
      <GuideButtonWrapper ref={hoverGuide.parentRef}>
        <ChatBottomButton type="question" onClick={hoverGuide.handleClick} />
      </GuideButtonWrapper>
      {hoverGuide.open(({ position, childRef }) => (
        <ModalPositionPortal position={position}>
          <HelpContentWrapper column align="flex-start" ref={childRef}>
            <HelpContentSection column>
              <HelpContentTitle>이렇게 사용해보세요.</HelpContentTitle>
              <HelpContentList>
                <HelpContentSub>
                  구체적으로 쓸수록 잘 알아들어요.
                </HelpContentSub>
                <HelpContentSub>피드백을 주면 더 잘 알아들어요.</HelpContentSub>
              </HelpContentList>
            </HelpContentSection>
            <HelpContentDivider />
            <HelpContentSection column>
              <HelpContentTitle>주의해 주세요!</HelpContentTitle>
              <HelpContentList>
                <HelpContentSub>
                  사실이 아닌 말을 출력할 수도 있어요.
                </HelpContentSub>
                <HelpContentSub>
                  2021년도까지의 정보만 학습했어요.
                </HelpContentSub>
                <HelpContentSub>활용 전 사실 점검을 권장드려요.</HelpContentSub>
              </HelpContentList>
            </HelpContentSection>
          </HelpContentWrapper>
        </ModalPositionPortal>
      ))}
    </>
  );
};

const GuideButtonWrapper = styled.div`
  margin-right: 12px;
`;

const HelpContentWrapper = styled(FlexWrapper)`
  padding: 24px;
  width: 285px;
  height: 330px;
  background-color: ${colors.WHITE};
  border: 1px solid ${colors.WHITE_BOLDER_LINE};
  border-radius: 10px;
  ${boxShadow.tooltip_message_shadow};

  @media (max-width: 767px) {
    width: 260px;
    height: 295px;
  }
`;

const HelpContentSection = styled(FlexWrapper)`
  align-items: flex-start;
`;

const HelpContentDivider = styled.div`
  border: 0.5px solid ${colors.WHITE_BOLDER_LINE};
  width: 100%;
  height: 0px;
  margin-top: 14px;
  margin-bottom: 24px;
`;

const HelpContentTitle = styled.div`
  ${typo({
    weight: 700,
    size: "16px",
    height: "160%",
    color: colors.ACTION_BLUE,
  })};
  margin-bottom: 10px;
`;

const HelpContentList = styled.ul`
  list-style-position: inside;
  padding-left: 6px;
`;

const HelpContentSub = styled.li`
  ${typo({
    weight: 600,
    size: "14px",
    height: "160%",
    color: colors.gray_70,
  })};
  margin-bottom: 10px;
`;
