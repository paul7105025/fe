import React from "react";
import styled, { css } from "styled-components";
import { animated, useTransition } from "@react-spring/web";

import ChatToolHeader from "src/containers/ChatToolHeader";
import ChatFormContainer from "src/containers/ChatFormContainer";

import { FlexWrapper, colors } from "@wrtn/ui";

import { useEvent } from "@wrtn/core/hooks";
import { FormMetadataType, ToolType } from "@wrtn/core/types";
import { useChatScroll } from "src/hooks";

interface ToolInputProps {
  isTool: boolean;
  tool: ToolType;
  formData: {
    forms: FormMetadataType[];
    isDisabled: boolean;
    updateForms: (id: any, newForm: any) => void;
    exampleForms: () => void;
    resetForms: () => void;
    count: number;
    incrementCount: () => void;
    decrementCount: () => void;
  };
  closeTool: () => void;
}

export const ToolInput = ({
  isTool,
  tool,
  formData,
  closeTool,
}: ToolInputProps) => {
  const [visibleForm, setVisibleForm] = React.useState<boolean>(true);

  const { collectEvent } = useEvent();

  const { handleScrollToBottom } = useChatScroll();

  const toggleVisibleForm = () =>
    setVisibleForm((v) => {
      if (!v) {
        collectEvent("minimize_tool_in_chat");
      } else {
        collectEvent("maximize_tool_in_chat");
      }
      return !v;
    });

  const ToolInputTransition = useTransition(isTool, {
    from: {
      opacity: 0,
      maxHeight: 70,
    },
    enter: {
      opacity: 1,
      maxHeight: 500,
    },
    leave: { opacity: 0, maxHeight: 70 },
    // update: ({ height }) => ({ height }),
    config: { duration: 400 },
    onDestroyed: () => handleScrollToBottom(),
  });

  return ToolInputTransition(
    (style, item) =>
      item && (
        <animated.div
          style={{
            width: "100%",
            backgroundColor: colors.WHITE,
            minHeight: 70,
            borderRadius: "8px 8px 0px 0px",
            ...style,
          }}
        >
          <ChatToolHeader
            tool={tool}
            onClose={closeTool}
            onClickVisible={toggleVisibleForm}
            isVisible={visibleForm}
          />

          <FormContainerWrapper align="flex-start" visibleForm={visibleForm}>
            <ChatFormContainer
              forms={formData.forms}
              updateForms={formData.updateForms}
              exampleForms={formData.exampleForms}
              resetForms={formData.resetForms}
            />
          </FormContainerWrapper>
        </animated.div>
      )
  );
};

const FormContainerWrapper = styled(FlexWrapper)<{ visibleForm: boolean }>`
  max-height: 360px;

  width: 100%;
  height: 100%;
  overflow: scroll;
  padding: 0px 30px 10px;
  /* background-color: ${colors.WHITE}; */
  background: transparent;

  ${(props) =>
    !props.visibleForm &&
    css`
      height: 0px;
      max-height: 0px;
      min-height: 0px;
      visibility: hidden;
      padding: 0px;
    `};
`;
