import { FlexWrapper } from "@wrtn/ui";

import { ReactComponent as Clova } from "../assets/logos/logo_clova.svg";
import { ReactComponent as Openai } from "../assets/logos/logo_openai.svg";

export const ToolProvider = ({ provider }: { provider: string }) => {
  return (
    <FlexWrapper>
      {/* clova */}
      {provider === "HyperClova" && <Clova />}
      {/* openai */}
      {provider === "GPT3" && <Openai />}
    </FlexWrapper>
  );
};
