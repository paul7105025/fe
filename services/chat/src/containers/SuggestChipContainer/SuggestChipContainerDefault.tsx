import React from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";

import { useChatInit, useChatScroll, useChatSuggest } from "src/hooks";

import { chatIdState, chatTrailState } from "src/stores";

import { chatInputTypeState, streamAbortState } from "@wrtn/core";

import { ReactComponent as ChatIcon } from "src/assets/icons/icon_chat.svg";
import { ReactComponent as SearchIcon } from "src/assets/icons/icon_search.svg";

import {
  ChatCommandWrapper,
  ChatCommandTitle,
  ChatCommandButtons,
  ChatCommandButton,
} from "./styles";

import {
  suggestType,
  exampleListDefault,
  exampleListForSearch,
} from "src/constants/suggest";
import { newColors } from "@wrtn/ui";

export const SuggestChipContainerDefault = () => {
  const chatId = useRecoilValue(chatIdState);
  const chatInputType = useRecoilValue(chatInputTypeState);

  const setChatTrail = useSetRecoilState(chatTrailState);
  const setStreamAbort = useSetRecoilState(streamAbortState);

  const [defaultChipList, setDefaultChipList] =
    React.useState<Array<suggestType>>(exampleListDefault);

  const { initChat } = useChatInit();
  const { handleScrollToBottom } = useChatScroll();
  const { handleClickChip, handleClickSearchChip } = useChatSuggest();

  const handleClickSearch = async (chip: suggestType) => {
    if (!chatId) {
      const chatId = await initChat();
      await handleClickSearchChip(chip, chatId);
    } else {
      await handleClickSearchChip(chip);
    }
  };

  const handleClick = async (chip: suggestType) => {
    if (!chatId) {
      const chatId = await initChat();

      setChatTrail(false);
      const _controller = await handleClickChip(
        chip,
        () => {
          handleScrollToBottom();
        },
        chatId
      );

      if (_controller)
        setStreamAbort({
          startTime: new Date(),
          controller: _controller,
        });
    } else {
      const _controller = await handleClickChip(
        chip,
        () => {
          handleScrollToBottom();
        },
        chatId
      );
      if (_controller)
        setStreamAbort({
          startTime: new Date(),
          controller: _controller,
        });
    }
  };

  React.useEffect(() => {
    // console.log(chatInputType);
    if (chatInputType === "search") {
      setDefaultChipList(exampleListForSearch);
    } else {
      setDefaultChipList(exampleListDefault);
    }
  }, [chatInputType]);

  return (
    <ChatCommandWrapper
      column
      style={{
        visibility: ["default", "search"].includes(chatInputType)
          ? "visible"
          : "hidden",
        maxHeight: ["default", "search"].includes(chatInputType)
          ? "100vh"
          : "0px",
      }}
    >
      <ChatCommandTitle>
        {chatInputType === "default" ? <ChatIcon fill={newColors.PURPLE_500_PRIMARY}/> : <SearchIcon />}
        {chatInputType === "default"
          ? "이런 걸 물어볼 수 있어요"
          : "검색 모드에서는 이런 걸 물어볼 수 있어요"}
      </ChatCommandTitle>
      <ChatCommandButtons column>
        {defaultChipList.map((chip) => (
          <ChatCommandButton
            onClick={() =>
              chatInputType === "search"
                ? handleClickSearch(chip)
                : handleClick(chip)
            }
            key={chip.id}
          >
            {chip.name}
          </ChatCommandButton>
        ))}
      </ChatCommandButtons>
    </ChatCommandWrapper>
  );
};
