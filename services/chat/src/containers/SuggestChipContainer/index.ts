import { SuggestChipContainerChat } from "./SuggestChipContainerChat";
import { SuggestChipContainerDefault } from "./SuggestChipContainerDefault";

export interface ISuggestChipContainerProps {}

const SuggestChipContainer = {
  Default: SuggestChipContainerDefault,
  Chat: SuggestChipContainerChat,
};

export default SuggestChipContainer;
