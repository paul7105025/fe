import React from "react";

import { useRecoilValue, useSetRecoilState } from "recoil";

import { newColors } from "@wrtn/ui";

import {
  useChatImageTutorial,
  useChatInit,
  useChatScroll,
  useChatSuggest,
} from "src/hooks";

import { chatIdState, chatTrailState } from "src/stores";

import {
  chatInputTypeState,
  chatType,
  streamAbortState,
  useIsMobile,
  useLoginDialog,
  userState,
} from "@wrtn/core";
import useHoverMenu from "@wrtn/core/hooks/useHoverMenu";

import { ReactComponent as ChatIcon } from "src/assets/icons/icon_chat.svg";

import {
  ChatCommandWrapper,
  ChatCommandTitle,
  ChatCommandButtons,
  ChatCommandButtonForInitial,
} from "./styles";
import { suggestType } from "src/constants/suggest";

export const SuggestChipContainerChat = () => {
  const user = useRecoilValue(userState);
  const chatId = useRecoilValue(chatIdState);
  const chatInputType = useRecoilValue(chatInputTypeState);

  const setChatTrail = useSetRecoilState(chatTrailState);
  const setStreamAbort = useSetRecoilState(streamAbortState);

  const { handleOpen } = useLoginDialog();

  const { chatChipList, handleClickChip, handleClickSearchChip } =
    useChatSuggest();
  const { isImageTutorialOpen } = useChatImageTutorial();

  const isMobile = useIsMobile();
  const hoverImageTutorial = useHoverMenu({
    position: {
      top: isMobile ? -80 : -5,
      // left: isMobile ? 380 : -257,
      right: isMobile ? -250 : -470,
    },
  });
  const { initChat } = useChatInit();
  const { handleScrollToBottom } = useChatScroll();

  const handleClickSearch = async (chip: suggestType) => {
    if (!user) {
      handleOpen();
      return;
    }

    if (!chatId) {
      const chatId = await initChat();
      await handleClickSearchChip(chip, chatId);
    } else {
      await handleClickSearchChip(chip);
    }
  };

  const handleClick = async (chip: suggestType) => {
    if (!user) {
      handleOpen();
      return;
    }

    if (!chatId) {
      initChat(async (data: chatType) => {
        setChatTrail(false);
        const _controller = await handleClickChip(
          chip,
          () => {
            handleScrollToBottom();
          },
          data._id
        );

        if (_controller)
          setStreamAbort({
            startTime: new Date(),
            controller: _controller,
          });
      });
    } else {
      const _controller = await handleClickChip(
        chip,
        () => {
          handleScrollToBottom();
        },
        chatId
      );
      if (_controller)
        setStreamAbort({
          startTime: new Date(),
          controller: _controller,
        });
    }
  };

  React.useEffect(() => {
    if (isImageTutorialOpen) {
      hoverImageTutorial.handleMouseOver();
    } else {
      hoverImageTutorial.handleMouseOut();
    }
  }, [isImageTutorialOpen]);

  return (
    <>
      <ChatCommandWrapper
        column
        align="flex-end"
        ref={hoverImageTutorial.parentRef}
        style={{
          visibility: ["default", "search"].includes(chatInputType)
            ? "visible"
            : "hidden",
          maxHeight: ["default", "search"].includes(chatInputType)
            ? "100vh"
            : "0px",
        }}
      >
        <ChatCommandTitle>
          <ChatIcon fill={newColors.PURPLE_500_PRIMARY} />
          이런 걸 물어볼 수 있어요
        </ChatCommandTitle>
        <ChatCommandButtons column align="flex-end">
          {chatChipList.map((chip) => (
            <ChatCommandButtonForInitial
              style={{
                borderBottomRightRadius: 0,
              }}
              onClick={() =>
                chatInputType === "search"
                  ? handleClickSearch(chip)
                  : handleClick(chip)
              }
              key={chip.id}
            >
              {chip.name}
            </ChatCommandButtonForInitial>
          ))}
        </ChatCommandButtons>
      </ChatCommandWrapper>
    </>
  );
};
