import styled from "styled-components";

import {
  FlexWrapper,
  newTypo,
  newColors,
  FlexButton,
  ChipStyleSelector,
} from "@wrtn/ui";

export const ChatCommandWrapper = styled(FlexWrapper)`
  align-content: center;
  gap: 9px;
`;

export const ChatCommandTitle = styled(FlexWrapper)`
  ${newTypo("body_compact-14-semi")}
  color: ${newColors.PURPLE_500_PRIMARY};
  gap: 9px;
  margin-bottom: 19px;
`;

export const ChatCommandButtons = styled(FlexWrapper)`
  flex-wrap: wrap;
  gap: 12px;
  justify-content: center;
`;

export const ChatCommandButton = styled(FlexButton)<{ selected?: boolean }>`
  ${ChipStyleSelector["chat"]}

  cursor: pointer;

  &:hover {
    background: ${newColors.PURPLE_200};
  }
  &:disabled {
    cursor: default;
    z-index: -1;
  }

  @media (max-width: 767px) {
    line-height: 160%;
  }
`;

export const ChatCommandButtonForInitial = styled(FlexButton)<{
  selected?: boolean;
}>`
  ${ChipStyleSelector["chat"]}

  cursor: pointer;

  &:hover {
    background: ${newColors.PURPLE_200};
  }
  &:disabled {
    cursor: default;
    z-index: -1;
  }

  text-align: end;

  @media (max-width: 767px) {
    line-height: 160%;
  }
`;
