import styled from "styled-components";
import { Icon, FlexWrapper, newColors, typo, colors } from "@wrtn/ui";
import { ReactComponent as ChatIcon } from "src/assets/icons/icon_chat.svg";
import { useRecoilValue } from "recoil";
import { useChatLoading, useShare } from "src/hooks";
import { currentChatState, useHoverPopup, useIsMobile } from "@wrtn/core";
import { ModalPortal, ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import { Dialog } from "@wrtn/ui/components/Dialog";
import React, { useState } from "react";

interface IModeContainerProps {
  mode: "default" | "search" | "tool";
  isFileMode: boolean;
  hasEnoughForShare: boolean;
  onChangeMode(mode: "default" | "search"): void;
}

export const ModeContainer = ({ mode, isFileMode, hasEnoughForShare, onChangeMode }: IModeContainerProps) => {
  const chat = useRecoilValue(currentChatState);
  const [isShared, setIsShared] = useState(!!chat?.shared)
  const { loading } = useChatLoading()

  React.useEffect(() => {
    if (chat) {
      setIsShared(!!chat.shared);
    }
  }, [chat])
  
  const hoverPopup = useHoverPopup();
  const isMobile = useIsMobile();
  const { showModal, setShowModal, handleShare } = useShare();

  const onClickShare = () => {
    if (loading) return;
    setShowModal(false);
    setIsShared(true);
    handleShare();
  };

  if (mode === "tool") return null

  return (
    <ModeWrapper>
      {["default", "search"].includes(mode) && !isFileMode && (
        <>
          <Mode style={{ gap: "8px" }} onClick={() => onChangeMode("default")}>
            <ChatIcon fill={mode === "default" ? newColors.BLUE_500_NOTICE : newColors.GRAY_500} />
            <DefaultText isActive={mode === "default"}>일반 모드</DefaultText>
          </Mode>
          <Mode onClick={() => onChangeMode("search")}>
            <Icon
              icon="exclamation"
              size={22}
              color={mode === "search" ? newColors.PURPLE_500_PRIMARY : newColors.GRAY_500}
            />
            <SearchText isActive={mode === "search"}>검색 모드</SearchText>
          </Mode>
        </>
      )}
      {isMobile && hasEnoughForShare && (
        <ShareWrapper 
          onClick={isShared ? undefined : () => setShowModal(true)}
          ref={hoverPopup.hoverRef}
          onMouseEnter={hoverPopup.onMouseEnter}
          onMouseLeave={hoverPopup.onMouseLeave}  
        >
          <span>{isShared ? "공유된 채팅방" : "공유하기"}</span>
          <Icon icon="upload" size={20} color={isShared ? newColors.GRAY_300 : newColors.GRAY_500} />
          {isShared && hoverPopup.open(() => {
            return (
              <ModalPositionPortal
                position={{
                  top:
                    hoverPopup.hoverRef?.current &&
                    hoverPopup.hoverRef?.current.getBoundingClientRect().top + 25,
                  left:
                    hoverPopup.hoverRef?.current &&
                    hoverPopup.hoverRef?.current.getBoundingClientRect().left + 110,
                  width: "204px"
                }}
              >
                <Popup>다시 공유하려면 기존 게시물을 삭제해주세요.</Popup>
              </ModalPositionPortal>
            );
          })}
        </ShareWrapper>
      )}
      {showModal && (
        <ModalPortal>
          <Dialog
            title="공유하시겠습니까?"
            description={[
              "채팅방이 공유되면 대화 내용이 타 유저에게",
              "공개됩니다. 공개를 원하지 않으시면",
              "취소 버튼을 눌러주세요."
            ]}
            rightButtonLabel="공유하기"
            iconType="link"
            handleClose={() => setShowModal(false)}
            handleRightButton={onClickShare}
          />
        </ModalPortal>
      )}
    </ModeWrapper>
  )
}

const ModeWrapper = styled(FlexWrapper)`
  max-width: 1217px;
  width: 100%;
  align-self: center;
  align-items: center;
  padding-left: 6px;
  margin-bottom: 10px;
  gap: 12px;

  @media (max-width: 767px) {
    padding-left: 20px;
    margin-bottom: 5px;
  }
`

const Mode = styled(FlexWrapper)`
  align-items: center;
  cursor: pointer;
`
const DefaultText = styled.span<{ isActive: boolean }>`
  ${props => props.isActive ? `
    ${typo({
      weight: 600,
      size: "14px",
      color: newColors.BLUE_500_NOTICE,
    })}`
  : `
    ${typo({
      weight: 500,
      size: "14px",
      color: newColors.GRAY_500,
    })}
  `};
`

const SearchText = styled.span<{ isActive: boolean }>`
  ${props => props.isActive ? `
    ${typo({
      weight: 600,
      size: "14px",
      color: newColors.PURPLE_500_PRIMARY,
    })}`
  : `
    ${typo({
      weight: 500,
      size: "14px",
      color: newColors.GRAY_500,
    })}
  `};
`

const ShareWrapper = styled.div`
  gap: 5px;
  display: flex;
  align-items: center;
  flex: 1;
  flex-direction: row-reverse;
  padding-right: 17px;

  ${typo({
  size: "14px",
  weight: 600,
  color: newColors.GRAY_500,
})}
`

const Popup = styled.div`
  background-color: ${colors.gray_80};
  padding: 4px 9px;
  ${typo({
    weight: 600,
    size: "12px",
    color: colors.white,
  })};
  transform: translateX(-50%);
  border-radius: 5px;
`;
