import React from "react";
import styled from "styled-components";

import FormSelector from "@wrtn/ui/components/FormSelector";

import { FormMetadataType } from "@wrtn/core";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";

interface ChatFormContainerProps {
  forms: Array<FormMetadataType>;
  exampleForms: () => void;
  resetForms: () => void;
  updateForms: (id: string, targetForm: FormMetadataType) => void;
}

const ChatFormContainer = ({
  forms,
  exampleForms,
  resetForms,
  updateForms,
}: ChatFormContainerProps) => {
  return (
    <FormScrollWrapper column>
      <ExampleButton onClick={exampleForms}>예시 텍스트</ExampleButton>
      <ResetButton onClick={resetForms}>전체 지우기</ResetButton>
      {forms.map((form) => {
        return (
          <FormSelector key={form.id} form={form} updateForm={updateForms} />
        );
      })}
    </FormScrollWrapper>
  );
};

export default ChatFormContainer;

//style

const FormScrollWrapper = styled(FlexWrapper)`
  flex: 1;

  gap: 30px;

  overflow: scroll;

  width: 100%;

  padding-top: 28px;
  padding-bottom: 28px;

  @media (max-width: 1023px) {
    overflow: inherit;
  }
`;

const FormControlButton = styled(FlexButton)`
  z-index: 3;
  position: absolute;
  top: 20px;
  padding: 5px 9px;
  user-select: none;

  ${typo({
    weight: "600",
    size: "14px",
    height: "14px",
    color: colors.ACTION_BLUE,
  })}

  cursor: pointer;
  background-color: ${colors.BACKGROUND};
  border-radius: 5px;

  &:hover {
    opacity: 0.7;
  }
`;

const ResetButton = styled(FormControlButton)`
  right: 0px;
`;

const ExampleButton = styled(FormControlButton)`
  right: 90px;
`;
