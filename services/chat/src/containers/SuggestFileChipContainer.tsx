import styled from "styled-components";
import {
  FlexWrapper,
  newTypo,
  newColors,
  FlexButton,
  ChipStyleSelector,
} from "@wrtn/ui";
import { ReactComponent as ChatIcon } from "src/assets/icons/icon_chat.svg";
import { useFileChatGenerate } from "src/hooks/file/useFileChatGenerate";

export const SuggestFileChipContainer = ({
  disabled,
  chips,
}: {
  disabled: boolean;
  chips: string[];
}) => {
  const { generateFileChatByChip } = useFileChatGenerate({});

  const onGenerateFileChatByChip = async (index: number) => {
    await generateFileChatByChip(index);
  };

  return (
    <ChatCommandWrapper column>
      <ChatCommandTitle>
        <ChatIcon />
        <ChatIcon fill={newColors.PURPLE_500_PRIMARY} />
        이런 걸 물어볼 수 있어요
      </ChatCommandTitle>
      <ChatCommandButtons column>
        {chips.map((chip, idx) => {
          return (
            <ChatCommandButton
              key={chip}
              onClick={() => {
                if (disabled) return;

                onGenerateFileChatByChip(idx);
              }}
            >{chip}</ChatCommandButton>
          )
        })}
      </ChatCommandButtons>
    </ChatCommandWrapper>
  );
};

const ChatCommandWrapper = styled(FlexWrapper)`
  align-content: center;
  gap: 9px;
  max-height: 100vh;
  align-items: flex-end;
  margin-bottom: 50px;
`;

const ChatCommandTitle = styled(FlexWrapper)`
  ${newTypo("body_compact-14-semi")}
  color: ${newColors.PURPLE_500_PRIMARY};
  gap: 9px;
  margin-bottom: 12px;
  padding-right: 10px;
`;

const ChatCommandButtons = styled(FlexWrapper)`
  flex-wrap: wrap;
  gap: 12px;
  justify-content: center;
  align-items: flex-end;
`;

const ChatCommandButton = styled(FlexButton)<{ selected?: boolean }>`
  ${ChipStyleSelector["chat"]}

  cursor: pointer;

  &:hover {
    background: ${newColors.PURPLE_200};
  }
  &:disabled {
    cursor: default;
    z-index: -1;
  }
`;
