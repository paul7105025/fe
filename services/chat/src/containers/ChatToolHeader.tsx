import styled from "styled-components";

import { useEvent, useFavoriteIdList } from "@wrtn/core";

import {
  colors,
  FilledStar,
  FlexButton,
  FlexWrapper,
  Icon,
  Star,
  Tag,
  TOOL_ICON_SELECTOR,
  TOOL_TAG_SELECTOR,
  typo,
} from "@wrtn/ui";

import { GuideButtonWrapper } from "@wrtn/ui/components/GuideButton/styles";

import { ToolProvider } from "src/components/ToolProvider";

const ChatToolHeader = ({ tool, onClose, onClickVisible, isVisible }) => {
  const { collectEvent } = useEvent();

  const { handleClickFavorite, isFavorite } = useFavoriteIdList();

  const currentFavorite = isFavorite(tool?._id);

  const handleFavorite = () => {
    handleClickFavorite(tool?._id);

    collectEvent("click_tool_bookmark", {
      onoff: !currentFavorite,
      feature_menu: "tool",
      feature_category: tool?.category,
      feature_name: tool?.name,
      feature_tag: tool?.tag,
      feature_form: tool?.kind,
    });
  };

  return (
    <Wrapper column isVisible={isVisible}>
      {isVisible && (
        <BlackAlertText>
          <span style={{ color: colors.STAR_YELLOW, marginRight: 3 }}>⚠️</span>{" "}
          <b>툴</b>을 사용할 때는{" "}
          <b style={{ marginLeft: 3 }}>대화체가 아닌 키워드 중심</b>으로
          작성해주세요! (예시 텍스트 참고)
        </BlackAlertText>
      )}
      <MobileWrapper>
        <FlexWrapper
          justify="start"
          onClick={() => onClickVisible(!isVisible)}
          style={{ gap: 5, marginRight: 25 }}
        >
          <p>{isVisible ? "작게보기" : "크게보기"}</p>
          <CarouselDown open={isVisible}>
            <Icon icon="angle-down" color={colors.GRAY_80} size={24} />
          </CarouselDown>
        </FlexWrapper>
        <AbsoluteButton onClick={() => onClose()}>
          <Icon icon="multiply" size={20} />
        </AbsoluteButton>
      </MobileWrapper>
      <RowWrapper>
        <Left>
          <FlexWrapper style={{ gap: 8 }}>
            <LogoIcon>{TOOL_ICON_SELECTOR[tool?.icon]}</LogoIcon>
            {tool?.tag !== "none" && (
              <HeaderTag color={TOOL_TAG_SELECTOR[tool?.tag]?.color}>
                {TOOL_TAG_SELECTOR[tool?.tag]?.name}
              </HeaderTag>
            )}
            <Title>{tool?.name}</Title>
          </FlexWrapper>
          <ToolProvider provider={tool?.provider} />
        </Left>
        <Right>
          {/* <ToolGuideButton tutorialLink={tool?.tutorial?.link} /> */}
          <StarIcon isFavorite={currentFavorite} onClick={handleFavorite}>
            {/* <Icon icon='taxi' /> */}
            {currentFavorite ? <FilledStar /> : <Star />}
          </StarIcon>
          <FlexWrapper
            onClick={() => onClickVisible(!isVisible)}
            style={{ gap: 5, marginRight: 25 }}
          >
            <DesktopWrapper>
              <p>{isVisible ? "작게보기" : "크게보기"}</p>
              <CarouselDown open={isVisible}>
                <Icon icon="angle-down" color={colors.GRAY_80} size={24} />
              </CarouselDown>
            </DesktopWrapper>
          </FlexWrapper>
          <DesktopWrapper onClick={() => onClose()}>
            <Icon icon="multiply" size={20} />
          </DesktopWrapper>
        </Right>
      </RowWrapper>
    </Wrapper>
  );
};

export default ChatToolHeader;

const Wrapper = styled(FlexWrapper)<{ isVisible: boolean }>`
  position: relative;
  width: 100%;
  height: 70px;
  gap: 28px;

  padding: 24px 30px;
  background: transparent;

  border-radius: 8px 8px 0px 0px;
  border-bottom: ${({ isVisible }) =>
    isVisible ? `1.5px solid ${colors.BLUE_GRAY_LINE}` : "none"};

  @media (max-width: 1023px) {
    width: 100%;
  }
  @media (max-width: 767px) {
    height: auto;
    padding: 11px 22px;
    gap: 20px;
  }
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: space-between;

  @media (max-width: 767px) {
    justify-content: flex-start;
    align-items: flex-start;
    gap: 11px;
  }
`;

const Title = styled.p`
  ${typo({
    weight: "600",
    size: "20px",
    height: "100%",
    color: colors.gray_80,
  })}

  display: flex;
  align-items: center;
  white-space: nowrap;
`;

const StarIcon = styled(GuideButtonWrapper)<{
  isFavorite: boolean;
}>`
  svg {
    width: 24px;
    height: 24px;
    path {
      fill: ${({ isFavorite }) =>
        isFavorite ? colors.STAR_YELLOW : colors.GRAY_70};
    }
  }
  &:hover {
    svg {
      path {
        fill: ${({ isFavorite }) =>
          isFavorite ? colors.STAR_YELLOW : colors.POINT_PURPLE};
      }
    }
  }

  cursor: pointer;
`;

const Left = styled(FlexWrapper)`
  gap: 8px;

  @media (max-width: 767px) {
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
  }
`;

const HeaderTag = styled(Tag)`
  margin-left: 3px;
  margin-right: 0;
  @media (max-width: 767px) {
    display: none;
  }
`;

const Right = styled(FlexWrapper)`
  gap: 6px;
`;

const LogoIcon = styled(FlexWrapper)`
  max-width: 36px;
  height: 26px;
  svg {
    height: 26px;
    display: block;
    margin: 0 auto;
    text-align: center;
    g {
      transform-origin: center center;
    }
  }
`;

const CarouselDown = styled.div<{ open: boolean }>`
  width: 22px;
  height: 22px;

  transform: ${(props) => (props.open ? "rotate(0deg)" : "rotate(180deg)")}
    translateY(-2px);
  transition: transform 0.3s ease-in-out;
`;

const MobileWrapper = styled.div`
  display: none;
  @media (max-width: 767px) {
    display: block;
    width: 100%;
  }
`;

const DesktopWrapper = styled.div`
  display: flex;
  ${typo({
    weight: 600,
    size: "14px",
    height: "186%",
    color: colors.GRAY_80,
  })}
  transform: translateY(2px);
  @media (max-width: 767px) {
    display: none;
  }
`;

const AbsoluteButton = styled(FlexButton)`
  position: absolute;
  top: 27px;
  right: 26px;
`;

const BlackAlertText = styled(FlexWrapper)`
  padding: 5px 10px;
  ${typo({
    size: "16px",
    weight: 500,
    color: colors.GRAY_10,
  })}

  background-color: ${colors.GRAY_80};
  border-radius: 6px;

  position: absolute;
  top: -40px;
  left: 0;

  @media (max-width: 767px) {
    display: none;
  }
`;
