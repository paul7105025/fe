import React from "react";
import styled from "styled-components";
import { useAlert } from "react-alert";
import { useRecoilState, useRecoilValue } from "recoil";
import {
  useTransition,
  animated,
  useChain,
  useSpringRef,
} from "@react-spring/web";

import ChatOutput from "src/components/ChatOutput";
import SuggestChipContainer from "./SuggestChipContainer";
import { SuggestFileChipContainer } from "./SuggestFileChipContainer";
import { ModalPortal, ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import { Dialog } from "@wrtn/ui/components/Dialog";

import { ChatToolPagination } from "src/components/ChatToolPagination";

import { useChatLoading, useChatScroll, useHandleAllMessage } from "src/hooks";

import {
  chipSuggestOpenState,
  currentChatState,
  fileSuggestChipsState,
  isMessageArray,
  messageType,
  newMessageType,
  useIntersectAll,
  useIsMobile,
} from "@wrtn/core";

import { newColors, typo } from "@wrtn/ui";

import { chatIdState, chatTrailState, chipTutorialState } from "src/stores";
import { ChatShareButton } from "src/components/ChatShareButton";

enum MESSAGE_TYPE {
  USER_CHIP = "userChip",
  USER_COMMAND_CHIP_OUTPUT = "userCommandChipOutput",

  USER_DYNAMIC_CHIP_INPUT = "userDynamicChipInput",
  ASSISTANT_DYNAMIC_CHIP_OUTPUT = "assistantDynamicChipOutput",

  USER_TOOL_CHIP_INPUT = "userToolChipInput",
  ASSISTANT_TOOL_CHIP_OUTPUT = "assistantToolChipOutput",

  USER_COMMAND_CHIP_INPUT = "userCommandChipInput",
  ASSISTANT_COMMAND_CHIP_OUTPUT = "assistantCommandChipOutput",

  USER_TOOL_INPUT = "userToolInput",
  ASSISTANT_TOOL_OUTPUT = "assistantToolOutput",

  USER_IMAGE_INPUT = "userImageInput",
  ASSISTANT_IMAGE_OUTPUT = "assistantImageOutput",

  USER_MESSAGE = "userMessage",
  ASSISTANT_MESSAGE = "assistantMessage",

  USER_FILE_INPUT = "userFileInput",
  ASSISTANT_FILE_OUTPUT = "assistantFileOutput",

  USER_PLUGIN_INPUT = "userPluginInput",
  ASSISTANT_PLUGIN_OUTPUT = "assistantPluginOutput",
}

const tutorialMessage: messageType[] = [
  {
    role: "assistant",
    type: "CommandChipOutput",
    content:
      "안녕하세요! 모두를 위한 AI 포털 뤼튼이에요. 저에게 무엇이든 요청해주세요.",
    liked: false,
    disliked: false,
    _id: "6459d41498cc91e0c3fb098e",
    createdAt: "2023-05-09T05:03:16.406Z",
    updatedAt: "2023-05-09T05:03:16.406Z",
  },
];

export const ChatScrollContainer = () => {
  const isMobile = useIsMobile();
  const chatId = useRecoilValue(chatIdState);
  const chat = useRecoilValue(currentChatState);
  const chatTrail = useRecoilValue(chatTrailState);
  const chipTutorial = useRecoilValue(chipTutorialState);
  const [fileSuggestChips, setFileSuggestChips] = useRecoilState(fileSuggestChipsState)
  const isChipSuggestOpen = useRecoilValue(chipSuggestOpenState);

  const [isLoadingMessage, setIsLoadingMessage] = React.useState(false);

  const alert = useAlert();
  const manageLoading = useChatLoading();

  const { handleScrollToBottom } = useChatScroll();
  const { initMessages, allMessages } = useHandleAllMessage();

  React.useEffect(() => {
    setFileSuggestChips(null);
  }, [chatId])

  const showShareButton = React.useMemo(
    () => allMessages.length > 1 && !isMobile && !navigator.language.includes("zh"),
    [allMessages, isMobile]
  );

  const lastMessageId = React.useMemo(() => {
    if (!allMessages.length) return "null";
    const last = allMessages[allMessages.length - 1];

    if (Array.isArray(last)) return last[last.length - 1]._id;
    return last._id;
  }, [allMessages]);

  const copy = () => {
    alert.removeAll();
    alert.show("결과물을 복사했어요.");
  };

  const anchorRef = useIntersectAll(
    (entry, observer) => {
      if (!entry.isIntersecting) {
        // handleScrollToBottom();
      }
    },
    {
      threshold: 0,
    }
  );

  const chatTransRef = useSpringRef();
  const chipTransRef = useSpringRef();

  const chatTransition = useTransition(allMessages, {
    keys: (item) => (Array.isArray(item) ? item[0].uuid : item.uuid),
    ref: chatTransRef,
    trail: chatTrail ? 10 : 800,
    from: { opacity: 0, transform: "translate3d(0, 20px, 0)" },
    enter: { opacity: 1, transform: "translate3d(0, 0px, 0)" },
    config: { duration: chatTrail ? 200 : 400 },
  });

  const chipTransition = useTransition(!chipTutorial && isChipSuggestOpen, {
    ref: chipTransRef,
    from: { opacity: 0, transform: "translate3d(0, 20px, 0)" },
    enter: { opacity: 1, transform: "translate3d(0, 0px, 0)" },
    unique: true,
  });

  useChain([chatTransRef, chipTransRef], [0, 0.1]);

  React.useEffect(() => {
    handleScrollToBottom();
  }, [lastMessageId, isChipSuggestOpen]);

  React.useEffect(() => {
    if (chipTutorial) {
      const timer = setTimeout(() => {
        initMessages(tutorialMessage);
      }, 400);
      return () => clearTimeout(timer);
    }
  }, [chipTutorial]);

  React.useLayoutEffect(() => {
    if (manageLoading.loading) {
      const timer = setTimeout(() => {
        setIsLoadingMessage(true);
      }, 5000);
      return () => {
        setIsLoadingMessage(false);
        clearTimeout(timer);
      };
    }
  }, [manageLoading.loading]);

  if (!chatId && !chipTutorial) return <></>;

  return (
    <BoxScrollWrapper>
      {showShareButton && <ChatShareButton />}
      {chatTransition((style, item, t, idx) => {
        if (isMessageArray(item)) {
          if (!item || item.length < 1) return null;

          const firstMessage = item[0];
          const type = getTypeOfMessage(firstMessage);

          const isImageOutputLoading =
            item.length > 1 &&
            firstMessage.type === "Message" &&
            item[1].type === "ImageOutput";

          let component: React.ReactNode | null = null;
          if (type === MESSAGE_TYPE.ASSISTANT_TOOL_OUTPUT) {
            component = (
              <ChatOutput.ToolOutput
                contents={item}
                toolId={firstMessage.toolId}
                onCopy={copy}
                chatId={chatId}
                withLeftTop={(props) => {
                  if (isMobile) return null;
                  const hasOnlyOneMessage = item.length < 2;
                  if (hasOnlyOneMessage) return null;

                  return <ChatToolPagination {...props} />;
                }}
              />
            );
          } else if (type === MESSAGE_TYPE.ASSISTANT_TOOL_CHIP_OUTPUT) {
            return (
              <ChatOutput.Assistant
                contents={item}
                onCopy={copy}
                chatId={chatId}
              />
            );
          } else if (
            type === MESSAGE_TYPE.ASSISTANT_MESSAGE &&
            !isImageOutputLoading
          ) {
            return (
              <ChatOutput.Assistant
                contents={item}
                onCopy={copy}
                chatId={chatId}
                withLeftTop={(props) => {
                  if (isMobile) return null;
                  const hasOnlyOneMessage = item.length < 2;
                  if (hasOnlyOneMessage) return null;

                  return <ChatToolPagination {...props} />;
                }}
              />
            );
          } else if (type === MESSAGE_TYPE.USER_COMMAND_CHIP_OUTPUT) {
            component = (
              <ChatOutput.Assistant
                contents={item}
                onCopy={copy}
                chatId={chatId}
              />
            );
          } else if (type === MESSAGE_TYPE.ASSISTANT_COMMAND_CHIP_OUTPUT) {
            component = (
              <ChatOutput.Assistant
                contents={item}
                onCopy={copy}
                chatId={chatId}
              />
            );
          } else if (type === MESSAGE_TYPE.ASSISTANT_DYNAMIC_CHIP_OUTPUT) {
            component = (
              <ChatOutput.Assistant
                contents={item}
                onCopy={copy}
                chatId={chatId}
              />
            );
          } else if (
            type === MESSAGE_TYPE.ASSISTANT_IMAGE_OUTPUT ||
            isImageOutputLoading
          ) {
            component = (
              <ChatOutput.ImageOutput
                contents={item}
                withLeftTop={(props) => {
                  if (isMobile) return null;
                  const hasOnlyOneMessage = item.length < 2;
                  if (hasOnlyOneMessage) return null;

                  return <ChatToolPagination {...props} />;
                }}
              />
            );
          } else if (type === MESSAGE_TYPE.ASSISTANT_PLUGIN_OUTPUT) {
            component = (
              <ChatOutput.PluginOutput
                contents={item}
                onCopy={copy}
                chatId={chatId}
              />
            );
          } else if (type === MESSAGE_TYPE.ASSISTANT_FILE_OUTPUT) {
            component = (
              <ChatOutput.FileOutput
                contents={item}
                onCopy={copy}
                chatId={chatId}
                withLeftTop={(props) => {
                  if (isMobile) return null;
                  const hasOnlyOneMessage = item.length < 2;
                  if (hasOnlyOneMessage) return null;

                  return <ChatToolPagination {...props} />;
                }}
              />
            );
          } else {
            const isDefaultForAssistantRole = firstMessage.role === "assistant";
            if (isDefaultForAssistantRole) {
              component = (
                <ChatOutput.Assistant
                  contents={item}
                  onCopy={copy}
                  chatId={chatId}
                />
              );
            }
          }

          return (
            <animated.div style={{ ...style }} key={firstMessage.uuid}>
              {component}
            </animated.div>
          ); // END handling newMessageType[]
        } else {
          const type = getTypeOfMessage(item);

          let component: React.ReactNode | null = null;
          switch (type) {
            case MESSAGE_TYPE.ASSISTANT_PLUGIN_OUTPUT:
            case MESSAGE_TYPE.USER_COMMAND_CHIP_OUTPUT:
            case MESSAGE_TYPE.ASSISTANT_COMMAND_CHIP_OUTPUT:
            case MESSAGE_TYPE.ASSISTANT_DYNAMIC_CHIP_OUTPUT:
              component = (
                <ChatOutput.Assistant
                  contents={[item]}
                  onCopy={copy}
                  chatId={chatId}
                />
              );
              break;
            case MESSAGE_TYPE.USER_TOOL_INPUT:
              component = <ChatOutput.ToolInput message={item} />;
              break;
            case MESSAGE_TYPE.USER_TOOL_CHIP_INPUT:
            case MESSAGE_TYPE.USER_COMMAND_CHIP_INPUT:
            case MESSAGE_TYPE.USER_MESSAGE:
            case MESSAGE_TYPE.USER_CHIP:
            case MESSAGE_TYPE.USER_IMAGE_INPUT:
            case MESSAGE_TYPE.USER_PLUGIN_INPUT:
            case MESSAGE_TYPE.USER_DYNAMIC_CHIP_INPUT:
              component = <ChatOutput.User message={item} />;
              break;
            case MESSAGE_TYPE.USER_FILE_INPUT:
              component = <ChatOutput.FileInput message={item} />;
              break;
            default:
              const isDefaultForAssistantRole = item.role === "assistant";
              const isDefaultForUserRole = item.role === "user";
              if (isDefaultForAssistantRole) {
                component = (
                  <ChatOutput.Assistant
                    contents={[item]}
                    onCopy={copy}
                    chatId={chatId}
                  />
                );
              }
              if (isDefaultForUserRole) {
                component = <ChatOutput.User message={item} />;
              }
          }

          // loading section
          const isLoadingUser =
            item.role === "user" &&
            (item._id.startsWith("loading-user-Chip-") ||
              item._id.startsWith("loading-user-message-") ||
              item._id.startsWith("loading-user-search-message"));

          const isFileMessageLoadingUser =
            item.role === "user" &&
            item._id.startsWith("loading-user-file-message");

          const isLoadingUserTool =
            item.role === "user" &&
            item._id.startsWith("loading-user-ToolInput-");

          const isNormalLoading =
            item.role === "assistant" &&
            (item._id.startsWith("loading-assistant-ToolOutput-") ||
              item._id.startsWith("loading-assistant-search-message"));

          const isStreamLoading =
            item._id.startsWith("loading-assistant-message-") ||
            item._id.startsWith("loading-invisible-regenerateChat") ||
            item._id.startsWith("loading-assistant-file-message");

          const isFileLoading =
            item.role === "user" && item._id.startsWith("loading-user-file");

          if (isFileLoading) {
            component = (
              <>
                <ChatOutput.FileLoading
                  showProgress={
                    manageLoading.loading && item._id === lastMessageId
                  }
                  message={item}
                />
                {manageLoading.loading && item._id === lastMessageId && (
                  <LoadingMessage>
                    파일 내용을 꼼꼼하게 분석하고 있어요! 조금만 기다려 주세요.{" "}
                    <br />
                    최대 5분까지 소요될 수 있어요.
                  </LoadingMessage>
                )}
              </>
            );
          }

          if (isFileMessageLoadingUser) {
            component = <ChatOutput.FileInput message={item} />;
          }

          if (isLoadingUser) {
            component = <ChatOutput.User message={item} />;
          }

          if (isLoadingUserTool) {
            component = <ChatOutput.ToolInput message={item} />;
          }

          if (isNormalLoading) {
            component = (
              <>
                <ChatOutput.Loading />
                {manageLoading.loading && isLoadingMessage && (
                  <LoadingMessage>
                    뤼튼이 열심히 답변을 생성하고 있어요. 조금만 기다려주세요!
                  </LoadingMessage>
                )}
              </>
            );
          }

          if (isStreamLoading) {
            component = <ChatOutput.StreamLoading />;
          }

          if (!component) return null;

          return (
            <animated.div style={{ ...style }} key={item.uuid}>
              {component}
            </animated.div>
          );
        }
      })}
      {isChipSuggestOpen &&
        chipTransition((style) => (
          <animated.div style={{ ...style }}>
            <SuggestChipContainer.Chat />
          </animated.div>
        ))}
      {fileSuggestChips && (
        <animated.div>
          <SuggestFileChipContainer
            disabled={manageLoading.loading}
            chips={fileSuggestChips}
          />
        </animated.div>
      )}
      <div
        style={{
          width: "100%",
          height: 56,
        }}
      />
      <div
        key="bottom-div"
        ref={anchorRef}
        style={{
          width: "100%",
          height: 1,
        }}
      />
    </BoxScrollWrapper>
  );
};

const BoxScrollWrapper = styled.div`
  max-width: 766px;
  width: 100%;

  margin: 0px auto;
  margin-bottom: 50px;

  padding: 40px 0px 0px;
`;

const LoadingMessage = styled.div`
  ${typo({
    weight: 600,
    size: "16px",
    height: "180%",
    color: "#A1A5B9",
  })};
`;

const ShareWrapper = styled.div<{ isDisabled: boolean }>`
  position: absolute;
  z-index: 5;
  right: 8%;
  top: 40px;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 8px;

  ${({ isDisabled }) =>
    typo({
      weight: 600,
      size: "14px",
      color: isDisabled ? newColors.GRAY_500 : newColors.PURPLE_500_PRIMARY,
    })}
`

const ShareButton = styled.div<{ isDisabled: boolean }>`
  width: 50px;
  height: 50px;
  background: ${({ isDisabled }) => isDisabled ? newColors.GRAY_500 : newColors.PURPLE_500_PRIMARY};
  border-radius: 25px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  cursor: ${({ isDisabled }) => isDisabled ? "default" : "pointer"};
  box-shadow: 0px 4px 20px rgba(57, 68, 112, 0.1); 
`;

const getTypeOfMessage = (message: newMessageType) => {
  return `${message.role}${message.type}`;
};
