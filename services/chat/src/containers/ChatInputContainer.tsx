import React from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import styled, { css } from "styled-components";
import { useAlert } from "react-alert";

import { colors, Divider, FlexWrapper, newColors, typo } from "@wrtn/ui";

import { allFilesState, chatToggleState, FormMetadataType, TOGGLE_BARD, TOGGLE_PERFORMANCE, TOGGLE_SPEED, ToolType, useEvent, useHoverPopup, useLoginDialog, userState } from "@wrtn/core";

import { OutputCounter } from "@wrtn/ui/components/OutputCounter";

import { SendButton } from "src/components/SendButton";

import { ToolInput } from "src/components/ChatInput/ToolInput";
import { DefaultInput } from "src/components/ChatInput/DefaultInput";
import { FileInput } from "src/components/ChatInput/FileInput";

import { useChatTextarea, useFileUpload } from "src/hooks";
import {
  ChatBottomPopup,
  ChatBottomButton,
  ChatGuideButton,
} from "src/components/ChatInput";
import { ModeContainer } from "./ModeContainer";
import { SearchInput } from "src/components/ChatInput/SearchInput";
import { lastOneMessage } from "src/stores";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

interface ChatInputContainerProps {
  input: string;
  setInput: React.Dispatch<React.SetStateAction<string>>;
  tool: ToolType;
  formData: {
    forms: FormMetadataType[];
    isDisabled: boolean;
    updateForms: (id: any, newForm: any) => void;
    exampleForms: () => void;
    resetForms: () => void;
    count: number;
    incrementCount: () => void;
    decrementCount: () => void;
  };
  loading: boolean;
  hasEnoughForShare: boolean;

  handleClickGenerate: (input: string) => Promise<void>;
  handleClickGenerateFile: (input: string) => Promise<void>;
}

export const ChatInputContainer = ({
  input,
  setInput,
  //
  tool,
  formData,
  loading,
  hasEnoughForShare,
  handleClickGenerate,
  handleClickGenerateFile,
}: ChatInputContainerProps) => {
  const user = useRecoilValue(userState);
  const [chatToggle, setChatToggle] = useRecoilState(chatToggleState);
  const hoverPopupGPT3 = useHoverPopup();
  const hoverPopupGPT4 = useHoverPopup();
  const hoverPopupBard = useHoverPopup();

  const [focus, setFocus] = React.useState<boolean>(false);
  const inputRef = React.useRef<HTMLTextAreaElement>(null);
  const loginDialog = useLoginDialog();

  const fileRef = React.useRef<HTMLInputElement>(null);
  const showFinder = () => {
    if (!fileRef.current) return
    if (!user) return loginDialog.handleOpen();

    fileRef.current.click();
  }

  const chatInput = useChatTextarea();
  const { collectEvent } = useEvent();

  const { isFileMode, handleUploadFile, closeFileMode } = useFileUpload();
  const alert = useAlert()

  const onClickCode = () => {
    collectEvent("click_code_markdown");
    const blockCode = '```\n(코드를 입력해주세요)\n```';

    setInput(prev => (prev ? `${prev}\n` : prev) + blockCode);
  }

  const onUploadFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) return
    const inputFile = e.target.files[0]
    const MAX_UPLOAD_SIZE = 32000000

    if (inputFile.size >= MAX_UPLOAD_SIZE) {
      return alert.show("파일용량이 너무 큽니다.")
    } else if (inputFile.type !== "application/pdf") {
      alert.show("pdf만 지원 가능합니다.")
    } else {
      handleUploadFile(e.target.files[0]);
    }
  }
  React.useEffect(() => {
    const EXCLAMATION_MARK = "!";
    if (input === EXCLAMATION_MARK) {
      chatInput.onChangeChatType("search");
      setInput("");
    }
  }, [input]);

  React.useEffect(() => {
    if (inputRef.current && ["default", "search"].includes(chatInput.type)) {
      inputRef.current.focus()
      const valueLength = inputRef.current.value.length;
      inputRef.current.setSelectionRange(valueLength, valueLength);
    }
  }, [chatInput.type]);

  const hideTool = chatInput.type === "search" || isFileMode;

  return (
    <>
      <ModeContainer mode={chatInput.type} onChangeMode={chatInput.onChangeChatType} isFileMode={isFileMode} hasEnoughForShare={hasEnoughForShare} />
      <InputWrapper focus={focus} column justify="flex-end" mode={chatInput.type}>
        {chatInput.type === "search" && (
          <SearchInput
            loading={false}
            input={input}
            inputRef={inputRef}
            setInput={setInput}
            setFocus={setFocus}
            handleClickGenerate={handleClickGenerate}
            onChangeMode={chatInput.onChangeChatType}
          />
        )}
        <DefaultInput
          input={input}
          isDefault={chatInput.isDefault && !isFileMode}
          loading={loading}
          inputRef={inputRef}
          setInput={setInput}
          setFocus={setFocus}
          handleClickGenerate={handleClickGenerate}
        />
        <ToolInput
          isTool={chatInput.isTool}
          tool={tool}
          formData={formData}
          closeTool={chatInput.closeTool}
        />
        {isFileMode && (
          <FileInput
            input={input}
            inputRef={inputRef}
            setInput={setInput}
            setFocus={setFocus}
            handleClickGenerate={handleClickGenerateFile}
            closeFileInput={() => closeFileMode(fileRef)} />
        )}
        <InputFooter row isTool={chatInput.isTool} mode={chatInput.type}>
          <FlexWrapper row style={{ gap: "14px", marginLeft: 20 }}>
            {!hideTool && (
              <>
                <ChatBottomButton type="code" onClick={onClickCode} />
                <ChatBottomButton type="image" />
                <ChatBottomButton type="file" tooltip="파일 첨부 (PDF만 지원)" onClick={showFinder}/>
                <input style={{ display: "none" }} type="file" accept=".pdf" ref={fileRef} onChange={onUploadFile}/>
                <Divider direction="vertical" height="16px" />
              </>
            )}
            <ChatBottomPopup setInput={setInput} hideTool={hideTool}/>
          </FlexWrapper>
          <div style={{ flex: 1 }} />
          {chatInput.isDefault && <ChatGuideButton />}
          {chatInput.isTool && (
            <CounterWrapper>
              <OutputCounter
                handleDown={formData.decrementCount}
                handleUp={formData.incrementCount}
                count={formData.count}
                maxCount={user && user.plan === "PLUS" ? 3 : 1}
                isText={false}
              />
            </CounterWrapper>
          )}
          { chatInput.isDefault &&
            <ChatToggle
            >
              <Toggle 
                onClick={() => { setChatToggle(TOGGLE_SPEED) }}
                ref={hoverPopupGPT3.hoverRef} 
                onMouseEnter={hoverPopupGPT3.onMouseEnter} 
                onMouseLeave={hoverPopupGPT3.onMouseLeave} 
                selected={chatToggle === TOGGLE_SPEED}>GPT-3.5</Toggle>
              <Toggle 
              onClick={() => { setChatToggle(TOGGLE_PERFORMANCE) }}
                ref={hoverPopupGPT4.hoverRef} 
                onMouseEnter={hoverPopupGPT4.onMouseEnter} 
                onMouseLeave={hoverPopupGPT4.onMouseLeave} 
                selected={chatToggle === TOGGLE_PERFORMANCE}>GPT-4</Toggle>
              {/* <Toggle 
              onClick={() => { setChatToggle(TOGGLE_BARD) }}
                ref={hoverPopupBard.hoverRef} 
                onMouseEnter={hoverPopupBard.onMouseEnter} 
                onMouseLeave={hoverPopupBard.onMouseLeave} 
                selected={chatToggle === TOGGLE_BARD}>Bard <BETA selected={chatToggle === TOGGLE_BARD}>BETA</BETA></Toggle> */}
            </ChatToggle>
          }
          <div style={{'width': '14px'}}/>
          <SendButton
            type={chatInput.isDefault ? "default" : "tool"}
            onClick={() => {
                if (isFileMode) {
                collectEvent("click_generate_btn", {
                  feature_menu: "file",
                  generate_position: "chat",
                  method: "button",
                })

                return handleClickGenerateFile(input);
              }
              handleClickGenerate(input);
              setInput("");
            }}
            disabled={
              loading ||
              (chatInput.isDefault && input.trim().length === 0) ||
              (chatInput.isTool && formData.isDisabled) ||
              (chatInput.type === "search" && input.trim().length === 0) ||
              (isFileMode && input.trim().length === 0)
            }
            loading={loading}
          />
        </InputFooter>
      </InputWrapper>
      { hoverPopupGPT3.open(() => {
          return (
            <ModalPositionPortal
              position={{
                top: hoverPopupGPT3.hoverRef?.current && hoverPopupGPT3.hoverRef.current.getBoundingClientRect().top + 35,
                left: hoverPopupGPT3.hoverRef?.current && hoverPopupGPT3.hoverRef.current.getBoundingClientRect().left + 8
              }}
            >
              <Popup>
                {"더 빨라요"}
              </Popup>
            </ModalPositionPortal>
          )
        })
      }
      { hoverPopupGPT4.open(() => {
          return (
            <ModalPositionPortal
              position={{
                top: hoverPopupGPT4.hoverRef?.current && hoverPopupGPT4.hoverRef.current.getBoundingClientRect().top + 35,
                left: hoverPopupGPT4.hoverRef?.current && hoverPopupGPT4.hoverRef.current.getBoundingClientRect().left + 8
              }}
            >
              <Popup>
                {"더 똑똑해요"}
              </Popup>
            </ModalPositionPortal>
          )
        })
      }
      { hoverPopupBard.open(() => {
          return (
            <ModalPositionPortal
              position={{
                top: hoverPopupBard.hoverRef?.current && hoverPopupBard.hoverRef.current.getBoundingClientRect().top + 35,
                left: hoverPopupBard.hoverRef?.current && hoverPopupBard.hoverRef.current.getBoundingClientRect().left + 8
              }}
            >
              <Popup>
                {"(베타) 최신 정보에 강해요"}
              </Popup>
            </ModalPositionPortal>
          )
        })
      }
    </>
  );
};

const InputWrapper = styled(FlexWrapper)<{ focus: boolean, mode: string }>`
  background: transparent;
  border-radius: 8px;
  max-width: 1217px;
  width: 100%;
  min-height: 124px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  /* overflow: hidden; */
  background-color: ${({ mode }) => mode === "search" ? newColors.PURPLE_200 : colors.WHITE};

  ${({ focus, mode }) =>
    focus &&
    css`
      border: 1px solid ${mode === "search" ? newColors.PURPLE_500_PRIMARY : newColors.BLUE_500_NOTICE};
      overflow: hidden;
    `}

  transition: height 0.4s ease-in-out;

  @media (max-width: 767px) {
    min-height: 106px;
    border-radius: 0;
  }
`;

const InputFooter = styled(FlexWrapper)<{ isTool: boolean, mode: string }>`
  width: 100%;
  padding: 10px;
  background-color: ${({ mode }) => mode === "search" ? newColors.PURPLE_200 : colors.WHITE};
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;

  ${(props) =>
    props.isTool &&
    css`
      border-top: 1px solid ${colors.BLUE_GRAY_LINE};
    `}
`;

const CounterWrapper = styled(FlexWrapper)`
  margin-right: 7px;
`;


const ChatToggle = styled(FlexWrapper)`
  height: 34px;
  width: 160px;
  border: 1px solid ${colors.BLUE_GRAY_LINE};
  border-radius: 5px;
  cursor: pointer;
  @media(max-width: 767px){
    display: none;
  }
`;

const Toggle = styled.div<{ selected: boolean }>`
  display: inline-block;
  text-align: center;
  width: 80px;

  height: 100%;
  border-radius: 5px;
  ${typo({
    weight: 600,
    size: "14px",
    height: "28px",
    color: colors.GRAY_58,
  })};

  ${({ selected }) =>
    selected &&
    css`
      outline: 1px solid ${colors.ACTION_BLUE};
      color: ${colors.ACTION_BLUE};
      background: ${colors.LIGHT_BLUE};
    `}
`;


const Popup = styled.div`
  background-color: ${colors.gray_80};
  padding: 4px 9px;
  ${typo({
    weight: 600,
    size: "12px",
    color: colors.white,
  })};
  /* transform: translateX(-50%); */
  border-radius: 5px;
`;