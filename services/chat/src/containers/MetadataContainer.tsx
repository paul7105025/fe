import { useEffect, useState } from "react";
import styled from "styled-components";
import { FlexWrapper, newColors, typo } from "@wrtn/ui";
import axios from "axios";
import { useIsMobile } from "@wrtn/core";

interface IMetadataContainer {
  links: string[];
}

const PROXY_URL = process.env.NEXT_PUBLIC_METADATA_PROXY || process.env.REACT_APP_METADATA_PROXY || "";

export const MetadataContainer = ({ links }: IMetadataContainer) => {
  const [metadata, setMetadata] = useState<any[]>([]);
  const isMobile = useIsMobile();

  useEffect(() => {
    void fetchMetadata(links);
  }, []);

  const fetchMetadata = async (linkArray: string[]) => {
    if (linkArray.length < 1) return;

    const meta = await Promise.all(linkArray.map(async link => {
      const response = await axios.post(`${PROXY_URL}/parse`, { url: link })

      return { ...response.data, link };
    }));
    setMetadata(meta)
  }

  const onClickLink = (link: string) => {
    window.open(link);
  }

  const cutText = (text: string, limit: number) => {
    if (text === "") return "";
    if (text.length < limit) return text;

    return `${text.slice(0, limit)}...`;
  }

  return (
    <BoxWrapper>
      {metadata.length > 0 && (
        <>
          {metadata.map((data) => {
            if (data.thumbnail === "") return null;

            return (
              <CardWrapper key={data.link} onClick={() => onClickLink(data.link)}>
                <ThumbnailWrapper>
                  <img src={data.thumbnail} />
                </ThumbnailWrapper>
                <TextWrapper>
                  <Title>{cutText(data.title ?? "", isMobile ? 20 : 30)}</Title>
                  <Content>{cutText(data.content ?? "", isMobile ? 30 : 80)}</Content>
                </TextWrapper>
              </CardWrapper>
            )
          })}
        </>
        )
        // (
        // <>
        //   {links.map((link) => {
        //     return (
        //       <CardWrapper key={link} onClick={() => onClickLink(link)}>
        //         <TextWrapper>
        //           <Title>{cutText(link ?? "", isMobile ? 20 : 30)}</Title>
        //           <Content>여기를 눌러 링크를 확인해보세요.</Content>
        //         </TextWrapper>
        //       </CardWrapper>
        //     )
        //   })}
        // </>
        // )
      }
    </BoxWrapper>
  )
}

const BoxWrapper = styled(FlexWrapper)`
  gap: 20px;
  flex-wrap: wrap;
  padding-right: 60px;

  @media (max-width : 480px) {
    padding-right: 0;
  }
`

const CardWrapper = styled(FlexWrapper)`
  flex-direction: row;
  width: 100%;
  border-radius: 5px;
  cursor: pointer;
  background: white;
  height: 92px;
  overflow: hidden;

  @media (max-width: 767px) {
    height: 50px;
  }
`

const ThumbnailWrapper = styled.div`
  width: 150px;
  height: 100%;
  background: ${newColors.PURPLE_500_PRIMARY};
  border-radius: inherit;

  @media (max-width : 767px) {
    width: 80px;
  }

  > img {
    width: 100%;
    height: 100%;
  }
`

const TextWrapper = styled(FlexWrapper)`
  flex-direction: column;
  align-items: self-start;
  align-self: baseline;
  flex: 1;
  margin: 13px 17px 12px 17px;

  @media (max-width: 767px) {
    margin: 5px;
  }
`

const Title = styled.div`
  padding: 0 5px;

  &:hover {
    text-decoration: underline;
  }

  @media (max-width: 767px) {
    ${typo({
      size: "14px",
      weight: 600,
    })}
  }
`

const Content = styled.div`
  padding: 0 5px;

  ${typo({
    size: "12px",
    weight: 500,
    color: newColors.GRAY_600,
  })}
`
