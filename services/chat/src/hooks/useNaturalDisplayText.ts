import React from "react";
import { useRecoilValue } from "recoil";
import { streamMessageState } from "@wrtn/core";

const TYPING_DELAY = 10;

export const useNaturalDisplayText = () => {
  const streamMessage = useRecoilValue(streamMessageState);
  const [displayText, setDisplayText] = React.useState("");

  React.useEffect(() => {
    const fullText = streamMessage;
    let currentCharIndex = displayText.length;
    let intervalId;

    const type = () => {
      setDisplayText(fullText.substring(0, currentCharIndex));
      currentCharIndex++;
      if (currentCharIndex > fullText.length) {
        clearInterval(intervalId);
      }
    };

    intervalId = setInterval(type, TYPING_DELAY);

    return () => clearInterval(intervalId);
  }, [streamMessage]);

  return { displayText };
}