import React from "react";
import { useRecoilState, useRecoilValue } from "recoil";

import { streamMessageState } from "@wrtn/core";

import { chatScrollToBottomState } from "src/stores";

export const useChatScroll = () => {
  const [chatScrollToBottom, setChatScrollToBottom] = useRecoilState(
    chatScrollToBottomState
  );

  const handleScrollToBottom = () => {
    chatScrollToBottom(0, true);
  };

  return {
    setChatScrollToBottom,
    handleScrollToBottom,
  };
};

export const useScrollRef = () => {
  const streamMessage = useRecoilValue(streamMessageState);

  const scrollRef = React.useRef<HTMLDivElement>();

  const { setChatScrollToBottom } = useChatScroll();

  React.useEffect(() => {
    const scrollToBottom = (trial = 0, isSmooth = true) => {
      if (scrollRef && scrollRef.current)
        scrollRef.current.scrollTo({
          top: scrollRef.current.scrollHeight,
          behavior: isSmooth ? "smooth" : "auto",
        });
      else return;
    };

    setChatScrollToBottom(() => scrollToBottom);
  }, [setChatScrollToBottom, scrollRef]);

  React.useEffect(() => {
    const scrollContainer = scrollRef.current;
    const { scrollHeight, scrollTop, offsetHeight } = scrollContainer;

    const isScrollingByUser =
      scrollHeight - scrollTop - offsetHeight > offsetHeight / 2;
    if (!isScrollingByUser && streamMessage.length % 10 === 0) {
      scrollContainer.scrollTo({
        top: scrollRef.current.scrollHeight,
        behavior: "smooth",
      });
    }
  }, [streamMessage]);

  return {
    scrollRef,
  };
};
