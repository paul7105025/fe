import React from "react";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import {
  allFilesState,
  currentFileState,
  getFileList,
  useReCaptcha,
  userState,
} from "@wrtn/core";
import { chatIdState, lastOneMessage } from "src/stores";

export const useFetchFile = () => {
  const setCurrentFile = useSetRecoilState(currentFileState);
  const user = useRecoilValue(userState);
  const lastMessage = useRecoilValue(lastOneMessage);
  const chatId = useRecoilValue(chatIdState);
  const [files, setFiles] = useRecoilState(allFilesState);

  const { openRecaptcha } = useReCaptcha()

  React.useEffect(() => {
    if (!user) {
      setCurrentFile(null);
      setFiles([]);
    }
  }, [user]);

  React.useEffect(() => {
    if (lastMessage && lastMessage.fileId) {
      const file = files.find(f => f._id === lastMessage.fileId);
      if (file) {
        setCurrentFile(file);
      }
    } else if (lastMessage && !lastMessage.fileId){
      setCurrentFile(null);
    }
  }, [chatId, lastMessage])

  const fetchFilesByUserId = async () => {
    const res = await getFileList();
    if (res.status === 200) {
      const { data } = res.data;
      setFiles(data);
    } else if (res.status > 400) {
      setFiles([]);

      if (res.status === 444) {
        openRecaptcha();
      }
    }
    setCurrentFile(null);
  };

  return {
    fetchFilesByUserId,
  };
};
