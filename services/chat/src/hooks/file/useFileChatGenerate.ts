import React, { useEffect } from "react";
import { useAlert } from "react-alert";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";

import {
  DEFAULT_ERROR_MESSAGE,
  GENERATE_FAIL,
  GENERATE_FAIL_TOO_MANY_REQUEST,
  GENERATE_INVALID_INPUT,
  SERVER_ERROR_MESSAGE,
  currentFileState,
  fileSuggestChipsState,
  handleFileStream,
  messageType,
  putUser,
  streamAbortState,
  useCustomFetch,
  useEvent,
  useLoginDialog,
  useModalOpen,
  useReCaptcha,
  useRefresh,
  useTagManager,
} from "@wrtn/core";
import { getPlatform } from "@wrtn/core/utils/userAgent";

import { useHandleAllMessage } from "../chatGenerate";

import { chatIdState, lastUserMessageSelector } from "src/stores";

import { useChatInit } from "../useChatInit";
import { useChatLoading } from "../useChatLoading";
import { useChatPreviousMessage } from "../useChatLastMessage";

const DEFAULT_MESSAGE = {
  content: "",
  createdAt: "",
  disliked: false,
  liked: false,
  role: "user",
  updatedAt: "",
  _id: "",
  type: "",
  toolId: "",
};

export const useFileChatGenerate = ({ setInput }: { setInput?: any }) => {
  const file = useRecoilValue(currentFileState);
  const chatId = useRecoilValue(chatIdState);
  const lastUserMessage = useRecoilValue(lastUserMessageSelector);

  const setStreamAbort = useSetRecoilState(streamAbortState);

  const [fileSuggestChips, setFileSuggestChips] = useRecoilState(
    fileSuggestChipsState
  );

  const loginDialog = useLoginDialog();
  const manageLoading = useChatLoading();
  const chatPreviousMessage = useChatPreviousMessage();
  const banAbuseModal = useModalOpen({ modalId: "banAbuseModal" });

  const {
    changeMessage,
    changeStreamMessage,
    removeRerollTempMessage,
    replaceRerollTempMessage,
  } = useHandleAllMessage();
  const { initChat } = useChatInit();
  const { fetchChat } = useRefresh();
  const { customFetch } = useCustomFetch();

  const alert = useAlert();

  const {
    collectEvent,
    collectUserProperties,
    collectUserProperties_increment,
    collectUserProperties_once,
  } = useEvent();
  const tagManager = useTagManager();

  useEffect(() => {
    if (!chatId) setFileSuggestChips(null);
  }, [chatId]);

  useEffect(() => {
    if (!file) setFileSuggestChips(null);
  }, [file]);

  const { openRecaptcha } = useReCaptcha()

  const generateFileChat = async (message: string) => {
    let fileChatId = chatId;
    if (!chatId) {
      fileChatId = await initChat();
    }
    console.log(fileChatId);

    setFileSuggestChips(null);

    const userLoadingId = manageLoading.startLoading(
      "loading-user-file-message",
      {
        ...DEFAULT_MESSAGE,
        _id: fileChatId,
        type: "FileInput",
        content: message,
        role: "user",
        fileId: file._id,
      }
    );

    const assistantLoadingId = manageLoading.startLoading(
      "loading-assistant-file-message",
      {
        ...DEFAULT_MESSAGE,
        fileId: file._id,
        _id: fileChatId,
        type: "FileOutput",
        content: "",
        role: "assistant",
      }
    );

    const controller = new AbortController();
    const platform = getPlatform();
    customFetch(
      `/file/${file._id}/chat/${fileChatId}/stream?platform=${
        platform ?? "web"
      }`,
      {
        method: "POST",
        body: JSON.stringify({ message, reroll: false }),
        signal: controller.signal,
      }
    )
      .then(async (res) => {
        const stream = res.body?.getReader();

        return await handleFileStream(
          stream,
          changeStreamMessage,
          updateCompleteMessage
        );
      })
      .catch((err) => updateCompleteMessage(null, err));

    setStreamAbort({
      startTime: new Date(),
      controller,
    });

    const updateCompleteMessage = async (
      output: messageType | null,
      error?: any
    ) => {
      const resultMessage: messageType = {
        ...DEFAULT_MESSAGE,
        _id: file._id,
        fileId: file._id,
        role: "assistant",
        type: "FileOutput",
        ...output,
      };

      if (!error) {
        if (setInput) setInput("");
        updateEvent(resultMessage.content, false);
        changeMessage([resultMessage], assistantLoadingId);

        await putUser({ data: { isNewbie: false } });
        await fetchChat();
      } else {
        handleError(error, "", userLoadingId, assistantLoadingId);
      }

      chatPreviousMessage.init();
      manageLoading.stopLoading();
    };
  };

  const reGenerateFileChat = async (fileId: string) => {
    if (!chatId) return;
    const prevMessage = lastUserMessage.content;

    const tempRerollMessage: messageType = {
      ...DEFAULT_MESSAGE,
      _id: "loading-invisible-file",
      type: "FileOutput",
      content: "",
      role: "assistant",
    };
    manageLoading.startRerollLoading(tempRerollMessage);

    const controller = new AbortController();
    const platform = getPlatform();
    customFetch(
      `/file/${fileId}/chat/${chatId}/stream?platform=${platform ?? "web"}`,
      {
        method: "POST",
        body: JSON.stringify({ message: prevMessage, reroll: true }),
        signal: controller.signal,
      }
    )
      .then(async (res) => {
        const stream = res.body?.getReader();

        return await handleFileStream(
          stream,
          changeStreamMessage,
          updateCompleteMessage
        );
      })
      .catch((err) => updateCompleteMessage(null, err));

    setStreamAbort({
      startTime: new Date(),
      controller,
    });

    const updateCompleteMessage = async (
      output: messageType | null,
      error?: any
    ) => {
      if (!error) {
        const resultMessage: messageType = {
          ...DEFAULT_MESSAGE,
          _id: fileId,
          fileId,
          role: "assistant",
          type: "FileOutput",
          ...output,
        };

        if (setInput) setInput("");
        updateEvent(resultMessage.content, true);
        replaceRerollTempMessage(tempRerollMessage._id, resultMessage);
        await fetchChat();
      } else {
        handleError(error, "");
        removeRerollTempMessage(tempRerollMessage._id);
      }
      chatPreviousMessage.init();
      manageLoading.stopLoading();
    };
  };

  const generateFileChatByChip = async (chipIndex: number) => {
    if (!chatId) return;
    const chip = fileSuggestChips[chipIndex];

    collectEvent("click_commend_chip", {
      commend_chip_type: "file",
      commend_chip_name: chip,
    });

    const userLoadingId = manageLoading.startLoading(
      "loading-user-file-message",
      {
        ...DEFAULT_MESSAGE,
        _id: chatId,
        type: "FileInput",
        content: chip,
        role: "user",
        fileId: file._id,
      }
    );

    const assistantLoadingId = manageLoading.startLoading(
      "loading-assistant-file-message",
      {
        ...DEFAULT_MESSAGE,
        _id: chatId,
        type: "FileOutput",
        content: "",
        role: "assistant",
      }
    );

    // console.log(chip);
    // console.log(`/file/${file._id}/chat/${chatId}/stream`);
    setFileSuggestChips(null);

    const controller = new AbortController();
    const platform = getPlatform();
    customFetch(
      `/file/${file._id}/chat/${chatId}/stream?platform=${platform ?? "web"}`,
      {
        method: "POST",
        body: JSON.stringify({ message: chip, reroll: false }),
        signal: controller.signal,
      }
    )
      .then(async (res) => {
        const stream = res.body?.getReader();

        return await handleFileStream(
          stream,
          changeStreamMessage,
          updateCompleteMessage
        );
      })
      .catch((err) => updateCompleteMessage(null, err));

    setStreamAbort({
      startTime: new Date(),
      controller,
    });

    const updateCompleteMessage = async (
      output: messageType | null,
      error?: any
    ) => {
      const resultMessage: messageType = {
        ...DEFAULT_MESSAGE,
        _id: file._id,
        fileId: file._id,
        role: "assistant",
        type: "FileOutput",
        ...output,
      };

      if (!error) {
        if (setInput) setInput("");
        updateEvent(resultMessage.content, false);
        changeMessage([resultMessage], assistantLoadingId);
        await fetchChat();
      } else {
        handleError(error, chip, userLoadingId, assistantLoadingId);
      }

      chatPreviousMessage.init();
      manageLoading.stopLoading();
    };
  };

  const handleError = (
    error: { status: number; message: string },
    input: string,
    userLoadingId?: string,
    assistantLoadingId?: string
  ) => {
    const { status, message } = error;
    if (status === 401) {
      loginDialog.handleOpen();
    } else if (status === 403) {
      banAbuseModal.open();
    } else if (status === 429) {
      alert.show(GENERATE_FAIL_TOO_MANY_REQUEST);
    } else if (status === 444) {
      openRecaptcha();
    } else if (status === 500) {
      if ((message as string).includes("GPT")) {
        alert.show(GENERATE_FAIL);
      } else if ((message as string).includes("SD")) {
        alert.show(GENERATE_FAIL);
      } else if ((message as string).includes("FILTER")) {
        alert.show(GENERATE_INVALID_INPUT);
      } else {
        alert.show(SERVER_ERROR_MESSAGE);
      }
    } else {
      alert.show(DEFAULT_ERROR_MESSAGE);
    }

    if (setInput) setInput(input);
    if (userLoadingId) manageLoading.removeLoading(userLoadingId);
    if (assistantLoadingId) manageLoading.removeLoading(assistantLoadingId);
  };

  const updateEvent = (content: string, reroll: boolean) => {
    collectUserProperties_once("first_generate_date", new Date());
    collectUserProperties_increment("count_generate_accum", 1);
    collectUserProperties("last_generate_date", new Date());
    collectUserProperties_increment(
      "amount_generate_accum",
      content.replace(/ /g, "")?.length
    );

    tagManager({
      event: "generate_done",
      data: {
        feature_menu: "file",
        generate_position: "chat",
        re_generate: reroll,
        generate_character: content.replace(/ /g, "")?.length,
      },
    });
    collectEvent("generate_done", {
      feature_menu: "file",
      generate_position: "chat",
      re_generate: reroll,
      generate_character: content.replace(/ /g, "")?.length,
    });
  };

  return {
    generateFileChat,
    reGenerateFileChat,
    generateFileChatByChip,
  };
};
