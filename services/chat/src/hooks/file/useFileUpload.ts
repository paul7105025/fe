import { useAlert } from "react-alert";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";

import { useChatLoading } from "../useChatLoading";
import { chatIdState } from "src/stores";
import {
  getPreSignedURL,
  uploadS3,
  fileLoadingProgressState,
  messageType,
  fileType,
  ANALYZE_FAIL,
  executePollingRequest,
  postFile,
  fileLoadingTimerState,
  fileSuggestChipsState,
  useEvent,
  currentFileState,
} from "@wrtn/core";

import { useHandleAllMessage } from "../chatGenerate";
import { useChatInit } from "../useChatInit";
import { useFileManage } from "./useFileManage";

const USER_FAKE_FILE_MESSAGE: messageType = {
  content: "",
  createdAt: new Date().toISOString(),
  updatedAt: new Date().toISOString(),
  disliked: false,
  liked: false,
  role: "user",
  _id: "loading-user-file",
  type: "FileInput",
};

const ASSISTANT_FAKE_FILE_MESSAGE: messageType = {
  content: "",
  createdAt: new Date().toISOString(),
  updatedAt: new Date().toISOString(),
  disliked: false,
  liked: false,
  role: "assistant",
  _id: "loading-assistant-file",
  type: "FileOutput",
};

export const useFileUpload = () => {
  const chatId = useRecoilValue(chatIdState);

  const setLoadingPercent = useSetRecoilState(fileLoadingProgressState);
  const setFileSuggestChips = useSetRecoilState(fileSuggestChipsState);

  const [fileTimer, setFileTimer] = useRecoilState(fileLoadingTimerState);
  const [currentFile, setCurrentFile] = useRecoilState(currentFileState);

  const alert = useAlert();
  const manageLoading = useChatLoading();

  const { initChat } = useChatInit();
  const { collectEvent } = useEvent();
  const { addMessage, changeMessage } = useHandleAllMessage();
  const { addFile, removeFile } = useFileManage();

  const handleUploadFile = async (_file: File) => {
    if (manageLoading.loading) return;
    setLoadingPercent(0);

    let currentChatId = chatId;

    // 0. chat이 없을때 생성
    if (!chatId) {
      currentChatId = await initChat();
    }

    const loadingId = manageLoading.startLoading("loading-user-file", {
      ...USER_FAKE_FILE_MESSAGE,
      content: `${_file.name}`,
    });

    let file;

    try {
      // [1/3] 1. presigned url 요청
      const res1 = await getPreSignedURL();
      if (!res1.data) throw Error("no data");

      const { fileUploadUrl, fileUUID } = res1.data.data;

      // [1/3] 2. aws s3 파일 업로드
      await uploadS3(fileUploadUrl, _file);

      // [2/3] 1. 분석 요청
      const res2 = await postFile({
        fileUUID,
        fileName: _file.name,
        chatId: currentChatId,
        fileExtension: "pdf",
      });
      if (!res2.data) throw Error("no data");

      const { fileId, fileUrl } = res2.data.data;

      const fakeFile: fileType = {
        _id: fileId,
        fileName: _file.name,
        fileUrl,
        fileUUID,
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
        size: _file.size,
      };
      file = fakeFile;

      addFile(fakeFile);
      setCurrentFile(fakeFile);

      // [2/3] 2. 분석 상황 체크
      const updateProgress = (progress: number) => setLoadingPercent(progress);
      const intervalId = executePollingRequest(
        { file: fakeFile, chatId: currentChatId, loadingId },
        updateProgress,
        callbackAnalyzeSuccess
      );

      setFileTimer({ intervalId, loadingId, fileId: file._id });
    } catch (err) {
      const response = err?.response || err;
      handleError(response, loadingId, file);
    }
  };

  // [3/3] 성공시에 결과값 처리
  const callbackAnalyzeSuccess = (
    loadingObj: { loadingId: string; file: fileType },
    result: { intro: string; questions: string[] },
    err: any
  ) => {
    const { loadingId, file } = loadingObj;

    if (err) {
      const response = err?.response || err;
      handleError(response, loadingId, file);
    }

    const { intro, questions } = result;

    changeMessage(
      {
        ...USER_FAKE_FILE_MESSAGE,
        fileId: file._id,
        _id: `${file._id}-user`,
        content: file.fileName,
      },
      loadingId
    );

    addMessage([
      {
        ...ASSISTANT_FAKE_FILE_MESSAGE,
        fileId: file._id,
        _id: file._id,
        content: intro,
      },
    ]);

    setFileSuggestChips(questions);

    collectEvent("use_file", {
      file_size: file.size,
      upload_success: true,
    });

    setFileTimer(null);
    manageLoading.stopLoading();
  };

  const stopAnalyzeFile = async () => {
    const { loadingId, fileId, intervalId } = fileTimer;
    clearInterval(intervalId);

    removeFile(fileId);
    setCurrentFile(null);
    // await deleteFile(fileId);

    alert.show("분석이 중단되었습니다.");

    collectEvent("file_stop_upload");

    manageLoading.stopLoading();
    manageLoading.removeLoading(loadingId);
  };

  const handleError = async (
    error: any,
    loadingId: string,
    file?: fileType
  ) => {
    const { code, message } = error;
    // TODO: code에 따라 분기??
    if (code === 422) {
      alert.show(message ?? ANALYZE_FAIL);
    } else {
      alert.show(ANALYZE_FAIL);
    }

    collectEvent("use_file", {
      file_size: file?.size,
      upload_success: false,
    });

    if (file) {
      removeFile(file._id);
      // await deleteFile(file._id);
    }

    setCurrentFile(null);
    manageLoading.stopLoading();
    manageLoading.removeLoading(loadingId);
  };

  const closeFileMode = (fileRef: React.MutableRefObject<any> | null) => {
    if (fileRef?.current) fileRef.current.value = "";

    // 파일을 여는 시점을 알아야함
    // collectEvent("", {
    //   is_new: !!currentFile.size,
    //   ask_turns: allMessages.length / 2,
    // })

    setCurrentFile(null);
  };

  return {
    isFileMode: !!currentFile,
    handleUploadFile,
    stopAnalyzeFile,
    closeFileMode,
  };
};
