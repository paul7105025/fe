import { fileType, deleteFile, allFilesState, currentFileState } from "@wrtn/core";
import { useRecoilState } from "recoil";

export const useFileManage = () => {
  const [files, setFiles] = useRecoilState(allFilesState);
  const [currentFile, setCurrentFile] = useRecoilState(currentFileState);

  const addFile = async (file: fileType) => {
    setFiles([...files, file])
  }

  const removeFile = async (fileId: string) => {
    const newFiles = files.map(file => file._id === fileId ? { ...file, isDeleted: true } : file);
    setFiles(newFiles);
    await deleteFile(fileId);

    if (fileId === currentFile?._id) {
      setCurrentFile(null);
    }
  }

  const handleDownloadFile = async (id?: string) => {
    const fileId = id ?? currentFile._id;
    const selectedFile = files.find(f => f._id === fileId);
    if (!selectedFile) return;

    window.open(selectedFile.fileUrl);
  }

  return {
    addFile,
    removeFile,
    handleDownloadFile,
  }
}