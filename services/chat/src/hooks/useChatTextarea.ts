import React from "react";
import { useRecoilState, useRecoilValue } from "recoil";

import { chatInputType, chatInputTypeState, currentFileState, currentToolChatIdState, useEvent } from "@wrtn/core";

// 채팅 인풋 창에 관한 로직
export const useChatTextarea = () => {
  const [toolId, setToolId] = useRecoilState(currentToolChatIdState);
  const [type, setType] = useRecoilState(chatInputTypeState);

  const { collectEvent } = useEvent();

  React.useEffect(() => {
    if (!toolId) setType("default");
    if (toolId) setType("tool");
  }, [setType, toolId]);

  const closeTool = () => {
    setType("default");
    setToolId(null);
    collectEvent("close_tool_in_chat");
  };

  const onChangeChatType = (newType: chatInputType) => {
    if (type === "default" && newType === "search") {
      collectEvent("click_chat_mode", { mode_name: "검색 모드" })
    }

    if (type === "search" && newType === "default") {
      collectEvent("click_chat_mode", { mode_name: "일반 모드" })
    }

    setType(newType)
  }

  const isDefault = React.useMemo(() => type === "default", [type]);
  const isTool = React.useMemo(() => type === "tool", [type]);

  return {
    type,
    isDefault,
    isTool,
    onChangeChatType,
    closeTool,
  };
};

export default useChatTextarea;
