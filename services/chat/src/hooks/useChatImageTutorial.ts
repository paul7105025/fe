import React from "react";
import { getLocal } from "@wrtn/core";

type imageTutorialType = {
  first: null | string;
  second: null | string;
};

export const useChatImageTutorial = () => {
  const [imageTutorial, setImageTutorial] = React.useState<imageTutorialType>({
    first: null,
    second: null,
  });

  React.useEffect(() => {
    const savedImageTutorial = getLocal<imageTutorialType>("imageTutorial");

    if (savedImageTutorial) {
      setImageTutorial(savedImageTutorial);
    }
  }, []);

  const isImageTutorialOpen = React.useMemo(() => {
    let temp = true;

    if (imageTutorial.second !== null) {
      temp = false;
    }

    if (imageTutorial.first !== null) {
      const currentTime = new Date();
      const firstTime = new Date(imageTutorial.first);

      const diffTime = Math.abs(currentTime.getTime() - firstTime.getTime());
      const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));

      if (diffDays < 1) {
        temp = false;
      }
    }

    return temp;
  }, [imageTutorial]);

  const closeImageTutorial = () => {
    const temp = {
      first: imageTutorial.first,
      second: imageTutorial.second,
    };

    if (temp.first === null) {
      temp.first = new Date().toISOString();
    } else if (temp.second === null) {
      temp.second = new Date().toISOString();
    }

    localStorage.setItem("imageTutorial", JSON.stringify(temp));
  };

  return {
    isImageTutorialOpen,
    closeImageTutorial,
  };
};
