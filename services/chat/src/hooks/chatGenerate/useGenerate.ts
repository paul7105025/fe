import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";

import {
  chatInputType,
  chatToggleState,
  chipSuggestOpenState,
  currentToolChatIdState,
  streamAbortState,
  TOGGLE_BARD,
  useEvent,
  useLoginDialog,
  userState,
} from "@wrtn/core";
import { chatIdState } from "src/stores";

import { useChatInit } from "../useChatInit";
import { useChatLoading } from "../useChatLoading";

import useChatSuggest from "./useChatSuggest";
import useChatGenerate from "./useChatGenerate";
import useChatTextarea from "../useChatTextarea";
import useChatToolGenerate from "./useChatToolGenerate";

export type handleClickRegenerateProps = {
  type: chatInputType;
  toolId?: string;
};

export const useGenerate = ({ tool, setInput, input }) => {
  const user = useRecoilValue(userState);
  const chatId = useRecoilValue(chatIdState);
  const toolId = useRecoilValue(currentToolChatIdState);
  const chatToggle = useRecoilValue(chatToggleState);

  const [streamAbort, setStreamAbort] = useRecoilState(streamAbortState);

  const setIsChipShow = useSetRecoilState(chipSuggestOpenState);

  const manageLoading = useChatLoading();
  const chatInput = useChatTextarea();

  const { handleOpen } = useLoginDialog();
  const { initChat } = useChatInit();

  const {
    regenerateChat,
    generateStreamChat,
    generateSearchChat,
    generateBardChat,
    regenerateBardChat,
  } = useChatGenerate({ setInput });

  const { generateTool, regenerateTool, formData } = useChatToolGenerate({
    tool,
  });

  const { showToolSuggest, closeSuggest } = useChatSuggest();
  const { collectEvent } = useEvent();

  // 생성 누를 때
  const handleClickGenerate = async (input: string) => {
    if (!user) {
      handleOpen();
      return;
    }

    if (manageLoading.loading) return;

    const isImageType = input.includes("그려줘") || input.includes("Draw");

    setIsChipShow(false);
    if (chatInput.isDefault) {
      collectEvent("click_generate_btn", {
        feature_menu: isImageType ? "image" : "chat",
        generate_position: "chat",
        feature_category: "",
        feature_name: "",
        feature_tag: "",
        feature_form: "",
        feature_prompt: "",
        prompt_id: [],
        repeat_count: 1,
        method: "button",
      });
      if (chatId) {
        closeSuggest();
        if (chatToggle === TOGGLE_BARD) {
          // BARD
          generateBardChat(input, chatId);
        } else {
          // GPT-3.5 / GPT-4
          setStreamAbort({
            startTime: new Date(),
            controller: generateStreamChat(input),
          });
        }
      } else {
        await initChat(async (data) => {
          setTimeout(() => {
            if (data._id) {
              closeSuggest();
              if (chatToggle === TOGGLE_BARD) {
                // BARD
                generateBardChat(input, data._id);
              } else {
                // GPT-3.5 / GPT-4
                setStreamAbort({
                  startTime: new Date(),
                  controller: generateStreamChat(input, data._id),
                });
              }
            }
          }, 200);
        });
      }
    }

    if (chatInput.isTool) {
      collectEvent("click_generate_btn", {
        feature_menu: "tool",
        generate_position: "chat",
        feature_category: tool.category,
        feature_name: tool.name,
        feature_tag: tool.tag,
        feature_form: tool.kind,
        feature_prompt: [],
        prompt_id: [],
        repeat_count: formData.count,
        method: "button",
      });
      if (toolId && chatId) {
        const res = await generateTool();
        if (res) {
          closeSuggest();
          await showToolSuggest({ toolId });
        }
      } else if (toolId) {
        await initChat(async (data) => {
          setTimeout(async () => {
            if (data._id) {
              closeSuggest();
              const res = await generateTool(data._id);

              if (res) await showToolSuggest({ toolId });
            }
          }, 200);
        });
      }
    }

    if (chatInput.type === "search") {
      collectEvent("click_generate_btn", {
        feature_menu: "search",
        generate_position: "chat",
        method: "button",
      });

      if (chatId) {
        closeSuggest();
        await generateSearchChat(input);
      } else {
        const newChatId = await initChat();
        closeSuggest();
        await generateSearchChat(input, newChatId);
      }
    }
  };

  //다시 생성 버튼을 누를 때
  const handleClickRegenerate = async ({
    type,
    toolId,
  }: handleClickRegenerateProps) => {
    if (!user) {
      handleOpen();
      return;
    }

    if (manageLoading.loading) return;
    if (type === "tool" && !toolId) return;
    if (type === "search") return;

    setIsChipShow(false);
    if (type === "default") {
      if (chatId) {
        if (chatToggle === TOGGLE_BARD) {
          // BARD
          regenerateBardChat(input, chatId);
        } else {
          // GPT-3.5 / GPT-4
          setStreamAbort({
            startTime: new Date(),
            controller: regenerateChat(input),
          });
        }
      } else {
        await initChat(async (data) => {
          setTimeout(() => {
            if (data._id) {
              if (chatToggle === TOGGLE_BARD) {
                // BARD
                regenerateBardChat(input, data._id);
              } else {
                // GPT-3.5 / GPT-4
                setStreamAbort({
                  startTime: new Date(),
                  controller: regenerateChat(input, data._id),
                });
              }
            }
          }, 200);
        });
      }
    }
    if (type === "tool") {
      if (chatId) {
        await regenerateTool(chatId);
      } else {
        await initChat(async (data) => {
          setTimeout(async () => {
            if (data._id) {
              await regenerateTool(data._id);
            }
          });
        });
      }
    }
  };

  // 채팅 멈추기
  const handleStopGenerate = () => {
    if (!streamAbort) return;

    const { controller: abortController, startTime } = streamAbort;
    const diff = (new Date().getTime() - startTime.getTime()) / 1000;
    collectEvent("stop_generate", { wait_time: diff });

    abortController.abort();
  };

  return {
    handleClickGenerate,
    handleClickRegenerate,
    handleStopGenerate,
    formData,
  };
};

export default useGenerate;
