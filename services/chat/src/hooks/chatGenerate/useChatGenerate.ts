import React from "react";
import { useAlert } from "react-alert";
import { useRecoilValue } from "recoil";

import {
  GENERATE_FAIL,
  GENERATE_INVALID_INPUT,
  chatInputToolSourceState,
  chatToggleState,
  messageType,
  putUser,
  useEvent,
  useLoginDialog,
  useModalOpen,
  useRefresh,
  useCustomFetch,
  handleStream,
  useTagManager,
  postGenerateChat_Id,
  GENERATE_FAIL_TOO_MANY_REQUEST,
  SERVER_ERROR_MESSAGE,
  DEFAULT_ERROR_MESSAGE,
  userState,
  postGenerateBard_Id,
  useReCaptcha,
  SERVICE_UNAVAILABLE_MESSAGE,
} from "@wrtn/core";
import { chatIdState } from "src/stores";

import { useChatLoading } from "../useChatLoading";
import { useHandleAllMessage } from "./useHandleAllMessage";
import { useChatPreviousMessage } from "../useChatLastMessage";

import { getPlatform } from "@wrtn/core/utils/userAgent";

const loadingMessageTemplate = (id: string, content: string): messageType => ({
  content: content,
  createdAt: "",
  disliked: false,
  liked: false,
  role: "user",
  updatedAt: "",
  _id: id,
  type: "",
  toolId: "",
});

const loadingMessageTemplate2 = (
  id: string,
  content?: string
): messageType => ({
  content: "",
  createdAt: "",
  disliked: false,
  liked: false,
  role: "assistant",
  updatedAt: "",
  _id: id,
  type: "Message",
  toolId: "",
  image: {
    _id: "loading-image",
    imageUrls: [
      { url: "", liked: false, disliked: false },
      { url: "", liked: false, disliked: false },
      { url: "", liked: false, disliked: false },
      { url: "", liked: false, disliked: false },
    ],
  },
});

// 채팅 전체적인 거랑 관련된 것들
export const useChatGenerate = ({ setInput }) => {
  const user = useRecoilValue(userState);
  const chatId = useRecoilValue(chatIdState);
  const chatToggle = useRecoilValue(chatToggleState);

  const alert = useAlert();
  const loginDialog = useLoginDialog();

  const { fetchChat } = useRefresh();
  const manageLoading = useChatLoading();
  const { customFetch } = useCustomFetch();

  const {
    changeMessage,
    replaceRerollTempMessage,
    removeRerollTempMessage,
    changeStreamMessage,
  } = useHandleAllMessage();
  const chatPreviousMessage = useChatPreviousMessage();

  const chatInputToolSource = useRecoilValue(chatInputToolSourceState);

  const {
    collectEvent,
    collectUserProperties,
    collectUserProperties_increment,
    collectUserProperties_once,
  } = useEvent();

  const tagManager = useTagManager();
  const banAbuseModal = useModalOpen({ modalId: "banAbuseModal" });

  const { openRecaptcha } = useReCaptcha()

  // 다시 생성 - 채팅 // 1. 채팅 다시 생성 버튼을 누를 때.
  const regenerateChat = (input: string, _chatId?: string) => {
    const tempRerollMessage = loadingMessageTemplate2(
      "loading-invisible-regenerateChat",
      ""
    );
    manageLoading.startRerollLoading({ ...tempRerollMessage, type: "Message" });

    const controller = new AbortController();
    const platform = getPlatform();
    customFetch(
      `/stream2/${_chatId || chatId}?type=big&model=${
        chatToggle === 0 ? "GPT3.5" : "GPT4"
      }&platform=${platform ?? "web"}`,
      {
        method: "POST",
        body: JSON.stringify({ message: "", reroll: true }),
        signal: controller.signal,
      }
    )
      .then(async (res) => {
        const stream = res.body?.getReader();
        if (!stream) return;

        return await handleStream(
          stream,
          changeStreamMessage,
          updateCompleteMessage
        );
      })
      .catch((err) => updateCompleteMessage(null, err));

    return controller;

    function updateCompleteMessage(message: messageType, errorObj: any) {
      if (message && !errorObj) {
        updateAllAfterChat(message, undefined, tempRerollMessage);
      }

      if (errorObj) {
        const { status, message } = errorObj;
        if (status === 401) {
          loginDialog.handleOpen();
        } else if (status === 403) {
          banAbuseModal.open();
        } else if (status === 429) {
          alert.show(GENERATE_FAIL_TOO_MANY_REQUEST);
        } else if (status === 444) {
          openRecaptcha();
        } else if (status === 500) {
          if ((message as string).includes("GPT")) {
            alert.show(GENERATE_FAIL);
          } else if ((message as string).includes("SD")) {
            alert.show(GENERATE_FAIL);
          } else if ((message as string).includes("FILTER")) {
            alert.show(GENERATE_INVALID_INPUT);
          } else if ((message as string).includes("과도한")) {
            alert.show(message);
          } else {
            alert.show(SERVER_ERROR_MESSAGE);
          }
        } else {
          alert.show(DEFAULT_ERROR_MESSAGE);
        }

        removeRerollTempMessage(tempRerollMessage._id);
      }

      chatPreviousMessage.init();
      manageLoading.stopLoading();
    }
  };

  const regenerateBardChat = async (input: string, _chatId?: string) => {
    const tempRerollMessage = loadingMessageTemplate2(
      "loading-invisible-regenerateBardChat",
      ""
    );
    manageLoading.startRerollLoading({ ...tempRerollMessage, type: "Message" });

    const res = await postGenerateBard_Id({
      chatId: _chatId || chatId,
      message: input,
      user: user,
      reroll: true,
    });

    if (res?.status === 201) {
      changeMessage([res.data.data], tempRerollMessage._id);
      const content = res.data.data?.content;

      tagManager({
        event: "generate_done",
        data: {
          feature_menu: "bard",
          generate_position: "chat",
          re_generate: false,
          generate_character: content?.replace(/ /g, "")?.length,
        },
      });
      collectEvent("generate_done", {
        feature_menu: "bard",
        generate_position: "chat",
        generate_character: content?.replace(/ /g, "")?.length,
      });

      await putUser({ data: { isNewbie: false } });
      await fetchChat();
    } else {
      if (res.status === 401) {
        loginDialog.handleOpen();
      }
      if (res.status === 403) {
        banAbuseModal.open();
      }
      if (res.status === 444) {
        openRecaptcha();
      }
      if (res.status === 500) {
        alert.show(GENERATE_FAIL);
      }

      setInput(input);
      manageLoading.removeLoading(tempRerollMessage._id);
    }

    chatPreviousMessage.init();
    manageLoading.stopLoading();
  };

  const generateBardChat = async (input: string, _chatId?: string) => {
    const userLoadingId = manageLoading.startLoading(
      "loading-user-message-generateChat",
      loadingMessageTemplate(_chatId || chatId, input)
    );
    const assistantLoadingId = manageLoading.startLoading(
      "loading-assistant-message-generateChat",
      loadingMessageTemplate2(_chatId || chatId, input)
    );

    const res = await postGenerateBard_Id({
      chatId: _chatId || chatId,
      message: input,
      user: user,
      reroll: false,
    });

    if (res?.status === 201) {
      changeMessage([res.data.data], assistantLoadingId);
      const content = res.data.data?.content;

      tagManager({
        event: "generate_done",
        data: {
          feature_menu: "bard",
          generate_position: "chat",
          re_generate: false,
          generate_character: content?.replace(/ /g, "")?.length,
        },
      });
      collectEvent("generate_done", {
        feature_menu: "bard",
        generate_position: "chat",
        generate_character: content?.replace(/ /g, "")?.length,
      });

      await putUser({ data: { isNewbie: false } });
      await fetchChat();
    } else {
      if (res.status === 401) {
        loginDialog.handleOpen();
      }
      if (res.status === 403) {
        banAbuseModal.open();
      }
      if (res.status === 444) {
        openRecaptcha();
      }
      if (res.status === 500) {
        if(!res.data.message.includes("과도한")) alert.show(GENERATE_FAIL);
      }

      setInput(input);
      manageLoading.removeLoading(userLoadingId);
      manageLoading.removeLoading(assistantLoadingId);
    }

    chatPreviousMessage.init();
    manageLoading.stopLoading();
  };

  const generateStreamChat = (input: string, _chatId?: string) => {
    const userLoadingId = manageLoading.startLoading(
      "loading-user-message-generateChat",
      loadingMessageTemplate(_chatId || chatId, input)
    );
    const assistantLoadingId = manageLoading.startLoading(
      "loading-assistant-message-generateChat",
      loadingMessageTemplate2(_chatId || chatId, input)
    );

    const controller = new AbortController();
    const platform = getPlatform();
    customFetch(
      `/stream2/${_chatId || chatId}?type=big&model=${
        chatToggle === 0 ? "GPT3.5" : "GPT4"
      }&platform=${platform ?? "web"}`,
      {
        method: "POST",
        body: JSON.stringify({ message: input, reroll: false }),
        signal: controller.signal,
      }
    )
      .then(async (res) => {
        const stream = res.body?.getReader();
        if (!stream) return;

        return await handleStream(
          stream,
          changeStreamMessage,
          updateCompleteMessage
        );
      })
      .catch((err) => updateCompleteMessage(null, err));

    return controller;

    function updateCompleteMessage(message: messageType, errorObj: any) {
      if (message && !errorObj) {
        updateAllAfterChat(message, assistantLoadingId);
      }

      if (errorObj) {
        const { status, message } = errorObj;
        if (status === 401) {
          loginDialog.handleOpen();
        } else if (status === 403) {
          banAbuseModal.open();
        } else if (status === 429) {
          alert.show(GENERATE_FAIL_TOO_MANY_REQUEST);
        } else if (status === 444) {
          openRecaptcha();
        } else if (status === 500) {
          if ((message as string).includes("GPT")) {
            alert.show(GENERATE_FAIL);
          } else if ((message as string).includes("SD")) {
            alert.show(GENERATE_FAIL);
          } else if ((message as string).includes("FILTER")) {
            alert.show(GENERATE_INVALID_INPUT);
          } else if ((message as string).includes("과도한")) {
            alert.show(message);
          } else {
            alert.show(SERVER_ERROR_MESSAGE);
          }
        } else if (status === 503) {
          alert.show(SERVICE_UNAVAILABLE_MESSAGE);
        } else {
          alert.show(DEFAULT_ERROR_MESSAGE);
        }

        setInput(input);
        manageLoading.removeLoading(userLoadingId);
        manageLoading.removeLoading(assistantLoadingId);
      }

      chatPreviousMessage.init();
      manageLoading.stopLoading();
    }
  };

  const generateSearchChat = async (input: string, _chatId?: string) => {
    const userLoadingId = manageLoading.startLoading(
      "loading-user-search-message",
      {
        ...loadingMessageTemplate(_chatId || chatId, input),
        type: "PluginInput",
        pluginName: "search",
      }
    );

    const assistantLoadingId = manageLoading.startLoading(
      "loading-assistant-search-message",
      { ...loadingMessageTemplate2(_chatId || chatId), type: "PluginOutput" }
    );

    const res = await postGenerateChat_Id({
      chatId: _chatId || chatId,
      message: input,
      user: user,
    });

    if (res?.status === 200 || res?.status === 201) {
      changeMessage([res.data.data], assistantLoadingId);
      const content = res.data.data?.content;

      tagManager({
        event: "generate_done",
        data: {
          feature_menu: "search",
          generate_position: "chat",
          re_generate: false,
          generate_character: content?.replace(/ /g, "")?.length,
        },
      });
      collectEvent("generate_done", {
        feature_menu: "search",
        generate_position: "chat",
        generate_character: content?.replace(/ /g, "")?.length,
      });

      await putUser({ data: { isNewbie: false } });
      await fetchChat();
    } else {
      if (res.status === 401) {
        loginDialog.handleOpen();
      }
      if (res.status === 403) {
        banAbuseModal.open();
      }
      if (res.status === 444) {
        openRecaptcha();
      }
      if (res.status === 500) {
        if(!res.data.message.includes("과도한")) alert.show(GENERATE_FAIL);
      }

      setInput(input);
      manageLoading.removeLoading(userLoadingId);
      manageLoading.removeLoading(assistantLoadingId);
    }

    chatPreviousMessage.init();
    manageLoading.stopLoading();
  };

  const updateAllAfterChat = async (
    _message: messageType,
    assistantLoadingId?: string,
    tempRerollMessage?: messageType
  ) => {
    const { content } = _message;
    const isImageType = _message.hasOwnProperty("imageId");

    const message = {
      ...loadingMessageTemplate2("loading-temp", content),
      ..._message,
    };
    if (assistantLoadingId) changeMessage([message], assistantLoadingId);

    collectUserProperties_once("first_generate_date", new Date());
    collectUserProperties_increment("count_generate_accum", 1);
    collectUserProperties("last_generate_date", new Date());
    collectUserProperties_increment(
      "amount_generate_accum",
      content?.replace(/ /g, "")?.length
    );

    tagManager({
      event: "generate_done",
      data: {
        feature_menu: isImageType ? "image" : "chat",
        generate_position: "chat",
        tool_open_position: chatInputToolSource,
        re_generate: false,
        feature_category: "",
        feature_name: "",
        feature_tag: "",
        feature_form: "",
        prompt_id: [],
        generate_character: content?.replace(/ /g, "")?.length,
        generate_token: "", // TODO: generate_token
        generate_model: "", // TODO: generate model
        input_tone_and_manner: "",
        input_category: "",
        input_continue_option: "",
        input_style: "",
        input_ratio: "",
      },
    });
    collectEvent("generate_done", {
      feature_menu: isImageType ? "image" : "chat",
      generate_position: "chat",
      tool_open_position: chatInputToolSource,
      re_generate: false,
      feature_category: "",
      feature_name: "",
      feature_tag: "",
      feature_form: "",
      prompt_id: [],
      generate_character: content?.replace(/ /g, "")?.length,
      generate_token: "", // TODO: generate_token
      generate_model: "", // TODO: generate model
      input_tone_and_manner: "",
      input_category: "",
      input_continue_option: "",
      input_style: "",
      input_ratio: "",
    });
    await putUser({
      data: {
        isNewbie: false,
      },
    });
    if (tempRerollMessage) {
      replaceRerollTempMessage(tempRerollMessage._id, message);
      chatPreviousMessage.init();
    }
    await fetchChat();
  };

  return {
    generateSearchChat,
    regenerateChat,
    generateStreamChat,
    generateBardChat,
    regenerateBardChat,
  };
};

export default useChatGenerate;
