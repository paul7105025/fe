import {
  chatInputToolSourceState,
  chatInputTypeState,
  currentToolChatIdState,
  postChatReroll,
  putUser,
  useCount,
  useEvent,
  useForms,
  useGenerate,
  useModalOpen,
  useRefresh,
  userState,
  useTagManager,
  useTriggerGenerateCount,
} from "@wrtn/core";
import React from "react";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { chatChipTypeState, chatIdState } from "src/stores";
import { useChatPreviousMessage } from "../useChatLastMessage";
import { useChatLoading } from "../useChatLoading";
import { useHandleAllMessage } from "./useHandleAllMessage";

const toolLoadingMessage = (
  content: string,
  role: "user" | "assistant",
  toolId: string
) => ({
  content: content,
  createdAt: "",
  disliked: false,
  liked: false,
  role: role,
  updatedAt: "",
  _id: "",
  type: "",
  toolId: toolId,
});
// chat에서 tool 생성하는 훅. 기존 useGenerate 로직을 공유합니다.
export const useChatToolGenerate = ({ tool }) => {
  const user = useRecoilValue(userState);
  const [chatId] = useRecoilState(chatIdState);
  const toolId = useRecoilValue(currentToolChatIdState);
  const chatInputType = useRecoilValue(chatInputTypeState);
  const manageLoading = useChatLoading();
  const chatPreviousMessage = useChatPreviousMessage();
  const { fetchChat } = useRefresh();

  const { rerollMessage, changeMessage } = useHandleAllMessage();

  const [output, _setOutput] = React.useState(null);
  const chatInputToolSource = useRecoilValue(chatInputToolSourceState);

  const { forms, isDisabled, updateForms, exampleForms, resetForms } = useForms(
    {
      tool,
      kind: "tool",
    }
  );
  const {
    count,
    reset: resetCount,
    increment: incrementCount,
    decrement: decrementCount,
  } = useCount(toolId);
  const { handleIncreaseCount, currentCount } = useTriggerGenerateCount(
    toolId,
    "tool"
  );

  const setOutput = (callback) => {
    const value = callback([])[0];
    if (value) _setOutput(value);
  };

  const { handleGenerateAtChat } = useGenerate({
    forms,
    setOutput,
    tool,
  });
  const blockChatGPT4LimitModal = useModalOpen({
    modalId: "blockChatGPT4Limit",
  });
  const {
    collectEvent,
    collectUserProperties,
    collectUserProperties_increment,
    collectUserProperties_once,
  } = useEvent();

  React.useEffect(() => {
    if (chatInputType === "default") resetCount();
  }, [chatInputType]);

  const tempTxt = React.useMemo(() => {
    let ret = "";
    forms.map((v) => {
      ret += "**" + v.label + "**";
      ret += "\n";
      ret += v.value;
      ret += "\n";
    });
    return ret;
  }, [forms]);

  const tagManager = useTagManager();

  // 생성 - 툴 // 1. 채팅 생성 버튼을 누를 때
  const generateTool = async (_chatId?: string) => {
    if (chatPreviousMessage.isSameWithPrevious(forms)) {
      return await regenerateTool(_chatId);
    }

    const userLoadingId = manageLoading.startLoading(
      "loading-user-ToolInput-generateTool",
      toolLoadingMessage(tempTxt, "user", toolId)
    );
    const assistantLoadingId = manageLoading.startLoading(
      "loading-assistant-ToolOutput-generateTool",
      toolLoadingMessage(tempTxt, "assistant", toolId)
    );

    const tempForms = [...formData.forms];
    const tempToolId = toolId;
    const res = await handleGenerateAtChat({
      count,
      toolId,
      type: "chat",
      id: _chatId || chatId,
    });

    if (res && res.length > 0) {
      collectUserProperties_once("first_generate_date", new Date());
      collectUserProperties_increment("count_generate_accum", 1);
      collectUserProperties("last_generate_date", new Date());
      let contentLength = 0;
      res.forEach(
        (v) => (contentLength += v.content?.replace(/ /g, "")?.length)
      );
      collectUserProperties_increment("amount_generate_accum", contentLength);
      // TODO: tool갱신해야함
      if (tool) {
        tagManager({
          event: "generate_done",
          data: {
            feature_menu: "tool",
            generate_position: "chat",
            tool_open_position: chatInputToolSource,
            re_generate: false,
            feature_category: tool.category,
            feature_name: tool.name,
            feature_tag: tool.tag,
            feature_form: tool.kind,
            prompt_id: [],
            generate_character: contentLength,
            generate_token: "", // TODO: generate_token
            generate_model: "", // TODO: generate model
            input_tone_and_manner: "",
            input_category: "",
            input_continue_option: "",
            input_style: "",
            input_ratio: "",
          },
        });
        collectEvent("generate_done", {
          feature_menu: "tool",
          generate_position: "chat",
          tool_open_position: chatInputToolSource,
          re_generate: false,
          feature_category: tool.category,
          feature_name: tool.name,
          feature_tag: tool.tag,
          feature_form: tool.kind,
          prompt_id: [],
          generate_character: contentLength,
          generate_token: "", // TODO: generate_token
          generate_model: "", // TODO: generate model
          input_tone_and_manner: "",
          input_category: "",
          input_continue_option: "",
          input_style: "",
          input_ratio: "",
        });
        await putUser({
          data: {
            isNewbie: false,
          },
        });
      }
      changeMessage(res, assistantLoadingId);

      chatPreviousMessage.updatePreviousMessage({
        forms: tempForms,
        toolId: tempToolId,
      });
      await fetchChat();
      manageLoading.stopLoading();
      return true;
    } else {
      manageLoading.removeLoading(userLoadingId);
      manageLoading.removeLoading(assistantLoadingId);
      manageLoading.stopLoading();
      return false;
    }
  };

  // 다시 생성 - 툴
  const regenerateTool = async (_chatId?: string) => {
    const ret = new Array(0);
    for (let i = 0; i < count; i++) {
      const invisibleId = manageLoading.startLoading(
        "loading-invisible-regenerateTool"
      );

      const res = await postChatReroll({
        chatId: _chatId || chatId,
        user: user
      });
      if (res?.status === 200 || res?.status === 201) {
        collectUserProperties_once("first_generate_date", new Date());
        collectUserProperties_increment("count_generate_accum", 1);
        collectUserProperties("last_generate_date", new Date());
        collectUserProperties_increment(
          "amount_generate_accum",
          res?.data?.data.content?.replace(/ /g, "")?.length
        );

        // TODO: tool 정보 읽어와ㅑㅇ함
        if (tool) {
          tagManager({
            event: "generate_done",
            data: {
              feature_menu: "tool",
              generate_position: "chat",
              tool_open_position: chatInputToolSource,
              re_generate: true,
              feature_category: tool.category,
              feature_name: tool.name,
              feature_tag: tool.tag,
              feature_form: tool.kind,
              prompt_id: [],
              generate_character: res?.data?.data.content?.replace(/ /g, "")
                ?.length,
              generate_token: "", // TODO: generate_token
              generate_model: "", // TODO: generate model
              input_tone_and_manner: "",
              input_category: "",
              input_continue_option: "",
              input_style: "",
              input_ratio: "",
            },
          });
          collectEvent("generate_done", {
            feature_menu: "tool",
            generate_position: "chat",
            tool_open_position: chatInputToolSource,
            re_generate: true,
            feature_category: tool.category,
            feature_name: tool.name,
            feature_tag: tool.tag,
            feature_form: tool.kind,
            prompt_id: [],
            generate_character: res?.data?.data.content?.replace(/ /g, "")
              ?.length,
            generate_token: "", // TODO: generate_token
            generate_model: "", // TODO: generate model
            input_tone_and_manner: "",
            input_category: "",
            input_continue_option: "",
            input_style: "",
            input_ratio: "",
          });
          await putUser({
            data: {
              isNewbie: false,
            },
          });
        }
        manageLoading.removeLoading(invisibleId);
        rerollMessage(res.data.data);
        await fetchChat();
        ret.push(true);
      } else {
        if (res?.status === 403) {
          blockChatGPT4LimitModal.open();
        }
        manageLoading.removeLoading(invisibleId);
        ret.push(false);
      }
    }
    manageLoading.stopLoading();

    return !ret.find((v) => !v);
  };

  const formData = {
    forms,
    isDisabled,
    updateForms,
    exampleForms,
    resetForms,
    count,
    incrementCount,
    decrementCount,
  };

  return {
    generateTool,
    regenerateTool,
    formData,
  };
};
export default useChatToolGenerate;
