import React from "react";
import { useRecoilState, useRecoilValue } from "recoil";

import {
  chipSuggestOpenState,
  getChipList,
  messageType,
  postGenerateChip_Command,
  allMessagesState,
  GENERATE_FAIL,
  useEvent,
  useRefresh,
  useCustomFetch,
  handleStream,
  GENERATE_INVALID_INPUT,
  postGenerateChat_Id,
  putUser,
  userState,
  chatInputTypeState,
  useReCaptcha,
} from "@wrtn/core";

import { useAlert } from "react-alert";

import { chatChipListState, chatChipTypeState, chatIdState } from "src/stores";
import { useChatLoading } from "../useChatLoading";
import { useHandleAllMessage } from "../chatGenerate";

import { getPlatform } from "@wrtn/core/utils/userAgent";

import {
  exampleListForSearch,
  exampleListDefault,
  suggestType,
} from "src/constants/suggest";

const loadingMessageTemplate = (id: string, content: string): messageType => ({
  content: content,
  createdAt: "",
  disliked: false,
  liked: false,
  role: "user",
  updatedAt: "",
  _id: id,
  type: "",
  toolId: "",
});

export const useChipList = () => {
  const chatId = useRecoilValue(chatIdState);
  const allMessage = useRecoilValue(allMessagesState);
  const chatInputType = useRecoilValue(chatInputTypeState);

  const allMessageLength = allMessage.length;
  const lastMessage = allMessage.at(-1);

  const [chatChipList, setChatChipList] = useRecoilState(chatChipListState);
  const [isChipSuggestOpen, setIsChipSuggestOpen] =
    useRecoilState(chipSuggestOpenState);

  const changeChipList = (message: messageType) => {
    if (message.hasOwnProperty("dynamicChipList") && message.dynamicChipList) {
      const newChips = message.dynamicChipList.map((chip, i) => ({
        name: chip,
        id: i.toString(),
        dynamic: true,
      }));

      setChatChipList(newChips);

      if (newChips.length > 0) {
        setIsChipSuggestOpen(true);
      } else {
        setIsChipSuggestOpen(false);
      }
    } else {
      setChatChipList([]);
      setIsChipSuggestOpen(false);
    }
  };

  React.useEffect(() => {
    if (chatId) {
      setChatChipList([]);
      setIsChipSuggestOpen(false);
    }
  }, [chatId]);

  // React.useEffect(() => {
  //   if (chatInputType === "default") {
  //     console.log("open");
  //     setIsChipSuggestOpen(true);
  //   } else {
  //     console.log("close");
  //     setIsChipSuggestOpen(false);
  //   }
  // }, [chatInputType, setIsChipSuggestOpen]);

  React.useEffect(() => {
    if (allMessageLength === 0) {
      setChatChipList([]);
      setIsChipSuggestOpen(false);
    } else if (allMessageLength === 1) {
      // console.log(lastMessage);
      if (!Array.isArray(lastMessage) && lastMessage.type === "FileInput") {
        setChatChipList([]);
        setIsChipSuggestOpen(false);
        return;
      }

      setChatChipList(
        chatInputType === "search" ? exampleListForSearch : exampleListDefault
      );
      setIsChipSuggestOpen(true);
    } else {
      if (Array.isArray(lastMessage)) {
        changeChipList(lastMessage[0]);
      } else {
        changeChipList(lastMessage);
      }

      if (chatInputType === "search") {
        setIsChipSuggestOpen(false);
      }
    }
  }, [chatInputType, lastMessage, allMessageLength]);
};

export const useChatSuggest = () => {
  const user = useRecoilValue(userState);
  const chatId = useRecoilValue(chatIdState);
  const allMessage = useRecoilValue(allMessagesState);

  const [isChipSuggestOpen, setIsChipSuggestOpen] =
    useRecoilState(chipSuggestOpenState);
  const [chatChipList, setChatChipList] = useRecoilState(chatChipListState);
  const [chatChipType, setChatChipType] = useRecoilState(chatChipTypeState);

  const { fetchChat } = useRefresh();
  const manageLoading = useChatLoading();
  const { changeMessage, changeStreamMessage } = useHandleAllMessage();

  const { collectEvent } = useEvent();

  const { customFetch } = useCustomFetch();

  const alert = useAlert();

  const { openRecaptcha } = useReCaptcha();

  // search 모드
  const handleClickSearchChip = async (
    suggest: suggestType,
    _chatId?: string
  ) => {
    const chipMessage = suggest.name.replace("!", "");

    const userLoadingId = manageLoading.startLoading(
      "loading-user-search-message",
      {
        ...loadingMessageTemplate(_chatId || chatId, chipMessage),
        type: "PluginInput",
        pluginName: "search",
      }
    );

    const assistantLoadingId = manageLoading.startLoading(
      "loading-assistant-search-message",
      {
        ...loadingMessageTemplate(_chatId || chatId, ""),
        type: "PluginOutput",
        role: "assistant",
      }
    );

    collectEvent("click_commend_chip", {
      commend_chip_type: "search",
      commend_chip_name: chipMessage,
    });

    const res = await postGenerateChat_Id({
      chatId: _chatId || chatId,
      message: chipMessage,
      user: user,
    });

    if (res?.status === 200 || res?.status === 201) {
      changeMessage([res.data.data], assistantLoadingId);
      // TODO: mixpanel 작업
      collectEvent("generate_done", {
        feature_menu: "chat",
        generate_position: "chat",
        is_command_chip: true,
        generate_character: res.data.data.content?.replace(/ /g, "")?.length,
      });
      await putUser({ data: { isNewbie: false } });
      await fetchChat();
    } else {
      if (res?.status !== 401) {
        alert.show(GENERATE_FAIL);
      }
      if (res?.status === 444) {
        openRecaptcha();
      }

      manageLoading.removeLoading(userLoadingId);
      manageLoading.removeLoading(assistantLoadingId);
    }

    manageLoading.stopLoading();
  };

  // Default 모드
  const handleClickChip = async (
    suggest: suggestType,
    callback?: Function,
    _chatId?: string
  ) => {
    closeSuggest();

    const chipType = suggest.dynamic ? "dynamic" : "default";

    if (chatChipType === "tool") {
      // 참고: 현재 칩이 모두 Message에 대한 칩으로 구성되어있어서 임의로 타입을 Message로 바꿔놓았음
      const chipUserSuggestId = manageLoading.startLoading(
        "loading-user-message-handleClickChip",
        { ...loadingMessageTemplate(chatId, suggest.name), type: "Message" }
      );
      const chipAssistantSuggestId = manageLoading.startLoading(
        "loading-assistant-message-handleClickChip",
        {
          ...loadingMessageTemplate(chatId, ""),
          role: "assistant",
          type: "Message",
        }
      );

      collectEvent("open_tool_in_chat", {
        open_method: "command_chip",
      });
      collectEvent("click_commend_chip", {
        commend_chip_type:
          chipType === "default" ? "first_chip_general" : "dynamic_chip",
        commend_chip_name: suggest.name,
      });

      const controller = new AbortController();
      const platform = getPlatform();
      customFetch(
        `/stream2/${_chatId || chatId}?type=big&model=GPT3.5&platform=${
          platform ?? "web"
        }`,
        {
          method: "POST",
          body: JSON.stringify({
            message: suggest.name,
            reroll: false,
            chip: !suggest.dynamic,
          }),
          signal: controller.signal,
        }
      ).then(async (res) => {
        const stream = res.body?.getReader();
        if (!stream) return;

        return await handleStream(
          stream,
          changeStreamMessage,
          (_message: messageType, errorObj: any) => {
            if (_message) {
              const message = {
                ...loadingMessageTemplate("loading-temp", _message.content),
                role: "assistant",
                type: "Message",
                ..._message,
              };
              changeMessage([message], chipAssistantSuggestId);

              collectEvent("generate_done", {
                feature_menu: "chat",
                generate_position: "chat",
                is_command_chip: true,
                generate_character: message.content?.replace(/ /g, "")?.length,
              });

              void fetchChat();
            } else {
              const { status, message } = errorObj;

              if (status === 444) {
                openRecaptcha();
              }

              if (status === 500) {
                if ((message as string).includes("GPT")) {
                  alert.show(GENERATE_FAIL);
                }
                if ((message as string).includes("SD")) {
                  alert.show(GENERATE_FAIL);
                }
                if ((message as string).includes("FILTER")) {
                  alert.show(GENERATE_INVALID_INPUT);
                }
              }

              manageLoading.removeLoading(chipUserSuggestId);
              manageLoading.removeLoading(chipAssistantSuggestId);
            }
            manageLoading.stopLoading();
          }
        );
      });

      return controller;
    }

    if (chatChipType === "commend") {
      const chipUserSuggestId = manageLoading.startLoading(
        "loading-user-message-handleClickChip",
        loadingMessageTemplate(chatId, suggest.name)
      );
      const chipAssistantSuggestId = manageLoading.startLoading(
        "loading-assistant-message-handleClickChip"
      );
      collectEvent("click_commend_chip", {
        commend_chip_type: "prompt",
        commend_chip_name: suggest.name,
      });
      const lastMessage = allMessage.at(-1);
      const res = await postGenerateChip_Command({
        chatId,
        command: suggest.name,
        historyId: Array.isArray(lastMessage)
          ? lastMessage[0].historyId
          : lastMessage.historyId,
        user: user,
      });
      if (res?.status === 200 || res?.status === 201) {
        changeMessage(res.data.data, chipAssistantSuggestId);
        collectEvent("generate_done", {
          feature_menu: "chat",
          generate_position: "chat",
          is_command_chip: true,
          generate_character: res.data.data.content?.replace(/ /g, "")?.length,
        });

        if (callback) callback();
      } else {
        if (res.status === 400) {
          alert.removeAll();
          alert.show(GENERATE_FAIL);
        }
        if (res.status === 444) {
          openRecaptcha();
        }
        manageLoading.removeLoading(chipUserSuggestId);
        manageLoading.removeLoading(chipAssistantSuggestId);
      }
      manageLoading.stopLoading();
    }
  };

  // 명령칩
  const showToolSuggest = async ({ toolId }) => {
    console.log("show");
    // setIsChipSuggestOpen(true);
    setChatChipType("commend");
    setChatChipList([]);
    const res = await getChipList({ chatId, toolId });
    if (res?.status === 200) {
      if (res?.data.data.length === 0) {
        setIsChipSuggestOpen(false);
        setChatChipList([]);
        setChatChipType("tool");
        return;
      }
      setChatChipList(
        res?.data.data.map((v: string, i: number) => {
          return {
            name: v,
            id: i.toString(),
            dynamic: false,
          };
        })
      );
    } else {
      if (res?.status === 444) {
        openRecaptcha();
      }
      setIsChipSuggestOpen(false);
      setChatChipType("tool");
      setChatChipList([]);
    }
  };

  // 명령칩 / 툴칩
  const closeSuggest = () => {
    setIsChipSuggestOpen(false);
  };

  return {
    isShow: isChipSuggestOpen,
    chatChipList,
    handleClickChip,
    handleClickSearchChip,
    showToolSuggest,
    closeSuggest,
  };
};

export default useChatSuggest;
