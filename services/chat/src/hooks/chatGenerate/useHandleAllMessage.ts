import { nanoid } from "nanoid";
import { useRecoilState } from "recoil";

import { allMessagesState, messageType, streamMessageState } from "@wrtn/core";
import { changeUUID, addUUID } from "src/utils";

export const useHandleAllMessage = () => {
  const [allMessages, setAllMessages] = useRecoilState(allMessagesState);
  const [streamMessage, setStreamMessage] = useRecoilState(streamMessageState);

  const resetMessages = () => {
    setAllMessages([]);
  };

  const initMessages = (messages: Array<messageType | Array<messageType>>) => {
    setAllMessages(changeUUID(messages));
  };

  const addMessage = (message: messageType | Array<messageType>) => {
    const newNanoId = nanoid(10);

    setAllMessages((res) => [...res, addUUID(message, newNanoId)]);

    return newNanoId;
  };

  const loadMoreMessages = (
    messages: Array<messageType | Array<messageType>>
  ) => {
    if (messages.length < 1) return;
    setAllMessages((res) => [...changeUUID(messages), ...res]);
  };

  const changeMessage = (
    message: messageType | Array<messageType>,
    uuid: string
  ) => {
    setAllMessages((res) =>
      res.map((v) => {
        if (Array.isArray(v)) {
          if (v[0].uuid === uuid) {
            return addUUID(message, uuid);
          }
        } else {
          if (v.uuid === uuid) {
            return addUUID(message, uuid);
          }
        }
        return v;
      })
    );
  };

  const replaceRerollTempMessage = (tempId: string, realMessage: messageType) => {
    setAllMessages((res) => 
      res.map((v, i) => {
        const isLastMessage = i === res.length - 1;
        if (!isLastMessage) return v;

        if (Array.isArray(v)) {
          const messagesWithoutTemp = v.filter(msg => msg._id !== tempId);

          const newRealMessage = addUUID(realMessage, v[0].uuid);
          if (Array.isArray(newRealMessage)) {
            return [...newRealMessage, ...messagesWithoutTemp];
          } else {
            return [newRealMessage, ...messagesWithoutTemp]
          }
        } else {
          return v;
        }
      })
    )
  }

  const removeRerollTempMessage = (tempId: string) => {
    setAllMessages((res) => (
      res.map((v, i) => {
        const isLastMessage = i === res.length - 1;
        if (!isLastMessage) return v;

        if (Array.isArray(v)) {
          return v.filter(msg => msg._id !== tempId);
        } else {
          return v;
        }
      })
    ))
  }

  const rerollMessage = (message: messageType | Array<messageType>) => {
    setAllMessages((res) =>
      res.map((v, i) => {
        if (i !== res.length - 1) return v;
        if (Array.isArray(v)) {
          const newMessages = addUUID(message, v[0].uuid);

          if (Array.isArray(newMessages)) {
            return [...newMessages, ...v];
          } else {
            return [newMessages, ...v];
          }
        } else {
          const newMessages = addUUID(message, v.uuid);

          if (Array.isArray(newMessages)) {
            return [...newMessages, v];
          } else {
            return [newMessages, v];
          }
        }
      })
    );
  };

  const removeMessage = (uuid: string) => {
    setAllMessages((res) =>
      res.filter((v) =>
        Array.isArray(v) ? v[0].uuid !== uuid : v.uuid !== uuid
      )
    );
  };

  return {
    allMessages,
    resetMessages,
    initMessages,
    loadMoreMessages,
    addMessage,
    changeMessage,
    rerollMessage,
    removeMessage,
    replaceRerollTempMessage,
    removeRerollTempMessage,
    changeStreamMessage: setStreamMessage,
  };
};
