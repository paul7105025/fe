import { useState } from "react";
import { useAlert } from "react-alert";
import { useRecoilValue } from "recoil"
import { postShareChat, useEvent, useIsMobile } from "@wrtn/core"
import { chatIdState } from "src/stores";

export const useShare = () => {
  const [showModal, setShowModal] = useState(false);
  const chatId = useRecoilValue(chatIdState);
  const alert = useAlert();
  const isMobile = useIsMobile();
  const { collectEvent } = useEvent();

  const handleShare = async () => {
    if (!chatId) return alert.show("해당 채팅이 없어요.")

    try {
      collectEvent("click_share_trend");
      const res = await postShareChat(chatId);

      const url = res.data.data;
      const baseurl = window.location.href;

      if (isMobile) {
        navigator.clipboard.writeText(`${baseurl}share/${url}`)
        window.location.href = `/share/${url}`;
      } else {
        await navigator.clipboard.writeText(`${baseurl}share/${url}`)
        window.open(`/share/${url}`, "_blank", "noopener,noreferrer");
      }

      collectEvent("share_trend_done", {
        share_trand_post_id: url,
      });
    } catch (err) {
      console.log(err);
      const message = err?.response?.data?.message || "오류가 발생했어요!";
      return alert.show(message)
    }
  }

  return {
    showModal,
    setShowModal,
    handleShare,
  }
}
