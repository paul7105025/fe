import React from "react";
import { useRecoilState, useSetRecoilState } from "recoil";

import {
  chatType,
  getChat_Id,
  currentChatState,
  chatLoadingState,
} from "@wrtn/core";
import { chatIdState, chatTrailState } from "src/stores";
import { useHandleAllMessage } from "./chatGenerate";

export const useFetchChat = () => {
  const [chatId, setChatId] = useRecoilState(chatIdState);

  const setChatTrail = useSetRecoilState(chatTrailState);
  const setCurrentChat = useSetRecoilState(currentChatState);
  const setChatLoading = useSetRecoilState(chatLoadingState);

  const { resetMessages, initMessages } = useHandleAllMessage();

  React.useEffect(() => {
    setCurrentChat(null);
    setChatTrail(true);

    resetMessages();
  }, [chatId]);

  const fetchChatById = async (id?: string) => {
    try {
      setChatLoading(true);
      if (id || chatId) {
        const res = await getChat_Id({ chatId: id || chatId });
        if (res.status === 200) {
          const { data }: { data: chatType } = res.data;
          setCurrentChat(data);
          setChatId(data._id);
          initMessages(data.messages);
        } else if (res.status === 404) {
          setChatId(null);
        }
      }
    } catch {
    } finally {
      setTimeout(() => {
        setChatLoading(false);
        setChatTrail(false);
      }, 100);
    }
  };

  return { fetchChatById };
};
