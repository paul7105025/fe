// 툴 챗일 경우 이전 form 정보가 없으면 다시 생성이 불가능하기 때문에 인메모리에 저장하는 역할을 합니다.

import { FormMetadataType } from "@wrtn/core";
import { useRecoilState, useResetRecoilState } from "recoil";
import { previousGeneratedToolChatState } from "src/stores";

// 서버 쪽에서 form 정보를 주지 않기 때문에 새로고침, 라우트 변경 등으로 인메모리가 초기화되면 다시 생성은 불가능합니다
export const useChatPreviousMessage = () => {
  const [previousToolForms, setPreviousToolForms] = useRecoilState(
    previousGeneratedToolChatState
  );

  const reset = useResetRecoilState(previousGeneratedToolChatState)

  const updatePreviousMessage = ({ forms, toolId }) => {
    setPreviousToolForms({
      toolId,
      forms,
    });
  };

  const previousForms = previousToolForms.forms;
  const previousToolId = previousToolForms.toolId;

  const isSameWithPrevious = (forms: FormMetadataType[]) => {
    if (!previousForms) return false;
    if (forms.length !== previousForms.length) return false;
    for (let i = 0; i < forms.length; i++) {
      if (
        forms[i].id !== previousForms[i].id ||
        forms[i].value !== previousForms[i].value
      )
        return false;
    }
    return true;
  };

  const init = () => {
    reset();
  }
  return {
    updatePreviousMessage,
    previousForms,
    previousToolId,
    isSameWithPrevious,
    init,
  };
};
