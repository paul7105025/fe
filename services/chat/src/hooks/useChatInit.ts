import { useRecoilState, useSetRecoilState } from "recoil";

import {
  blockChatLimitState,
  chatType,
  postChat,
  useEvent,
  useLoginDialog,
  useRefresh,
} from "@wrtn/core";

import { chatIdState } from "src/stores";

export const useChatInit = () => {
  const [chatId, setChatId] = useRecoilState(chatIdState);

  const setBlockChatLimit = useSetRecoilState(blockChatLimitState);

  const loginDialog = useLoginDialog();

  const { collectEvent } = useEvent();
  const { fetchChat } = useRefresh();

  const initChat = async (
    successCallback?: (data: chatType) => Promise<void>
  ) => {
    if (chatId) return; // 채팅방이 이미 존재함

    collectEvent("create_chat", {
      chat_type: "main",
    });
    const res = await postChat();
    if (res?.status === 201) {
      const { data }: { data: chatType } = res.data;
      setChatId(data._id);
      await fetchChat();
      if (successCallback) await successCallback(data);
      return data._id;
    } else if (res.status === 401) {
      loginDialog.handleOpen();
    } else if (res.status === 403) {
      setBlockChatLimit(true);
    }
  };

  return { initChat };
};
