import React from "react";
import { useRecoilState } from "recoil";

import { setLocal, getLocal } from "@wrtn/core";
import { chatTooltipTutorialState } from "src/stores";

export const useChatTutorial = (step: number) => {
  const [isActive, setIsActive] = useRecoilState(chatTooltipTutorialState);

  React.useEffect(() => {
    const isLocalActive =
      getLocal<[boolean, boolean, boolean]>("chat_tutorial");

    if (isLocalActive) {
      setIsActive(isLocalActive);
    } else {
      setIsActive([true, true, true]);
    }
  }, []);

  const currentActive = isActive[step];

  const handleActive = () => {
    setIsActive((res) => {
      const newActive = [...res];
      newActive[step] = false;

      setLocal("chat_tutorial", newActive);
      return newActive;
    });
  };

  const handleRemove = () => {
    const newActive = [...isActive];
    newActive[step] = false;

    setLocal("chat_tutorial", newActive);
  };

  return { currentActive, handleActive, handleRemove };
};
