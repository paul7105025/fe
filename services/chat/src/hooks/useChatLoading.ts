import { messageType, chatLoadingState } from "@wrtn/core";
import React from "react";

import { useRecoilState } from "recoil";
import { useHandleAllMessage } from "./chatGenerate";

const loadingMessageTemplate = (id: string): messageType => ({
  content: "",
  createdAt: "",
  disliked: false,
  liked: false,
  role: "user",
  updatedAt: "",
  _id: id,
  type: "ToolOutput",
  toolId: "",
});

// 로딩을 관리합니다.
export const useChatLoading = () => {
  const { addMessage, removeMessage, rerollMessage } = useHandleAllMessage();
  const [chatLoading, setChatLoading] = useRecoilState(chatLoadingState);

  const startLoading = (id: string, message?: messageType) => {
    setChatLoading(true);

    let tempMessage: messageType = { ...message, _id: id };

    return addMessage(message ? tempMessage : loadingMessageTemplate(id));
  };

  const startRerollLoading = (tempRerollMessage: messageType) => {
    setChatLoading(true)
    rerollMessage(tempRerollMessage);
  }

  const stopLoading = () => {
    setChatLoading(false);
  };

  const removeLoading = (id: string) => {
    removeMessage(id);
  };

  return {
    loading: chatLoading,
    startLoading,
    stopLoading,
    removeLoading,
    startRerollLoading,
  };
};
