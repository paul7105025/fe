import React from "react";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
  useFetchToolList,
  templateToolListState,
  useFavoriteIdList,
  currentToolChatIdState,
  getTemplate,
  useEvent,
  chatInputToolSourceState,
  chatInputToolSourceType,
  chatInputTypeState,
} from "@wrtn/core";

type chatHoverMenu = "tool" | "template";

const defaultList = [
  "63b2b5587b66829fa8483b89", // 긴블로그포스팅
  "63a94dec50d5e5e8a0040e98", // 영문이메일
  "6412b907270278c8665625cf", // 레포트목차
];

const defaultTemplateList = [
  // {
  //   "name": "첨삭",
  //   "content": `국어 선생님처럼 행동해주세요. 전달한 문장에서 맞춤법, 문법적 혹은 문장부호 오류가 있는지 검토해서 알려주세요. 검토가 끝나면 문장을 개선하기 위해 필요한 수정 사항이나 제안 사항에 대해 알려주세요. 전달한 문장을 다시 말하지 마세요. 첫번째 전달할 문장은 "{외않되}" 입니다.`,
  // },
  // {
  //   "name": "요리메뉴",
  //   "content": "내가 지금 당근, 애호박, 양파, 베이컨을 가지고 있는데 이걸로 할 수 있는 요리 목록을 간단한 설명과 함께 알려줘.",
  // },
  // {
  //   "name": "요약",
  //   "content": `[요약 규칙]\n단어를 낭비해서 사용하지 말 것\n짧고 명확하며, 완전한 문장을 사용할 것\n글머리 기호나 -(대시)를 사용하지 말 것\n능동적으로 작성할 것\n세부사항, 의미를 극대화할 것\n내용에 집중할 것\n\n[사용 금지]\n이 문서\n이 기사\n이 페이지\n이 자료\n이 작품\n[목록 끝]\n\n요약: 안녕, 만나서 반가워!\n+++++\n안녕!\n\n아래 문장을 요약하시오:\n{{$input}}\n+++++`,
  // },
  // {
  //   "name": "텍스트 게임",
  //   "content": `텍스트 기반 어드벤처 게임이라고 가정하고 게임을 진행해보자. 내가 명령어를 입력하면, 캐릭터가 보고 있는 걸 설명과 함께 나에게 알려줘야해. 하나의 코드 블럭 안에는 게임과 관련된 정보만 포함되어야 해. 내가 너에게 명령어를 입력하기 전에, 혼자서 게임을 진행하면 안돼. 내가 한국어로 된 답을 얻고 싶을 땐 { }처럼 중괄호 내부에 명령어를 입력할거야.\n내 첫번째 명령은 {일어나} 야.`,
  // },
];

export const useManageChatHoverMenu = () => {
  const { chatToolList } = useFetchToolList();
  const [templateToolList, setTemplateToolList] = useRecoilState(
    templateToolListState
  );
  const { favoriteIdList } = useFavoriteIdList();
  const { collectEvent } = useEvent();

  const setToolId = useSetRecoilState(currentToolChatIdState);
  const setChatInputType = useSetRecoilState(chatInputTypeState);
  const setChatInputToolSource = useSetRecoilState(chatInputToolSourceState);  

  const [menu, setMenu] = React.useState<chatHoverMenu>("tool");

  let toolList = React.useMemo(() => {
    let tempList =
      chatToolList
        ?.filter((tool) => [...favoriteIdList, ...defaultList].includes(tool._id))
        // .filter((v, i) => i <= 4) 
        || [];

    tempList =
      tempList.length > 0
        ? tempList
        : chatToolList.filter((tool) => defaultList.includes(tool._id));

    return tempList;
  }, [chatToolList, favoriteIdList]);

  const templateList = templateToolList.length > 0 ? templateToolList : defaultTemplateList;
  const list = menu === "tool" ? toolList : templateList;

  const handleChange = (val: chatHoverMenu) => {
    setMenu(val);
  };

  React.useEffect(() => {
    const fetchTemplateToolList = async () => {
      const res = await getTemplate();
      if (res?.status === 200) {
        setTemplateToolList(res?.data?.data || []);
      }
    };

    fetchTemplateToolList();
  }, []);

  const onClickToolMenu = (toolId: string, callback: Function, source: chatInputToolSourceType) => {
    setToolId(toolId);
    callback();
    setChatInputToolSource(source);
  };

  const onClickTemplate = (
    content: string,
    setInput: React.Dispatch<React.SetStateAction<string>>,
    callback: Function
  ) => {
    collectEvent("use_template", {
      use_template_method: "plus_button"
    });
    setInput((c) => c + content);
    setChatInputType("default");
    callback();
  };

  return {
    list,
    currentMenu: menu,
    handleChange,
    onClickToolMenu,
    onClickTemplate,
  };
};
