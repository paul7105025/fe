export type suggestType = {
  name: string;
  id: string;
  dynamic: boolean;
};

export const exampleListDefault: Array<suggestType> = [
  {
    name: "우주복을 입은 강아지 캐릭터를 그려줘.",
    id: "63b2b5587b66829fa8483b89",
    dynamic: false,
  },
  {
    name: "지금부터 너는 나의 심리상담사야. 요즘 내가 느끼는 스트레스를 듣고 적절한 조언을 해줘.",
    id: "6412b907270278c8665625cf",
    dynamic: false,
  },
  {
    name: `칸트와 데카르트 관점에서 각각 "있음"과 "존재"의 차이를 설명해줘.`,
    id: "6302e4fba4a7d28a2dc83153",
    dynamic: false,
  },
];

export const exampleListForSearch: Array<suggestType> = [
  {
    name: "!이번 주 한국야구 경기 결과 알려줘",
    id: "63b2b5587b66829fa8483b89",
    dynamic: false,
  },
  {
    name: "!오늘 서울 역삼동 날씨 알려줘",
    id: "6412b907270278c8665625cf",
    dynamic: false,
  },
  {
    name: "!이번달 반도체 수출 관련 뉴스 알려줘",
    id: "6302e4fba4a7d28a2dc83153",
    dynamic: false,
  },
];
