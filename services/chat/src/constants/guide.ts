export const chatEmptyGuide = [
  {
    type: 'block',
    title: "이렇게 써보세요",
    description: ["“소설을 써줘”", "“블로그 글 써줘”", "“다이어트 식단 짜줘”"],
  },
  {
    type: 'block',
    title: "이런 장점이 있어요",
    description: [
      "앞에서 입력한 말을 기억해요.",
      "구체적으로 쓸수록 잘 알아들어요",
      "수정방향을 알려주면 잘 알아들어요 ",
    ],
  },
  {
    type: 'block',
    title: "주의해 주세요",
    description: [
      "사실이 아닌 말을 출력할\n가능성이 있어요.",
      "2021년도까지의 정보만\n학습했어요.",
    ],
  },
];

export const chatInappGuide = [
  {
    type: 'block',
    title: "이렇게 써보세요",
    description: ["툴에서 결과물을 생성한 이후", "챗 뤼튼을 통해 가공해보세요."],
    sub_description: ["“문장 사이사이 이모지를 붙여줘”", "“존댓말로 바꿔줘”", "“조금 더 감성적으로 써줘”"],
  },
  {
    type: 'block',
    title: "이런 장점이 있어요",
    description: [
      "앞에서 입력한 말을 기억해요.",
      "구체적으로 쓸수록 잘 알아들어요",
      "피드백을 주면 더 잘 알아들어요 ",
    ],
    sub_description: [
      "“맞아, 정확해!“",
      "“이건 아니야. ~를 의미했어“",
    ],
  },
  {
    type: 'warning',
    for: "free",
    description: [
      "사실이 아닌 말을 출력할 가능성이 있으며, 2021년도까지의 정보만 학습했어요.",
      "무료 요금제의 경우 툴 페이지 내에서 한 달에 20회만 챗 뤼튼을 사용할 수 있으며 메인 메뉴의 챗 뤼튼은 무제한으로 사용할 수 있어요."
    ]
  },
  {
    type: 'warning',
    for: "paid",
    description: [
      "사실이 아닌 말을 출력할 가능성이 있으며, 2021년도까지의 정보만 학습했어요.",
    ]
  },
];

