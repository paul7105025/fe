const remotesComponentsName = [
  ["login", process.env.MODULE_FEDERATION_LOGIN_PATH]
];

/* You only need to edit the above variables. */
/* ########################################## */

const _remoteMap = new Map();
remotesComponentsName.forEach((v) => {
  _remoteMap.set(v[0], `${v[0]}@${v[1]}/remoteEntry.js`);
})
const remotes = Object.fromEntries(_remoteMap);
module.exports = remotes;