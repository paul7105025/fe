const exposeComponentsName = [
  "ToolPage",
  "ToolLNB",
  "ChatToolLNB"
];

/* You only need to edit the above variables. */
/* ########################################## */

const _exposeUrl = './src/exposes/';

const _exposesMap = new Map();
exposeComponentsName.forEach((v) => {
  _exposesMap.set("./" + v, `${_exposeUrl}${v}`)
})

const exposes = Object.fromEntries(_exposesMap);
module.exports = exposes;
