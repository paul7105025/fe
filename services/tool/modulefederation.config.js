const { dependencies } = require("./package.json");
const exposes = require("./config/exposes");
const remotes = require("./config/remotes");

module.exports = {
  name: "toolSingle",
  filename: "remoteEntry.js",
  exposes: exposes,
  remotes: remotes,
  shared: {
    react: {
      singleton: true,
      eager: true,
      requiredVersion: dependencies["react"],
    },
    "react-dom": {
      singleton: true,
      eager: true,
      requiredVersion: dependencies["react-dom"],
    },
    "react-router-dom": {
      singleton: true,
      eager: true,
      requiredVersion: dependencies["react-router-dom"],
    },
    recoil: {
      eager: true,
    },
    "styled-components": {
      singleton: true,
    },
    "react-alert": {
      singleton: true,
      eager: true,
    },
    "mixpanel-browser": {
      eager: true,
      singleton: true,
    },
  },
};
