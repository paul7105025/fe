import { atom } from "recoil";

export type bundleGenerateEventType = {
  bundleId: string | null;
  events: Array<string> | null;
};

export const bundleGenerateEventState = atom<bundleGenerateEventType>({
  key: "bundleGenerateEventState",
  default: {
    bundleId: null,
    events: null,
  },
});

export const bundleListState = atom({
  key: "bundleListState",
  default: [],
});
