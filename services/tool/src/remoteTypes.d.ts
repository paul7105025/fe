
declare module "login/LoginPage" {
  const LoginPage : React.ComponentType;

  export default LoginPage;
}

declare module "login/LoginFramework" {
  const LoginFramework : React.ComponentType | any;

  export default LoginFramework;
}

declare module "login/LoginMount" {
   const Mount: React.ComponentType | any;
   export default Mount;
}