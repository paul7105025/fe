import React from "react";

import { getLocal, setLocal } from "@wrtn/core";
import { dialogDataType, dialogLocalDataType } from "@wrtn/core";

type dialogDataArrayType = Array<dialogDataType>;
type dialogLocalDataArrayType = Array<dialogLocalDataType>;

export const useDialog = (dialogData: dialogDataArrayType) => {
  const [openDialog, setOpenDialog] =
    React.useState<dialogDataArrayType>(dialogData);

  const toggleDialog = (type: string, value: boolean) => {
    setOpenDialog((res) =>
      res.map((dialog) => {
        if (dialog.name === type) return { ...dialog, isOpen: value };
        else return dialog;
      })
    );
  };

  const closeDialogTemp = (dialog: dialogDataType) => {
    const { name } = dialog;
    toggleDialog(name, false);
  };

  const closeDialog = (dialog: dialogDataType) => {
    const { name } = dialog;

    toggleDialog(name, false);
    const localData = getLocal<dialogLocalDataArrayType>(`wrtn-image-event`);

    const newData = {
      name: name,
      dueDate: new Date(),
    };

    if (localData) {
      const targetLocalData = localData.find((data) => data.name === name);
      if (targetLocalData) {
        setLocal(`wrtn-image-event`, [
          ...localData.filter((data) => data.name !== name),
          newData,
        ]);
      } else {
        setLocal(`wrtn-image-event`, [...localData, newData]);
      }
    } else {
      setLocal(`wrtn-image-event`, [newData]);
    }
  };

  const fetchDataFromLocal = (
    targetLocalData: dialogLocalDataType | undefined,
    dialog: dialogDataType
  ) => {
    const currentTime = new Date();
    const currentPath = window.location.pathname;

    if (currentPath.match(dialog.path)) {
      if (targetLocalData) {
        const diffTime = Math.abs(
          currentTime.getTime() - new Date(targetLocalData.dueDate).getTime()
        );
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        if (diffDays > 1) {
          toggleDialog(dialog.name, true);
        } else {
          toggleDialog(dialog.name, false);
        }
      } else {
        toggleDialog(dialog.name, true);
      }
    }
  };

  React.useEffect(() => {
    const localData = getLocal<dialogLocalDataArrayType>(`wrtn-image-event`);

    openDialog.forEach((dialog) => {
      const targetLocalData = localData
        ? localData.find((data) => data.name === dialog.name) || undefined
        : undefined;

      fetchDataFromLocal(targetLocalData, dialog);
    });
  }, [window.location.pathname]);

  return {
    openDialog,
    closeDialogTemp,
    closeDialog,
  };
};
