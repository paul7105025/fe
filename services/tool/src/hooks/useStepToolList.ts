import React from "react";

import { getTool_Id, SingleToolType } from "@wrtn/core";

export const useStepToolList = ({ step }) => {
  const [loading, setLoading] = React.useState(false);
  const [stepToolList, setStepToolList] = React.useState<Array<SingleToolType>>(
    []
  );

  const fetchTools = async (toolIdList: string[]) => {
    setLoading(true);
    let newData: Array<SingleToolType> = [];

    for (let i = 0; i < toolIdList.length; i++) {
      const res = await getTool_Id({ toolId: toolIdList[i] });

      if (res?.status === 200) {
        const { data } = res.data;
        newData = [...newData, data];
      }
    }

    if (newData.length > 0) setStepToolList(newData);
    setLoading(false);
  };

  React.useEffect(() => {
    if (step && step.tools && step.tools.length > 0) fetchTools(step.tools);
  }, [step]);

  const stepArray = React.useMemo(
    () => [...stepToolList.map((tool) => tool.name), "완성본"],
    [stepToolList]
  );

  return { loading, stepToolList, stepArray };
};
