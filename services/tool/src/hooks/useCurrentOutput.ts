import _ from "lodash";
import React from "react";
import { OutputMetadata, TAB_ALL, TAB_FAVORITE } from "@wrtn/core";

interface useCurrentOutputProps {
  currentTab: string;
  likeList: Array<OutputMetadata>;
  outputList: Array<OutputMetadata>;
}

export const useCurrentOutput = ({
  currentTab,
  likeList,
  outputList,
}: useCurrentOutputProps) => {
  const output = React.useMemo(() => {
    if (currentTab === TAB_ALL) {
      return outputList;
    } else if (currentTab === TAB_FAVORITE) {
      let filteredOutputList = outputList.filter((e) => e.liked);

      let filteredLikeList = likeList.filter(
        (e) => outputList.findIndex((d) => d._id === e._id) === -1
      );

      const newOutputList = _.uniqBy(
        [...filteredOutputList, ...filteredLikeList],
        "_id"
      );

      return newOutputList;
    }
    return [];
  }, [currentTab, likeList, outputList]);

  return output;
};
