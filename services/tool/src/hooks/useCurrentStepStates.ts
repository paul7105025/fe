import React from "react";

import { SingleToolType, useTimeout } from "@wrtn/core";

interface useCurrentStepStatesProps {
  currentStep: number;
  stepToolList: Array<SingleToolType>;
  scrollToTop: () => void;
}

export const useCurrentStepStates = ({
  currentStep,
  stepToolList,
  scrollToTop,
}: useCurrentStepStatesProps) => {
  const toolIndex = React.useMemo(
    () =>
      currentStep >= stepToolList.length
        ? stepToolList.length - 1
        : currentStep,
    [currentStep, stepToolList]
  );

  const isResult = React.useMemo(() => {
    return currentStep >= stepToolList.length && currentStep !== 0;
  }, [currentStep, stepToolList]);

  useTimeout(
    () => {
      if (window.innerWidth < 1024 && typeof scrollToTop === "function") {
        scrollToTop();
      }
    },
    150,
    [currentStep]
  );

  return { toolIndex, isResult };
};
