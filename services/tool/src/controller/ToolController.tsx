import React from "react";

import { useForms, useOutputs, useGenerate, SingleToolType } from "@wrtn/core";

import { FormContainer } from "@wrtn/ui/containers/FormContainer";
import { OutputContainer } from "../containers/OutputContainer";
import { GenerateContainer } from "../containers/GenerateContainer";
import { ToolMetaContainer } from "../containers/ToolMetaContainer";

import { ToolControllerComposition } from "./ToolControllerComposition";

interface ToolControllerProps {
  tool: SingleToolType;
}

const ToolController = ({ tool }: ToolControllerProps) => {
  const kind = tool.kind;
  const toolId = React.useMemo(() => tool?._id, [tool]);

  const {
    forms,
    isDisabled,
    updateForms,
    exampleForms,
    resetForms,
    syncForms,
  } = useForms({
    tool,
    kind: "tool",
  });

  const { output, setOutput, handleDelete, handleUpdate } = useOutputs({
    toolId,
  });

  const { handleGenerate } = useGenerate({
    forms,
    setOutput,
    tool,
  });

  return (
    <React.Fragment>
      <ToolControllerComposition
        kind={kind}
        toolHeader={<ToolMetaContainer tool={tool} />}
        formContainer={
          <FormContainer
            forms={forms}
            exampleForms={exampleForms}
            resetForms={resetForms}
            updateForms={updateForms}
          />
        }
        generateContainer={
          <GenerateContainer
            tool={tool}
            kind={kind}
            isDisabled={isDisabled}
            handleGenerate={handleGenerate}
            parent={tool}
          />
        }
        outputContainer={
          <OutputContainer
            tool={tool}
            outputList={output}
            syncForms={syncForms}
            onDeleteOutput={handleDelete}
            onUpdateOutput={handleUpdate}
          />
        }
      />
    </React.Fragment>
  );
};

export default ToolController;
