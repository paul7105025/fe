import React from "react";
import styled, { css } from "styled-components";

import { Kind } from "@wrtn/core";

import { FlexWrapper, colors } from "@wrtn/ui";
import polyfill from "@wrtn/ui/styles/polyfill";

interface ToolControllerCompositionProps {
  kind: Kind;
  toolHeader?: React.ReactNode;
  step?: React.ReactNode;
  formContainer: React.ReactNode;
  generateContainer: React.ReactNode;
  outputContainer: React.ReactNode;
  outputExpandContainer?: React.ReactNode;
}

/**
 * @param {"tool" | "step" | "bundle"} props.kind
 * @param {React.Component} props.formContainer
 * @param {React.Component} props.generateContainer
 * @param {React.Component} props.outputContainer
 * @param {React.Component} props.outputExpandContainer
 * @param {React.Component} props.toolHeader
 * @param {React.Component} props.step
 * @returns
 */

export const ToolControllerComposition = ({
  kind,
  toolHeader,
  step,
  formContainer,
  generateContainer,
  outputContainer,
  outputExpandContainer,
}: ToolControllerCompositionProps) => {
  return (
    <React.Fragment>
      <ToolLeftWrapper>
        {toolHeader}
        {step}
        <ToolFormWrapper kind={kind}>
          {formContainer}
          {generateContainer}
        </ToolFormWrapper>
      </ToolLeftWrapper>
      <ToolResultWrapper kind={kind}>
        {outputContainer}
        {outputExpandContainer}
      </ToolResultWrapper>
    </React.Fragment>
  );
};

const ToolLeftWrapper = styled(FlexWrapper)`
  flex: 1;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  height: 100%;
  @media(max-width: 1279px) {
    height: initial;
  }
`;

export const ToolFormWrapper = styled(FlexWrapper)<{
  kind: Kind;
}>`
  position: relative;
  flex: 1;
  flex-direction: column;
  justify-content: flex-start;
  ${"" /* overflow: scroll; */}
  width: 100%;
  height: 100%;
  padding: 0px 28px 44px;
  
  overflow: scroll;
  position: relative;
  @media (max-width: 1279px) {
    height: auto;
    overflow: initial;
  }

  ${({ kind }) => styleSelector[kind]}
`;

const SingleToolStyle = css`
  padding: 0px 28px 44px;

  @media (max-width: 1279px) {
    padding: 0px 30px 56px;
  }
  @media (max-width: 480px) {
    padding: 0px 16px 56px;
  }
`;

const styleSelector = {
  tool: SingleToolStyle,
  bundle: SingleToolStyle,
  step: SingleToolStyle,
};

export const ToolResultWrapper = styled.div<{
  kind: Kind;
}>`
  flex: 1;
  background: ${colors.BACKGROUND};
  width: 100%;
  height: 100%;
  position: relative;

  /* ${({ kind }) => styleSelector[kind]} */

  @media (max-width: 1279px) {
    ${polyfill.dvh("height", 100)};
    /* height: calc(100dvh - 90px); */
  }
  @media (max-width: 480px) {
    max-height: calc(100vh - 120px);
    height: calc(100dvh - 60px);
  }
`;
