import React from "react";
import styled from "styled-components";
import { useRecoilState, useSetRecoilState } from "recoil";

import { outputLoadingState } from "@wrtn/core/stores";
import {
  BundleToolType,
  useEvent,
  useForms,
  useGenerateFilter,
} from "@wrtn/core";

import { FormContainer } from "@wrtn/ui/containers/FormContainer";
import { GenerateContainer } from "src/containers/GenerateContainer";
import BundleOutputContainer from "src/containers/OutputContainer/BundleOutputContainer";

import { ToolControllerComposition } from "./ToolControllerComposition";

import { bundleGenerateEventState } from "../stores/bundle";
import { ToolMetaContainer } from "src/containers/ToolMetaContainer";

interface BundleControllerProps {
  bundle: BundleToolType;
}

const BundleController = ({ bundle }: BundleControllerProps) => {
  const kind = "bundle";
  const bundleId = React.useMemo(() => bundle._id, [bundle]);

  const {
    forms,
    isDisabled,
    exampleForms,
    resetForms,
    updateForms,
    syncForms,
  } = useForms({
    tool: bundle,
    kind,
  });

  // 여러 개의 generate를 관리하는 hooks
  const setBundleGenerateEvent = useSetRecoilState(bundleGenerateEventState);
  const generateFilter = useGenerateFilter({ forms, toolId: bundleId });
  const [outputLoading, setOutputLoading] = useRecoilState(outputLoadingState);
  const loadingRef = React.useRef(outputLoading);
  loadingRef.current = outputLoading;
  const { collectEvent } = useEvent();

  const handleGenerateList = async () => {
    // 하위 툴들에게 개별 생성 이벤트를 내려줍니다.
    collectEvent("click_generate_btn", {
      feature_menu: "tool",
      generate_position: "tool",
      feature_category: bundle.category,
      feature_name: bundle.name,
      feature_tag: bundle.tag,
      feature_form: bundle.kind,
      prompt_id: bundle.tools.map((v) => v.toolId),
      feature_prompt: "전체",
      repeat_count: 1,
      method: "button",
    });
    const isToxicity = await generateFilter.handleCheckToxicityGenerate({
      isImage: false,
    });
    if (isToxicity) return;
    setOutputLoading(true);
    setBundleGenerateEvent({
      bundleId: bundleId,
      events: [],
    });
  };

  React.useEffect(() => {
    setBundleGenerateEvent({
      bundleId: bundleId,
      events: null,
    });
  }, [bundleId]);

  return (
    <ToolContentWrapper>
      <ToolControllerComposition
        kind={kind}
        toolHeader={<ToolMetaContainer tool={bundle} />}
        formContainer={
          <FormContainer
            forms={forms}
            exampleForms={exampleForms}
            resetForms={resetForms}
            updateForms={updateForms}
          />
        }
        generateContainer={
          <GenerateContainer
            tool={bundle}
            kind={kind}
            isDisabled={isDisabled}
            handleGenerate={handleGenerateList}
            parent={bundle}
          />
        }
        outputContainer={
          <BundleOutputContainer
            bundleId={bundleId}
            syncForms={syncForms}
            forms={forms}
            count={1}
            bundle={bundle}
          />
        }
      />
    </ToolContentWrapper>
  );
};

export default BundleController;

const ToolContentWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  @media(max-width: 1279px) {
    height: initial;
  }

  position: relative;
  @media (max-width: 1279px) {
    display: block;
  }
`;
