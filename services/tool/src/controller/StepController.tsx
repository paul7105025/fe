import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import { outputLoadingState } from "@wrtn/core/stores";
import { useForms, useStep, useOutputs, useGenerate } from "@wrtn/core";

import { useStepToolList, useCurrentStepStates } from "../hooks";

import Step from "../components/Step";

import { Dialog } from "@wrtn/ui/components/Dialog";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { NextSendToEditorButton } from "@wrtn/ui/components/SendToEditorButton";

import { FormContainer } from "@wrtn/ui/containers/FormContainer";

import { OutputContainer } from "../containers/OutputContainer";
import { GenerateContainer } from "../containers/GenerateContainer";
import { ToolMetaContainer } from "../containers/ToolMetaContainer";
import { ToolControllerComposition } from "./ToolControllerComposition";

import { stepCategorySelector } from "@wrtn/ui/utils/textToLexical";

import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";
import {
  BlogPostingStepContainer,
  BookDraftStepContainer,
} from "../containers/StepResultContainer";

const StepHeader = ({
  isResult,
  stepArray,
  stepToolList,
  currentStep,
  setCurrentStep,
  setTempStep,
  stepDisabled,
  outputLoading,
}) => {
  return (
    <Step.Header isResult={isResult}>
      {stepToolList.length > 0 &&
        stepArray.map((item, idx) => {
          return (
            <Step.Item
              key={item}
              index={idx}
              label={item}
              selected={idx === currentStep}
              isLast={stepArray.length - 1 === idx}
              disabled={
                (stepDisabled && currentStep < idx) ||
                (!stepDisabled && currentStep < idx - 1)
              }
              onClick={() => {
                if (!outputLoading) {
                  if (
                    currentStep === stepArray.length - 1 &&
                    currentStep > idx
                  ) {
                    setTempStep(idx);
                  } else {
                    setCurrentStep(idx);
                  }
                }
              }}
            />
          );
        })}
    </Step.Header>
  );
};

const StepController = ({ step }) => {
  const outputLoading = useRecoilValue(outputLoadingState);

  const kind = "step";
  const stepId = React.useMemo(() => step._id, [step]);

  const [tempStep, setTempStep] = React.useState(-1);
  const [stepOutput, setStepOutput] = React.useState("");
  const [stepDisabled, setStepDisabled] = React.useState(false);
  const [isSelectedStepExist, setIsSelectedStepExist] = React.useState(false);

  const {
    currentStep,
    setCurrentStep,

    scanOutputForm,
    stepHistory,

    scanLink,
    saveStepHistoryStore,
    deleteStepHistoryStore,
    moveToNextStep,
  } = useStep({ step });

  const { stepToolList, stepArray } = useStepToolList({ step });

  const { toolIndex, isResult } = useCurrentStepStates({
    currentStep,
    stepToolList,
    scrollToTop: () => {},
  });

  const tool = React.useMemo(() => {
    if (stepToolList.length > 0 && toolIndex > -1)
      return stepToolList[toolIndex];
    return null;
  }, [stepToolList, toolIndex]);

  const toolId = React.useMemo(() => tool?._id || "", [tool]);

  const {
    forms,
    isDisabled,
    resetForms,
    exampleForms,
    updateForms,
    syncForms,
    returnHistoryStore,
  } = useForms({
    tool,
    kind,
    scanLink,
  });

  const { output, setOutput, handleDelete, handleUpdate } = useOutputs({
    toolId,
  });

  const { handleGenerate } = useGenerate({
    forms,
    setOutput,
    tool: step,
  });

  const getValueByLabel = React.useCallback(
    (label) => {
      return scanOutputForm.find((v) => v.label === label)?.value;
    },
    [scanOutputForm]
  );

  const stepSyncForms = (input: any[], option: any[], item) => {
    syncForms(input, option);
    saveStepHistoryStore(returnHistoryStore(item));
  };

  const stepHandleDelete = (id, item) => {
    handleDelete(id);
    saveStepHistoryStore(returnHistoryStore(item));
    deleteStepHistoryStore(id);
  };

  const stepHandleUpdate = (id, object, item) => {
    handleUpdate(id, object);
    saveStepHistoryStore(returnHistoryStore(item));
  };

  React.useEffect(() => {
    if (output.length < 1) {
      setStepDisabled(true);
      setIsSelectedStepExist(false);
    } else {
      let result = false;

      output.forEach((v) => {
        if (stepHistory.findIndex((history) => history.outputId === v._id) > -1)
          result = true;
      });

      setStepDisabled(!result); // 스텝 상단
      setIsSelectedStepExist(result); // 다음 단계로
    }
  }, [output, stepHistory]);

  return (
    <React.Fragment>
      <ToolContentWrapper>
        {isResult && (
          <ToolResultWrapper column>
            <ToolMetaContainer tool={step} />
            <StepHeader
              isResult={isResult}
              stepArray={stepArray}
              stepToolList={stepToolList}
              currentStep={currentStep}
              setCurrentStep={setCurrentStep}
              setTempStep={setTempStep}
              stepDisabled={stepDisabled}
              outputLoading={outputLoading}
            />
            <ToolResultWrapper column>
              {stepId === "634ce3a16201f2e3452649ed" && (
                <BlogPostingStepContainer
                  scanOutputForm={scanOutputForm}
                  stepId={stepId}
                  isResult={isResult}
                  text={stepOutput}
                  setText={setStepOutput}
                />
              )}
              {stepId === "634d297f5d1d6e5d78d45dab" && (
                <BookDraftStepContainer
                  scanOutputForm={scanOutputForm}
                  stepId={stepId}
                  isResult={isResult}
                  text={stepOutput}
                  setText={setStepOutput}
                />
              )}
            </ToolResultWrapper>
          </ToolResultWrapper>
        )}
        {!isResult && stepToolList.length > 0 && tool && (
          <>
            <ToolControllerComposition
              kind={kind}
              toolHeader={<ToolMetaContainer tool={step} />}
              step={
                <StepHeader
                  isResult={isResult}
                  stepArray={stepArray}
                  stepToolList={stepToolList}
                  currentStep={currentStep}
                  setCurrentStep={setCurrentStep}
                  setTempStep={setTempStep}
                  stepDisabled={stepDisabled}
                  outputLoading={outputLoading}
                />
              }
              formContainer={
                <FormContainer
                  forms={forms}
                  exampleForms={exampleForms}
                  resetForms={resetForms}
                  updateForms={updateForms}
                />
              }
              generateContainer={
                <GenerateContainer
                  tool={tool}
                  kind={kind}
                  isDisabled={isDisabled}
                  handleGenerate={handleGenerate}
                  parent={step}
                />
              }
              outputContainer={
                <OutputContainer
                  tool={tool}
                  outputList={output}
                  syncForms={stepSyncForms}
                  onDeleteOutput={stepHandleDelete}
                  onUpdateOutput={stepHandleUpdate}
                  stepHistory={stepHistory}
                />
              }
              outputExpandContainer={
                <>
                  <NextSendToEditorButton
                    disabled={!isSelectedStepExist || outputLoading}
                    title={getValueByLabel(
                      toolId === "634ca61bea01540effd6c0aa" ||
                        toolId === "634cab262ac5086d66f2f83f"
                        ? "포스팅 주제"
                        : "주제"
                    )}
                    topic={getValueByLabel(
                      toolId === "634ca61bea01540effd6c0aa" ||
                        toolId === "634cab262ac5086d66f2f83f"
                        ? "포스팅 주제"
                        : "주제"
                    )}
                    category={
                      toolId === "634ca61bea01540effd6c0aa" ||
                      toolId === "634cab262ac5086d66f2f83f"
                        ? stepCategorySelector[getValueByLabel("카테고리")]
                        : "선택 안 함"
                    }
                    text={
                      toolId === "634ca61bea01540effd6c0aa" ||
                      toolId === "634cb161e4aa7d9eb533af30"
                        ? stepHistory.find(
                            (history) => history.toolId === toolId
                          )?.outputData
                        : toolId === "634cab262ac5086d66f2f83f"
                        ? (getValueByLabel("서론 결과물")
                            ? getValueByLabel("서론 결과물") + "\n\n"
                            : "") + (getValueByLabel("본론 결과물") || "")
                        : (getValueByLabel("책 개요")
                            ? getValueByLabel("책 개요") + "\n\n"
                            : "") + (getValueByLabel("책 목차") || "")
                    }
                  />
                  <NextStepButton
                    disabled={!isSelectedStepExist || outputLoading}
                    onClick={isSelectedStepExist ? moveToNextStep : undefined}
                  >
                    다음 단계로
                  </NextStepButton>
                </>
              }
            />
          </>
        )}
      </ToolContentWrapper>
      {tempStep >= 0 && (
        <ModalPortal>
          <Dialog
            iconType=""
            title="이전 단계로 돌아가시겠어요?"
            description={[
              "이전 단계로 돌아가면 완성본이 초기화됩니다. ",
              "완성본을 미리 복사해주세요!",
            ]}
            rightButtonLabel="돌아가기"
            handleClose={() => {
              setTempStep(-1);
            }}
            handleRightButton={() => {
              setCurrentStep(tempStep);
              setTempStep(-1);
            }}
          />
        </ModalPortal>
      )}
    </React.Fragment>
  );
};

export default StepController;

const ToolContentWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  @media(max-width: 1279px) {
    height: initial;
  }

  position: relative;
  @media (max-width: 1279px) {
    display: block;
  }
`;

const ToolResultWrapper = styled(FlexWrapper)`
  width: 100%;

  height: 100%;
  position: relative;
  @media (max-width: 1279px) {
    display: block;
  }
`;

const NextStepButton = styled(FlexButton)`
  position: absolute;
  width: 173px;
  height: 42px;
  right: 30px;
  bottom: 45px;
  background: ${({ disabled }) =>
    disabled ? colors.BLUE_GRAY_LINE : colors.POINT_PURPLE};
  border-radius: 5px;
  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
  z-index: 3;
  color: white;
  ${typo({
    size: "16px",
    weight: "600",
    height: "23px",
    color: colors.WHITE,
  })}

  &:hover {
    background: ${({ disabled }) =>
      disabled ? colors.BLUE_GRAY_LINE : colors.POINT_PURPLE_HOVER};
  }

  @media (max-width: 1279px) {
    position: fixed;
    bottom: 40px;
  }

  @media (max-width: 767px) {
    width: calc(100% - 60px);
    bottom: 25px;
  }
`;
