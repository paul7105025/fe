import { RecoilRoot } from "recoil";
import { Routes, BrowserRouter, Route } from "react-router-dom";
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import { ToastMessage } from "@wrtn/ui/components/ToastMessage";
import ToolPage from "./exposes/ToolPage";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

const queryClient = new QueryClient();

function App() {
  return (
    // @ts-ignore
    <AlertProvider template={ToastMessage} {...alertOptions}>
      <QueryClientProvider client={queryClient}>
        <RecoilRoot>
          <BrowserRouter>
            <Routes>
              <Route path="tool">
                <Route path=":toolId" element={<ToolPage />} />
              </Route>
            </Routes>
          </BrowserRouter>
        </RecoilRoot>
      </QueryClientProvider>
    </AlertProvider>
  );
}

export default App;
