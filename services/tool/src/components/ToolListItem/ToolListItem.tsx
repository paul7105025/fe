import React from "react";
import styled from "styled-components";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal/ModalPortal";

import { TOOL_ICON_SELECTOR, TOOL_TAG_SELECTOR } from "@wrtn/ui/types";

import {
  colors,
  FlexButton,
  FlexWrapper,
  SmallTag,
  typo,
} from "@wrtn/ui/styles";

interface ToolListItemProps {
  icon: string;
  name: string;
  tag: string;
  selected: boolean;
  onClick: () => void;

  outputLoading: boolean;
}

export const ToolListItem = ({
  icon = "google",
  name = "",
  tag = "none",
  selected,
  onClick,
  outputLoading,
}: ToolListItemProps) => {
  const [isHover, setIsHover] = React.useState(false);

  const ref = React.useRef<HTMLButtonElement>(null);
  const timer = React.useRef<any>();

  const handleDelay = () => {
    setIsHover(true);
  };

  const handleMouseEnter = (e) => {
    if (!isHover) {
      timer.current = setTimeout(handleDelay, 250);
    }
  };

  const handleMouseOut = (e) => {
    clearTimeout(timer.current);
    setIsHover(false);
  };

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  return (
    <ToggleItem
      justify="flex-start"
      selected={selected}
      disabled={outputLoading}
      onClick={onClick}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseOut}
      ref={ref}
    >
      <ToggleItemIcon>{TOOL_ICON_SELECTOR[icon]}</ToggleItemIcon>
      {tag !== "none" && (
        <SmallTag color={TOOL_TAG_SELECTOR[tag]?.color}>
          {TOOL_TAG_SELECTOR[tag]?.name}
        </SmallTag>
      )}
      {isHover && (
        <ModalPositionPortal
          position={{
            left: ref.current && ref.current.getBoundingClientRect().left + 250,
            top: ref.current && ref.current.getBoundingClientRect().top + 21,
          }}
        >
          <TitleWholeName>{name}</TitleWholeName>
        </ModalPositionPortal>
      )}
      <ToggleItemLabel>{name}</ToggleItemLabel>
    </ToggleItem>
  );
};

const ToggleItem = styled(FlexButton)<{
  selected: boolean;
}>`
  position: relative;
  width: 100%;

  gap: 8px;
  padding: 12px 8px 12px 24px;

  background-color: ${({ selected }) => selected && "white"};

  &:hover {
    cursor: ${({ disabled }) => (disabled ? "wait" : "pointer")};
    background: ${({ disabled }) =>
      disabled ? colors.LIGHT_BLUE : colors.WHITE};
  }
  user-select: none;
`;

const ToggleItemIcon = styled(FlexWrapper)`
  /* width: 18px; */
  height: 18px;

  max-width: 26px;
  > svg {
    height: 18px;
  }
`;

const ToggleItemLabel = styled.p<{ selected?: boolean }>`
  flex: 1;
  text-align: left;
  ${({ selected }) =>
    typo({
      size: "14px",
      weight: selected ? 600 : 500,
      color: selected ? colors.GRAY_90 : colors.GRAY_80,
    })};
  min-height: 14px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

const TitleWholeName = styled.div`
  position: absolute;
  width: fit-content;
  transform: translateY(-50%);
  white-space: nowrap;

  background: ${colors.GRAY_80};
  /* Pop-up shadow */

  box-shadow: 0px 0px 5px rgba(56, 71, 130, 0.25);
  border-radius: 5px;
  font-weight: 600;
  font-size: 12px;
  line-height: 100%;
  /* identical to box height, or 12px */

  text-align: center;

  /* White */

  color: ${colors.WHITE};
  padding: 4px 9px;
  z-index: 100;
`;
