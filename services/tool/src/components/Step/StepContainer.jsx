import styled from "styled-components"
import { colors } from "@wrtn/ui/styles";

const StepContainer = ({ children }) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  )
}

export default StepContainer;

const Wrapper = styled.div`
  display: flex;
  background: ${colors.BACKGROUND};
  overflow:hidden;
`