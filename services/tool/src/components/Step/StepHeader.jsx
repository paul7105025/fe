import styled from "styled-components";
import { colors } from "@wrtn/ui/styles";

const StepHeader = ({ children, isResult }) => {
  return (
    <PositionWrapper>
      <Wrapper isResult={isResult}>{children}</Wrapper>
    </PositionWrapper>
  );
};

export default StepHeader;

const PositionWrapper = styled.div`
  width: 100%;

  z-index: 5;
`;

const Wrapper = styled.div`
  display: flex;
  background: ${colors.BACKGROUND};
  width: 100%;

  ${({ isResult }) => isResult && `padding-right: calc(50% - 28px);`}

  position: relative;
  overflow-y: hidden;

  @media (max-width: 1023px) {
    padding-right: 0;
  }
`;
