import styled, { css } from "styled-components";
import { colors, typo } from "@wrtn/ui/styles";

const StepItem = ({
  index,
  label,
  selected,
  isLast,
  onClick,
  disabled,
  noCircular = false,
}) => {
  return (
    <Wrapper onClick={!disabled ? onClick : () => {}} index={index}>
      <Square
        disabled={disabled}
        index={index}
        selected={selected}
        isLast={isLast}
      >
        {!noCircular && <Circular selected={selected}>{index + 1}</Circular>}{" "}
        {label}
      </Square>
    </Wrapper>
  );
};

export default StepItem;

const Wrapper = styled.div`
  width: 100%;
  cursor: pointer;
  position: relative;
  z-index: ${({ index }) => 19 - index};
`;

const Square = styled.div`
  background: ${({ selected }) =>
    selected ? colors.POINT_PURPLE : colors.BACKGROUND};

  ${({ selected }) =>
    typo({
      size: "16px",
      weight: "700",
      color: selected ? colors.WHITE : colors.GRAY_55,
    })}

  padding: 6px 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
  position: relative;
  min-height: 34px;

  &::before {
    border-left: 15px solid
      ${({ selected, isLast }) =>
        selected ? colors.POINT_PURPLE : isLast ? "none" : colors.GRAY_55};
    border-top: 17px solid transparent;
    border-bottom: 17px solid transparent;
    transform: scale(1.1);
    transform-origin: left center;
    content: "";
    left: 100%;
    position: absolute;
    width: 0;
    height: 0;
  }

  ${({ selected }) =>
    !selected &&
    css`
      ${({ isLast }) =>
        !isLast &&
        css`
          &::after {
            border-left: 15px solid ${colors.BACKGROUND};
            border-top: 17px solid transparent;
            border-bottom: 17px solid transparent;
            transform: translateX(-0.5px);
            content: "";
            z-index: ${({ index }) => 99 - index};
            left: 100%;
            position: absolute;
            width: 0;
            height: 0;
          }
        `}
      ${({ disabled }) =>
        !disabled &&
        css`
          &:hover {
            > span {
              border-color: white;
            }
            &::after {
              border-left: 15px solid ${colors.TAG_PURPLE};
              border-top: 17px solid transparent;
              border-bottom: 17px solid transparent;
              content: "";
              z-index: ${({ index }) => 19 - index};
              left: 0%;
              position: absolute;
              width: 0;
              height: 0;
            }
            background: ${colors.TAG_PURPLE};
            color: white;
            ${({ isLast }) =>
              !isLast &&
              css`
                &::before {
                  border-left: 15px solid ${colors.TAG_PURPLE};
                }
              `}
          }
        `}
    `}
  ${({ disabled }) =>
    disabled &&
    css`
      cursor: default;
    `}

  @media (max-width: 767px) {
    gap: 8px;
  }
`;

const Circular = styled.span`
  width: 22px;
  height: 22px;
  border-radius: 50%;
  border: 1.5px solid
    ${({ selected }) => (selected ? colors.WHITE : colors.GRAY_55)};
  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-width: 767px) {
    width: 18px;
    height: 18px;
  }
`;
