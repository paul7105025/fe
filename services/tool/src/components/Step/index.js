import StepItem from "./StepItem";
import StepHeader from "./StepHeader";
import StepContainer from "./StepContainer";

const Step = {
  Item : StepItem,
  Header : StepHeader,
  Container : StepContainer
}

export default Step;