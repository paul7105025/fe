import React from "react";
import styled from "styled-components";

import { FlexButton, colors } from "@wrtn/ui/styles";
import { GuideButtonWrapper } from "@wrtn/ui/components/GuideButton/styles";

import { Help } from "@wrtn/ui/assets";

export const ToolGuideButton = ({
  tutorialLink,
}: {
  tutorialLink: string | undefined;
}) => {
  const openTutorial = () => {
    if (typeof tutorialLink === "string" && tutorialLink.length > 0) {
      window.open(tutorialLink, "_blank");
    }
  };

  const isDisabled = !(
    typeof tutorialLink === "string" && tutorialLink.length > 0
  );

  return (
    <WrapperButton onClick={openTutorial} disabled={isDisabled}>
      <GuideButton disabled={isDisabled}>
        <Help />
      </GuideButton>
    </WrapperButton>
  );
};

const WrapperButton = styled(FlexButton)<{
  disabled: boolean;
}>`
  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
`;

const GuideButton = styled(GuideButtonWrapper)<{
  disabled: boolean;
}>`
  svg {
    width: 24px;
    height: 24px;
    path {
      fill: ${colors.gray_70};
    }
  }
  &:hover {
    svg {
      path {
        fill: ${(props) =>
          props.disabled ? colors.gray_70 : colors.POINT_PURPLE};
      }
    }
  }
`;
