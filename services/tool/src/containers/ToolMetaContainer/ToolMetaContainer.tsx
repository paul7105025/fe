import React from "react";
import styled from "styled-components";

import { ToolType, useEvent, useFavoriteIdList } from "@wrtn/core";

import { TOOL_ICON_SELECTOR, TOOL_TAG_SELECTOR } from "@wrtn/ui/types";
import { GuideButtonWrapper } from "@wrtn/ui/components/GuideButton/styles";

import { ToolGuideButton } from "./ToolGuideButton";
import { FlexWrapper, typo, colors, Tag } from "@wrtn/ui/styles";

import { FilledStar, Star } from "@wrtn/ui/assets";
//@ts-ignore
import metaPng from "@wrtn/ui/assets/tool/tool_meta.png";

interface ToolHeaderContainerProps {
  tool: ToolType;
}

export const ToolMetaContainer = ({ tool }: ToolHeaderContainerProps) => {
  const { collectEvent } = useEvent();

  const { handleClickFavorite, isFavorite } = useFavoriteIdList();

  const currentFavorite = isFavorite(tool?._id);

  const handleFavorite = () => {
    handleClickFavorite(tool?._id);

    collectEvent("click_tool_bookmark", {
      onoff: !currentFavorite,
      feature_menu: "tool",
      feature_category: tool.category,
      feature_name: tool.name,
      feature_tag: tool.tag,
      feature_form: tool.kind,
    });
  };

  return (
    <Wrapper column>
      <RowWrapper>
        <Left>
          {tool.icon === "meta" ? (
            <LogoPng src={metaPng} />
          ) : (
            <LogoIcon>{TOOL_ICON_SELECTOR[tool.icon]}</LogoIcon>
          )}
          {tool.tag !== "none" && (
            <HeaderTag color={TOOL_TAG_SELECTOR[tool.tag]?.color}>
              {TOOL_TAG_SELECTOR[tool.tag].name}
            </HeaderTag>
          )}
          <Title>{tool?.name}</Title>
        </Left>
        <Right>
          <ToolGuideButton tutorialLink={tool?.tutorial?.link} />
          <StarIcon isFavorite={currentFavorite} onClick={handleFavorite}>
            {currentFavorite ? <FilledStar /> : <Star />}
          </StarIcon>
        </Right>
      </RowWrapper>
      <Description>
        {tool.innerDescription && tool.innerDescription.length > 0
          ? tool.innerDescription
          : tool.description}
      </Description>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)`
  position: relative;
  width: 100%;

  gap: 28px;
  padding: 40px 30px 20px;

  border-bottom: 1.5px solid #ecf0ff;

  @media (max-width: 1023px) {
    width: 100%;
  }
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: space-between;
`;

const Title = styled.p`
  ${typo({
    weight: "600",
    size: "20px",
    height: "100%",
    color: colors.gray_80,
  })}

  display: flex;
  align-items: center;
  white-space: nowrap;
`;

const StarIcon = styled(GuideButtonWrapper)<{
  isFavorite: boolean;
}>`
  svg {
    width: 24px;
    height: 24px;
    path {
      fill: ${({ isFavorite }) =>
        isFavorite ? colors.STAR_YELLOW : colors.GRAY_70};
    }
  }
  &:hover {
    svg {
      path {
        fill: ${({ isFavorite }) =>
          isFavorite ? colors.STAR_YELLOW : colors.POINT_PURPLE};
      }
    }
  }

  cursor: pointer;
`;

const Left = styled(FlexWrapper)`
  gap: 8px;
`;

const HeaderTag = styled(Tag)`
  margin-left: 3px;
  margin-right: 0;
`;

const Right = styled(FlexWrapper)`
  gap: 6px;
`;

const LogoIcon = styled(FlexWrapper)`
  max-width: 36px;
  height: 26px;
  svg {
    height: 26px;
    display: block;
    margin: 0 auto;
    text-align: center;
    g {
      transform-origin: center center;
    }
  }
`;

const LogoPng = styled.img`
  width: 23px;
  height: 14px;

  display: flex;
  justify-content: center;
  align-items: center;
`;

const Description = styled.p`
  width: 100%;

  ${typo({
    size: "16px",
    weight: 500,
    height: "140%",
    color: colors.GRAY_70,
  })}

  white-space: pre-line;
`;
