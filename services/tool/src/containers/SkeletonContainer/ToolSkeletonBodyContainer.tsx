import styled, { css } from "styled-components";

import { Kind } from "@wrtn/core";

import { Skeleton } from "@wrtn/ui/components/Skeleton";
import { Spinner } from "@wrtn/ui/components/Spinner";
import Step from "src/components/Step";

import {
  ToolFormWrapper,
  ToolResultWrapper,
} from "../../controller/ToolControllerComposition";

import { FlexWrapper } from "@wrtn/ui";

interface ToolSkeletonBodyContainerProps {
  kind: Kind;
}

export const ToolSkeletonBodyContainer = ({
  kind = "tool",
}: ToolSkeletonBodyContainerProps) => {
  return (
    <>
      {kind === "step" && (
        <Step.Header isResult={false}>
          {[" ", "  ", "  "].map((item, idx) => (
            <Step.Item
              key={idx}
              index={idx}
              label={item}
              selected={idx === 0}
              isLast={idx === 2}
              disabled={false}
              noCircular={true}
              onClick={undefined}
            />
          ))}
        </Step.Header>
      )}
      <ToolContentWrapper>
        <ToolFormWrapper kind={kind}>
          <FormScrollWrapper>
            <InputWrapper>
              <GuideWrapper>
                <Skeleton width="73px" height="20px" />
                <Skeleton width="24px" height="24px" borderRadius="50%" />
                <Spacer />
                <Skeleton width="64px" height="20px" />
              </GuideWrapper>
              <Skeleton height="42px" borderRadius="8px" />
              <CounterWrapper>
                <Skeleton width="52px" height="18px" />
              </CounterWrapper>
            </InputWrapper>
          </FormScrollWrapper>
          <Footer>
            {kind !== "bundle" && <Skeleton width="180px" height="42px" />}
            <AutoMakeButtonWrapper>
              <Skeleton height="42px" style={{ flex: 1 }} />
            </AutoMakeButtonWrapper>
          </Footer>
        </ToolFormWrapper>
        <ToolResultWrapper kind={kind}>
          <SafeWrapper>
            <HeaderWrapper kind={kind}></HeaderWrapper>
            <ContentWrapper>
              <SpinnerWrapper>
                <Spinner width={53} height={53} />
              </SpinnerWrapper>
            </ContentWrapper>
          </SafeWrapper>
        </ToolResultWrapper>
      </ToolContentWrapper>
    </>
  );
};

const Spacer = styled.div`
  flex: 1;
`;

const ToolContentWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  @media (max-width: 1279px) {
    display: block;
  }
`;

// eslint-disable-next-line no-undef
const SingleToolStyle = css`
  border-radius: 20px 0px 0px 0px;
  @media (max-width: 1279px) {
    border-radius: 0px;
  }
`;

const StepToolStyle = css``;

const styleSelector = {
  tool: SingleToolStyle,
  bundle: SingleToolStyle,
  step: StepToolStyle,
};

const FormScrollWrapper = styled(FlexWrapper)`
  flex: 1;
  flex-direction: column;
  justify-content: flex-start;
  gap: 30px;
  ${"" /* overflow: scroll; */}
  width: 100%;
  padding-top: 28px;
  ${
    "" /* @media (max-width: 1023px) {
    overflow: inherit;
  } */
  }
`;

const InputWrapper = styled(FlexWrapper)`
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  width: 100%;

  position: relative;
`;

const SafeWrapper = styled.div`
  height: 100%;
  position: relative;
`;

const GuideWrapper = styled(SafeWrapper)`
  gap: 5px;
  width: 100%;
`;

const CounterWrapper = styled.div`
  position: absolute;
  bottom: -30px;
  right: 0px;
`;

const Footer = styled(FlexWrapper)`
  width: 100%;
  justify-content: flex-start;
  left: 0px;
  padding-top: 40px;
  gap: 12px;
  @media (max-width: 1279px) {
    padding-top: 40px;
  }
  @media (max-width: 480px) {
    flex-direction: column;
  }
`;

const AutoMakeButtonWrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
`;

const HeaderWrapper = styled.div<{
  kind: Kind;
  isHidden?: boolean;
}>`
  padding: ${({ kind, isHidden }) =>
      isHidden ? "4px" : kind === "step" ? "4px" : "38px"}
    30px 0px 30px;
  border-bottom: 1.5px solid #ecf0ff;
  display: flex;
  gap: 23px;
  align-items: flex-end;
  z-index: ${({ isHidden }) => (isHidden ? -1 : 1)};

  @media (max-width: 480px) {
    padding: 38px 20px 0px 20px;
  }
`;

export const ContentWrapper = styled.div`
  padding: 20px 30px 15%;
  overflow: auto;
  height: calc(100% - 36px);

  @media (max-width: 767px) {
    height: calc(100% - 72px);
  }
`;

const SpinnerWrapper = styled(FlexWrapper)`
  flex-direction: column;
  gap: 13px;
  width: 100%;
  height: 100%;
  justify-content: center;
`;
