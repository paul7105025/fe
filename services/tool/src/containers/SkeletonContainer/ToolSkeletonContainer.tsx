import styled from "styled-components";

import { ToolSkeletonBodyContainer } from "./ToolSkeletonBodyContainer";
import { Skeleton } from "@wrtn/ui/components/Skeleton";
import { FlexWrapper } from "@wrtn/ui";

interface ToolSkeletonContainerProps {
  type: "tool" | "step" | "bundle";
}

export const ToolSkeletonContainer = ({
  type = "tool",
}: ToolSkeletonContainerProps) => {
  return (
    <>
      <ToolHeaderWrapper align="flex-start">
        <InnerHeaderWrapper column align="flex-start">
          <RowWrapper justify="space-between">
            <Skeleton width="26px" height="26px" />
            <Skeleton width="200px" height="26px" />
            <Spacer />
            <Skeleton width="24px" height="24px" borderRadius="50%" />
            <Skeleton width="22px" height="22px" />
          </RowWrapper>
          <Skeleton width="360px" height="20px" />
        </InnerHeaderWrapper>
        <div style={{ flex: 1 }} />
      </ToolHeaderWrapper>
      <ToolSkeletonBodyContainer kind={type} />
    </>
  );
};

export const ToolHeaderWrapper = styled(FlexWrapper)`
  position: absolute;
  top: 0px;
  left: 0px;

  width: 100%;
  height: 140px;
  padding: 48px 0px 0px;

  z-index: 3;

  @media (max-width: 1023px) {
    flex-direction: column-reverse;
  }
`;

const InnerHeaderWrapper = styled(FlexWrapper)`
  position: relative;
  flex: 1;

  gap: 24px;
  padding: 0px 28px 25px;

  border-bottom: 1.5px solid #ecf0ff;

  @media (max-width: 1023px) {
    width: 100%;
  }
`;

const RowWrapper = styled(FlexWrapper)`
  width: 100%;

  gap: 28px;
`;

const Spacer = styled.div`
  flex: 1;
`;
