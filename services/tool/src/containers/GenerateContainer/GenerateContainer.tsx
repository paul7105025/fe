import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import {
  Kind,
  ToolType,
  PLAN_INFO,
  toolAverageUsage,
  //
  useCount,
  useEvent,
  useFreePlanBubble,
  useTriggerAutoMake,
  useTriggerEvaluate,
  useTriggerGenerateCount,
} from "@wrtn/core";
import { outputLoadingState, userState } from "@wrtn/core/stores";

import { ToolScoreModal } from "@wrtn/ui/components/ScoreModal";
import { OutputCounter } from "@wrtn/ui/components/OutputCounter";
import { AutoMakeButton } from "@wrtn/ui/components/AutoMakeButton";
import { FreePlanBubble } from "@wrtn/ui/components/FreePlanBubble";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

import { IconInfo } from "@wrtn/ui/assets";

interface GenerateContainerProps {
  tool: ToolType;
  kind: Kind;

  isDisabled: boolean;
  handleGenerate: (payload: { count: number; toolId: string }) => Promise<any>;
  parent: ToolType;
}

export const GenerateContainer = ({
  tool,
  kind = "tool",

  isDisabled,
  handleGenerate,
  parent,
}: GenerateContainerProps) => {
  const toolId = tool._id;

  const outputLoading = useRecoilValue(outputLoadingState);
  const user = useRecoilValue(userState);



  const { isNoCreditModalOpen, isClosing, closeNoCreditModal } =
    useFreePlanBubble();

  const { count, increment, decrement } = useCount(toolId);

  const { handleEvaluate } = useTriggerEvaluate();

  const { handleIncreaseCount, currentCount } = useTriggerGenerateCount(
    toolId,
    kind
  );
  const { handleAutoMake } = useTriggerAutoMake();
  const { collectEvent } = useEvent();

  const resultRef = React.useRef<HTMLDivElement>(null);
  const resultClientRect = resultRef?.current?.getBoundingClientRect();

  const onClickGenerate = async () => {

    if (!outputLoading) {
      handleGenerate({ count, toolId }).then((res) => {
        handleAutoMake();
        handleEvaluate();
        handleIncreaseCount(count);
      });
      if (kind !== "bundle") {
        collectEvent("click_generate_btn", {
          feature_menu: "tool",
          generate_position: "tool",
          feature_category: parent.category,
          feature_name: parent.name,
          feature_tag: parent.tag,
          feature_form: parent.kind,
          prompt_id: [toolId],
          feature_prompt: tool.name,
          repeat_count: count,
          method: "button",
        });
      }
    }
  };

  const maxGenerate =
    user && user?.hasOwnProperty("plan")
      ? PLAN_INFO[user.plan || "FREE"]?.maxGenerate
      : 0;

  return (
    <Wrapper column>
      <InfoWrapper justify="flex-end">
        {toolAverageUsage.get(toolId) && (
          <>
            <IconInfo /> 공백 포함 약 {toolAverageUsage.get(toolId)}자 생성
          </>
        )}
      </InfoWrapper>
      <Footer>
        {kind !== "bundle" && (
          <OutputCounter
            handleUp={increment}
            handleDown={decrement}
            count={count}
            maxCount={maxGenerate}
          />
        )}
        <AutoMakeButtonWrapper ref={resultRef}>
          <AutoMakeButton
            outputLoading={outputLoading}
            disabled={isDisabled}
            handleGenerate={onClickGenerate}
          />
        </AutoMakeButtonWrapper>
      </Footer>
      {resultClientRect && (
        <ToolScoreModal
          toolId={toolId}
          toolName={tool.name}
          currentCount={currentCount}
          resultClientRect={resultClientRect}
        />
      )}
      {isNoCreditModalOpen && (
        <FreePlanBubble
          isClosing={isClosing}
          handleClose={closeNoCreditModal}
        />
      )}
    </Wrapper>
  );
};

const Footer = styled(FlexWrapper)`
  width: 100%;

  left: 0px;
  padding-top: 12px;
  gap: 12px;

  @media (max-width: 1023px) {
    padding-top: 12px;
  }
  @media (max-width: 480px) {
    flex-direction: column;
  }
`;

const AutoMakeButtonWrapper = styled(FlexWrapper)`
  flex: 1;
  width: 100%;
`;

const InfoWrapper = styled(FlexWrapper)`
  padding-top: 40px;
  ${typo({
    weight: 600,
    size: "14px",
    height: "21px",
    color: colors.gray_70,
  })};
  width: 100%;
  gap: 6px;
`;

const Wrapper = styled(FlexWrapper)`
  width: 100%;
  position: sticky;
  bottom: 0px;
  @media (max-width: 1279px) {
    position: initial;
    bottom: initial;
  }
`;