import styled from "styled-components";
import {
  AutoSizeTextArea,
  colors,
  FlexButton,
  FlexWrapper,
  typo,
} from "@wrtn/ui";

export const Wrapper = styled.div<{ isResult: boolean }>`
  width: 100%;
  height: 100%;
  background-color: ${colors.WHITE};

  position: absolute;
  left: 0;
  z-index: ${({ isResult }) => (isResult ? 30 : -30)};
  opacity: ${({ isResult }) => (isResult ? 1 : 0)};
`;

export const InnerWrapper = styled.div`
  max-width: 694px;

  width: 100%;
  height: 100%;

  margin: 0px auto;

  flex: 1;
  overflow-y: auto;
`;

export const ScrollWrapper = styled.div`
  width: 100%;
  padding: 56px 0px 20px;
`;

export const Divider = styled.div`
  background: #dee4f3;
  max-width: 654px;
  margin: 0px auto;
  height: 1px;
  margin-bottom: 19px;
`;

export const TextInput = styled(AutoSizeTextArea)`
  padding-top: 34px;
  border: none;
  width: 100%;

  ${typo({
    size: "16px",
    height: "180%",
    weight: 500,
    color: "#555555",
  })}

  resize: none;
  overflow: hidden;
  padding: 15px 23px;
  max-width: 694px;
  &::placeholder {
    color: ${colors.GRAY_55};
  }
  &:hover {
    background: ${colors.BACKGROUND};
    border-radius: 5px;
  }
  &:focus {
    background: ${colors.BACKGROUND};
  }
`;

export const TitleWrapper = styled.div`
  margin-bottom: 15px;
  display: flex;
  padding: 0px 5px;
`;

export const Title = styled.input`
  flex-grow: 1;

  ${typo({
    size: "20px",
    height: "24px",
    weight: 700,
    color: colors.GRAY_100,
  })}

  padding: 6px 16px;
  &:hover {
    background: ${colors.BACKGROUND};
    border-radius: 5px;
  }

  &::placeholder {
    color: ${colors.GRAY_55};
  }
`;

export const Spacer = styled.div`
  flex-grow: 1;
`;

export const MenuButton = styled(FlexButton)`
  gap: 3px;
  cursor: pointer;
  padding: 0px 6px;
  color: ${colors.GRAY_55};
  svg {
    width: 100%;
    height: 100%;
    path {
      fill: ${colors.GRAY_55};
    }
  }

  &:hover {
    color: ${colors.POINT_PURPLE};
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  }
`;

export const MenuIcon = styled.div`
  width: 16px;
  height: 16px;
`;

export const MenuText = styled.p`
  ${typo({
    size: "16px",
    height: "170%",
    weight: 600,
    color: colors.BLACK,
  })}
`;

export const Header = styled(FlexWrapper)`
  gap: 6px;
  padding: 0px 20px;
  margin-bottom: 10px;
`;

export const HeaderText = styled.p`
  ${typo({
    size: "16px",
    height: "24px",
    weight: 600,
    color: colors.GRAY_60,
  })}
`;
