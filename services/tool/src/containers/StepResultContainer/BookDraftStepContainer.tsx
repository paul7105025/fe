import React from "react";

import { useAlert } from "react-alert";
import { useRecoilState } from "recoil";
import CopyToClipboard from "react-copy-to-clipboard";

import { GuideButton } from "@wrtn/ui/components/GuideButton";
import { ModalPortal } from "@wrtn/ui/components/ModalPortal";
import { StepScoreModal } from "@wrtn/ui/components/ScoreModal";
import { DefaultSpinner } from "@wrtn/ui/components/DefaultSpinner";
import { SendToEditorButton } from "@wrtn/ui/components/SendToEditorButton";

import { useEvent, useTriggerGenerateCount } from "@wrtn/core";
import { currentCountScoreState } from "@wrtn/core/stores";
import { postStaticBookTitle } from "@wrtn/core/services";

import { Copy, Refresh } from "@wrtn/ui/assets";
import {
  Wrapper,
  InnerWrapper,
  ScrollWrapper,
  Header,
  HeaderText,
  Spacer,
  MenuButton,
  MenuIcon,
  MenuText,
  TitleWrapper,
  Title,
  Divider,
  TextInput,
} from "./styles";

// 책 목차 스탭 전용
export const BookDraftStepContainer = ({
  stepId,
  isResult,
  text,
  setText,
  scanOutputForm,
}) => {
  const [currentCount, setCurrentCount] = useRecoilState(
    currentCountScoreState
  );
  const { handleIncreaseCount } = useTriggerGenerateCount(stepId, "step");

  const {
    collectUserProperties,
    collectUserProperties_once,
    collectUserProperties_increment,
  } = useEvent();
  const alert = useAlert();

  const [loading, setLoading] = React.useState(false);
  const [title, setTitle] = React.useState("");
  const [tempTitle, setTempTitle] = React.useState("");

  const getValueByLabel = React.useCallback(
    (label) => {
      return scanOutputForm.find((v) => v.label === label)?.value;
    },
    [scanOutputForm]
  );

  const getText = () => {
    setText(
      (getValueByLabel("책 개요") ? getValueByLabel("책 개요") + "\n\n" : "") +
        (getValueByLabel("책 목차") || "")
    );
  };
  const handleClickCopy = () => {
    alert.removeAll();
    alert.show("완성본을 복사했어요.");
  };
  const handleClickRestore = () => {
    getText();
    setTempTitle(title);
  };

  React.useEffect(() => {
    const f = async () => {
      setLoading(true);
      const res = await postStaticBookTitle({
        data: {
          inputs: [getValueByLabel("주제"), getValueByLabel("책 개요")],
        },
      });
      //  should be 201
      if (res.status === 200 || res.status === 201) {
        collectUserProperties_once("first_generate_date", new Date());
        collectUserProperties_increment("count_generate_accum", 1);
        // collectUserProperties_increment("amount_generate_accum", )
        collectUserProperties("last_generate_date", new Date());

        setTitle(res.data.data);
        setTempTitle(res.data.data);
        setLoading(false);
        handleIncreaseCount(1);
      } else {
        setLoading(false);
      }
    };
    if (
      isResult &&
      getValueByLabel("주제") &&
      getValueByLabel("책 개요") &&
      getValueByLabel("책 목차") &&
      title?.length === 0
    )
      f();
  }, [isResult, getValueByLabel, title]);

  React.useEffect(() => {
    getText();
  }, [scanOutputForm]);

  return (
    <Wrapper isResult={isResult}>
      {loading && (
        <ModalPortal>
          <DefaultSpinner />
        </ModalPortal>
      )}
      <InnerWrapper>
        <ScrollWrapper>
          <Header>
            <HeaderText>AI 제목 추천</HeaderText>
            <GuideButton
              hoverTitle="AI 제목 추천이 무엇인가요?"
              guide={{
                isVisible: true,
                text: "AI가 글의 내용을 분석해 알맞은 제목을 추천해 드려요.",
              }}
            />
            <Spacer />
            <MenuButton onClick={handleClickRestore}>
              <MenuIcon>
                <Refresh />
              </MenuIcon>
              <MenuText>원본으로</MenuText>
            </MenuButton>
            <CopyToClipboard text={text}>
              <MenuButton onClick={handleClickCopy}>
                <MenuIcon>
                  <Copy />
                </MenuIcon>
                <MenuText>전체 복사</MenuText>
              </MenuButton>
            </CopyToClipboard>
          </Header>
          <TitleWrapper>
            <Title
              placeholder="제목"
              value={tempTitle}
              onChange={(e) => setTempTitle(e.target.value)}
            />
          </TitleWrapper>
          <Divider />
          <TextInput
            placeholder="내용"
            spellCheck={false}
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
          <SendToEditorButton
            title={title}
            topic={getValueByLabel("주제")}
            category="선택 안 함"
            text={text}
          />
        </ScrollWrapper>
        <StepScoreModal stepId={stepId} currentCount={currentCount} />
      </InnerWrapper>
    </Wrapper>
  );
};
