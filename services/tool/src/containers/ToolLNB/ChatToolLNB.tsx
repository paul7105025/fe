import React from "react";
import styled from "styled-components";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";

import { SearchInput } from "@wrtn/ui/components/SearchInput";
import CategoryToggle from "../../containers/CategoryToggle";

import { colors, FlexButton, FlexWrapper } from "@wrtn/ui/styles";

import {
  currentToolChatIdState,
  outputLoadingState,
  userState,
} from "@wrtn/core/stores";

import { TRIGGER_TOOL_MESSAGE } from "@wrtn/core/constants";

import {
  ToolType,
  useCategoryList,
  useCategorySearch,
  useEvent,
  useFavoriteToolListSearch,
  useFetchToolList,
  useLoginDialog,
  useNewToolListSearch,
  useRefresh,
  chatLoadingState,
  useIsMobile,
  chatInputToolSourceState
} from "@wrtn/core";
import { getLocal, setLocal } from "@wrtn/core";
import { TOOL_CATEGORY_RECORD } from "@wrtn/core";

import { DoubleCarousel, IconMenu } from "@wrtn/ui/assets";

interface LeftToolNavigationContainerProps {
  isOpen: boolean;
  isMobile?: boolean;
  isSideNavigationOpened: boolean;
  toggleWrapper?: () => void;
  toggleSideNavigation?: () => void;
  onClose?: () => void;
}

export const ChatToolLNB = ({
  isOpen,
  isMobile = false,
  isSideNavigationOpened,
  toggleWrapper,
  toggleSideNavigation,
  onClose = () => {},
}: LeftToolNavigationContainerProps) => {
  const { collectEvent } = useEvent();

  const isNavMobile = useIsMobile() || isMobile;

  const user = useRecoilValue(userState);
  const [currentToolId, setCurrentToolId] = useRecoilState(
    currentToolChatIdState
  );

  const loginDialog = useLoginDialog();

  const changeTool = (toolId: string) => {
    if (user) {
      setCurrentToolId(toolId);
      if (isNavMobile) {
        onClose();
      }
    } else {
      loginDialog.handleOpen();
    }
  };

  const [searchInput, setSearchInput] = React.useState("");

  const { fetchFavorite } = useRefresh();
  const { recommendToolList, chatToolList } = useFetchToolList();
  const { categoryList } = useCategoryList(chatToolList);

  const outputLoading = useRecoilValue(outputLoadingState);
  const chatLoading = useRecoilValue(chatLoadingState);

  const searchedToolList = useCategorySearch(
    searchInput,
    chatToolList,
    categoryList
  );

  const favoriteSearchedToolList = useFavoriteToolListSearch(
    searchInput,
    chatToolList
  );

  // console.log(favorite)
  const initialCollapsedList = React.useMemo(() => {
    return getLocal<Array<string>>("collapsedList") || [];
  }, []);

  const [searchCollapsedList, setSearchCollapsedList] = React.useState([]);

  const isSearchMode = React.useMemo(
    () => searchInput.length > 0,
    [searchInput]
  );
  const currentCollapsedList = React.useMemo(
    () => (isSearchMode ? searchCollapsedList : initialCollapsedList),
    [isSearchMode, initialCollapsedList, searchCollapsedList]
  );

  //

  const newSearchedToolList = useNewToolListSearch(searchInput, chatToolList);

  const saveCollapse = React.useCallback((idx: string) => {
    const savedCollapsedList = getLocal<Array<string>>("collapsedList") || [];

    if (savedCollapsedList.includes(idx)) {
      const newCollapsedList = savedCollapsedList.filter(
        (item) => item !== idx
      );
      setLocal("collapsedList", newCollapsedList);
    }
    if (!savedCollapsedList.includes(idx)) {
      const newCollapsedList = [...savedCollapsedList, idx];
      setLocal("collapsedList", newCollapsedList);
    }
  }, []);

  const [isFirstCollect, setIsFirstCollect] = React.useState(true);
  const setInputToolSource = useSetRecoilState(chatInputToolSourceState);

  React.useEffect(() => {
    if (searchInput !== "" && isFirstCollect) {
      setIsFirstCollect(false);
      collectEvent("search_tool", {
        position: "tool_nav",
      });
    }
  }, [searchInput]);

  React.useEffect(() => {
    if (isMobile) setLocal(TRIGGER_TOOL_MESSAGE, true);
  }, [isMobile]);

  React.useEffect(() => {
    setSearchCollapsedList([]);
  }, [searchInput.length]);

  const handleClickTool = (item: ToolType) => {
    collectEvent("open_tool_in_chat", {
      open_method: "left_bar"
    });
    setInputToolSource("leftbar");
    if (item._id !== currentToolId) {
      changeTool(item._id);
    }
  };

  React.useEffect(() => {
    fetchFavorite();
  }, []);

  const loading = React.useMemo(
    () => chatLoading || outputLoading,
    [chatLoading, outputLoading]
  );
  return (
    <Wrapper isOpen={true}>
      <MenuWrapper isMobile={isMobile}>
        {!isMobile ? (
          isOpen && (
            <MenuButton
              isOpened={isSideNavigationOpened}
              onClick={toggleSideNavigation}
            >
              <DoubleCarousel />
            </MenuButton>
          )
        ) : (
          <MenuButton onClick={() => toggleWrapper && toggleWrapper()}>
            <IconMenu />
          </MenuButton>
        )}
      </MenuWrapper>
      <SearchWrapper>
        <SearchInput
          value={searchInput}
          onChange={(e) => setSearchInput(e.target.value)}
          placeholder="툴 검색"
          onKeyDown={undefined}
        />
      </SearchWrapper>
      <CategoryWrapper>
        <CategoryToggle
          searchInput={searchInput}
          category="favorite"
          title={TOOL_CATEGORY_RECORD["favorite"]}
          initialCollapsed={true}
          saveCollapse={() => saveCollapse("favorite")}
          data={favoriteSearchedToolList}
          outputLoading={loading}
          selectedToolId={currentToolId || ""}
          onClick={(tool: ToolType) => {
            // 블럭 추가
            handleClickTool(tool);
          }}
        />
        <CategoryToggle
          searchInput={searchInput}
          category="new"
          title={TOOL_CATEGORY_RECORD["new"]}
          initialCollapsed={true}
          saveCollapse={() => saveCollapse("new")}
          data={newSearchedToolList}
          outputLoading={loading}
          selectedToolId={currentToolId || ""}
          onClick={(tool: ToolType) => {
            // 블럭 추가
            handleClickTool(tool);
          }}
        />
        {searchedToolList.map((item) => {
          return (
            <CategoryToggle
              key={item.category}
              category={item.category}
              searchInput={searchInput}
              title={TOOL_CATEGORY_RECORD[item.category]}
              initialCollapsed={true}
              saveCollapse={() => saveCollapse(item.category)}
              data={item.data}
              outputLoading={loading}
              selectedToolId={currentToolId || ""}
              onClick={(tool: ToolType) => {
                // 블럭 추가
                handleClickTool(tool);
              }}
            />
          );
        })}
      </CategoryWrapper>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)<{ isOpen: boolean }>`
  flex-direction: column;

  height: 100%;
  gap: 16px;

  justify-content: flex-start;
  align-items: flex-start;
  background-color: ${colors.BACKGROUND};

  width: ${({ isOpen }) => (isOpen ? "100%" : "0px")};
  @media (max-width: 767px) {
    background: white;
    width: 100%;
  }
`;

const MenuWrapper = styled(FlexWrapper)<{ isMobile: boolean }>`
  padding: ${({ isMobile }) => (isMobile ? 16 : 22)}px 16px 0px;
`;

const MenuButton = styled(FlexButton)<{ isOpened?: boolean }>`
  z-index: 80;

  cursor: pointer;

  svg {
    fill: ${colors.gray_80};
  }

  transform: ${({ isOpened }) => (isOpened ? "none" : "rotate(180deg)")};
  transition: transform 0.3s cubic-bezier(0.1, -0.15, 0.6, 1.45);
`;

const SearchWrapper = styled.div`
  padding: 0px 16px;
  width: 100%;
`;

const CategoryWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  height: 100%;
  overflow-y: scroll;
  overflow-x: visible;
  padding-bottom: 60px;
`;
