import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import { SearchInput } from "@wrtn/ui/components/SearchInput";
import CategoryToggle from "../CategoryToggle";

import { colors, FlexButton, FlexWrapper } from "@wrtn/ui/styles";
import {
  useCategorySearch,
  useFavoriteToolListSearch,
  useNewToolListSearch,
  useFetchToolList,
  useCategoryList,
} from "@wrtn/core/hooks";

import {
  useEvent,
  useSync,
  getLocal,
  setLocal,
  ToolType,
  TOOL_CATEGORY_RECORD,
} from "@wrtn/core";
import { TRIGGER_TOOL_MESSAGE } from "@wrtn/core/constants";

import { nextRouteState, outputLoadingState } from "@wrtn/core/stores";

import { DoubleCarousel, IconMenu } from "@wrtn/ui/assets";

interface LeftToolNavigationContainerProps {
  isOpen: boolean;
  isMobile?: boolean;
  isSideNavigationOpened: boolean;
  toggleWrapper?: () => void;
  toggleSideNavigation?: () => void;
  currentToolId: string | null;
  onClose: () => void,
}

export const ToolLNB = ({
  isOpen,
  isMobile = false,
  isSideNavigationOpened,
  toggleWrapper,
  toggleSideNavigation,
  currentToolId,
  onClose,
}: LeftToolNavigationContainerProps) => {
  useSync();

  const { collectEvent } = useEvent();
  const router = useRecoilValue(nextRouteState);

  const [searchInput, setSearchInput] = React.useState("");

  const { allToolList } = useFetchToolList();
  const { categoryList } = useCategoryList(allToolList);

  const outputLoading = useRecoilValue(outputLoadingState);

  const searchedToolList = useCategorySearch(
    searchInput,
    allToolList,
    categoryList
  );

  const favoriteSearchedToolList = useFavoriteToolListSearch(
    searchInput,
    allToolList
  );

  const initialCollapsedList = React.useMemo(() => {
    return getLocal<Array<string>>("collapsedList") || [];
  }, []);

  const [searchCollapsedList, setSearchCollapsedList] = React.useState([]);

  const isSearchMode = React.useMemo(
    () => searchInput.length > 0,
    [searchInput]
  );
  const currentCollapsedList = React.useMemo(
    () => (isSearchMode ? searchCollapsedList : initialCollapsedList),
    [isSearchMode, initialCollapsedList, searchCollapsedList]
  );

  const newSearchedToolList = useNewToolListSearch(searchInput, allToolList);

  const saveCollapse = React.useCallback((idx: string) => {
    const savedCollapsedList = getLocal<Array<string>>("collapsedList") || [];

    if (savedCollapsedList.includes(idx)) {
      const newCollapsedList = savedCollapsedList.filter(
        (item) => item !== idx
      );
      setLocal("collapsedList", newCollapsedList);
    }
    if (!savedCollapsedList.includes(idx)) {
      const newCollapsedList = [...savedCollapsedList, idx];
      setLocal("collapsedList", newCollapsedList);
    }
  }, []);

  const [isFirstCollect, setIsFirstCollect] = React.useState(true);

  React.useEffect(() => {
    if (searchInput !== "" && isFirstCollect) {
      setIsFirstCollect(false);
      collectEvent("search_tool", {
        position: "tool_nav",
      });
    }
  }, [searchInput]);

  React.useEffect(() => {
    if (isMobile) setLocal(TRIGGER_TOOL_MESSAGE, true);
  }, [isMobile]);

  React.useEffect(() => {
    setSearchCollapsedList([]);
  }, [searchInput.length]);

  const handleClickTool = (item: ToolType) => {
    collectEvent("click_tool_card", {
      position: "left_nav",
      feature_category: item.category,
      feature_name: item.name,
      feature_tag: item.tag,
      feature_form: item.kind,
      is_recomm_feature: false,
    });
    if (item._id !== currentToolId) {
      router.push(`/tool/${item._id}`);
    }
  };

  return (
    <Wrapper isOpen={true}>
      <MenuWrapper isMobile={isMobile}>
        {!isMobile ? (
          isOpen && (
            <MenuButton
              isOpened={isSideNavigationOpened}
              onClick={toggleSideNavigation}
            >
              <DoubleCarousel />
            </MenuButton>
          )
        ) : (
          <MenuButton onClick={() => toggleWrapper && toggleWrapper()}>
            <IconMenu />
          </MenuButton>
        )}
      </MenuWrapper>
      <SearchWrapper>
        <SearchInput
          value={searchInput}
          onChange={(e) => setSearchInput(e.target.value)}
          placeholder="툴 검색"
          onKeyDown={undefined}
        />
      </SearchWrapper>
      <CategoryWrapper>
        <CategoryToggle
          searchInput={searchInput}
          category="favorite"
          title={TOOL_CATEGORY_RECORD["favorite"]}
          initialCollapsed={currentCollapsedList.includes("favorite")}
          saveCollapse={() => saveCollapse("favorite")}
          data={favoriteSearchedToolList}
          outputLoading={outputLoading}
          selectedToolId={currentToolId || ""}
          onClick={(tool: ToolType) => {

            handleClickTool(tool);
            onClose();
          }}
        />
        <CategoryToggle
          searchInput={searchInput}
          category="new"
          title={TOOL_CATEGORY_RECORD["new"]}
          initialCollapsed={currentCollapsedList.includes("new")}
          saveCollapse={() => saveCollapse("new")}
          data={newSearchedToolList}
          outputLoading={outputLoading}
          selectedToolId={currentToolId || ""}
          onClick={(tool: ToolType) => {
            handleClickTool(tool);
            onClose();
          }}
        />
        {searchedToolList.map((item) => {
          return (
            <CategoryToggle
              key={item.category}
              category={item.category}
              searchInput={searchInput}
              title={TOOL_CATEGORY_RECORD[item.category]}
              initialCollapsed={currentCollapsedList.includes(item.category)}
              saveCollapse={() => saveCollapse(item.category)}
              data={item.data}
              outputLoading={outputLoading}
              selectedToolId={currentToolId || ""}
              onClick={(tool: ToolType) => {
                handleClickTool(tool);
                onClose();
              }}
            />
          );
        })}
      </CategoryWrapper>
    </Wrapper>
  );
};

const Wrapper = styled(FlexWrapper)<{ isOpen: boolean }>`
  flex-direction: column;

  height: 100%;
  gap: 16px;

  justify-content: flex-start;
  align-items: flex-start;
  background-color: ${colors.BACKGROUND};

  width: ${({ isOpen }) => (isOpen ? "100%" : "0px")};
  @media(max-width: 767px){
    background: white;
    width: 100%;
  }
`;

const MenuWrapper = styled(FlexWrapper)<{ isMobile: boolean }>`
  padding: ${({ isMobile }) => (isMobile ? 16 : 22)}px 16px 0px;
`;

const MenuButton = styled(FlexButton)<{ isOpened?: boolean }>`
  z-index: 80;

  cursor: pointer;

  svg {
    fill: ${colors.gray_80};
  }

  transform: ${({ isOpened }) => (isOpened ? "none" : "rotate(180deg)")};
  transition: transform 0.3s cubic-bezier(0.1, -0.15, 0.6, 1.45);
`;

const SearchWrapper = styled.div`
  padding: 0px 16px;
  width: 100%;
`;

const CategoryWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  height: 100%;
  overflow-y: scroll;
  overflow-x: visible;
  padding-bottom: 60px;
`;
