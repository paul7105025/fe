import React from "react";
import styled from "styled-components";
import { useRecoilValue } from "recoil";

import OutputHeader from "./OutputHeader";
import { EmptyContainer } from "./EmptyContainer";

import {
  BundleToolType,
  FormMetadataType,
  useTriggerAutoMake,
} from "@wrtn/core";
import { userState } from "@wrtn/core/stores";
import { TAB_ALL, TAB_FAVORITE } from "@wrtn/core/constants/common";

import TriggerSpeechBubble from "@wrtn/ui/components/TriggerSpeechBubble";
import { BundleSingleOutputContainer } from "./BundleSingleOutputContainer";

import { ContentWrapper, SafeWrapper, ShadowWrapper } from "./styles";

interface BundleOutputContainerProps {
  count: number;
  bundleId: string;
  syncForms: (input: any[], option: any[]) => void;
  forms: Array<FormMetadataType>;
  bundle: BundleToolType;
}

export const BundleOutputContainer = ({
  count,
  bundleId,
  syncForms,
  forms,
  bundle,
}: BundleOutputContainerProps) => {
  const toolList = React.useMemo(() => bundle.tools, [bundle.tools]);

  const user = useRecoilValue(userState);

  const { isTriggerAutoMake, handleCloseAutoMake } = useTriggerAutoMake();
  const [currentTab, setCurrentTab] = React.useState<
    typeof TAB_ALL | typeof TAB_FAVORITE
  >(TAB_ALL);
  const scrollRef = React.useRef<HTMLDivElement>(null);

  const [childrenOutputCount, setChildrenOutputCount] = React.useState<
    Array<number>
  >([]);

  React.useLayoutEffect(() => {
    setChildrenOutputCount(toolList.map((v) => 0));
  }, [toolList]);

  const countChildrenOutput = React.useCallback(
    (index: number, count: number) => {
      setChildrenOutputCount((res) => {
        const newChildrenOutputCount = [...res];
        newChildrenOutputCount[index] = count;
        return newChildrenOutputCount;
      });
    },
    []
  );

  const isCreated = childrenOutputCount.reduce((a, b) => a + b, 0) > 0;

  return (
    <SafeWrapper>
      <OutputHeader
        handleChangeTab={setCurrentTab}
        currentTab={currentTab}
        kind="bundle"
      />
      {isTriggerAutoMake && (
        <TriggerSpeechBubble
          handleClose={handleCloseAutoMake}
          toolId={bundleId}
        />
      )}
      <ContentWrapper ref={scrollRef}>
        {toolList?.map((v, i) => (
          <BundleSingleOutputContainer
            key={v.toolId}
            index={i}
            count={count}
            toolName={v.toolName}
            toolId={v.toolId}
            toolListLength={toolList.length}
            syncForms={syncForms}
            forms={forms}
            currentTab={currentTab}
            plan={user?.plan || "FREE"}
            parent={bundle}
            countChildrenOutput={(c) => countChildrenOutput(i, c)}
          />
        ))}
      </ContentWrapper>
      {bundle?.tutorial?.youtubeId && !isCreated && (
        <EmptyWrapper>
          <EmptyContainer youtubeId={bundle?.tutorial?.youtubeId || null} />
        </EmptyWrapper>
      )}
      <ShadowWrapper />
    </SafeWrapper>
  );
};

const EmptyWrapper = styled.div`
  position: absolute;

  top: 30px;
  left: 0;

  width: 100%;
  height: 100%;
`;

export default BundleOutputContainer;
