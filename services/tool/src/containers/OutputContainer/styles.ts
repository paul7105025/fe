import styled from "styled-components";

import { FlexWrapper, colors } from "@wrtn/ui";

import { ReactComponent as ClovaLogo } from "@wrtn/ui/assets/logo/logo_powered_by_clova.svg";

export const SafeWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;

  width: 100%;
  height: 100%;
  position: relative;
`;

export const ContentWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;

  padding: 20px 30px 10%;
  overflow: auto;

  width: 100%;
  max-height: calc(100vh - 80px);
  height: calc(100dvh - 80px);

  position: relative;

  @media (max-width: 767px) {
    height: calc(100% - 72px);
  }

  @media (max-width: 480px) {
    padding: 20px 18px 10%;
  }

  background-color: ${colors.BACKGROUND};
`;

export const OutputWrapper = styled.div`
  margin-bottom: 20px;
  width: 100%;
  transition: all 0.3s ease-in-out;
  z-index: 1;
`;

export const ShadowWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-end;
  padding-bottom: 32px;

  position: absolute;
  z-index: 2;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 10%;
  background: linear-gradient(180deg, #f2f7ff 0%, rgba(242, 247, 255, 0) 100%);
  transform: rotate(-180deg);
`;

export const LastDiv = styled.div`
  width: 100%;
  height: 25px;
`;

export const LogoWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-end;
  padding: 32px 0px 0px;

  width: 100%;
  height: 10%;
`;

export const PoweredByClovaLogo = styled(ClovaLogo)``;
