import React from "react";
import { useRecoilValue } from "recoil";
import styled, { css } from "styled-components";

import { useLoginDialog, userState } from "@wrtn/core";
import { TAB_ALL, TAB_FAVORITE } from "@wrtn/core/constants/common";

import { GuideModal } from "@wrtn/ui/components/GuideModal";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { FlexWrapper, FlexButton, typo, colors } from "@wrtn/ui";

const useOutputHeader = () => {
  const [isGuideOpen, setIsGuideOpen] = React.useState(false);

  const openGuide = () => setIsGuideOpen(true);
  const closeGuide = () => setIsGuideOpen(false);
  const toggleGuide = () => setIsGuideOpen((res) => !res);

  return {
    isGuideOpen,
    openGuide,
    closeGuide,
    toggleGuide,
  };
};

interface OutputHeaderProps {
  handleChangeTab: (tab: typeof TAB_ALL | typeof TAB_FAVORITE) => void;
  currentTab: string;
  kind: "tool" | "step" | "bundle";
}

const OutputHeader = ({
  handleChangeTab,
  currentTab,
  kind,
}: OutputHeaderProps) => {
  const user = useRecoilValue(userState);
  const { handleOpen } = useLoginDialog();

  const onClickFavorite = () => {
    if (!user) {
      handleOpen();
    } else {
      handleChangeTab(TAB_FAVORITE);
    }
  };

  const guideRef = React.useRef<HTMLDivElement>(null);

  const { isGuideOpen, closeGuide, toggleGuide } = useOutputHeader();

  return (
    <HeaderWrapper isHidden={kind === "step"} align="flex-end">
      <TabItem
        selected={currentTab === TAB_ALL}
        onClick={() => handleChangeTab(TAB_ALL)}
      >
        {TAB_ALL}
      </TabItem>
      <TabItem selected={currentTab === TAB_FAVORITE} onClick={onClickFavorite}>
        {TAB_FAVORITE}
      </TabItem>
      <div style={{ flex: 1 }} />
      <div ref={guideRef}>
        <GuideLink onClick={toggleGuide}>
          결과물의 저작권과 데이터 출처
        </GuideLink>
        {isGuideOpen && (
          <ModalPositionPortal
            position={{
              top:
                guideRef.current &&
                guideRef?.current?.getBoundingClientRect().top + 36,
              left:
                guideRef.current &&
                guideRef?.current?.getBoundingClientRect().left -
                  (window.innerWidth < 480
                    ? window.innerWidth * 0.9 - 140
                    : 320),
            }}
          >
            <GuideModal onClose={closeGuide} guideRef={guideRef} />
          </ModalPositionPortal>
        )}
      </div>
    </HeaderWrapper>
  );
};

export default OutputHeader;

const HeaderWrapper = styled(FlexWrapper)<{ isHidden: boolean }>`
  width: 100%;
  padding: ${({ isHidden }) => (isHidden ? "4px" : "53px")} 30px 0px 30px;
  border-bottom: 1.5px solid #ecf0ff;

  gap: 23px;

  z-index: ${({ isHidden }) => (isHidden ? -1 : 1)};

  @media (max-width: 767px) {
    padding: 38px 20px 0px 20px;
  }
`;

const TabItem = styled(FlexButton)<{ selected: boolean }>`
  ${typo({
    size: "14px",
    weight: "500",
    color: colors.GRAY_60,
  })}

  white-space: nowrap;
  padding: 0px 4px 10px;
  background-color: transparent;
  z-index: 10;

  ${({ selected }) =>
    selected &&
    css`
      font-weight: 600;
      color: ${colors.GRAY_90};
      padding-bottom: 7px;
      border-bottom: 3px solid #5a2cda;
    `};

  &:hover {
    cursor: pointer;
    color: ${colors.GRAY_90};
  }

  @media (max-width: 480px) {
    font-size: 14px;
  }

  user-select: none;
`;

const GuideLink = styled.div`
  z-index: 100;
  user-select: none;
  ${typo({
    size: "14px",
    weight: "600",
    height: "26px",
    color: colors.ACTION_BLUE,
  })}
  white-space: nowrap;

  text-decoration: underline;
  cursor: pointer;
  transform: translateY(-10px);

  @media (max-width: 1023px) {
    font-size: 12px;
    transform: translateY(-5px);
  }
`;
