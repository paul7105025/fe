import React from "react";
import { useAlert } from "react-alert";
import styled from "styled-components";
import { useRecoilState, useSetRecoilState } from "recoil";

import {
  useOutputs,
  useGenerate,
  outputLoadingState,
  useEvent,
  useTriggerAutoMake,
  useTriggerEvaluate,
  GENERATE_INVALID_INPUT,
  GENERATE_FAIL,
  useLikeFetch,
  TAB_ALL,
  TAB_FAVORITE,
  FormMetadataType,
  BundleToolType,
} from "@wrtn/core";

import { bundleGenerateEventState } from "../../stores/bundle";
import { useCurrentOutput } from "../../hooks/useCurrentOutput";

import { Spinner } from "@wrtn/ui/components/Spinner";
import { CarouselOutputCard } from "@wrtn/ui/components/OutputCard";

import { colors, FlexButton, FlexWrapper, IconReload, typo } from "@wrtn/ui";

import { OutputWrapper } from "./styles";

interface BundleSingleOutputContainerProps {
  index: number;
  count: number;
  toolName: string;
  toolId: string;
  toolListLength: number;
  syncForms: (input: any[], option: any[]) => void;
  forms: Array<FormMetadataType>;
  currentTab: typeof TAB_ALL | typeof TAB_FAVORITE;
  plan: string;
  parent: BundleToolType;
  countChildrenOutput: (c: number) => void;
}

export const BundleSingleOutputContainer = ({
  index,
  count,
  toolName,
  toolId,
  toolListLength,
  syncForms,
  forms,
  currentTab,
  plan,
  parent,
  countChildrenOutput,
}: BundleSingleOutputContainerProps) => {
  const [bundleGenerateEvent, setBundleGenerateEvent] = useRecoilState(
    bundleGenerateEventState
  );

  const {
    output: outputList,
    setOutput,
    handleDelete,
    handleUpdate,
  } = useOutputs({
    toolId,
  });

  const { handleGenerateFunc } = useGenerate({
    tool: parent,
    forms,
    setOutput,
  });

  const setOutputLoading = useSetRecoilState(outputLoadingState);

  const alert = useAlert();
  const { collectEvent } = useEvent();
  const { handleAutoMake } = useTriggerAutoMake();
  const { handleEvaluate } = useTriggerEvaluate();

  const [load, setLoad] = React.useState(false);

  const { isTriggerEvaluate, handleCloseEvaluate } = useTriggerEvaluate();

  React.useEffect(() => {
    const _isLast = (c: string[] | null) => {
      if (c && c.length === toolListLength - 1) {
        let isError = false;
        let isInvalidInput = false;
        c.forEach((v) => {
          if (v === "fail") isError = true;
          if (v === "400") isInvalidInput = true;
        });

        if (isInvalidInput) {
          alert.removeAll();
          alert.show(GENERATE_INVALID_INPUT);
        } else if (isError) {
          alert.removeAll();
          alert.error(GENERATE_FAIL);
        }
        return true;
      }
      return false;
    };

    if (
      bundleGenerateEvent.events?.length === index &&
      bundleGenerateEvent.bundleId === parent._id
    ) {
      setLoad(true);
      handleGenerateFunc({ count, toolId })
        .then(() => {
          let isLast = false;
          setBundleGenerateEvent((c) => {
            isLast = _isLast(c.events);
            const newEvent =
              c.events === null ? null : [...c.events, "success"];

            return {
              bundleId: c.bundleId,
              events: newEvent,
            };
          });
          if (isLast) {
            setOutputLoading(false);
          }
          setLoad(false);
        })
        .catch(() => {
          let isLast = false;
          setBundleGenerateEvent((c) => {
            isLast = _isLast(c.events);
            const newEvent = c.events === null ? null : [...c.events, "fail"];

            return {
              bundleId: c.bundleId,
              events: newEvent,
            };
          });
          if (isLast) {
            setOutputLoading(false);
          }
          setLoad(false);
        });
    }
  }, [bundleGenerateEvent]);

  const { likeList, refetchLikeList } = useLikeFetch({
    toolId,
    tab: currentTab,
  });

  const currentTabOutputList = useCurrentOutput({
    currentTab,
    outputList,
    likeList,
  });

  const [currentTabOutputIdx, setCurrentTabOutputIdx] = React.useState(0);

  const currentTabOutput = React.useMemo(() => {
    return currentTabOutputList[currentTabOutputIdx];
  }, [currentTabOutputIdx, currentTabOutputList]);

  const onClickGenerate = async () => {
    setLoad(true);
    collectEvent("click_generate_btn", {
      feature_menu: "tool",
      generate_position: "tool",
      feature_category: parent.category,
      feature_name: parent.name,
      feature_tag: parent.tag,
      feature_form: parent.kind,
      prompt_id: [toolId],
      feature_prompt: toolName,
      repeat_count: count,
      method: "button",
    });
    await handleGenerateFunc({ count, toolId })
      .then((res) => {
        handleAutoMake();
        handleEvaluate();
      })
      .catch((err) => {
        if (err === "400") {
          alert.removeAll();
          alert.show(GENERATE_INVALID_INPUT);
        }
        if (err === "failed") {
          alert.removeAll();
          alert.error(GENERATE_FAIL);
        }
      })
      .finally(() => {
        setCurrentTabOutputIdx(0);
        setLoad(false);
      });
  };

  const onClickDelete = () => {
    handleDelete(currentTabOutput._id);
    if (
      currentTabOutputIdx + 1 >= currentTabOutputList.length &&
      currentTabOutputIdx !== 0
    ) {
      setCurrentTabOutputIdx((i) => i - 1);
    }
  };

  React.useEffect(() => {
    if (currentTabOutputList.length > 0) {
      if (currentTabOutputIdx >= currentTabOutputList.length)
        setCurrentTabOutputIdx(currentTabOutputList.length - 1);
      else if (currentTabOutputIdx < 0) setCurrentTabOutputIdx(0);
    } else {
    }
  }, [currentTabOutputIdx, currentTabOutputList.length]);

  React.useEffect(() => {
    countChildrenOutput(currentTabOutputList.length);
  }, [currentTabOutputList.length]);

  return (
    <SingleOutputWrapper>
      <SingleOutputHeader>
        <ToolNameWrapper>
          <ToolName>{toolName}</ToolName>
          {load ? <Spinner width={15} height={15} /> : null}
        </ToolNameWrapper>
        {currentTab === TAB_ALL && (
          <GenerateButton
            disabled={
              load ||
              (bundleGenerateEvent.events === null &&
                currentTabOutputList?.length === 0)
            }
            onClick={onClickGenerate}
          >
            개별 생성 <IconReload />
          </GenerateButton>
        )}
      </SingleOutputHeader>
      <CarouselWrapper></CarouselWrapper>
      {currentTabOutputList?.length > 0 ? (
        <OutputWrapper
          key={currentTabOutput?._id}
          style={{
            display: "flex",
            flexDirection: "column-reverse",
          }}
        >
          <CarouselOutputCard
            isTriggerEvaluate={isTriggerEvaluate && index === 0}
            handleCloseEvaluate={handleCloseEvaluate}
            currentIdx={currentTabOutputIdx}
            maxIdx={currentTabOutputList.length}
            onIncrease={() =>
              setCurrentTabOutputIdx((c) =>
                c + 1 < currentTabOutputList.length ? c + 1 : c
              )
            }
            onDecrease={() =>
              setCurrentTabOutputIdx((c) => (c - 1 >= 0 ? c - 1 : c))
            }
            history={currentTabOutput}
            onClick={async () =>
              syncForms(currentTabOutput?.inputs, currentTabOutput?.options)
            }
            onDelete={() => {
              onClickDelete();
              refetchLikeList();
            }}
            onUpdate={(object) => {
              handleUpdate(currentTabOutput._id, object);
              refetchLikeList();
            }}
            plan={plan}
            parent={parent}
          />
        </OutputWrapper>
      ) : (
        <></>
      )}
    </SingleOutputWrapper>
  );
};

const SingleOutputWrapper = styled(FlexWrapper)`
  flex-direction: column;
  padding-bottom: 24px;
  width: 100%;
`;

const ToolName = styled.div`
  ${typo({ weight: 600, size: "16px", height: "100%", color: colors.gray_90 })};
`;

const ToolNameWrapper = styled(FlexWrapper)`
  gap: 9px;
`;

const SingleOutputHeader = styled(FlexWrapper)`
  width: 100%;
  justify-content: space-between;
  padding-bottom: 16px;
  min-height: 42px;
`;

const CarouselWrapper = styled.div``;

const GenerateButton = styled(FlexButton)`
  padding: 5px 14px;
  ${typo({ weight: 600, size: "14px", height: "16px", color: colors.WHITE })};
  background: ${colors.POINT_PURPLE};
  border-radius: 5px;
  gap: 8px;
  cursor: pointer;

  &:disabled {
    background: ${colors.gray_50};
    cursor: default;
  }

  &:hover {
    background: ${({ disabled }) =>
      disabled ? colors.gray_50 : colors.POINT_PURPLE_HOVER};
  }
`;
