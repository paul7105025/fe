import React from "react";
import { useRecoilValue } from "recoil";

import OutputHeader from "./OutputHeader";

import { OutputCard } from "@wrtn/ui/components/OutputCard";
import { ResultLoader } from "@wrtn/ui/components/ResultLoader";
import TriggerSpeechBubble from "@wrtn/ui/components/TriggerSpeechBubble/TriggerSpeechBubble";

import { EmptyContainer } from "./EmptyContainer";

import {
  useIntersect,
  useLikeFetch,
  useNonInitialEffect,
  useTriggerEvaluate,
  useTriggerAutoMake,
  //
  ToolType,
  OutputMetadata,
} from "@wrtn/core";
import { outputLoadingState, userState } from "@wrtn/core/stores";
import { TAB_ALL, TAB_FAVORITE } from "@wrtn/core/constants/common";

import {
  ContentWrapper,
  LastDiv,
  OutputWrapper,
  SafeWrapper,
  ShadowWrapper,
  LogoWrapper,
  PoweredByClovaLogo,
} from "./styles";
import { useCurrentOutput } from "../../hooks/useCurrentOutput";

interface OutputContainerProps {
  tool: ToolType;

  outputList: Array<OutputMetadata>;
  syncForms: (
    input: Array<any>,
    option: Array<any>,
    item?: OutputMetadata
  ) => void;
  onDeleteOutput: (outputId: string, item?: OutputMetadata) => void;
  onUpdateOutput: (outputId: string, data: any, item?: OutputMetadata) => void;

  stepHistory?: Array<{ outputId: string }>;
}

export const OutputContainer = ({
  tool,

  outputList,
  syncForms,
  onDeleteOutput,
  onUpdateOutput,

  stepHistory = [],
}: OutputContainerProps) => {
  const toolId = React.useMemo(() => tool._id, [tool]);

  const { isTriggerAutoMake, handleCloseAutoMake } = useTriggerAutoMake();
  const { isTriggerEvaluate, handleCloseEvaluate } = useTriggerEvaluate();

  const [currentTab, setCurrentTab] = React.useState(TAB_ALL);

  const outputLoading = useRecoilValue(outputLoadingState);
  const user = useRecoilValue(userState);

  const { refetchLikeList, fetchMore, likeList } = useLikeFetch({
    toolId,
    tab: currentTab,
  });

  const currentOutputList = useCurrentOutput({
    currentTab,
    likeList,
    outputList,
  });

  const wrapperRef = React.useRef<HTMLDivElement>(null);
  const scrollRef = React.useRef<HTMLDivElement>(null);

  const lastElementRef = useIntersect(async (entry, observer) => {
    observer.unobserve(entry.target);
    if (currentTab === TAB_FAVORITE) {
      fetchMore();
    }
  });

  const isSelectedInStep = React.useCallback(
    (_id: string) => {
      return stepHistory.findIndex((history) => history.outputId === _id) > -1;
    },
    [stepHistory]
  );

  const scrollToRef = () => {
    if (!scrollRef.current) return;
    scrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };

  useNonInitialEffect(() => {
    if (currentOutputList.length > 0) {
      scrollToRef();
    }
  }, [currentOutputList.length]);

  return (
    <React.Fragment>
      <SafeWrapper ref={wrapperRef}>
        <OutputHeader
          handleChangeTab={setCurrentTab}
          currentTab={currentTab}
          kind={tool.kind}
        />
        {isTriggerAutoMake && (
          <TriggerSpeechBubble
            handleClose={handleCloseAutoMake}
            toolId={toolId}
          />
        )}
        <ContentWrapper ref={scrollRef}>
          {currentOutputList.length > 0 ? (
            currentOutputList.map((item, idx) => {
              return (
                <OutputWrapper key={item?._id}>
                  <OutputCard
                    isTriggerEvaluate={isTriggerEvaluate && idx === 0}
                    handleCloseEvaluate={handleCloseEvaluate}
                    parent={tool}
                    history={item}
                    onClick={async () =>
                      syncForms(item?.inputs, item?.options, item)
                    }
                    onDelete={() => {
                      onDeleteOutput(item._id, item);
                      refetchLikeList();
                    }}
                    onUpdate={(object) => {
                      onUpdateOutput(item._id, object, item);
                      refetchLikeList();
                    }}
                    plan={user?.plan}
                    isSelectedInStep={isSelectedInStep(item?._id)}
                  />
                </OutputWrapper>
              );
            })
          ) : !outputLoading ? (
            <EmptyContainer youtubeId={tool?.tutorial?.youtubeId || null} />
          ) : (
            <ResultLoader />
          )}
          <div style={{ flex: 1 }} />
          {((currentTab === TAB_ALL && outputList.length > 0) ||
            (currentTab === TAB_FAVORITE && likeList.length > 0)) && (
            <LastDiv ref={lastElementRef} />
          )}
          <LogoWrapper>
            <PoweredByClovaLogo />
          </LogoWrapper>
        </ContentWrapper>
      </SafeWrapper>
      <ShadowWrapper />
    </React.Fragment>
  );
};
