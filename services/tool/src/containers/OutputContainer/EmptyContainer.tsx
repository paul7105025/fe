import React from "react";
import styled from "styled-components";
import YouTube, { YouTubeProps } from "react-youtube";

import { ModalPortal } from "@wrtn/ui/components/ModalPortal";

import { useIs1024, useIs1440, useIs480, useIsMobile } from "@wrtn/core";

import {
  IconPlayButton,
  FlexWrapper,
  FlexButton,
  WandGradient,
  typo,
  colors,
} from "@wrtn/ui";

const deskOpts: YouTubeProps["opts"] = {
  width: "1440",
  height: "810",
};

const pcOpts: YouTubeProps["opts"] = {
  width: "1024",
  height: "576",
};

const tabletOpts: YouTubeProps["opts"] = {
  width: "768",
  height: "432",
};

const mobileOpts: YouTubeProps["opts"] = {
  width: "360",
  height: "202",
};

export const EmptyContainer = ({ youtubeId }: { youtubeId: string | null }) => {
  const [isVideoModalOpen, setIsVideoModalOpen] = React.useState(false);

  const onClickVideo = () => {
    setIsVideoModalOpen(true);
  };

  const onPlayerReady: YouTubeProps["onReady"] = (event) => {
    event.target.pauseVideo();
  };

  const is1440 = useIs1440();
  const is1024 = useIs1024();
  const isTablet = useIsMobile();
  const isMobile = useIs480();

  const opts = React.useMemo(() => {
    if (isMobile) return mobileOpts;
    if (isTablet) return mobileOpts;
    if (is1024) return tabletOpts;
    if (is1440) return pcOpts;
    return deskOpts;
  }, [isMobile, isTablet, is1024, is1440]);

  return (
    <EmptyContentWrapper column justify="center">
      {typeof youtubeId === "string" && youtubeId.length > 0 ? (
        <EmptyIconWrapper onClick={onClickVideo}>
          <VideoThumbnailWrapper
            src={`https://img.youtube.com/vi/${youtubeId}/maxresdefault.jpg`}
          />
          <EmptyIconButton
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%) scale(1.5)",
            }}
          >
            <IconPlayButton />
          </EmptyIconButton>
        </EmptyIconWrapper>
      ) : (
        <EmptyIcon />
      )}
      <EmptyText>자동 생성을 눌러 다양한 표현을 받아보세요!</EmptyText>
      {isVideoModalOpen &&
        typeof youtubeId === "string" &&
        youtubeId.length > 0 && (
          <ModalPortal onClose={() => setIsVideoModalOpen(false)}>
            <YouTube videoId={youtubeId} opts={opts} onReady={onPlayerReady} />
          </ModalPortal>
        )}
    </EmptyContentWrapper>
  );
};

const EmptyContentWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 90%;
  gap: 16px;
  @media (max-width: 1023px) {
    height: 90%;
  }
`;

const VideoThumbnailWrapper = styled.img`
  width: 300px;
  height: 170px;

  margin-bottom: 11px;
  object-fit: cover;

  border-radius: 10px;
`;

const EmptyIconWrapper = styled(FlexWrapper)`
  display: flex;

  position: relative;
`;

const EmptyIconButton = styled(FlexButton)`
  cursor: pointer;
`;

const EmptyIcon = styled(WandGradient)`
  width: 48px;
  height: 48px;
`;

const EmptyText = styled.p`
  ${typo({
    weight: 600,
    size: "16px",
    color: colors.GRAY_60,
  })}
`;
