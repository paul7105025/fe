import React from "react";
import { RecoilRoot } from "recoil";
import styled from "styled-components";
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import {
  useQuery,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";

import ToolController from "../controller/ToolController";
import StepController from "../controller/StepController";
import BundleController from "../controller/BundleController";

import {
  BundleToolType,
  SingleToolType,
  StepToolType,
  useSync,
  useRefresh,
} from "@wrtn/core";
import { getTool_Id, getStep_Id, getBundle_Id } from "@wrtn/core/services";

import { ToastMessage } from "@wrtn/ui/components/ToastMessage";

import { ToolSkeletonBodyContainer } from "src/containers/SkeletonContainer";

import { FlexWrapper } from "@wrtn/ui";

const getTool = async ({ toolId }) => {
  const toolRes = await getTool_Id({ toolId });
  if (toolRes.status === 200 || toolRes.status === 201) {
    if (toolRes.data.data.kind === "tool")
      return toolRes.data.data as SingleToolType;
  }

  const stepRes = await getStep_Id({ stepId: toolId });

  if (stepRes.status === 200 || stepRes.status === 201) {
    if (stepRes.data.data && stepRes.data.data.kind === "step")
      return stepRes.data.data as StepToolType;
  }

  const bundleRes = await getBundle_Id({ bundleId: toolId });
  if (bundleRes.status === 200 || bundleRes.status === 201) {
    if (bundleRes.data.data && bundleRes.data.data.kind === "bundle")
      return bundleRes.data.data as BundleToolType;
  }

  return null;
};

const InnerPage: React.FC = () => {
  const pathname = window.location.pathname;
  const toolId = pathname.split("/")[2];

  const { data: tool } = useQuery({
    queryKey: [toolId],
    queryFn: () => getTool({ toolId }).then((res) => res),
    keepPreviousData: true,
  });

  if (!tool) {
    return (
      <ToolContentWrapper>
        <ToolSkeletonBodyContainer kind="single" />;
      </ToolContentWrapper>
    );
  }

  if (tool.kind === "tool")
    return (
      <ToolContentWrapper>
        <ToolController tool={tool} />
      </ToolContentWrapper>
    );

  if (tool.kind === "step")
    return (
      <ToolContentWrapper>
        <StepController step={tool} />
      </ToolContentWrapper>
    );

  if (tool.kind === "bundle")
    return (
      <ToolContentWrapper>
        <BundleController bundle={tool} />
      </ToolContentWrapper>
    );

  return null;
};

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

const ToolPage = () => {
  const { fetchFavorite } = useRefresh();
  useSync();

  React.useEffect(() => {
    fetchFavorite();
  }, []);

  return (
    <AlertProvider template={ToastMessage} {...alertOptions}>
      <QueryClientProvider client={queryClient}>
        <RecoilRoot override={false}>
          <InnerPage />
        </RecoilRoot>
      </QueryClientProvider>
    </AlertProvider>
  );
};

const ToolContentWrapper = styled(FlexWrapper)`
  width: 100%;
  height: 100%;
  @media (max-width: 1279px) {
    display: block;
    overflow: scroll;
  }
`;

export default ToolPage;
