import React from "react";
import styled from "styled-components";
import { useCookies } from "react-cookie";
import { positions, Provider as AlertProvider, transitions } from "react-alert";

//@ts-ignore
import { colors, FlexWrapper } from "@wrtn/ui/styles";
import { Spinner } from "@wrtn/ui/components/Spinner";

import { useIs1024, useSync } from "@wrtn/core";

import { TitleInput } from "../components/TitleInput";
import EditorStatusIndicator from "../components/EditorStatusIndicator";

import { ToastMessage } from "@wrtn/ui/components/ToastMessage";

import useDocInit from "../hooks/useDocInit";

import EditorContainer from "../containers/EditorContainer";
import TutorialContainer from "../containers/TutorialContainer";
import BlockMobileContainer from "../containers/BlockMobileContainer";
import TutorialEditorContainer from "../containers/EditorContainer/TutorialEditorContainer";
import { tutorialState } from "../containers/TutorialContainer/tutorialState";

const EditorDocumentPage = ({ onClickLNB, isOpenLNB, onClickChannel }) => {
  const tutorialRef = React.useRef([null, null, null, null]);

  const [isTutorial, setIsTutorial] = React.useState(true);
  const [tutorialStage, setTutorialStage] = React.useState(0);

  const [cookies, setCookies] = useCookies(["wrtn_editor_tutorial"]);

  const pathname = window.location.pathname;
  // const docId = pathname.split("/")[2];
  const [docId, setDocId] = React.useState("");

  React.useEffect(() => {
    setDocId(pathname.split("/")[2]);
  }, []);

  const isBlocked = useIs1024();

  React.useEffect(() => {
    if (cookies["wrtn_editor_tutorial"]) setIsTutorial(false);
  }, []);

  React.useEffect(() => {
    if (tutorialStage > 3) {
      setCookies("wrtn_editor_tutorial", true);
    }
  }, [tutorialStage]);

  React.useEffect(() => {
    if (!isTutorial) {
      setCookies("wrtn_editor_tutorial", true);
    }
  }, [isTutorial]);

  const {
    title,
    content,
    metadata,
    loading,
    saveStatus,
    handleChangeTitle,
    setSaveStatus,
  } = useDocInit(docId);

  React.useEffect(() => {
    const preventExit = (e) => {
      e.preventDefault();

      if (saveStatus === "saving") {
        e.preventDefault();
        e.returnValue = "";
      }
    };

    window.addEventListener("beforeunload", preventExit);

    return () => {
      window.removeEventListener("beforeunload", preventExit);
    };
  }, [saveStatus]);

  //

  return (
    <Wrapper>
      <HeaderWrapper justify="center">
        <TitleWrapper>
          <TitleInput title={title} handleChangeTitle={handleChangeTitle} />
          <EditorStatusIndicator saveStatus={saveStatus} />
        </TitleWrapper>
      </HeaderWrapper>
      {isBlocked && (
        <BlockMobile>
          <BlockMobileContainer />
        </BlockMobile>
      )}
      <ContentWrapper>
        {loading ? (
          <Spinner width={undefined} height={undefined} color={undefined} />
        ) : (
          <>
            {!isTutorial && (
              <EditorContainer
                title={title}
                docId={docId}
                content={content}
                metadata={metadata}
                setSaveStatus={setSaveStatus}
                tutorialRef={tutorialRef}
              />
            )}
            {isTutorial && tutorialStage >= 2 && (
              <TutorialEditorContainer
                key="1"
                docId={docId}
                // @ts-ignore
                content={tutorialState}
                metadata={metadata}
                setSaveStatus={setSaveStatus}
                tutorialRef={tutorialRef}
                tutorialStage={tutorialStage}
              />
            )}
            {isTutorial && tutorialStage < 2 && (
              <TutorialEditorContainer
                key="2"
                docId={docId}
                content={undefined}
                metadata={metadata}
                setSaveStatus={setSaveStatus}
                tutorialRef={tutorialRef}
                tutorialStage={tutorialStage}
              />
            )}
            {!isBlocked && isTutorial && (
              <TutorialContainer
                targetRef={tutorialRef}
                tutorialStage={tutorialStage}
                setTutorialStage={setTutorialStage}
                setIsTutorial={setIsTutorial}
              />
            )}
          </>
        )}
      </ContentWrapper>
    </Wrapper>
  );
};

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

const EditorDocumentPage1 = (props) => {
  useSync();

  return (
    <AlertProvider template={ToastMessage} {...alertOptions}>
      <EditorDocumentPage {...props} />
    </AlertProvider>
  );
};

export default EditorDocumentPage1;

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  position: relative;
`;

const HeaderWrapper = styled(FlexWrapper)`
  height: 74px;
  border-bottom: 1px solid ${colors.WHITE_BOLDER_LINE};
  padding: 0px 30px;
`;

const ContentWrapper = styled(FlexWrapper)`
  height: calc(100% - 74px);
`;

const BlockMobile = styled.div`
  @media (max-width: 1180px) {
    position: absolute;
    right: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    background-color: ${colors.WHITE};
    z-index: 10;
  }
`;

const TitleWrapper = styled(FlexWrapper)`
  margin-left: 115px;
  gap: 15px;
  overflow: hidden;
`;
