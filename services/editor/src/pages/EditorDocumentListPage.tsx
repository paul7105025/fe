import React from "react";
import { RecoilRoot } from "recoil";
import { useAlert } from "react-alert";
import styled from "styled-components";
import { useRecoilValue } from "recoil";
import { positions, Provider as AlertProvider, transitions } from "react-alert";
import {
  QueryClient,
  QueryClientProvider,
  useQuery,
} from "@tanstack/react-query";

import {
  deleteDoc,
  getDocCount,
  getDocList,
  postDoc,
} from "@wrtn/core/services";
import {
  docType,
  userState,
  nextRouteState,
  useSync,
  useEvent,
  useTagManager,
} from "@wrtn/core";

import { Spinner } from "@wrtn/ui/components/Spinner";
import { ToastMessage } from "@wrtn/ui/components/ToastMessage";
import { Pagination } from "@wrtn/ui/components/Pagination";
import { OutlineButton } from "@wrtn/ui/components/Button/OutlineButton";

import { TrashCan } from "@wrtn/ui";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui/styles";

import DocTableItem from "../components/DocTableItem";
import { LandingContainer } from "src/containers/LandingContainer";

const PAGINATION_LIMIT = 10;

export type tabType = "전체";

const tabList: Array<tabType> = ["전체"];

const EditorDocumentListPage = () => {
  useSync();

  const user = useRecoilValue(userState);
  const router = useRecoilValue(nextRouteState);

  const alert = useAlert();
  const { collectEvent, collectUserProperties_increment } = useEvent();

  const [page, setPage] = React.useState<number>(0);
  const [currentTab, setCurrentTab] = React.useState<tabType>("전체");
  const [selectedItems, setSelectedItems] = React.useState<Array<string>>([]);

  const tagManager = useTagManager();

  const {
    data: docList,
    isLoading: docListLoading,
    refetch: refetchDocList,
  } = useQuery<Array<docType>, Error>({
    queryKey: ["historyList", currentTab, page],
    queryFn: () =>
      getDocList({
        page: page + 1,
        limit: PAGINATION_LIMIT,
      }).then((res) => res.data.data),
  });

  const { data: docCount, refetch: refetchDocCount } = useQuery<number, Error>({
    queryKey: ["historyCount"],
    queryFn: () => getDocCount().then((res) => res.data.data),
  });

  const maxPage =
    typeof docCount === "number" ? Math.ceil(docCount / PAGINATION_LIMIT) : 0;

  const createDoc = async () => {
    const res = await postDoc();

    if (res.data.result === "SUCCESS") {
      tagManager({
        event: "click_tool_card",
        data: null,
      });

      collectEvent("create_editor_document");
      collectUserProperties_increment("editor_document_count", 1);
      router.push(`/editor/${res.data.data._id}`);
    }
  };

  const enterDoc = (id: string) => {
    router.push(`/editor/${id}`);
  };

  const removeDoc = async () => {
    //TODO : 동시접속 관련 로직 필요함

    await deleteDoc({
      docIdList: selectedItems,
    });

    setSelectedItems([]);

    alert.show("문서를 삭제했어요.");

    await refetchDocList();
    await refetchDocCount();
  };

  if (!user) {
    return (
      <Wrapper>
        <LandingContainer isLogin={false} createDoc={createDoc} />
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      <TitleWrapper>
        <Title>에디터</Title>
        <div style={{ flex: 1 }} />
        <OutlineButton
          icon="plus"
          buttonProps={{
            onClick: createDoc,
          }}
        >
          문서 생성하기
        </OutlineButton>
      </TitleWrapper>
      {docListLoading ? (
        <EmptyWrapper>
          <Spinner />
        </EmptyWrapper>
      ) : docList && docList.length > 0 ? (
        <DocTableItemWrapper>
          <ContentWrapper style={{ marginTop: 40 }}>
            <DeleteButton
              isActive={selectedItems.length > 0}
              disabled={selectedItems.length === 0}
              onClick={removeDoc}
            >
              <TrashCan />
              선택 항목 삭제
            </DeleteButton>
            <DocTableItem
              docList={docList}
              selectedItems={selectedItems}
              setSelectedItems={setSelectedItems}
              onClick={enterDoc}
            />
          </ContentWrapper>
          <div style={{ flex: 1 }} />
          <PaginationWrapper>
            {maxPage > 0 && (
              <Pagination
                maxPage={maxPage}
                currentPage={page}
                setPage={setPage}
              />
            )}
          </PaginationWrapper>
        </DocTableItemWrapper>
      ) : (
        <LandingContainer isLogin={true} createDoc={createDoc} />
      )}
    </Wrapper>
  );
};

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

const EditorDocumentListPage1 = (props) => {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <AlertProvider template={ToastMessage} {...alertOptions}>
        <RecoilRoot override={false}>
          <EditorDocumentListPage {...props} />
        </RecoilRoot>
      </AlertProvider>
    </QueryClientProvider>
  );
};

export default EditorDocumentListPage1;

const Wrapper = styled(FlexWrapper)`
  height: 100%;
  width: 100%;
  overflow: auto;

  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

const TitleWrapper = styled(FlexWrapper)`
  width: 100%;
  padding: 4rem 128px 2rem;
  justify-content: space-between;
  @media (max-width: 767px){
    gap: 10px;
    padding: 4rem 40px 2rem;
    justify-content: flex-start;
  }
`;

const Title = styled.h1`
  ${typo({
    size: "24px",
    weight: 600,
    color: colors.GRAY_90,
  })}
`;

const ContentWrapper = styled(FlexWrapper)`
  width: 100%;
  flex-direction: column;
  align-items: flex-start;

  padding: 0px 130px;
  @media (max-width: 1280px) {
    padding: 0px 80px;
  }
  @media (max-width: 480px) {
    padding: 0px 40px;
  }
`;

const DeleteButton = styled(FlexButton)<{ isActive: boolean }>`
  gap: 7px;
  margin-bottom: 28px;
  padding: 5px 0px;

  cursor: ${({ isActive }) => (isActive ? "pointer" : "default")};

  ${({ isActive }) =>
    typo({
      size: "14px",
      weight: 600,
      color: isActive ? colors.GRAY_60 : colors.GRAY_55,
    })}

  svg {
    path {
      fill: ${({ isActive }) => (isActive ? colors.GRAY_60 : colors.GRAY_55)};
    }
  }

  :hover {
    svg {
      path {
        fill: ${({ isActive }) =>
          isActive ? colors.POINT_PURPLE : colors.GRAY_55};
      }
    }
    color: ${({ isActive }) =>
      isActive ? colors.POINT_PURPLE : colors.GRAY_55};
  }
`;

const DocTableItemWrapper = styled(FlexWrapper)`
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  width: 100%;
  flex: 1;
`;

const EmptyWrapper = styled(FlexWrapper)`
  flex-direction: column;
  gap: 21px;

  width: 100%;
  flex: 1;

  @media (max-width: 767px) {
    top: 100px;
  }
`;

const PaginationWrapper = styled(FlexWrapper)`
  width: 100%;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 40px;
`;
