import styled from "styled-components";

import { useLoginDialog } from "@wrtn/core";
import { colors, FlexButton, FlexWrapper, typo } from "@wrtn/ui";

import EditorLandingImage from "../../assets/image/editor_landing.png";

interface LandingContainerProps {
  isLogin: boolean;
  createDoc: () => Promise<void>;
}

export const LandingContainer = ({
  isLogin,
  createDoc,
}: LandingContainerProps) => {
  const { handleOpen } = useLoginDialog();

  const handleStart = () => {
    if (isLogin) {
      createDoc();
    } else {
      handleOpen();
    }
  };

  return (
    <LandingWrapper column>
      <LadingCatchphrase>
        긴 문서 작성도 막힘 없이 빠르게 완성!
      </LadingCatchphrase>
      <LadingDescription>
        Ctrl + Enter 만 누르세요.
        <br />
        뤼튼이 자동으로 이어서 써드립니다!
      </LadingDescription>
      <StartButton onClick={handleStart}>지금 사용해보기</StartButton>
      <LadingImage src={EditorLandingImage} />
    </LandingWrapper>
  );
};

const LandingWrapper = styled(FlexWrapper)`
  width: 100%;
  max-width: 1024px;
  flex: 1;

  padding: 70px 20px 0px;
`;

const LadingCatchphrase = styled.p`
  ${typo({
    size: "24px",
    weight: 700,
    color: colors.POINT_PURPLE,
    isStatic: true,
  })}

  text-align: center;

  margin-bottom: 20px;
  @media(max-width: 767px){
    ${typo({
    size: "16px",
    weight: 700,
    color: colors.POINT_PURPLE,
    isStatic: true,
  })}
  }
`;

const LadingDescription = styled.p`
  ${typo({
    size: "32px",
    weight: 700,
    height: "140%",
    color: colors.GRAY_90,
    isStatic: true,
  })}

  

  text-align: center;

  margin-bottom: 23px;
  @media(max-width: 767px){
    ${typo({
    size: "20px",
    weight: 700,
    height: "140%",
    color: colors.GRAY_90,
  })}
  }
`;

const StartButton = styled(FlexButton)`
  padding: 4px 16px;
  background-color: ${colors.POINT_PURPLE};
  border-radius: 5px;

  ${typo({
    size: "16px",
    weight: 600,
    height: "162%",
    color: colors.WHITE,
    isStatic: true,
  })}

  margin-bottom: 30px;

  &:hover {
    cursor: pointer;
    background-color: ${colors.POINT_PURPLE_HOVER};
  }
`;

const LadingImage = styled.img`
  max-width: 830px;
  width: 100%;
`;
