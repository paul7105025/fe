export const tutorialState = {
  "root": {
      "children": [
          {
              "children": [
                  {
                      "detail": 0,
                      "format": 0,
                      "mode": "normal",
                      "style": "",
                      "text": "초코 쿠키를 만드는 방법은 어렵지 않다. 사람들이 잘 모르는 나만의 초코 쿠키 만들기 비법을 공개하려고 한다. 일단 밀가루 1컵을 체에 치고 녹인 버터 3 밥숟가락을 그 위에 천천히 붓는다. 버터를 붓고 나서 밀가루와 버터를 잘 섞어준 후 초코가루 반컵, 초코칩을 적당히 넣고 동그랗게 빚는다. 이후 전자레인지에 3분간 돌려주면 완성! 이 레시피는 구체적인 계량도 필요없다. 왜냐하면 자신의 취향대로 재료를 가감하면 되기 때문이다.",
                      "type": "HighlightText",
                      "version": 1,
                      "className": "WrtnEditor_highlightText",
                      "tutorial": true,
                  },
              ],
              "direction": "ltr",
              "format": "",
              "indent": 0,
              "type": "paragraph",
              "version": 1
          }
      ],
      "direction": "ltr",
      "format": "",
      "indent": 0,
      "type": "root",
      "version": 1
  }
};