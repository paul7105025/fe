import React from "react";
import styled from "styled-components";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import { OutlineButton } from "@wrtn/ui/components/Button/OutlineButton";

import { typo, colors, FlexWrapper } from "@wrtn/ui/styles";

import { Close } from "@wrtn/ui/assets";

import { Divider } from "../../components";
import TutorialBubble from "../../components/TutorialBubble";
import {
  continualHotKey,
  rewriteHoyKey,
} from "../../plugins/CustomGenDialogPlugin";

type TutorialContainerType = {
  targetRef: React.MutableRefObject<any>;
  tutorialStage: number;
  setTutorialStage: React.Dispatch<React.SetStateAction<number>>;
  setIsTutorial: React.Dispatch<React.SetStateAction<boolean>>;
};

const TutorialBubbleWrapper = styled.div<{ left: number; top: number }>`
  position: absolute;
  left: ${(props) => props.left}px;
  top: ${(props) => props.top}px;
`;

const TutorialContainer = ({
  targetRef,
  tutorialStage,
  setTutorialStage,
  setIsTutorial,
}: TutorialContainerType) => {
  // const rs = React.useRef<any>(null);
  const [position, setPosition] = React.useState({
    top: null,
    left: null,
  });

  const [size, setSize] = React.useState({
    width: null,
    height: null,
  });

  const handleNextStage = () => {
    setTutorialStage((c) => {
      if (c === 3) {
        setIsTutorial(false);
      }
      return c + 1;
    });
  };

  const handleClose = () => {
    setIsTutorial(false);
  };

  React.useEffect(() => {
    setPosition(
      bubbleAlign(targetRef?.current[tutorialStage]?.getBoundingClientRect())
    );
  }, [targetRef, tutorialStage]);

  React.useEffect(() => {
    setSize({
      width: targetRef?.current[tutorialStage]?.getBoundingClientRect().width,
      height: targetRef?.current[tutorialStage]?.getBoundingClientRect().height,
    });
  }, [targetRef, tutorialStage]);

  const bubbleDirection: { direction: string; position: number } =
    React.useMemo(() => {
      if (tutorialStage === 0) {
        return { direction: "left", position: 34 };
      } else if (tutorialStage === 1) {
        return { direction: "left", position: 104 };
      } else if (tutorialStage === 2) {
        return { direction: "top", position: 297 };
      } else if (tutorialStage === 3) {
        return { direction: "left", position: 79 };
      }
      return { direction: "left", position: 0 };
    }, [tutorialStage]);

  const bubbleAlign = React.useCallback(
    (pos: any) => {
      if (tutorialStage === 0) {
        return {
          top: pos.top - 20,
          left: pos.right + 45,
        };
      } else if (tutorialStage === 1) {
        return {
          top: pos.top - 92,
          left: pos.right,
        };
      } else if (tutorialStage === 2) {
        return {
          top: pos.top + 220,
          left: (pos.right + pos.left) / 2 - 310,
        };
      } else if (tutorialStage === 3) {
        return {
          top: pos.top - 30,
          left: pos.right,
        };
      }
      return {
        top: pos.top,
        left: pos.right,
      };
    },
    [tutorialStage]
  );

  const bubbleTitle = React.useMemo(() => {
    if (tutorialStage === 0) {
      return <>작성할 글에 대한 기본적인 정보를 입력해주세요.</>;
    } else if (tutorialStage === 1) {
      return <>기본 세팅을 입력한 후 자동 생성 버튼을 눌러주세요.</>;
    } else if (tutorialStage === 2) {
      return (
        <>
          생성된 글 뒤로 바로 이어서 생성하거나, 해당 부분을 다시 생성해보세요.
        </>
      );
    } else if (tutorialStage === 3) {
      return <>기본 세팅을 입력한 후 자동 생성 버튼을 눌러주세요.</>;
    }
    return <></>;
  }, [tutorialStage]);

  const bubbleDesc = React.useMemo(() => {
    if (tutorialStage === 0) {
      return <>기본 세팅을 작성해야만 AI의 도움을 받을 수 있어요!</>;
    } else if (tutorialStage === 1) {
      return (
        <>
          단축키 <HotKey>{continualHotKey}</HotKey> 로도 생성할 수 있어요.
        </>
      );
    } else if (tutorialStage === 2) {
      return (
        <div style={{ display: "flex", gap: "24px", flexDirection: "column" }}>
          <TutorialDescHorizontal>
            <div>이어쓰기</div>
            <VerticalDivider />
            <div style={{ lineHeight: "140%", whiteSpace: "nowrap" }}>
              문장이 삽입되기를 원하는 부분에 커서를 둔 후, 단축키{" "}
              <HotKey>{continualHotKey}</HotKey> 또는 좌측 하단 이어쓰기 버튼을
              눌러 이어지는 문장을 생성할 수 있어요.
            </div>
          </TutorialDescHorizontal>
          <TutorialDescHorizontal>
            <div>다시쓰기</div>
            <VerticalDivider />
            <div>
              단축키 <HotKey>{rewriteHoyKey}</HotKey> 를 눌러 생성된 문장을 다시
              생성할 수 있어요.
            </div>
          </TutorialDescHorizontal>
        </div>
      );
    } else if (tutorialStage === 3) {
      return (
        <>
          온점(.), 느낌표(!), 물음표(?)로 완결된 문장의 뒤에 커서를
          <br /> 가져다 두면, 방향성 옵션이 활성화돼요.
        </>
      );
    }
    return <></>;
  }, [tutorialStage]);

  return (
    <ModalPositionPortal
      position={{
        left: 0,
        top: 0,
      }}
    >
      <TutorialBubbleWrapper top={position.top || 0} left={position.left || 0}>
        <TutorialBubble {...bubbleDirection}>
          <Wrapper>
            <TutorialTitle>{bubbleTitle}</TutorialTitle>
            <TutorialDescription>{bubbleDesc}</TutorialDescription>
            <Divider lineColor={colors.WHITE} style={{ margin: "15px 0px" }} />
            <BottomWrapper>
              <StageWrapper>
                <Stage>{tutorialStage + 1}/4</Stage>
                <Skip onClick={handleClose}>건너뛰기</Skip>
              </StageWrapper>
              <Next
                buttonProps={{
                  onClick: handleNextStage,
                }}
              >
                다음으로
              </Next>
            </BottomWrapper>
          </Wrapper>
        </TutorialBubble>
      </TutorialBubbleWrapper>
    </ModalPositionPortal>
  );
};

export default TutorialContainer;

// const Next = styled.div`
//   cursor: pointer;
//   position: absolute;
//   bottom: 23px;
//   right: 22px;
// `;

const Show = styled.div<{
  width: number;
  height: number;
  top: number;
  left: number;
}>`
  box-shadow: rgb(0, 0, 0) 0px 0px 0px 0px,
    rgb(59 63 78 / 40%) 0px 0px 0px 3141592px;
  opacity: 1;
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;
  left: ${(props) => props.left}px;
  top: ${(props) => props.top}px;
  transition: all 0.34s ease-out;
  border-radius: 4px;
  position: absolute;
`;

const Wrapper = styled.div`
  position: relative;
  max-width: 578px;
`;

const CloseIcon = styled(Close)`
  cursor: pointer;
  position: absolute;
  right: 27px;
  top: 37px;
  width: 22px;
  height: 22px;
  > path {
    fill: ${colors.WHITE} !important;
  }
`;

const TutorialTitle = styled.div`
  white-space: nowrap;
  display: inline-block;
  padding-bottom: 6px;
  ${typo({ weight: 700, size: "18px", height: "130%", color: colors.WHITE })};
`;

const TutorialDescription = styled.div`
  white-space: nowrap;
  display: inline-block;
  ${typo({ weight: 500, size: "16px", height: "130%", color: colors.WHITE })};
`;

const HotKey = styled.span`
  ${typo({ weight: 700, size: "14px", height: "100%", color: colors.WHITE })};
  border: 1px solid #ffffff;
  border-radius: 5px;
  padding: 3px 5px;
  white-space: normal;
`;

const BottomWrapper = styled(FlexWrapper)`
  justify-content: space-between;
`;

const StageWrapper = styled(FlexWrapper)`
  gap: 15px;
`;

const Stage = styled.div`
  ${typo({ weight: 600, size: "16px", height: "23px", color: colors.WHITE })};
`;

const Skip = styled.div`
  cursor: pointer;
  text-decoration-line: underline;
  ${typo({ weight: 500, size: "14px", height: "130%", color: colors.WHITE })};
`;

const Next = styled(OutlineButton)`
  background-color: ${colors.WHITE};
  color: ${colors.POINT_PURPLE};
  padding-top: 4px;
  padding-bottom: 4px;
`;

const TutorialDescHorizontal = styled.div`
  display: flex;
  gap: 17px;
`;

const VerticalDivider = styled.div`
  border: 0.5px solid #ffffff;
  height: 17px;
  margin-top: 2px;
`;
