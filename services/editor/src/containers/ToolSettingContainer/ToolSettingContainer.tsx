import React from "react";
import { useAlert } from "react-alert";

import styled from "styled-components";
import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";

import { useEvent, useTagManager } from "@wrtn/core";

import { ListSelect } from "@wrtn/ui/components/Select";
import { GuideButton } from "@wrtn/ui/components/GuideButton";
import { DefaultSpinner } from "@wrtn/ui/components/DefaultSpinner";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { Divider } from "../../components";
import GradientButton from "../../components/GradientButton";

import { IS_APPLE } from "../../plugins/CustomKeyBindingPlugin/utils";
import { useGlobalContext } from "../../plugins/GlobalPlugin/Context";
import useEditorGenerate from "../../plugins/GeneratePlugin/useEditorGenerate";
import { useGenerateContext } from "../../plugins/GeneratePlugin/GenerateContext";
import { useCollectDataContext } from "../../plugins/CollectDataPlugin/CollectDataPlugin";

import { categoryList, connectList, docMetaType } from "../../types";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";
import { MagicWand } from "@wrtn/ui/assets/SVGComponent/Icon";

interface ToolSettingContainerProps {
  documentMetadata: docMetaType;
  setMeta: (key: keyof docMetaType, value: any) => void;
  formatMeta: (prevText: string) => {
    inputs: string[];
    connect: string;
    docId: string | undefined;
  };
  isFocus: boolean;
  isSentence: boolean;
  currentSelection: any;
  tutorialRef: any;
}

const ToolSettingContainer = ({
  documentMetadata,
  setMeta,
  formatMeta,

  isFocus,
  isSentence,

  currentSelection,
  tutorialRef,
}: ToolSettingContainerProps) => {
  const [editor] = useLexicalComposerContext();
  const tagManager = useTagManager();

  const { collect, type } = useCollectDataContext();
  const { textState } = useGlobalContext();
  const { collectUserProperties, collectUserProperties_once } = useEvent();
  const [text] = textState;

  const { genNodeKeyState, genCountState, genLoadingState, genDialogState } =
    useGenerateContext();
  const { collectEvent } = useEvent();

  const [genNodeKey, setGenNodeKey] = genNodeKeyState;
  const [genCount, setGenCount] = genCountState;
  const [genLoading, setGenLoading] = genLoadingState;
  const [genDialog, setGenDialog] = genDialogState;

  const alert = useAlert();

  const [tempKeyword, setTempKeyword] = React.useState("");

  const buttonRef = React.useRef<HTMLDivElement>(null);

  const [hover, setHover] = React.useState(false);

  const { topic, category, connect } = documentMetadata;

  const { handleGenerateContinual } = useEditorGenerate({
    metadata: documentMetadata,
    formatMeta,
  });

  const handleGenerate = (e: Event) => {
    e.preventDefault();
    e.stopPropagation();
    setMeta("connect", "선택 안 함");
    tagManager({
      event: "editor_click_output_btn",
    });
    collectEvent("click_generate_btn", {
      feature_menu: "editor",
      generate_position: "editor",
      feature_category: "",
      feature_name: "이어쓰기",
      feature_tag: "",
      feature_form: "",
      feature_prompt: "",
      prompt_id: [],
      repeat_count: 1,
      method: "button",
    });
    return handleGenerateContinual({
      currentSelection: currentSelection,
      isFocus: isFocus,
    });
  };

  const handleChangeTopic = (val: string) => setMeta("topic", val);
  const handleChangeCategory = (val: string) => setMeta("category", val);
  const handleChangeDirection = (val: string) => setMeta("connect", val);

  const generateText = React.useMemo(() => {
    if (genLoading) {
      if (topic.trim().length > 0 && text.length > 1) return "이어 쓰는 중";
      else return "자동 생성 중";
    }
    if (IS_APPLE) {
      if (topic.trim().length > 0 && text.length > 1)
        return "이어쓰기 ⌘ + Enter";
      return "자동 생성 ⌘ + Enter";
    } else {
      if (topic.trim().length > 0 && text.length > 1)
        return "이어쓰기 Ctrl + Enter";
      return "자동 생성 ⌘ + Enter";
    }
  }, [topic, text, genLoading]);

  return (
    <Wrapper>
      <ScrollWrapper>
        <InsideWrapper>
          <InputWrapper>
            <LabelWrapper>
              <Title>기본 세팅</Title>
              <GuideButton
                guide={{
                  isVisible: true,
                  text: "글에 대한 기본 정보를 세팅해야 에디터를 사용할 수 있어요. 기본 정보를 토대로 글의 초안을 생성하고, 이어써보세요. 기본 세팅 내용은 언제든지 변경할 수 있습니다.",
                  tag: "",
                }}
              />
            </LabelWrapper>
          </InputWrapper>
          <DefaultSettingWrapper>
            <InputWrapper>
              <LabelWrapper>
                <InputLabel>글의 주제</InputLabel>
                <GuideButton
                  guide={{
                    isVisible: true,
                    text: "작성할 글의 주제를 키워드를 담아 작성해주세요.",
                    tag: "",
                  }}
                />
              </LabelWrapper>
              <SettingTextInput
                ref={(el) => (tutorialRef.current[0] = el)}
                placeholder="초코 쿠키를 만드는 법"
                value={topic}
                onChange={(e) => handleChangeTopic(e.target.value)}
              />
            </InputWrapper>
            <InputWrapper>
              <LabelWrapper>
                <InputLabel>카테고리</InputLabel>
                <GuideButton
                  guide={{
                    isVisible: true,
                    text: "작성할 글의 카테고리를 선택해주세요.",
                    tag: "",
                  }}
                />
              </LabelWrapper>
              <ListSelect
                list={categoryList}
                setValue={handleChangeCategory}
                value={category}
                disabled={false}
              />
            </InputWrapper>
          </DefaultSettingWrapper>
          <div style={{ flex: 1 }} />
          <Divider
            lineColor={colors.WHITE_BOLDER_LINE}
            style={{ marginBottom: 30 }}
          />
          <div ref={(el) => (tutorialRef.current[3] = el)}>
            <InputWrapper>
              <LabelWrapper>
                <InputLabel disabled={!isSentence}>
                  연결될 내용의 방향성
                </InputLabel>
                <GuideButton
                  disabled={!isSentence}
                  guide={{
                    isVisible: true,
                    text: "연결될 내용의 방향성을 결정해주세요. 온점(.), 느낌표(!), 물음표(?) 로 끝난 완결된 문장의 뒤에 커서를 가져다 두어야 방향성 옵션이 활성화됩니다.",
                    tag: "",
                  }}
                />
              </LabelWrapper>
              <ListSelect
                reverse
                list={connectList}
                setValue={handleChangeDirection}
                value={connect}
                // disabled={false}
                disabled={!isSentence}
              />
            </InputWrapper>
          </div>
        </InsideWrapper>
      </ScrollWrapper>
      <div style={{ flex: 1 }} />
      <div ref={(el) => (tutorialRef.current[1] = el)}>
        <ButtonWrapper
          ref={buttonRef}
          onMouseOver={() => setHover(true)}
          onMouseOut={() => setHover(false)}
        >
          <GradientButton
            handleGenerate={handleGenerate}
            disabled={genLoading || topic.trim().length === 0}
          >
            {genLoading ? (
              <DefaultSpinner width={18} height={18} color={colors.WHITE} />
            ) : (
              <MagicWand />
            )}
            <GradientButtonTitle>{generateText}</GradientButtonTitle>
          </GradientButton>
        </ButtonWrapper>
      </div>
      {hover && topic.trim().length === 0 && (
        <ModalPositionPortal
          position={{
            top: buttonRef.current
              ? buttonRef.current.getBoundingClientRect().top - 25
              : 0,
            left: buttonRef.current
              ? buttonRef.current.getBoundingClientRect().left +
                (buttonRef.current.getBoundingClientRect().width - 201) / 2
              : 0,
          }}
        >
          <HoverWrapper>기본 세팅을 입력해야 생성할 수 있어요.</HoverWrapper>
        </ModalPositionPortal>
      )}
    </Wrapper>
  );
};

export default ToolSettingContainer;

const HoverWrapper = styled.div`
  padding: 4px 9px;
  background-color: ${colors.gray_80};
  box-shadow: 0px 0px 5px rgba(56, 71, 130, 0.25);
  border-radius: 5px;

  ${typo({
    weight: 600,
    size: "12px",
    height: "100%",
    color: colors.WHITE,
  })};
`;

const Wrapper = styled.div`
  width: 285px;
  background-color: #f2f7ff;
  height: 100%;
  z-index: 1;
  @media (max-width: 1280px) {
    width: 230px;
  }
`;

const InsideWrapper = styled.div`
  display: flex;
  height: calc(100%);
  flex-direction: column;
  padding: 38px 0px 20px;
`;

const Title = styled.h3`
  ${typo({
    weight: 700,
    size: "18px",
    color: colors.GRAY_90,
  })}
`;

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 20px;
  gap: 12px;
`;

const SettingTextInput = styled.input`
  background-color: ${colors.WHITE};
  border-radius: 8px;
  width: 100%;
  padding: 12px 16px;
  ${typo({
    weight: 500,
    size: "16px",
    height: "100%",
    color: colors.gray_100,
  })};

  &::placeholder {
    ${typo({
      color: colors.gray_55,
    })}
  }

  &:focus {
    padding: 11px 15px;
    border: 1px solid ${colors.ACTION_BLUE};
  }
`;

const InputLabel = styled.p<{ disabled?: boolean }>`
  ${typo({ weight: 600, size: "16px", height: "100%", color: colors.gray_80 })};
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
`;

const LabelWrapper = styled(FlexWrapper)`
  justify-content: flex-start;
  gap: 7px;
`;

const ButtonWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 42px;
  padding: 0px 20px;
`;

const GradientButtonTitle = styled.p`
  padding: 6px 10px;
  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: colors.WHITE,
  })}
`;

const ScrollWrapper = styled.div`
  height: calc(100% - 58px);

  overflow: scroll;
`;

const DefaultSettingWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 32px 0px 20px;
  gap: 24px;
`;

// const KeywordLabel = styled.p`
//   ${typo({
//     weight: 500,
//     size: "12px",
//     height: "12px",
//     color: colors.gray_60,
//   })};
// `;

// const KeywordWrapper = styled(FlexWrapper)`
//   justify-content: flex-start;
// `;

// const Keyword = styled(FlexWrapper)`
//   background: ${colors.WHITE};
//   border-radius: 30px;
//   padding: 8px 12px;

//   ${typo({
//     weight: 500,
//     size: "16px",
//     height: "100%",
//     color: colors.gray_90,
//   })};

//   border: 1px solid ${colors.BLUE_GRAY_LINE};
//   gap: 4px;

//   cursor: pointer;

//   &:hover {
//     background-color: ${colors.BACKGROUND};
//   }
// `;

// const CloseIcon = styled(Close)`
//   width: 18px;
//   height: 18px;

//   path {
//     fill: ${colors.gray_70};
//   }
// `;
