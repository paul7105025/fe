import React from "react";
import styled from "styled-components";
import { colors, typo, FlexButton, FlexWrapper } from "@wrtn/ui/styles";
import { ReactComponent as More } from "../../assets/icons/icon_more.svg";
import { ReactComponent as ArrowRight } from "../../assets/icons/icon_carousel_down.svg";
import { useClickOutside } from "@wrtn/core";
import GradientButton from "../../components/GradientButton";
import {
  continualHotKey,
  rebaseHotKey,
  rewriteHoyKey,
} from "../../plugins/CustomGenDialogPlugin";
import { useExportContext } from "../../plugins/ExportPlugin/ExportContext";
import { useGlobalContext } from "../../plugins/GlobalPlugin/Context";

const MenuItem = ({ list, isChild }) => {
  const [selectedNumber, setSelectedNumber] = React.useState(-1);

  const currentChild = list[selectedNumber]?.children;

  const handleClick = (children: Array<any>, i: number) => {
    if (children.length > 0) {
      setSelectedNumber(i);
    } else {
      setSelectedNumber(-1);
    }
  };

  return (
    <>
      <InsideWrapper
        maxHeight={list.length * 40}
        isParentOpen={selectedNumber > -1}
        isChild={isChild}
      >
        {list.map((data: any, i: number) => {
          return (
            <MenuElement
              key={data.key}
              first={i === 0}
              onClick={() => {
                data?.onClick ? data.onClick() : handleClick(data.children, i);
              }}
              isSelected={selectedNumber === i}
            >
              {data.name}
            </MenuElement>
          );
        })}
      </InsideWrapper>
      {selectedNumber > -1 && (
        <ChildWrapper>
          <MenuItem list={currentChild} isChild={true} />
        </ChildWrapper>
      )}
    </>
  );
};

const SettingMenuListContainer = () => {
  const { makePdf, makeDoc } = useExportContext();
  const { isTextCountTrimState } = useGlobalContext();
  const [open, setOpen] = React.useState(false);

  const [_, setIsTextCountTrim] = isTextCountTrimState;

  const moreRef = React.useRef(null);
  const innerRef = React.useRef(null);

  const handleClick = () => {
    setOpen((c) => !c);
  };

  const handleChangeTextTrim = (trim: boolean) => {
    setIsTextCountTrim(trim);
  };

  useClickOutside([moreRef, innerRef], "mousedown", () => setOpen(false));

  return (
    <>
      <MoreIcon ref={moreRef} onClick={handleClick}>
        <More />
      </MoreIcon>
      <Wrapper ref={innerRef}>
        {open && (
          <MenuItem
            list={listExample({
              exportDocx: makeDoc,
              exportPdf: makePdf,
              changeTrim: handleChangeTextTrim,
            })}
            isChild={false}
          />
        )}
      </Wrapper>
    </>
  );
};

export default SettingMenuListContainer;

interface InsideWrapperProps {
  maxHeight: number;
  isParentOpen: boolean;
  isChild: boolean;
}

const InsideWrapper = styled(FlexWrapper)<InsideWrapperProps>`
  flex-direction: column;
  justify-content: flex-start;

  width: 100%;
  z-index: 1;
  max-height: ${(props) => props.maxHeight}px;

  border-top-left-radius: ${(props) => (props.isChild ? "0px" : "5px")};
  border-bottom-left-radius: ${(props) => (props.isChild ? "0px" : "5px")};

  border-top-right-radius: ${(props) => (props.isParentOpen ? "0px" : "5px")};
  border-bottom-right-radius: ${(props) =>
    props.isParentOpen ? "0px" : "5px"};

  background: ${colors.WHITE};
  overflow: hidden;

  border: 1px solid ${colors.BLUE_GRAY_LINE};
`;

interface NonSelectProps {
  first: boolean;
  isSelected: boolean;
}

const MenuElement = styled(FlexWrapper)<NonSelectProps>`
  background-color: ${({ isSelected }) =>
    isSelected ? colors.BACKGROUND : colors.WHITE};

  width: 100%;
  height: 40px;
  padding: 0px 16px;

  ${typo({
    weight: 500,
    size: "16px",
    height: "100%",
    color: colors.gray_100,
  })};

  cursor: pointer;

  &:hover {
    background-color: ${colors.BACKGROUND};
  }

  z-index: 10;
`;

const Wrapper = styled.div`
  position: absolute;
  cursor: pointer;
  top: 30px;
  left: 30px;
  display: flex;
  flex-direction: row;
  z-index: 5;
`;

const ChildWrapper = styled.div``;

const IconWrapper = styled(FlexWrapper)`
  width: 20px;
  height: 20px;
`;

const MenuItemSelectWrapper = styled(FlexWrapper)`
  position: relative;

  justify-content: space-between;

  width: 100%;
  height: 100%;

  ${typo({
    weight: 500,
    size: "16px",
    height: "26px",
    color: colors.gray_90,
  })};
  word-wrap: none;
  min-width: 110px;
`;

const EmptyMenuItemSelectWrapper = styled(MenuItemSelectWrapper)``;

const MoreIcon = styled(FlexButton)`
  cursor: pointer;
  width: 32px;
  height: 32px;
`;

const GradientButtonTitle = styled.p`
  padding: 6px 9px;
  min-width: 60px;
  ${typo({
    weight: 600,
    size: "16px",
    height: "100%",
    color: colors.WHITE,
  })}
`;

const ButtonWrapper = styled.div`
  max-width: 95px;
  flex: 1;
  display: flex;
`;

const ArrowDown = styled(ArrowRight)`
  transform: rotate(-90deg);
`;

const listExample = ({ exportPdf, exportDocx, changeTrim }) => [
  {
    key: 1,
    name: (
      <MenuItemSelectWrapper>
        내보내기
        <IconWrapper>
          <ArrowDown />
        </IconWrapper>
      </MenuItemSelectWrapper>
    ),
    children: [
      {
        key: 3,
        name: <MenuItemSelectWrapper>*.pdf</MenuItemSelectWrapper>,
        children: [],
        onClick: exportPdf,
      },
      {
        key: 4,
        name: <MenuItemSelectWrapper>*.docx</MenuItemSelectWrapper>,
        children: [],
        onClick: exportDocx,
      },
      // {
      //   name: <MenuItemSelectWrapper>.txt</MenuItemSelectWrapper>,
      //   children: [],
      // },
    ],
  },
  {
    key: 2,
    name: (
      <MenuItemSelectWrapper>
        단축키 안내
        <IconWrapper>
          <ArrowDown />
        </IconWrapper>
      </MenuItemSelectWrapper>
    ),
    children: [
      {
        key: 5,
        name: (
          <MenuItemSelectWrapper
            style={{ minWidth: "252px", justifyContent: "space-between" }}
          >
            이 부분 다시 생성
            <ButtonWrapper>
              <GradientButton disabled={false} handleGenerate={() => {}}>
                <GradientButtonTitle>{rewriteHoyKey}</GradientButtonTitle>
              </GradientButton>
            </ButtonWrapper>
          </MenuItemSelectWrapper>
        ),
        children: [],
      },
      {
        key: 6,
        name: (
          <MenuItemSelectWrapper
            style={{ minWidth: "252px", justifyContent: "space-between" }}
          >
            이 부분 뒤로 이어서 생성
            <ButtonWrapper>
              <GradientButton disabled={false} handleGenerate={() => {}}>
                <GradientButtonTitle>{continualHotKey}</GradientButtonTitle>
              </GradientButton>
            </ButtonWrapper>
          </MenuItemSelectWrapper>
        ),
        children: [],
      },
      {
        key: 7,
        name: (
          <MenuItemSelectWrapper
            style={{ minWidth: "252px", justifyContent: "space-between" }}
          >
            뒤로가기
            <ButtonWrapper>
              <GradientButton disabled={false} handleGenerate={() => {}}>
                <GradientButtonTitle>{rebaseHotKey}</GradientButtonTitle>
              </GradientButton>
            </ButtonWrapper>
          </MenuItemSelectWrapper>
        ),
        children: [],
      },
    ],
  },
  {
    key: 8,
    name: (
      <MenuItemSelectWrapper>
        글자 수
        <IconWrapper>
          <ArrowDown />
        </IconWrapper>
      </MenuItemSelectWrapper>
    ),
    children: [
      {
        key: 10,
        name: (
          <MenuItemSelectWrapper
            // style={{ minWidth: "252px", justifyContent: "space-between" }}
            onClick={() => changeTrim(false)}
          >
            공백 포함
          </MenuItemSelectWrapper>
        ),
        children: [],
      },
      {
        key: 11,
        name: (
          <MenuItemSelectWrapper
            // style={{ minWidth: "252px", justifyContent: "space-between" }}
            onClick={() => changeTrim(true)}
          >
            공백 미포함
          </MenuItemSelectWrapper>
        ),
        children: [],
      },
    ],
  },
];
