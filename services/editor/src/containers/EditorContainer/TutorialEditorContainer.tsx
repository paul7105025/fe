import React from "react";
import styled from "styled-components";

import { $getTextContent, EditorState, EditorThemeClasses } from "lexical";

import {
  InitialConfigType,
  LexicalComposer,
} from "@lexical/react/LexicalComposer";

import { ListNode } from "@lexical/list";
import { HeadingNode } from "@lexical/rich-text";

import LexicalErrorBoundary from "@lexical/react/LexicalErrorBoundary";
import { ContentEditable } from "@lexical/react/LexicalContentEditable";

import { HistoryPlugin } from "@lexical/react/LexicalHistoryPlugin";
import { OnChangePlugin } from "@lexical/react/LexicalOnChangePlugin";
import { RichTextPlugin } from "@lexical/react/LexicalRichTextPlugin";

import EditorTheme from "../../theme/EditorTheme";

import InfoContainer from "../InfoContainer";
import ToolSettingContainer from "../ToolSettingContainer";

import { HighlightTextNode } from "../../nodes/HighlightTextNode";
import { $isGenerateNode, GenerateNode } from "../../nodes/GenerateNode";

import ToolBarPlugin from "../../plugins/ToolBarPlugin";
import CustomFocusPlugin from "../../plugins/CustomFocusPlugin";
import CustomGenDialogPlugin from "../../plugins/CustomGenDialogPlugin";
import CustomKeyBindingPlugin from "../../plugins/CustomKeyBindingPlugin";
import CustomSentenceParserPlugin from "../../plugins/CustomSentenceParserPlugin";
import CustomPlaceholderPlugin from "../../plugins/CustomPlaceholderPlugin/CustomPlaceholderPlugin";

import { docMetaType, saveStatusType } from "../../types";
import useDocMeta from "../../hooks/useDocMeta";
import { GeneratePlugin } from "../../plugins/GeneratePlugin";

import { putDoc_Id } from "@wrtn/core/services";

import _ from "lodash";
import { useParams } from "react-router-dom";
import TutorialPlugin from "../../plugins/TutorialPlugin";
import { tutorialState } from "../TutorialContainer/tutorialState";
import CollectDataPlugin from "../../plugins/CollectDataPlugin/CollectDataPlugin";
import ExportPlugin from "../../plugins/ExportPlugin";
import GlobalPlugin from "../../plugins/GlobalPlugin";

const theme: EditorThemeClasses = {
  ...EditorTheme,
};

const onError = (e: Error) => {
  console.error(e);
};

interface EditorContainerProps {
  docId: string | undefined;
  setSaveStatus: React.Dispatch<React.SetStateAction<saveStatusType>>;
  content: EditorState | undefined;
  metadata: docMetaType;
  tutorialRef: any;
  tutorialStage: number;
}

const save = _.throttle((id, state, callback) => {
  callback();
  return putDoc_Id({
    id: id,
    data: {
      content: state,
    },
  });
}, 2200);

const TutorialEditorContainer = ({
  docId,
  setSaveStatus,
  content,
  metadata,
  tutorialRef,
  tutorialStage,
}: EditorContainerProps) => {
  const initialConfig: InitialConfigType = {
    namespace: "WrtnEditor",
    theme,
    onError,
    editable: false,
    nodes: [HeadingNode, ListNode, GenerateNode, HighlightTextNode],
    editorState: content ? JSON.stringify(content) : null,
  };

  const [currentSelection, setCurrentSelection] = React.useState(null);

  const [isSentenceCursor, setIsSentenceCursor] =
    React.useState<boolean>(false);
  const [focus, setFocus] = React.useState(false);

  const { setMeta, formatMeta } = useDocMeta({
    docId,
    metadata,
  });

  const documentMetadata = {
    topic: "초코 쿠키를 만드는 방법",
    category: "선택 안 함",
    connect: "내용 반전",
  };

  // INFO: 플러그인 순서는 반드시 지켜주세요.
  // 부모 - 자식 플러그인 관계: 데이터 흐름과 관련이 있습니다.
  // 형제 플러그인 관계: UI 배치와 관련이 있습니다.
  return (
    <LexicalComposer initialConfig={initialConfig}>
      <GlobalPlugin>
        <CollectDataPlugin>
          <GeneratePlugin>
            <CustomGenDialogPlugin
              formatMeta={formatMeta}
              isFocus={focus}
              documentMetadata={documentMetadata}
              setMeta={setMeta}
            >
              <ToolSettingContainer
                // @ts-ignore
                documentMetadata={documentMetadata}
                setMeta={setMeta}
                formatMeta={formatMeta}
                isFocus={focus}
                isSentence={isSentenceCursor}
                currentSelection={currentSelection}
                tutorialRef={tutorialRef}
              />

              <CustomFocusPlugin
                setFocus={setFocus}
                setCurrentSelection={setCurrentSelection}
              >
                <ExportPlugin title="">
                  <ToolBarPlugin />
                </ExportPlugin>
                <RichTextPlugin
                  contentEditable={
                    <EditorWrapper ref={(el) => (tutorialRef.current[2] = el)}>
                      <ContentEditable spellCheck={false} />
                    </EditorWrapper>
                  }
                  placeholder={<CustomPlaceholderPlugin key={"placeholder"} />}
                  ErrorBoundary={LexicalErrorBoundary}
                />
                <TutorialPlugin tutorialStage={tutorialStage} />
                <CustomSentenceParserPlugin
                  setIsSentence={setIsSentenceCursor}
                />
              </CustomFocusPlugin>
              <InfoContainer />
            </CustomGenDialogPlugin>
          </GeneratePlugin>
        </CollectDataPlugin>
      </GlobalPlugin>
    </LexicalComposer>
  );
};

export default TutorialEditorContainer;

const EditorWrapper = styled.div`
  width: 100%;
  padding: 44px 120px;
  max-width: calc(640px + 120px + 120px);
  height: calc(100% - 74px - 46px - 30px);
  overflow: scroll;
  line-height: 150%;
`;
