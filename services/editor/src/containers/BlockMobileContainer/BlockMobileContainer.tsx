import { ReactComponent as Computer } from "../../assets/icons/icon_computer.svg";
import { FlexWrapper, typo, colors } from "@wrtn/ui/styles";
import styled from "styled-components";

const BlockMobileContainer = () => {
  return (
    <Wrapper>
      <InnerWrapper>
        <Computer style={{marginBottom: '40px'}}/>
        <Title style={{marginBottom: '18px'}}>
          에디터 기능은 현재 <strong>PC 환경</strong>에서만 지원하고있어요.
        </Title>
        <Desc style={{marginBottom: '40px'}}>
          모바일 환경에서도 사용하실 수 있도록 열심히 만드는 중이에요. <br />그
          전까지 PC에서 해당 기능을 이용해 주세요.
        </Desc>
        <Link
          href="https://wrtn.notion.site/56a6bfe4d63e4e32bb53f998dac0151f"
          target="_blank"
          rel="noopener noreferrer"
        >
          에디터 기능 더 알아보기
        </Link>
      </InnerWrapper>
    </Wrapper>
  );
};

export default BlockMobileContainer;

const Wrapper = styled(FlexWrapper)`
  flex-direction: column;
  height: 100%;
  width: 100%;
  gap: 18px;
  align-items: center;
  justify-content: center;
`;

const InnerWrapper = styled(FlexWrapper)`
  flex-direction: column;
  width: 100%;
  height: 100%;
   
  align-items: center;
  justify-content: center;
`;

const Title = styled.p`
  ${typo({
    weight: 600,
    size: "20px",
    height: "100%",
    color: colors.gray_90,
  })};
  text-align: center;
`;

const Desc = styled.p`
  ${typo({
    weight: 500,
    size: "16px",
    height: "150%",
    color: colors.gray_80,
  })};
  text-align: center;
`;

const Link = styled.a`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.ACTION_BLUE,
  })};
  text-decoration-line: underline;
`;
