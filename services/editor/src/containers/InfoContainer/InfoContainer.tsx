import styled from 'styled-components';
import GuideText from '../../components/GuideText';
import { FlexWrapper } from '@wrtn/ui/styles';
import { useGenerateContext } from '../../plugins/GeneratePlugin';
import React from 'react';
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";
import { useCookies } from 'react-cookie';
import TutorialBubble from '../../components/TutorialBubble';
import { colors, typo} from "@wrtn/ui/styles"
import { useRecoilValue } from 'recoil';
// @ts-ignore
import { userState } from "@wrtn/core/stores/login";

const InfoContainer = () => {
  
  const user:any = useRecoilValue(userState);

  const { genCountState } = useGenerateContext();

  const [genCount, setGenCount] = genCountState;


  const [cookies, setCookies] = useCookies(['wrtn-editor-coffee-event']);
  const guideRef = React.useRef(null);
  const [coffeeEvent, setCoffeeEvent] = React.useState(false);

  React.useEffect(() => {
    if (genCount >= 5) {
      const cache = cookies['wrtn-editor-coffee-event']

      if (!cache) {
        setCoffeeEvent(true);
        setCookies('wrtn-editor-coffee-event', true);
      }
    }
  }, [genCount])

  return (
    <Wrapper>
      <GuideText update={false}>
        결과물의 저작권과 데이터 출처 
      </GuideText>
      <GuideAnchor href={"https://wrtn.notion.site/910597b52def4269aa6facee9aa652d8"} target="_blank" rel="noopener noreferrer">
        에디터 사용 가이드
      </GuideAnchor>
      <div ref={guideRef}>
        <GuideAnchor href={`https://wrtn.typeform.com/to/TipqBRdQ#user_id=${user?._id}`} target="_blank" rel="noopener noreferrer">
          에디터 사용 후기 이벤트
        </GuideAnchor>
      </div>
      { coffeeEvent && guideRef?.current &&
        <ModalPositionPortal
          position={{
            // @ts-ignore
            left: guideRef?.current.getBoundingClientRect().left - 250 || 0,
            // @ts-ignore
            top: guideRef?.current.getBoundingClientRect().top - 15 || 0,
          }}
        >
          <TutorialBubble 
            direction="right" 
            position={20}
            style={{borderRadius: '10px', padding: '14px 16px 16px 25px'}}
          >
            <div onClick={() => {
            setCoffeeEvent(false)
          }}>
            <CoffeeTitle>에디터 사용 후기 이벤트 ☕️</CoffeeTitle>
            <CoffeeDesc>에디터 기능 사용 후 간단한 설문에<br/>참여해주시면, 추첨을 통해<br/>아메리카노를 드립니다!</CoffeeDesc>
            </div>
          </TutorialBubble>
        </ModalPositionPortal>
      }
      {/* <GuideText update={false}>
        에디터 사용 가이드
      </GuideText> */}
    </Wrapper>
  )
}

export default InfoContainer;

const Wrapper = styled(FlexWrapper)`
  // width: 267px;
  padding: 24px 20px;
  background-color: #F2F7FF;
  height: 100%;
  // align-items: flex-start;
  // padding-top: 24px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  gap: 5px;

  @media(max-width: 1280px) {
    width: 200px;
  }
`

const GuideAnchor = styled.a`
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-weight: 600;
  font-size: 0.875rem;
  line-height: 26px;
  color: #3784F6;
  white-space: nowrap;
  -webkit-text-decoration: underline;
  text-decoration: underline;
  cursor: pointer;
`;

const CoffeeDesc = styled.div`
${typo({weight: 500, size: '14px', height: '20px', color: colors.WHITE})};
`;

const CoffeeTitle = styled.div`
  ${typo({weight: 700, size: '16px', height: '22px', color: colors.WHITE})};
`;