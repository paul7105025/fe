import React from "react";
import styled from "styled-components";
import { useAlert } from "react-alert";
import { $createParagraphNode, $createTextNode, $getNodeByKey } from "lexical";
import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";

import { useClickOutside, useEvent } from "@wrtn/core";

import { FlexWrapper } from "@wrtn/ui";
import { Bubble } from "@wrtn/ui/components/Bubble/Bubble";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { colors, typo } from "@wrtn/ui/styles";

import { Divider } from "../../components";
import GradientButton from "../../components/GradientButton";

import { IS_APPLE } from "../CustomKeyBindingPlugin/utils";
import { useGenerateContext } from "../GeneratePlugin/GenerateContext";
import { useCollectDataContext } from "../CollectDataPlugin/CollectDataPlugin";
import useEditorGenerate, {
  EDITOR_TOOL_ID,
} from "../GeneratePlugin/useEditorGenerate";

export const rewriteHoyKey = IS_APPLE ? "⌘ + \\" : "Ctrl + \\";

export const continualHotKey = IS_APPLE ? "⌘ + Enter" : "Ctrl + Enter";

export const rebaseHotKey = IS_APPLE ? "⌘ + Z" : "Ctrl + Z";

const CustomGenDialogPlugin = ({
  documentMetadata,
  formatMeta,
  isFocus,
  children,
  setMeta,
}) => {
  const [editor] = useLexicalComposerContext();
  const { collect, type } = useCollectDataContext();
  const { genNodeKeyState, genLoadingState, genCountState, genDialogState } =
    useGenerateContext();
  const alert = useAlert();
  const [isSelected, setIsSelected] = React.useState(false);

  const [genNodeKey, setGenNodeKey] = genNodeKeyState;
  const [genLoading, setGenLoading] = genLoadingState;
  const [genCount, setGenCount] = genCountState;
  const [genDialog, setGenDialog] = genDialogState;

  // const [genDialog, setGenDialog] = React.useState<genDialog>(null);
  const dialogRef = React.useRef(null);

  useClickOutside(dialogRef, "mousedown", () => handleMouseOut());

  const { collectEvent } = useEvent();

  // const generateDialogContext = React.useMemo(() => {
  //   const context: GenerateDialogContext = {
  //     genDialogState: [genDialog, setGenDialog],
  //   };

  //   return context;
  //   // only do once.
  // }, [editor, genNodeKey, genCount, genLoading]);

  const { handleGenerateContinual, handleGenerateRewrite } = useEditorGenerate({
    metadata: documentMetadata,
    formatMeta,
  });

  // 다시 쓰기
  const handleRewrite = () => {
    collectEvent("click_generate_btn", {
      feature_menu: "editor",
      generate_position: "editor",
      feature_category: "",
      feature_name: "다시쓰기",
      feature_tag: "",
      feature_form: "",
      prompt_id: [EDITOR_TOOL_ID],
      feature_prompt: "",
      repeat_count: 1,
      method: "button",
    });
    return handleGenerateRewrite();
  };

  // 생성 하이라이팅 노드가 있었다가 모종의 이유로 사라지면, key 값 갱신
  React.useEffect(() => {
    const removeUpdateListener = editor.registerUpdateListener((listener) => {
      editor.update(() => {
        let current = false; // 현재 state에 highlighttext가 있나요
        let prev = false; // 이전 state에 highlighttext가 있나요
        listener.editorState._nodeMap.forEach((v) => {
          if (v.getType() === "HighlightText") current = true;
        });

        listener.prevEditorState._nodeMap.forEach((v) => {
          if (v.getType() === "HighlightText") prev = true;
        });

        if (prev && !current) {
          setGenDialog(null);
        }
      });
    });
    return () => removeUpdateListener();
  }, [editor]);

  // 생성 하이라이팅 하나라도 변경 시 일반 텍스트 노드로 변경
  React.useEffect(() => {
    const removeUpdateListener = editor.registerUpdateListener((listener) => {
      editor.update(() => {
        const changedNodes = listener.dirtyLeaves;
        for (const nodeKey of changedNodes) {
          // it should be unique even if it is map
          const current = listener.editorState._nodeMap.get(nodeKey);
          const prev = listener.prevEditorState._nodeMap.get(nodeKey);
          if (current?.__text === prev?.__text) continue;
          if (
            current &&
            prev &&
            current?.getType() === "HighlightText" &&
            !current.__isAnimating
          ) {
            setGenDialog(null);
          }
        }
      });
    });
    return () => removeUpdateListener();
  }, [editor]);

  React.useEffect(() => {
    const removeUpdateListener = editor.registerUpdateListener((listener) => {
      let loading: string = "";
      let highlight: string = "";

      // 현재 select 존재 여부
      editor.update(() => {
        setIsSelected($getNodeByKey(genNodeKey)?.isSelected() || false);
      });
      listener.editorState._nodeMap.forEach((v) => {
        if (v.getType() === "HighlightText") highlight = v.getKey();
        if (v.getType() === "Generate") loading = v.getKey();
      });

      if (loading && highlight) {
        editor.update(() => {
          const highlightNode = editor
            .getEditorState()
            ._nodeMap.get(highlight || "");
          const newNode = $createTextNode(highlightNode?.getTextContent());
          if (highlightNode) {
            if (!highlightNode?.getParent()) {
              const parent = $createParagraphNode();
              parent.append(highlightNode);
              highlightNode?.replace(newNode);
            } else {
              highlightNode.replace(newNode);
            }
          }
          setGenNodeKey(newNode.getKey());
          setGenDialog(null);
        });
      }
    });
    return () => removeUpdateListener();
  }, [editor, genNodeKey]);

  // 이어 쓰기
  const handleContinualWrite = () => {
    // if (wordCount.wordCount < 1) {
    //   return openNoCreditModal();
    // }
    setMeta("connect", "선택 안 함");

    collectEvent("click_generate_btn", {
      feature_menu: "editor",
      generate_position: "editor",
      feature_category: "",
      feature_name: "이어쓰기",
      feature_tag: "",
      feature_form: "",
      prompt_id: [EDITOR_TOOL_ID],
      feature_prompt: "",
      repeat_count: 1,
      method: "button",
    });

    return handleGenerateContinual({
      isFocus,
    });
  };

  React.useEffect(() => {
    if (genDialog && !genDialog?.right && !genDialog?.top) {
      handleMouseOver();
    }
  }, [genDialog]);
  const handleMouseOver = () => {
    const pos = editor.getElementByKey(genNodeKey)?.getBoundingClientRect();

    // if (genNodeKey) {
    if (pos?.right && pos?.bottom) {
      setGenDialog((c) => ({
        right: pos.right,
        top: (pos.bottom + pos.top) / 2,
      }));
    }
    // }
  };

  const handleMouseOut = () => {
    if (genDialog) {
      setGenDialog(null);
    }
  };

  return (
    <>
      {genDialog && documentMetadata.topic.trim().length !== 0 && (
        <ModalPositionPortal
          position={{
            left: genDialog?.right - 10,
            top:
              genDialog?.top - 45 > window.innerHeight - 120
                ? window.innerHeight - 120
                : genDialog?.top - 45,
          }}
        >
          <MouseEventWrapper
            // onMouseEnter={handleMouseOver}
            // onMouseLeave={handleMouseOut}
            ref={dialogRef}
          >
            <Bubble
              direction={"left"}
              position={33}
              style={{ padding: "10px 12px" }}
            >
              <GenDialogWrapper>
                <GenDialogContent>
                  <ContentTitle>결과물 다시 생성</ContentTitle>
                  <GradientButton
                    disabled={false}
                    handleGenerate={handleRewrite}
                  >
                    <GradientButtonTitle>{rewriteHoyKey}</GradientButtonTitle>
                  </GradientButton>
                </GenDialogContent>
                <Divider
                  style={{
                    margin: "8px 0px",
                    background: colors.BLUE_GRAY_LINE,
                  }}
                />
                <GenDialogContent>
                  <ContentTitle>커서 뒤로 이어서 생성</ContentTitle>
                  <GradientButton
                    disabled={false}
                    handleGenerate={handleContinualWrite}
                  >
                    <GradientButtonTitle>{continualHotKey}</GradientButtonTitle>
                  </GradientButton>
                </GenDialogContent>
              </GenDialogWrapper>
            </Bubble>
          </MouseEventWrapper>
        </ModalPositionPortal>
      )}
      {children}
    </>
  );
};

export default CustomGenDialogPlugin;

const GenDialogWrapper = styled.div`
  width: 242px;
  height: 67px;
`;

const MouseEventWrapper = styled.div`
  padding-left: 30px;
  pointer-events: auto;
`;

const GenDialogContent = styled(FlexWrapper)``;

const ContentTitle = styled.p`
  ${typo({
    weight: 600,
    size: "12px",
    height: "100%",
    color: colors.gray_80,
  })};
  min-width: 138px;
  padding-left: 9px;
`;

const GradientButtonTitle = styled.p`
  padding: 6px 10px;
  ${typo({
    weight: 600,
    size: "12px",
    height: "100%",
    color: colors.WHITE,
  })}
`;
