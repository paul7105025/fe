import React from "react";
import { useCookies } from "react-cookie";

export type CollectDataContext = {
  collect: collectType,
  type: abType
}

const CollectDataContext = React.createContext<CollectDataContext>(undefined as any);

export const useCollectDataContext = () => {
  const collectDataContext = React.useContext(CollectDataContext);

  if (collectDataContext === null) throw new Error("nope")
  return collectDataContext;
}

export type abType = () => 'a' | 'b';

const collectTypeArray = [
  'wrtn-test-data-type',
  'wrtn-test-data-timer',
  'wrtn-test-data-continual-tunib-call',
  'wrtn-test-data-continual-tunib-pass', 
  'wrtn-test-data-continual-tunib-block', 
  'wrtn-test-data-continual-tunib-word',
  'wrtn-test-data-continual-gen-count',
  'wrtn-test-data-rewrite-tunib-call',
  'wrtn-test-data-rewrite-tunib-pass', 
  'wrtn-test-data-rewrite-tunib-block', 
  'wrtn-test-data-rewrite-tunib-word',
  'wrtn-test-data-rewrite-gen-count',
];

export type collectDataType = typeof collectTypeArray[number];

export type collectType = (id: collectDataType, callback: (prev: any) => any) => void
// 튜닙 테스트 용
const CollectDataPlugin = ({
  children
}) => {
  // 
  const [cookies, setCookies] = useCookies(collectTypeArray);


  React.useEffect(() => {
    if (!cookies['wrtn-test-data-type']) setCookies('wrtn-test-data-type', Math.random() > 0.5 ? 'a' : 'b')
  }, []);

  const type: abType = () => cookies['wrtn-test-data-type'] ? cookies['wrtn-test-data-type'] : 'a';

  const collect: collectType = (id: collectDataType, callback: (prev: any) => any): void => {
    setCookies(id, callback(cookies[id]));
  }

  // 접속 시간
  // React.useEffect(() => {
  //   const intervalId = setInterval(() => {
  //     if (cookies['wrtn-test-data-timer']) setCookies('wrtn-test-data-timer', parseInt(cookies['wrtn-test-data-timer']) + 1 );
  //     else setCookies('wrtn-test-data-timer', 0);
  //   }, 1000);

  //   return () => clearInterval(intervalId);
  // }, [cookies, setCookies])

  return <CollectDataContext.Provider value={{collect, type}}>
    {children}
  </CollectDataContext.Provider>
}

export default CollectDataPlugin;