import React from "react";
import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";

import {
  GENERATE_FAIL,
  GENERATE_FAIL_TOO_MANY_REQUEST,
  GENERATE_INVALID_INPUT,
  postEditorGenerate,
  postToxicity,
  putUser,
  useEvent,
  useFreePlanBubble,
  useReCaptcha,
  useRefresh,
  userState,
  useTagManager,
} from "@wrtn/core";

import {
  $createParagraphNode,
  $createTextNode,
  $getNodeByKey,
  $getRoot,
  $getSelection,
  $isRangeSelection,
  $isRootNode,
  $setSelection,
  EditorState,
  LexicalEditor,
  LexicalNode,
} from "lexical";
import { useAlert } from "react-alert";
import { useGenerateContext } from "./GenerateContext";
import { docMetaType, saveStatusType } from "../../types";
import { $createGenerateNode, $isGenerateNode } from "../../nodes/GenerateNode";
import { normalizeKorUnicode } from "../../utils/korean";
import useDocMeta from "../../hooks/useDocMeta";
import { $createHighlightTextNode } from "../../nodes/HighlightTextNode";
import { useParams } from "react-router-dom";
import { useRecoilValue } from "recoil";

export const EditorGenerateType = {
  EDITOR_GENERATE_CONTINUAL: "continual",
  EDITOR_GENERATE_REWRITE: "rewrite",
  EDITOR_GENERATE_CONTINUAL_SELECTED: "continual_selected",
} as const;

export type EditorGenerateType =
  (typeof EditorGenerateType)[keyof typeof EditorGenerateType];

const TEXT_RULE = /(?=[?.!])/g; // INFO: 사파리 이슈로 인해 look behind -> look forward로 변경.
const MAXIMUM_READING_SENTENCE = 9; // INFO: TextRule 한 문장 덜 읽음.
const MINIMUM_GENERATE_TEXT = 10;
export const EDITOR_TOOL_ID = "6390af3533d5f4206021ee03";
const ANIMATING_INTERVAL_TIME = 10;

export type GenerateFunctionProps = {
  currentSelection?: any;
  isFocus: boolean;
};
export const useEditorGenerate = ({ metadata, formatMeta }) => {
  const { docId } = useParams();
  const user = useRecoilValue(userState);

  const [editor] = useLexicalComposerContext();
  const alert = useAlert();
  const { genNodeKeyState, genLoadingState, genCountState, genDialogState } =
    useGenerateContext();
  const {
    collectUserProperties,
    collectUserProperties_increment,
    collectUserProperties_once,
    collectEvent,
  } = useEvent();
  const { fetchMetric } = useRefresh();
  const [genNodeKey, setGenNodeKey] = genNodeKeyState;
  const [genLoading, setGenLoading] = genLoadingState;
  const [genDialog, setGenDialog] = genDialogState;
  const [genCount, setGenCount] = genCountState;

  const { updateTodayMetric } = useFreePlanBubble();

  const tagManager = useTagManager();

  const { openRecaptcha } = useReCaptcha();

  // const { documentMetadata, formatMeta } = useDocMeta({
  //   docId,
  //   metadata,
  // }); // 얻기 힘든 구조일텐데..

  const init = (loadingNode: LexicalNode | null) => {
    setGenLoading(false);
    setGenDialog(null);
    setGenNodeKey("");
    editor.update(() => {
      editor._updateTags.add("history-merge");
      loadingNode?.remove(false);
    });
  };

  const _isInvalidGenerate = (
    loading: boolean,
    genNodeKey: string
  ): boolean => {
    return loading;
    // || genNodeKey !== ""
  };

  const end = ({ originText, animatingHighlightTextNode, intervalId }) => {
    editor.update(() => {
      editor.setEditable(true);
      setGenLoading(false);
      const nonAnimatedNode = $createHighlightTextNode(
        "WrtnEditor_highlightText",
        originText,
        false,
        (ev) => {
          const pos = editor
            .getElementByKey(nonAnimatedNode.getKey())
            ?.getBoundingClientRect();

          if (pos?.right && pos?.bottom) {
            setGenDialog((c) => ({
              right: pos.right, //pos.left,
              top: (pos.bottom + pos.top) / 2, // pos.bottom
            }));
          }
        },
        (ev) => {
          setGenDialog(null);
        }
      );

      setGenDialog({
        right: 0,
        top: 0,
      });

      setGenCount((c) => c + 1);
      animatingHighlightTextNode.replace(nonAnimatedNode);
      nonAnimatedNode.insertAfter($createTextNode(" "));
      setGenNodeKey(nonAnimatedNode.getKey());
      nonAnimatedNode.selectNext(0, 0);
      editor._updateTags.add("history-merge");
      editor.focus();
      clearInterval(intervalId);
    });
  };
  // TODO: 개선할 필요 있음.
  // LexiclaJS 는 노드들을 배열로 관리하지만 사실 unordered(map) 임.
  // 따라서, unordered된 노드들을 ordered되게 만드는 과정이 필요함.
  // nodeMaps 형식말고 텍스트만 긁어오는 거면 lexical 자체에서 지원하는 텍스트 파서를 사용하면 되지만,
  // selection 위치를 판단하는 로직 때문에 불가능
  const _getPreviousTextArray = (
    editor: LexicalEditor,
    type: EditorGenerateType
  ): Array<string | undefined> => {
    let text = "";
    const struct = new Map();
    const state = editor.getEditorState();

    let _tempNodeKey: string | null = null;
    let selectedParagraphNodeKey: string | null = null;
    // Paragraph 수집
    state._nodeMap.forEach((v, nodeKey) => {
      if (
        v.getType() === "paragraph" ||
        v.getType() === "heading" ||
        v.getType() === "linebreak"
      ) {
        if (!_tempNodeKey) _tempNodeKey = v.__key;
        struct.set(v.__key, {
          prev: v.__prev,
          next: v.__next,
          child: new Map(),
        });
        if (
          $isRangeSelection(state?._selection) &&
          v.__key === state?._selection?.anchor.key
        ) {
          selectedParagraphNodeKey = v.__key;
        }
      }
    });

    let selectedNodeKey: string | null = null;
    // text, HighlightText 수집. selection 된 노드라면 따로 저장
    state._nodeMap.forEach((v, nodeKey) => {
      if (v.getType() === "text" || v.getType() === "HighlightText") {
        if (struct.has(v.__parent)) {
          if (
            $isRangeSelection(state?._selection) &&
            v.__key === state?._selection?.anchor.key
          ) {
            selectedNodeKey = v.__key;
            // should be unique.
          }
          struct.get(v.__parent).child.set(v.__key, {
            prev: v.__prev,
            next: v.__next,
            text: v.getTextContent(),
          });
        }
      }
    });

    // paragraph에서 가장 앞에 있는  node 찾기
    let _parentNodeKey = _tempNodeKey;
    while (_parentNodeKey) {
      if (struct.get(_parentNodeKey)?.prev) {
        _parentNodeKey = struct.get(_parentNodeKey).prev;
      } else {
        break;
      }
    }
    if (type === EditorGenerateType.EDITOR_GENERATE_REWRITE) {
      while (_parentNodeKey) {
        const arr = Array.from(struct.get(_parentNodeKey).child).map(
          // @ts-ignore
          ([key, value]) => ({ key, value })
        );
        // @ts-ignore
        let _childNodeKey = arr[0]?.key;

        while (_childNodeKey) {
          if (struct.get(_parentNodeKey)?.child.get(_childNodeKey)?.prev) {
            _childNodeKey = struct
              .get(_parentNodeKey)
              ?.child.get(_childNodeKey).prev;
          } else {
            break;
          }
        }

        let ending = false;
        while (_childNodeKey) {
          const node = $getNodeByKey(_childNodeKey);
          if (node && node.__type === "HighlightText") {
            // text += node?.getTextContent();
            ending = true;
            break;
          }
          text += node?.getTextContent();

          _childNodeKey = struct
            .get(_parentNodeKey)
            .child.get(_childNodeKey)?.next;
        }

        if (ending) break;
        _parentNodeKey = struct.get(_parentNodeKey).next;
      }
    } else {
      // for continual generate
      while (_parentNodeKey) {
        const arr = Array.from(struct.get(_parentNodeKey)?.child).map(
          // @ts-ignore
          ([key, value]) => ({ key, value })
        );
        // @ts-ignore
        let _childNodeKey = arr[0]?.key;

        while (_childNodeKey) {
          if (struct.get(_parentNodeKey).child.get(_childNodeKey)?.prev) {
            _childNodeKey = struct
              .get(_parentNodeKey)
              .child.get(_childNodeKey).prev;
          } else {
            break;
          }
        }

        let ending = false;
        while (_childNodeKey) {
          const node = $getNodeByKey(_childNodeKey);

          if (selectedNodeKey && node && _childNodeKey === selectedNodeKey) {
            const select = state._selection;
            if ($isRangeSelection(select)) {
              text += node.getTextContent().slice(0, select?.anchor?.offset);
            }
            ending = true;
            break;
          }
          text += node?.getTextContent();
          _childNodeKey = struct
            .get(_parentNodeKey)
            .child.get(_childNodeKey)?.next;
        }

        if (!selectedNodeKey && _parentNodeKey === selectedParagraphNodeKey) {
          ending = true;
          break;
        }

        if (ending) break;
        _parentNodeKey = struct.get(_parentNodeKey).next;
      }
    }

    const textArray = text.split(TEXT_RULE);
    const parsedTextArray: Array<string | undefined> = [];
    while (
      textArray.length > 0 &&
      parsedTextArray.length < MAXIMUM_READING_SENTENCE
    )
      parsedTextArray.unshift(textArray.pop());
    return parsedTextArray;
  };

  // TODO: 튜닙 필터 캐싱 구현 필요!
  const getClovaText = async (
    editor: LexicalEditor,
    type: EditorGenerateType
  ): Promise<string> => {
    const textArray = _getPreviousTextArray(editor, type);
    const joinedText = textArray.join("").trimEnd();
    const clovaText = formatMeta(normalizeKorUnicode(joinedText));

    const toxicityResA = await postToxicity({
      inputs: [clovaText.inputs[0], joinedText],
      text: clovaText.inputs[0] + joinedText,
    });
    if (toxicityResA?.status !== 200) {
      alert.removeAll();
      alert.show("생성에 실패했어요. 다시 시도해주세요.");
      throw new Error("Wrtn: Editor Cannot Generate");
    }
    if (toxicityResA.data.isToxicity) {
      alert.removeAll();
      alert.show(GENERATE_INVALID_INPUT);
      throw new Error("Wrtn: Tunib Block");
    }

    //TODO: 깔끔한 분기 없으려나
    const res = await postEditorGenerate({
      toolId: EDITOR_TOOL_ID,
      data: {
        ...clovaText,
        command:
          type === EditorGenerateType.EDITOR_GENERATE_REWRITE
            ? "다시쓰기"
            : "이어쓰기",
      },
      user: user,
    });

    if (res?.status === 401 || res?.response?.status === 401) {
      alert.removeAll();
      throw new Error("Cannot Generate");
    }
    if (res?.status === 400 || res?.response?.status === 400) {
      alert.removeAll();
      alert.show(GENERATE_INVALID_INPUT);
      throw new Error("Cannot Generate");
    }
    if (res?.status === 429 || res?.response?.status === 429) {
      alert.removeAll();
      alert.show(GENERATE_FAIL_TOO_MANY_REQUEST);
    }
    if (res?.status === 444 || res?.response?.status === 444) {
      openRecaptcha();
    } else if (res?.status !== 201) {
      alert.removeAll();
      alert.show(GENERATE_FAIL);
      throw new Error("Cannot Generate");
    }

    updateTodayMetric(1);

    const ret: string = normalizeKorUnicode(res.data.data.output);
    return ret;
  };

  const createLoadingNode = (editor: LexicalEditor): LexicalNode | null => {
    let loadingNode: LexicalNode | null = null;

    editor.getEditorState()._nodeMap.forEach((v) => {
      if ($isGenerateNode(v)) {
        v.remove();
      }
    });

    const selection = $getSelection();
    const generateNode = $createGenerateNode("WrtnEditor__loading", " ");

    loadingNode = generateNode;
    if (!selection || selection.getTextContent() !== "") return null;

    editor._updateTags.add("history-merge");

    selection.insertNodes([generateNode], false);

    editor._updateTags.add("history-merge");

    $setSelection(selection);

    return loadingNode;
  };

  const createHighlightTextNode = ({ originText, editor, loadingNode }) => {
    const node = $createHighlightTextNode(
      "WrtnEditor__highlightText",
      " ",
      true,
      () => {},
      () => {}
    );

    // editor._updateTags.add('history-merge')
    if ($isRootNode(node.getParent())) {
      const parent = $createParagraphNode();
      parent.append(node);
      loadingNode.replace(parent);
    } else {
      loadingNode.replace(node);
    }

    let animatingText = "";
    let idx = 0;
    let maxLength = originText.length;

    const intervalId = setInterval(() => {
      editor.update(() => {
        editor.setEditable(false);
        editor._updateTags.add("history-merge");
        if (idx >= maxLength) {
          end({
            originText: originText,
            animatingHighlightTextNode: node,
            intervalId: intervalId,
          });
        }
        animatingText += originText[idx];
        node.setTextContent(animatingText);
        ++idx;
      });
    }, ANIMATING_INTERVAL_TIME);
  };
  // TODO: editor 인자로 받을지, composer에서 받아도 되는지?
  // composer에서 받으면 문제 있을 수도 있어서 일단은 prop로 계속 넘겨줘서 맥락 유지
  const generate = async (editor: LexicalEditor, type: EditorGenerateType) => {
    if (_isInvalidGenerate(genLoading, genNodeKey)) return;
    editor.blur();
    editor.focus();
    let loadingNode: LexicalNode | null = createLoadingNode(editor);
    try {
      if (!loadingNode) return;
      setGenLoading(true);
      const res = await getClovaText(editor, type);
      if (res) {
        collectUserProperties_once("first_generate_date", new Date());
        collectUserProperties_increment("count_generate_accum", 1);
        collectUserProperties("last_generate_date", new Date());
        collectUserProperties_increment(
          "amount_generate_accum",
          res?.replace(/ /g, "").length
        );
        tagManager({
          event: " generate_done",
          data: {
            feature_menu: "editor",
            generate_position: "editor",
            feature_category: "",
            tool_open_position: "",
            feature_name: type === "rewrite" ? "다시쓰기" : "이어쓰기",
            feature_tag: "",
            feature_form: "",
            prompt_id: [EDITOR_TOOL_ID],
            generate_character: res?.replace(/ /g, "").length,
            generate_token: "", // TODO: generate_token
            generate_model: "", // TODO: generate model
            input_tone_and_manner: "",
            input_category: metadata.category || "",
            input_continue_option: metadata.connect,
            input_style: "",
            input_ratio: "",
          },
        });
        collectEvent("generate_done", {
          feature_menu: "editor",
          generate_position: "editor",
          feature_category: "",
          tool_open_position: "",
          feature_name: type === "rewrite" ? "다시쓰기" : "이어쓰기",
          feature_tag: "",
          feature_form: "",
          prompt_id: [EDITOR_TOOL_ID],
          generate_character: res?.replace(/ /g, "").length,
          generate_token: "", // TODO: generate_token
          generate_model: "", // TODO: generate model
          input_tone_and_manner: "",
          input_category: metadata.category || "",
          input_continue_option: metadata.connect,
          input_style: "",
          input_ratio: "",
        });
        await putUser({
          data: {
            isNewbie: false,
          },
        });
        fetchMetric();
        editor.update(() => {
          try {
            if (!loadingNode) return;
            createHighlightTextNode({
              originText: res,
              editor,
              loadingNode,
            });
          } catch (err) {
            init(loadingNode);
          }
        });
      }
    } catch (err) {
      init(loadingNode);
    }
  };

  const handleGenerateContinual = ({
    currentSelection,
    isFocus,
  }: GenerateFunctionProps) => {
    if (genLoading) {
      return false;
    } else if (metadata.topic.trim().length === 0) {
      alert.removeAll();
      alert.show("기본 세팅을 입력해야 생성할 수 있어요.");
      return false;
    }
    editor.update(() => {
      const selection = currentSelection || $getSelection();

      if (
        $getRoot().getTextContent().trim() !== "" &&
        $getSelection()?.getTextContent() !== ""
      ) {
        alert.removeAll();
        alert.show(
          "문장을 생성하고 싶은 지점에 커서를 클릭한 후 생성해주세요."
        );
        return;
      }
      if (genNodeKey === "") {
        generate(editor, EditorGenerateType.EDITOR_GENERATE_CONTINUAL);
        return;
      }
      if (!selection || !isFocus) {
        generate(editor, EditorGenerateType.EDITOR_GENERATE_CONTINUAL);
        return;
      }
      if (selection && $getNodeByKey(genNodeKey)?.isSelected()) {
        // 얘만 하이라이팅 텍스트 내에서 이어쓰기 하는 경우.
        generate(editor, EditorGenerateType.EDITOR_GENERATE_CONTINUAL_SELECTED);
        return;
      }
      if (selection) {
        generate(editor, EditorGenerateType.EDITOR_GENERATE_CONTINUAL);
        return;
      }
    });
    fetchMetric();
    return true;
  };

  const handleGenerateRewrite = () => {
    if (genLoading) {
      return false;
    }
    setGenDialog(null);
    editor.update(() => {
      if ($getSelection()?.getTextContent() !== "") {
        alert.removeAll();
        alert.show(
          "문장을 생성하고 싶은 지점에 커서를 클릭한 후 생성해주세요."
        );
        return;
      }
      if (genNodeKey === "") {
        alert.removeAll();
        alert.show(
          genCount > 0
            ? "문장을 생성한 후 수정하지 않은 상태에서만 해당 부분을 다시 생성할 수 있어요."
            : "문장을 생성한 이후에 다시 생성 기능을 사용할 수 있어요."
        );
        return;
      }

      $getNodeByKey(genNodeKey)?.selectNext(0, 0);
      if (!$getNodeByKey(genNodeKey)?.__isAnimating) {
        $getNodeByKey(genNodeKey)?.remove(false);
        setGenNodeKey("");
        generate(editor, EditorGenerateType.EDITOR_GENERATE_REWRITE);
      }
    });
    fetchMetric();
    return true;
  };

  return {
    handleGenerateContinual,
    handleGenerateRewrite,
  };
};

export default useEditorGenerate;
