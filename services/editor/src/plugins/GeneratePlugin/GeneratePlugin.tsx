import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import { $createParagraphNode, $createTextNode, $isTextNode } from "lexical";
import React from "react";
import { genDialog, GenerateContext } from "./GenerateContext";
import { $generateHtmlFromNodes } from "@lexical/html";
import { $isGenerateNode, GenerateNode } from "../../nodes/GenerateNode";
import { FreePlanBubble } from "@wrtn/ui/components/FreePlanBubble";
import { useRecoilValue } from "recoil";
import { useFreePlanBubble } from "@wrtn/core";

/**
 * 생성에 관련한 코어 기능을 제공합니다.
 */
const GeneratePlugin = ({ children }) => {
  const [editor] = useLexicalComposerContext();

  const [genNodeKey, setGenNodeKey] = React.useState("");
  const [genCount, setGenCount] = React.useState(0);
  const [genLoading, setGenLoading] = React.useState(false);
  const [genDialog, setGenDialog] = React.useState(null as genDialog);

  const { isNoCreditModalOpen, isClosing, closeNoCreditModal } =
    useFreePlanBubble();

  const generateContext = React.useMemo(() => {
    const context: GenerateContext = {
      genNodeKeyState: [genNodeKey, setGenNodeKey],
      genCountState: [genCount, setGenCount],
      genLoadingState: [genLoading, setGenLoading],
      genDialogState: [genDialog, setGenDialog],
    };

    return context;
  }, [genNodeKey, genCount, genLoading, genDialog]);

  // React.useEffect(() => {
  //   const removeUpdateListener = editor.registerEditableListener((listener) => {
  //     if (listener.valueOf()) {
  //       editor.focus();
  //     }
  //   })
  //   return () => removeUpdateListener();
  // }, [editor]);

  // 생성 하이라이팅이 있을 때마다 key 값 갱신, 생성 하이라이팅이 없어졌다면 초기화하는 기능
  React.useEffect(() => {
    const removeUpdateListener = editor.registerUpdateListener((listener) => {
      editor.update(() => {
        let current = false;
        let prev = false;
        listener.editorState._nodeMap.forEach((v, key) => {
          if (v.getType() === "HighlightText") {
            setGenNodeKey(v.getKey());
            current = true;
          }
        });
        listener.prevEditorState._nodeMap.forEach((v) => {
          if (v.getType() === "HighlightText") prev = true;
        });
        if (prev && !current) setGenNodeKey("");
      });
    });
    return () => removeUpdateListener();
  }, [editor]);

  // 로딩이 아닌데 로딩 노드가 있으면 제거
  React.useEffect(() => {
    const removeUpdateListener = editor.registerUpdateListener((listener) => {
      editor.update(() => {
        let prev = false;
        listener.prevEditorState._nodeMap.forEach((v) => {
          if (v.getType() === "generate") prev = true;
        });
        listener.editorState._nodeMap.forEach((v, key) => {
          if (!prev && !genLoading && $isGenerateNode(v)) {
            editor._updateTags.add("history-merge");
            v.remove(false);
          }
        });
      });
    });
    return () => removeUpdateListener();
  }, [editor, genLoading]);

  // 생성 하이라이팅 하나라도 변경 시 일반 텍스트 노드로 변경
  React.useEffect(() => {
    const removeUpdateListener = editor.registerUpdateListener((listener) => {
      editor.update(() => {
        const changedNodes = listener.dirtyLeaves;
        for (const nodeKey of changedNodes) {
          // it should be unique even if it is map
          const current = listener.editorState._nodeMap.get(nodeKey);
          const prev = listener.prevEditorState._nodeMap.get(nodeKey);
          if (current?.__text === prev?.__text) continue;
          if (
            current &&
            prev &&
            current?.getType() === "HighlightText" &&
            !current.__isAnimating
          ) {
            const newNode = $createTextNode(current?.getTextContent());
            editor
              .getEditorState()
              ._nodeMap.get(current.getKey())
              ?.replace(newNode);
            setGenNodeKey("");
            editor._updateTags.add("history-merge");
          }
        }
      });
    });
    return () => removeUpdateListener();
  }, [editor]);

  return (
    <GenerateContext.Provider value={generateContext}>
      {isNoCreditModalOpen && (
        <FreePlanBubble
          isClosing={isClosing}
          handleClose={closeNoCreditModal}
        />
      )}
      {children}
    </GenerateContext.Provider>
  );
};

export default GeneratePlugin;
