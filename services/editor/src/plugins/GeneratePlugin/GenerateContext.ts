import React from "react";

export type genDialog = {
  right: number;
  top: number;
} | null;

export type GenerateContext = {
  genNodeKeyState: [string, React.Dispatch<React.SetStateAction<string>>]
  genCountState: [number, React.Dispatch<React.SetStateAction<number>>]
  genLoadingState: [boolean, React.Dispatch<React.SetStateAction<boolean>>]
  genDialogState: [genDialog, React.Dispatch<React.SetStateAction<genDialog>>]
}

export const GenerateContext = React.createContext<GenerateContext>(undefined as any);

export const useGenerateContext = () => {
  const generateContext = React.useContext(GenerateContext);

  if (generateContext === null) throw new Error("generate context doens't exist. it should be inside GeneratePlugin")
  return generateContext;
}
