import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext"
import { $getNodeByKey, LexicalNode } from "lexical";
import React from "react";
import { useGenerateContext } from "../GeneratePlugin";

const tutorialState = () => {

}

type TutorialPluginProps = {
  tutorialStage: number;
}

const TutorialPlugin = ({
  tutorialStage
}: TutorialPluginProps) => {
  const [editor] = useLexicalComposerContext();
  const { genDialogState } = useGenerateContext();
  const [genDialog, setGenDialog] = genDialogState;

  React.useEffect(() => {
    if (tutorialStage === 2) {
      editor._editorState.read(() => {
        let node: LexicalNode | undefined = undefined
        editor.getEditorState()._nodeMap.forEach(v => {
          if (v.getType() === 'HighlightText') node = v;
        })

        if (node) {
          // @ts-ignore
          const pos = editor.getElementByKey(node?.getKey())?.getBoundingClientRect();
          if (pos?.right && pos?.bottom) { 
            setGenDialog({
              right: pos.right,
              top: (pos.bottom + pos.top) / 2
            })
          }
        }
      });
    }
  }, [editor, tutorialStage]);
  
  return <></>
}

export default TutorialPlugin;