import React from "react";
import styled from "styled-components";

import { BLUR_COMMAND, COMMAND_PRIORITY_LOW, FOCUS_COMMAND } from "lexical";
import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";

import { typo, colors } from "@wrtn/ui/styles";

const CustomPlaceholderPlugin = () => {
  const [editor] = useLexicalComposerContext();

  const [hasFocus, setFocus] = React.useState<boolean>(false);

  const handleClickPlaceholder = () => {
    if (!hasFocus) editor.focus();
  };

  React.useEffect(() => {
    const removeRegisterCommand = editor.registerCommand(
      BLUR_COMMAND,
      () => {
        setFocus(false);
        return false;
      },
      COMMAND_PRIORITY_LOW
    );

    return () => removeRegisterCommand();
  }, [editor]);

  React.useEffect(() => {
    const removeRegisterCommand = editor.registerCommand(
      FOCUS_COMMAND,
      () => {
        setFocus(true);
        return false;
      },
      COMMAND_PRIORITY_LOW
    );
    return () => removeRegisterCommand();
  }, [editor]);

  return (
    <>
      {!hasFocus ? (
        <PlaceholderWrapper onClick={handleClickPlaceholder}>
          {/* <EditorWrapper> */}
          <PlaceholderText>
            왼쪽 기본 세팅을 입력한 후 자동 생성 버튼을 눌러주세요! (자동생성
            단축키: Ctrl(cmd)+ Enter){" "}
          </PlaceholderText>
          {/* </EditorWrapper> */}
        </PlaceholderWrapper>
      ) : null}
    </>
  );
};

export default CustomPlaceholderPlugin;

const PlaceholderText = styled.p`
  ${typo({ weight: 500, size: "16px", height: "26px", color: colors.gray_55 })};
`;

const PlaceholderWrapper = styled.div`
  position: absolute;
  top: 46px;
  padding: 44px 120px 0px;
  width: 100%;
  max-width: 880px;
`;
