import React from 'react';
import { $getSelection, BLUR_COMMAND, COMMAND_PRIORITY_EDITOR, COMMAND_PRIORITY_LOW, FOCUS_COMMAND } from "lexical"
import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import styled from 'styled-components';
import { FlexWrapper } from '@wrtn/ui/styles'
import { $generateHtmlFromNodes } from '@lexical/html';


const CustomFocusPlugin = ({
  children,
  setFocus,
  setCurrentSelection
}) => {
  const [editor] = useLexicalComposerContext()

  React.useEffect(() => {
    const removeRegister = editor.registerUpdateListener((listener) => {
      listener.editorState.read(() => {
        setCurrentSelection($getSelection());
      })
    });
    return () => removeRegister();
  }, [editor]);

  React.useEffect(() => {
    const removeRegisterCommand = editor.registerCommand(
      BLUR_COMMAND,
      () => {
        setFocus(false)
        setCurrentSelection($getSelection());
        
        return false
      }, 
      COMMAND_PRIORITY_LOW
    );

    return () => removeRegisterCommand();
  }, [editor])

  React.useEffect(() => {
    const removeRegisterCommand = editor.registerCommand(
      FOCUS_COMMAND,
      () => {
        setFocus(true)
        return false
      }, 
      COMMAND_PRIORITY_LOW
    );
    return () => removeRegisterCommand();
  }, [editor])  

  return (
    <Wrapper>
      {children}
    </Wrapper>
  );
}

export default CustomFocusPlugin;

const Wrapper = styled(FlexWrapper)`
  flex: 1;
  min-width: 650px;
  height: 100%;
  flex-direction: column;
  justify-content: flex-start;
`;
