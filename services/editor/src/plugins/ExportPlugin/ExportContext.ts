import React from "react";


export type ExportContext = {
  makeDoc: () => void
  makePdf: () => void
}

export const ExportContext = React.createContext<ExportContext>(undefined as any);

export const useExportContext = () => {
  const exportContext = React.useContext(ExportContext);

  if (exportContext === null) throw new Error("export context doens't exist. it should be inside ExportPlugin")
  return exportContext;
}
