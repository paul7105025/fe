import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import { saveAs } from "file-saver";

import { ElementFormatType, ElementNode } from "lexical";

import {
  Packer,
  Document,
  Paragraph,
  HeadingLevel,
  AlignmentType,
  SectionType,
  UnderlineType,
  TextRun,
} from "docx";

//개뻘짓의 흔적
// class TextRun extends Run {
//   public constructor(options: IRunOptions | string) {
//     if (typeof options === "string") {
//       super({});
//       //fixed
//       this.root.push(new Text(options));
//       return this;
//     }
//     super(options);
//   }
// }

type headerTag = "h1" | "h2" | "h3";

const DOCX_HEADER: Record<headerTag, HeadingLevel> = {
  h1: HeadingLevel.HEADING_1,
  h2: HeadingLevel.HEADING_2,
  h3: HeadingLevel.HEADING_3,
};

const DOCX_ALIGN: Record<ElementFormatType, AlignmentType> = {
  left: AlignmentType.LEFT,
  center: AlignmentType.CENTER,
  right: AlignmentType.RIGHT,
  start: AlignmentType.START,
  end: AlignmentType.END,
  justify: AlignmentType.JUSTIFIED,
  "": AlignmentType.START,
};

export const useExportDocx = ({ title }: { title?: string }) => {
  const [editor] = useLexicalComposerContext();
  const data = editor.getEditorState().toJSON();

  const makeDoc = () => {
    try {
      const doc = new Document({
        styles: {
          default: {},
        },
        sections: [
          {
            properties: {
              type: SectionType.CONTINUOUS,
            },
            children: [...data.root.children.map((p) => paragraphMaker(p))],
          },
        ],
      });

      Packer.toBlob(doc).then((blob) => {
        // saveAs from FileSaver will download the file
        saveAs(blob, `${title || "wrtn"}.docx`);
      });
    } catch (error) {
      // console.error(error);
    }
  };

  return { makeDoc };
};

const paragraphMaker = (p: any) => {
  const format = p.format as ElementFormatType;
  const children = p.children.map((c: ElementNode) => childrenTextMaker(c));

  const tag = p.tag as headerTag;

  const paragraph = new Paragraph({
    children: children,
    heading: p.type === "heading" ? DOCX_HEADER[tag] : undefined,
    alignment: DOCX_ALIGN[format],
  });

  return paragraph;
};

const formatByNumber = (styles: Array<string>, f: number) => {
  if (f / 8 >= 1) {
    return formatByNumber([...styles, "strike"], f - 8);
  } else if (f / 4 >= 1) {
    return formatByNumber([...styles, "underline"], f - 4);
  } else if (f / 2 >= 1) {
    return formatByNumber([...styles, "italic"], f - 2);
  } else if (f >= 1) {
    return formatByNumber([...styles, "bold"], f - 1);
  } else return styles;
};

const childrenTextMaker = (p: ElementNode) => {
  const format = p.format;

  const styles = formatByNumber([], format);

  const textRun = new TextRun({
    text: p.text,
    bold: styles.includes("bold"),
    italics: styles.includes("italic"),
    underline: styles.includes("underline")
      ? { type: UnderlineType.SINGLE }
      : undefined,
    strike: styles.includes("strike"),
    font: {
      name: "나눔고딕",
    },
  });

  return textRun;
};

export default useExportDocx;
