import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import { jsPDF } from "jspdf";
import {
  ElementFormatType,
  SerializedLexicalNode,
  SerializedTextNode,
} from "lexical";
import { pretendardFont } from "./Pretendard-Regular-normal";

type headerTag = "h1" | "h2" | "h3";

export const useExportPdf = ({ title }: { title?: string }) => {
  const [editor] = useLexicalComposerContext();
  const makePdf = () => {
    const data = editor.getEditorState().toJSON();
    const doc = new jsPDF({
      unit: "px",
    });

    let height: number = 44;
    doc.addFileToVFS("Pretendard-Regular-normal.ttf", pretendardFont);
    doc.addFont(
      "Pretendard-Regular-normal.ttf",
      "Pretendard-Regular",
      "normal"
    );
    doc.setFont("Pretendard-Regular", "normal");
    data.root.children.map((v) => {
      if (v.type === "heading") {
        height += addHeading(v, doc, height);
      } else if (v.type === "paragraph") {
        height += addParagraph(v, doc, height);
      }
    });
    doc.save(`${title || "wrtn"}.pdf`);
  };

  return { makePdf };
};
//32, 24, 18, 16

const headerPixel = {
  h1: 28,
  h2: 22,
  h3: 18,
  p: 16,
};

const MAX_WIDTH = 380;

const addHeading = (node, doc: jsPDF, height: number) => {
  const header = headerPixel[node.tag as headerTag];
  const format = node.format as ElementFormatType;

  let text = "";
  node.children.forEach((v) => {
    if (v?.text) text += v?.text;
  });

  doc.setFontSize(header);
  const textLine = doc.splitTextToSize(text, MAX_WIDTH);
  doc.text(textLine, 30, height);

  return textLine.length * header + 10;
};

const addParagraph = (node, doc: jsPDF, height: number) => {
  const format = node.format as ElementFormatType;

  let text = "";
  node.children.forEach((v) => {
    if (v?.text) text += v?.text;
  });

  doc.setFontSize(16);
  const textLine = doc.splitTextToSize(text, MAX_WIDTH);
  doc.text(textLine, 30, height);

  return textLine.length * 16 + 10;
};

export default useExportPdf;
