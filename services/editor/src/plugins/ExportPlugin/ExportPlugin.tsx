import { ExportContext } from "./ExportContext";
import { useExportDocx } from "./useExportDocx";
import useExportPdf from "./useExportPdf";

interface ExportPluginProps {
  title: string;
  children: React.ReactNode;
}

const ExportPlugin = ({ title, children }: ExportPluginProps) => {
  const { makeDoc } = useExportDocx({ title });
  const { makePdf } = useExportPdf({ title });

  return (
    <ExportContext.Provider value={{ makeDoc, makePdf }}>
      {children}
    </ExportContext.Provider>
  );
};

export default ExportPlugin;
