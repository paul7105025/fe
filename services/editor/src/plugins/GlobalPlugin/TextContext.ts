import React from "react";


export type TextContext = {
  textState: [string, React.Dispatch<React.SetStateAction<string>>];
}

export const TextContext = React.createContext<TextContext>(undefined as any);

export const useTextContext = () => {
  const textContext = React.useContext(TextContext);

  if (textContext === null) throw new Error("text context doens't exist. it should be inside GlobalPlugin")
  return textContext;
}
