import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import React from "react";
import { GlobalContext,  isTextCountTrimStateType } from "./Context";
import { textStateType } from './Context'

// 어디서든지 사용할 수 있는 데이터들을 모아놓습니다.
// 규모가 커진다싶으면, 별도의 Plugin으로 분리하는 걸 지향합니다.
const GlobalPlugin = ({
  children
}) => {
  const [editor] = useLexicalComposerContext();
  const [text, setText] = React.useState("");
  const [isTextCountTrim, setIsTextCountTrim] = React.useState(false);

  const textState = React.useMemo(() => {
    const context = [text, setText] as textStateType
    return context;
  }, [text]);

  const isTextCountTrimState = React.useMemo(() => {
    const context =[isTextCountTrim, setIsTextCountTrim] as isTextCountTrimStateType
    return context;
  }, [isTextCountTrim])


  React.useEffect(() => {
    setText(editor.getRootElement()?.textContent || "")
    editor.registerTextContentListener((textContent) => {
      if (textContent.trim() === "​") {
        setText("")
      } else {
        setText(textContent);
      }
    })
  }, [editor])

  return (
    <GlobalContext.Provider value={{textState, isTextCountTrimState} as GlobalContext}>
      {children}
    </GlobalContext.Provider>
    // <IsTextCountTrimContext.Provider value={}>
    //   <TextContext.Provider value={textContext}>
    //     {children}
    //   </TextContext.Provider>
    // </IsTextCountTrimContext.Provider>
  )
}

export default GlobalPlugin;