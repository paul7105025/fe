import React from "react";

export type textStateType = [string, React.Dispatch<React.SetStateAction<string>>];
export type isTextCountTrimStateType = [boolean, React.Dispatch<React.SetStateAction<boolean>>]
export type GlobalContext = {
  isTextCountTrimState: isTextCountTrimStateType;
  textState: textStateType;
}

export const GlobalContext = React.createContext<GlobalContext>(undefined as any);

export const useGlobalContext = () => {
  const context = React.useContext(GlobalContext);

  if (context === null) throw new Error("globalcontext doens't exist. it should be inside GlobalPlugin")
  return context;
}