import React from "react";
import styled from "styled-components";
import { FlexWrapper, colors, typo } from "@wrtn/ui/styles";
import { ReactComponent as Forward } from "../../assets/icons/icon_forward.svg";
import { ReactComponent as Backward } from "../../assets/icons/icon_backward.svg";
import { ReactComponent as H1 } from "../../assets/icons/icon_h1.svg";
import { ReactComponent as H2 } from "../../assets/icons/icon_h2.svg";
import { ReactComponent as H3 } from "../../assets/icons/icon_h3.svg";
import { ReactComponent as Bold } from "../../assets/icons/icon_bold.svg";
import { ReactComponent as Italic } from "../../assets/icons/icon_italic.svg";
import { ReactComponent as Underline } from "../../assets/icons/icon_underline.svg";
import { ReactComponent as Strike } from "../../assets/icons/icon_strike.svg";
import { ReactComponent as LeftAlign } from "../../assets/icons/icon_left_align.svg";
import { ReactComponent as MiddleAlign } from "../../assets/icons/icon_middle_align.svg";
import { ReactComponent as RightAlign } from "../../assets/icons/icon_right_align.svg";
import { ReactComponent as More } from "../../assets/icons/icon_more.svg";

import { Copy } from "@wrtn/ui/assets/SVGComponent/Icon";

import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import { $isListNode, ListNode } from "@lexical/list";
import {
  $createHeadingNode,
  $createQuoteNode,
  $isHeadingNode,
  HeadingTagType,
} from "@lexical/rich-text";
import { $wrapNodes } from "@lexical/selection";

import {
  $getSelection,
  $createParagraphNode,
  $isRangeSelection,
  $isRootOrShadowRoot,
  COMMAND_PRIORITY_CRITICAL,
  CAN_UNDO_COMMAND,
  CAN_REDO_COMMAND,
  UNDO_COMMAND,
  REDO_COMMAND,
  SELECTION_CHANGE_COMMAND,
  FORMAT_TEXT_COMMAND,
  FORMAT_ELEMENT_COMMAND,
  $nodesOfType,
  $getTextContent,
} from "lexical";

import {
  $findMatchingParent,
  mergeRegister,
  $getNearestNodeOfType,
} from "@lexical/utils";
import SettingMenuListContainer from "../../containers/SettingMenuListContainer";
import { useTextContext } from "../GlobalPlugin/TextContext";
import { useAlert } from "react-alert";
import { useGlobalContext } from "../GlobalPlugin/Context";

type blockType =
  | "bullet"
  | "check"
  | "code"
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "h5"
  | "h6"
  | "number"
  | "paragraph"
  | "quote";

const blockTypeToBlockName: Record<blockType, string> = {
  bullet: "Bulleted List",
  check: "Check List",
  code: "Code Block",
  h1: "Heading 1",
  h2: "Heading 2",
  h3: "Heading 3",
  h4: "Heading 4",
  h5: "Heading 5",
  h6: "Heading 6",
  number: "Numbered List",
  paragraph: "Normal",
  quote: "Quote",
};

const ToolBarPlugin = () => {
  const alert = useAlert();

  const [editor] = useLexicalComposerContext();
  const { textState, isTextCountTrimState } = useGlobalContext();
  const [text] = textState;
  const [isTextCountTrim] = isTextCountTrimState;

  const [isBold, setIsBold] = React.useState<boolean>(false);
  const [isStrike, setIsStrike] = React.useState(false);
  const [isUnderline, setIsUnderline] = React.useState(false);
  const [isItalic, setIsItalic] = React.useState(false);
  const [isH1, setIsH1] = React.useState(false);
  const [isH2, setIsH2] = React.useState(false);
  const [isH3, setIsH3] = React.useState(false);
  const [isLeft, setIsLeft] = React.useState(false);
  const [isCenter, setIsCenter] = React.useState(false);
  const [isRight, setIsRight] = React.useState(false);

  const [currentBlockType, setCurrentBlockType] =
    React.useState<blockType>("paragraph");

  const updateToolbar = React.useCallback(() => {
    const selection = $getSelection();
    if ($isRangeSelection(selection)) {
      const anchorNode = selection.anchor.getNode();
      let element =
        anchorNode.getKey() === "root"
          ? anchorNode
          : $findMatchingParent(anchorNode, (e) => {
              const parent = e.getParent();
              return parent !== null && $isRootOrShadowRoot(parent);
            });

      if (element === null) {
        element = anchorNode.getTopLevelElementOrThrow();
      }

      const elementKey = element.getKey();
      const elementDOM = editor.getElementByKey(elementKey);

      // text styling
      setIsBold(selection.hasFormat("bold"));
      setIsItalic(selection.hasFormat("italic"));
      setIsStrike(selection.hasFormat("strikethrough"));
      setIsUnderline(selection.hasFormat("underline"));

      // align

      setIsLeft(
        element.getFormatType() === "" || element.getFormatType() === "left"
      );
      setIsCenter(element.getFormatType() === "center");
      setIsRight(element.getFormatType() === "right");

      // heading

      if (elementDOM !== null) {
        const type = $isHeadingNode(element)
          ? element.getTag()
          : element.getType();

        if (type in blockTypeToBlockName) {
          setCurrentBlockType(type as keyof typeof blockTypeToBlockName);
          setIsH1(type === "h1");
          setIsH2(type === "h2");
          setIsH3(type === "h3");
        }
      }
    }
  }, [editor]);

  React.useEffect(() => {
    return mergeRegister(
      editor.registerUpdateListener(({ editorState }) => {
        editorState.read(() => {
          updateToolbar();
        });
      })
      // editor.registerCommand<boolean>(
      //   CAN_UNDO_COMMAND,
      //   (payload) => {
      //     setCanUndo(payload);
      //     return false;
      //   },
      //   COMMAND_PRIORITY_CRITICAL,
      // ),
      // editor.registerCommand<boolean>(
      //   CAN_REDO_COMMAND,
      //   (payload) => {
      //     setCanRedo(payload);
      //     return false;
      //   },
      //   COMMAND_PRIORITY_CRITICAL,
      // )
    );
  }, [editor, updateToolbar]);

  const handleToggleHistory = React.useCallback(
    (e: React.MouseEvent<HTMLElement>, type: string) => {
      e.preventDefault();
      e.stopPropagation();
      if (type === "undo") editor.dispatchCommand(UNDO_COMMAND, undefined);
      else if (type === "redo") editor.dispatchCommand(REDO_COMMAND, undefined);
    },
    [editor]
  );

  const handleToggleHeading = React.useCallback(
    (e: React.MouseEvent<HTMLElement>, type: HeadingTagType) => {
      e.preventDefault();
      e.stopPropagation();
      if (currentBlockType !== type) {
        editor.update(() => {
          const selection = $getSelection();
          if ($isRangeSelection(selection)) {
            $wrapNodes(selection, () => $createHeadingNode(type));
          }
        });
      } else {
        editor.update(() => {
          const selection = $getSelection();
          if ($isRangeSelection(selection)) {
            $wrapNodes(selection, () => $createParagraphNode());
          }
        });
      }
    },
    [editor, currentBlockType]
  );

  const handleToggleTextStyle = (e: React.MouseEvent<HTMLElement>, type) => {
    editor.dispatchCommand(FORMAT_TEXT_COMMAND, type);
  };

  const handleToggleElementStyle = React.useCallback(
    (e: React.MouseEvent<HTMLElement>, type) => {
      e.preventDefault();
      e.stopPropagation();
      editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, type);
    },
    [editor]
  );

  const handleCopy = () => {
    editor.update(() => {
      navigator.clipboard.writeText(text);
    });
    alert.show("문서를 복사했어요.");
  };

  return (
    <Wrapper justify="center">
      <FlexWrapper justify="center" align="center">
        <StyleWrapper>
          <ToolbarIconStroke onClick={(e) => handleToggleHistory(e, "undo")}>
            <Backward />
          </ToolbarIconStroke>
          <ToolbarIconStroke onClick={(e) => handleToggleHistory(e, "redo")}>
            <Forward />
          </ToolbarIconStroke>
          <VerticalDivider />
          <ToolbarIcon
            isSelected={isH1}
            onClick={(e) => handleToggleHeading(e, "h1")}
          >
            <H1 />
          </ToolbarIcon>
          <ToolbarIcon
            isSelected={isH2}
            onClick={(e) => handleToggleHeading(e, "h2")}
          >
            <H2 />
          </ToolbarIcon>
          <ToolbarIcon
            isSelected={isH3}
            onClick={(e) => handleToggleHeading(e, "h3")}
          >
            <H3 />
          </ToolbarIcon>
          <VerticalDivider />
          <ToolbarIcon
            isSelected={isBold}
            onClick={(e) => handleToggleTextStyle(e, "bold")}
          >
            <Bold />
          </ToolbarIcon>
          <ToolbarIcon
            isSelected={isItalic}
            onClick={(e) => handleToggleTextStyle(e, "italic")}
          >
            <Italic />
          </ToolbarIcon>
          <ToolbarIcon
            isSelected={isUnderline}
            onClick={(e) => handleToggleTextStyle(e, "underline")}
          >
            <Underline />
          </ToolbarIcon>
          <ToolbarIcon
            isSelected={isStrike}
            onClick={(e) => handleToggleTextStyle(e, "strikethrough")}
          >
            <Strike />
          </ToolbarIcon>
          <VerticalDivider />
          <ToolbarIcon
            style={{ width: "34px" }}
            isSelected={isLeft}
            onClick={(e) => handleToggleElementStyle(e, "left")}
          >
            <LeftAlign />
          </ToolbarIcon>
          <ToolbarIcon
            style={{ width: "34px" }}
            isSelected={isCenter}
            onClick={(e) => handleToggleElementStyle(e, "center")}
          >
            <MiddleAlign />
          </ToolbarIcon>
          <ToolbarIcon
            style={{ width: "34px" }}
            isSelected={isRight}
            onClick={(e) => handleToggleElementStyle(e, "right")}
          >
            <RightAlign />
          </ToolbarIcon>
          <VerticalDivider />
        </StyleWrapper>
        <div style={{ flex: 0.8 }} />
        <OptionWrapper>
          <TextLength>
            {!isTextCountTrim
              ? text?.length || 0
              : text?.replace(/\s/g, "").length || 0}
            자
          </TextLength>
          {/* <More /> */}
          <SettingMenuListContainer />
        </OptionWrapper>
        <div style={{ flex: 1 }} />
      </FlexWrapper>
      <CopyIconWrapper>
        <CopyIcon onClick={handleCopy} />
      </CopyIconWrapper>
    </Wrapper>
  );
};

export default ToolBarPlugin;

const CopyIcon = styled(Copy)`
  width: 15px;
  height: 18px;

  > path {
    fill: ${colors.gray_55};
  }

  cursor: pointer;

  &:hover {
    path {
      fill: ${colors.POINT_PURPLE};
    }
  }
`;

const CopyIconWrapper = styled.div`
  position: absolute;
  right: 22px;
`;

const VerticalDivider = styled.div`
  height: 100%;
  width: 1px;
  background: ${colors.gray_40};
  margin: 0px 10px;
`;

const Wrapper = styled(FlexWrapper)`
  height: 46px;
  border-bottom: 1px solid ${colors.gray_40};
  width: 100%;
`;

const OptionWrapper = styled(FlexWrapper)`
  gap: 10px;
`;

const StyleWrapper = styled(FlexWrapper)`
  height: 100%;
`;

const TextLength = styled.p`
  white-space: nowrap;
  ${typo({ weight: 600, size: "14px", height: "100%", color: colors.gray_60 })};
  min-width: 60px;
  text-align: right;
`;

interface ToolbarIconProps {
  isSelected?: boolean;
}

const ToolbarIcon = styled.div<ToolbarIconProps>`
  cursor: pointer;
  width: 28px;
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: center;

  ${(props) =>
    props.isSelected &&
    `
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  `};

  &:hover {
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
        // stroke: ${colors.POINT_PURPLE};
      }
    }
  }
`;

const ToolbarIconStroke = styled.div<ToolbarIconProps>`
  cursor: pointer;
  width: 36px;
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: center;

  ${(props) =>
    props.isSelected &&
    `
    svg {
      path {
        fill: ${colors.POINT_PURPLE};
      }
    }
  `};

  &:hover {
    svg {
      path {
        stroke: ${colors.POINT_PURPLE};
      }
    }
  }
`;
