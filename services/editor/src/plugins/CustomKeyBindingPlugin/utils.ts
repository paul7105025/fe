export const CAN_USE_DOM: boolean =
  typeof window !== 'undefined' &&
  typeof window.document !== 'undefined' &&
  typeof window.document.createElement !== 'undefined';

export const IS_APPLE: boolean =
CAN_USE_DOM && /Mac|iPod|iPhone|iPad/.test(navigator.platform);

export function controlOrMeta(metaKey: boolean, ctrlKey: boolean): boolean {
  if (IS_APPLE) {
    return metaKey;
  }
  return ctrlKey;
}

export function isGenerateKey(
  keyCode: number, 
  altKey: boolean,
  metaKey: boolean,
  ctrlKey: boolean,
): boolean {
  return keyCode === 13 // enter
  && !altKey 
  && controlOrMeta(metaKey, ctrlKey);
}

export function isContinualGenerate(
  keyCode: number, 
  altKey: boolean,
  metaKey: boolean,
  ctrlKey: boolean,
): boolean {
  return keyCode === 13 // enter
  && !altKey 
  && controlOrMeta(metaKey, ctrlKey);
}

export function isRewriteGenerate(
  keyCode: number, 
  altKey: boolean,
  metaKey: boolean,
  ctrlKey: boolean,
): boolean {
  return keyCode === 220 // |
  && !altKey 
  && controlOrMeta(metaKey, ctrlKey);
}