import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import { useEvent, useFreePlanBubble, useTagManager } from "@wrtn/core";

import {
  COMMAND_PRIORITY_CRITICAL,
  COMMAND_PRIORITY_HIGH,
  createCommand,
  KEY_ENTER_COMMAND,
  LexicalCommand,
  LexicalEditor,
} from "lexical";
import React from "react";
import { useRecoilValue } from "recoil";
import { useGenerateContext } from "../GeneratePlugin/GenerateContext";
import useEditorGenerate, {
  EDITOR_TOOL_ID,
} from "../GeneratePlugin/useEditorGenerate";
import { controlOrMeta, isContinualGenerate, isRewriteGenerate } from "./utils";

export const KEY_GENERATE_CONTINUAL: LexicalCommand<KeyboardEvent | null> =
  createCommand("KEY_GENERATE_CONTINUAL");

export const KEY_GENERATE_REWRITE: LexicalCommand<KeyboardEvent | null> =
  createCommand("KEY_GENERATE_REWRITE");

export const KEY_GENERATE: LexicalCommand<KeyboardEvent | null> =
  createCommand("KEY_GENERATE");

const CustomKeyBindingPlugin = ({
  formatMeta,
  documentMetadata,
  isFocus,
  setMeta,
}) => {
  const [editor] = useLexicalComposerContext();
  const tagManager = useTagManager();
  const { collectEvent } = useEvent();

  const { genNodeKeyState, genCountState, genLoadingState } =
    useGenerateContext();

  const [genNodeKey, setGenNodeKey] = genNodeKeyState;
  const [genLoading, setGenLoading] = genLoadingState;

  const { handleGenerateContinual, handleGenerateRewrite } = useEditorGenerate({
    metadata: documentMetadata,
    formatMeta,
  });

  // Shift + Enter 시 기본 Enter 기능을 차단합니다.
  React.useEffect(() => {
    const removeCommand = editor.registerCommand(
      KEY_ENTER_COMMAND,
      (e: KeyboardEvent) => {
        if (controlOrMeta(e.metaKey, e.ctrlKey)) return true;
        return false;
      },
      COMMAND_PRIORITY_HIGH
    );
    return () => removeCommand();
  }, [editor]);

  React.useEffect(() => {
    const f = (e: KeyboardEvent) => handleKeyDown(e, editor);
    window.addEventListener("keydown", f);
    return () => {
      window.removeEventListener("keydown", f);
    };
  }, [editor]);

  // 이어 쓰기
  React.useEffect(() => {
    const removeCommand = editor.registerCommand(
      KEY_GENERATE_CONTINUAL,
      (e: KeyboardEvent) => {
        setMeta("connect", "선택 안 함");
        tagManager({
          event: "editor_click_continue",
        });
        collectEvent("click_generate_btn", {
          feature_menu: "editor",
          generate_position: "editor",
          feature_category: "",
          feature_name: "이어쓰기",
          feature_tag: "",
          feature_form: "",
          prompt_id: [EDITOR_TOOL_ID],
          feature_prompt: "",
          repeat_count: 1,
          method: "shortcut",
        });

        return handleGenerateContinual({
          isFocus,
        });
      },
      COMMAND_PRIORITY_CRITICAL
    );
    return () => removeCommand();
  }, [
    editor,
    genNodeKey,
    setGenNodeKey,
    genLoading,
    setGenLoading,
    documentMetadata,
    isFocus,
    editor.getEditorState(),
  ]);

  // 다시 쓰기
  React.useEffect(() => {
    const removeCommand = editor.registerCommand(
      KEY_GENERATE_REWRITE,
      (e: KeyboardEvent) => {
        e.preventDefault();
        e.stopPropagation();
        tagManager({
          event: "editor_click_rewrite",
        });
        collectEvent("click_generate_btn", {
          feature_menu: "editor",
          generate_position: "editor",
          feature_category: "",
          feature_name: "다시쓰기",
          feature_tag: "",
          feature_form: "",
          prompt_id: [EDITOR_TOOL_ID],
          feature_prompt: "",
          repeat_count: 1,
          method: "button",
        });
        return handleGenerateRewrite();
      },
      COMMAND_PRIORITY_CRITICAL
    );
    return () => removeCommand();
  }, [editor, genNodeKey, setGenNodeKey, genLoading, setGenLoading]);

  const handleKeyDown = (e: KeyboardEvent, editor: LexicalEditor) => {
    const { keyCode, shiftKey, ctrlKey, metaKey, altKey } = e;

    // if (isGenerateKey(keyCode, altKey, metaKey, ctrlKey)) {
    //   e.preventDefault();
    //   e.stopPropagation();
    //   editor.dispatchCommand(KEY_GENERATE, e);
    // }
    if (isContinualGenerate(keyCode, altKey, metaKey, ctrlKey)) {
      e.preventDefault();
      e.stopPropagation();
      editor.dispatchCommand(KEY_GENERATE_CONTINUAL, e);
    }
    if (isRewriteGenerate(keyCode, altKey, metaKey, ctrlKey)) {
      e.preventDefault();
      e.stopPropagation();
      editor.dispatchCommand(KEY_GENERATE_REWRITE, e);
    }
  };
  return <></>;
};

export default CustomKeyBindingPlugin;
