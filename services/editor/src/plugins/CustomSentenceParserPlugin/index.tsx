import React from "react";
import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import { $getSelection, RangeSelection } from "lexical";

/**
 * 커서 앞에 있는 텍스트가 완전한 문장인지 판별합니다.
 */
const CustomSentenceParserPlugin = ({
  setIsSentence,
}) => {
  const [editor] = useLexicalComposerContext();

  // 현재 마우스 커서가 가리키는 곳보다 앞에 있는 노드의 텍스트가 문장으로 끝나는지?
  // TODO: 문장 순서 제대로 읽어오지 않는 문제가 있습니다. 
  React.useEffect(() => {
    const removeRegister = editor.registerUpdateListener((listener) => {
      listener.editorState.read(() => {
        const selection = $getSelection();
        let text = ""

        if (!selection || selection.getTextContent() !== "") return ;
        const find = selection.getNodes();
        
        listener.editorState._nodeMap.forEach((v, nodeKey) => {
            if (v.getType() !== "paragraph" && v.getType() !== "root" && v.getType() !== 'HighlightText' && v.getType() !== 'Generate' && (v?.isBefore(find[0]))) {
              text += v.getTextContent()
            }
            if (v === find[0] ) {
              // @ts-ignore
              text += v.getTextContent().slice(0, selection?.anchor.offset)

            }
        })
        const trimed = text.trim();
        setIsSentence(!!trimed.match(/[.?!]$/));
      })
    })

    return () => removeRegister()
  }, [editor])

  return (
    <></>
  )
}

export default CustomSentenceParserPlugin;