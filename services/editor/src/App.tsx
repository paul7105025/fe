import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ToastMessage } from "@wrtn/ui/components/ToastMessage";
import { colors } from "@wrtn/ui/styles";
import { positions, Provider as AlertProvider, transitions } from "react-alert";
import { useCookies } from "react-cookie";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { RecoilRoot } from "recoil";
import styled from "styled-components";
import EditorDocumentListPage from "./exposes/EditorDocumentListPage";
import EditorDocumentPage from "./exposes/EditorDocumentPage";
import polyfill from "@wrtn/ui/styles/polyfill";

const alertOptions = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "30px",
  transition: transitions.FADE,
};

function App() {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });

  return (
    <QueryClientProvider client={queryClient}>
      <AlertProvider template={ToastMessage} {...alertOptions}>
        <RecoilRoot override={false}>
          <BrowserRouter>
            <Routes>
              <Route path="editor">
                <Route
                  path=""
                  element={
                    <Wrapper>
                      <ContentWrapper>
                        <AsideWrapper />
                        <EditorDocumentListPage />
                      </ContentWrapper>
                    </Wrapper>
                  }
                />
                <Route
                  path=":docId"
                  element={
                    <Wrapper>
                      <ContentWrapper>
                        <AsideWrapper />
                        <EditorDocumentPage
                          onClickLNB={() => {}}
                          isOpenLNB={true}
                          onClickChannel={() => {}}
                        />
                      </ContentWrapper>
                    </Wrapper>
                  }
                />
              </Route>
            </Routes>
          </BrowserRouter>
        </RecoilRoot>
      </AlertProvider>
    </QueryClientProvider>
  );
}

export default App;

const Wrapper = styled.div`
  display: flex;
  background: #f2f7ff;
  width: 100%;
  ${polyfill.dvh("height", 100)};
  @media (max-width: 1023px) {
    flex-direction: column;
  }
  background-color: ${colors.BACKGROUND};
`;

const AsideWrapper = styled.aside`
  @media (max-width: 1023px) {
    display: none;
  }
`;

const ContentWrapper = styled.div`
  background: white;
  width: 100%;
  height: 100%;
  border-top-left-radius: 20px;
  filter: drop-shadow(0px 4px 24px rgba(151, 149, 207, 0.05));
  overflow: hidden;

  @media (max-width: 1023px) {
    border-top-left-radius: 0px;
  }
`;
