import Graphemer from 'graphemer';

const splitter = new Graphemer();
// 기본 유니코드 -> 호환형 자모
const mapping = new Map([
    [
    4352,
    12593
    ],
    [
    4520,
    12593
    ],
    [
    4353,
    12594
    ],
    [
    4521,
    12594
    ],
    [
    4522,
    12595
    ],
    [
    4354,
    12596
    ],
    [
    4523,
    12596
    ],
    [
    4372,
    12645
    ],
    [
    4607,
    12645
    ],
    [
    4373,
    12646
    ],
    [
    4550,
    12646
    ],
    [
    4443,
    12647
    ],
    [
    4551,
    12647
    ],
    [
    4552,
    12648
    ],
    [
    4444,
    12597
    ],
    [
    4524,
    12597
    ],
    [
    4445,
    12598
    ],
    [
    4525,
    12598
    ],
    [
    4355,
    12599
    ],
    [
    4526,
    12599
    ],
    [
    4356,
    12600
    ],
    [
    55245,
    12600
    ],
    [
    4357,
    12601
    ],
    [
    4527,
    12601
    ],
    [
    43364,
    12602
    ],
    [
    4528,
    12602
    ],
    [
    4556,
    12649
    ],
    [
    43366,
    12650
    ],
    [
    4558,
    12650
    ],
    [
    43368,
    12603
    ],
    [
    4529,
    12603
    ],
    [
    43369,
    12604
    ],
    [
    4530,
    12604
    ],
    [
    4563,
    12651
    ],
    [
    43372,
    12605
    ],
    [
    4531,
    12605
    ],
    [
    4567,
    12652
    ],
    [
    4532,
    12606
    ],
    [
    4533,
    12607
    ],
    [
    4378,
    12608
    ],
    [
    4534,
    12608
    ],
    [
    4569,
    12653
    ],
    [
    4358,
    12609
    ],
    [
    4535,
    12609
    ],
    [
    4380,
    12654
    ],
    [
    4572,
    12654
    ],
    [
    43377,
    12655
    ],
    [
    4573,
    12655
    ],
    [
    4575,
    12656
    ],
    [
    4381,
    12657
    ],
    [
    4578,
    12657
    ],
    [
    4359,
    12610
    ],
    [
    4536,
    12610
    ],
    [
    4382,
    12658
    ],
    [
    4384,
    12659
    ],
    [
    55267,
    12659
    ],
    [
    4360,
    12611
    ],
    [
    55270,
    12611
    ],
    [
    4385,
    12612
    ],
    [
    4537,
    12612
    ],
    [
    4386,
    12660
    ],
    [
    4387,
    12661
    ],
    [
    55271,
    12661
    ],
    [
    4391,
    12662
    ],
    [
    55272,
    12662
    ],
    [
    4393,
    12663
    ],
    [
    4395,
    12664
    ],
    [
    4582,
    12664
    ],
    [
    4396,
    12665
    ],
    [
    4361,
    12613
    ],
    [
    4538,
    12613
    ],
    [
    4397,
    12666
    ],
    [
    4583,
    12666
    ],
    [
    4398,
    12667
    ],
    [
    4399,
    12668
    ],
    [
    4584,
    12668
    ],
    [
    4402,
    12669
    ],
    [
    4586,
    12669
    ],
    [
    4362,
    12614
    ],
    [
    4539,
    12614
    ],
    [
    4406,
    12670
    ],
    [
    55279,
    12670
    ],
    [
    4416,
    12671
    ],
    [
    4587,
    12671
    ],
    [
    4363,
    12615
    ],
    [
    4540,
    12615
    ],
    [
    4423,
    12672
    ],
    [
    4428,
    12673
    ],
    [
    4592,
    12673
    ],
    [
    4593,
    12674
    ],
    [
    4594,
    12675
    ],
    [
    4364,
    12616
    ],
    [
    4541,
    12616
    ],
    [
    4365,
    12617
    ],
    [
    55289,
    12617
    ],
    [
    4366,
    12618
    ],
    [
    4542,
    12618
    ],
    [
    4367,
    12619
    ],
    [
    4543,
    12619
    ],
    [
    4368,
    12620
    ],
    [
    4544,
    12620
    ],
    [
    4369,
    12621
    ],
    [
    4545,
    12621
    ],
    [
    4439,
    12676
    ],
    [
    4596,
    12676
    ],
    [
    4370,
    12622
    ],
    [
    4546,
    12622
    ],
    [
    4440,
    12677
    ],
    [
    4441,
    12678
    ],
    [
    4601,
    12678
    ],
    [
    4447,
    12644
    ],
    [
    4449,
    12623
    ],
    [
    4450,
    12624
    ],
    [
    4451,
    12625
    ],
    [
    4452,
    12626
    ],
    [
    4453,
    12627
    ],
    [
    4454,
    12628
    ],
    [
    4455,
    12629
    ],
    [
    4456,
    12630
    ],
    [
    4457,
    12631
    ],
    [
    4458,
    12632
    ],
    [
    4459,
    12633
    ],
    [
    4460,
    12634
    ],
    [
    4461,
    12635
    ],
    [
    4484,
    12679
    ],
    [
    4485,
    12680
    ],
    [
    4488,
    12681
    ],
    [
    4462,
    12636
    ],
    [
    4463,
    12637
    ],
    [
    4464,
    12638
    ],
    [
    4465,
    12639
    ],
    [
    4466,
    12640
    ],
    [
    4497,
    12682
    ],
    [
    4498,
    12683
    ],
    [
    4500,
    12684
    ],
    [
    4467,
    12641
    ],
    [
    4468,
    12642
    ],
    [
    4469,
    12643
    ],
    [
    4510,
    12685
    ],
    [
    4513,
    12686
    ],
    [
    4448,
    12644
    ]
])
// TODO: NFKC 정규화를 한 후 초성, 종성 문자들을 호환형 자모로 변경합니다.
export const normalizeKorUnicode = (str: string) => {
  const unicodes: Array<string> = [];
  const nfkc = str.normalize("NFKC")
  // const strLength = str.
  const split = splitter.splitGraphemes(nfkc);
  for (let i = 0; i < split.length; i++) {
    let cp = split[i];

    if (!cp) continue ;
    let j = 0;
    const codePoints: Array<number> = [];
    while(cp.codePointAt(j)) {
      const r = mapping.get(cp.codePointAt(j) || 0);
      if (r) codePoints.push(r);
      j++;
    }

    cp = String.fromCodePoint(...codePoints) || cp;
    unicodes.push(cp);
  }

  return unicodes.join("");
}

// 아래 코드 
// https://cosmic.mearie.org/f/ngsdoc/appendix_coderef.htm
//  console에서 실행하면 Map 구조로 가져올 수 있음.
// 복붙이 안되긴하는데 어떻게든 가져온 후에 테이블로 사용하면 될듯

// const mappping = new Map();

// let parent = document.querySelector("body > table > tbody:nth-child(1)")?.children;

// let isValid = (str) => str && str.trim().length > 0 && str.trim().charAt(0) === 'U' && str.trim().charAt(1) === '+'
// let to16 = (str) => str.slice(2);

// if (parent) {
//   for (let i = 0; i< parent.length; i++ ){

//     if (parent && (i === parent.length - 1 || i === parent.length - 2) && i <= 1) {

//     } else {
//       const tr = parent.item(i);

//       const answer = tr?.children.item(1)?.textContent;
//       const chosung = tr?.children.item(2)?.textContent;
//       const jongsung = tr?.children.item(3)?.textContent;


//       if (isValid(answer) && isValid(chosung)) {
//         // @ts-ignore
//         mappping.set(parseInt(to16(chosung?.trim()), 16), parseInt(to16(answer?.trim()), 16));
//       }
//       if (isValid(answer) && isValid(jongsung)) {
//         mappping.set(parseInt(to16(jongsung?.trim()), 16), parseInt(to16(answer?.trim()), 16));
//       }
//     }

//   }
   
// }




// const mappping = new Map();

// let parent = document.querySelector("body > table > tbody:nth-child(1)")?.children;

// let isValid = (str) => str && str.trim().length > 0 && str.trim().charAt(0) === 'U' && str.trim().charAt(1) === '+'
// let to16 = (str) => str.slice(2);

// if (parent) {
//   for (let i = 0; i< parent.length; i++ ){

//     if (parent && (i === parent.length - 1 || i === parent.length - 2) && i <= 1) {

//     } else {
//       const tr = parent.item(i);

//       const answer = tr?.children.item(1)?.textContent;
//       const middlesung = tr?.children.item(2)?.textContent;
//     //   const jongsung = tr?.children.item(3)?.textContent;


//     //   if (isValid(answer) && isValid(chosung)) {
//     //     // @ts-ignore
//     //     mappping.set(parseInt(to16(chosung?.trim()), 16), parseInt(to16(answer?.trim()), 16));
//     //   }
//       if (isValid(answer) && isValid(middlesung)) {
//         mappping.set(parseInt(to16(middlesung?.trim()), 16), parseInt(to16(answer?.trim()), 16));
//       }
//     }

//   }
   
// }


