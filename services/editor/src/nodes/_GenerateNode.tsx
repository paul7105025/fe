import {
  EditorConfig,
  ElementNode,
  LexicalNode,
  NodeKey,
  SerializedTextNode,
  Spread,
} from 'lexical';

import {
  $applyNodeReplacement, 
  TextNode
} from 'lexical';


export type SerializedGenerateNode = Spread<
  {
    className: string;
    type: 'Generate';
  },
  SerializedTextNode
>;

export class GenerateNode extends ElementNode {
  __className: string;

  static getType(): string {
    return 'Generate';
  }

  static clone(node: GenerateNode): GenerateNode {
    return new GenerateNode(node.__className, node.__key);
  }

  constructor(className: string, key?: NodeKey) {
    super(key);
    this.__className = className;
  }

  createDOM(config: EditorConfig): HTMLElement {
    const dom = document.createElement('kdb');
    // dom.style.backgroundImage = "https://lex.page/assets/loader-96e37726c2f9b114c963eb78c109ee07b3caef97e473e931e7a4f8c47b044117.svg"
    // dom.('background-image', 'https://lex.page/assets/loader-96e37726c2f9b114c963eb78c109ee07b3caef97e473e931e7a4f8c47b044117.svg')
    // dom.setAttribute('width', '1em');
    // dom.setAttribute('height', '1em');
    // dom.setAttribute('')

    dom.style.cssText = `
      background-image: url(https://lex.page/assets/loader-96e37726c2f9b114c963eb78c109ee07b3caef97e473e931e7a4f8c47b044117.svg);
      width: 1em;
      height: 1em;
      display: inline-block;
      user-select: none;
      color: transparent;
    `
    // const inner = super.createDOM(config);
    dom.className = this.__className;
    // inner.className = 'Generate-inner';
    // dom.appendChild(inner);
    return dom;
  }

  isInline(): boolean {
    return true;
  }

  canInsertTextBefore(): boolean {
    return true;
  }
  canBeEmpty(): boolean {
    return true;
  }

  canReplaceWith(replacement: LexicalNode): boolean {
    // if (replacement.)
    return true;
  }

  updateDOM(
    prevNode: TextNode,
    dom: HTMLElement,
    config: EditorConfig,
  ): boolean {
    const inner = dom.firstChild;
    if (inner === null) {
      return true;
    }
    super.updateDOM(prevNode, inner as HTMLElement, config);
    return false;
  }

  static importJSON(serializedNode: SerializedGenerateNode): GenerateNode {
    const node = $createGenerateNode(
      serializedNode.className,
    );
    // node.setFormat(serializedNode.format);
    node.setDetail(serializedNode.detail);
    node.setMode(serializedNode.mode);
    node.setStyle(serializedNode.style);
    return node;
  }

  // exportJSON(): SerializedGenerateNode {
  //   return {
  //     ...super.exportJSON(),
  //     className: this.getClassName(),
  //     type: 'Generate',
  //   };
  // }

  getClassName(): string {
    const self = this.getLatest();
    return self.__className;
  }
}

export function $isGenerateNode(
  node: LexicalNode | null | undefined,
): node is GenerateNode {
  return node instanceof GenerateNode;
}

export function $createGenerateNode(
  className: string,
): GenerateNode {
  const node = new GenerateNode(className)
  // .setMode('token');
  return $applyNodeReplacement(node);
}