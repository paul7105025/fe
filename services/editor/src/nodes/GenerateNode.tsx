import type {
  EditorConfig,
  LexicalNode,
  NodeKey,
  SerializedTextNode,
  Spread,
} from 'lexical';

import {
  $applyNodeReplacement, 
  TextNode
} from 'lexical';


export type SerializedGenerateNode = Spread<
  {
    className: string;
    type: 'Generate';
  },
  SerializedTextNode
>;

export class GenerateNode extends TextNode {
  __className: string;

  static getType(): string {
    return 'Generate';
  }

  static clone(node: GenerateNode): GenerateNode {
    return new GenerateNode(node.__className, node.__text, node.__key);
  }

  constructor(className: string, text: string, key?: NodeKey) {
    super(text, key);
    this.__className = className;
  }

  createDOM(config: EditorConfig): HTMLElement {
    const dom = document.createElement('span');
    const inner = super.createDOM(config);
    dom.className = this.__className;
    dom.style.cssText = `
    background-image: url(https://image.wrtn.ai/wrtn/editor_generate_loading_spinner.gif);
    background-size: contain;
    width: 1em;
    height: 1em;
    transform: translate(0.0em, 0.25em);
    display: inline-block;
    user-select: none;
    color: transparent;
  `
    inner.className = 'Generate-inner';
    dom.appendChild(inner);
    return dom;
  }

  updateDOM(
    prevNode: TextNode,
    dom: HTMLElement,
    config: EditorConfig,
  ): boolean {
    const inner = dom.firstChild;
    if (inner === null) {
      return true;
    }
    super.updateDOM(prevNode, inner as HTMLElement, config);
    return false;
  }

  static importJSON(serializedNode: SerializedGenerateNode): GenerateNode {
    const node = $createGenerateNode(
      serializedNode.className,
      serializedNode.text,
    );
    node.setFormat(serializedNode.format);
    node.setDetail(serializedNode.detail);
    node.setMode(serializedNode.mode);
    node.setStyle(serializedNode.style);
    return node;
  }

  exportJSON(): SerializedGenerateNode {
    return {
      ...super.exportJSON(),
      className: this.getClassName(),
      type: 'Generate',
    };
  }

  getClassName(): string {
    const self = this.getLatest();
    return self.__className;
  }
}

export function $isGenerateNode(
  node: LexicalNode | null | undefined,
): node is GenerateNode {
  return node instanceof GenerateNode;
}

export function $createGenerateNode(
  className: string,
  GenerateText: string,
): GenerateNode {
  const node = new GenerateNode(className, GenerateText).setMode('token');
  return $applyNodeReplacement(node);
}