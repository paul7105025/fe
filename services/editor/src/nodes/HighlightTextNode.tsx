import {
  colors
} from '@wrtn/ui/styles'

import type {
  EditorConfig,
  LexicalNode,
  NodeKey,
  SerializedTextNode,
  Spread,
} from 'lexical';

import {useLexicalComposerContext} from '@lexical/react/LexicalComposerContext';

import {
  $applyNodeReplacement, 
  TextNode,
  $createTextNode,
} from 'lexical';

import './HighlightTextNode.css'

export type SerializedHighlightTextNode = Spread<
  {
    className: string;
    type: 'HighlightText';
    tutorial?: boolean;
  },
  SerializedTextNode
>;

export class HighlightTextNode extends TextNode {
  __className: string;
  __isAnimating: boolean;
  __handleMouseOver: (ev: MouseEvent) => any | null;
  __handleMouseOut: (ev: MouseEvent) => any | null;
  __tutorial: boolean | undefined;


  static getType(): string {
    return 'HighlightText';
  }

  static clone(node: HighlightTextNode): HighlightTextNode {
    return new HighlightTextNode(node.__className, node.__text, node.__isAnimating, node.__handleMouseOver, node.__handleMouseOut, node.__key);
  }

  constructor(className: string, text: string, animating: boolean, handleMouseOver: (ev: MouseEvent) => any | null, handleMouseOut: (ev: MouseEvent) => any | null, key?: NodeKey, tutorial?: boolean) {
    super(text, key);
    this.__className = className;
    this.__isAnimating = animating;
    this.__handleMouseOver = handleMouseOver;
    this.__handleMouseOut = handleMouseOut
    this.__tutorial = tutorial;
  }

  createDOM(config: EditorConfig): HTMLElement {
    const dom = super.createDOM(config);
    dom.className = this.__className;
    dom.style.background = `#ded5f8`


    // INFO: 기획 변경으로 아래 이벤트는 임시로 닫습니다.
    // dom.addEventListener("click")
    // dom.addEventListener("build", this.__handleMouseOver);
    // @ts-ignore
    this.__handleMouseOver(null)
    // dom.addEventListener("visible", this.__handleMouseOver);
    // click outside
    // dom.addEventListener("")

    // dom.addEventListener("click", (e) => {

    //   if (!(e.target instanceof Node && dom.contains(e.target))) {
    //     this.__handleMouseOut(e);
    //   }
    // })
    // dom.addEventListener("mouseout", this.__handleMouseOut);
    return dom;
  }

  updateDOM(
    prevNode: HighlightTextNode,
    dom: HTMLElement,
    config: EditorConfig,
  ): boolean {
    const inner = dom.firstChild;

    if (inner === null) {
      return true;
    }
    super.updateDOM(prevNode, inner as HTMLElement, config);
    return false;
  }

  getTextContent(): string {
    return this.__text;
  }


  static importJSON(serializedNode: SerializedHighlightTextNode): TextNode | HighlightTextNode {
    // INFO: 튜토리얼 시에만 초기 값에서 하이라이팅이 유지되어야 합니다.
    if (serializedNode?.tutorial) {
      const node = $createHighlightTextNode(
        serializedNode.className,
        serializedNode.text,
        false,
        () => {},
        () => {}
      );
      node.setTextContent(serializedNode.text);
      node.setFormat(serializedNode.format);
      node.setDetail(serializedNode.detail);
      node.setMode(serializedNode.mode);
      node.setStyle(serializedNode.style);
      return node;
  
    }
    const node = $createTextNode(
      // serializedNode.className,
      serializedNode.text,
      // false,
      // () => {},
      // () => {}
    );
    node.setTextContent(serializedNode.text);
    node.setFormat(serializedNode.format);
    node.setDetail(serializedNode.detail);
    node.setMode(serializedNode.mode);
    node.setStyle(serializedNode.style);
    return node;
  }

  exportJSON(): SerializedHighlightTextNode {
    return {
      ...super.exportJSON(),
      className: this.getClassName(),
      type: 'HighlightText',
    };
  }

  getClassName(): string {
    const self = this.getLatest();
    return self.__className;
  }
}

export function $isHighlightTextNode(
  node: LexicalNode | null | undefined,
): node is HighlightTextNode {
  return node instanceof HighlightTextNode;
}

export function $createHighlightTextNode(
  className: string,
  highlightText: string,
  animating: boolean,
  handleMouseOver: (ev: MouseEvent) => any | null,
  handleMouseOut: (ev: MouseEvent) => any | null
): HighlightTextNode {
  const node = new HighlightTextNode(className, highlightText, animating, handleMouseOver, handleMouseOut).setMode('normal');
  return $applyNodeReplacement(node);
}