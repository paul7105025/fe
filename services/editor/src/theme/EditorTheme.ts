import {EditorThemeClasses} from 'lexical';
import './EditorTheme.css';

const EditorTheme: EditorThemeClasses = {
  heading: {
    h1: 'WrtnEditorTheme__Heading1',
    h2: 'WrtnEditorTheme__Heading2',
    h3: 'WrtnEditorTheme__Heading3',
  },
  text: {
    bold: 'WrtnEditorTheme__bold',
    // code: 'WrtnEditorTheme__textCode',
    italic: 'WrtnEditorTheme__italic',
    strikethrough: 'WrtnEditorTheme__strikethrough',
    // subscript: 'WrtnEditorTheme__textSubscript',
    // superscript: 'PlaygroundEditorTheme__textSuperscript',
    underline: 'WrtnEditorTheme__underline',
    underlineStrikethrough: 'WrtnEditorTheme__strikethrough__underline',
  },
  root: 'WrtnEditorTheme__root',
}

export default EditorTheme;