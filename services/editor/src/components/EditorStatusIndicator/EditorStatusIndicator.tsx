import React from "react";
import styled from "styled-components";

import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { FlexWrapper, colors, typo } from "@wrtn/ui/styles";

import { saveStatusType } from "../../types";

import { ReactComponent as Check } from "../../assets/icons/icon_check.svg";
import { ReactComponent as Help } from "../../assets/icons/icon_help.svg";

import { Spinner } from "@wrtn/ui/components/Spinner";
interface EditorStatusIndicatorProps {
  saveStatus: saveStatusType;
}

const EditorStatusIndicator = ({ saveStatus }: EditorStatusIndicatorProps) => {
  const [hover, setHover] = React.useState(false);
  const helpRef = React.useRef<HTMLDivElement>(null);

  const statusMessage = React.useMemo(() => {
    if (saveStatus === "saved") return "저장 완료";
    if (saveStatus === "saving") return "저장 중";
    if (saveStatus === "offline") return "오프라인";
    return "";
  }, [saveStatus]);

  return (
    <Wrapper>
      <Message>{statusMessage}</Message>
      {saveStatus === "saving" && (
        <Spinner width={12} height={12} color={colors.GRAY_55} />
      )}
      {saveStatus === "offline" && (
        <div
          ref={helpRef}
          onMouseOver={() => setHover(true)}
          onMouseOut={() => setHover(false)}
        >
          <IconHelp />
        </div>
      )}
      {saveStatus === "saved" && <IconCheck />}
      {hover && (
        <ModalPositionPortal
          position={{
            // @ts-ignore
            top: helpRef.current?.getBoundingClientRect().top + 25,
            // @ts-ignore
            left: helpRef.current
              ? helpRef.current.getBoundingClientRect().left +
                (helpRef.current.getBoundingClientRect().width - 341) / 2
              : 0,
          }}
        >
          <HoverWrapper>
            오프라인 상태입니다. 저장 오류 방지를 위해 문서의 내용을
            복사해주세요.
          </HoverWrapper>
        </ModalPositionPortal>
      )}
    </Wrapper>
  );
};

export default EditorStatusIndicator;

const IconCheck = styled(Check)`
  path {
    stroke: ${colors.gray_55};
    fill: ${colors.gray_55};
  }
`;

const IconHelp = styled(Help)`
  path {
    fill: ${colors.gray_55};
  }
`;

const Wrapper = styled(FlexWrapper)`
  gap: 6px;

  min-width: 100px;
  justify-content: flex-start;
`;

const Message = styled.p`
  ${typo({
    weight: 600,
    size: "14px",
    height: "100%",
    color: colors.gray_55,
  })}
`;

const HoverWrapper = styled.div`
  padding: 4px 9px;
  background-color: ${colors.gray_80};
  box-shadow: 0px 0px 5px rgba(56, 71, 130, 0.25);
  border-radius: 5px;

  ${typo({
    weight: 600,
    size: "12px",
    height: "100%",
    color: colors.WHITE,
  })};
`;
