import React from "react";
import styled, { css } from "styled-components";
import { boxShadow, colors, FlexWrapper } from "@wrtn/ui/styles";

type TutorialBubbleProps = {
  children?: any;
  direction: string;
  position: number;
  css?: any;
  style?: any;
};
const TutorialBubble = ({
  children,
  direction,
  position,
  css,
  style,
}: TutorialBubbleProps) => {
  return (
    <Wrapper direction={direction} position={position} css={css} style={style}>
      {children}
    </Wrapper>
  );
};

export default TutorialBubble;

const Common = css`
  &::after {
    position: absolute;
    content: "";
    border-width: 8px;
    border-style: solid;
  }

  &::before {
    position: absolute;
    content: "";
    border-width: 8px;
    border-radius: 1px;
    border-style: solid;
  }
`;

const Left = (position) => css`
  ${Common}
  &::after {
    top: ${position}px;
    left: -16px;
    border-color: transparent ${colors.POINT_PURPLE} transparent transparent;
  }

  &::before {
    top: ${position}px;
    left: -17px;
    border-color: transparent ${colors.POINT_PURPLE} transparent transparent;
  }
`;

const Right = (position) => css`
  ${Common}
  &::after {
    top: ${position}px;
    right: -16px;
    border-color: transparent transparent transparent ${colors.POINT_PURPLE};
  }

  &::before {
    top: ${position}px;
    right: -17px;
    border-color: transparent transparent transparent ${colors.POINT_PURPLE};
  }
`;

const Bottom = (position) => css`
  ${Common}
  &::after {
    left: ${position}px;
    bottom: -16px;
    border-color: ${colors.POINT_PURPLE} transparent transparent transparent;
  }

  &::before {
    left: ${position}px;
    bottom: -17px;
    border-color: ${colors.POINT_PURPLE} transparent transparent transparent;
  }
`;

const Top = (position) => css`
  ${Common}
  &::after {
    left: ${position}px;
    top: -16px;
    border-color: transparent transparent ${colors.POINT_PURPLE} transparent;
  }

  &::before {
    left: ${position}px;
    top: -17px;
    border-color: transparent transparent ${colors.POINT_PURPLE} transparent;
  }
`;

const directionSelector = (direction, position) => {
  switch (direction) {
    case "left":
      return Left(position);
    case "right":
      return Right(position);
    case "top":
      return Top(position);
    case "bottom":
      return Bottom(position);
    case "none":
      return css``;
    default:
      return css``;
  }
};

const Wrapper = styled(FlexWrapper)<{
  direction: string;
  position: number;
  css: any;
}>`
  border-radius: 20px;
  padding: 22px 21px 13px 21px;
  background: ${colors.POINT_PURPLE};
  ${boxShadow.guideline_shadow};

  border: 1px solid ${colors.POINT_PURPLE};

  ${({ direction, position }) => directionSelector(direction, position)}

  ${({ css }) => css}
`;
