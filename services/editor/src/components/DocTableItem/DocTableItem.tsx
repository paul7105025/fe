import styled from "styled-components";

import CheckBox from "../CheckBox";

import { docType } from "../../types";

import { dateResultFormat } from "@wrtn/core";

import { typo, colors } from "@wrtn/ui/styles";

interface DocTableItemProps {
  docList: Array<docType> | undefined;
  selectedItems: Array<string>;
  setSelectedItems: React.Dispatch<React.SetStateAction<Array<string>>>;
  onClick: (id: string) => void;
}

const isInclude = (selectedItems: string[], item: string) => {
  return selectedItems.includes(item);
};

const DocTableItem = ({
  docList,
  selectedItems,
  setSelectedItems,
  onClick,
}: DocTableItemProps) => {
  const selectItem = (item: string): void => {
    setSelectedItems((res) => {
      if (isInclude(res, item)) {
        return res.filter((i) => i !== item);
      } else {
        return [...res, item];
      }
    });
  };

  return (
    <DocTableItemContent style={{ borderCollapse: "collapse" }}>
      <thead>
        <tr>
          <DocTableItemHeader style={{ width: "15%" }}>선택</DocTableItemHeader>
          <DocTableItemHeader style={{ width: "65%" }}>이름</DocTableItemHeader>
          <DocTableItemHeader style={{ width: "20%" }}>
            마지막 수정일
          </DocTableItemHeader>
          {/* <DocTableItemHeader style={{ width: "5%" }}></DocTableItemHeader> */}
        </tr>
      </thead>
      <tbody>
        {docList?.map((data) => (
          <DocTableItemListItemWrapper key={data._id}>
            <DocTableItemData>
              <CheckBox
                consent={isInclude(selectedItems, data._id)}
                handleClick={() => selectItem(data._id)}
              />
            </DocTableItemData>
            <DocTableItemDataTitle onClick={() => onClick(data._id)}>
              {data.title}
            </DocTableItemDataTitle>
            <DocTableItemData>
              {dateResultFormat(data.updatedAt)}
            </DocTableItemData>
            {/* <DocTableItemData>
              <DocTableItemMore>
                <More />
              </DocTableItemMore>
            </DocTableItemData> */}
          </DocTableItemListItemWrapper>
        ))}
      </tbody>
    </DocTableItemContent>
  );
};

export default DocTableItem;

const DocTableItemHeader = styled.th`
  ${typo({ weight: 500, size: "14px", height: "100%", color: colors.gray_60 })};
  text-align: left;
`;

const DocTableItemData = styled.td`
  ${typo({ weight: 600, size: "16px", height: "100%", color: colors.gray_70 })};
  border-bottom: 1px solid ${colors.gray_50};
`;

const DocTableItemDataTitle = styled(DocTableItemData)`
  color: ${colors.gray_90};
  cursor: pointer;
`;

const DocTableItemListItemWrapper = styled.tr`
  // border-bottom: 1px solid ${colors.gray_60};
  height: 56px;
`;

const DocTableItemContent = styled.table`
  width: 100%;
`;
