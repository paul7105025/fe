import styled, { css } from "styled-components";
import { typo, colors, FlexButton } from "@wrtn/ui/styles";

/**
 * TS 인터페이스가 필요한  (단일) 컴포넌트거나, 자주 사용되지만 UI 라이브러리에 없는 컴포넌트들입니다.
 */

interface TabItemProps {
  selected: boolean;
  onClick: () => void;
}

export const TabItem = styled.button<TabItemProps>`
  border: none;
  background: none;

  ${typo({
    size: "18px",
    weight: 500,
    color: colors.GRAY_60,
  })}

  padding: 0px 4px 12px;

  ${({ selected }) =>
    selected &&
    `
      color: ${colors.GRAY_90};
      font-weight: 600;
      padding-bottom: 10px;
      border-bottom: 2px solid #5a2cda;
    `};

  &:hover {
    color: ${colors.GRAY_90};
    cursor: pointer;
  }

  @media (max-width: 767px) {
    overflow: auto;
    white-space: nowrap;
    flex-wrap: nowrap;
  }
`;

type DividerProps = {
  lineColor?: string;
};

export const Divider = styled.div<DividerProps>`
  height: 1px;
  width: 100%;
  background-color: ${(props) =>
    props.lineColor ? props.lineColor : colors.gray_80};
`;

interface MenuButtonProps {
  isOpen: boolean;
}

export const MenuButton = styled(FlexButton)<MenuButtonProps>`
  width: 26px;
  height: 24px;
  cursor: pointer;
  svg {
    width: 100%;
    height: 100%;
  }

  transform: ${({ isOpen }) => (!isOpen ? "rotate(180deg)" : "none")};
  transition: transform 0.3s cubic-bezier(0.1, -0.15, 0.6, 1.45);

  @media (max-width: 1023px) {
    display: none;
  }
`;
