import React from "react";
import styled, { css } from "styled-components";
import { ModalPositionPortal } from "@wrtn/ui/components/ModalPortal";

import { GuideModal } from "@wrtn/ui/components/GuideModal";

import { colors, FlexWrapper, typo } from "@wrtn/ui/styles";

const GuideText = ({ children, update }) => {
  const guideRef = React.useRef<null | HTMLDivElement>(null);
  const [isOpenGuide, setIsOpenGuide] = React.useState(false);

  return (
    <div ref={guideRef}>
      <GuideLink onClick={() => setIsOpenGuide((c) => !c)}>
        {children}
      </GuideLink>
      {isOpenGuide && (
        <ModalPositionPortal
          position={{
            top: (guideRef?.current?.getBoundingClientRect().top || 0) + 36,
            left:
              (guideRef?.current?.getBoundingClientRect().left || 0) -
              (window.innerWidth < 480 ? window.innerWidth * 0.9 - 140 : 320),
          }}
        >
          <GuideModal
            guideRef={guideRef}
            onClose={() => setIsOpenGuide(false)}
            type="editor"
          />
        </ModalPositionPortal>
      )}
    </div>
  );
};

export default GuideText;

const GuideLink = styled.div`
  user-select: none;
  ${typo({
    size: "14px",
    weight: 600,
    height: "26px",
    color: colors.ACTION_BLUE,
  })}
  white-space: nowrap;

  text-decoration: underline;
  cursor: pointer;

  @media (max-width: 1023px) {
    font-size: 12px;
  }
`;
const Wrapper = styled(FlexWrapper)`
  display: flex;
  z-index: 20;
  position: absolute;
  background: transparent;
`;

const hoverContent = css`
  min-width: 469px;
  ${"" /* max-width: 90vw; */}
  min-height: 206px;

  padding: 22px 28px;

  @media (max-width: 767px) {
    min-width: 70vw;
    padding: 22px 16px;
  }
`;

const InnerWrapper = styled(FlexWrapper)`
  position: relative;
  flex-direction: column;
  align-items: flex-start;
  gap: 13px;
  @media (max-width: 480px) {
    gap: 20px;
  }
`;

const Title = styled.p`
  ${typo({
    weight: 600,
    size: "16px",
    height: "24px",
    color: colors.ACTION_BLUE,
  })}
  white-space: nowrap;
  @media (max-width: 480px) {
    font-size: 12px;
    white-space: normal;
  }
`;

const IconDesc = styled(FlexWrapper)`
  gap: 4px;
  ${"" /* white-space: nowrap; */}
  white-space: pre-wrap;
  ${typo({
    weight: 500,
    size: "14px",
    height: "21px",
    color: colors.GRAY_80,
  })}
  padding-bottom: 7px;
  @media (max-width: 480px) {
    font-size: 8px;
  }
`;
