import styled, { keyframes } from "styled-components";
import { boxShadow } from "@wrtn/ui/styles/boxShadow";
import { typo, colors } from "@wrtn/ui/styles";

const GradientButton = ({ disabled, handleGenerate, children }) => {
  return (
    <Button disabled={disabled} onClick={handleGenerate}>
      {children}
    </Button>
  );
};

export default GradientButton;

interface ButtonProps {}

const gradient = keyframes`
  0% {
    background-position: 10% 50%;
  }
  5%{
    background-position: 0% 50%;
  }
  45%{
    background-position: 100% 50%;
  }
  50% {
    background-position: 90% 50%;
  }
  55%{
    background-position: 100% 50%;
  }
  95%{
    background-position: 0% 50%;
  }
  100% {
    background-position: 10% 50%;
  }
`;

const Button = styled.button<ButtonProps>`
  flex: 1;
  /* width: 100%; */
  /* max-width: 392px; */
  // height: 42px;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 7px;

  background: linear-gradient(
    90.34deg,
    #c57dff -21.05%,
    #669cff 70.31%,
    #99e5f4 169.62%
  );
  background-size: 125% 100%;
  background-position: 10% 50%;
  animation: ${gradient} 5s ease-in-out infinite;
  animation-play-state: paused;
  &:hover {
    animation-play-state: running;
  }

  cursor: pointer;

  ${(props) => !props.disabled && boxShadow.color_button_shadow};
  border-radius: 4.45729px;

  &:disabled {
    background: #e6e8f0;
    cursor: not-allowed;
  }

  @media (max-width: 1023px) {
    width: 100%;
    min-height: 42px;
  }
`;
