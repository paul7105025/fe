import { ReactComponent as Check } from "../../assets/icons/icon_check.svg";
import styled from "styled-components";
import { colors } from "@wrtn/ui/styles";

interface CheckBoxProps {
  consent: boolean;
  handleClick: () => void;
}

export const CheckBox = ({ consent, handleClick }: CheckBoxProps) => {
  return (
    <CheckBoxWrapper
      consent={consent}
      onClick={(e) => {
        e.stopPropagation();
        handleClick();
      }}
    >
      <Check />
    </CheckBoxWrapper>
  );
};

const CheckBoxWrapper = styled.div<{ consent: boolean }>`
  width: 21px;
  height: 21px;

  cursor: pointer;
  background: ${(props) => (props.consent ? colors.ACTION_BLUE : colors.WHITE)};
  border: 2px solid
    ${(props) => (props.consent ? colors.ACTION_BLUE : colors.BLUE_GRAY_LINE)};
  border-radius: 1px;
  padding: 0px 3.515px;
`;
