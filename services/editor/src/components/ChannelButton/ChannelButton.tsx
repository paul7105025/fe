import styled from "styled-components";

import { IconHeadphone } from "@wrtn/ui/assets";
import { colors, FlexButton, typo } from "@wrtn/ui/styles";

const ChannelButton = ({ onClick }) => {
  return (
    <Button onClick={onClick}>
      <IconHeadphone />
      <p>문의하기</p>
    </Button>
  );
};

export default ChannelButton;

const Button = styled(FlexButton)`
  ${typo({
    size: "14px",
    weight: 600,
    height: "26px",
    color: colors.GRAY_70,
  })}

  border: none;
  background: none;
  text-decoration: underline;
  display: flex;
  align-items: center;
  cursor: pointer;
  gap: 5px;

  svg {
    width: 20px;
    height: 20px;
  }

  user-select: none;
`;
