import React from "react";
import styled from "styled-components";

import { colors, typo } from "@wrtn/ui/styles";

interface TitleInputProps {
  title: string;
  handleChangeTitle: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export const TitleInput = ({ title, handleChangeTitle }: TitleInputProps) => {
  const [width, setWidth] = React.useState(0);
  const span = React.useRef<HTMLElement>(null);

  React.useEffect(() => {
    if (title.length > 0) {
      setWidth(span.current ? span.current.offsetWidth : 0);
    } else setWidth(0);
  }, [title]);

  return (
    <div is="custom" style={{ position: "relative" }}>
      {title.length > 0 && (
        <HiddenSpan id="hide" ref={span}>
          {title}
        </HiddenSpan>
      )}
      <Title
        placeholder="문서 이름"
        type="text"
        style={{ width: width + 20 }}
        autoFocus
        value={title}
        onChange={handleChangeTitle}
      />
    </div>
  );
};



const Title = styled.input`
  ${typo({
    weight: 600,
    size: "18px",
    height: "100%",
    color: colors.gray_90,
  })}

  min-width: 90px;

  padding: 6px 10px;

  max-width: 50vw;

  text-align: right;

  ::placeholder {
    ${typo({
      weight: 600,
      size: "18px",
      height: "100%",
      color: colors.gray_55,
    })}
  }

  :focus {
    border-radius: 5px;
    background-color: ${colors.BACKGROUND};
  }
`;

const HiddenSpan = styled.span`
  ${typo({
    weight: 600,
    size: "18px",
    height: "100%",
    color: colors.gray_90,
  })}

  position: absolute;
  right: 0px;
  opacity: 0;
  z-index: -100;
  white-space: pre;
`;
