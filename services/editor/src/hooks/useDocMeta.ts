import _ from "lodash";
import React from "react";

import { putDoc_Id } from "@wrtn/core/services/editor";
import {
  docMetaType,
  convertConnectSelector,
  convertCategorySelector,
} from "../types";

import { useNonInitialEffect } from "@wrtn/core";

const saveMeta = _.throttle(async (docId: string, meta: docMetaType) => {
  await putDoc_Id({
    id: docId,
    data: {
      topic: meta.topic,
      category: meta.category,
      connect: convertConnectSelector[meta.connect],
    },
  });
}, 2200);

const isSentence = (prevText: string) => {
  return !!prevText.trim().match(/[.?!]$/);
};

interface useDocMetaArgs {
  docId: string | undefined;
  metadata: docMetaType;
}

const useDocMeta = ({ docId, metadata }: useDocMetaArgs) => {
  const [documentMetadata, setDocumentMetadata] =
    React.useState<docMetaType>(metadata);

  const setMeta = (key: keyof docMetaType, value: any) => {
    setDocumentMetadata((res) => ({
      ...res,
      [key]: value,
    }));
  };

  const formatMeta = (prevText: string) => {
    return {
      inputs: [
        documentMetadata.topic,
        documentMetadata.category,
        prevText,
        // prevText.length > 0
        //   ? isSentence(prevText)
        //     ? prevText
        //     : prevText
        //   : documentMetadata.topic,
      ],
      connect: isSentence(prevText) ? documentMetadata.connect : "없음",
      docId: docId,
    };
  };

  useNonInitialEffect(() => {
    if (docId) {
      saveMeta(docId, documentMetadata);
    }
  }, [docId, documentMetadata]);

  return { documentMetadata, setMeta, formatMeta };
};

export default useDocMeta;
