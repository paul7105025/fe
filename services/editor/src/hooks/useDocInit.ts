import _ from "lodash";
import React from "react";
import { EditorState } from "lexical";

import { getDoc_Id, putDoc_Id } from "@wrtn/core/services";

import { docMetaType, saveStatusType, reverseConnectSelector } from "../types";

const handlePutTitle = _.debounce((val, docId) => {
  putDoc_Id({
    id: docId,
    data: {
      title: val,
    },
  });
}, 1000);
//
const useDocInit = (docId?: string) => {
  const [title, setTitle] = React.useState("");
  const [content, setContent] = React.useState<EditorState>();
  const [metadata, setMetadata] = React.useState<docMetaType>({
    topic: "",
    category: "선택 안 함",
    connect: "선택 안 함",
  });

  const [loading, setLoading] = React.useState(true);
  const [saveStatus, setSaveStatus] = React.useState<saveStatusType>("saved");

  const getDocInfo = async (docId: string) => {
    try {
      setLoading(true);
      const res = await getDoc_Id({ id: docId });

      if (res?.status === 200) {
        const { data } = res.data;

        setTitle(data.title);
        setContent(data.content);
        setMetadata({
          topic: data.topic,
          category: data.category,
          connect: reverseConnectSelector[data.connect],
        });
        setLoading(false);
      }
    } catch (error) {
    } finally {
    }
  };

  React.useEffect(() => {
    if (docId) {
      getDocInfo(docId);
    }
  }, [docId]);

  const handleChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
    handlePutTitle(e.target.value, docId);
  };

  return {
    title,
    content,
    metadata,
    loading,
    saveStatus,
    handleChangeTitle,
    setSaveStatus,
  };
};

export default useDocInit;
