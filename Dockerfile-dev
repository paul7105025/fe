# 1. turborepo 복사
FROM node:alpine AS builder
RUN apk add --no-cache libc6-compat
RUN apk update

WORKDIR /app

ARG PNPM_VERSION=8.1.0

ENV PNPM_HOME=/root/.local/share/pnpm
ENV PATH=$PATH:$PNPM_HOME
# RUN wget -qO- https://get.pnpm.io/install.sh | ENV="$HOME/.shrc" SHELL="$(which sh)" sh -
# RUN apk add --no-cache curl && \
#   curl -fsSL "https://github.com/pnpm/pnpm/releases/download/v${PNPM_VERSION}/pnpm-linuxstatic-x64" -o /bin/pnpm && chmod +x /bin/pnpm && \
#   apk del curl
RUN apk add --no-cache git
# RUN npm install -g pnpm
# RUN mv /root/.local/share/pnpm/pnpm /usr/bin/
RUN yarn global add turbo
COPY . .
RUN turbo prune --scope="@wrtn/web" --docker
 
# Add lockfile and package.json's of isolated subworkspace

# 2. 프로젝트 빌드
FROM node:alpine AS installer
RUN apk add --no-cache libc6-compat
RUN apk update
WORKDIR /app
 
# First install the dependencies (as they change less often)
COPY .gitignore .gitignore
COPY --from=builder /app/out/json/ .
COPY --from=builder /app/out/pnpm-lock.yaml ./pnpm-lock.yaml

RUN mkdir -p ./platforms
RUN mkdir -p ./packages

# Build the project
COPY --from=builder /app/out/full/platforms ./platforms
COPY --from=builder /app/out/full/packages ./packages
COPY --from=builder /app/out/full/.gitignore ./.gitignore
COPY --from=builder /app/out/full/package.json ./package.json
COPY --from=builder /app/out/full/babel.config.json ./babel.config.json
COPY --from=builder /app/out/full/pnpm-workspace.yaml ./pnpm-workspace.yaml
COPY --from=builder /app/out/full/turbo.json ./turbo.json
RUN yarn global add pnpm
RUN pnpm install
RUN pnpm install --prod --frozen-lockfile --offline --shamefully-hoist --ignore-scripts --workspace-root --filter ${PROJECT_NAME} && \
    cp -Lr ./node_modules ./node_modules_temp && \
    rm -rf ./node_modules_temp/.cache && \
    rm -rf ./node_modules_temp/.pnpm
# RUN yarn turbo run build --filter=web...
RUN yarn turbo run build --filter=@wrtn/web
 
 # 실제 파일로
FROM node:alpine AS runner
WORKDIR /app
 
# Don't run production as root
RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs
USER nextjs
 
COPY --from=installer /app/platforms/web/next.config.js .
COPY --from=installer /app/platforms/web/package.json .
 
# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=installer --chown=nextjs:nodejs /app/platforms/web/.next/standalone ./
COPY --from=installer --chown=nextjs:nodejs /app/platforms/web/.next/static ./.next/static
# COPY --from=installer --chown=nextjs:nodejs /app/platforms/web/public ./public
COPY --from=installer /app/node_modules_temp ./node_modules

EXPOSE 3000

ENV PORT 3000

# RUN yarn global add pnpm
# RUN pnpm i
CMD ["node", "./platforms/web/server.js"]